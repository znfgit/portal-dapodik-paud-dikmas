
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- artikel
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `artikel`;

CREATE TABLE `artikel`
(
    `ARTIKEL_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `JUDUL` VARCHAR(100) NOT NULL,
    `ISI` VARCHAR(5000) NOT NULL,
    `GAMBAR` VARCHAR(100),
    `TANGGAL_EDIT` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`ARTIKEL_ID`),
    INDEX `FK_REFERENCE_15` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_15`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- berita
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `berita`;

CREATE TABLE `berita`
(
    `BERITA_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `JUDUL` VARCHAR(100) NOT NULL,
    `ISI` VARCHAR(5000) NOT NULL,
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `GAMBAR` VARCHAR(50),
    PRIMARY KEY (`BERITA_ID`),
    INDEX `FK_REFERENCE_1` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_1`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- dokumentasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `dokumentasi`;

CREATE TABLE `dokumentasi`
(
    `DOKUMENTASI_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `JENIS_DOKUMENTASI_ID` INTEGER,
    `JUDUL` VARCHAR(100),
    `DESKRIPSI` VARCHAR(5000),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `GAMBAR` VARCHAR(50),
    PRIMARY KEY (`DOKUMENTASI_ID`),
    INDEX `FK_REFERENCE_12` (`PENGGUNA_ID`),
    INDEX `FK_REFERENCE_13` (`JENIS_DOKUMENTASI_ID`),
    CONSTRAINT `FK_REFERENCE_12`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_13`
        FOREIGN KEY (`JENIS_DOKUMENTASI_ID`)
        REFERENCES `jenis_dokumentasi` (`JENIS_DOKUMENTASI_ID`)
        ON UPDATE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- faq
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `faq`;

CREATE TABLE `faq`
(
    `FAQ_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `PERTANYAAN` VARCHAR(100),
    `JAWABAN` VARCHAR(5000),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`FAQ_ID`),
    INDEX `FK_REFERENCE_2` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_2`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- feedback
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback`
(
    `FEEDBACK_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `JENIS_FEEDBACK_ID` INTEGER,
    `JUDUL` VARCHAR(100),
    `DESKRIPSI` VARCHAR(5000),
    `GAMBAR` VARCHAR(50),
    `EMAIL_PENGIRIM` VARCHAR(30),
    `INSTANSI` VARCHAR(100),
    `NOMOR_TELEPON` VARCHAR(14),
    `TANGGAL_LAPOR` DATETIME,
    `STATUS` INTEGER,
    `TANGGAL_UPDATE` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`FEEDBACK_ID`),
    INDEX `FK_REFERENCE_14` (`JENIS_FEEDBACK_ID`),
    CONSTRAINT `FK_REFERENCE_14`
        FOREIGN KEY (`JENIS_FEEDBACK_ID`)
        REFERENCES `jenis_feedback` (`JENIS_FEEDBACK_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- file
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file`
(
    `FILE_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `JENIS_FILE_ID` INTEGER,
    `PENGGUNA_ID` INTEGER,
    `NAMA` VARCHAR(100),
    `FILE` VARCHAR(100),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `VERSI` VARCHAR(50),
    PRIMARY KEY (`FILE_ID`),
    INDEX `FK_REFERENCE_6` (`JENIS_FILE_ID`),
    INDEX `FK_REFERENCE_7` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_6`
        FOREIGN KEY (`JENIS_FILE_ID`)
        REFERENCES `jenis_file` (`JENIS_FILE_ID`),
    CONSTRAINT `FK_REFERENCE_7`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- installer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `installer`;

CREATE TABLE `installer`
(
    `INSTALLER_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `NAMA` VARCHAR(100),
    `FILE` VARCHAR(100),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `VERSI` VARCHAR(20),
    `ACTIVE` INTEGER,
    `DESKRIPSI` VARCHAR(5000),
    PRIMARY KEY (`INSTALLER_ID`),
    INDEX `FK_REFERENCE_3` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_3`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- jenis_dokumentasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_dokumentasi`;

CREATE TABLE `jenis_dokumentasi`
(
    `JENIS_DOKUMENTASI_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `NAMA` VARCHAR(100),
    PRIMARY KEY (`JENIS_DOKUMENTASI_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- jenis_feedback
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_feedback`;

CREATE TABLE `jenis_feedback`
(
    `JENIS_FEEDBACK_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `NAMA` VARCHAR(50),
    PRIMARY KEY (`JENIS_FEEDBACK_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- jenis_file
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_file`;

CREATE TABLE `jenis_file`
(
    `JENIS_FILE_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `NAMA` VARCHAR(100),
    PRIMARY KEY (`JENIS_FILE_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- jenis_kelamin
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_kelamin`;

CREATE TABLE `jenis_kelamin`
(
    `JENIS_KELAMIN_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `NAMA` VARCHAR(20),
    PRIMARY KEY (`JENIS_KELAMIN_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- log_download_installer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `log_download_installer`;

CREATE TABLE `log_download_installer`
(
    `LOG_DOWNLOAD_INSTALLER_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `INSTALLER_ID` INTEGER,
    `IP` VARCHAR(100),
    `BROWSER` VARCHAR(100),
    `OS` VARCHAR(100),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`LOG_DOWNLOAD_INSTALLER_ID`),
    INDEX `FK_REFERENCE_9` (`INSTALLER_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- log_download_patch
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `log_download_patch`;

CREATE TABLE `log_download_patch`
(
    `LOG_DOWNLOAD_PATCH_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PATCH_ID` INTEGER,
    `IP` VARCHAR(50),
    `BROWSER` VARCHAR(100),
    `OS` VARCHAR(50),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`LOG_DOWNLOAD_PATCH_ID`),
    INDEX `FK_REFERENCE_16` (`PATCH_ID`),
    CONSTRAINT `FK_REFERENCE_16`
        FOREIGN KEY (`PATCH_ID`)
        REFERENCES `patch` (`PATCH_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- log_login
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `log_login`;

CREATE TABLE `log_login`
(
    `LOG_LOGIN_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `IP` VARCHAR(50),
    `BROWSER` VARCHAR(50),
    `OS` VARCHAR(50),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`LOG_LOGIN_ID`),
    INDEX `FK_REFERENCE_10` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_10`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- log_visitor
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `log_visitor`;

CREATE TABLE `log_visitor`
(
    `LOG_VISITOR_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `IP` VARCHAR(20),
    `BROWSER` VARCHAR(20),
    `OS` VARCHAR(20),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`LOG_VISITOR_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- media_sosial
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `media_sosial`;

CREATE TABLE `media_sosial`
(
    `MEDIA_SOSIAL_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `NAMA` VARCHAR(100),
    `URL` VARCHAR(100),
    `LOGO` VARCHAR(50),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`MEDIA_SOSIAL_ID`),
    INDEX `FK_REFERENCE_8` (`PENGGUNA_ID`),
    CONSTRAINT `FK_REFERENCE_8`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- patch
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `patch`;

CREATE TABLE `patch`
(
    `PATCH_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PENGGUNA_ID` INTEGER,
    `INSTALLER_ID` INTEGER,
    `NAMA` VARCHAR(100),
    `FILE` VARCHAR(100),
    `TANGGAL` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `VERSI` VARCHAR(20),
    `ACTIVE` INTEGER,
    `DESKRIPSI` VARCHAR(5000),
    PRIMARY KEY (`PATCH_ID`),
    INDEX `FK_REFERENCE_4` (`PENGGUNA_ID`),
    INDEX `FK_REFERENCE_5` (`INSTALLER_ID`),
    CONSTRAINT `FK_REFERENCE_4`
        FOREIGN KEY (`PENGGUNA_ID`)
        REFERENCES `pengguna` (`PENGGUNA_ID`)
        ON UPDATE SET NULL
        ON DELETE SET NULL,
    CONSTRAINT `FK_REFERENCE_5`
        FOREIGN KEY (`INSTALLER_ID`)
        REFERENCES `installer` (`INSTALLER_ID`)
        ON UPDATE SET NULL
        ON DELETE SET NULL
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- pengguna
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna`
(
    `PENGGUNA_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `JENIS_KELAMIN_ID` INTEGER,
    `NAMA` VARCHAR(100),
    `EMAIL` VARCHAR(100),
    `USERNAME` VARCHAR(100),
    `PASSWORD` VARCHAR(100),
    PRIMARY KEY (`PENGGUNA_ID`),
    INDEX `FK_REFERENCE_11` (`JENIS_KELAMIN_ID`),
    CONSTRAINT `FK_REFERENCE_11`
        FOREIGN KEY (`JENIS_KELAMIN_ID`)
        REFERENCES `jenis_kelamin` (`JENIS_KELAMIN_ID`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
