<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Debug;
use infopendataan\Model as M;

require_once __DIR__.'/bootstrap.php';

define("SEMESTER_ID", "20142");
define("TAHUN_AJARAN_ID", "2014");
/**
 * Enable Silex Error
 */
// $app->error(function (\Exception $e, $code) use ($app){
//     switch ($code) {
//         case 404:
//             $message = 'The requested page could not be found.';
        	
//             break;
//         default:
//             $message = 'We are sorry, but something went terribly wrong.';
            
//     }

//     //return new Response($message);
    
//     return $app->redirect("laman/error");
//     // return $app['twig']->render('index.twig');
// });

$app->get('pengumuman', function() use ($app){
	return $app['twig']->render('templates/pengumuman.twig');
});

$app->get('/', function() use ($app){
	
	$array = array(
        'konektor' => "",
        'laman' => "laman/",
        'judul' => '',
        'host' => $_SERVER["HTTP_HOST"],
        'tahun' => date('Y')
    );

	if ($app['session']->get('user')){
		$user = $app['session']->get('user');

		$array['user'] = $user['nama'];
		$array['sekolah_id'] = $user['sekolah_id'];
		$array['peran_id'] = $user['peran_id'];
		$array['peran_id_str'] = $user['peran_id_str'];
		$array['wilayah_id_str'] = $user['wilayah_id_str'];
		$array['sekolah_id_str'] = $user['sekolah_id_str'];
		$array['mst_kode_wilayah_str'] = $user['mst_kode_wilayah_str'];
	}

	return $app['twig']->render('index.twig',$array);

});

$app->get('/loginsob', function() use ($app){
	$array = array(
        'konektor' => ""
    );

	return $app['twig']->render('login.twig', $array);
});

$app->get('/rest/pengguna', 'library\Rest::pengguna');
$app->get('/rest/latest', 'library\Rest::latestDownload');
$app->get('/rest/faq', 'library\Rest::getFaq');
$app->get('/rest/jenisdokumentasi', 'library\Rest::getJenisDokumentasi');
$app->get('/rest/prevpatch', 'library\Rest::previousPatch');

//rss
$app->get('/rss/{model}', 'library\Rss::get');

$app->get('laman/', function() use ($app){
	return $app->redirect("../");
});

$app->get('report/lastSync', 'library\Rest::getLastSync');
$app->get('report/top5prop', 'library\Rest::top5Prop');
$app->get('report/top10kab', 'library\Rest::top10Kab');
$app->get('report/reportProp/{semester_id}', 'library\Rest::reportProp');
$app->get('report/reportPropPeriodik/{semester_id}', 'library\Rest::reportPropPeriodik');
$app->get('report/cekdataun_nasional', 'library\Rest::cekdataun_nasional');
$app->get('report/cekdataun_nasional/{id_level}/{kode_wilayah}', 'library\Rest::cekdataun_nasional');
$app->get('report/cekdataun_kecamatan/{id_level}/{kode_wilayah}', 'library\Rest::cekdataun_kecamatan');
$app->get('report/reportKab/{kode_wilayah}', 'library\Rest::reportKab');
$app->get('report/reportKabPeriodik/{kode_wilayah}', 'library\Rest::reportKabPeriodik');
$app->get('report/reportKec/{kode_wilayah}', 'library\Rest::reportKec');
$app->get('report/reportKecDetail/{kode_wilayah}', 'library\Rest::reportSekolahPerKecamatan');
$app->get('report/reportKabDetail/{kode_wilayah}', 'library\Rest::reportSekolahPerKabupaten');
$app->get('report/detailSekolah/{sekolah_id}', 'library\Rest::detailSekolah');
$app->get('report/petaSekolah/{sekolah_id}', 'library\Rest::petaSekolah');
$app->get('report/daftarPtk/{sekolah_id}', 'library\Rest::daftarPtk');
$app->get('report/rekapall', 'library\Rest::getRekapAll');
$app->get('report/rekapall2', 'library\Rest::getRekapAll2');
$app->get('report/rekapall3', 'library\Rest::getRekapAll3');
$app->get('report/countsyncday', 'library\Rest::getCountSyncPerDay');
$app->get('report/rekapPdNasional/{semester_id}', 'library\Rest::rekapPdNasional');
$app->get('report/rekapPdPropinsi/{kode_wilayah}/{semester_id}', 'library\Rest::rekapPdPropinsi');
$app->get('report/rekapPdKabkota/{kode_wilayah}/{semester_id}', 'library\Rest::rekapPdKabkota');
$app->get('report/getRekapDapodikCore', 'library\Rest::getRekapDapodikCore');
$app->get('report/getRekapDapodikCore2', 'library\Rest::getRekapDapodikCore2');
$app->get('report/getRekapDapodikPdRombel', 'library\Rest::getRekapDapodikPdRombel');
$app->get('report/getRekapDapodikPdNegeriSwasta', 'library\Rest::getRekapDapodikPdNegeriSwasta');
$app->get('report/getRekapDapodikSekolahStatus', 'library\Rest::getRekapDapodikSekolahStatus');
$app->get('report/rekapPesertaDidikStatus/{semester_id}', 'library\Rest::getRekapPesertaDidikStatus');
$app->get('report/chartBarPdRombel', 'library\Rest::chartBarPdRombel');
$app->get('report/chartPiePdRombel', 'library\Rest::chartPiePdRombel');
$app->get('report/chartPiePdStatusTotal', 'library\Rest::chartPiePdStatusTotal');
$app->get('report/chartPiePdStatusSma', 'library\Rest::chartPiePdStatusSma');
$app->get('report/chartPiePdStatusSmk', 'library\Rest::chartPiePdStatusSmk');
$app->get('report/chartPiePdStatusSmlb', 'library\Rest::chartPiePdStatusSmlb');
$app->get('report/chartBarPdStatusTotal', 'library\Rest::chartBarPdStatusTotal');
$app->get('report/chartPieSekolahStatus/{jenis}', 'library\Rest::chartPieSekolahStatus');
$app->get('report/lihatSekolah/{id_level_wilayah}/{kode_wilayah}', 'library\Rest::lihatSekolah');

$app->get('data/logSinkronisasi/{sekolah_id}', 'library\Rest::getLogSinkronisasi');
$app->get('getSekolahLongitudinal', 'library\Rest::getSekolahLongitudinal');

$app->get('breadcrumb/reportProp/{kode_wilayah}', 'library\Rest::breadcrumbProp');
$app->get('breadcrumb/reportKab/{kode_wilayah}', 'library\Rest::breadcrumbKab');
$app->get('breadcrumb/reportKec/{kode_wilayah}', 'library\Rest::breadcrumbKec');
$app->get('breadcrumb/datasekolah/{sekolah_id}', 'library\Rest::breadcrumbDataSekolah');
$app->get('breadcrumb/sekolahAsal/{ptk_id}', 'library\Rest::breadcrumbSekolahAsal');
$app->get('breadcrumb/getDetailPtk/{ptk_id}', 'library\Rest::breadcrumbGetDetailPtk');
$app->get('breadcrumb/getDetailSekolahAsalPtk/{ptk_id}', 'library\Rest::getDetailSekolahAsalPtk');
$app->get('breadcrumb/getPenugasanPtk/{ptk_id}/{ta}', 'library\Rest::getPenugasanPtk');
$app->get('breadcrumb/getjumlahsiswaperrombel/{sekolah_id}', 'library\Rest::getJumlahSiswaPerRombel');
$app->get('breadcrumb/breadcrumbDataSekolahJumlahPtk/{sekolah_id}/{semester_id}', 'library\Rest::breadcrumbDataSekolahJumlahPtk');
$app->get('breadcrumb/breadcrumbDataSekolahJumlahPd/{sekolah_id}/{semester_id}', 'library\Rest::breadcrumbDataSekolahJumlahPd');
$app->get('breadcrumb/breadcrumbDataSekolahJumlahPdTotal/{sekolah_id}', 'library\Rest::breadcrumbDataSekolahJumlahPdTotal');
$app->get('breadcrumb/getsemesterterakhir', 'library\Rest::getSemesterTerakhir');
$app->get('breadcrumb/getsemester', 'library\Rest::getSemester');
$app->get('breadcrumb/getTahunAjaran', 'library\Rest::getTahunAjaran');
$app->get('breadcrumb/rekappdpropinsi/{kode_wilayah}', 'library\Rest::breadcrumbrekappdpropinsi');
$app->get('breadcrumb/rekappdkabkota/{kode_wilayah}', 'library\Rest::breadcrumbrekappdkabkota');
$app->get('breadcrumb/breadcrumbpdperrombel/{sekolah_id}/{semester_id}', 'library\Rest::breadcrumbpdperrombel');

$app->post('report/validasiKoreg', 'library\Rest::validasiKoreg');

$app->get('getTentang', 'library\Rest::getTentang');
$app->get('latestDownload', 'library\Rest::latestDownload');
$app->get('getDokumentasi', 'library\Rest::getDokumentasi');
$app->get('getFormulir', 'library\Rest::getFormulir');

$app->get('datasekolah/{sekolah_id}', 'library\Rest::getDataSekolah');

$app->get('peta/geojson', 'library\Report::geojson');

$app->get('laman/rekapdapodik/{p}', function($p) use ($app){

	$array = array(
        'p' => $p,
        'konektor' => "../../",
        'laman' => "../",
        'tahun' => date('Y')
    );

    switch ($p) {

		case 'pesertadidik':

			$array['judul'] = "Rekapitulasi Peserta Didik - Dapodik PAUD-Dikmas";
			
			break;

		case 'sekolah':

			$array['judul'] = "Rekapitulasi Sekolah - Dapodik PAUD-Dikmas";
			
			break;
	}

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/{p}', function($p) use ($app){

	$array = array(
        'p' => $p,
        'konektor' => "../",
        'laman' => "",
        'tahun' => date('Y')
    );

	switch ($p) {

		case 'beranda':
			
			$array['judul'] = "Dapodik PAUD-Dikmas";
			
			break;

		case 'download':

			$array['judul'] = "Download - Dapodik PAUD-Dikmas";		
			
			break;

		case 'dokumentasi':

			$jenis_dokumentasi = library\Rest::getJenisDokumentasi();

			if(!empty($jenis_dokumentasi)){
				$array['jd'] = $jenis_dokumentasi;
			}

			$array['judul'] = "Dokumentasi  - Dapodik PAUD-Dikmas";
			
			break;

		case 'faq':

			$faq = library\Rest::getFaq();

			if(!empty($faq)){
				$array['faq'] = $faq;
			}

			$array['judul'] = "F.A.Q - Dapodik PAUD-Dikmas";
			
			break;

		case 'helpdesk':

			$array['judul'] = "Helpdesk - Dapodik PAUD-Dikmas";
			
			break;

		case 'tentang':
			$array['judul'] = "Tentang - Dapodik PAUD-Dikmas";
			
			break;

		case 'feedback':

			$jenis_feedback = library\Rest::getJenisFeedback();

			if(!empty($jenis_feedback)){
				$array['jenis_feedback'] = $jenis_feedback;
			}

			$array['judul'] = "Feedback - Dapodik PAUD-Dikmas";
			
			break;
		
		case 'datapokok':


			$array['judul'] = "Data Pokok - Dapodik PAUD-Dikmas";
			
			break;

		case 'statistikgrafik':


			$array['judul'] = "Statistik Grafik - Dapodik PAUD-Dikmas";
			
			break;

		case 'grafikrekap':
			$array['judul'] = "Grafik Rekap - Dapodik PAUD-Dikmas";
			
			break;

		case 'detailptk':
			$array['judul'] = "Detail PTK - Dapodik PAUD-Dikmas";
			
			break;

		case 'rekappdnasional':
			$array['judul'] = "Rekapitulasi Jumlah Peserta Didik Berdasarkan Pemetaan Rombel - Dapodik PAUD-Dikmas";
			
			break;

		case 'rekappdpropinsi':
			$array['judul'] = "Rekapitulasi Jumlah Peserta Didik Berdasarkan Pemetaan Rombel - Dapodik PAUD-Dikmas";
			
			break;

		case 'rekappdkabkota':
			$array['judul'] = "Rekapitulasi Jumlah Peserta Didik Berdasarkan Pemetaan Rombel - Dapodik PAUD-Dikmas";
			
			break;

		case 'rekapdapodik':
			$array['judul'] = "Rekapitulasi Data Pokok - Dapodik PAUD-Dikmas";

		case 'rekappdperstatussekolah':
			$array['judul'] = "Rekapitulasi Jumlah Peserta Didik Berdasarkan Status Sekolah - Dapodik PAUD-Dikmas";

			break;

		default:

			$array['judul'] = "Dapodik PAUD-Dikmas";

			break;

	}

	return $app['twig']->render('index.twig',$array);

	//return var_dump($latest_download);
}); 

$app->get('download/{installer_id}', 'library\Rest::getDownload');
$app->get('downloadpatch/{patch_id}', 'library\Rest::getPatch');

$app->post('/kirimfeedback', 'library\Rest::kirimFeedback');
$app->post('/proseslogin', 'library\login::proseslogin');
$app->get('/logout', 'library\login::prosesLogout');


$app->get('/laman/feedback/{status}', function($status) use ($app){

	$jenis_feedback = library\Rest::getJenisFeedback();

	$array = array(
        'p' => 'feedback',
        'konektor' => "../../",
        'laman' => "../",
        'status' => $status,
        'jenis_feedback' => $jenis_feedback,
        'tahun' => date('Y'),
        'judul' => 'Feedback - Pendataan'
    );

    return $app['twig']->render('index.twig',$array);
});

$app->get('laman/detailprop/{kode_wilayah}', function($kode_wilayah) use ($app){
	$array = array(
        'konektor' => "../../",
        'laman' => "../",
        'kode_wilayah' => $kode_wilayah,
        'tahun' => date('Y'),
        'p' => 'detailprop',
        'judul' => 'Progress Pengiriman - Pendataan'
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/detailkab/{kode_wilayah}', function($kode_wilayah) use ($app){
	$array = array(
        'konektor' => "../../",
        'laman' => "../",
        'kode_wilayah' => $kode_wilayah,
        'tahun' => date('Y'),
        'p' => 'detailkab',
        'judul' => 'Progress Pengiriman - Pendataan'
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/detailkec/{kode_wilayah}', function($kode_wilayah) use ($app){
	$array = array(
        'konektor' => "../../",
        'laman' => "../",
        'kode_wilayah' => $kode_wilayah,
        'tahun' => date('Y'),
        'p' => 'detailkec',
        'judul' => 'Progress Pengiriman - Pendataan'
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/detailSekolah/{sekolah_id}', function($sekolah_id) use ($app){

	$user = $app['session']->get('user');
	// return $user['sekolah_id'];

	if ($user['sekolah_id']){

		if($user['peran_id'] == 10){

			if($user['sekolah_id'] == $sekolah_id){
				// $con = library\Rest::initdb();
				// $linbur = library\Rest::petaSekolah($sekolah_id,$con);

				// return $linbur['lintang'];
				if($con){
					return "konek";
				}

				$array = array(
			        'konektor' => "../../",
			        'laman' => "../",
			        'sekolah_id' => $sekolah_id,
			        'tahun' => date('Y'),
			        'p' => 'detailSekolah',
			        'judul' => 'Detail Sekolah - Pendataan',
			        // 'lintang' => $linbur['lintang'],
			        // 'bujur' => $linbur['bujur'],
			        // 'label' => $linbur['label']
			        'lintang' => 0,
			        'bujur' => 0,
			        'label' => 0
			    );

				return $app['twig']->render('index.twig',$array);
			}else{
				return $app->redirect("../../");
			}

			
		}else if($user['peran_id'] == 1){
			$array = array(
		        'konektor' => "../../",
		        'laman' => "../",
		        'sekolah_id' => $sekolah_id,
		        'tahun' => date('Y'),
		        'p' => 'detailSekolah',
		        'judul' => 'Detail Sekolah - Pendataan',
		        // 'lintang' => $linbur['lintang'],
		        // 'bujur' => $linbur['bujur'],
		        // 'label' => $linbur['label']
		        'lintang' => 0,
		        'bujur' => 0,
		        'label' => 0
		    );

			return $app['twig']->render('index.twig',$array);
		}else{
			return $app->redirect("../../");
		}
	}else{
		return $app->redirect("../../");
	}

});

$app->get('laman/detailBerita/{tanggal}/{judul}', function($tanggal, $judul) use ($app){
	$array = array(
        'konektor' => "../../../",
        'laman' => "../../",
        'tahun' => date('Y'),
        'p' => 'detailberita',
        'judul' => 'Berita - Pendataan',
        'judul_berita' => $judul,
        'tanggal' => $tanggal

    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/datasekolah/{sekolah_id}', function($sekolah_id) use ($app){
	$array = array(
        'konektor' => "../../",
        'laman' => "../",
        'tahun' => date('Y'),
        'p' => 'datasekolah',
        'judul' => 'Data - Pendataan',
        'sekolah_id' => $sekolah_id

    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/detailptk/{ptk_id}', function($ptk_id) use ($app){
	if ($app['session']->get('user')){

		$user = $app['session']->get('user');

		if($user['peran_id'] == 10){
			$con = library\Rest::initdb();
			$sekolah_id = library\Rest::sekolahIdPtk($ptk_id,$con);

			if($sekolah_id == $user['sekolah_id']){

				$array = array(
			        'konektor' => "../../",
			        'laman' => "../",
			        'tahun' => date('Y'),
			        'p' => 'detailptk',
			        'judul' => 'Detail PTK - Pendataan',
			        'ptk_id' => $ptk_id
			    );

				return $app['twig']->render('index.twig',$array);

			}else{

				return $app->redirect("../../");
			}

			// return $sekolah_id;

		}else{
			return $app->redirect("../../");
		}

	}else{
		return $app->redirect("../../");
	}
});

$app->get('laman/rekappdpropinsi/{kode_wilayah}', function($kode_wilayah) use ($app){
	$array = array(
		'konektor' => "../../",
        'laman' => "../",
        'tahun' => date('Y'),
        'p' => 'rekappdpropinsi',
        'judul' => 'Rekapitulasi Jumlah Peserta Didik Berdasarkan Pemetaan Rombel - Pendataan',
        'kode_wilayah' => $kode_wilayah
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/rekappdkabkota/{kode_wilayah}', function($kode_wilayah) use ($app){
	$array = array(
		'konektor' => "../../",
        'laman' => "../",
        'tahun' => date('Y'),
        'p' => 'rekappdkabkota',
        'judul' => 'Rekapitulasi Jumlah Peserta Didik Berdasarkan Pemetaan Rombel - Pendataan',
        'kode_wilayah' => $kode_wilayah
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('tesdata', function() use ($app){
	

	return '[
			   {
			      "key":"Laki - Laki",
			      "values":[
			         {
			            "x":"Kelas 1",
			            "y":1676
			         },
			         {
			            "x":"Kelas 2",
			            "y":1646
			         },
			         {
			            "x":"Kelas 3",
			            "y":1562
			         },
			         {
			            "x":"Kelas 4",
			            "y":1523
			         },
			         {
			            "x":"Kelas 5",
			            "y":1354
			         },
			         {
			            "x":"Kelas 6",
			            "y":1383
			         }
			      ]
			   }
			]';
});

$app->get('report/daftarPtkJson/{sekolah_id}/{semester_id}', 'library\Rest::daftarPtkJson');
$app->get('report/daftarPdJson/{sekolah_id}/{semester_id}', 'library\Rest::daftarPdJson');
$app->get('report/daftarRombelJson/{sekolah_id}/{semester_id}', 'library\Rest::daftarRombelJson');
$app->get('report/Berita', 'library\Rest::getBerita');
$app->get('report/getBeritaAll', 'library\Rest::getBeritaAll');
$app->get('report/getDetailBerita/{tanggal}/{judul}', 'library\Rest::getDetailBerita');

$app->get('report/chart/{model}', 'library\Rest::getChart');

$app->get('listKabupaten/{offset}', 'library\Rest::getListBerita');
$app->get('rest/{model}', 'library\Rest::resting');
$app->get('rest/{model}/{kode}', 'library\Rest::resting');

$app->get('laman/cekdataun_propinsi/{id_level_wilayah}/{kode_wilayah}', function($id_level_wilayah, $kode_wilayah) use ($app){
	$array = array(
        'konektor' => "../../../",
        'laman' => "../../",
        'kode_wilayah' => $kode_wilayah,
        'id_level_wilayah' => $id_level_wilayah,
        'tahun' => date('Y'),
        'p' => 'cekdataun_propinsi',
        'judul' => 'Pendataan'
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/cekdataun_kabupaten/{id_level_wilayah}/{kode_wilayah}', function($id_level_wilayah, $kode_wilayah) use ($app){
	$array = array(
        'konektor' => "../../../",
        'laman' => "../../",
        'kode_wilayah' => $kode_wilayah,
        'id_level_wilayah' => $id_level_wilayah,
        'tahun' => date('Y'),
        'p' => 'cekdataun_propinsi',
        'judul' => 'Pendataan'
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('laman/cekdataun_kecamatan/{id_level_wilayah}/{kode_wilayah}', function($id_level_wilayah, $kode_wilayah) use ($app){
	$array = array(
        'konektor' => "../../../",
        'laman' => "../../",
        'kode_wilayah' => $kode_wilayah,
        'id_level_wilayah' => $id_level_wilayah,
        'tahun' => date('Y'),
        'p' => 'cekdataun_kecamatan',
        'judul' => 'Pendataan'
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('cek/debugging/tesSession', function() use ($app){
	$user = $app['session']->get('user');
	// return $user['sekolah_id'];
});

$app->get('laman/lihatSekolah/{id_level_wilayah}/{kode_wilayah}', function($id_level_wilayah, $kode_wilayah) use ($app){
	$array = array(
		'konektor' => "../../../",
        'laman' => "../../",
        'tahun' => date('Y'),
        'p' => 'lihatSekolah',
        'judul' => 'Daftar Sekolah Per Wilayah - Pendataan',
        'kode_wilayah' => $kode_wilayah,
        'id_level_wilayah' => $id_level_wilayah
    );

	return $app['twig']->render('index.twig',$array);
});

$app->get('beritaFeed', 'library\Rest::beritaFeed');

return $app;
?>