<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Debug;
use infopendataan\Model as M;

$loader = require_once __DIR__.'/../vendor/autoload.php';
$loader->add('infopendataan', __DIR__.'/../src/');
$loader->add('library', __DIR__.'/../src/');

// initialize the app
$app = new Silex\Application();
$app['debug'] = true;

//usable functions
require_once '../src/functions.php';
require_once '../src/function.php';
require_once '../src/tes.php';

//  Propel
$app['propel.config_file'] = __DIR__.'/config/build/conf/infopendataan-conf.php';
$app['propel.model_path'] = __DIR__.'/../src/';
$app->register(new Propel\Silex\PropelServiceProvider());

//register TWIG
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../web',
));

// Register Session
$app->register(new Silex\Provider\SessionServiceProvider());

// Register for auth too
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// use FormServiceProvider;
// $app->register(new Silex\Provider\TranslationServiceProvider());
// $app->register(new Silex\Provider\FormServiceProvider());

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array (
        'driver'    => 'pdo_sqlsrv',
        'host'		=> '172.17.3.19',
        'dbname'    => 'Dapodikmen',
        'user'      => 'UserRpt',
        'password'  => 'Reportd1km3n'
    ),
));


// Set TIMEZONE
date_default_timezone_set('Asia/Jakarta');

return $app;

?>