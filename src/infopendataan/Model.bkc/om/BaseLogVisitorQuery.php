<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\LogVisitor;
use infopendataan\Model\LogVisitorPeer;
use infopendataan\Model\LogVisitorQuery;

/**
 * Base class that represents a query for the 'log_visitor' table.
 *
 *
 *
 * @method LogVisitorQuery orderByLogVisitorId($order = Criteria::ASC) Order by the LOG_VISITOR_ID column
 * @method LogVisitorQuery orderByIp($order = Criteria::ASC) Order by the IP column
 * @method LogVisitorQuery orderByBrowser($order = Criteria::ASC) Order by the BROWSER column
 * @method LogVisitorQuery orderByOs($order = Criteria::ASC) Order by the OS column
 * @method LogVisitorQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 *
 * @method LogVisitorQuery groupByLogVisitorId() Group by the LOG_VISITOR_ID column
 * @method LogVisitorQuery groupByIp() Group by the IP column
 * @method LogVisitorQuery groupByBrowser() Group by the BROWSER column
 * @method LogVisitorQuery groupByOs() Group by the OS column
 * @method LogVisitorQuery groupByTanggal() Group by the TANGGAL column
 *
 * @method LogVisitorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogVisitorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogVisitorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogVisitor findOne(PropelPDO $con = null) Return the first LogVisitor matching the query
 * @method LogVisitor findOneOrCreate(PropelPDO $con = null) Return the first LogVisitor matching the query, or a new LogVisitor object populated from the query conditions when no match is found
 *
 * @method LogVisitor findOneByIp(string $IP) Return the first LogVisitor filtered by the IP column
 * @method LogVisitor findOneByBrowser(string $BROWSER) Return the first LogVisitor filtered by the BROWSER column
 * @method LogVisitor findOneByOs(string $OS) Return the first LogVisitor filtered by the OS column
 * @method LogVisitor findOneByTanggal(string $TANGGAL) Return the first LogVisitor filtered by the TANGGAL column
 *
 * @method array findByLogVisitorId(int $LOG_VISITOR_ID) Return LogVisitor objects filtered by the LOG_VISITOR_ID column
 * @method array findByIp(string $IP) Return LogVisitor objects filtered by the IP column
 * @method array findByBrowser(string $BROWSER) Return LogVisitor objects filtered by the BROWSER column
 * @method array findByOs(string $OS) Return LogVisitor objects filtered by the OS column
 * @method array findByTanggal(string $TANGGAL) Return LogVisitor objects filtered by the TANGGAL column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseLogVisitorQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogVisitorQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\LogVisitor';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogVisitorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogVisitorQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogVisitorQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogVisitorQuery) {
            return $criteria;
        }
        $query = new LogVisitorQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogVisitor|LogVisitor[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogVisitorPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogVisitorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogVisitor A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLogVisitorId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogVisitor A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `LOG_VISITOR_ID`, `IP`, `BROWSER`, `OS`, `TANGGAL` FROM `log_visitor` WHERE `LOG_VISITOR_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogVisitor();
            $obj->hydrate($row);
            LogVisitorPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogVisitor|LogVisitor[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogVisitor[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogVisitorPeer::LOG_VISITOR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogVisitorPeer::LOG_VISITOR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LOG_VISITOR_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByLogVisitorId(1234); // WHERE LOG_VISITOR_ID = 1234
     * $query->filterByLogVisitorId(array(12, 34)); // WHERE LOG_VISITOR_ID IN (12, 34)
     * $query->filterByLogVisitorId(array('min' => 12)); // WHERE LOG_VISITOR_ID >= 12
     * $query->filterByLogVisitorId(array('max' => 12)); // WHERE LOG_VISITOR_ID <= 12
     * </code>
     *
     * @param     mixed $logVisitorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByLogVisitorId($logVisitorId = null, $comparison = null)
    {
        if (is_array($logVisitorId)) {
            $useMinMax = false;
            if (isset($logVisitorId['min'])) {
                $this->addUsingAlias(LogVisitorPeer::LOG_VISITOR_ID, $logVisitorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logVisitorId['max'])) {
                $this->addUsingAlias(LogVisitorPeer::LOG_VISITOR_ID, $logVisitorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogVisitorPeer::LOG_VISITOR_ID, $logVisitorId, $comparison);
    }

    /**
     * Filter the query on the IP column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE IP = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE IP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogVisitorPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the BROWSER column
     *
     * Example usage:
     * <code>
     * $query->filterByBrowser('fooValue');   // WHERE BROWSER = 'fooValue'
     * $query->filterByBrowser('%fooValue%'); // WHERE BROWSER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $browser The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByBrowser($browser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($browser)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $browser)) {
                $browser = str_replace('*', '%', $browser);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogVisitorPeer::BROWSER, $browser, $comparison);
    }

    /**
     * Filter the query on the OS column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE OS = 'fooValue'
     * $query->filterByOs('%fooValue%'); // WHERE OS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $os)) {
                $os = str_replace('*', '%', $os);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogVisitorPeer::OS, $os, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(LogVisitorPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(LogVisitorPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogVisitorPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   LogVisitor $logVisitor Object to remove from the list of results
     *
     * @return LogVisitorQuery The current query, for fluid interface
     */
    public function prune($logVisitor = null)
    {
        if ($logVisitor) {
            $this->addUsingAlias(LogVisitorPeer::LOG_VISITOR_ID, $logVisitor->getLogVisitorId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
