<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\File;
use infopendataan\Model\FilePeer;
use infopendataan\Model\FileQuery;
use infopendataan\Model\JenisFile;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'file' table.
 *
 *
 *
 * @method FileQuery orderByFileId($order = Criteria::ASC) Order by the FILE_ID column
 * @method FileQuery orderByJenisFileId($order = Criteria::ASC) Order by the JENIS_FILE_ID column
 * @method FileQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method FileQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 * @method FileQuery orderByFile($order = Criteria::ASC) Order by the FILE column
 * @method FileQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method FileQuery orderByVersi($order = Criteria::ASC) Order by the VERSI column
 *
 * @method FileQuery groupByFileId() Group by the FILE_ID column
 * @method FileQuery groupByJenisFileId() Group by the JENIS_FILE_ID column
 * @method FileQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method FileQuery groupByNama() Group by the NAMA column
 * @method FileQuery groupByFile() Group by the FILE column
 * @method FileQuery groupByTanggal() Group by the TANGGAL column
 * @method FileQuery groupByVersi() Group by the VERSI column
 *
 * @method FileQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FileQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FileQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FileQuery leftJoinJenisFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisFile relation
 * @method FileQuery rightJoinJenisFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisFile relation
 * @method FileQuery innerJoinJenisFile($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisFile relation
 *
 * @method FileQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method FileQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method FileQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method File findOne(PropelPDO $con = null) Return the first File matching the query
 * @method File findOneOrCreate(PropelPDO $con = null) Return the first File matching the query, or a new File object populated from the query conditions when no match is found
 *
 * @method File findOneByJenisFileId(int $JENIS_FILE_ID) Return the first File filtered by the JENIS_FILE_ID column
 * @method File findOneByPenggunaId(int $PENGGUNA_ID) Return the first File filtered by the PENGGUNA_ID column
 * @method File findOneByNama(string $NAMA) Return the first File filtered by the NAMA column
 * @method File findOneByFile(string $FILE) Return the first File filtered by the FILE column
 * @method File findOneByTanggal(string $TANGGAL) Return the first File filtered by the TANGGAL column
 * @method File findOneByVersi(string $VERSI) Return the first File filtered by the VERSI column
 *
 * @method array findByFileId(int $FILE_ID) Return File objects filtered by the FILE_ID column
 * @method array findByJenisFileId(int $JENIS_FILE_ID) Return File objects filtered by the JENIS_FILE_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return File objects filtered by the PENGGUNA_ID column
 * @method array findByNama(string $NAMA) Return File objects filtered by the NAMA column
 * @method array findByFile(string $FILE) Return File objects filtered by the FILE column
 * @method array findByTanggal(string $TANGGAL) Return File objects filtered by the TANGGAL column
 * @method array findByVersi(string $VERSI) Return File objects filtered by the VERSI column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseFileQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFileQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\File';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FileQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FileQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FileQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FileQuery) {
            return $criteria;
        }
        $query = new FileQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   File|File[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FilePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 File A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByFileId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 File A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `FILE_ID`, `JENIS_FILE_ID`, `PENGGUNA_ID`, `NAMA`, `FILE`, `TANGGAL`, `VERSI` FROM `file` WHERE `FILE_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new File();
            $obj->hydrate($row);
            FilePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return File|File[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|File[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FilePeer::FILE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FilePeer::FILE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the FILE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByFileId(1234); // WHERE FILE_ID = 1234
     * $query->filterByFileId(array(12, 34)); // WHERE FILE_ID IN (12, 34)
     * $query->filterByFileId(array('min' => 12)); // WHERE FILE_ID >= 12
     * $query->filterByFileId(array('max' => 12)); // WHERE FILE_ID <= 12
     * </code>
     *
     * @param     mixed $fileId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByFileId($fileId = null, $comparison = null)
    {
        if (is_array($fileId)) {
            $useMinMax = false;
            if (isset($fileId['min'])) {
                $this->addUsingAlias(FilePeer::FILE_ID, $fileId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fileId['max'])) {
                $this->addUsingAlias(FilePeer::FILE_ID, $fileId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilePeer::FILE_ID, $fileId, $comparison);
    }

    /**
     * Filter the query on the JENIS_FILE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisFileId(1234); // WHERE JENIS_FILE_ID = 1234
     * $query->filterByJenisFileId(array(12, 34)); // WHERE JENIS_FILE_ID IN (12, 34)
     * $query->filterByJenisFileId(array('min' => 12)); // WHERE JENIS_FILE_ID >= 12
     * $query->filterByJenisFileId(array('max' => 12)); // WHERE JENIS_FILE_ID <= 12
     * </code>
     *
     * @see       filterByJenisFile()
     *
     * @param     mixed $jenisFileId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByJenisFileId($jenisFileId = null, $comparison = null)
    {
        if (is_array($jenisFileId)) {
            $useMinMax = false;
            if (isset($jenisFileId['min'])) {
                $this->addUsingAlias(FilePeer::JENIS_FILE_ID, $jenisFileId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisFileId['max'])) {
                $this->addUsingAlias(FilePeer::JENIS_FILE_ID, $jenisFileId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilePeer::JENIS_FILE_ID, $jenisFileId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(FilePeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(FilePeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilePeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FilePeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the FILE column
     *
     * Example usage:
     * <code>
     * $query->filterByFile('fooValue');   // WHERE FILE = 'fooValue'
     * $query->filterByFile('%fooValue%'); // WHERE FILE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $file The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByFile($file = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($file)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $file)) {
                $file = str_replace('*', '%', $file);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FilePeer::FILE, $file, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(FilePeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(FilePeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilePeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the VERSI column
     *
     * Example usage:
     * <code>
     * $query->filterByVersi('fooValue');   // WHERE VERSI = 'fooValue'
     * $query->filterByVersi('%fooValue%'); // WHERE VERSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function filterByVersi($versi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $versi)) {
                $versi = str_replace('*', '%', $versi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FilePeer::VERSI, $versi, $comparison);
    }

    /**
     * Filter the query by a related JenisFile object
     *
     * @param   JenisFile|PropelObjectCollection $jenisFile The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisFile($jenisFile, $comparison = null)
    {
        if ($jenisFile instanceof JenisFile) {
            return $this
                ->addUsingAlias(FilePeer::JENIS_FILE_ID, $jenisFile->getJenisFileId(), $comparison);
        } elseif ($jenisFile instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FilePeer::JENIS_FILE_ID, $jenisFile->toKeyValue('PrimaryKey', 'JenisFileId'), $comparison);
        } else {
            throw new PropelException('filterByJenisFile() only accepts arguments of type JenisFile or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisFile relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function joinJenisFile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisFile');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisFile');
        }

        return $this;
    }

    /**
     * Use the JenisFile relation JenisFile object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\JenisFileQuery A secondary query class using the current class as primary query
     */
    public function useJenisFileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisFile', '\infopendataan\Model\JenisFileQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(FilePeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FilePeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   File $file Object to remove from the list of results
     *
     * @return FileQuery The current query, for fluid interface
     */
    public function prune($file = null)
    {
        if ($file) {
            $this->addUsingAlias(FilePeer::FILE_ID, $file->getFileId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
