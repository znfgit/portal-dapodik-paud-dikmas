<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\LogDownloadInstaller;
use infopendataan\Model\LogDownloadInstallerPeer;
use infopendataan\Model\LogDownloadInstallerQuery;

/**
 * Base class that represents a query for the 'log_download_installer' table.
 *
 *
 *
 * @method LogDownloadInstallerQuery orderByLogDownloadInstallerId($order = Criteria::ASC) Order by the LOG_DOWNLOAD_INSTALLER_ID column
 * @method LogDownloadInstallerQuery orderByInstallerId($order = Criteria::ASC) Order by the INSTALLER_ID column
 * @method LogDownloadInstallerQuery orderByIp($order = Criteria::ASC) Order by the IP column
 * @method LogDownloadInstallerQuery orderByBrowser($order = Criteria::ASC) Order by the BROWSER column
 * @method LogDownloadInstallerQuery orderByOs($order = Criteria::ASC) Order by the OS column
 * @method LogDownloadInstallerQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 *
 * @method LogDownloadInstallerQuery groupByLogDownloadInstallerId() Group by the LOG_DOWNLOAD_INSTALLER_ID column
 * @method LogDownloadInstallerQuery groupByInstallerId() Group by the INSTALLER_ID column
 * @method LogDownloadInstallerQuery groupByIp() Group by the IP column
 * @method LogDownloadInstallerQuery groupByBrowser() Group by the BROWSER column
 * @method LogDownloadInstallerQuery groupByOs() Group by the OS column
 * @method LogDownloadInstallerQuery groupByTanggal() Group by the TANGGAL column
 *
 * @method LogDownloadInstallerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogDownloadInstallerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogDownloadInstallerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogDownloadInstaller findOne(PropelPDO $con = null) Return the first LogDownloadInstaller matching the query
 * @method LogDownloadInstaller findOneOrCreate(PropelPDO $con = null) Return the first LogDownloadInstaller matching the query, or a new LogDownloadInstaller object populated from the query conditions when no match is found
 *
 * @method LogDownloadInstaller findOneByInstallerId(int $INSTALLER_ID) Return the first LogDownloadInstaller filtered by the INSTALLER_ID column
 * @method LogDownloadInstaller findOneByIp(string $IP) Return the first LogDownloadInstaller filtered by the IP column
 * @method LogDownloadInstaller findOneByBrowser(string $BROWSER) Return the first LogDownloadInstaller filtered by the BROWSER column
 * @method LogDownloadInstaller findOneByOs(string $OS) Return the first LogDownloadInstaller filtered by the OS column
 * @method LogDownloadInstaller findOneByTanggal(string $TANGGAL) Return the first LogDownloadInstaller filtered by the TANGGAL column
 *
 * @method array findByLogDownloadInstallerId(int $LOG_DOWNLOAD_INSTALLER_ID) Return LogDownloadInstaller objects filtered by the LOG_DOWNLOAD_INSTALLER_ID column
 * @method array findByInstallerId(int $INSTALLER_ID) Return LogDownloadInstaller objects filtered by the INSTALLER_ID column
 * @method array findByIp(string $IP) Return LogDownloadInstaller objects filtered by the IP column
 * @method array findByBrowser(string $BROWSER) Return LogDownloadInstaller objects filtered by the BROWSER column
 * @method array findByOs(string $OS) Return LogDownloadInstaller objects filtered by the OS column
 * @method array findByTanggal(string $TANGGAL) Return LogDownloadInstaller objects filtered by the TANGGAL column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseLogDownloadInstallerQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogDownloadInstallerQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\LogDownloadInstaller';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogDownloadInstallerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogDownloadInstallerQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogDownloadInstallerQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogDownloadInstallerQuery) {
            return $criteria;
        }
        $query = new LogDownloadInstallerQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogDownloadInstaller|LogDownloadInstaller[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogDownloadInstallerPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogDownloadInstallerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogDownloadInstaller A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLogDownloadInstallerId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogDownloadInstaller A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `LOG_DOWNLOAD_INSTALLER_ID`, `INSTALLER_ID`, `IP`, `BROWSER`, `OS`, `TANGGAL` FROM `log_download_installer` WHERE `LOG_DOWNLOAD_INSTALLER_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogDownloadInstaller();
            $obj->hydrate($row);
            LogDownloadInstallerPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogDownloadInstaller|LogDownloadInstaller[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogDownloadInstaller[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogDownloadInstallerPeer::LOG_DOWNLOAD_INSTALLER_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogDownloadInstallerPeer::LOG_DOWNLOAD_INSTALLER_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LOG_DOWNLOAD_INSTALLER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByLogDownloadInstallerId(1234); // WHERE LOG_DOWNLOAD_INSTALLER_ID = 1234
     * $query->filterByLogDownloadInstallerId(array(12, 34)); // WHERE LOG_DOWNLOAD_INSTALLER_ID IN (12, 34)
     * $query->filterByLogDownloadInstallerId(array('min' => 12)); // WHERE LOG_DOWNLOAD_INSTALLER_ID >= 12
     * $query->filterByLogDownloadInstallerId(array('max' => 12)); // WHERE LOG_DOWNLOAD_INSTALLER_ID <= 12
     * </code>
     *
     * @param     mixed $logDownloadInstallerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByLogDownloadInstallerId($logDownloadInstallerId = null, $comparison = null)
    {
        if (is_array($logDownloadInstallerId)) {
            $useMinMax = false;
            if (isset($logDownloadInstallerId['min'])) {
                $this->addUsingAlias(LogDownloadInstallerPeer::LOG_DOWNLOAD_INSTALLER_ID, $logDownloadInstallerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logDownloadInstallerId['max'])) {
                $this->addUsingAlias(LogDownloadInstallerPeer::LOG_DOWNLOAD_INSTALLER_ID, $logDownloadInstallerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogDownloadInstallerPeer::LOG_DOWNLOAD_INSTALLER_ID, $logDownloadInstallerId, $comparison);
    }

    /**
     * Filter the query on the INSTALLER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByInstallerId(1234); // WHERE INSTALLER_ID = 1234
     * $query->filterByInstallerId(array(12, 34)); // WHERE INSTALLER_ID IN (12, 34)
     * $query->filterByInstallerId(array('min' => 12)); // WHERE INSTALLER_ID >= 12
     * $query->filterByInstallerId(array('max' => 12)); // WHERE INSTALLER_ID <= 12
     * </code>
     *
     * @param     mixed $installerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByInstallerId($installerId = null, $comparison = null)
    {
        if (is_array($installerId)) {
            $useMinMax = false;
            if (isset($installerId['min'])) {
                $this->addUsingAlias(LogDownloadInstallerPeer::INSTALLER_ID, $installerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($installerId['max'])) {
                $this->addUsingAlias(LogDownloadInstallerPeer::INSTALLER_ID, $installerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogDownloadInstallerPeer::INSTALLER_ID, $installerId, $comparison);
    }

    /**
     * Filter the query on the IP column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE IP = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE IP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogDownloadInstallerPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the BROWSER column
     *
     * Example usage:
     * <code>
     * $query->filterByBrowser('fooValue');   // WHERE BROWSER = 'fooValue'
     * $query->filterByBrowser('%fooValue%'); // WHERE BROWSER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $browser The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByBrowser($browser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($browser)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $browser)) {
                $browser = str_replace('*', '%', $browser);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogDownloadInstallerPeer::BROWSER, $browser, $comparison);
    }

    /**
     * Filter the query on the OS column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE OS = 'fooValue'
     * $query->filterByOs('%fooValue%'); // WHERE OS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $os)) {
                $os = str_replace('*', '%', $os);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogDownloadInstallerPeer::OS, $os, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(LogDownloadInstallerPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(LogDownloadInstallerPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogDownloadInstallerPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   LogDownloadInstaller $logDownloadInstaller Object to remove from the list of results
     *
     * @return LogDownloadInstallerQuery The current query, for fluid interface
     */
    public function prune($logDownloadInstaller = null)
    {
        if ($logDownloadInstaller) {
            $this->addUsingAlias(LogDownloadInstallerPeer::LOG_DOWNLOAD_INSTALLER_ID, $logDownloadInstaller->getLogDownloadInstallerId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
