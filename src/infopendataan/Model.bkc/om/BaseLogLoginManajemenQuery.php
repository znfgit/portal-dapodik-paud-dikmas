<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\LogLoginManajemen;
use infopendataan\Model\LogLoginManajemenPeer;
use infopendataan\Model\LogLoginManajemenQuery;

/**
 * Base class that represents a query for the 'log_login_manajemen' table.
 *
 *
 *
 * @method LogLoginManajemenQuery orderByLogLoginManajemenId($order = Criteria::ASC) Order by the LOG_LOGIN_MANAJEMEN_ID column
 * @method LogLoginManajemenQuery orderByIp($order = Criteria::ASC) Order by the IP column
 * @method LogLoginManajemenQuery orderByBrowser($order = Criteria::ASC) Order by the BROWSER column
 * @method LogLoginManajemenQuery orderByOs($order = Criteria::ASC) Order by the OS column
 * @method LogLoginManajemenQuery orderByKodeWilayah($order = Criteria::ASC) Order by the KODE_WILAYAH column
 * @method LogLoginManajemenQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method LogLoginManajemenQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method LogLoginManajemenQuery orderByIdLevelWilayah($order = Criteria::ASC) Order by the ID_LEVEL_WILAYAH column
 *
 * @method LogLoginManajemenQuery groupByLogLoginManajemenId() Group by the LOG_LOGIN_MANAJEMEN_ID column
 * @method LogLoginManajemenQuery groupByIp() Group by the IP column
 * @method LogLoginManajemenQuery groupByBrowser() Group by the BROWSER column
 * @method LogLoginManajemenQuery groupByOs() Group by the OS column
 * @method LogLoginManajemenQuery groupByKodeWilayah() Group by the KODE_WILAYAH column
 * @method LogLoginManajemenQuery groupByTanggal() Group by the TANGGAL column
 * @method LogLoginManajemenQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method LogLoginManajemenQuery groupByIdLevelWilayah() Group by the ID_LEVEL_WILAYAH column
 *
 * @method LogLoginManajemenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogLoginManajemenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogLoginManajemenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogLoginManajemen findOne(PropelPDO $con = null) Return the first LogLoginManajemen matching the query
 * @method LogLoginManajemen findOneOrCreate(PropelPDO $con = null) Return the first LogLoginManajemen matching the query, or a new LogLoginManajemen object populated from the query conditions when no match is found
 *
 * @method LogLoginManajemen findOneByIp(string $IP) Return the first LogLoginManajemen filtered by the IP column
 * @method LogLoginManajemen findOneByBrowser(string $BROWSER) Return the first LogLoginManajemen filtered by the BROWSER column
 * @method LogLoginManajemen findOneByOs(string $OS) Return the first LogLoginManajemen filtered by the OS column
 * @method LogLoginManajemen findOneByKodeWilayah(string $KODE_WILAYAH) Return the first LogLoginManajemen filtered by the KODE_WILAYAH column
 * @method LogLoginManajemen findOneByTanggal(string $TANGGAL) Return the first LogLoginManajemen filtered by the TANGGAL column
 * @method LogLoginManajemen findOneByPenggunaId(string $PENGGUNA_ID) Return the first LogLoginManajemen filtered by the PENGGUNA_ID column
 * @method LogLoginManajemen findOneByIdLevelWilayah(int $ID_LEVEL_WILAYAH) Return the first LogLoginManajemen filtered by the ID_LEVEL_WILAYAH column
 *
 * @method array findByLogLoginManajemenId(int $LOG_LOGIN_MANAJEMEN_ID) Return LogLoginManajemen objects filtered by the LOG_LOGIN_MANAJEMEN_ID column
 * @method array findByIp(string $IP) Return LogLoginManajemen objects filtered by the IP column
 * @method array findByBrowser(string $BROWSER) Return LogLoginManajemen objects filtered by the BROWSER column
 * @method array findByOs(string $OS) Return LogLoginManajemen objects filtered by the OS column
 * @method array findByKodeWilayah(string $KODE_WILAYAH) Return LogLoginManajemen objects filtered by the KODE_WILAYAH column
 * @method array findByTanggal(string $TANGGAL) Return LogLoginManajemen objects filtered by the TANGGAL column
 * @method array findByPenggunaId(string $PENGGUNA_ID) Return LogLoginManajemen objects filtered by the PENGGUNA_ID column
 * @method array findByIdLevelWilayah(int $ID_LEVEL_WILAYAH) Return LogLoginManajemen objects filtered by the ID_LEVEL_WILAYAH column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseLogLoginManajemenQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogLoginManajemenQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\LogLoginManajemen';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogLoginManajemenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogLoginManajemenQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogLoginManajemenQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogLoginManajemenQuery) {
            return $criteria;
        }
        $query = new LogLoginManajemenQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogLoginManajemen|LogLoginManajemen[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogLoginManajemenPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogLoginManajemenPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogLoginManajemen A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLogLoginManajemenId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogLoginManajemen A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `LOG_LOGIN_MANAJEMEN_ID`, `IP`, `BROWSER`, `OS`, `KODE_WILAYAH`, `TANGGAL`, `PENGGUNA_ID`, `ID_LEVEL_WILAYAH` FROM `log_login_manajemen` WHERE `LOG_LOGIN_MANAJEMEN_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogLoginManajemen();
            $obj->hydrate($row);
            LogLoginManajemenPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogLoginManajemen|LogLoginManajemen[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogLoginManajemen[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogLoginManajemenPeer::LOG_LOGIN_MANAJEMEN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogLoginManajemenPeer::LOG_LOGIN_MANAJEMEN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LOG_LOGIN_MANAJEMEN_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByLogLoginManajemenId(1234); // WHERE LOG_LOGIN_MANAJEMEN_ID = 1234
     * $query->filterByLogLoginManajemenId(array(12, 34)); // WHERE LOG_LOGIN_MANAJEMEN_ID IN (12, 34)
     * $query->filterByLogLoginManajemenId(array('min' => 12)); // WHERE LOG_LOGIN_MANAJEMEN_ID >= 12
     * $query->filterByLogLoginManajemenId(array('max' => 12)); // WHERE LOG_LOGIN_MANAJEMEN_ID <= 12
     * </code>
     *
     * @param     mixed $logLoginManajemenId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByLogLoginManajemenId($logLoginManajemenId = null, $comparison = null)
    {
        if (is_array($logLoginManajemenId)) {
            $useMinMax = false;
            if (isset($logLoginManajemenId['min'])) {
                $this->addUsingAlias(LogLoginManajemenPeer::LOG_LOGIN_MANAJEMEN_ID, $logLoginManajemenId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logLoginManajemenId['max'])) {
                $this->addUsingAlias(LogLoginManajemenPeer::LOG_LOGIN_MANAJEMEN_ID, $logLoginManajemenId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::LOG_LOGIN_MANAJEMEN_ID, $logLoginManajemenId, $comparison);
    }

    /**
     * Filter the query on the IP column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE IP = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE IP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the BROWSER column
     *
     * Example usage:
     * <code>
     * $query->filterByBrowser('fooValue');   // WHERE BROWSER = 'fooValue'
     * $query->filterByBrowser('%fooValue%'); // WHERE BROWSER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $browser The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByBrowser($browser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($browser)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $browser)) {
                $browser = str_replace('*', '%', $browser);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::BROWSER, $browser, $comparison);
    }

    /**
     * Filter the query on the OS column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE OS = 'fooValue'
     * $query->filterByOs('%fooValue%'); // WHERE OS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $os)) {
                $os = str_replace('*', '%', $os);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::OS, $os, $comparison);
    }

    /**
     * Filter the query on the KODE_WILAYAH column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE KODE_WILAYAH = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE KODE_WILAYAH LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(LogLoginManajemenPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(LogLoginManajemenPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE PENGGUNA_ID = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE PENGGUNA_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the ID_LEVEL_WILAYAH column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayah(1234); // WHERE ID_LEVEL_WILAYAH = 1234
     * $query->filterByIdLevelWilayah(array(12, 34)); // WHERE ID_LEVEL_WILAYAH IN (12, 34)
     * $query->filterByIdLevelWilayah(array('min' => 12)); // WHERE ID_LEVEL_WILAYAH >= 12
     * $query->filterByIdLevelWilayah(array('max' => 12)); // WHERE ID_LEVEL_WILAYAH <= 12
     * </code>
     *
     * @param     mixed $idLevelWilayah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayah($idLevelWilayah = null, $comparison = null)
    {
        if (is_array($idLevelWilayah)) {
            $useMinMax = false;
            if (isset($idLevelWilayah['min'])) {
                $this->addUsingAlias(LogLoginManajemenPeer::ID_LEVEL_WILAYAH, $idLevelWilayah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLevelWilayah['max'])) {
                $this->addUsingAlias(LogLoginManajemenPeer::ID_LEVEL_WILAYAH, $idLevelWilayah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogLoginManajemenPeer::ID_LEVEL_WILAYAH, $idLevelWilayah, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   LogLoginManajemen $logLoginManajemen Object to remove from the list of results
     *
     * @return LogLoginManajemenQuery The current query, for fluid interface
     */
    public function prune($logLoginManajemen = null)
    {
        if ($logLoginManajemen) {
            $this->addUsingAlias(LogLoginManajemenPeer::LOG_LOGIN_MANAJEMEN_ID, $logLoginManajemen->getLogLoginManajemenId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
