<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\LogDownloadPatch;
use infopendataan\Model\LogDownloadPatchPeer;
use infopendataan\Model\LogDownloadPatchQuery;
use infopendataan\Model\Patch;

/**
 * Base class that represents a query for the 'log_download_patch' table.
 *
 *
 *
 * @method LogDownloadPatchQuery orderByLogDownloadPatchId($order = Criteria::ASC) Order by the LOG_DOWNLOAD_PATCH_ID column
 * @method LogDownloadPatchQuery orderByPatchId($order = Criteria::ASC) Order by the PATCH_ID column
 * @method LogDownloadPatchQuery orderByIp($order = Criteria::ASC) Order by the IP column
 * @method LogDownloadPatchQuery orderByBrowser($order = Criteria::ASC) Order by the BROWSER column
 * @method LogDownloadPatchQuery orderByOs($order = Criteria::ASC) Order by the OS column
 * @method LogDownloadPatchQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 *
 * @method LogDownloadPatchQuery groupByLogDownloadPatchId() Group by the LOG_DOWNLOAD_PATCH_ID column
 * @method LogDownloadPatchQuery groupByPatchId() Group by the PATCH_ID column
 * @method LogDownloadPatchQuery groupByIp() Group by the IP column
 * @method LogDownloadPatchQuery groupByBrowser() Group by the BROWSER column
 * @method LogDownloadPatchQuery groupByOs() Group by the OS column
 * @method LogDownloadPatchQuery groupByTanggal() Group by the TANGGAL column
 *
 * @method LogDownloadPatchQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogDownloadPatchQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogDownloadPatchQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogDownloadPatchQuery leftJoinPatch($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patch relation
 * @method LogDownloadPatchQuery rightJoinPatch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patch relation
 * @method LogDownloadPatchQuery innerJoinPatch($relationAlias = null) Adds a INNER JOIN clause to the query using the Patch relation
 *
 * @method LogDownloadPatch findOne(PropelPDO $con = null) Return the first LogDownloadPatch matching the query
 * @method LogDownloadPatch findOneOrCreate(PropelPDO $con = null) Return the first LogDownloadPatch matching the query, or a new LogDownloadPatch object populated from the query conditions when no match is found
 *
 * @method LogDownloadPatch findOneByPatchId(int $PATCH_ID) Return the first LogDownloadPatch filtered by the PATCH_ID column
 * @method LogDownloadPatch findOneByIp(string $IP) Return the first LogDownloadPatch filtered by the IP column
 * @method LogDownloadPatch findOneByBrowser(string $BROWSER) Return the first LogDownloadPatch filtered by the BROWSER column
 * @method LogDownloadPatch findOneByOs(string $OS) Return the first LogDownloadPatch filtered by the OS column
 * @method LogDownloadPatch findOneByTanggal(string $TANGGAL) Return the first LogDownloadPatch filtered by the TANGGAL column
 *
 * @method array findByLogDownloadPatchId(int $LOG_DOWNLOAD_PATCH_ID) Return LogDownloadPatch objects filtered by the LOG_DOWNLOAD_PATCH_ID column
 * @method array findByPatchId(int $PATCH_ID) Return LogDownloadPatch objects filtered by the PATCH_ID column
 * @method array findByIp(string $IP) Return LogDownloadPatch objects filtered by the IP column
 * @method array findByBrowser(string $BROWSER) Return LogDownloadPatch objects filtered by the BROWSER column
 * @method array findByOs(string $OS) Return LogDownloadPatch objects filtered by the OS column
 * @method array findByTanggal(string $TANGGAL) Return LogDownloadPatch objects filtered by the TANGGAL column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseLogDownloadPatchQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogDownloadPatchQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\LogDownloadPatch';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogDownloadPatchQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogDownloadPatchQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogDownloadPatchQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogDownloadPatchQuery) {
            return $criteria;
        }
        $query = new LogDownloadPatchQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogDownloadPatch|LogDownloadPatch[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogDownloadPatchPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogDownloadPatchPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogDownloadPatch A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLogDownloadPatchId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogDownloadPatch A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `LOG_DOWNLOAD_PATCH_ID`, `PATCH_ID`, `IP`, `BROWSER`, `OS`, `TANGGAL` FROM `log_download_patch` WHERE `LOG_DOWNLOAD_PATCH_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogDownloadPatch();
            $obj->hydrate($row);
            LogDownloadPatchPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogDownloadPatch|LogDownloadPatch[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogDownloadPatch[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogDownloadPatchPeer::LOG_DOWNLOAD_PATCH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogDownloadPatchPeer::LOG_DOWNLOAD_PATCH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LOG_DOWNLOAD_PATCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByLogDownloadPatchId(1234); // WHERE LOG_DOWNLOAD_PATCH_ID = 1234
     * $query->filterByLogDownloadPatchId(array(12, 34)); // WHERE LOG_DOWNLOAD_PATCH_ID IN (12, 34)
     * $query->filterByLogDownloadPatchId(array('min' => 12)); // WHERE LOG_DOWNLOAD_PATCH_ID >= 12
     * $query->filterByLogDownloadPatchId(array('max' => 12)); // WHERE LOG_DOWNLOAD_PATCH_ID <= 12
     * </code>
     *
     * @param     mixed $logDownloadPatchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByLogDownloadPatchId($logDownloadPatchId = null, $comparison = null)
    {
        if (is_array($logDownloadPatchId)) {
            $useMinMax = false;
            if (isset($logDownloadPatchId['min'])) {
                $this->addUsingAlias(LogDownloadPatchPeer::LOG_DOWNLOAD_PATCH_ID, $logDownloadPatchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logDownloadPatchId['max'])) {
                $this->addUsingAlias(LogDownloadPatchPeer::LOG_DOWNLOAD_PATCH_ID, $logDownloadPatchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogDownloadPatchPeer::LOG_DOWNLOAD_PATCH_ID, $logDownloadPatchId, $comparison);
    }

    /**
     * Filter the query on the PATCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPatchId(1234); // WHERE PATCH_ID = 1234
     * $query->filterByPatchId(array(12, 34)); // WHERE PATCH_ID IN (12, 34)
     * $query->filterByPatchId(array('min' => 12)); // WHERE PATCH_ID >= 12
     * $query->filterByPatchId(array('max' => 12)); // WHERE PATCH_ID <= 12
     * </code>
     *
     * @see       filterByPatch()
     *
     * @param     mixed $patchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByPatchId($patchId = null, $comparison = null)
    {
        if (is_array($patchId)) {
            $useMinMax = false;
            if (isset($patchId['min'])) {
                $this->addUsingAlias(LogDownloadPatchPeer::PATCH_ID, $patchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($patchId['max'])) {
                $this->addUsingAlias(LogDownloadPatchPeer::PATCH_ID, $patchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogDownloadPatchPeer::PATCH_ID, $patchId, $comparison);
    }

    /**
     * Filter the query on the IP column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE IP = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE IP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogDownloadPatchPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the BROWSER column
     *
     * Example usage:
     * <code>
     * $query->filterByBrowser('fooValue');   // WHERE BROWSER = 'fooValue'
     * $query->filterByBrowser('%fooValue%'); // WHERE BROWSER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $browser The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByBrowser($browser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($browser)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $browser)) {
                $browser = str_replace('*', '%', $browser);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogDownloadPatchPeer::BROWSER, $browser, $comparison);
    }

    /**
     * Filter the query on the OS column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE OS = 'fooValue'
     * $query->filterByOs('%fooValue%'); // WHERE OS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $os)) {
                $os = str_replace('*', '%', $os);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogDownloadPatchPeer::OS, $os, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(LogDownloadPatchPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(LogDownloadPatchPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogDownloadPatchPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query by a related Patch object
     *
     * @param   Patch|PropelObjectCollection $patch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LogDownloadPatchQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPatch($patch, $comparison = null)
    {
        if ($patch instanceof Patch) {
            return $this
                ->addUsingAlias(LogDownloadPatchPeer::PATCH_ID, $patch->getPatchId(), $comparison);
        } elseif ($patch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LogDownloadPatchPeer::PATCH_ID, $patch->toKeyValue('PrimaryKey', 'PatchId'), $comparison);
        } else {
            throw new PropelException('filterByPatch() only accepts arguments of type Patch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function joinPatch($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patch');
        }

        return $this;
    }

    /**
     * Use the Patch relation Patch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PatchQuery A secondary query class using the current class as primary query
     */
    public function usePatchQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPatch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patch', '\infopendataan\Model\PatchQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   LogDownloadPatch $logDownloadPatch Object to remove from the list of results
     *
     * @return LogDownloadPatchQuery The current query, for fluid interface
     */
    public function prune($logDownloadPatch = null)
    {
        if ($logDownloadPatch) {
            $this->addUsingAlias(LogDownloadPatchPeer::LOG_DOWNLOAD_PATCH_ID, $logDownloadPatch->getLogDownloadPatchId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
