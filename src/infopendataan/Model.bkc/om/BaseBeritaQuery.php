<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Berita;
use infopendataan\Model\BeritaPeer;
use infopendataan\Model\BeritaQuery;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'berita' table.
 *
 *
 *
 * @method BeritaQuery orderByBeritaId($order = Criteria::ASC) Order by the BERITA_ID column
 * @method BeritaQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method BeritaQuery orderByJudul($order = Criteria::ASC) Order by the JUDUL column
 * @method BeritaQuery orderByIsi($order = Criteria::ASC) Order by the ISI column
 * @method BeritaQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method BeritaQuery orderByGambar($order = Criteria::ASC) Order by the GAMBAR column
 *
 * @method BeritaQuery groupByBeritaId() Group by the BERITA_ID column
 * @method BeritaQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method BeritaQuery groupByJudul() Group by the JUDUL column
 * @method BeritaQuery groupByIsi() Group by the ISI column
 * @method BeritaQuery groupByTanggal() Group by the TANGGAL column
 * @method BeritaQuery groupByGambar() Group by the GAMBAR column
 *
 * @method BeritaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BeritaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BeritaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BeritaQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method BeritaQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method BeritaQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method Berita findOne(PropelPDO $con = null) Return the first Berita matching the query
 * @method Berita findOneOrCreate(PropelPDO $con = null) Return the first Berita matching the query, or a new Berita object populated from the query conditions when no match is found
 *
 * @method Berita findOneByPenggunaId(int $PENGGUNA_ID) Return the first Berita filtered by the PENGGUNA_ID column
 * @method Berita findOneByJudul(string $JUDUL) Return the first Berita filtered by the JUDUL column
 * @method Berita findOneByIsi(string $ISI) Return the first Berita filtered by the ISI column
 * @method Berita findOneByTanggal(string $TANGGAL) Return the first Berita filtered by the TANGGAL column
 * @method Berita findOneByGambar(string $GAMBAR) Return the first Berita filtered by the GAMBAR column
 *
 * @method array findByBeritaId(int $BERITA_ID) Return Berita objects filtered by the BERITA_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return Berita objects filtered by the PENGGUNA_ID column
 * @method array findByJudul(string $JUDUL) Return Berita objects filtered by the JUDUL column
 * @method array findByIsi(string $ISI) Return Berita objects filtered by the ISI column
 * @method array findByTanggal(string $TANGGAL) Return Berita objects filtered by the TANGGAL column
 * @method array findByGambar(string $GAMBAR) Return Berita objects filtered by the GAMBAR column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseBeritaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBeritaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\Berita';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BeritaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BeritaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BeritaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BeritaQuery) {
            return $criteria;
        }
        $query = new BeritaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Berita|Berita[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BeritaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BeritaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Berita A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByBeritaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Berita A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `BERITA_ID`, `PENGGUNA_ID`, `JUDUL`, `ISI`, `TANGGAL`, `GAMBAR` FROM `berita` WHERE `BERITA_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Berita();
            $obj->hydrate($row);
            BeritaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Berita|Berita[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Berita[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BeritaPeer::BERITA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BeritaPeer::BERITA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the BERITA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBeritaId(1234); // WHERE BERITA_ID = 1234
     * $query->filterByBeritaId(array(12, 34)); // WHERE BERITA_ID IN (12, 34)
     * $query->filterByBeritaId(array('min' => 12)); // WHERE BERITA_ID >= 12
     * $query->filterByBeritaId(array('max' => 12)); // WHERE BERITA_ID <= 12
     * </code>
     *
     * @param     mixed $beritaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByBeritaId($beritaId = null, $comparison = null)
    {
        if (is_array($beritaId)) {
            $useMinMax = false;
            if (isset($beritaId['min'])) {
                $this->addUsingAlias(BeritaPeer::BERITA_ID, $beritaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($beritaId['max'])) {
                $this->addUsingAlias(BeritaPeer::BERITA_ID, $beritaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeritaPeer::BERITA_ID, $beritaId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(BeritaPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(BeritaPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeritaPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the JUDUL column
     *
     * Example usage:
     * <code>
     * $query->filterByJudul('fooValue');   // WHERE JUDUL = 'fooValue'
     * $query->filterByJudul('%fooValue%'); // WHERE JUDUL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $judul The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByJudul($judul = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($judul)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $judul)) {
                $judul = str_replace('*', '%', $judul);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BeritaPeer::JUDUL, $judul, $comparison);
    }

    /**
     * Filter the query on the ISI column
     *
     * Example usage:
     * <code>
     * $query->filterByIsi('fooValue');   // WHERE ISI = 'fooValue'
     * $query->filterByIsi('%fooValue%'); // WHERE ISI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $isi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByIsi($isi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($isi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $isi)) {
                $isi = str_replace('*', '%', $isi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BeritaPeer::ISI, $isi, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(BeritaPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(BeritaPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeritaPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the GAMBAR column
     *
     * Example usage:
     * <code>
     * $query->filterByGambar('fooValue');   // WHERE GAMBAR = 'fooValue'
     * $query->filterByGambar('%fooValue%'); // WHERE GAMBAR LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gambar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function filterByGambar($gambar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gambar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gambar)) {
                $gambar = str_replace('*', '%', $gambar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BeritaPeer::GAMBAR, $gambar, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BeritaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(BeritaPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeritaPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Berita $berita Object to remove from the list of results
     *
     * @return BeritaQuery The current query, for fluid interface
     */
    public function prune($berita = null)
    {
        if ($berita) {
            $this->addUsingAlias(BeritaPeer::BERITA_ID, $berita->getBeritaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
