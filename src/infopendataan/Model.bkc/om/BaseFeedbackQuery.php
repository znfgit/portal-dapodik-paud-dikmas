<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Feedback;
use infopendataan\Model\FeedbackPeer;
use infopendataan\Model\FeedbackQuery;
use infopendataan\Model\JenisFeedback;

/**
 * Base class that represents a query for the 'feedback' table.
 *
 *
 *
 * @method FeedbackQuery orderByFeedbackId($order = Criteria::ASC) Order by the FEEDBACK_ID column
 * @method FeedbackQuery orderByJenisFeedbackId($order = Criteria::ASC) Order by the JENIS_FEEDBACK_ID column
 * @method FeedbackQuery orderByJudul($order = Criteria::ASC) Order by the JUDUL column
 * @method FeedbackQuery orderByDeskripsi($order = Criteria::ASC) Order by the DESKRIPSI column
 * @method FeedbackQuery orderByGambar($order = Criteria::ASC) Order by the GAMBAR column
 * @method FeedbackQuery orderByEmailPengirim($order = Criteria::ASC) Order by the EMAIL_PENGIRIM column
 * @method FeedbackQuery orderByInstansi($order = Criteria::ASC) Order by the INSTANSI column
 * @method FeedbackQuery orderByNomorTelepon($order = Criteria::ASC) Order by the NOMOR_TELEPON column
 * @method FeedbackQuery orderByTanggalLapor($order = Criteria::ASC) Order by the TANGGAL_LAPOR column
 * @method FeedbackQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method FeedbackQuery orderByTanggalUpdate($order = Criteria::ASC) Order by the TANGGAL_UPDATE column
 *
 * @method FeedbackQuery groupByFeedbackId() Group by the FEEDBACK_ID column
 * @method FeedbackQuery groupByJenisFeedbackId() Group by the JENIS_FEEDBACK_ID column
 * @method FeedbackQuery groupByJudul() Group by the JUDUL column
 * @method FeedbackQuery groupByDeskripsi() Group by the DESKRIPSI column
 * @method FeedbackQuery groupByGambar() Group by the GAMBAR column
 * @method FeedbackQuery groupByEmailPengirim() Group by the EMAIL_PENGIRIM column
 * @method FeedbackQuery groupByInstansi() Group by the INSTANSI column
 * @method FeedbackQuery groupByNomorTelepon() Group by the NOMOR_TELEPON column
 * @method FeedbackQuery groupByTanggalLapor() Group by the TANGGAL_LAPOR column
 * @method FeedbackQuery groupByStatus() Group by the STATUS column
 * @method FeedbackQuery groupByTanggalUpdate() Group by the TANGGAL_UPDATE column
 *
 * @method FeedbackQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FeedbackQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FeedbackQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FeedbackQuery leftJoinJenisFeedback($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisFeedback relation
 * @method FeedbackQuery rightJoinJenisFeedback($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisFeedback relation
 * @method FeedbackQuery innerJoinJenisFeedback($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisFeedback relation
 *
 * @method Feedback findOne(PropelPDO $con = null) Return the first Feedback matching the query
 * @method Feedback findOneOrCreate(PropelPDO $con = null) Return the first Feedback matching the query, or a new Feedback object populated from the query conditions when no match is found
 *
 * @method Feedback findOneByJenisFeedbackId(int $JENIS_FEEDBACK_ID) Return the first Feedback filtered by the JENIS_FEEDBACK_ID column
 * @method Feedback findOneByJudul(string $JUDUL) Return the first Feedback filtered by the JUDUL column
 * @method Feedback findOneByDeskripsi(string $DESKRIPSI) Return the first Feedback filtered by the DESKRIPSI column
 * @method Feedback findOneByGambar(string $GAMBAR) Return the first Feedback filtered by the GAMBAR column
 * @method Feedback findOneByEmailPengirim(string $EMAIL_PENGIRIM) Return the first Feedback filtered by the EMAIL_PENGIRIM column
 * @method Feedback findOneByInstansi(string $INSTANSI) Return the first Feedback filtered by the INSTANSI column
 * @method Feedback findOneByNomorTelepon(string $NOMOR_TELEPON) Return the first Feedback filtered by the NOMOR_TELEPON column
 * @method Feedback findOneByTanggalLapor(string $TANGGAL_LAPOR) Return the first Feedback filtered by the TANGGAL_LAPOR column
 * @method Feedback findOneByStatus(int $STATUS) Return the first Feedback filtered by the STATUS column
 * @method Feedback findOneByTanggalUpdate(string $TANGGAL_UPDATE) Return the first Feedback filtered by the TANGGAL_UPDATE column
 *
 * @method array findByFeedbackId(int $FEEDBACK_ID) Return Feedback objects filtered by the FEEDBACK_ID column
 * @method array findByJenisFeedbackId(int $JENIS_FEEDBACK_ID) Return Feedback objects filtered by the JENIS_FEEDBACK_ID column
 * @method array findByJudul(string $JUDUL) Return Feedback objects filtered by the JUDUL column
 * @method array findByDeskripsi(string $DESKRIPSI) Return Feedback objects filtered by the DESKRIPSI column
 * @method array findByGambar(string $GAMBAR) Return Feedback objects filtered by the GAMBAR column
 * @method array findByEmailPengirim(string $EMAIL_PENGIRIM) Return Feedback objects filtered by the EMAIL_PENGIRIM column
 * @method array findByInstansi(string $INSTANSI) Return Feedback objects filtered by the INSTANSI column
 * @method array findByNomorTelepon(string $NOMOR_TELEPON) Return Feedback objects filtered by the NOMOR_TELEPON column
 * @method array findByTanggalLapor(string $TANGGAL_LAPOR) Return Feedback objects filtered by the TANGGAL_LAPOR column
 * @method array findByStatus(int $STATUS) Return Feedback objects filtered by the STATUS column
 * @method array findByTanggalUpdate(string $TANGGAL_UPDATE) Return Feedback objects filtered by the TANGGAL_UPDATE column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BaseFeedbackQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFeedbackQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\Feedback';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FeedbackQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FeedbackQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FeedbackQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FeedbackQuery) {
            return $criteria;
        }
        $query = new FeedbackQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Feedback|Feedback[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FeedbackPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FeedbackPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Feedback A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByFeedbackId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Feedback A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `FEEDBACK_ID`, `JENIS_FEEDBACK_ID`, `JUDUL`, `DESKRIPSI`, `GAMBAR`, `EMAIL_PENGIRIM`, `INSTANSI`, `NOMOR_TELEPON`, `TANGGAL_LAPOR`, `STATUS`, `TANGGAL_UPDATE` FROM `feedback` WHERE `FEEDBACK_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Feedback();
            $obj->hydrate($row);
            FeedbackPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Feedback|Feedback[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Feedback[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FeedbackPeer::FEEDBACK_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FeedbackPeer::FEEDBACK_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the FEEDBACK_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByFeedbackId(1234); // WHERE FEEDBACK_ID = 1234
     * $query->filterByFeedbackId(array(12, 34)); // WHERE FEEDBACK_ID IN (12, 34)
     * $query->filterByFeedbackId(array('min' => 12)); // WHERE FEEDBACK_ID >= 12
     * $query->filterByFeedbackId(array('max' => 12)); // WHERE FEEDBACK_ID <= 12
     * </code>
     *
     * @param     mixed $feedbackId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByFeedbackId($feedbackId = null, $comparison = null)
    {
        if (is_array($feedbackId)) {
            $useMinMax = false;
            if (isset($feedbackId['min'])) {
                $this->addUsingAlias(FeedbackPeer::FEEDBACK_ID, $feedbackId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($feedbackId['max'])) {
                $this->addUsingAlias(FeedbackPeer::FEEDBACK_ID, $feedbackId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::FEEDBACK_ID, $feedbackId, $comparison);
    }

    /**
     * Filter the query on the JENIS_FEEDBACK_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisFeedbackId(1234); // WHERE JENIS_FEEDBACK_ID = 1234
     * $query->filterByJenisFeedbackId(array(12, 34)); // WHERE JENIS_FEEDBACK_ID IN (12, 34)
     * $query->filterByJenisFeedbackId(array('min' => 12)); // WHERE JENIS_FEEDBACK_ID >= 12
     * $query->filterByJenisFeedbackId(array('max' => 12)); // WHERE JENIS_FEEDBACK_ID <= 12
     * </code>
     *
     * @see       filterByJenisFeedback()
     *
     * @param     mixed $jenisFeedbackId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByJenisFeedbackId($jenisFeedbackId = null, $comparison = null)
    {
        if (is_array($jenisFeedbackId)) {
            $useMinMax = false;
            if (isset($jenisFeedbackId['min'])) {
                $this->addUsingAlias(FeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedbackId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisFeedbackId['max'])) {
                $this->addUsingAlias(FeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedbackId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedbackId, $comparison);
    }

    /**
     * Filter the query on the JUDUL column
     *
     * Example usage:
     * <code>
     * $query->filterByJudul('fooValue');   // WHERE JUDUL = 'fooValue'
     * $query->filterByJudul('%fooValue%'); // WHERE JUDUL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $judul The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByJudul($judul = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($judul)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $judul)) {
                $judul = str_replace('*', '%', $judul);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::JUDUL, $judul, $comparison);
    }

    /**
     * Filter the query on the DESKRIPSI column
     *
     * Example usage:
     * <code>
     * $query->filterByDeskripsi('fooValue');   // WHERE DESKRIPSI = 'fooValue'
     * $query->filterByDeskripsi('%fooValue%'); // WHERE DESKRIPSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deskripsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByDeskripsi($deskripsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deskripsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $deskripsi)) {
                $deskripsi = str_replace('*', '%', $deskripsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::DESKRIPSI, $deskripsi, $comparison);
    }

    /**
     * Filter the query on the GAMBAR column
     *
     * Example usage:
     * <code>
     * $query->filterByGambar('fooValue');   // WHERE GAMBAR = 'fooValue'
     * $query->filterByGambar('%fooValue%'); // WHERE GAMBAR LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gambar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByGambar($gambar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gambar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gambar)) {
                $gambar = str_replace('*', '%', $gambar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::GAMBAR, $gambar, $comparison);
    }

    /**
     * Filter the query on the EMAIL_PENGIRIM column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailPengirim('fooValue');   // WHERE EMAIL_PENGIRIM = 'fooValue'
     * $query->filterByEmailPengirim('%fooValue%'); // WHERE EMAIL_PENGIRIM LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailPengirim The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByEmailPengirim($emailPengirim = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailPengirim)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $emailPengirim)) {
                $emailPengirim = str_replace('*', '%', $emailPengirim);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::EMAIL_PENGIRIM, $emailPengirim, $comparison);
    }

    /**
     * Filter the query on the INSTANSI column
     *
     * Example usage:
     * <code>
     * $query->filterByInstansi('fooValue');   // WHERE INSTANSI = 'fooValue'
     * $query->filterByInstansi('%fooValue%'); // WHERE INSTANSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instansi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByInstansi($instansi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instansi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instansi)) {
                $instansi = str_replace('*', '%', $instansi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::INSTANSI, $instansi, $comparison);
    }

    /**
     * Filter the query on the NOMOR_TELEPON column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorTelepon('fooValue');   // WHERE NOMOR_TELEPON = 'fooValue'
     * $query->filterByNomorTelepon('%fooValue%'); // WHERE NOMOR_TELEPON LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorTelepon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByNomorTelepon($nomorTelepon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorTelepon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorTelepon)) {
                $nomorTelepon = str_replace('*', '%', $nomorTelepon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::NOMOR_TELEPON, $nomorTelepon, $comparison);
    }

    /**
     * Filter the query on the TANGGAL_LAPOR column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalLapor('2011-03-14'); // WHERE TANGGAL_LAPOR = '2011-03-14'
     * $query->filterByTanggalLapor('now'); // WHERE TANGGAL_LAPOR = '2011-03-14'
     * $query->filterByTanggalLapor(array('max' => 'yesterday')); // WHERE TANGGAL_LAPOR < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggalLapor The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByTanggalLapor($tanggalLapor = null, $comparison = null)
    {
        if (is_array($tanggalLapor)) {
            $useMinMax = false;
            if (isset($tanggalLapor['min'])) {
                $this->addUsingAlias(FeedbackPeer::TANGGAL_LAPOR, $tanggalLapor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggalLapor['max'])) {
                $this->addUsingAlias(FeedbackPeer::TANGGAL_LAPOR, $tanggalLapor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::TANGGAL_LAPOR, $tanggalLapor, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE STATUS = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE STATUS IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE STATUS >= 12
     * $query->filterByStatus(array('max' => 12)); // WHERE STATUS <= 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(FeedbackPeer::STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(FeedbackPeer::STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the TANGGAL_UPDATE column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalUpdate('2011-03-14'); // WHERE TANGGAL_UPDATE = '2011-03-14'
     * $query->filterByTanggalUpdate('now'); // WHERE TANGGAL_UPDATE = '2011-03-14'
     * $query->filterByTanggalUpdate(array('max' => 'yesterday')); // WHERE TANGGAL_UPDATE < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggalUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function filterByTanggalUpdate($tanggalUpdate = null, $comparison = null)
    {
        if (is_array($tanggalUpdate)) {
            $useMinMax = false;
            if (isset($tanggalUpdate['min'])) {
                $this->addUsingAlias(FeedbackPeer::TANGGAL_UPDATE, $tanggalUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggalUpdate['max'])) {
                $this->addUsingAlias(FeedbackPeer::TANGGAL_UPDATE, $tanggalUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FeedbackPeer::TANGGAL_UPDATE, $tanggalUpdate, $comparison);
    }

    /**
     * Filter the query by a related JenisFeedback object
     *
     * @param   JenisFeedback|PropelObjectCollection $jenisFeedback The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FeedbackQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisFeedback($jenisFeedback, $comparison = null)
    {
        if ($jenisFeedback instanceof JenisFeedback) {
            return $this
                ->addUsingAlias(FeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedback->getJenisFeedbackId(), $comparison);
        } elseif ($jenisFeedback instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedback->toKeyValue('PrimaryKey', 'JenisFeedbackId'), $comparison);
        } else {
            throw new PropelException('filterByJenisFeedback() only accepts arguments of type JenisFeedback or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisFeedback relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function joinJenisFeedback($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisFeedback');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisFeedback');
        }

        return $this;
    }

    /**
     * Use the JenisFeedback relation JenisFeedback object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\JenisFeedbackQuery A secondary query class using the current class as primary query
     */
    public function useJenisFeedbackQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisFeedback($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisFeedback', '\infopendataan\Model\JenisFeedbackQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Feedback $feedback Object to remove from the list of results
     *
     * @return FeedbackQuery The current query, for fluid interface
     */
    public function prune($feedback = null)
    {
        if ($feedback) {
            $this->addUsingAlias(FeedbackPeer::FEEDBACK_ID, $feedback->getFeedbackId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
