<?php

namespace infopendataan\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Artikel;
use infopendataan\Model\ArtikelQuery;
use infopendataan\Model\Berita;
use infopendataan\Model\BeritaQuery;
use infopendataan\Model\Dokumentasi;
use infopendataan\Model\DokumentasiQuery;
use infopendataan\Model\Faq;
use infopendataan\Model\FaqQuery;
use infopendataan\Model\File;
use infopendataan\Model\FileQuery;
use infopendataan\Model\Installer;
use infopendataan\Model\InstallerQuery;
use infopendataan\Model\JenisKelamin;
use infopendataan\Model\JenisKelaminQuery;
use infopendataan\Model\LogLogin;
use infopendataan\Model\LogLoginQuery;
use infopendataan\Model\MediaSosial;
use infopendataan\Model\MediaSosialQuery;
use infopendataan\Model\Patch;
use infopendataan\Model\PatchQuery;
use infopendataan\Model\Pengguna;
use infopendataan\Model\PenggunaPeer;
use infopendataan\Model\PenggunaQuery;

/**
 * Base class that represents a row from the 'pengguna' table.
 *
 *
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BasePengguna extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'infopendataan\\Model\\PenggunaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PenggunaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pengguna_id field.
     * @var        int
     */
    protected $pengguna_id;

    /**
     * The value for the jenis_kelamin_id field.
     * @var        int
     */
    protected $jenis_kelamin_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * @var        JenisKelamin
     */
    protected $aJenisKelamin;

    /**
     * @var        PropelObjectCollection|Artikel[] Collection to store aggregation of Artikel objects.
     */
    protected $collArtikels;
    protected $collArtikelsPartial;

    /**
     * @var        PropelObjectCollection|Berita[] Collection to store aggregation of Berita objects.
     */
    protected $collBeritas;
    protected $collBeritasPartial;

    /**
     * @var        PropelObjectCollection|Dokumentasi[] Collection to store aggregation of Dokumentasi objects.
     */
    protected $collDokumentasis;
    protected $collDokumentasisPartial;

    /**
     * @var        PropelObjectCollection|Faq[] Collection to store aggregation of Faq objects.
     */
    protected $collFaqs;
    protected $collFaqsPartial;

    /**
     * @var        PropelObjectCollection|File[] Collection to store aggregation of File objects.
     */
    protected $collFiles;
    protected $collFilesPartial;

    /**
     * @var        PropelObjectCollection|Installer[] Collection to store aggregation of Installer objects.
     */
    protected $collInstallers;
    protected $collInstallersPartial;

    /**
     * @var        PropelObjectCollection|LogLogin[] Collection to store aggregation of LogLogin objects.
     */
    protected $collLogLogins;
    protected $collLogLoginsPartial;

    /**
     * @var        PropelObjectCollection|MediaSosial[] Collection to store aggregation of MediaSosial objects.
     */
    protected $collMediaSosials;
    protected $collMediaSosialsPartial;

    /**
     * @var        PropelObjectCollection|Patch[] Collection to store aggregation of Patch objects.
     */
    protected $collPatchs;
    protected $collPatchsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $artikelsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beritasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $dokumentasisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $faqsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $filesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $installersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $logLoginsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $mediaSosialsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $patchsScheduledForDeletion = null;

    /**
     * Get the [pengguna_id] column value.
     *
     * @return int
     */
    public function getPenggunaId()
    {

        return $this->pengguna_id;
    }

    /**
     * Get the [jenis_kelamin_id] column value.
     *
     * @return int
     */
    public function getJenisKelaminId()
    {

        return $this->jenis_kelamin_id;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {

        return $this->nama;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {

        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Set the value of [pengguna_id] column.
     *
     * @param  int $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::PENGGUNA_ID;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [jenis_kelamin_id] column.
     *
     * @param  int $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setJenisKelaminId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jenis_kelamin_id !== $v) {
            $this->jenis_kelamin_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::JENIS_KELAMIN_ID;
        }

        if ($this->aJenisKelamin !== null && $this->aJenisKelamin->getJenisKelaminId() !== $v) {
            $this->aJenisKelamin = null;
        }


        return $this;
    } // setJenisKelaminId()

    /**
     * Set the value of [nama] column.
     *
     * @param  string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PenggunaPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = PenggunaPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [username] column.
     *
     * @param  string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = PenggunaPeer::USERNAME;
        }


        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param  string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = PenggunaPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pengguna_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->jenis_kelamin_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->nama = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->email = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->username = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->password = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 6; // 6 = PenggunaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Pengguna object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aJenisKelamin !== null && $this->jenis_kelamin_id !== $this->aJenisKelamin->getJenisKelaminId()) {
            $this->aJenisKelamin = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PenggunaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aJenisKelamin = null;
            $this->collArtikels = null;

            $this->collBeritas = null;

            $this->collDokumentasis = null;

            $this->collFaqs = null;

            $this->collFiles = null;

            $this->collInstallers = null;

            $this->collLogLogins = null;

            $this->collMediaSosials = null;

            $this->collPatchs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PenggunaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PenggunaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJenisKelamin !== null) {
                if ($this->aJenisKelamin->isModified() || $this->aJenisKelamin->isNew()) {
                    $affectedRows += $this->aJenisKelamin->save($con);
                }
                $this->setJenisKelamin($this->aJenisKelamin);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->artikelsScheduledForDeletion !== null) {
                if (!$this->artikelsScheduledForDeletion->isEmpty()) {
                    foreach ($this->artikelsScheduledForDeletion as $artikel) {
                        // need to save related object because we set the relation to null
                        $artikel->save($con);
                    }
                    $this->artikelsScheduledForDeletion = null;
                }
            }

            if ($this->collArtikels !== null) {
                foreach ($this->collArtikels as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->beritasScheduledForDeletion !== null) {
                if (!$this->beritasScheduledForDeletion->isEmpty()) {
                    foreach ($this->beritasScheduledForDeletion as $berita) {
                        // need to save related object because we set the relation to null
                        $berita->save($con);
                    }
                    $this->beritasScheduledForDeletion = null;
                }
            }

            if ($this->collBeritas !== null) {
                foreach ($this->collBeritas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dokumentasisScheduledForDeletion !== null) {
                if (!$this->dokumentasisScheduledForDeletion->isEmpty()) {
                    foreach ($this->dokumentasisScheduledForDeletion as $dokumentasi) {
                        // need to save related object because we set the relation to null
                        $dokumentasi->save($con);
                    }
                    $this->dokumentasisScheduledForDeletion = null;
                }
            }

            if ($this->collDokumentasis !== null) {
                foreach ($this->collDokumentasis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->faqsScheduledForDeletion !== null) {
                if (!$this->faqsScheduledForDeletion->isEmpty()) {
                    foreach ($this->faqsScheduledForDeletion as $faq) {
                        // need to save related object because we set the relation to null
                        $faq->save($con);
                    }
                    $this->faqsScheduledForDeletion = null;
                }
            }

            if ($this->collFaqs !== null) {
                foreach ($this->collFaqs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->filesScheduledForDeletion !== null) {
                if (!$this->filesScheduledForDeletion->isEmpty()) {
                    foreach ($this->filesScheduledForDeletion as $file) {
                        // need to save related object because we set the relation to null
                        $file->save($con);
                    }
                    $this->filesScheduledForDeletion = null;
                }
            }

            if ($this->collFiles !== null) {
                foreach ($this->collFiles as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->installersScheduledForDeletion !== null) {
                if (!$this->installersScheduledForDeletion->isEmpty()) {
                    foreach ($this->installersScheduledForDeletion as $installer) {
                        // need to save related object because we set the relation to null
                        $installer->save($con);
                    }
                    $this->installersScheduledForDeletion = null;
                }
            }

            if ($this->collInstallers !== null) {
                foreach ($this->collInstallers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->logLoginsScheduledForDeletion !== null) {
                if (!$this->logLoginsScheduledForDeletion->isEmpty()) {
                    foreach ($this->logLoginsScheduledForDeletion as $logLogin) {
                        // need to save related object because we set the relation to null
                        $logLogin->save($con);
                    }
                    $this->logLoginsScheduledForDeletion = null;
                }
            }

            if ($this->collLogLogins !== null) {
                foreach ($this->collLogLogins as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mediaSosialsScheduledForDeletion !== null) {
                if (!$this->mediaSosialsScheduledForDeletion->isEmpty()) {
                    foreach ($this->mediaSosialsScheduledForDeletion as $mediaSosial) {
                        // need to save related object because we set the relation to null
                        $mediaSosial->save($con);
                    }
                    $this->mediaSosialsScheduledForDeletion = null;
                }
            }

            if ($this->collMediaSosials !== null) {
                foreach ($this->collMediaSosials as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->patchsScheduledForDeletion !== null) {
                if (!$this->patchsScheduledForDeletion->isEmpty()) {
                    foreach ($this->patchsScheduledForDeletion as $patch) {
                        // need to save related object because we set the relation to null
                        $patch->save($con);
                    }
                    $this->patchsScheduledForDeletion = null;
                }
            }

            if ($this->collPatchs !== null) {
                foreach ($this->collPatchs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = PenggunaPeer::PENGGUNA_ID;
        if (null !== $this->pengguna_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PenggunaPeer::PENGGUNA_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PenggunaPeer::PENGGUNA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PENGGUNA_ID`';
        }
        if ($this->isColumnModified(PenggunaPeer::JENIS_KELAMIN_ID)) {
            $modifiedColumns[':p' . $index++]  = '`JENIS_KELAMIN_ID`';
        }
        if ($this->isColumnModified(PenggunaPeer::NAMA)) {
            $modifiedColumns[':p' . $index++]  = '`NAMA`';
        }
        if ($this->isColumnModified(PenggunaPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`EMAIL`';
        }
        if ($this->isColumnModified(PenggunaPeer::USERNAME)) {
            $modifiedColumns[':p' . $index++]  = '`USERNAME`';
        }
        if ($this->isColumnModified(PenggunaPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`PASSWORD`';
        }

        $sql = sprintf(
            'INSERT INTO `pengguna` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`PENGGUNA_ID`':
                        $stmt->bindValue($identifier, $this->pengguna_id, PDO::PARAM_INT);
                        break;
                    case '`JENIS_KELAMIN_ID`':
                        $stmt->bindValue($identifier, $this->jenis_kelamin_id, PDO::PARAM_INT);
                        break;
                    case '`NAMA`':
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case '`EMAIL`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`USERNAME`':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case '`PASSWORD`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setPenggunaId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJenisKelamin !== null) {
                if (!$this->aJenisKelamin->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisKelamin->getValidationFailures());
                }
            }


            if (($retval = PenggunaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collArtikels !== null) {
                    foreach ($this->collArtikels as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collBeritas !== null) {
                    foreach ($this->collBeritas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDokumentasis !== null) {
                    foreach ($this->collDokumentasis as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collFaqs !== null) {
                    foreach ($this->collFaqs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collFiles !== null) {
                    foreach ($this->collFiles as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collInstallers !== null) {
                    foreach ($this->collInstallers as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collLogLogins !== null) {
                    foreach ($this->collLogLogins as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collMediaSosials !== null) {
                    foreach ($this->collMediaSosials as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPatchs !== null) {
                    foreach ($this->collPatchs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPenggunaId();
                break;
            case 1:
                return $this->getJenisKelaminId();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getUsername();
                break;
            case 5:
                return $this->getPassword();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Pengguna'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pengguna'][$this->getPrimaryKey()] = true;
        $keys = PenggunaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPenggunaId(),
            $keys[1] => $this->getJenisKelaminId(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getUsername(),
            $keys[5] => $this->getPassword(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aJenisKelamin) {
                $result['JenisKelamin'] = $this->aJenisKelamin->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collArtikels) {
                $result['Artikels'] = $this->collArtikels->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBeritas) {
                $result['Beritas'] = $this->collBeritas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDokumentasis) {
                $result['Dokumentasis'] = $this->collDokumentasis->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFaqs) {
                $result['Faqs'] = $this->collFaqs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFiles) {
                $result['Files'] = $this->collFiles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInstallers) {
                $result['Installers'] = $this->collInstallers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLogLogins) {
                $result['LogLogins'] = $this->collLogLogins->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMediaSosials) {
                $result['MediaSosials'] = $this->collMediaSosials->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPatchs) {
                $result['Patchs'] = $this->collPatchs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPenggunaId($value);
                break;
            case 1:
                $this->setJenisKelaminId($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setUsername($value);
                break;
            case 5:
                $this->setPassword($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PenggunaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPenggunaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setJenisKelaminId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEmail($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setUsername($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPassword($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);

        if ($this->isColumnModified(PenggunaPeer::PENGGUNA_ID)) $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(PenggunaPeer::JENIS_KELAMIN_ID)) $criteria->add(PenggunaPeer::JENIS_KELAMIN_ID, $this->jenis_kelamin_id);
        if ($this->isColumnModified(PenggunaPeer::NAMA)) $criteria->add(PenggunaPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PenggunaPeer::EMAIL)) $criteria->add(PenggunaPeer::EMAIL, $this->email);
        if ($this->isColumnModified(PenggunaPeer::USERNAME)) $criteria->add(PenggunaPeer::USERNAME, $this->username);
        if ($this->isColumnModified(PenggunaPeer::PASSWORD)) $criteria->add(PenggunaPeer::PASSWORD, $this->password);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
        $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getPenggunaId();
    }

    /**
     * Generic method to set the primary key (pengguna_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPenggunaId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPenggunaId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Pengguna (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setJenisKelaminId($this->getJenisKelaminId());
        $copyObj->setNama($this->getNama());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getArtikels() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addArtikel($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBeritas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBerita($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDokumentasis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDokumentasi($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFaqs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFaq($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFiles() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFile($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInstallers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInstaller($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLogLogins() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogLogin($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMediaSosials() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMediaSosial($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPatchs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPatch($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPenggunaId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Pengguna Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PenggunaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PenggunaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a JenisKelamin object.
     *
     * @param                  JenisKelamin $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisKelamin(JenisKelamin $v = null)
    {
        if ($v === null) {
            $this->setJenisKelaminId(NULL);
        } else {
            $this->setJenisKelaminId($v->getJenisKelaminId());
        }

        $this->aJenisKelamin = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisKelamin object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisKelamin object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisKelamin The associated JenisKelamin object.
     * @throws PropelException
     */
    public function getJenisKelamin(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisKelamin === null && ($this->jenis_kelamin_id !== null) && $doQuery) {
            $this->aJenisKelamin = JenisKelaminQuery::create()->findPk($this->jenis_kelamin_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisKelamin->addPenggunas($this);
             */
        }

        return $this->aJenisKelamin;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Artikel' == $relationName) {
            $this->initArtikels();
        }
        if ('Berita' == $relationName) {
            $this->initBeritas();
        }
        if ('Dokumentasi' == $relationName) {
            $this->initDokumentasis();
        }
        if ('Faq' == $relationName) {
            $this->initFaqs();
        }
        if ('File' == $relationName) {
            $this->initFiles();
        }
        if ('Installer' == $relationName) {
            $this->initInstallers();
        }
        if ('LogLogin' == $relationName) {
            $this->initLogLogins();
        }
        if ('MediaSosial' == $relationName) {
            $this->initMediaSosials();
        }
        if ('Patch' == $relationName) {
            $this->initPatchs();
        }
    }

    /**
     * Clears out the collArtikels collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addArtikels()
     */
    public function clearArtikels()
    {
        $this->collArtikels = null; // important to set this to null since that means it is uninitialized
        $this->collArtikelsPartial = null;

        return $this;
    }

    /**
     * reset is the collArtikels collection loaded partially
     *
     * @return void
     */
    public function resetPartialArtikels($v = true)
    {
        $this->collArtikelsPartial = $v;
    }

    /**
     * Initializes the collArtikels collection.
     *
     * By default this just sets the collArtikels collection to an empty array (like clearcollArtikels());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initArtikels($overrideExisting = true)
    {
        if (null !== $this->collArtikels && !$overrideExisting) {
            return;
        }
        $this->collArtikels = new PropelObjectCollection();
        $this->collArtikels->setModel('Artikel');
    }

    /**
     * Gets an array of Artikel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Artikel[] List of Artikel objects
     * @throws PropelException
     */
    public function getArtikels($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collArtikelsPartial && !$this->isNew();
        if (null === $this->collArtikels || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collArtikels) {
                // return empty collection
                $this->initArtikels();
            } else {
                $collArtikels = ArtikelQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collArtikelsPartial && count($collArtikels)) {
                      $this->initArtikels(false);

                      foreach ($collArtikels as $obj) {
                        if (false == $this->collArtikels->contains($obj)) {
                          $this->collArtikels->append($obj);
                        }
                      }

                      $this->collArtikelsPartial = true;
                    }

                    $collArtikels->getInternalIterator()->rewind();

                    return $collArtikels;
                }

                if ($partial && $this->collArtikels) {
                    foreach ($this->collArtikels as $obj) {
                        if ($obj->isNew()) {
                            $collArtikels[] = $obj;
                        }
                    }
                }

                $this->collArtikels = $collArtikels;
                $this->collArtikelsPartial = false;
            }
        }

        return $this->collArtikels;
    }

    /**
     * Sets a collection of Artikel objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $artikels A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setArtikels(PropelCollection $artikels, PropelPDO $con = null)
    {
        $artikelsToDelete = $this->getArtikels(new Criteria(), $con)->diff($artikels);


        $this->artikelsScheduledForDeletion = $artikelsToDelete;

        foreach ($artikelsToDelete as $artikelRemoved) {
            $artikelRemoved->setPengguna(null);
        }

        $this->collArtikels = null;
        foreach ($artikels as $artikel) {
            $this->addArtikel($artikel);
        }

        $this->collArtikels = $artikels;
        $this->collArtikelsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Artikel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Artikel objects.
     * @throws PropelException
     */
    public function countArtikels(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collArtikelsPartial && !$this->isNew();
        if (null === $this->collArtikels || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collArtikels) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getArtikels());
            }
            $query = ArtikelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collArtikels);
    }

    /**
     * Method called to associate a Artikel object to this object
     * through the Artikel foreign key attribute.
     *
     * @param    Artikel $l Artikel
     * @return Pengguna The current object (for fluent API support)
     */
    public function addArtikel(Artikel $l)
    {
        if ($this->collArtikels === null) {
            $this->initArtikels();
            $this->collArtikelsPartial = true;
        }

        if (!in_array($l, $this->collArtikels->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddArtikel($l);

            if ($this->artikelsScheduledForDeletion and $this->artikelsScheduledForDeletion->contains($l)) {
                $this->artikelsScheduledForDeletion->remove($this->artikelsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Artikel $artikel The artikel object to add.
     */
    protected function doAddArtikel($artikel)
    {
        $this->collArtikels[]= $artikel;
        $artikel->setPengguna($this);
    }

    /**
     * @param	Artikel $artikel The artikel object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeArtikel($artikel)
    {
        if ($this->getArtikels()->contains($artikel)) {
            $this->collArtikels->remove($this->collArtikels->search($artikel));
            if (null === $this->artikelsScheduledForDeletion) {
                $this->artikelsScheduledForDeletion = clone $this->collArtikels;
                $this->artikelsScheduledForDeletion->clear();
            }
            $this->artikelsScheduledForDeletion[]= $artikel;
            $artikel->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collBeritas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addBeritas()
     */
    public function clearBeritas()
    {
        $this->collBeritas = null; // important to set this to null since that means it is uninitialized
        $this->collBeritasPartial = null;

        return $this;
    }

    /**
     * reset is the collBeritas collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeritas($v = true)
    {
        $this->collBeritasPartial = $v;
    }

    /**
     * Initializes the collBeritas collection.
     *
     * By default this just sets the collBeritas collection to an empty array (like clearcollBeritas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeritas($overrideExisting = true)
    {
        if (null !== $this->collBeritas && !$overrideExisting) {
            return;
        }
        $this->collBeritas = new PropelObjectCollection();
        $this->collBeritas->setModel('Berita');
    }

    /**
     * Gets an array of Berita objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Berita[] List of Berita objects
     * @throws PropelException
     */
    public function getBeritas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeritasPartial && !$this->isNew();
        if (null === $this->collBeritas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeritas) {
                // return empty collection
                $this->initBeritas();
            } else {
                $collBeritas = BeritaQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeritasPartial && count($collBeritas)) {
                      $this->initBeritas(false);

                      foreach ($collBeritas as $obj) {
                        if (false == $this->collBeritas->contains($obj)) {
                          $this->collBeritas->append($obj);
                        }
                      }

                      $this->collBeritasPartial = true;
                    }

                    $collBeritas->getInternalIterator()->rewind();

                    return $collBeritas;
                }

                if ($partial && $this->collBeritas) {
                    foreach ($this->collBeritas as $obj) {
                        if ($obj->isNew()) {
                            $collBeritas[] = $obj;
                        }
                    }
                }

                $this->collBeritas = $collBeritas;
                $this->collBeritasPartial = false;
            }
        }

        return $this->collBeritas;
    }

    /**
     * Sets a collection of Berita objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beritas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setBeritas(PropelCollection $beritas, PropelPDO $con = null)
    {
        $beritasToDelete = $this->getBeritas(new Criteria(), $con)->diff($beritas);


        $this->beritasScheduledForDeletion = $beritasToDelete;

        foreach ($beritasToDelete as $beritaRemoved) {
            $beritaRemoved->setPengguna(null);
        }

        $this->collBeritas = null;
        foreach ($beritas as $berita) {
            $this->addBerita($berita);
        }

        $this->collBeritas = $beritas;
        $this->collBeritasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Berita objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Berita objects.
     * @throws PropelException
     */
    public function countBeritas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeritasPartial && !$this->isNew();
        if (null === $this->collBeritas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeritas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBeritas());
            }
            $query = BeritaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collBeritas);
    }

    /**
     * Method called to associate a Berita object to this object
     * through the Berita foreign key attribute.
     *
     * @param    Berita $l Berita
     * @return Pengguna The current object (for fluent API support)
     */
    public function addBerita(Berita $l)
    {
        if ($this->collBeritas === null) {
            $this->initBeritas();
            $this->collBeritasPartial = true;
        }

        if (!in_array($l, $this->collBeritas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBerita($l);

            if ($this->beritasScheduledForDeletion and $this->beritasScheduledForDeletion->contains($l)) {
                $this->beritasScheduledForDeletion->remove($this->beritasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Berita $berita The berita object to add.
     */
    protected function doAddBerita($berita)
    {
        $this->collBeritas[]= $berita;
        $berita->setPengguna($this);
    }

    /**
     * @param	Berita $berita The berita object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeBerita($berita)
    {
        if ($this->getBeritas()->contains($berita)) {
            $this->collBeritas->remove($this->collBeritas->search($berita));
            if (null === $this->beritasScheduledForDeletion) {
                $this->beritasScheduledForDeletion = clone $this->collBeritas;
                $this->beritasScheduledForDeletion->clear();
            }
            $this->beritasScheduledForDeletion[]= $berita;
            $berita->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collDokumentasis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addDokumentasis()
     */
    public function clearDokumentasis()
    {
        $this->collDokumentasis = null; // important to set this to null since that means it is uninitialized
        $this->collDokumentasisPartial = null;

        return $this;
    }

    /**
     * reset is the collDokumentasis collection loaded partially
     *
     * @return void
     */
    public function resetPartialDokumentasis($v = true)
    {
        $this->collDokumentasisPartial = $v;
    }

    /**
     * Initializes the collDokumentasis collection.
     *
     * By default this just sets the collDokumentasis collection to an empty array (like clearcollDokumentasis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDokumentasis($overrideExisting = true)
    {
        if (null !== $this->collDokumentasis && !$overrideExisting) {
            return;
        }
        $this->collDokumentasis = new PropelObjectCollection();
        $this->collDokumentasis->setModel('Dokumentasi');
    }

    /**
     * Gets an array of Dokumentasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Dokumentasi[] List of Dokumentasi objects
     * @throws PropelException
     */
    public function getDokumentasis($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDokumentasisPartial && !$this->isNew();
        if (null === $this->collDokumentasis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDokumentasis) {
                // return empty collection
                $this->initDokumentasis();
            } else {
                $collDokumentasis = DokumentasiQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDokumentasisPartial && count($collDokumentasis)) {
                      $this->initDokumentasis(false);

                      foreach ($collDokumentasis as $obj) {
                        if (false == $this->collDokumentasis->contains($obj)) {
                          $this->collDokumentasis->append($obj);
                        }
                      }

                      $this->collDokumentasisPartial = true;
                    }

                    $collDokumentasis->getInternalIterator()->rewind();

                    return $collDokumentasis;
                }

                if ($partial && $this->collDokumentasis) {
                    foreach ($this->collDokumentasis as $obj) {
                        if ($obj->isNew()) {
                            $collDokumentasis[] = $obj;
                        }
                    }
                }

                $this->collDokumentasis = $collDokumentasis;
                $this->collDokumentasisPartial = false;
            }
        }

        return $this->collDokumentasis;
    }

    /**
     * Sets a collection of Dokumentasi objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $dokumentasis A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setDokumentasis(PropelCollection $dokumentasis, PropelPDO $con = null)
    {
        $dokumentasisToDelete = $this->getDokumentasis(new Criteria(), $con)->diff($dokumentasis);


        $this->dokumentasisScheduledForDeletion = $dokumentasisToDelete;

        foreach ($dokumentasisToDelete as $dokumentasiRemoved) {
            $dokumentasiRemoved->setPengguna(null);
        }

        $this->collDokumentasis = null;
        foreach ($dokumentasis as $dokumentasi) {
            $this->addDokumentasi($dokumentasi);
        }

        $this->collDokumentasis = $dokumentasis;
        $this->collDokumentasisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Dokumentasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Dokumentasi objects.
     * @throws PropelException
     */
    public function countDokumentasis(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDokumentasisPartial && !$this->isNew();
        if (null === $this->collDokumentasis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDokumentasis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDokumentasis());
            }
            $query = DokumentasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collDokumentasis);
    }

    /**
     * Method called to associate a Dokumentasi object to this object
     * through the Dokumentasi foreign key attribute.
     *
     * @param    Dokumentasi $l Dokumentasi
     * @return Pengguna The current object (for fluent API support)
     */
    public function addDokumentasi(Dokumentasi $l)
    {
        if ($this->collDokumentasis === null) {
            $this->initDokumentasis();
            $this->collDokumentasisPartial = true;
        }

        if (!in_array($l, $this->collDokumentasis->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDokumentasi($l);

            if ($this->dokumentasisScheduledForDeletion and $this->dokumentasisScheduledForDeletion->contains($l)) {
                $this->dokumentasisScheduledForDeletion->remove($this->dokumentasisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Dokumentasi $dokumentasi The dokumentasi object to add.
     */
    protected function doAddDokumentasi($dokumentasi)
    {
        $this->collDokumentasis[]= $dokumentasi;
        $dokumentasi->setPengguna($this);
    }

    /**
     * @param	Dokumentasi $dokumentasi The dokumentasi object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeDokumentasi($dokumentasi)
    {
        if ($this->getDokumentasis()->contains($dokumentasi)) {
            $this->collDokumentasis->remove($this->collDokumentasis->search($dokumentasi));
            if (null === $this->dokumentasisScheduledForDeletion) {
                $this->dokumentasisScheduledForDeletion = clone $this->collDokumentasis;
                $this->dokumentasisScheduledForDeletion->clear();
            }
            $this->dokumentasisScheduledForDeletion[]= $dokumentasi;
            $dokumentasi->setPengguna(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related Dokumentasis from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Dokumentasi[] List of Dokumentasi objects
     */
    public function getDokumentasisJoinJenisDokumentasi($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DokumentasiQuery::create(null, $criteria);
        $query->joinWith('JenisDokumentasi', $join_behavior);

        return $this->getDokumentasis($query, $con);
    }

    /**
     * Clears out the collFaqs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addFaqs()
     */
    public function clearFaqs()
    {
        $this->collFaqs = null; // important to set this to null since that means it is uninitialized
        $this->collFaqsPartial = null;

        return $this;
    }

    /**
     * reset is the collFaqs collection loaded partially
     *
     * @return void
     */
    public function resetPartialFaqs($v = true)
    {
        $this->collFaqsPartial = $v;
    }

    /**
     * Initializes the collFaqs collection.
     *
     * By default this just sets the collFaqs collection to an empty array (like clearcollFaqs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFaqs($overrideExisting = true)
    {
        if (null !== $this->collFaqs && !$overrideExisting) {
            return;
        }
        $this->collFaqs = new PropelObjectCollection();
        $this->collFaqs->setModel('Faq');
    }

    /**
     * Gets an array of Faq objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Faq[] List of Faq objects
     * @throws PropelException
     */
    public function getFaqs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collFaqsPartial && !$this->isNew();
        if (null === $this->collFaqs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFaqs) {
                // return empty collection
                $this->initFaqs();
            } else {
                $collFaqs = FaqQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collFaqsPartial && count($collFaqs)) {
                      $this->initFaqs(false);

                      foreach ($collFaqs as $obj) {
                        if (false == $this->collFaqs->contains($obj)) {
                          $this->collFaqs->append($obj);
                        }
                      }

                      $this->collFaqsPartial = true;
                    }

                    $collFaqs->getInternalIterator()->rewind();

                    return $collFaqs;
                }

                if ($partial && $this->collFaqs) {
                    foreach ($this->collFaqs as $obj) {
                        if ($obj->isNew()) {
                            $collFaqs[] = $obj;
                        }
                    }
                }

                $this->collFaqs = $collFaqs;
                $this->collFaqsPartial = false;
            }
        }

        return $this->collFaqs;
    }

    /**
     * Sets a collection of Faq objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $faqs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setFaqs(PropelCollection $faqs, PropelPDO $con = null)
    {
        $faqsToDelete = $this->getFaqs(new Criteria(), $con)->diff($faqs);


        $this->faqsScheduledForDeletion = $faqsToDelete;

        foreach ($faqsToDelete as $faqRemoved) {
            $faqRemoved->setPengguna(null);
        }

        $this->collFaqs = null;
        foreach ($faqs as $faq) {
            $this->addFaq($faq);
        }

        $this->collFaqs = $faqs;
        $this->collFaqsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Faq objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Faq objects.
     * @throws PropelException
     */
    public function countFaqs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collFaqsPartial && !$this->isNew();
        if (null === $this->collFaqs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFaqs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFaqs());
            }
            $query = FaqQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collFaqs);
    }

    /**
     * Method called to associate a Faq object to this object
     * through the Faq foreign key attribute.
     *
     * @param    Faq $l Faq
     * @return Pengguna The current object (for fluent API support)
     */
    public function addFaq(Faq $l)
    {
        if ($this->collFaqs === null) {
            $this->initFaqs();
            $this->collFaqsPartial = true;
        }

        if (!in_array($l, $this->collFaqs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddFaq($l);

            if ($this->faqsScheduledForDeletion and $this->faqsScheduledForDeletion->contains($l)) {
                $this->faqsScheduledForDeletion->remove($this->faqsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Faq $faq The faq object to add.
     */
    protected function doAddFaq($faq)
    {
        $this->collFaqs[]= $faq;
        $faq->setPengguna($this);
    }

    /**
     * @param	Faq $faq The faq object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeFaq($faq)
    {
        if ($this->getFaqs()->contains($faq)) {
            $this->collFaqs->remove($this->collFaqs->search($faq));
            if (null === $this->faqsScheduledForDeletion) {
                $this->faqsScheduledForDeletion = clone $this->collFaqs;
                $this->faqsScheduledForDeletion->clear();
            }
            $this->faqsScheduledForDeletion[]= $faq;
            $faq->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collFiles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addFiles()
     */
    public function clearFiles()
    {
        $this->collFiles = null; // important to set this to null since that means it is uninitialized
        $this->collFilesPartial = null;

        return $this;
    }

    /**
     * reset is the collFiles collection loaded partially
     *
     * @return void
     */
    public function resetPartialFiles($v = true)
    {
        $this->collFilesPartial = $v;
    }

    /**
     * Initializes the collFiles collection.
     *
     * By default this just sets the collFiles collection to an empty array (like clearcollFiles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFiles($overrideExisting = true)
    {
        if (null !== $this->collFiles && !$overrideExisting) {
            return;
        }
        $this->collFiles = new PropelObjectCollection();
        $this->collFiles->setModel('File');
    }

    /**
     * Gets an array of File objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|File[] List of File objects
     * @throws PropelException
     */
    public function getFiles($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collFilesPartial && !$this->isNew();
        if (null === $this->collFiles || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFiles) {
                // return empty collection
                $this->initFiles();
            } else {
                $collFiles = FileQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collFilesPartial && count($collFiles)) {
                      $this->initFiles(false);

                      foreach ($collFiles as $obj) {
                        if (false == $this->collFiles->contains($obj)) {
                          $this->collFiles->append($obj);
                        }
                      }

                      $this->collFilesPartial = true;
                    }

                    $collFiles->getInternalIterator()->rewind();

                    return $collFiles;
                }

                if ($partial && $this->collFiles) {
                    foreach ($this->collFiles as $obj) {
                        if ($obj->isNew()) {
                            $collFiles[] = $obj;
                        }
                    }
                }

                $this->collFiles = $collFiles;
                $this->collFilesPartial = false;
            }
        }

        return $this->collFiles;
    }

    /**
     * Sets a collection of File objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $files A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setFiles(PropelCollection $files, PropelPDO $con = null)
    {
        $filesToDelete = $this->getFiles(new Criteria(), $con)->diff($files);


        $this->filesScheduledForDeletion = $filesToDelete;

        foreach ($filesToDelete as $fileRemoved) {
            $fileRemoved->setPengguna(null);
        }

        $this->collFiles = null;
        foreach ($files as $file) {
            $this->addFile($file);
        }

        $this->collFiles = $files;
        $this->collFilesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related File objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related File objects.
     * @throws PropelException
     */
    public function countFiles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collFilesPartial && !$this->isNew();
        if (null === $this->collFiles || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFiles) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFiles());
            }
            $query = FileQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collFiles);
    }

    /**
     * Method called to associate a File object to this object
     * through the File foreign key attribute.
     *
     * @param    File $l File
     * @return Pengguna The current object (for fluent API support)
     */
    public function addFile(File $l)
    {
        if ($this->collFiles === null) {
            $this->initFiles();
            $this->collFilesPartial = true;
        }

        if (!in_array($l, $this->collFiles->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddFile($l);

            if ($this->filesScheduledForDeletion and $this->filesScheduledForDeletion->contains($l)) {
                $this->filesScheduledForDeletion->remove($this->filesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	File $file The file object to add.
     */
    protected function doAddFile($file)
    {
        $this->collFiles[]= $file;
        $file->setPengguna($this);
    }

    /**
     * @param	File $file The file object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeFile($file)
    {
        if ($this->getFiles()->contains($file)) {
            $this->collFiles->remove($this->collFiles->search($file));
            if (null === $this->filesScheduledForDeletion) {
                $this->filesScheduledForDeletion = clone $this->collFiles;
                $this->filesScheduledForDeletion->clear();
            }
            $this->filesScheduledForDeletion[]= $file;
            $file->setPengguna(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related Files from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|File[] List of File objects
     */
    public function getFilesJoinJenisFile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FileQuery::create(null, $criteria);
        $query->joinWith('JenisFile', $join_behavior);

        return $this->getFiles($query, $con);
    }

    /**
     * Clears out the collInstallers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addInstallers()
     */
    public function clearInstallers()
    {
        $this->collInstallers = null; // important to set this to null since that means it is uninitialized
        $this->collInstallersPartial = null;

        return $this;
    }

    /**
     * reset is the collInstallers collection loaded partially
     *
     * @return void
     */
    public function resetPartialInstallers($v = true)
    {
        $this->collInstallersPartial = $v;
    }

    /**
     * Initializes the collInstallers collection.
     *
     * By default this just sets the collInstallers collection to an empty array (like clearcollInstallers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInstallers($overrideExisting = true)
    {
        if (null !== $this->collInstallers && !$overrideExisting) {
            return;
        }
        $this->collInstallers = new PropelObjectCollection();
        $this->collInstallers->setModel('Installer');
    }

    /**
     * Gets an array of Installer objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Installer[] List of Installer objects
     * @throws PropelException
     */
    public function getInstallers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInstallersPartial && !$this->isNew();
        if (null === $this->collInstallers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInstallers) {
                // return empty collection
                $this->initInstallers();
            } else {
                $collInstallers = InstallerQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInstallersPartial && count($collInstallers)) {
                      $this->initInstallers(false);

                      foreach ($collInstallers as $obj) {
                        if (false == $this->collInstallers->contains($obj)) {
                          $this->collInstallers->append($obj);
                        }
                      }

                      $this->collInstallersPartial = true;
                    }

                    $collInstallers->getInternalIterator()->rewind();

                    return $collInstallers;
                }

                if ($partial && $this->collInstallers) {
                    foreach ($this->collInstallers as $obj) {
                        if ($obj->isNew()) {
                            $collInstallers[] = $obj;
                        }
                    }
                }

                $this->collInstallers = $collInstallers;
                $this->collInstallersPartial = false;
            }
        }

        return $this->collInstallers;
    }

    /**
     * Sets a collection of Installer objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $installers A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setInstallers(PropelCollection $installers, PropelPDO $con = null)
    {
        $installersToDelete = $this->getInstallers(new Criteria(), $con)->diff($installers);


        $this->installersScheduledForDeletion = $installersToDelete;

        foreach ($installersToDelete as $installerRemoved) {
            $installerRemoved->setPengguna(null);
        }

        $this->collInstallers = null;
        foreach ($installers as $installer) {
            $this->addInstaller($installer);
        }

        $this->collInstallers = $installers;
        $this->collInstallersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Installer objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Installer objects.
     * @throws PropelException
     */
    public function countInstallers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInstallersPartial && !$this->isNew();
        if (null === $this->collInstallers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInstallers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getInstallers());
            }
            $query = InstallerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collInstallers);
    }

    /**
     * Method called to associate a Installer object to this object
     * through the Installer foreign key attribute.
     *
     * @param    Installer $l Installer
     * @return Pengguna The current object (for fluent API support)
     */
    public function addInstaller(Installer $l)
    {
        if ($this->collInstallers === null) {
            $this->initInstallers();
            $this->collInstallersPartial = true;
        }

        if (!in_array($l, $this->collInstallers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInstaller($l);

            if ($this->installersScheduledForDeletion and $this->installersScheduledForDeletion->contains($l)) {
                $this->installersScheduledForDeletion->remove($this->installersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Installer $installer The installer object to add.
     */
    protected function doAddInstaller($installer)
    {
        $this->collInstallers[]= $installer;
        $installer->setPengguna($this);
    }

    /**
     * @param	Installer $installer The installer object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeInstaller($installer)
    {
        if ($this->getInstallers()->contains($installer)) {
            $this->collInstallers->remove($this->collInstallers->search($installer));
            if (null === $this->installersScheduledForDeletion) {
                $this->installersScheduledForDeletion = clone $this->collInstallers;
                $this->installersScheduledForDeletion->clear();
            }
            $this->installersScheduledForDeletion[]= $installer;
            $installer->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collLogLogins collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addLogLogins()
     */
    public function clearLogLogins()
    {
        $this->collLogLogins = null; // important to set this to null since that means it is uninitialized
        $this->collLogLoginsPartial = null;

        return $this;
    }

    /**
     * reset is the collLogLogins collection loaded partially
     *
     * @return void
     */
    public function resetPartialLogLogins($v = true)
    {
        $this->collLogLoginsPartial = $v;
    }

    /**
     * Initializes the collLogLogins collection.
     *
     * By default this just sets the collLogLogins collection to an empty array (like clearcollLogLogins());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogLogins($overrideExisting = true)
    {
        if (null !== $this->collLogLogins && !$overrideExisting) {
            return;
        }
        $this->collLogLogins = new PropelObjectCollection();
        $this->collLogLogins->setModel('LogLogin');
    }

    /**
     * Gets an array of LogLogin objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LogLogin[] List of LogLogin objects
     * @throws PropelException
     */
    public function getLogLogins($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLogLoginsPartial && !$this->isNew();
        if (null === $this->collLogLogins || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogLogins) {
                // return empty collection
                $this->initLogLogins();
            } else {
                $collLogLogins = LogLoginQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLogLoginsPartial && count($collLogLogins)) {
                      $this->initLogLogins(false);

                      foreach ($collLogLogins as $obj) {
                        if (false == $this->collLogLogins->contains($obj)) {
                          $this->collLogLogins->append($obj);
                        }
                      }

                      $this->collLogLoginsPartial = true;
                    }

                    $collLogLogins->getInternalIterator()->rewind();

                    return $collLogLogins;
                }

                if ($partial && $this->collLogLogins) {
                    foreach ($this->collLogLogins as $obj) {
                        if ($obj->isNew()) {
                            $collLogLogins[] = $obj;
                        }
                    }
                }

                $this->collLogLogins = $collLogLogins;
                $this->collLogLoginsPartial = false;
            }
        }

        return $this->collLogLogins;
    }

    /**
     * Sets a collection of LogLogin objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $logLogins A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLogLogins(PropelCollection $logLogins, PropelPDO $con = null)
    {
        $logLoginsToDelete = $this->getLogLogins(new Criteria(), $con)->diff($logLogins);


        $this->logLoginsScheduledForDeletion = $logLoginsToDelete;

        foreach ($logLoginsToDelete as $logLoginRemoved) {
            $logLoginRemoved->setPengguna(null);
        }

        $this->collLogLogins = null;
        foreach ($logLogins as $logLogin) {
            $this->addLogLogin($logLogin);
        }

        $this->collLogLogins = $logLogins;
        $this->collLogLoginsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LogLogin objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LogLogin objects.
     * @throws PropelException
     */
    public function countLogLogins(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLogLoginsPartial && !$this->isNew();
        if (null === $this->collLogLogins || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogLogins) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLogLogins());
            }
            $query = LogLoginQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collLogLogins);
    }

    /**
     * Method called to associate a LogLogin object to this object
     * through the LogLogin foreign key attribute.
     *
     * @param    LogLogin $l LogLogin
     * @return Pengguna The current object (for fluent API support)
     */
    public function addLogLogin(LogLogin $l)
    {
        if ($this->collLogLogins === null) {
            $this->initLogLogins();
            $this->collLogLoginsPartial = true;
        }

        if (!in_array($l, $this->collLogLogins->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLogLogin($l);

            if ($this->logLoginsScheduledForDeletion and $this->logLoginsScheduledForDeletion->contains($l)) {
                $this->logLoginsScheduledForDeletion->remove($this->logLoginsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	LogLogin $logLogin The logLogin object to add.
     */
    protected function doAddLogLogin($logLogin)
    {
        $this->collLogLogins[]= $logLogin;
        $logLogin->setPengguna($this);
    }

    /**
     * @param	LogLogin $logLogin The logLogin object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeLogLogin($logLogin)
    {
        if ($this->getLogLogins()->contains($logLogin)) {
            $this->collLogLogins->remove($this->collLogLogins->search($logLogin));
            if (null === $this->logLoginsScheduledForDeletion) {
                $this->logLoginsScheduledForDeletion = clone $this->collLogLogins;
                $this->logLoginsScheduledForDeletion->clear();
            }
            $this->logLoginsScheduledForDeletion[]= $logLogin;
            $logLogin->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collMediaSosials collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addMediaSosials()
     */
    public function clearMediaSosials()
    {
        $this->collMediaSosials = null; // important to set this to null since that means it is uninitialized
        $this->collMediaSosialsPartial = null;

        return $this;
    }

    /**
     * reset is the collMediaSosials collection loaded partially
     *
     * @return void
     */
    public function resetPartialMediaSosials($v = true)
    {
        $this->collMediaSosialsPartial = $v;
    }

    /**
     * Initializes the collMediaSosials collection.
     *
     * By default this just sets the collMediaSosials collection to an empty array (like clearcollMediaSosials());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMediaSosials($overrideExisting = true)
    {
        if (null !== $this->collMediaSosials && !$overrideExisting) {
            return;
        }
        $this->collMediaSosials = new PropelObjectCollection();
        $this->collMediaSosials->setModel('MediaSosial');
    }

    /**
     * Gets an array of MediaSosial objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|MediaSosial[] List of MediaSosial objects
     * @throws PropelException
     */
    public function getMediaSosials($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collMediaSosialsPartial && !$this->isNew();
        if (null === $this->collMediaSosials || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMediaSosials) {
                // return empty collection
                $this->initMediaSosials();
            } else {
                $collMediaSosials = MediaSosialQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collMediaSosialsPartial && count($collMediaSosials)) {
                      $this->initMediaSosials(false);

                      foreach ($collMediaSosials as $obj) {
                        if (false == $this->collMediaSosials->contains($obj)) {
                          $this->collMediaSosials->append($obj);
                        }
                      }

                      $this->collMediaSosialsPartial = true;
                    }

                    $collMediaSosials->getInternalIterator()->rewind();

                    return $collMediaSosials;
                }

                if ($partial && $this->collMediaSosials) {
                    foreach ($this->collMediaSosials as $obj) {
                        if ($obj->isNew()) {
                            $collMediaSosials[] = $obj;
                        }
                    }
                }

                $this->collMediaSosials = $collMediaSosials;
                $this->collMediaSosialsPartial = false;
            }
        }

        return $this->collMediaSosials;
    }

    /**
     * Sets a collection of MediaSosial objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $mediaSosials A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setMediaSosials(PropelCollection $mediaSosials, PropelPDO $con = null)
    {
        $mediaSosialsToDelete = $this->getMediaSosials(new Criteria(), $con)->diff($mediaSosials);


        $this->mediaSosialsScheduledForDeletion = $mediaSosialsToDelete;

        foreach ($mediaSosialsToDelete as $mediaSosialRemoved) {
            $mediaSosialRemoved->setPengguna(null);
        }

        $this->collMediaSosials = null;
        foreach ($mediaSosials as $mediaSosial) {
            $this->addMediaSosial($mediaSosial);
        }

        $this->collMediaSosials = $mediaSosials;
        $this->collMediaSosialsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MediaSosial objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related MediaSosial objects.
     * @throws PropelException
     */
    public function countMediaSosials(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collMediaSosialsPartial && !$this->isNew();
        if (null === $this->collMediaSosials || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMediaSosials) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMediaSosials());
            }
            $query = MediaSosialQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collMediaSosials);
    }

    /**
     * Method called to associate a MediaSosial object to this object
     * through the MediaSosial foreign key attribute.
     *
     * @param    MediaSosial $l MediaSosial
     * @return Pengguna The current object (for fluent API support)
     */
    public function addMediaSosial(MediaSosial $l)
    {
        if ($this->collMediaSosials === null) {
            $this->initMediaSosials();
            $this->collMediaSosialsPartial = true;
        }

        if (!in_array($l, $this->collMediaSosials->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddMediaSosial($l);

            if ($this->mediaSosialsScheduledForDeletion and $this->mediaSosialsScheduledForDeletion->contains($l)) {
                $this->mediaSosialsScheduledForDeletion->remove($this->mediaSosialsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	MediaSosial $mediaSosial The mediaSosial object to add.
     */
    protected function doAddMediaSosial($mediaSosial)
    {
        $this->collMediaSosials[]= $mediaSosial;
        $mediaSosial->setPengguna($this);
    }

    /**
     * @param	MediaSosial $mediaSosial The mediaSosial object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeMediaSosial($mediaSosial)
    {
        if ($this->getMediaSosials()->contains($mediaSosial)) {
            $this->collMediaSosials->remove($this->collMediaSosials->search($mediaSosial));
            if (null === $this->mediaSosialsScheduledForDeletion) {
                $this->mediaSosialsScheduledForDeletion = clone $this->collMediaSosials;
                $this->mediaSosialsScheduledForDeletion->clear();
            }
            $this->mediaSosialsScheduledForDeletion[]= $mediaSosial;
            $mediaSosial->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collPatchs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addPatchs()
     */
    public function clearPatchs()
    {
        $this->collPatchs = null; // important to set this to null since that means it is uninitialized
        $this->collPatchsPartial = null;

        return $this;
    }

    /**
     * reset is the collPatchs collection loaded partially
     *
     * @return void
     */
    public function resetPartialPatchs($v = true)
    {
        $this->collPatchsPartial = $v;
    }

    /**
     * Initializes the collPatchs collection.
     *
     * By default this just sets the collPatchs collection to an empty array (like clearcollPatchs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPatchs($overrideExisting = true)
    {
        if (null !== $this->collPatchs && !$overrideExisting) {
            return;
        }
        $this->collPatchs = new PropelObjectCollection();
        $this->collPatchs->setModel('Patch');
    }

    /**
     * Gets an array of Patch objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Patch[] List of Patch objects
     * @throws PropelException
     */
    public function getPatchs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPatchsPartial && !$this->isNew();
        if (null === $this->collPatchs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPatchs) {
                // return empty collection
                $this->initPatchs();
            } else {
                $collPatchs = PatchQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPatchsPartial && count($collPatchs)) {
                      $this->initPatchs(false);

                      foreach ($collPatchs as $obj) {
                        if (false == $this->collPatchs->contains($obj)) {
                          $this->collPatchs->append($obj);
                        }
                      }

                      $this->collPatchsPartial = true;
                    }

                    $collPatchs->getInternalIterator()->rewind();

                    return $collPatchs;
                }

                if ($partial && $this->collPatchs) {
                    foreach ($this->collPatchs as $obj) {
                        if ($obj->isNew()) {
                            $collPatchs[] = $obj;
                        }
                    }
                }

                $this->collPatchs = $collPatchs;
                $this->collPatchsPartial = false;
            }
        }

        return $this->collPatchs;
    }

    /**
     * Sets a collection of Patch objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $patchs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPatchs(PropelCollection $patchs, PropelPDO $con = null)
    {
        $patchsToDelete = $this->getPatchs(new Criteria(), $con)->diff($patchs);


        $this->patchsScheduledForDeletion = $patchsToDelete;

        foreach ($patchsToDelete as $patchRemoved) {
            $patchRemoved->setPengguna(null);
        }

        $this->collPatchs = null;
        foreach ($patchs as $patch) {
            $this->addPatch($patch);
        }

        $this->collPatchs = $patchs;
        $this->collPatchsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Patch objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Patch objects.
     * @throws PropelException
     */
    public function countPatchs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPatchsPartial && !$this->isNew();
        if (null === $this->collPatchs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPatchs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPatchs());
            }
            $query = PatchQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collPatchs);
    }

    /**
     * Method called to associate a Patch object to this object
     * through the Patch foreign key attribute.
     *
     * @param    Patch $l Patch
     * @return Pengguna The current object (for fluent API support)
     */
    public function addPatch(Patch $l)
    {
        if ($this->collPatchs === null) {
            $this->initPatchs();
            $this->collPatchsPartial = true;
        }

        if (!in_array($l, $this->collPatchs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPatch($l);

            if ($this->patchsScheduledForDeletion and $this->patchsScheduledForDeletion->contains($l)) {
                $this->patchsScheduledForDeletion->remove($this->patchsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Patch $patch The patch object to add.
     */
    protected function doAddPatch($patch)
    {
        $this->collPatchs[]= $patch;
        $patch->setPengguna($this);
    }

    /**
     * @param	Patch $patch The patch object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removePatch($patch)
    {
        if ($this->getPatchs()->contains($patch)) {
            $this->collPatchs->remove($this->collPatchs->search($patch));
            if (null === $this->patchsScheduledForDeletion) {
                $this->patchsScheduledForDeletion = clone $this->collPatchs;
                $this->patchsScheduledForDeletion->clear();
            }
            $this->patchsScheduledForDeletion[]= $patch;
            $patch->setPengguna(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related Patchs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Patch[] List of Patch objects
     */
    public function getPatchsJoinInstaller($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PatchQuery::create(null, $criteria);
        $query->joinWith('Installer', $join_behavior);

        return $this->getPatchs($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pengguna_id = null;
        $this->jenis_kelamin_id = null;
        $this->nama = null;
        $this->email = null;
        $this->username = null;
        $this->password = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collArtikels) {
                foreach ($this->collArtikels as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBeritas) {
                foreach ($this->collBeritas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDokumentasis) {
                foreach ($this->collDokumentasis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFaqs) {
                foreach ($this->collFaqs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFiles) {
                foreach ($this->collFiles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInstallers) {
                foreach ($this->collInstallers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLogLogins) {
                foreach ($this->collLogLogins as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMediaSosials) {
                foreach ($this->collMediaSosials as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPatchs) {
                foreach ($this->collPatchs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aJenisKelamin instanceof Persistent) {
              $this->aJenisKelamin->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collArtikels instanceof PropelCollection) {
            $this->collArtikels->clearIterator();
        }
        $this->collArtikels = null;
        if ($this->collBeritas instanceof PropelCollection) {
            $this->collBeritas->clearIterator();
        }
        $this->collBeritas = null;
        if ($this->collDokumentasis instanceof PropelCollection) {
            $this->collDokumentasis->clearIterator();
        }
        $this->collDokumentasis = null;
        if ($this->collFaqs instanceof PropelCollection) {
            $this->collFaqs->clearIterator();
        }
        $this->collFaqs = null;
        if ($this->collFiles instanceof PropelCollection) {
            $this->collFiles->clearIterator();
        }
        $this->collFiles = null;
        if ($this->collInstallers instanceof PropelCollection) {
            $this->collInstallers->clearIterator();
        }
        $this->collInstallers = null;
        if ($this->collLogLogins instanceof PropelCollection) {
            $this->collLogLogins->clearIterator();
        }
        $this->collLogLogins = null;
        if ($this->collMediaSosials instanceof PropelCollection) {
            $this->collMediaSosials->clearIterator();
        }
        $this->collMediaSosials = null;
        if ($this->collPatchs instanceof PropelCollection) {
            $this->collPatchs->clearIterator();
        }
        $this->collPatchs = null;
        $this->aJenisKelamin = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PenggunaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
