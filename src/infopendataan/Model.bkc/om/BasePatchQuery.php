<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Installer;
use infopendataan\Model\LogDownloadPatch;
use infopendataan\Model\Patch;
use infopendataan\Model\PatchPeer;
use infopendataan\Model\PatchQuery;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'patch' table.
 *
 *
 *
 * @method PatchQuery orderByPatchId($order = Criteria::ASC) Order by the PATCH_ID column
 * @method PatchQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method PatchQuery orderByInstallerId($order = Criteria::ASC) Order by the INSTALLER_ID column
 * @method PatchQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 * @method PatchQuery orderByFile($order = Criteria::ASC) Order by the FILE column
 * @method PatchQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method PatchQuery orderByVersi($order = Criteria::ASC) Order by the VERSI column
 * @method PatchQuery orderByActive($order = Criteria::ASC) Order by the ACTIVE column
 * @method PatchQuery orderByDeskripsi($order = Criteria::ASC) Order by the DESKRIPSI column
 *
 * @method PatchQuery groupByPatchId() Group by the PATCH_ID column
 * @method PatchQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method PatchQuery groupByInstallerId() Group by the INSTALLER_ID column
 * @method PatchQuery groupByNama() Group by the NAMA column
 * @method PatchQuery groupByFile() Group by the FILE column
 * @method PatchQuery groupByTanggal() Group by the TANGGAL column
 * @method PatchQuery groupByVersi() Group by the VERSI column
 * @method PatchQuery groupByActive() Group by the ACTIVE column
 * @method PatchQuery groupByDeskripsi() Group by the DESKRIPSI column
 *
 * @method PatchQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PatchQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PatchQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PatchQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method PatchQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method PatchQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method PatchQuery leftJoinInstaller($relationAlias = null) Adds a LEFT JOIN clause to the query using the Installer relation
 * @method PatchQuery rightJoinInstaller($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Installer relation
 * @method PatchQuery innerJoinInstaller($relationAlias = null) Adds a INNER JOIN clause to the query using the Installer relation
 *
 * @method PatchQuery leftJoinLogDownloadPatch($relationAlias = null) Adds a LEFT JOIN clause to the query using the LogDownloadPatch relation
 * @method PatchQuery rightJoinLogDownloadPatch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LogDownloadPatch relation
 * @method PatchQuery innerJoinLogDownloadPatch($relationAlias = null) Adds a INNER JOIN clause to the query using the LogDownloadPatch relation
 *
 * @method Patch findOne(PropelPDO $con = null) Return the first Patch matching the query
 * @method Patch findOneOrCreate(PropelPDO $con = null) Return the first Patch matching the query, or a new Patch object populated from the query conditions when no match is found
 *
 * @method Patch findOneByPenggunaId(int $PENGGUNA_ID) Return the first Patch filtered by the PENGGUNA_ID column
 * @method Patch findOneByInstallerId(int $INSTALLER_ID) Return the first Patch filtered by the INSTALLER_ID column
 * @method Patch findOneByNama(string $NAMA) Return the first Patch filtered by the NAMA column
 * @method Patch findOneByFile(string $FILE) Return the first Patch filtered by the FILE column
 * @method Patch findOneByTanggal(string $TANGGAL) Return the first Patch filtered by the TANGGAL column
 * @method Patch findOneByVersi(string $VERSI) Return the first Patch filtered by the VERSI column
 * @method Patch findOneByActive(int $ACTIVE) Return the first Patch filtered by the ACTIVE column
 * @method Patch findOneByDeskripsi(string $DESKRIPSI) Return the first Patch filtered by the DESKRIPSI column
 *
 * @method array findByPatchId(int $PATCH_ID) Return Patch objects filtered by the PATCH_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return Patch objects filtered by the PENGGUNA_ID column
 * @method array findByInstallerId(int $INSTALLER_ID) Return Patch objects filtered by the INSTALLER_ID column
 * @method array findByNama(string $NAMA) Return Patch objects filtered by the NAMA column
 * @method array findByFile(string $FILE) Return Patch objects filtered by the FILE column
 * @method array findByTanggal(string $TANGGAL) Return Patch objects filtered by the TANGGAL column
 * @method array findByVersi(string $VERSI) Return Patch objects filtered by the VERSI column
 * @method array findByActive(int $ACTIVE) Return Patch objects filtered by the ACTIVE column
 * @method array findByDeskripsi(string $DESKRIPSI) Return Patch objects filtered by the DESKRIPSI column
 *
 * @package    propel.generator.infopendataan.om
 */
abstract class BasePatchQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePatchQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'infopendataan';
        }
        if (null === $modelName) {
            $modelName = 'infopendataan\\Model\\Patch';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PatchQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PatchQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PatchQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PatchQuery) {
            return $criteria;
        }
        $query = new PatchQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Patch|Patch[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PatchPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PatchPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Patch A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByPatchId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Patch A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `PATCH_ID`, `PENGGUNA_ID`, `INSTALLER_ID`, `NAMA`, `FILE`, `TANGGAL`, `VERSI`, `ACTIVE`, `DESKRIPSI` FROM `patch` WHERE `PATCH_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Patch();
            $obj->hydrate($row);
            PatchPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Patch|Patch[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Patch[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PatchPeer::PATCH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PatchPeer::PATCH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the PATCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPatchId(1234); // WHERE PATCH_ID = 1234
     * $query->filterByPatchId(array(12, 34)); // WHERE PATCH_ID IN (12, 34)
     * $query->filterByPatchId(array('min' => 12)); // WHERE PATCH_ID >= 12
     * $query->filterByPatchId(array('max' => 12)); // WHERE PATCH_ID <= 12
     * </code>
     *
     * @param     mixed $patchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByPatchId($patchId = null, $comparison = null)
    {
        if (is_array($patchId)) {
            $useMinMax = false;
            if (isset($patchId['min'])) {
                $this->addUsingAlias(PatchPeer::PATCH_ID, $patchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($patchId['max'])) {
                $this->addUsingAlias(PatchPeer::PATCH_ID, $patchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PatchPeer::PATCH_ID, $patchId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(PatchPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(PatchPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PatchPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the INSTALLER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByInstallerId(1234); // WHERE INSTALLER_ID = 1234
     * $query->filterByInstallerId(array(12, 34)); // WHERE INSTALLER_ID IN (12, 34)
     * $query->filterByInstallerId(array('min' => 12)); // WHERE INSTALLER_ID >= 12
     * $query->filterByInstallerId(array('max' => 12)); // WHERE INSTALLER_ID <= 12
     * </code>
     *
     * @see       filterByInstaller()
     *
     * @param     mixed $installerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByInstallerId($installerId = null, $comparison = null)
    {
        if (is_array($installerId)) {
            $useMinMax = false;
            if (isset($installerId['min'])) {
                $this->addUsingAlias(PatchPeer::INSTALLER_ID, $installerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($installerId['max'])) {
                $this->addUsingAlias(PatchPeer::INSTALLER_ID, $installerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PatchPeer::INSTALLER_ID, $installerId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PatchPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the FILE column
     *
     * Example usage:
     * <code>
     * $query->filterByFile('fooValue');   // WHERE FILE = 'fooValue'
     * $query->filterByFile('%fooValue%'); // WHERE FILE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $file The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByFile($file = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($file)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $file)) {
                $file = str_replace('*', '%', $file);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PatchPeer::FILE, $file, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL < '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(PatchPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(PatchPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PatchPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the VERSI column
     *
     * Example usage:
     * <code>
     * $query->filterByVersi('fooValue');   // WHERE VERSI = 'fooValue'
     * $query->filterByVersi('%fooValue%'); // WHERE VERSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByVersi($versi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $versi)) {
                $versi = str_replace('*', '%', $versi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PatchPeer::VERSI, $versi, $comparison);
    }

    /**
     * Filter the query on the ACTIVE column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(1234); // WHERE ACTIVE = 1234
     * $query->filterByActive(array(12, 34)); // WHERE ACTIVE IN (12, 34)
     * $query->filterByActive(array('min' => 12)); // WHERE ACTIVE >= 12
     * $query->filterByActive(array('max' => 12)); // WHERE ACTIVE <= 12
     * </code>
     *
     * @param     mixed $active The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_array($active)) {
            $useMinMax = false;
            if (isset($active['min'])) {
                $this->addUsingAlias(PatchPeer::ACTIVE, $active['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($active['max'])) {
                $this->addUsingAlias(PatchPeer::ACTIVE, $active['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PatchPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the DESKRIPSI column
     *
     * Example usage:
     * <code>
     * $query->filterByDeskripsi('fooValue');   // WHERE DESKRIPSI = 'fooValue'
     * $query->filterByDeskripsi('%fooValue%'); // WHERE DESKRIPSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deskripsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function filterByDeskripsi($deskripsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deskripsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $deskripsi)) {
                $deskripsi = str_replace('*', '%', $deskripsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PatchPeer::DESKRIPSI, $deskripsi, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PatchQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(PatchPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PatchPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Installer object
     *
     * @param   Installer|PropelObjectCollection $installer The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PatchQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInstaller($installer, $comparison = null)
    {
        if ($installer instanceof Installer) {
            return $this
                ->addUsingAlias(PatchPeer::INSTALLER_ID, $installer->getInstallerId(), $comparison);
        } elseif ($installer instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PatchPeer::INSTALLER_ID, $installer->toKeyValue('PrimaryKey', 'InstallerId'), $comparison);
        } else {
            throw new PropelException('filterByInstaller() only accepts arguments of type Installer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Installer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function joinInstaller($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Installer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Installer');
        }

        return $this;
    }

    /**
     * Use the Installer relation Installer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\InstallerQuery A secondary query class using the current class as primary query
     */
    public function useInstallerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinInstaller($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Installer', '\infopendataan\Model\InstallerQuery');
    }

    /**
     * Filter the query by a related LogDownloadPatch object
     *
     * @param   LogDownloadPatch|PropelObjectCollection $logDownloadPatch  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PatchQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLogDownloadPatch($logDownloadPatch, $comparison = null)
    {
        if ($logDownloadPatch instanceof LogDownloadPatch) {
            return $this
                ->addUsingAlias(PatchPeer::PATCH_ID, $logDownloadPatch->getPatchId(), $comparison);
        } elseif ($logDownloadPatch instanceof PropelObjectCollection) {
            return $this
                ->useLogDownloadPatchQuery()
                ->filterByPrimaryKeys($logDownloadPatch->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogDownloadPatch() only accepts arguments of type LogDownloadPatch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LogDownloadPatch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function joinLogDownloadPatch($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LogDownloadPatch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LogDownloadPatch');
        }

        return $this;
    }

    /**
     * Use the LogDownloadPatch relation LogDownloadPatch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\LogDownloadPatchQuery A secondary query class using the current class as primary query
     */
    public function useLogDownloadPatchQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLogDownloadPatch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LogDownloadPatch', '\infopendataan\Model\LogDownloadPatchQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Patch $patch Object to remove from the list of results
     *
     * @return PatchQuery The current query, for fluid interface
     */
    public function prune($patch = null)
    {
        if ($patch) {
            $this->addUsingAlias(PatchPeer::PATCH_ID, $patch->getPatchId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
