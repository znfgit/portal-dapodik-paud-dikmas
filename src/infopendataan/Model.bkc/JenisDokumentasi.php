<?php

namespace infopendataan\Model;

use infopendataan\Model\om\BaseJenisDokumentasi;


/**
 * Skeleton subclass for representing a row from the 'jenis_dokumentasi' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.infopendataan
 */
class JenisDokumentasi extends BaseJenisDokumentasi
{
}
