<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'berita' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.map
 */
class BeritaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.map.BeritaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('berita');
        $this->setPhpName('Berita');
        $this->setClassname('infopendataan\\Model\\Berita');
        $this->setPackage('infopendataan');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('BERITA_ID', 'BeritaId', 'INTEGER', true, null, null);
        $this->addForeignKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', 'pengguna', 'PENGGUNA_ID', false, null, null);
        $this->addColumn('JUDUL', 'Judul', 'VARCHAR', true, 100, null);
        $this->addColumn('ISI', 'Isi', 'VARCHAR', true, 5000, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('GAMBAR', 'Gambar', 'VARCHAR', false, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pengguna', 'infopendataan\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null);
    } // buildRelations()

} // BeritaTableMap
