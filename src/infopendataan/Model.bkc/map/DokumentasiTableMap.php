<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'dokumentasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.map
 */
class DokumentasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.map.DokumentasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dokumentasi');
        $this->setPhpName('Dokumentasi');
        $this->setClassname('infopendataan\\Model\\Dokumentasi');
        $this->setPackage('infopendataan');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('DOKUMENTASI_ID', 'DokumentasiId', 'INTEGER', true, null, null);
        $this->addForeignKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', 'pengguna', 'PENGGUNA_ID', false, null, null);
        $this->addForeignKey('JENIS_DOKUMENTASI_ID', 'JenisDokumentasiId', 'INTEGER', 'jenis_dokumentasi', 'JENIS_DOKUMENTASI_ID', false, null, null);
        $this->addColumn('JUDUL', 'Judul', 'VARCHAR', false, 100, null);
        $this->addColumn('DESKRIPSI', 'Deskripsi', 'VARCHAR', false, 5000, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('GAMBAR', 'Gambar', 'VARCHAR', false, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pengguna', 'infopendataan\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null);
        $this->addRelation('JenisDokumentasi', 'infopendataan\\Model\\JenisDokumentasi', RelationMap::MANY_TO_ONE, array('JENIS_DOKUMENTASI_ID' => 'JENIS_DOKUMENTASI_ID', ), null, 'CASCADE');
    } // buildRelations()

} // DokumentasiTableMap
