<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'media_sosial' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.map
 */
class MediaSosialTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.map.MediaSosialTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('media_sosial');
        $this->setPhpName('MediaSosial');
        $this->setClassname('infopendataan\\Model\\MediaSosial');
        $this->setPackage('infopendataan');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('MEDIA_SOSIAL_ID', 'MediaSosialId', 'INTEGER', true, null, null);
        $this->addForeignKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', 'pengguna', 'PENGGUNA_ID', false, null, null);
        $this->addColumn('NAMA', 'Nama', 'VARCHAR', false, 100, null);
        $this->addColumn('URL', 'Url', 'VARCHAR', false, 100, null);
        $this->addColumn('LOGO', 'Logo', 'VARCHAR', false, 50, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pengguna', 'infopendataan\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null);
    } // buildRelations()

} // MediaSosialTableMap
