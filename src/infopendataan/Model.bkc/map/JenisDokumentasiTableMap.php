<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jenis_dokumentasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.map
 */
class JenisDokumentasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.map.JenisDokumentasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jenis_dokumentasi');
        $this->setPhpName('JenisDokumentasi');
        $this->setClassname('infopendataan\\Model\\JenisDokumentasi');
        $this->setPackage('infopendataan');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('JENIS_DOKUMENTASI_ID', 'JenisDokumentasiId', 'INTEGER', true, null, null);
        $this->addColumn('NAMA', 'Nama', 'VARCHAR', false, 100, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Dokumentasi', 'infopendataan\\Model\\Dokumentasi', RelationMap::ONE_TO_MANY, array('JENIS_DOKUMENTASI_ID' => 'JENIS_DOKUMENTASI_ID', ), null, 'CASCADE', 'Dokumentasis');
    } // buildRelations()

} // JenisDokumentasiTableMap
