<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Artikel;
use infopendataan\Model\Berita;
use infopendataan\Model\Dokumentasi;
use infopendataan\Model\Faq;
use infopendataan\Model\File;
use infopendataan\Model\Installer;
use infopendataan\Model\JenisKelamin;
use infopendataan\Model\LogLogin;
use infopendataan\Model\MediaSosial;
use infopendataan\Model\Patch;
use infopendataan\Model\Pengguna;
use infopendataan\Model\PenggunaPeer;
use infopendataan\Model\PenggunaQuery;

/**
 * Base class that represents a query for the 'pengguna' table.
 *
 * 
 *
 * @method PenggunaQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method PenggunaQuery orderByJenisKelaminId($order = Criteria::ASC) Order by the JENIS_KELAMIN_ID column
 * @method PenggunaQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 * @method PenggunaQuery orderByEmail($order = Criteria::ASC) Order by the EMAIL column
 * @method PenggunaQuery orderByUsername($order = Criteria::ASC) Order by the USERNAME column
 * @method PenggunaQuery orderByPassword($order = Criteria::ASC) Order by the PASSWORD column
 *
 * @method PenggunaQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method PenggunaQuery groupByJenisKelaminId() Group by the JENIS_KELAMIN_ID column
 * @method PenggunaQuery groupByNama() Group by the NAMA column
 * @method PenggunaQuery groupByEmail() Group by the EMAIL column
 * @method PenggunaQuery groupByUsername() Group by the USERNAME column
 * @method PenggunaQuery groupByPassword() Group by the PASSWORD column
 *
 * @method PenggunaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PenggunaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PenggunaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PenggunaQuery leftJoinJenisKelamin($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisKelamin relation
 * @method PenggunaQuery rightJoinJenisKelamin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisKelamin relation
 * @method PenggunaQuery innerJoinJenisKelamin($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisKelamin relation
 *
 * @method PenggunaQuery leftJoinArtikel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Artikel relation
 * @method PenggunaQuery rightJoinArtikel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Artikel relation
 * @method PenggunaQuery innerJoinArtikel($relationAlias = null) Adds a INNER JOIN clause to the query using the Artikel relation
 *
 * @method PenggunaQuery leftJoinBerita($relationAlias = null) Adds a LEFT JOIN clause to the query using the Berita relation
 * @method PenggunaQuery rightJoinBerita($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Berita relation
 * @method PenggunaQuery innerJoinBerita($relationAlias = null) Adds a INNER JOIN clause to the query using the Berita relation
 *
 * @method PenggunaQuery leftJoinDokumentasi($relationAlias = null) Adds a LEFT JOIN clause to the query using the Dokumentasi relation
 * @method PenggunaQuery rightJoinDokumentasi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Dokumentasi relation
 * @method PenggunaQuery innerJoinDokumentasi($relationAlias = null) Adds a INNER JOIN clause to the query using the Dokumentasi relation
 *
 * @method PenggunaQuery leftJoinFaq($relationAlias = null) Adds a LEFT JOIN clause to the query using the Faq relation
 * @method PenggunaQuery rightJoinFaq($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Faq relation
 * @method PenggunaQuery innerJoinFaq($relationAlias = null) Adds a INNER JOIN clause to the query using the Faq relation
 *
 * @method PenggunaQuery leftJoinFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the File relation
 * @method PenggunaQuery rightJoinFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the File relation
 * @method PenggunaQuery innerJoinFile($relationAlias = null) Adds a INNER JOIN clause to the query using the File relation
 *
 * @method PenggunaQuery leftJoinInstaller($relationAlias = null) Adds a LEFT JOIN clause to the query using the Installer relation
 * @method PenggunaQuery rightJoinInstaller($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Installer relation
 * @method PenggunaQuery innerJoinInstaller($relationAlias = null) Adds a INNER JOIN clause to the query using the Installer relation
 *
 * @method PenggunaQuery leftJoinLogLogin($relationAlias = null) Adds a LEFT JOIN clause to the query using the LogLogin relation
 * @method PenggunaQuery rightJoinLogLogin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LogLogin relation
 * @method PenggunaQuery innerJoinLogLogin($relationAlias = null) Adds a INNER JOIN clause to the query using the LogLogin relation
 *
 * @method PenggunaQuery leftJoinMediaSosial($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaSosial relation
 * @method PenggunaQuery rightJoinMediaSosial($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaSosial relation
 * @method PenggunaQuery innerJoinMediaSosial($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaSosial relation
 *
 * @method PenggunaQuery leftJoinPatch($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patch relation
 * @method PenggunaQuery rightJoinPatch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patch relation
 * @method PenggunaQuery innerJoinPatch($relationAlias = null) Adds a INNER JOIN clause to the query using the Patch relation
 *
 * @method Pengguna findOne(PropelPDO $con = null) Return the first Pengguna matching the query
 * @method Pengguna findOneOrCreate(PropelPDO $con = null) Return the first Pengguna matching the query, or a new Pengguna object populated from the query conditions when no match is found
 *
 * @method Pengguna findOneByJenisKelaminId(int $JENIS_KELAMIN_ID) Return the first Pengguna filtered by the JENIS_KELAMIN_ID column
 * @method Pengguna findOneByNama(string $NAMA) Return the first Pengguna filtered by the NAMA column
 * @method Pengguna findOneByEmail(string $EMAIL) Return the first Pengguna filtered by the EMAIL column
 * @method Pengguna findOneByUsername(string $USERNAME) Return the first Pengguna filtered by the USERNAME column
 * @method Pengguna findOneByPassword(string $PASSWORD) Return the first Pengguna filtered by the PASSWORD column
 *
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return Pengguna objects filtered by the PENGGUNA_ID column
 * @method array findByJenisKelaminId(int $JENIS_KELAMIN_ID) Return Pengguna objects filtered by the JENIS_KELAMIN_ID column
 * @method array findByNama(string $NAMA) Return Pengguna objects filtered by the NAMA column
 * @method array findByEmail(string $EMAIL) Return Pengguna objects filtered by the EMAIL column
 * @method array findByUsername(string $USERNAME) Return Pengguna objects filtered by the USERNAME column
 * @method array findByPassword(string $PASSWORD) Return Pengguna objects filtered by the PASSWORD column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BasePenggunaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePenggunaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\Pengguna', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PenggunaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PenggunaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PenggunaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PenggunaQuery) {
            return $criteria;
        }
        $query = new PenggunaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Pengguna|Pengguna[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PenggunaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Pengguna A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByPenggunaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Pengguna A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `PENGGUNA_ID`, `JENIS_KELAMIN_ID`, `NAMA`, `EMAIL`, `USERNAME`, `PASSWORD` FROM `pengguna` WHERE `PENGGUNA_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Pengguna();
            $obj->hydrate($row);
            PenggunaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Pengguna|Pengguna[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Pengguna[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the JENIS_KELAMIN_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisKelaminId(1234); // WHERE JENIS_KELAMIN_ID = 1234
     * $query->filterByJenisKelaminId(array(12, 34)); // WHERE JENIS_KELAMIN_ID IN (12, 34)
     * $query->filterByJenisKelaminId(array('min' => 12)); // WHERE JENIS_KELAMIN_ID >= 12
     * $query->filterByJenisKelaminId(array('max' => 12)); // WHERE JENIS_KELAMIN_ID <= 12
     * </code>
     *
     * @see       filterByJenisKelamin()
     *
     * @param     mixed $jenisKelaminId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByJenisKelaminId($jenisKelaminId = null, $comparison = null)
    {
        if (is_array($jenisKelaminId)) {
            $useMinMax = false;
            if (isset($jenisKelaminId['min'])) {
                $this->addUsingAlias(PenggunaPeer::JENIS_KELAMIN_ID, $jenisKelaminId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisKelaminId['max'])) {
                $this->addUsingAlias(PenggunaPeer::JENIS_KELAMIN_ID, $jenisKelaminId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::JENIS_KELAMIN_ID, $jenisKelaminId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the EMAIL column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE EMAIL = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE EMAIL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the USERNAME column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE USERNAME = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE USERNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the PASSWORD column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE PASSWORD = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE PASSWORD LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PenggunaPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query by a related JenisKelamin object
     *
     * @param   JenisKelamin|PropelObjectCollection $jenisKelamin The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisKelamin($jenisKelamin, $comparison = null)
    {
        if ($jenisKelamin instanceof JenisKelamin) {
            return $this
                ->addUsingAlias(PenggunaPeer::JENIS_KELAMIN_ID, $jenisKelamin->getJenisKelaminId(), $comparison);
        } elseif ($jenisKelamin instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaPeer::JENIS_KELAMIN_ID, $jenisKelamin->toKeyValue('PrimaryKey', 'JenisKelaminId'), $comparison);
        } else {
            throw new PropelException('filterByJenisKelamin() only accepts arguments of type JenisKelamin or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisKelamin relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinJenisKelamin($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisKelamin');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisKelamin');
        }

        return $this;
    }

    /**
     * Use the JenisKelamin relation JenisKelamin object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\JenisKelaminQuery A secondary query class using the current class as primary query
     */
    public function useJenisKelaminQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisKelamin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisKelamin', '\infopendataan\Model\JenisKelaminQuery');
    }

    /**
     * Filter the query by a related Artikel object
     *
     * @param   Artikel|PropelObjectCollection $artikel  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByArtikel($artikel, $comparison = null)
    {
        if ($artikel instanceof Artikel) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $artikel->getPenggunaId(), $comparison);
        } elseif ($artikel instanceof PropelObjectCollection) {
            return $this
                ->useArtikelQuery()
                ->filterByPrimaryKeys($artikel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByArtikel() only accepts arguments of type Artikel or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Artikel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinArtikel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Artikel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Artikel');
        }

        return $this;
    }

    /**
     * Use the Artikel relation Artikel object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\ArtikelQuery A secondary query class using the current class as primary query
     */
    public function useArtikelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinArtikel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Artikel', '\infopendataan\Model\ArtikelQuery');
    }

    /**
     * Filter the query by a related Berita object
     *
     * @param   Berita|PropelObjectCollection $berita  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBerita($berita, $comparison = null)
    {
        if ($berita instanceof Berita) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $berita->getPenggunaId(), $comparison);
        } elseif ($berita instanceof PropelObjectCollection) {
            return $this
                ->useBeritaQuery()
                ->filterByPrimaryKeys($berita->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBerita() only accepts arguments of type Berita or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Berita relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinBerita($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Berita');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Berita');
        }

        return $this;
    }

    /**
     * Use the Berita relation Berita object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\BeritaQuery A secondary query class using the current class as primary query
     */
    public function useBeritaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBerita($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Berita', '\infopendataan\Model\BeritaQuery');
    }

    /**
     * Filter the query by a related Dokumentasi object
     *
     * @param   Dokumentasi|PropelObjectCollection $dokumentasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDokumentasi($dokumentasi, $comparison = null)
    {
        if ($dokumentasi instanceof Dokumentasi) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $dokumentasi->getPenggunaId(), $comparison);
        } elseif ($dokumentasi instanceof PropelObjectCollection) {
            return $this
                ->useDokumentasiQuery()
                ->filterByPrimaryKeys($dokumentasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDokumentasi() only accepts arguments of type Dokumentasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Dokumentasi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinDokumentasi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Dokumentasi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Dokumentasi');
        }

        return $this;
    }

    /**
     * Use the Dokumentasi relation Dokumentasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\DokumentasiQuery A secondary query class using the current class as primary query
     */
    public function useDokumentasiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDokumentasi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Dokumentasi', '\infopendataan\Model\DokumentasiQuery');
    }

    /**
     * Filter the query by a related Faq object
     *
     * @param   Faq|PropelObjectCollection $faq  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFaq($faq, $comparison = null)
    {
        if ($faq instanceof Faq) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $faq->getPenggunaId(), $comparison);
        } elseif ($faq instanceof PropelObjectCollection) {
            return $this
                ->useFaqQuery()
                ->filterByPrimaryKeys($faq->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFaq() only accepts arguments of type Faq or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Faq relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinFaq($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Faq');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Faq');
        }

        return $this;
    }

    /**
     * Use the Faq relation Faq object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\FaqQuery A secondary query class using the current class as primary query
     */
    public function useFaqQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFaq($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Faq', '\infopendataan\Model\FaqQuery');
    }

    /**
     * Filter the query by a related File object
     *
     * @param   File|PropelObjectCollection $file  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFile($file, $comparison = null)
    {
        if ($file instanceof File) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $file->getPenggunaId(), $comparison);
        } elseif ($file instanceof PropelObjectCollection) {
            return $this
                ->useFileQuery()
                ->filterByPrimaryKeys($file->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFile() only accepts arguments of type File or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the File relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinFile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('File');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'File');
        }

        return $this;
    }

    /**
     * Use the File relation File object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\FileQuery A secondary query class using the current class as primary query
     */
    public function useFileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'File', '\infopendataan\Model\FileQuery');
    }

    /**
     * Filter the query by a related Installer object
     *
     * @param   Installer|PropelObjectCollection $installer  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInstaller($installer, $comparison = null)
    {
        if ($installer instanceof Installer) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $installer->getPenggunaId(), $comparison);
        } elseif ($installer instanceof PropelObjectCollection) {
            return $this
                ->useInstallerQuery()
                ->filterByPrimaryKeys($installer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInstaller() only accepts arguments of type Installer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Installer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinInstaller($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Installer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Installer');
        }

        return $this;
    }

    /**
     * Use the Installer relation Installer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\InstallerQuery A secondary query class using the current class as primary query
     */
    public function useInstallerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinInstaller($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Installer', '\infopendataan\Model\InstallerQuery');
    }

    /**
     * Filter the query by a related LogLogin object
     *
     * @param   LogLogin|PropelObjectCollection $logLogin  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLogLogin($logLogin, $comparison = null)
    {
        if ($logLogin instanceof LogLogin) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $logLogin->getPenggunaId(), $comparison);
        } elseif ($logLogin instanceof PropelObjectCollection) {
            return $this
                ->useLogLoginQuery()
                ->filterByPrimaryKeys($logLogin->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogLogin() only accepts arguments of type LogLogin or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LogLogin relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinLogLogin($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LogLogin');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LogLogin');
        }

        return $this;
    }

    /**
     * Use the LogLogin relation LogLogin object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\LogLoginQuery A secondary query class using the current class as primary query
     */
    public function useLogLoginQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLogLogin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LogLogin', '\infopendataan\Model\LogLoginQuery');
    }

    /**
     * Filter the query by a related MediaSosial object
     *
     * @param   MediaSosial|PropelObjectCollection $mediaSosial  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMediaSosial($mediaSosial, $comparison = null)
    {
        if ($mediaSosial instanceof MediaSosial) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $mediaSosial->getPenggunaId(), $comparison);
        } elseif ($mediaSosial instanceof PropelObjectCollection) {
            return $this
                ->useMediaSosialQuery()
                ->filterByPrimaryKeys($mediaSosial->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMediaSosial() only accepts arguments of type MediaSosial or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaSosial relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinMediaSosial($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaSosial');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaSosial');
        }

        return $this;
    }

    /**
     * Use the MediaSosial relation MediaSosial object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\MediaSosialQuery A secondary query class using the current class as primary query
     */
    public function useMediaSosialQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaSosial($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaSosial', '\infopendataan\Model\MediaSosialQuery');
    }

    /**
     * Filter the query by a related Patch object
     *
     * @param   Patch|PropelObjectCollection $patch  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PenggunaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPatch($patch, $comparison = null)
    {
        if ($patch instanceof Patch) {
            return $this
                ->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $patch->getPenggunaId(), $comparison);
        } elseif ($patch instanceof PropelObjectCollection) {
            return $this
                ->usePatchQuery()
                ->filterByPrimaryKeys($patch->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPatch() only accepts arguments of type Patch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function joinPatch($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patch');
        }

        return $this;
    }

    /**
     * Use the Patch relation Patch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PatchQuery A secondary query class using the current class as primary query
     */
    public function usePatchQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPatch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patch', '\infopendataan\Model\PatchQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Pengguna $pengguna Object to remove from the list of results
     *
     * @return PenggunaQuery The current query, for fluid interface
     */
    public function prune($pengguna = null)
    {
        if ($pengguna) {
            $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
