<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Feedback;
use infopendataan\Model\JenisFeedback;
use infopendataan\Model\JenisFeedbackPeer;
use infopendataan\Model\JenisFeedbackQuery;

/**
 * Base class that represents a query for the 'jenis_feedback' table.
 *
 * 
 *
 * @method JenisFeedbackQuery orderByJenisFeedbackId($order = Criteria::ASC) Order by the JENIS_FEEDBACK_ID column
 * @method JenisFeedbackQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 *
 * @method JenisFeedbackQuery groupByJenisFeedbackId() Group by the JENIS_FEEDBACK_ID column
 * @method JenisFeedbackQuery groupByNama() Group by the NAMA column
 *
 * @method JenisFeedbackQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JenisFeedbackQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JenisFeedbackQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JenisFeedbackQuery leftJoinFeedback($relationAlias = null) Adds a LEFT JOIN clause to the query using the Feedback relation
 * @method JenisFeedbackQuery rightJoinFeedback($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Feedback relation
 * @method JenisFeedbackQuery innerJoinFeedback($relationAlias = null) Adds a INNER JOIN clause to the query using the Feedback relation
 *
 * @method JenisFeedback findOne(PropelPDO $con = null) Return the first JenisFeedback matching the query
 * @method JenisFeedback findOneOrCreate(PropelPDO $con = null) Return the first JenisFeedback matching the query, or a new JenisFeedback object populated from the query conditions when no match is found
 *
 * @method JenisFeedback findOneByNama(string $NAMA) Return the first JenisFeedback filtered by the NAMA column
 *
 * @method array findByJenisFeedbackId(int $JENIS_FEEDBACK_ID) Return JenisFeedback objects filtered by the JENIS_FEEDBACK_ID column
 * @method array findByNama(string $NAMA) Return JenisFeedback objects filtered by the NAMA column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJenisFeedbackQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJenisFeedbackQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JenisFeedback', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JenisFeedbackQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JenisFeedbackQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JenisFeedbackQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JenisFeedbackQuery) {
            return $criteria;
        }
        $query = new JenisFeedbackQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JenisFeedback|JenisFeedback[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JenisFeedbackPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JenisFeedbackPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisFeedback A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJenisFeedbackId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisFeedback A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `JENIS_FEEDBACK_ID`, `NAMA` FROM `jenis_feedback` WHERE `JENIS_FEEDBACK_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JenisFeedback();
            $obj->hydrate($row);
            JenisFeedbackPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JenisFeedback|JenisFeedback[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JenisFeedback[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JenisFeedbackQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JenisFeedbackQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the JENIS_FEEDBACK_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisFeedbackId(1234); // WHERE JENIS_FEEDBACK_ID = 1234
     * $query->filterByJenisFeedbackId(array(12, 34)); // WHERE JENIS_FEEDBACK_ID IN (12, 34)
     * $query->filterByJenisFeedbackId(array('min' => 12)); // WHERE JENIS_FEEDBACK_ID >= 12
     * $query->filterByJenisFeedbackId(array('max' => 12)); // WHERE JENIS_FEEDBACK_ID <= 12
     * </code>
     *
     * @param     mixed $jenisFeedbackId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisFeedbackQuery The current query, for fluid interface
     */
    public function filterByJenisFeedbackId($jenisFeedbackId = null, $comparison = null)
    {
        if (is_array($jenisFeedbackId)) {
            $useMinMax = false;
            if (isset($jenisFeedbackId['min'])) {
                $this->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedbackId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisFeedbackId['max'])) {
                $this->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedbackId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedbackId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisFeedbackQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisFeedbackPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query by a related Feedback object
     *
     * @param   Feedback|PropelObjectCollection $feedback  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JenisFeedbackQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFeedback($feedback, $comparison = null)
    {
        if ($feedback instanceof Feedback) {
            return $this
                ->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $feedback->getJenisFeedbackId(), $comparison);
        } elseif ($feedback instanceof PropelObjectCollection) {
            return $this
                ->useFeedbackQuery()
                ->filterByPrimaryKeys($feedback->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFeedback() only accepts arguments of type Feedback or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Feedback relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JenisFeedbackQuery The current query, for fluid interface
     */
    public function joinFeedback($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Feedback');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Feedback');
        }

        return $this;
    }

    /**
     * Use the Feedback relation Feedback object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\FeedbackQuery A secondary query class using the current class as primary query
     */
    public function useFeedbackQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFeedback($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Feedback', '\infopendataan\Model\FeedbackQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JenisFeedback $jenisFeedback Object to remove from the list of results
     *
     * @return JenisFeedbackQuery The current query, for fluid interface
     */
    public function prune($jenisFeedback = null)
    {
        if ($jenisFeedback) {
            $this->addUsingAlias(JenisFeedbackPeer::JENIS_FEEDBACK_ID, $jenisFeedback->getJenisFeedbackId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
