<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\LogLogin;
use infopendataan\Model\LogLoginPeer;
use infopendataan\Model\LogLoginQuery;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'log_login' table.
 *
 * 
 *
 * @method LogLoginQuery orderByLogLoginId($order = Criteria::ASC) Order by the LOG_LOGIN_ID column
 * @method LogLoginQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method LogLoginQuery orderByIp($order = Criteria::ASC) Order by the IP column
 * @method LogLoginQuery orderByBrowser($order = Criteria::ASC) Order by the BROWSER column
 * @method LogLoginQuery orderByOs($order = Criteria::ASC) Order by the OS column
 * @method LogLoginQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 *
 * @method LogLoginQuery groupByLogLoginId() Group by the LOG_LOGIN_ID column
 * @method LogLoginQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method LogLoginQuery groupByIp() Group by the IP column
 * @method LogLoginQuery groupByBrowser() Group by the BROWSER column
 * @method LogLoginQuery groupByOs() Group by the OS column
 * @method LogLoginQuery groupByTanggal() Group by the TANGGAL column
 *
 * @method LogLoginQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogLoginQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogLoginQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogLoginQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method LogLoginQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method LogLoginQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method LogLogin findOne(PropelPDO $con = null) Return the first LogLogin matching the query
 * @method LogLogin findOneOrCreate(PropelPDO $con = null) Return the first LogLogin matching the query, or a new LogLogin object populated from the query conditions when no match is found
 *
 * @method LogLogin findOneByPenggunaId(int $PENGGUNA_ID) Return the first LogLogin filtered by the PENGGUNA_ID column
 * @method LogLogin findOneByIp(string $IP) Return the first LogLogin filtered by the IP column
 * @method LogLogin findOneByBrowser(string $BROWSER) Return the first LogLogin filtered by the BROWSER column
 * @method LogLogin findOneByOs(string $OS) Return the first LogLogin filtered by the OS column
 * @method LogLogin findOneByTanggal(string $TANGGAL) Return the first LogLogin filtered by the TANGGAL column
 *
 * @method array findByLogLoginId(int $LOG_LOGIN_ID) Return LogLogin objects filtered by the LOG_LOGIN_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return LogLogin objects filtered by the PENGGUNA_ID column
 * @method array findByIp(string $IP) Return LogLogin objects filtered by the IP column
 * @method array findByBrowser(string $BROWSER) Return LogLogin objects filtered by the BROWSER column
 * @method array findByOs(string $OS) Return LogLogin objects filtered by the OS column
 * @method array findByTanggal(string $TANGGAL) Return LogLogin objects filtered by the TANGGAL column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseLogLoginQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogLoginQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\LogLogin', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogLoginQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogLoginQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogLoginQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogLoginQuery) {
            return $criteria;
        }
        $query = new LogLoginQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogLogin|LogLogin[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogLoginPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogLoginPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogLogin A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLogLoginId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogLogin A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `LOG_LOGIN_ID`, `PENGGUNA_ID`, `IP`, `BROWSER`, `OS`, `TANGGAL` FROM `log_login` WHERE `LOG_LOGIN_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogLogin();
            $obj->hydrate($row);
            LogLoginPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogLogin|LogLogin[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogLogin[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogLoginPeer::LOG_LOGIN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogLoginPeer::LOG_LOGIN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LOG_LOGIN_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByLogLoginId(1234); // WHERE LOG_LOGIN_ID = 1234
     * $query->filterByLogLoginId(array(12, 34)); // WHERE LOG_LOGIN_ID IN (12, 34)
     * $query->filterByLogLoginId(array('min' => 12)); // WHERE LOG_LOGIN_ID >= 12
     * $query->filterByLogLoginId(array('max' => 12)); // WHERE LOG_LOGIN_ID <= 12
     * </code>
     *
     * @param     mixed $logLoginId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByLogLoginId($logLoginId = null, $comparison = null)
    {
        if (is_array($logLoginId)) {
            $useMinMax = false;
            if (isset($logLoginId['min'])) {
                $this->addUsingAlias(LogLoginPeer::LOG_LOGIN_ID, $logLoginId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logLoginId['max'])) {
                $this->addUsingAlias(LogLoginPeer::LOG_LOGIN_ID, $logLoginId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogLoginPeer::LOG_LOGIN_ID, $logLoginId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(LogLoginPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(LogLoginPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogLoginPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the IP column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE IP = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE IP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the BROWSER column
     *
     * Example usage:
     * <code>
     * $query->filterByBrowser('fooValue');   // WHERE BROWSER = 'fooValue'
     * $query->filterByBrowser('%fooValue%'); // WHERE BROWSER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $browser The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByBrowser($browser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($browser)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $browser)) {
                $browser = str_replace('*', '%', $browser);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginPeer::BROWSER, $browser, $comparison);
    }

    /**
     * Filter the query on the OS column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE OS = 'fooValue'
     * $query->filterByOs('%fooValue%'); // WHERE OS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $os)) {
                $os = str_replace('*', '%', $os);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogLoginPeer::OS, $os, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(LogLoginPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(LogLoginPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogLoginPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LogLoginQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(LogLoginPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LogLoginPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   LogLogin $logLogin Object to remove from the list of results
     *
     * @return LogLoginQuery The current query, for fluid interface
     */
    public function prune($logLogin = null)
    {
        if ($logLogin) {
            $this->addUsingAlias(LogLoginPeer::LOG_LOGIN_ID, $logLogin->getLogLoginId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
