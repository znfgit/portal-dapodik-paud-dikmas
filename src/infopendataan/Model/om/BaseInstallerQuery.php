<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Installer;
use infopendataan\Model\InstallerPeer;
use infopendataan\Model\InstallerQuery;
use infopendataan\Model\LogDownloadInstaller;
use infopendataan\Model\Patch;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'installer' table.
 *
 * 
 *
 * @method InstallerQuery orderByInstallerId($order = Criteria::ASC) Order by the INSTALLER_ID column
 * @method InstallerQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method InstallerQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 * @method InstallerQuery orderByFile($order = Criteria::ASC) Order by the FILE column
 * @method InstallerQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method InstallerQuery orderByVersi($order = Criteria::ASC) Order by the VERSI column
 * @method InstallerQuery orderByActive($order = Criteria::ASC) Order by the ACTIVE column
 * @method InstallerQuery orderByDeskripsi($order = Criteria::ASC) Order by the DESKRIPSI column
 *
 * @method InstallerQuery groupByInstallerId() Group by the INSTALLER_ID column
 * @method InstallerQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method InstallerQuery groupByNama() Group by the NAMA column
 * @method InstallerQuery groupByFile() Group by the FILE column
 * @method InstallerQuery groupByTanggal() Group by the TANGGAL column
 * @method InstallerQuery groupByVersi() Group by the VERSI column
 * @method InstallerQuery groupByActive() Group by the ACTIVE column
 * @method InstallerQuery groupByDeskripsi() Group by the DESKRIPSI column
 *
 * @method InstallerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method InstallerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method InstallerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method InstallerQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method InstallerQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method InstallerQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method InstallerQuery leftJoinLogDownloadInstaller($relationAlias = null) Adds a LEFT JOIN clause to the query using the LogDownloadInstaller relation
 * @method InstallerQuery rightJoinLogDownloadInstaller($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LogDownloadInstaller relation
 * @method InstallerQuery innerJoinLogDownloadInstaller($relationAlias = null) Adds a INNER JOIN clause to the query using the LogDownloadInstaller relation
 *
 * @method InstallerQuery leftJoinPatch($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patch relation
 * @method InstallerQuery rightJoinPatch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patch relation
 * @method InstallerQuery innerJoinPatch($relationAlias = null) Adds a INNER JOIN clause to the query using the Patch relation
 *
 * @method Installer findOne(PropelPDO $con = null) Return the first Installer matching the query
 * @method Installer findOneOrCreate(PropelPDO $con = null) Return the first Installer matching the query, or a new Installer object populated from the query conditions when no match is found
 *
 * @method Installer findOneByPenggunaId(int $PENGGUNA_ID) Return the first Installer filtered by the PENGGUNA_ID column
 * @method Installer findOneByNama(string $NAMA) Return the first Installer filtered by the NAMA column
 * @method Installer findOneByFile(string $FILE) Return the first Installer filtered by the FILE column
 * @method Installer findOneByTanggal(string $TANGGAL) Return the first Installer filtered by the TANGGAL column
 * @method Installer findOneByVersi(string $VERSI) Return the first Installer filtered by the VERSI column
 * @method Installer findOneByActive(int $ACTIVE) Return the first Installer filtered by the ACTIVE column
 * @method Installer findOneByDeskripsi(string $DESKRIPSI) Return the first Installer filtered by the DESKRIPSI column
 *
 * @method array findByInstallerId(int $INSTALLER_ID) Return Installer objects filtered by the INSTALLER_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return Installer objects filtered by the PENGGUNA_ID column
 * @method array findByNama(string $NAMA) Return Installer objects filtered by the NAMA column
 * @method array findByFile(string $FILE) Return Installer objects filtered by the FILE column
 * @method array findByTanggal(string $TANGGAL) Return Installer objects filtered by the TANGGAL column
 * @method array findByVersi(string $VERSI) Return Installer objects filtered by the VERSI column
 * @method array findByActive(int $ACTIVE) Return Installer objects filtered by the ACTIVE column
 * @method array findByDeskripsi(string $DESKRIPSI) Return Installer objects filtered by the DESKRIPSI column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseInstallerQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseInstallerQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\Installer', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new InstallerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   InstallerQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return InstallerQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof InstallerQuery) {
            return $criteria;
        }
        $query = new InstallerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Installer|Installer[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = InstallerPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(InstallerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Installer A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByInstallerId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Installer A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `INSTALLER_ID`, `PENGGUNA_ID`, `NAMA`, `FILE`, `TANGGAL`, `VERSI`, `ACTIVE`, `DESKRIPSI` FROM `installer` WHERE `INSTALLER_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Installer();
            $obj->hydrate($row);
            InstallerPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Installer|Installer[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Installer[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InstallerPeer::INSTALLER_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InstallerPeer::INSTALLER_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the INSTALLER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByInstallerId(1234); // WHERE INSTALLER_ID = 1234
     * $query->filterByInstallerId(array(12, 34)); // WHERE INSTALLER_ID IN (12, 34)
     * $query->filterByInstallerId(array('min' => 12)); // WHERE INSTALLER_ID >= 12
     * $query->filterByInstallerId(array('max' => 12)); // WHERE INSTALLER_ID <= 12
     * </code>
     *
     * @param     mixed $installerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByInstallerId($installerId = null, $comparison = null)
    {
        if (is_array($installerId)) {
            $useMinMax = false;
            if (isset($installerId['min'])) {
                $this->addUsingAlias(InstallerPeer::INSTALLER_ID, $installerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($installerId['max'])) {
                $this->addUsingAlias(InstallerPeer::INSTALLER_ID, $installerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InstallerPeer::INSTALLER_ID, $installerId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(InstallerPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(InstallerPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InstallerPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstallerPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the FILE column
     *
     * Example usage:
     * <code>
     * $query->filterByFile('fooValue');   // WHERE FILE = 'fooValue'
     * $query->filterByFile('%fooValue%'); // WHERE FILE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $file The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByFile($file = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($file)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $file)) {
                $file = str_replace('*', '%', $file);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstallerPeer::FILE, $file, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(InstallerPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(InstallerPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InstallerPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the VERSI column
     *
     * Example usage:
     * <code>
     * $query->filterByVersi('fooValue');   // WHERE VERSI = 'fooValue'
     * $query->filterByVersi('%fooValue%'); // WHERE VERSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByVersi($versi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $versi)) {
                $versi = str_replace('*', '%', $versi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstallerPeer::VERSI, $versi, $comparison);
    }

    /**
     * Filter the query on the ACTIVE column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(1234); // WHERE ACTIVE = 1234
     * $query->filterByActive(array(12, 34)); // WHERE ACTIVE IN (12, 34)
     * $query->filterByActive(array('min' => 12)); // WHERE ACTIVE >= 12
     * $query->filterByActive(array('max' => 12)); // WHERE ACTIVE <= 12
     * </code>
     *
     * @param     mixed $active The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_array($active)) {
            $useMinMax = false;
            if (isset($active['min'])) {
                $this->addUsingAlias(InstallerPeer::ACTIVE, $active['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($active['max'])) {
                $this->addUsingAlias(InstallerPeer::ACTIVE, $active['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InstallerPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the DESKRIPSI column
     *
     * Example usage:
     * <code>
     * $query->filterByDeskripsi('fooValue');   // WHERE DESKRIPSI = 'fooValue'
     * $query->filterByDeskripsi('%fooValue%'); // WHERE DESKRIPSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deskripsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function filterByDeskripsi($deskripsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deskripsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $deskripsi)) {
                $deskripsi = str_replace('*', '%', $deskripsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstallerPeer::DESKRIPSI, $deskripsi, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InstallerQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(InstallerPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InstallerPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related LogDownloadInstaller object
     *
     * @param   LogDownloadInstaller|PropelObjectCollection $logDownloadInstaller  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InstallerQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLogDownloadInstaller($logDownloadInstaller, $comparison = null)
    {
        if ($logDownloadInstaller instanceof LogDownloadInstaller) {
            return $this
                ->addUsingAlias(InstallerPeer::INSTALLER_ID, $logDownloadInstaller->getInstallerId(), $comparison);
        } elseif ($logDownloadInstaller instanceof PropelObjectCollection) {
            return $this
                ->useLogDownloadInstallerQuery()
                ->filterByPrimaryKeys($logDownloadInstaller->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogDownloadInstaller() only accepts arguments of type LogDownloadInstaller or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LogDownloadInstaller relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function joinLogDownloadInstaller($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LogDownloadInstaller');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LogDownloadInstaller');
        }

        return $this;
    }

    /**
     * Use the LogDownloadInstaller relation LogDownloadInstaller object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\LogDownloadInstallerQuery A secondary query class using the current class as primary query
     */
    public function useLogDownloadInstallerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLogDownloadInstaller($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LogDownloadInstaller', '\infopendataan\Model\LogDownloadInstallerQuery');
    }

    /**
     * Filter the query by a related Patch object
     *
     * @param   Patch|PropelObjectCollection $patch  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InstallerQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPatch($patch, $comparison = null)
    {
        if ($patch instanceof Patch) {
            return $this
                ->addUsingAlias(InstallerPeer::INSTALLER_ID, $patch->getInstallerId(), $comparison);
        } elseif ($patch instanceof PropelObjectCollection) {
            return $this
                ->usePatchQuery()
                ->filterByPrimaryKeys($patch->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPatch() only accepts arguments of type Patch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function joinPatch($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patch');
        }

        return $this;
    }

    /**
     * Use the Patch relation Patch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PatchQuery A secondary query class using the current class as primary query
     */
    public function usePatchQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPatch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patch', '\infopendataan\Model\PatchQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Installer $installer Object to remove from the list of results
     *
     * @return InstallerQuery The current query, for fluid interface
     */
    public function prune($installer = null)
    {
        if ($installer) {
            $this->addUsingAlias(InstallerPeer::INSTALLER_ID, $installer->getInstallerId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
