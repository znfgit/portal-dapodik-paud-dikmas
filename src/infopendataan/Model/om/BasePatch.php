<?php

namespace infopendataan\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Installer;
use infopendataan\Model\InstallerQuery;
use infopendataan\Model\LogDownloadPatch;
use infopendataan\Model\LogDownloadPatchQuery;
use infopendataan\Model\Patch;
use infopendataan\Model\PatchPeer;
use infopendataan\Model\PatchQuery;
use infopendataan\Model\Pengguna;
use infopendataan\Model\PenggunaQuery;

/**
 * Base class that represents a row from the 'patch' table.
 *
 * 
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BasePatch extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'infopendataan\\Model\\PatchPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PatchPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the patch_id field.
     * @var        int
     */
    protected $patch_id;

    /**
     * The value for the pengguna_id field.
     * @var        int
     */
    protected $pengguna_id;

    /**
     * The value for the installer_id field.
     * @var        int
     */
    protected $installer_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the file field.
     * @var        string
     */
    protected $file;

    /**
     * The value for the tanggal field.
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        string
     */
    protected $tanggal;

    /**
     * The value for the versi field.
     * @var        string
     */
    protected $versi;

    /**
     * The value for the active field.
     * @var        int
     */
    protected $active;

    /**
     * The value for the deskripsi field.
     * @var        string
     */
    protected $deskripsi;

    /**
     * @var        Pengguna
     */
    protected $aPengguna;

    /**
     * @var        Installer
     */
    protected $aInstaller;

    /**
     * @var        PropelObjectCollection|LogDownloadPatch[] Collection to store aggregation of LogDownloadPatch objects.
     */
    protected $collLogDownloadPatchs;
    protected $collLogDownloadPatchsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $logDownloadPatchsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
    }

    /**
     * Initializes internal state of BasePatch object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [patch_id] column value.
     * 
     * @return int
     */
    public function getPatchId()
    {
        return $this->patch_id;
    }

    /**
     * Get the [pengguna_id] column value.
     * 
     * @return int
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Get the [installer_id] column value.
     * 
     * @return int
     */
    public function getInstallerId()
    {
        return $this->installer_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [file] column value.
     * 
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get the [optionally formatted] temporal [tanggal] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTanggal($format = 'Y-m-d H:i:s')
    {
        if ($this->tanggal === null) {
            return null;
        }

        if ($this->tanggal === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->tanggal);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->tanggal, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [versi] column value.
     * 
     * @return string
     */
    public function getVersi()
    {
        return $this->versi;
    }

    /**
     * Get the [active] column value.
     * 
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get the [deskripsi] column value.
     * 
     * @return string
     */
    public function getDeskripsi()
    {
        return $this->deskripsi;
    }

    /**
     * Set the value of [patch_id] column.
     * 
     * @param int $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setPatchId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->patch_id !== $v) {
            $this->patch_id = $v;
            $this->modifiedColumns[] = PatchPeer::PATCH_ID;
        }


        return $this;
    } // setPatchId()

    /**
     * Set the value of [pengguna_id] column.
     * 
     * @param int $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = PatchPeer::PENGGUNA_ID;
        }

        if ($this->aPengguna !== null && $this->aPengguna->getPenggunaId() !== $v) {
            $this->aPengguna = null;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [installer_id] column.
     * 
     * @param int $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setInstallerId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->installer_id !== $v) {
            $this->installer_id = $v;
            $this->modifiedColumns[] = PatchPeer::INSTALLER_ID;
        }

        if ($this->aInstaller !== null && $this->aInstaller->getInstallerId() !== $v) {
            $this->aInstaller = null;
        }


        return $this;
    } // setInstallerId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PatchPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [file] column.
     * 
     * @param string $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setFile($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->file !== $v) {
            $this->file = $v;
            $this->modifiedColumns[] = PatchPeer::FILE;
        }


        return $this;
    } // setFile()

    /**
     * Sets the value of [tanggal] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Patch The current object (for fluent API support)
     */
    public function setTanggal($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tanggal !== null || $dt !== null) {
            $currentDateAsString = ($this->tanggal !== null && $tmpDt = new DateTime($this->tanggal)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->tanggal = $newDateAsString;
                $this->modifiedColumns[] = PatchPeer::TANGGAL;
            }
        } // if either are not null


        return $this;
    } // setTanggal()

    /**
     * Set the value of [versi] column.
     * 
     * @param string $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setVersi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->versi !== $v) {
            $this->versi = $v;
            $this->modifiedColumns[] = PatchPeer::VERSI;
        }


        return $this;
    } // setVersi()

    /**
     * Set the value of [active] column.
     * 
     * @param int $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[] = PatchPeer::ACTIVE;
        }


        return $this;
    } // setActive()

    /**
     * Set the value of [deskripsi] column.
     * 
     * @param string $v new value
     * @return Patch The current object (for fluent API support)
     */
    public function setDeskripsi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->deskripsi !== $v) {
            $this->deskripsi = $v;
            $this->modifiedColumns[] = PatchPeer::DESKRIPSI;
        }


        return $this;
    } // setDeskripsi()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->patch_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->pengguna_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->installer_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->nama = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->file = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tanggal = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->versi = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->active = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->deskripsi = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 9; // 9 = PatchPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Patch object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPengguna !== null && $this->pengguna_id !== $this->aPengguna->getPenggunaId()) {
            $this->aPengguna = null;
        }
        if ($this->aInstaller !== null && $this->installer_id !== $this->aInstaller->getInstallerId()) {
            $this->aInstaller = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PatchPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PatchPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPengguna = null;
            $this->aInstaller = null;
            $this->collLogDownloadPatchs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PatchPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PatchQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PatchPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PatchPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPengguna !== null) {
                if ($this->aPengguna->isModified() || $this->aPengguna->isNew()) {
                    $affectedRows += $this->aPengguna->save($con);
                }
                $this->setPengguna($this->aPengguna);
            }

            if ($this->aInstaller !== null) {
                if ($this->aInstaller->isModified() || $this->aInstaller->isNew()) {
                    $affectedRows += $this->aInstaller->save($con);
                }
                $this->setInstaller($this->aInstaller);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->logDownloadPatchsScheduledForDeletion !== null) {
                if (!$this->logDownloadPatchsScheduledForDeletion->isEmpty()) {
                    foreach ($this->logDownloadPatchsScheduledForDeletion as $logDownloadPatch) {
                        // need to save related object because we set the relation to null
                        $logDownloadPatch->save($con);
                    }
                    $this->logDownloadPatchsScheduledForDeletion = null;
                }
            }

            if ($this->collLogDownloadPatchs !== null) {
                foreach ($this->collLogDownloadPatchs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = PatchPeer::PATCH_ID;
        if (null !== $this->patch_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PatchPeer::PATCH_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PatchPeer::PATCH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PATCH_ID`';
        }
        if ($this->isColumnModified(PatchPeer::PENGGUNA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PENGGUNA_ID`';
        }
        if ($this->isColumnModified(PatchPeer::INSTALLER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`INSTALLER_ID`';
        }
        if ($this->isColumnModified(PatchPeer::NAMA)) {
            $modifiedColumns[':p' . $index++]  = '`NAMA`';
        }
        if ($this->isColumnModified(PatchPeer::FILE)) {
            $modifiedColumns[':p' . $index++]  = '`FILE`';
        }
        if ($this->isColumnModified(PatchPeer::TANGGAL)) {
            $modifiedColumns[':p' . $index++]  = '`TANGGAL`';
        }
        if ($this->isColumnModified(PatchPeer::VERSI)) {
            $modifiedColumns[':p' . $index++]  = '`VERSI`';
        }
        if ($this->isColumnModified(PatchPeer::ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`ACTIVE`';
        }
        if ($this->isColumnModified(PatchPeer::DESKRIPSI)) {
            $modifiedColumns[':p' . $index++]  = '`DESKRIPSI`';
        }

        $sql = sprintf(
            'INSERT INTO `patch` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`PATCH_ID`':						
                        $stmt->bindValue($identifier, $this->patch_id, PDO::PARAM_INT);
                        break;
                    case '`PENGGUNA_ID`':						
                        $stmt->bindValue($identifier, $this->pengguna_id, PDO::PARAM_INT);
                        break;
                    case '`INSTALLER_ID`':						
                        $stmt->bindValue($identifier, $this->installer_id, PDO::PARAM_INT);
                        break;
                    case '`NAMA`':						
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case '`FILE`':						
                        $stmt->bindValue($identifier, $this->file, PDO::PARAM_STR);
                        break;
                    case '`TANGGAL`':						
                        $stmt->bindValue($identifier, $this->tanggal, PDO::PARAM_STR);
                        break;
                    case '`VERSI`':						
                        $stmt->bindValue($identifier, $this->versi, PDO::PARAM_STR);
                        break;
                    case '`ACTIVE`':						
                        $stmt->bindValue($identifier, $this->active, PDO::PARAM_INT);
                        break;
                    case '`DESKRIPSI`':						
                        $stmt->bindValue($identifier, $this->deskripsi, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setPatchId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPengguna !== null) {
                if (!$this->aPengguna->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPengguna->getValidationFailures());
                }
            }

            if ($this->aInstaller !== null) {
                if (!$this->aInstaller->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aInstaller->getValidationFailures());
                }
            }


            if (($retval = PatchPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collLogDownloadPatchs !== null) {
                    foreach ($this->collLogDownloadPatchs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PatchPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPatchId();
                break;
            case 1:
                return $this->getPenggunaId();
                break;
            case 2:
                return $this->getInstallerId();
                break;
            case 3:
                return $this->getNama();
                break;
            case 4:
                return $this->getFile();
                break;
            case 5:
                return $this->getTanggal();
                break;
            case 6:
                return $this->getVersi();
                break;
            case 7:
                return $this->getActive();
                break;
            case 8:
                return $this->getDeskripsi();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Patch'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Patch'][$this->getPrimaryKey()] = true;
        $keys = PatchPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPatchId(),
            $keys[1] => $this->getPenggunaId(),
            $keys[2] => $this->getInstallerId(),
            $keys[3] => $this->getNama(),
            $keys[4] => $this->getFile(),
            $keys[5] => $this->getTanggal(),
            $keys[6] => $this->getVersi(),
            $keys[7] => $this->getActive(),
            $keys[8] => $this->getDeskripsi(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPengguna) {
                $result['Pengguna'] = $this->aPengguna->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aInstaller) {
                $result['Installer'] = $this->aInstaller->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collLogDownloadPatchs) {
                $result['LogDownloadPatchs'] = $this->collLogDownloadPatchs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PatchPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPatchId($value);
                break;
            case 1:
                $this->setPenggunaId($value);
                break;
            case 2:
                $this->setInstallerId($value);
                break;
            case 3:
                $this->setNama($value);
                break;
            case 4:
                $this->setFile($value);
                break;
            case 5:
                $this->setTanggal($value);
                break;
            case 6:
                $this->setVersi($value);
                break;
            case 7:
                $this->setActive($value);
                break;
            case 8:
                $this->setDeskripsi($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PatchPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPatchId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPenggunaId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setInstallerId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNama($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setFile($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTanggal($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setVersi($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setActive($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDeskripsi($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PatchPeer::DATABASE_NAME);

        if ($this->isColumnModified(PatchPeer::PATCH_ID)) $criteria->add(PatchPeer::PATCH_ID, $this->patch_id);
        if ($this->isColumnModified(PatchPeer::PENGGUNA_ID)) $criteria->add(PatchPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(PatchPeer::INSTALLER_ID)) $criteria->add(PatchPeer::INSTALLER_ID, $this->installer_id);
        if ($this->isColumnModified(PatchPeer::NAMA)) $criteria->add(PatchPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PatchPeer::FILE)) $criteria->add(PatchPeer::FILE, $this->file);
        if ($this->isColumnModified(PatchPeer::TANGGAL)) $criteria->add(PatchPeer::TANGGAL, $this->tanggal);
        if ($this->isColumnModified(PatchPeer::VERSI)) $criteria->add(PatchPeer::VERSI, $this->versi);
        if ($this->isColumnModified(PatchPeer::ACTIVE)) $criteria->add(PatchPeer::ACTIVE, $this->active);
        if ($this->isColumnModified(PatchPeer::DESKRIPSI)) $criteria->add(PatchPeer::DESKRIPSI, $this->deskripsi);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PatchPeer::DATABASE_NAME);
        $criteria->add(PatchPeer::PATCH_ID, $this->patch_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getPatchId();
    }

    /**
     * Generic method to set the primary key (patch_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPatchId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPatchId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Patch (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPenggunaId($this->getPenggunaId());
        $copyObj->setInstallerId($this->getInstallerId());
        $copyObj->setNama($this->getNama());
        $copyObj->setFile($this->getFile());
        $copyObj->setTanggal($this->getTanggal());
        $copyObj->setVersi($this->getVersi());
        $copyObj->setActive($this->getActive());
        $copyObj->setDeskripsi($this->getDeskripsi());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getLogDownloadPatchs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogDownloadPatch($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPatchId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Patch Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PatchPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PatchPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Pengguna object.
     *
     * @param             Pengguna $v
     * @return Patch The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPengguna(Pengguna $v = null)
    {
        if ($v === null) {
            $this->setPenggunaId(NULL);
        } else {
            $this->setPenggunaId($v->getPenggunaId());
        }

        $this->aPengguna = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Pengguna object, it will not be re-added.
        if ($v !== null) {
            $v->addPatch($this);
        }


        return $this;
    }


    /**
     * Get the associated Pengguna object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Pengguna The associated Pengguna object.
     * @throws PropelException
     */
    public function getPengguna(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPengguna === null && ($this->pengguna_id !== null) && $doQuery) {
            $this->aPengguna = PenggunaQuery::create()->findPk($this->pengguna_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPengguna->addPatchs($this);
             */
        }

        return $this->aPengguna;
    }

    /**
     * Declares an association between this object and a Installer object.
     *
     * @param             Installer $v
     * @return Patch The current object (for fluent API support)
     * @throws PropelException
     */
    public function setInstaller(Installer $v = null)
    {
        if ($v === null) {
            $this->setInstallerId(NULL);
        } else {
            $this->setInstallerId($v->getInstallerId());
        }

        $this->aInstaller = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Installer object, it will not be re-added.
        if ($v !== null) {
            $v->addPatch($this);
        }


        return $this;
    }


    /**
     * Get the associated Installer object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Installer The associated Installer object.
     * @throws PropelException
     */
    public function getInstaller(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aInstaller === null && ($this->installer_id !== null) && $doQuery) {
            $this->aInstaller = InstallerQuery::create()->findPk($this->installer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aInstaller->addPatchs($this);
             */
        }

        return $this->aInstaller;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('LogDownloadPatch' == $relationName) {
            $this->initLogDownloadPatchs();
        }
    }

    /**
     * Clears out the collLogDownloadPatchs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Patch The current object (for fluent API support)
     * @see        addLogDownloadPatchs()
     */
    public function clearLogDownloadPatchs()
    {
        $this->collLogDownloadPatchs = null; // important to set this to null since that means it is uninitialized
        $this->collLogDownloadPatchsPartial = null;

        return $this;
    }

    /**
     * reset is the collLogDownloadPatchs collection loaded partially
     *
     * @return void
     */
    public function resetPartialLogDownloadPatchs($v = true)
    {
        $this->collLogDownloadPatchsPartial = $v;
    }

    /**
     * Initializes the collLogDownloadPatchs collection.
     *
     * By default this just sets the collLogDownloadPatchs collection to an empty array (like clearcollLogDownloadPatchs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogDownloadPatchs($overrideExisting = true)
    {
        if (null !== $this->collLogDownloadPatchs && !$overrideExisting) {
            return;
        }
        $this->collLogDownloadPatchs = new PropelObjectCollection();
        $this->collLogDownloadPatchs->setModel('LogDownloadPatch');
    }

    /**
     * Gets an array of LogDownloadPatch objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Patch is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LogDownloadPatch[] List of LogDownloadPatch objects
     * @throws PropelException
     */
    public function getLogDownloadPatchs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLogDownloadPatchsPartial && !$this->isNew();
        if (null === $this->collLogDownloadPatchs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogDownloadPatchs) {
                // return empty collection
                $this->initLogDownloadPatchs();
            } else {
                $collLogDownloadPatchs = LogDownloadPatchQuery::create(null, $criteria)
                    ->filterByPatch($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLogDownloadPatchsPartial && count($collLogDownloadPatchs)) {
                      $this->initLogDownloadPatchs(false);

                      foreach($collLogDownloadPatchs as $obj) {
                        if (false == $this->collLogDownloadPatchs->contains($obj)) {
                          $this->collLogDownloadPatchs->append($obj);
                        }
                      }

                      $this->collLogDownloadPatchsPartial = true;
                    }

                    $collLogDownloadPatchs->getInternalIterator()->rewind();
                    return $collLogDownloadPatchs;
                }

                if($partial && $this->collLogDownloadPatchs) {
                    foreach($this->collLogDownloadPatchs as $obj) {
                        if($obj->isNew()) {
                            $collLogDownloadPatchs[] = $obj;
                        }
                    }
                }

                $this->collLogDownloadPatchs = $collLogDownloadPatchs;
                $this->collLogDownloadPatchsPartial = false;
            }
        }

        return $this->collLogDownloadPatchs;
    }

    /**
     * Sets a collection of LogDownloadPatch objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $logDownloadPatchs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Patch The current object (for fluent API support)
     */
    public function setLogDownloadPatchs(PropelCollection $logDownloadPatchs, PropelPDO $con = null)
    {
        $logDownloadPatchsToDelete = $this->getLogDownloadPatchs(new Criteria(), $con)->diff($logDownloadPatchs);

        $this->logDownloadPatchsScheduledForDeletion = unserialize(serialize($logDownloadPatchsToDelete));

        foreach ($logDownloadPatchsToDelete as $logDownloadPatchRemoved) {
            $logDownloadPatchRemoved->setPatch(null);
        }

        $this->collLogDownloadPatchs = null;
        foreach ($logDownloadPatchs as $logDownloadPatch) {
            $this->addLogDownloadPatch($logDownloadPatch);
        }

        $this->collLogDownloadPatchs = $logDownloadPatchs;
        $this->collLogDownloadPatchsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LogDownloadPatch objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LogDownloadPatch objects.
     * @throws PropelException
     */
    public function countLogDownloadPatchs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLogDownloadPatchsPartial && !$this->isNew();
        if (null === $this->collLogDownloadPatchs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogDownloadPatchs) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLogDownloadPatchs());
            }
            $query = LogDownloadPatchQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPatch($this)
                ->count($con);
        }

        return count($this->collLogDownloadPatchs);
    }

    /**
     * Method called to associate a LogDownloadPatch object to this object
     * through the LogDownloadPatch foreign key attribute.
     *
     * @param    LogDownloadPatch $l LogDownloadPatch
     * @return Patch The current object (for fluent API support)
     */
    public function addLogDownloadPatch(LogDownloadPatch $l)
    {
        if ($this->collLogDownloadPatchs === null) {
            $this->initLogDownloadPatchs();
            $this->collLogDownloadPatchsPartial = true;
        }
        if (!in_array($l, $this->collLogDownloadPatchs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLogDownloadPatch($l);
        }

        return $this;
    }

    /**
     * @param	LogDownloadPatch $logDownloadPatch The logDownloadPatch object to add.
     */
    protected function doAddLogDownloadPatch($logDownloadPatch)
    {
        $this->collLogDownloadPatchs[]= $logDownloadPatch;
        $logDownloadPatch->setPatch($this);
    }

    /**
     * @param	LogDownloadPatch $logDownloadPatch The logDownloadPatch object to remove.
     * @return Patch The current object (for fluent API support)
     */
    public function removeLogDownloadPatch($logDownloadPatch)
    {
        if ($this->getLogDownloadPatchs()->contains($logDownloadPatch)) {
            $this->collLogDownloadPatchs->remove($this->collLogDownloadPatchs->search($logDownloadPatch));
            if (null === $this->logDownloadPatchsScheduledForDeletion) {
                $this->logDownloadPatchsScheduledForDeletion = clone $this->collLogDownloadPatchs;
                $this->logDownloadPatchsScheduledForDeletion->clear();
            }
            $this->logDownloadPatchsScheduledForDeletion[]= $logDownloadPatch;
            $logDownloadPatch->setPatch(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->patch_id = null;
        $this->pengguna_id = null;
        $this->installer_id = null;
        $this->nama = null;
        $this->file = null;
        $this->tanggal = null;
        $this->versi = null;
        $this->active = null;
        $this->deskripsi = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collLogDownloadPatchs) {
                foreach ($this->collLogDownloadPatchs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPengguna instanceof Persistent) {
              $this->aPengguna->clearAllReferences($deep);
            }
            if ($this->aInstaller instanceof Persistent) {
              $this->aInstaller->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collLogDownloadPatchs instanceof PropelCollection) {
            $this->collLogDownloadPatchs->clearIterator();
        }
        $this->collLogDownloadPatchs = null;
        $this->aPengguna = null;
        $this->aInstaller = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PatchPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
