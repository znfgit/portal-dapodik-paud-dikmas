<?php

namespace infopendataan\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\RekapSekolah;
use infopendataan\Model\RekapSekolahPeer;
use infopendataan\Model\map\RekapSekolahTableMap;

/**
 * Base static class for performing query and update operations on the 'rekap_sekolah' table.
 *
 * 
 *
 * @package propel.generator.infopendataan.Model.om
 */
abstract class BaseRekapSekolahPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'infopendataan';

    /** the table name for this class */
    const TABLE_NAME = 'rekap_sekolah';

    /** the related Propel class for this table */
    const OM_CLASS = 'infopendataan\\Model\\RekapSekolah';

    /** the related TableMap class for this table */
    const TM_CLASS = 'RekapSekolahTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 35;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 35;

    /** the column name for the rekap_sekolah_id field */
    const REKAP_SEKOLAH_ID = 'rekap_sekolah.rekap_sekolah_id';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'rekap_sekolah.sekolah_id';

    /** the column name for the tanggal field */
    const TANGGAL = 'rekap_sekolah.tanggal';

    /** the column name for the ptk field */
    const PTK = 'rekap_sekolah.ptk';

    /** the column name for the pegawai field */
    const PEGAWAI = 'rekap_sekolah.pegawai';

    /** the column name for the pd field */
    const PD = 'rekap_sekolah.pd';

    /** the column name for the rombel field */
    const ROMBEL = 'rekap_sekolah.rombel';

    /** the column name for the kode_wilayah_kecamatan field */
    const KODE_WILAYAH_KECAMATAN = 'rekap_sekolah.kode_wilayah_kecamatan';

    /** the column name for the kecamatan field */
    const KECAMATAN = 'rekap_sekolah.kecamatan';

    /** the column name for the mst_kode_wilayah_kecamatan field */
    const MST_KODE_WILAYAH_KECAMATAN = 'rekap_sekolah.mst_kode_wilayah_kecamatan';

    /** the column name for the id_level_wilayah_kecamatan field */
    const ID_LEVEL_WILAYAH_KECAMATAN = 'rekap_sekolah.id_level_wilayah_kecamatan';

    /** the column name for the kode_wilayah_kabupaten field */
    const KODE_WILAYAH_KABUPATEN = 'rekap_sekolah.kode_wilayah_kabupaten';

    /** the column name for the kabupaten field */
    const KABUPATEN = 'rekap_sekolah.kabupaten';

    /** the column name for the mst_kode_wilayah_kabupaten field */
    const MST_KODE_WILAYAH_KABUPATEN = 'rekap_sekolah.mst_kode_wilayah_kabupaten';

    /** the column name for the id_level_wilayah_kabupaten field */
    const ID_LEVEL_WILAYAH_KABUPATEN = 'rekap_sekolah.id_level_wilayah_kabupaten';

    /** the column name for the kode_wilayah_provinsi field */
    const KODE_WILAYAH_PROVINSI = 'rekap_sekolah.kode_wilayah_provinsi';

    /** the column name for the provinsi field */
    const PROVINSI = 'rekap_sekolah.provinsi';

    /** the column name for the mst_kode_wilayah_provinsi field */
    const MST_KODE_WILAYAH_PROVINSI = 'rekap_sekolah.mst_kode_wilayah_provinsi';

    /** the column name for the id_level_wilayah_provinsi field */
    const ID_LEVEL_WILAYAH_PROVINSI = 'rekap_sekolah.id_level_wilayah_provinsi';

    /** the column name for the nama field */
    const NAMA = 'rekap_sekolah.nama';

    /** the column name for the npsn field */
    const NPSN = 'rekap_sekolah.npsn';

    /** the column name for the bentuk_pendidikan_id field */
    const BENTUK_PENDIDIKAN_ID = 'rekap_sekolah.bentuk_pendidikan_id';

    /** the column name for the status_sekolah field */
    const STATUS_SEKOLAH = 'rekap_sekolah.status_sekolah';

    /** the column name for the guru_matematika field */
    const GURU_MATEMATIKA = 'rekap_sekolah.guru_matematika';

    /** the column name for the guru_bahasa_indonesia field */
    const GURU_BAHASA_INDONESIA = 'rekap_sekolah.guru_bahasa_indonesia';

    /** the column name for the guru_bahasa_inggris field */
    const GURU_BAHASA_INGGRIS = 'rekap_sekolah.guru_bahasa_inggris';

    /** the column name for the guru_sejarah_indonesia field */
    const GURU_SEJARAH_INDONESIA = 'rekap_sekolah.guru_sejarah_indonesia';

    /** the column name for the guru_pkn field */
    const GURU_PKN = 'rekap_sekolah.guru_pkn';

    /** the column name for the guru_penjaskes field */
    const GURU_PENJASKES = 'rekap_sekolah.guru_penjaskes';

    /** the column name for the guru_agama_budi_pekerti field */
    const GURU_AGAMA_BUDI_PEKERTI = 'rekap_sekolah.guru_agama_budi_pekerti';

    /** the column name for the guru_seni_budaya field */
    const GURU_SENI_BUDAYA = 'rekap_sekolah.guru_seni_budaya';

    /** the column name for the pd_kelas_10 field */
    const PD_KELAS_10 = 'rekap_sekolah.pd_kelas_10';

    /** the column name for the pd_kelas_11 field */
    const PD_KELAS_11 = 'rekap_sekolah.pd_kelas_11';

    /** the column name for the pd_kelas_12 field */
    const PD_KELAS_12 = 'rekap_sekolah.pd_kelas_12';

    /** the column name for the pd_kelas_13 field */
    const PD_KELAS_13 = 'rekap_sekolah.pd_kelas_13';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of RekapSekolah objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array RekapSekolah[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. RekapSekolahPeer::$fieldNames[RekapSekolahPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('RekapSekolahId', 'SekolahId', 'Tanggal', 'Ptk', 'Pegawai', 'Pd', 'Rombel', 'KodeWilayahKecamatan', 'Kecamatan', 'MstKodeWilayahKecamatan', 'IdLevelWilayahKecamatan', 'KodeWilayahKabupaten', 'Kabupaten', 'MstKodeWilayahKabupaten', 'IdLevelWilayahKabupaten', 'KodeWilayahProvinsi', 'Provinsi', 'MstKodeWilayahProvinsi', 'IdLevelWilayahProvinsi', 'Nama', 'Npsn', 'BentukPendidikanId', 'StatusSekolah', 'GuruMatematika', 'GuruBahasaIndonesia', 'GuruBahasaInggris', 'GuruSejarahIndonesia', 'GuruPkn', 'GuruPenjaskes', 'GuruAgamaBudiPekerti', 'GuruSeniBudaya', 'PdKelas10', 'PdKelas11', 'PdKelas12', 'PdKelas13', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('rekapSekolahId', 'sekolahId', 'tanggal', 'ptk', 'pegawai', 'pd', 'rombel', 'kodeWilayahKecamatan', 'kecamatan', 'mstKodeWilayahKecamatan', 'idLevelWilayahKecamatan', 'kodeWilayahKabupaten', 'kabupaten', 'mstKodeWilayahKabupaten', 'idLevelWilayahKabupaten', 'kodeWilayahProvinsi', 'provinsi', 'mstKodeWilayahProvinsi', 'idLevelWilayahProvinsi', 'nama', 'npsn', 'bentukPendidikanId', 'statusSekolah', 'guruMatematika', 'guruBahasaIndonesia', 'guruBahasaInggris', 'guruSejarahIndonesia', 'guruPkn', 'guruPenjaskes', 'guruAgamaBudiPekerti', 'guruSeniBudaya', 'pdKelas10', 'pdKelas11', 'pdKelas12', 'pdKelas13', ),
        BasePeer::TYPE_COLNAME => array (RekapSekolahPeer::REKAP_SEKOLAH_ID, RekapSekolahPeer::SEKOLAH_ID, RekapSekolahPeer::TANGGAL, RekapSekolahPeer::PTK, RekapSekolahPeer::PEGAWAI, RekapSekolahPeer::PD, RekapSekolahPeer::ROMBEL, RekapSekolahPeer::KODE_WILAYAH_KECAMATAN, RekapSekolahPeer::KECAMATAN, RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN, RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN, RekapSekolahPeer::KODE_WILAYAH_KABUPATEN, RekapSekolahPeer::KABUPATEN, RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN, RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN, RekapSekolahPeer::KODE_WILAYAH_PROVINSI, RekapSekolahPeer::PROVINSI, RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI, RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI, RekapSekolahPeer::NAMA, RekapSekolahPeer::NPSN, RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, RekapSekolahPeer::STATUS_SEKOLAH, RekapSekolahPeer::GURU_MATEMATIKA, RekapSekolahPeer::GURU_BAHASA_INDONESIA, RekapSekolahPeer::GURU_BAHASA_INGGRIS, RekapSekolahPeer::GURU_SEJARAH_INDONESIA, RekapSekolahPeer::GURU_PKN, RekapSekolahPeer::GURU_PENJASKES, RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI, RekapSekolahPeer::GURU_SENI_BUDAYA, RekapSekolahPeer::PD_KELAS_10, RekapSekolahPeer::PD_KELAS_11, RekapSekolahPeer::PD_KELAS_12, RekapSekolahPeer::PD_KELAS_13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('REKAP_SEKOLAH_ID', 'SEKOLAH_ID', 'TANGGAL', 'PTK', 'PEGAWAI', 'PD', 'ROMBEL', 'KODE_WILAYAH_KECAMATAN', 'KECAMATAN', 'MST_KODE_WILAYAH_KECAMATAN', 'ID_LEVEL_WILAYAH_KECAMATAN', 'KODE_WILAYAH_KABUPATEN', 'KABUPATEN', 'MST_KODE_WILAYAH_KABUPATEN', 'ID_LEVEL_WILAYAH_KABUPATEN', 'KODE_WILAYAH_PROVINSI', 'PROVINSI', 'MST_KODE_WILAYAH_PROVINSI', 'ID_LEVEL_WILAYAH_PROVINSI', 'NAMA', 'NPSN', 'BENTUK_PENDIDIKAN_ID', 'STATUS_SEKOLAH', 'GURU_MATEMATIKA', 'GURU_BAHASA_INDONESIA', 'GURU_BAHASA_INGGRIS', 'GURU_SEJARAH_INDONESIA', 'GURU_PKN', 'GURU_PENJASKES', 'GURU_AGAMA_BUDI_PEKERTI', 'GURU_SENI_BUDAYA', 'PD_KELAS_10', 'PD_KELAS_11', 'PD_KELAS_12', 'PD_KELAS_13', ),
        BasePeer::TYPE_FIELDNAME => array ('rekap_sekolah_id', 'sekolah_id', 'tanggal', 'ptk', 'pegawai', 'pd', 'rombel', 'kode_wilayah_kecamatan', 'kecamatan', 'mst_kode_wilayah_kecamatan', 'id_level_wilayah_kecamatan', 'kode_wilayah_kabupaten', 'kabupaten', 'mst_kode_wilayah_kabupaten', 'id_level_wilayah_kabupaten', 'kode_wilayah_provinsi', 'provinsi', 'mst_kode_wilayah_provinsi', 'id_level_wilayah_provinsi', 'nama', 'npsn', 'bentuk_pendidikan_id', 'status_sekolah', 'guru_matematika', 'guru_bahasa_indonesia', 'guru_bahasa_inggris', 'guru_sejarah_indonesia', 'guru_pkn', 'guru_penjaskes', 'guru_agama_budi_pekerti', 'guru_seni_budaya', 'pd_kelas_10', 'pd_kelas_11', 'pd_kelas_12', 'pd_kelas_13', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. RekapSekolahPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('RekapSekolahId' => 0, 'SekolahId' => 1, 'Tanggal' => 2, 'Ptk' => 3, 'Pegawai' => 4, 'Pd' => 5, 'Rombel' => 6, 'KodeWilayahKecamatan' => 7, 'Kecamatan' => 8, 'MstKodeWilayahKecamatan' => 9, 'IdLevelWilayahKecamatan' => 10, 'KodeWilayahKabupaten' => 11, 'Kabupaten' => 12, 'MstKodeWilayahKabupaten' => 13, 'IdLevelWilayahKabupaten' => 14, 'KodeWilayahProvinsi' => 15, 'Provinsi' => 16, 'MstKodeWilayahProvinsi' => 17, 'IdLevelWilayahProvinsi' => 18, 'Nama' => 19, 'Npsn' => 20, 'BentukPendidikanId' => 21, 'StatusSekolah' => 22, 'GuruMatematika' => 23, 'GuruBahasaIndonesia' => 24, 'GuruBahasaInggris' => 25, 'GuruSejarahIndonesia' => 26, 'GuruPkn' => 27, 'GuruPenjaskes' => 28, 'GuruAgamaBudiPekerti' => 29, 'GuruSeniBudaya' => 30, 'PdKelas10' => 31, 'PdKelas11' => 32, 'PdKelas12' => 33, 'PdKelas13' => 34, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('rekapSekolahId' => 0, 'sekolahId' => 1, 'tanggal' => 2, 'ptk' => 3, 'pegawai' => 4, 'pd' => 5, 'rombel' => 6, 'kodeWilayahKecamatan' => 7, 'kecamatan' => 8, 'mstKodeWilayahKecamatan' => 9, 'idLevelWilayahKecamatan' => 10, 'kodeWilayahKabupaten' => 11, 'kabupaten' => 12, 'mstKodeWilayahKabupaten' => 13, 'idLevelWilayahKabupaten' => 14, 'kodeWilayahProvinsi' => 15, 'provinsi' => 16, 'mstKodeWilayahProvinsi' => 17, 'idLevelWilayahProvinsi' => 18, 'nama' => 19, 'npsn' => 20, 'bentukPendidikanId' => 21, 'statusSekolah' => 22, 'guruMatematika' => 23, 'guruBahasaIndonesia' => 24, 'guruBahasaInggris' => 25, 'guruSejarahIndonesia' => 26, 'guruPkn' => 27, 'guruPenjaskes' => 28, 'guruAgamaBudiPekerti' => 29, 'guruSeniBudaya' => 30, 'pdKelas10' => 31, 'pdKelas11' => 32, 'pdKelas12' => 33, 'pdKelas13' => 34, ),
        BasePeer::TYPE_COLNAME => array (RekapSekolahPeer::REKAP_SEKOLAH_ID => 0, RekapSekolahPeer::SEKOLAH_ID => 1, RekapSekolahPeer::TANGGAL => 2, RekapSekolahPeer::PTK => 3, RekapSekolahPeer::PEGAWAI => 4, RekapSekolahPeer::PD => 5, RekapSekolahPeer::ROMBEL => 6, RekapSekolahPeer::KODE_WILAYAH_KECAMATAN => 7, RekapSekolahPeer::KECAMATAN => 8, RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN => 9, RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN => 10, RekapSekolahPeer::KODE_WILAYAH_KABUPATEN => 11, RekapSekolahPeer::KABUPATEN => 12, RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN => 13, RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN => 14, RekapSekolahPeer::KODE_WILAYAH_PROVINSI => 15, RekapSekolahPeer::PROVINSI => 16, RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI => 17, RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI => 18, RekapSekolahPeer::NAMA => 19, RekapSekolahPeer::NPSN => 20, RekapSekolahPeer::BENTUK_PENDIDIKAN_ID => 21, RekapSekolahPeer::STATUS_SEKOLAH => 22, RekapSekolahPeer::GURU_MATEMATIKA => 23, RekapSekolahPeer::GURU_BAHASA_INDONESIA => 24, RekapSekolahPeer::GURU_BAHASA_INGGRIS => 25, RekapSekolahPeer::GURU_SEJARAH_INDONESIA => 26, RekapSekolahPeer::GURU_PKN => 27, RekapSekolahPeer::GURU_PENJASKES => 28, RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI => 29, RekapSekolahPeer::GURU_SENI_BUDAYA => 30, RekapSekolahPeer::PD_KELAS_10 => 31, RekapSekolahPeer::PD_KELAS_11 => 32, RekapSekolahPeer::PD_KELAS_12 => 33, RekapSekolahPeer::PD_KELAS_13 => 34, ),
        BasePeer::TYPE_RAW_COLNAME => array ('REKAP_SEKOLAH_ID' => 0, 'SEKOLAH_ID' => 1, 'TANGGAL' => 2, 'PTK' => 3, 'PEGAWAI' => 4, 'PD' => 5, 'ROMBEL' => 6, 'KODE_WILAYAH_KECAMATAN' => 7, 'KECAMATAN' => 8, 'MST_KODE_WILAYAH_KECAMATAN' => 9, 'ID_LEVEL_WILAYAH_KECAMATAN' => 10, 'KODE_WILAYAH_KABUPATEN' => 11, 'KABUPATEN' => 12, 'MST_KODE_WILAYAH_KABUPATEN' => 13, 'ID_LEVEL_WILAYAH_KABUPATEN' => 14, 'KODE_WILAYAH_PROVINSI' => 15, 'PROVINSI' => 16, 'MST_KODE_WILAYAH_PROVINSI' => 17, 'ID_LEVEL_WILAYAH_PROVINSI' => 18, 'NAMA' => 19, 'NPSN' => 20, 'BENTUK_PENDIDIKAN_ID' => 21, 'STATUS_SEKOLAH' => 22, 'GURU_MATEMATIKA' => 23, 'GURU_BAHASA_INDONESIA' => 24, 'GURU_BAHASA_INGGRIS' => 25, 'GURU_SEJARAH_INDONESIA' => 26, 'GURU_PKN' => 27, 'GURU_PENJASKES' => 28, 'GURU_AGAMA_BUDI_PEKERTI' => 29, 'GURU_SENI_BUDAYA' => 30, 'PD_KELAS_10' => 31, 'PD_KELAS_11' => 32, 'PD_KELAS_12' => 33, 'PD_KELAS_13' => 34, ),
        BasePeer::TYPE_FIELDNAME => array ('rekap_sekolah_id' => 0, 'sekolah_id' => 1, 'tanggal' => 2, 'ptk' => 3, 'pegawai' => 4, 'pd' => 5, 'rombel' => 6, 'kode_wilayah_kecamatan' => 7, 'kecamatan' => 8, 'mst_kode_wilayah_kecamatan' => 9, 'id_level_wilayah_kecamatan' => 10, 'kode_wilayah_kabupaten' => 11, 'kabupaten' => 12, 'mst_kode_wilayah_kabupaten' => 13, 'id_level_wilayah_kabupaten' => 14, 'kode_wilayah_provinsi' => 15, 'provinsi' => 16, 'mst_kode_wilayah_provinsi' => 17, 'id_level_wilayah_provinsi' => 18, 'nama' => 19, 'npsn' => 20, 'bentuk_pendidikan_id' => 21, 'status_sekolah' => 22, 'guru_matematika' => 23, 'guru_bahasa_indonesia' => 24, 'guru_bahasa_inggris' => 25, 'guru_sejarah_indonesia' => 26, 'guru_pkn' => 27, 'guru_penjaskes' => 28, 'guru_agama_budi_pekerti' => 29, 'guru_seni_budaya' => 30, 'pd_kelas_10' => 31, 'pd_kelas_11' => 32, 'pd_kelas_12' => 33, 'pd_kelas_13' => 34, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = RekapSekolahPeer::getFieldNames($toType);
        $key = isset(RekapSekolahPeer::$fieldKeys[$fromType][$name]) ? RekapSekolahPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(RekapSekolahPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, RekapSekolahPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return RekapSekolahPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. RekapSekolahPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(RekapSekolahPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RekapSekolahPeer::REKAP_SEKOLAH_ID);
            $criteria->addSelectColumn(RekapSekolahPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(RekapSekolahPeer::TANGGAL);
            $criteria->addSelectColumn(RekapSekolahPeer::PTK);
            $criteria->addSelectColumn(RekapSekolahPeer::PEGAWAI);
            $criteria->addSelectColumn(RekapSekolahPeer::PD);
            $criteria->addSelectColumn(RekapSekolahPeer::ROMBEL);
            $criteria->addSelectColumn(RekapSekolahPeer::KODE_WILAYAH_KECAMATAN);
            $criteria->addSelectColumn(RekapSekolahPeer::KECAMATAN);
            $criteria->addSelectColumn(RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN);
            $criteria->addSelectColumn(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN);
            $criteria->addSelectColumn(RekapSekolahPeer::KODE_WILAYAH_KABUPATEN);
            $criteria->addSelectColumn(RekapSekolahPeer::KABUPATEN);
            $criteria->addSelectColumn(RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN);
            $criteria->addSelectColumn(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN);
            $criteria->addSelectColumn(RekapSekolahPeer::KODE_WILAYAH_PROVINSI);
            $criteria->addSelectColumn(RekapSekolahPeer::PROVINSI);
            $criteria->addSelectColumn(RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI);
            $criteria->addSelectColumn(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI);
            $criteria->addSelectColumn(RekapSekolahPeer::NAMA);
            $criteria->addSelectColumn(RekapSekolahPeer::NPSN);
            $criteria->addSelectColumn(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID);
            $criteria->addSelectColumn(RekapSekolahPeer::STATUS_SEKOLAH);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_MATEMATIKA);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_BAHASA_INDONESIA);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_BAHASA_INGGRIS);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_SEJARAH_INDONESIA);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_PKN);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_PENJASKES);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI);
            $criteria->addSelectColumn(RekapSekolahPeer::GURU_SENI_BUDAYA);
            $criteria->addSelectColumn(RekapSekolahPeer::PD_KELAS_10);
            $criteria->addSelectColumn(RekapSekolahPeer::PD_KELAS_11);
            $criteria->addSelectColumn(RekapSekolahPeer::PD_KELAS_12);
            $criteria->addSelectColumn(RekapSekolahPeer::PD_KELAS_13);
        } else {
            $criteria->addSelectColumn($alias . '.rekap_sekolah_id');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.tanggal');
            $criteria->addSelectColumn($alias . '.ptk');
            $criteria->addSelectColumn($alias . '.pegawai');
            $criteria->addSelectColumn($alias . '.pd');
            $criteria->addSelectColumn($alias . '.rombel');
            $criteria->addSelectColumn($alias . '.kode_wilayah_kecamatan');
            $criteria->addSelectColumn($alias . '.kecamatan');
            $criteria->addSelectColumn($alias . '.mst_kode_wilayah_kecamatan');
            $criteria->addSelectColumn($alias . '.id_level_wilayah_kecamatan');
            $criteria->addSelectColumn($alias . '.kode_wilayah_kabupaten');
            $criteria->addSelectColumn($alias . '.kabupaten');
            $criteria->addSelectColumn($alias . '.mst_kode_wilayah_kabupaten');
            $criteria->addSelectColumn($alias . '.id_level_wilayah_kabupaten');
            $criteria->addSelectColumn($alias . '.kode_wilayah_provinsi');
            $criteria->addSelectColumn($alias . '.provinsi');
            $criteria->addSelectColumn($alias . '.mst_kode_wilayah_provinsi');
            $criteria->addSelectColumn($alias . '.id_level_wilayah_provinsi');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.npsn');
            $criteria->addSelectColumn($alias . '.bentuk_pendidikan_id');
            $criteria->addSelectColumn($alias . '.status_sekolah');
            $criteria->addSelectColumn($alias . '.guru_matematika');
            $criteria->addSelectColumn($alias . '.guru_bahasa_indonesia');
            $criteria->addSelectColumn($alias . '.guru_bahasa_inggris');
            $criteria->addSelectColumn($alias . '.guru_sejarah_indonesia');
            $criteria->addSelectColumn($alias . '.guru_pkn');
            $criteria->addSelectColumn($alias . '.guru_penjaskes');
            $criteria->addSelectColumn($alias . '.guru_agama_budi_pekerti');
            $criteria->addSelectColumn($alias . '.guru_seni_budaya');
            $criteria->addSelectColumn($alias . '.pd_kelas_10');
            $criteria->addSelectColumn($alias . '.pd_kelas_11');
            $criteria->addSelectColumn($alias . '.pd_kelas_12');
            $criteria->addSelectColumn($alias . '.pd_kelas_13');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(RekapSekolahPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            RekapSekolahPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(RekapSekolahPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 RekapSekolah
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = RekapSekolahPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return RekapSekolahPeer::populateObjects(RekapSekolahPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            RekapSekolahPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(RekapSekolahPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      RekapSekolah $obj A RekapSekolah object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getRekapSekolahId();
            } // if key === null
            RekapSekolahPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A RekapSekolah object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof RekapSekolah) {
                $key = (string) $value->getRekapSekolahId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or RekapSekolah object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(RekapSekolahPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   RekapSekolah Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(RekapSekolahPeer::$instances[$key])) {
                return RekapSekolahPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (RekapSekolahPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        RekapSekolahPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to rekap_sekolah
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = RekapSekolahPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = RekapSekolahPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = RekapSekolahPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RekapSekolahPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (RekapSekolah object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = RekapSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = RekapSekolahPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + RekapSekolahPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RekapSekolahPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            RekapSekolahPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(RekapSekolahPeer::DATABASE_NAME)->getTable(RekapSekolahPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseRekapSekolahPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseRekapSekolahPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new RekapSekolahTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return RekapSekolahPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a RekapSekolah or Criteria object.
     *
     * @param      mixed $values Criteria or RekapSekolah object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from RekapSekolah object
        }

        if ($criteria->containsKey(RekapSekolahPeer::REKAP_SEKOLAH_ID) && $criteria->keyContainsValue(RekapSekolahPeer::REKAP_SEKOLAH_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RekapSekolahPeer::REKAP_SEKOLAH_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(RekapSekolahPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a RekapSekolah or Criteria object.
     *
     * @param      mixed $values Criteria or RekapSekolah object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(RekapSekolahPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(RekapSekolahPeer::REKAP_SEKOLAH_ID);
            $value = $criteria->remove(RekapSekolahPeer::REKAP_SEKOLAH_ID);
            if ($value) {
                $selectCriteria->add(RekapSekolahPeer::REKAP_SEKOLAH_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(RekapSekolahPeer::TABLE_NAME);
            }

        } else { // $values is RekapSekolah object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(RekapSekolahPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the rekap_sekolah table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(RekapSekolahPeer::TABLE_NAME, $con, RekapSekolahPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RekapSekolahPeer::clearInstancePool();
            RekapSekolahPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a RekapSekolah or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or RekapSekolah object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            RekapSekolahPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof RekapSekolah) { // it's a model object
            // invalidate the cache for this single object
            RekapSekolahPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RekapSekolahPeer::DATABASE_NAME);
            $criteria->add(RekapSekolahPeer::REKAP_SEKOLAH_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                RekapSekolahPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(RekapSekolahPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            RekapSekolahPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given RekapSekolah object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      RekapSekolah $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(RekapSekolahPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(RekapSekolahPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(RekapSekolahPeer::DATABASE_NAME, RekapSekolahPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return RekapSekolah
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = RekapSekolahPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(RekapSekolahPeer::DATABASE_NAME);
        $criteria->add(RekapSekolahPeer::REKAP_SEKOLAH_ID, $pk);

        $v = RekapSekolahPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return RekapSekolah[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(RekapSekolahPeer::DATABASE_NAME);
            $criteria->add(RekapSekolahPeer::REKAP_SEKOLAH_ID, $pks, Criteria::IN);
            $objs = RekapSekolahPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseRekapSekolahPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseRekapSekolahPeer::buildTableMap();

