<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\JumlahPd;
use infopendataan\Model\JumlahPdPeer;
use infopendataan\Model\JumlahPdQuery;

/**
 * Base class that represents a query for the 'jumlah_pd' table.
 *
 * 
 *
 * @method JumlahPdQuery orderByJumlahPdId($order = Criteria::ASC) Order by the jumlah_pd_id column
 * @method JumlahPdQuery orderByTanggal($order = Criteria::ASC) Order by the tanggal column
 * @method JumlahPdQuery orderByIdLevelWilayah($order = Criteria::ASC) Order by the id_level_wilayah column
 * @method JumlahPdQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method JumlahPdQuery orderByMstKodeWilayah($order = Criteria::ASC) Order by the mst_kode_wilayah column
 * @method JumlahPdQuery orderByTahunAjaranId($order = Criteria::ASC) Order by the tahun_ajaran_id column
 * @method JumlahPdQuery orderByPdDiRombel($order = Criteria::ASC) Order by the pd_di_rombel column
 * @method JumlahPdQuery orderByPdBelumDiRombel($order = Criteria::ASC) Order by the pd_belum_di_rombel column
 * @method JumlahPdQuery orderByPdTotal($order = Criteria::ASC) Order by the pd_total column
 *
 * @method JumlahPdQuery groupByJumlahPdId() Group by the jumlah_pd_id column
 * @method JumlahPdQuery groupByTanggal() Group by the tanggal column
 * @method JumlahPdQuery groupByIdLevelWilayah() Group by the id_level_wilayah column
 * @method JumlahPdQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method JumlahPdQuery groupByMstKodeWilayah() Group by the mst_kode_wilayah column
 * @method JumlahPdQuery groupByTahunAjaranId() Group by the tahun_ajaran_id column
 * @method JumlahPdQuery groupByPdDiRombel() Group by the pd_di_rombel column
 * @method JumlahPdQuery groupByPdBelumDiRombel() Group by the pd_belum_di_rombel column
 * @method JumlahPdQuery groupByPdTotal() Group by the pd_total column
 *
 * @method JumlahPdQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JumlahPdQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JumlahPdQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JumlahPd findOne(PropelPDO $con = null) Return the first JumlahPd matching the query
 * @method JumlahPd findOneOrCreate(PropelPDO $con = null) Return the first JumlahPd matching the query, or a new JumlahPd object populated from the query conditions when no match is found
 *
 * @method JumlahPd findOneByTanggal(string $tanggal) Return the first JumlahPd filtered by the tanggal column
 * @method JumlahPd findOneByIdLevelWilayah(string $id_level_wilayah) Return the first JumlahPd filtered by the id_level_wilayah column
 * @method JumlahPd findOneByKodeWilayah(string $kode_wilayah) Return the first JumlahPd filtered by the kode_wilayah column
 * @method JumlahPd findOneByMstKodeWilayah(string $mst_kode_wilayah) Return the first JumlahPd filtered by the mst_kode_wilayah column
 * @method JumlahPd findOneByTahunAjaranId(string $tahun_ajaran_id) Return the first JumlahPd filtered by the tahun_ajaran_id column
 * @method JumlahPd findOneByPdDiRombel(double $pd_di_rombel) Return the first JumlahPd filtered by the pd_di_rombel column
 * @method JumlahPd findOneByPdBelumDiRombel(double $pd_belum_di_rombel) Return the first JumlahPd filtered by the pd_belum_di_rombel column
 * @method JumlahPd findOneByPdTotal(double $pd_total) Return the first JumlahPd filtered by the pd_total column
 *
 * @method array findByJumlahPdId(int $jumlah_pd_id) Return JumlahPd objects filtered by the jumlah_pd_id column
 * @method array findByTanggal(string $tanggal) Return JumlahPd objects filtered by the tanggal column
 * @method array findByIdLevelWilayah(string $id_level_wilayah) Return JumlahPd objects filtered by the id_level_wilayah column
 * @method array findByKodeWilayah(string $kode_wilayah) Return JumlahPd objects filtered by the kode_wilayah column
 * @method array findByMstKodeWilayah(string $mst_kode_wilayah) Return JumlahPd objects filtered by the mst_kode_wilayah column
 * @method array findByTahunAjaranId(string $tahun_ajaran_id) Return JumlahPd objects filtered by the tahun_ajaran_id column
 * @method array findByPdDiRombel(double $pd_di_rombel) Return JumlahPd objects filtered by the pd_di_rombel column
 * @method array findByPdBelumDiRombel(double $pd_belum_di_rombel) Return JumlahPd objects filtered by the pd_belum_di_rombel column
 * @method array findByPdTotal(double $pd_total) Return JumlahPd objects filtered by the pd_total column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJumlahPdQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJumlahPdQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JumlahPd', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JumlahPdQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JumlahPdQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JumlahPdQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JumlahPdQuery) {
            return $criteria;
        }
        $query = new JumlahPdQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JumlahPd|JumlahPd[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JumlahPdPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JumlahPd A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJumlahPdId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JumlahPd A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `jumlah_pd_id`, `tanggal`, `id_level_wilayah`, `kode_wilayah`, `mst_kode_wilayah`, `tahun_ajaran_id`, `pd_di_rombel`, `pd_belum_di_rombel`, `pd_total` FROM `jumlah_pd` WHERE `jumlah_pd_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JumlahPd();
            $obj->hydrate($row);
            JumlahPdPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JumlahPd|JumlahPd[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JumlahPd[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JumlahPdPeer::JUMLAH_PD_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JumlahPdPeer::JUMLAH_PD_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the jumlah_pd_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahPdId(1234); // WHERE jumlah_pd_id = 1234
     * $query->filterByJumlahPdId(array(12, 34)); // WHERE jumlah_pd_id IN (12, 34)
     * $query->filterByJumlahPdId(array('min' => 12)); // WHERE jumlah_pd_id >= 12
     * $query->filterByJumlahPdId(array('max' => 12)); // WHERE jumlah_pd_id <= 12
     * </code>
     *
     * @param     mixed $jumlahPdId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByJumlahPdId($jumlahPdId = null, $comparison = null)
    {
        if (is_array($jumlahPdId)) {
            $useMinMax = false;
            if (isset($jumlahPdId['min'])) {
                $this->addUsingAlias(JumlahPdPeer::JUMLAH_PD_ID, $jumlahPdId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahPdId['max'])) {
                $this->addUsingAlias(JumlahPdPeer::JUMLAH_PD_ID, $jumlahPdId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::JUMLAH_PD_ID, $jumlahPdId, $comparison);
    }

    /**
     * Filter the query on the tanggal column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE tanggal > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(JumlahPdPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(JumlahPdPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayah('fooValue');   // WHERE id_level_wilayah = 'fooValue'
     * $query->filterByIdLevelWilayah('%fooValue%'); // WHERE id_level_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idLevelWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayah($idLevelWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idLevelWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idLevelWilayah)) {
                $idLevelWilayah = str_replace('*', '%', $idLevelWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::ID_LEVEL_WILAYAH, $idLevelWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayah('fooValue');   // WHERE mst_kode_wilayah = 'fooValue'
     * $query->filterByMstKodeWilayah('%fooValue%'); // WHERE mst_kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayah($mstKodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayah)) {
                $mstKodeWilayah = str_replace('*', '%', $mstKodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::MST_KODE_WILAYAH, $mstKodeWilayah, $comparison);
    }

    /**
     * Filter the query on the tahun_ajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunAjaranId('fooValue');   // WHERE tahun_ajaran_id = 'fooValue'
     * $query->filterByTahunAjaranId('%fooValue%'); // WHERE tahun_ajaran_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tahunAjaranId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByTahunAjaranId($tahunAjaranId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tahunAjaranId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tahunAjaranId)) {
                $tahunAjaranId = str_replace('*', '%', $tahunAjaranId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::TAHUN_AJARAN_ID, $tahunAjaranId, $comparison);
    }

    /**
     * Filter the query on the pd_di_rombel column
     *
     * Example usage:
     * <code>
     * $query->filterByPdDiRombel(1234); // WHERE pd_di_rombel = 1234
     * $query->filterByPdDiRombel(array(12, 34)); // WHERE pd_di_rombel IN (12, 34)
     * $query->filterByPdDiRombel(array('min' => 12)); // WHERE pd_di_rombel >= 12
     * $query->filterByPdDiRombel(array('max' => 12)); // WHERE pd_di_rombel <= 12
     * </code>
     *
     * @param     mixed $pdDiRombel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByPdDiRombel($pdDiRombel = null, $comparison = null)
    {
        if (is_array($pdDiRombel)) {
            $useMinMax = false;
            if (isset($pdDiRombel['min'])) {
                $this->addUsingAlias(JumlahPdPeer::PD_DI_ROMBEL, $pdDiRombel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdDiRombel['max'])) {
                $this->addUsingAlias(JumlahPdPeer::PD_DI_ROMBEL, $pdDiRombel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::PD_DI_ROMBEL, $pdDiRombel, $comparison);
    }

    /**
     * Filter the query on the pd_belum_di_rombel column
     *
     * Example usage:
     * <code>
     * $query->filterByPdBelumDiRombel(1234); // WHERE pd_belum_di_rombel = 1234
     * $query->filterByPdBelumDiRombel(array(12, 34)); // WHERE pd_belum_di_rombel IN (12, 34)
     * $query->filterByPdBelumDiRombel(array('min' => 12)); // WHERE pd_belum_di_rombel >= 12
     * $query->filterByPdBelumDiRombel(array('max' => 12)); // WHERE pd_belum_di_rombel <= 12
     * </code>
     *
     * @param     mixed $pdBelumDiRombel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByPdBelumDiRombel($pdBelumDiRombel = null, $comparison = null)
    {
        if (is_array($pdBelumDiRombel)) {
            $useMinMax = false;
            if (isset($pdBelumDiRombel['min'])) {
                $this->addUsingAlias(JumlahPdPeer::PD_BELUM_DI_ROMBEL, $pdBelumDiRombel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdBelumDiRombel['max'])) {
                $this->addUsingAlias(JumlahPdPeer::PD_BELUM_DI_ROMBEL, $pdBelumDiRombel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::PD_BELUM_DI_ROMBEL, $pdBelumDiRombel, $comparison);
    }

    /**
     * Filter the query on the pd_total column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTotal(1234); // WHERE pd_total = 1234
     * $query->filterByPdTotal(array(12, 34)); // WHERE pd_total IN (12, 34)
     * $query->filterByPdTotal(array('min' => 12)); // WHERE pd_total >= 12
     * $query->filterByPdTotal(array('max' => 12)); // WHERE pd_total <= 12
     * </code>
     *
     * @param     mixed $pdTotal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function filterByPdTotal($pdTotal = null, $comparison = null)
    {
        if (is_array($pdTotal)) {
            $useMinMax = false;
            if (isset($pdTotal['min'])) {
                $this->addUsingAlias(JumlahPdPeer::PD_TOTAL, $pdTotal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTotal['max'])) {
                $this->addUsingAlias(JumlahPdPeer::PD_TOTAL, $pdTotal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdPeer::PD_TOTAL, $pdTotal, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   JumlahPd $jumlahPd Object to remove from the list of results
     *
     * @return JumlahPdQuery The current query, for fluid interface
     */
    public function prune($jumlahPd = null)
    {
        if ($jumlahPd) {
            $this->addUsingAlias(JumlahPdPeer::JUMLAH_PD_ID, $jumlahPd->getJumlahPdId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
