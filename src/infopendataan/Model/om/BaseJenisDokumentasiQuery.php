<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Dokumentasi;
use infopendataan\Model\JenisDokumentasi;
use infopendataan\Model\JenisDokumentasiPeer;
use infopendataan\Model\JenisDokumentasiQuery;

/**
 * Base class that represents a query for the 'jenis_dokumentasi' table.
 *
 * 
 *
 * @method JenisDokumentasiQuery orderByJenisDokumentasiId($order = Criteria::ASC) Order by the JENIS_DOKUMENTASI_ID column
 * @method JenisDokumentasiQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 *
 * @method JenisDokumentasiQuery groupByJenisDokumentasiId() Group by the JENIS_DOKUMENTASI_ID column
 * @method JenisDokumentasiQuery groupByNama() Group by the NAMA column
 *
 * @method JenisDokumentasiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JenisDokumentasiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JenisDokumentasiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JenisDokumentasiQuery leftJoinDokumentasi($relationAlias = null) Adds a LEFT JOIN clause to the query using the Dokumentasi relation
 * @method JenisDokumentasiQuery rightJoinDokumentasi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Dokumentasi relation
 * @method JenisDokumentasiQuery innerJoinDokumentasi($relationAlias = null) Adds a INNER JOIN clause to the query using the Dokumentasi relation
 *
 * @method JenisDokumentasi findOne(PropelPDO $con = null) Return the first JenisDokumentasi matching the query
 * @method JenisDokumentasi findOneOrCreate(PropelPDO $con = null) Return the first JenisDokumentasi matching the query, or a new JenisDokumentasi object populated from the query conditions when no match is found
 *
 * @method JenisDokumentasi findOneByNama(string $NAMA) Return the first JenisDokumentasi filtered by the NAMA column
 *
 * @method array findByJenisDokumentasiId(int $JENIS_DOKUMENTASI_ID) Return JenisDokumentasi objects filtered by the JENIS_DOKUMENTASI_ID column
 * @method array findByNama(string $NAMA) Return JenisDokumentasi objects filtered by the NAMA column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJenisDokumentasiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJenisDokumentasiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JenisDokumentasi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JenisDokumentasiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JenisDokumentasiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JenisDokumentasiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JenisDokumentasiQuery) {
            return $criteria;
        }
        $query = new JenisDokumentasiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JenisDokumentasi|JenisDokumentasi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JenisDokumentasiPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JenisDokumentasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisDokumentasi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJenisDokumentasiId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisDokumentasi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `JENIS_DOKUMENTASI_ID`, `NAMA` FROM `jenis_dokumentasi` WHERE `JENIS_DOKUMENTASI_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JenisDokumentasi();
            $obj->hydrate($row);
            JenisDokumentasiPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JenisDokumentasi|JenisDokumentasi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JenisDokumentasi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JenisDokumentasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JenisDokumentasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the JENIS_DOKUMENTASI_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisDokumentasiId(1234); // WHERE JENIS_DOKUMENTASI_ID = 1234
     * $query->filterByJenisDokumentasiId(array(12, 34)); // WHERE JENIS_DOKUMENTASI_ID IN (12, 34)
     * $query->filterByJenisDokumentasiId(array('min' => 12)); // WHERE JENIS_DOKUMENTASI_ID >= 12
     * $query->filterByJenisDokumentasiId(array('max' => 12)); // WHERE JENIS_DOKUMENTASI_ID <= 12
     * </code>
     *
     * @param     mixed $jenisDokumentasiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisDokumentasiQuery The current query, for fluid interface
     */
    public function filterByJenisDokumentasiId($jenisDokumentasiId = null, $comparison = null)
    {
        if (is_array($jenisDokumentasiId)) {
            $useMinMax = false;
            if (isset($jenisDokumentasiId['min'])) {
                $this->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisDokumentasiId['max'])) {
                $this->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasiId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisDokumentasiQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisDokumentasiPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query by a related Dokumentasi object
     *
     * @param   Dokumentasi|PropelObjectCollection $dokumentasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JenisDokumentasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDokumentasi($dokumentasi, $comparison = null)
    {
        if ($dokumentasi instanceof Dokumentasi) {
            return $this
                ->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $dokumentasi->getJenisDokumentasiId(), $comparison);
        } elseif ($dokumentasi instanceof PropelObjectCollection) {
            return $this
                ->useDokumentasiQuery()
                ->filterByPrimaryKeys($dokumentasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDokumentasi() only accepts arguments of type Dokumentasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Dokumentasi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JenisDokumentasiQuery The current query, for fluid interface
     */
    public function joinDokumentasi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Dokumentasi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Dokumentasi');
        }

        return $this;
    }

    /**
     * Use the Dokumentasi relation Dokumentasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\DokumentasiQuery A secondary query class using the current class as primary query
     */
    public function useDokumentasiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDokumentasi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Dokumentasi', '\infopendataan\Model\DokumentasiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JenisDokumentasi $jenisDokumentasi Object to remove from the list of results
     *
     * @return JenisDokumentasiQuery The current query, for fluid interface
     */
    public function prune($jenisDokumentasi = null)
    {
        if ($jenisDokumentasi) {
            $this->addUsingAlias(JenisDokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasi->getJenisDokumentasiId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
