<?php

namespace infopendataan\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\JumlahSekolah;
use infopendataan\Model\JumlahSekolahPeer;
use infopendataan\Model\JumlahSekolahQuery;

/**
 * Base class that represents a row from the 'jumlah_sekolah' table.
 *
 * 
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJumlahSekolah extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'infopendataan\\Model\\JumlahSekolahPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        JumlahSekolahPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the jumlah_sekolah_id field.
     * @var        int
     */
    protected $jumlah_sekolah_id;

    /**
     * The value for the tanggal field.
     * @var        string
     */
    protected $tanggal;

    /**
     * The value for the id_level_wilayah field.
     * @var        string
     */
    protected $id_level_wilayah;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the mst_kode_wilayah field.
     * @var        string
     */
    protected $mst_kode_wilayah;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the status_negeri field.
     * @var        double
     */
    protected $status_negeri;

    /**
     * The value for the status_swasta field.
     * @var        double
     */
    protected $status_swasta;

    /**
     * The value for the status_sma_negeri field.
     * @var        double
     */
    protected $status_sma_negeri;

    /**
     * The value for the status_sma_swasta field.
     * @var        double
     */
    protected $status_sma_swasta;

    /**
     * The value for the status_smk_negeri field.
     * @var        double
     */
    protected $status_smk_negeri;

    /**
     * The value for the status_smk_swasta field.
     * @var        double
     */
    protected $status_smk_swasta;

    /**
     * The value for the status_smlb_negeri field.
     * @var        double
     */
    protected $status_smlb_negeri;

    /**
     * The value for the status_smlb_swasta field.
     * @var        double
     */
    protected $status_smlb_swasta;

    /**
     * The value for the waktu_penyelenggaraan_pagi field.
     * @var        double
     */
    protected $waktu_penyelenggaraan_pagi;

    /**
     * The value for the waktu_penyelenggaraan_siang field.
     * @var        double
     */
    protected $waktu_penyelenggaraan_siang;

    /**
     * The value for the waktu_penyelenggaraan_sore field.
     * @var        double
     */
    protected $waktu_penyelenggaraan_sore;

    /**
     * The value for the waktu_penyelenggaraan_malam field.
     * @var        double
     */
    protected $waktu_penyelenggaraan_malam;

    /**
     * The value for the waktu_penyelenggaraan_kombinasi field.
     * @var        double
     */
    protected $waktu_penyelenggaraan_kombinasi;

    /**
     * The value for the waktu_penyelenggaraan_lainnya field.
     * @var        double
     */
    protected $waktu_penyelenggaraan_lainnya;

    /**
     * The value for the akreditasi_a field.
     * @var        double
     */
    protected $akreditasi_a;

    /**
     * The value for the akreditasi_b field.
     * @var        double
     */
    protected $akreditasi_b;

    /**
     * The value for the akreditasi_c field.
     * @var        double
     */
    protected $akreditasi_c;

    /**
     * The value for the akreditasi_tidak field.
     * @var        double
     */
    protected $akreditasi_tidak;

    /**
     * The value for the listrik_ada field.
     * @var        double
     */
    protected $listrik_ada;

    /**
     * The value for the listrik_tidak field.
     * @var        double
     */
    protected $listrik_tidak;

    /**
     * The value for the sanitasi_ada field.
     * @var        double
     */
    protected $sanitasi_ada;

    /**
     * The value for the sanitasi_tidak field.
     * @var        double
     */
    protected $sanitasi_tidak;

    /**
     * The value for the akses_internet_ada field.
     * @var        double
     */
    protected $akses_internet_ada;

    /**
     * The value for the akses_internet_tidak field.
     * @var        double
     */
    protected $akses_internet_tidak;

    /**
     * The value for the mbs_ada field.
     * @var        double
     */
    protected $mbs_ada;

    /**
     * The value for the mbs_tidak field.
     * @var        double
     */
    protected $mbs_tidak;

    /**
     * The value for the wilayah_khusus_ada field.
     * @var        double
     */
    protected $wilayah_khusus_ada;

    /**
     * The value for the wilayah_khusus_tidak field.
     * @var        double
     */
    protected $wilayah_khusus_tidak;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [jumlah_sekolah_id] column value.
     * 
     * @return int
     */
    public function getJumlahSekolahId()
    {
        return $this->jumlah_sekolah_id;
    }

    /**
     * Get the [tanggal] column value.
     * 
     * @return string
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Get the [id_level_wilayah] column value.
     * 
     * @return string
     */
    public function getIdLevelWilayah()
    {
        return $this->id_level_wilayah;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [mst_kode_wilayah] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayah()
    {
        return $this->mst_kode_wilayah;
    }

    /**
     * Get the [semester_id] column value.
     * 
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [status_negeri] column value.
     * 
     * @return double
     */
    public function getStatusNegeri()
    {
        return $this->status_negeri;
    }

    /**
     * Get the [status_swasta] column value.
     * 
     * @return double
     */
    public function getStatusSwasta()
    {
        return $this->status_swasta;
    }

    /**
     * Get the [status_sma_negeri] column value.
     * 
     * @return double
     */
    public function getStatusSmaNegeri()
    {
        return $this->status_sma_negeri;
    }

    /**
     * Get the [status_sma_swasta] column value.
     * 
     * @return double
     */
    public function getStatusSmaSwasta()
    {
        return $this->status_sma_swasta;
    }

    /**
     * Get the [status_smk_negeri] column value.
     * 
     * @return double
     */
    public function getStatusSmkNegeri()
    {
        return $this->status_smk_negeri;
    }

    /**
     * Get the [status_smk_swasta] column value.
     * 
     * @return double
     */
    public function getStatusSmkSwasta()
    {
        return $this->status_smk_swasta;
    }

    /**
     * Get the [status_smlb_negeri] column value.
     * 
     * @return double
     */
    public function getStatusSmlbNegeri()
    {
        return $this->status_smlb_negeri;
    }

    /**
     * Get the [status_smlb_swasta] column value.
     * 
     * @return double
     */
    public function getStatusSmlbSwasta()
    {
        return $this->status_smlb_swasta;
    }

    /**
     * Get the [waktu_penyelenggaraan_pagi] column value.
     * 
     * @return double
     */
    public function getWaktuPenyelenggaraanPagi()
    {
        return $this->waktu_penyelenggaraan_pagi;
    }

    /**
     * Get the [waktu_penyelenggaraan_siang] column value.
     * 
     * @return double
     */
    public function getWaktuPenyelenggaraanSiang()
    {
        return $this->waktu_penyelenggaraan_siang;
    }

    /**
     * Get the [waktu_penyelenggaraan_sore] column value.
     * 
     * @return double
     */
    public function getWaktuPenyelenggaraanSore()
    {
        return $this->waktu_penyelenggaraan_sore;
    }

    /**
     * Get the [waktu_penyelenggaraan_malam] column value.
     * 
     * @return double
     */
    public function getWaktuPenyelenggaraanMalam()
    {
        return $this->waktu_penyelenggaraan_malam;
    }

    /**
     * Get the [waktu_penyelenggaraan_kombinasi] column value.
     * 
     * @return double
     */
    public function getWaktuPenyelenggaraanKombinasi()
    {
        return $this->waktu_penyelenggaraan_kombinasi;
    }

    /**
     * Get the [waktu_penyelenggaraan_lainnya] column value.
     * 
     * @return double
     */
    public function getWaktuPenyelenggaraanLainnya()
    {
        return $this->waktu_penyelenggaraan_lainnya;
    }

    /**
     * Get the [akreditasi_a] column value.
     * 
     * @return double
     */
    public function getAkreditasiA()
    {
        return $this->akreditasi_a;
    }

    /**
     * Get the [akreditasi_b] column value.
     * 
     * @return double
     */
    public function getAkreditasiB()
    {
        return $this->akreditasi_b;
    }

    /**
     * Get the [akreditasi_c] column value.
     * 
     * @return double
     */
    public function getAkreditasiC()
    {
        return $this->akreditasi_c;
    }

    /**
     * Get the [akreditasi_tidak] column value.
     * 
     * @return double
     */
    public function getAkreditasiTidak()
    {
        return $this->akreditasi_tidak;
    }

    /**
     * Get the [listrik_ada] column value.
     * 
     * @return double
     */
    public function getListrikAda()
    {
        return $this->listrik_ada;
    }

    /**
     * Get the [listrik_tidak] column value.
     * 
     * @return double
     */
    public function getListrikTidak()
    {
        return $this->listrik_tidak;
    }

    /**
     * Get the [sanitasi_ada] column value.
     * 
     * @return double
     */
    public function getSanitasiAda()
    {
        return $this->sanitasi_ada;
    }

    /**
     * Get the [sanitasi_tidak] column value.
     * 
     * @return double
     */
    public function getSanitasiTidak()
    {
        return $this->sanitasi_tidak;
    }

    /**
     * Get the [akses_internet_ada] column value.
     * 
     * @return double
     */
    public function getAksesInternetAda()
    {
        return $this->akses_internet_ada;
    }

    /**
     * Get the [akses_internet_tidak] column value.
     * 
     * @return double
     */
    public function getAksesInternetTidak()
    {
        return $this->akses_internet_tidak;
    }

    /**
     * Get the [mbs_ada] column value.
     * 
     * @return double
     */
    public function getMbsAda()
    {
        return $this->mbs_ada;
    }

    /**
     * Get the [mbs_tidak] column value.
     * 
     * @return double
     */
    public function getMbsTidak()
    {
        return $this->mbs_tidak;
    }

    /**
     * Get the [wilayah_khusus_ada] column value.
     * 
     * @return double
     */
    public function getWilayahKhususAda()
    {
        return $this->wilayah_khusus_ada;
    }

    /**
     * Get the [wilayah_khusus_tidak] column value.
     * 
     * @return double
     */
    public function getWilayahKhususTidak()
    {
        return $this->wilayah_khusus_tidak;
    }

    /**
     * Set the value of [jumlah_sekolah_id] column.
     * 
     * @param int $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setJumlahSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jumlah_sekolah_id !== $v) {
            $this->jumlah_sekolah_id = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::JUMLAH_SEKOLAH_ID;
        }


        return $this;
    } // setJumlahSekolahId()

    /**
     * Set the value of [tanggal] column.
     * 
     * @param string $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setTanggal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal !== $v) {
            $this->tanggal = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::TANGGAL;
        }


        return $this;
    } // setTanggal()

    /**
     * Set the value of [id_level_wilayah] column.
     * 
     * @param string $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setIdLevelWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_level_wilayah !== $v) {
            $this->id_level_wilayah = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::ID_LEVEL_WILAYAH;
        }


        return $this;
    } // setIdLevelWilayah()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::KODE_WILAYAH;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [mst_kode_wilayah] column.
     * 
     * @param string $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setMstKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah !== $v) {
            $this->mst_kode_wilayah = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::MST_KODE_WILAYAH;
        }


        return $this;
    } // setMstKodeWilayah()

    /**
     * Set the value of [semester_id] column.
     * 
     * @param string $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::SEMESTER_ID;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [status_negeri] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusNegeri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_negeri !== $v) {
            $this->status_negeri = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_NEGERI;
        }


        return $this;
    } // setStatusNegeri()

    /**
     * Set the value of [status_swasta] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSwasta($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_swasta !== $v) {
            $this->status_swasta = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SWASTA;
        }


        return $this;
    } // setStatusSwasta()

    /**
     * Set the value of [status_sma_negeri] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSmaNegeri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_sma_negeri !== $v) {
            $this->status_sma_negeri = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SMA_NEGERI;
        }


        return $this;
    } // setStatusSmaNegeri()

    /**
     * Set the value of [status_sma_swasta] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSmaSwasta($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_sma_swasta !== $v) {
            $this->status_sma_swasta = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SMA_SWASTA;
        }


        return $this;
    } // setStatusSmaSwasta()

    /**
     * Set the value of [status_smk_negeri] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSmkNegeri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_smk_negeri !== $v) {
            $this->status_smk_negeri = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SMK_NEGERI;
        }


        return $this;
    } // setStatusSmkNegeri()

    /**
     * Set the value of [status_smk_swasta] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSmkSwasta($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_smk_swasta !== $v) {
            $this->status_smk_swasta = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SMK_SWASTA;
        }


        return $this;
    } // setStatusSmkSwasta()

    /**
     * Set the value of [status_smlb_negeri] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSmlbNegeri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_smlb_negeri !== $v) {
            $this->status_smlb_negeri = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SMLB_NEGERI;
        }


        return $this;
    } // setStatusSmlbNegeri()

    /**
     * Set the value of [status_smlb_swasta] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setStatusSmlbSwasta($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->status_smlb_swasta !== $v) {
            $this->status_smlb_swasta = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::STATUS_SMLB_SWASTA;
        }


        return $this;
    } // setStatusSmlbSwasta()

    /**
     * Set the value of [waktu_penyelenggaraan_pagi] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWaktuPenyelenggaraanPagi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->waktu_penyelenggaraan_pagi !== $v) {
            $this->waktu_penyelenggaraan_pagi = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI;
        }


        return $this;
    } // setWaktuPenyelenggaraanPagi()

    /**
     * Set the value of [waktu_penyelenggaraan_siang] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWaktuPenyelenggaraanSiang($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->waktu_penyelenggaraan_siang !== $v) {
            $this->waktu_penyelenggaraan_siang = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG;
        }


        return $this;
    } // setWaktuPenyelenggaraanSiang()

    /**
     * Set the value of [waktu_penyelenggaraan_sore] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWaktuPenyelenggaraanSore($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->waktu_penyelenggaraan_sore !== $v) {
            $this->waktu_penyelenggaraan_sore = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE;
        }


        return $this;
    } // setWaktuPenyelenggaraanSore()

    /**
     * Set the value of [waktu_penyelenggaraan_malam] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWaktuPenyelenggaraanMalam($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->waktu_penyelenggaraan_malam !== $v) {
            $this->waktu_penyelenggaraan_malam = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM;
        }


        return $this;
    } // setWaktuPenyelenggaraanMalam()

    /**
     * Set the value of [waktu_penyelenggaraan_kombinasi] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWaktuPenyelenggaraanKombinasi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->waktu_penyelenggaraan_kombinasi !== $v) {
            $this->waktu_penyelenggaraan_kombinasi = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI;
        }


        return $this;
    } // setWaktuPenyelenggaraanKombinasi()

    /**
     * Set the value of [waktu_penyelenggaraan_lainnya] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWaktuPenyelenggaraanLainnya($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->waktu_penyelenggaraan_lainnya !== $v) {
            $this->waktu_penyelenggaraan_lainnya = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA;
        }


        return $this;
    } // setWaktuPenyelenggaraanLainnya()

    /**
     * Set the value of [akreditasi_a] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setAkreditasiA($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->akreditasi_a !== $v) {
            $this->akreditasi_a = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::AKREDITASI_A;
        }


        return $this;
    } // setAkreditasiA()

    /**
     * Set the value of [akreditasi_b] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setAkreditasiB($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->akreditasi_b !== $v) {
            $this->akreditasi_b = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::AKREDITASI_B;
        }


        return $this;
    } // setAkreditasiB()

    /**
     * Set the value of [akreditasi_c] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setAkreditasiC($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->akreditasi_c !== $v) {
            $this->akreditasi_c = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::AKREDITASI_C;
        }


        return $this;
    } // setAkreditasiC()

    /**
     * Set the value of [akreditasi_tidak] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setAkreditasiTidak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->akreditasi_tidak !== $v) {
            $this->akreditasi_tidak = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::AKREDITASI_TIDAK;
        }


        return $this;
    } // setAkreditasiTidak()

    /**
     * Set the value of [listrik_ada] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setListrikAda($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->listrik_ada !== $v) {
            $this->listrik_ada = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::LISTRIK_ADA;
        }


        return $this;
    } // setListrikAda()

    /**
     * Set the value of [listrik_tidak] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setListrikTidak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->listrik_tidak !== $v) {
            $this->listrik_tidak = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::LISTRIK_TIDAK;
        }


        return $this;
    } // setListrikTidak()

    /**
     * Set the value of [sanitasi_ada] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setSanitasiAda($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->sanitasi_ada !== $v) {
            $this->sanitasi_ada = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::SANITASI_ADA;
        }


        return $this;
    } // setSanitasiAda()

    /**
     * Set the value of [sanitasi_tidak] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setSanitasiTidak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->sanitasi_tidak !== $v) {
            $this->sanitasi_tidak = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::SANITASI_TIDAK;
        }


        return $this;
    } // setSanitasiTidak()

    /**
     * Set the value of [akses_internet_ada] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setAksesInternetAda($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->akses_internet_ada !== $v) {
            $this->akses_internet_ada = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::AKSES_INTERNET_ADA;
        }


        return $this;
    } // setAksesInternetAda()

    /**
     * Set the value of [akses_internet_tidak] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setAksesInternetTidak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->akses_internet_tidak !== $v) {
            $this->akses_internet_tidak = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::AKSES_INTERNET_TIDAK;
        }


        return $this;
    } // setAksesInternetTidak()

    /**
     * Set the value of [mbs_ada] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setMbsAda($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->mbs_ada !== $v) {
            $this->mbs_ada = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::MBS_ADA;
        }


        return $this;
    } // setMbsAda()

    /**
     * Set the value of [mbs_tidak] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setMbsTidak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->mbs_tidak !== $v) {
            $this->mbs_tidak = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::MBS_TIDAK;
        }


        return $this;
    } // setMbsTidak()

    /**
     * Set the value of [wilayah_khusus_ada] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWilayahKhususAda($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->wilayah_khusus_ada !== $v) {
            $this->wilayah_khusus_ada = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WILAYAH_KHUSUS_ADA;
        }


        return $this;
    } // setWilayahKhususAda()

    /**
     * Set the value of [wilayah_khusus_tidak] column.
     * 
     * @param double $v new value
     * @return JumlahSekolah The current object (for fluent API support)
     */
    public function setWilayahKhususTidak($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->wilayah_khusus_tidak !== $v) {
            $this->wilayah_khusus_tidak = $v;
            $this->modifiedColumns[] = JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK;
        }


        return $this;
    } // setWilayahKhususTidak()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->jumlah_sekolah_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->tanggal = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->id_level_wilayah = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->kode_wilayah = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->mst_kode_wilayah = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->semester_id = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->status_negeri = ($row[$startcol + 6] !== null) ? (double) $row[$startcol + 6] : null;
            $this->status_swasta = ($row[$startcol + 7] !== null) ? (double) $row[$startcol + 7] : null;
            $this->status_sma_negeri = ($row[$startcol + 8] !== null) ? (double) $row[$startcol + 8] : null;
            $this->status_sma_swasta = ($row[$startcol + 9] !== null) ? (double) $row[$startcol + 9] : null;
            $this->status_smk_negeri = ($row[$startcol + 10] !== null) ? (double) $row[$startcol + 10] : null;
            $this->status_smk_swasta = ($row[$startcol + 11] !== null) ? (double) $row[$startcol + 11] : null;
            $this->status_smlb_negeri = ($row[$startcol + 12] !== null) ? (double) $row[$startcol + 12] : null;
            $this->status_smlb_swasta = ($row[$startcol + 13] !== null) ? (double) $row[$startcol + 13] : null;
            $this->waktu_penyelenggaraan_pagi = ($row[$startcol + 14] !== null) ? (double) $row[$startcol + 14] : null;
            $this->waktu_penyelenggaraan_siang = ($row[$startcol + 15] !== null) ? (double) $row[$startcol + 15] : null;
            $this->waktu_penyelenggaraan_sore = ($row[$startcol + 16] !== null) ? (double) $row[$startcol + 16] : null;
            $this->waktu_penyelenggaraan_malam = ($row[$startcol + 17] !== null) ? (double) $row[$startcol + 17] : null;
            $this->waktu_penyelenggaraan_kombinasi = ($row[$startcol + 18] !== null) ? (double) $row[$startcol + 18] : null;
            $this->waktu_penyelenggaraan_lainnya = ($row[$startcol + 19] !== null) ? (double) $row[$startcol + 19] : null;
            $this->akreditasi_a = ($row[$startcol + 20] !== null) ? (double) $row[$startcol + 20] : null;
            $this->akreditasi_b = ($row[$startcol + 21] !== null) ? (double) $row[$startcol + 21] : null;
            $this->akreditasi_c = ($row[$startcol + 22] !== null) ? (double) $row[$startcol + 22] : null;
            $this->akreditasi_tidak = ($row[$startcol + 23] !== null) ? (double) $row[$startcol + 23] : null;
            $this->listrik_ada = ($row[$startcol + 24] !== null) ? (double) $row[$startcol + 24] : null;
            $this->listrik_tidak = ($row[$startcol + 25] !== null) ? (double) $row[$startcol + 25] : null;
            $this->sanitasi_ada = ($row[$startcol + 26] !== null) ? (double) $row[$startcol + 26] : null;
            $this->sanitasi_tidak = ($row[$startcol + 27] !== null) ? (double) $row[$startcol + 27] : null;
            $this->akses_internet_ada = ($row[$startcol + 28] !== null) ? (double) $row[$startcol + 28] : null;
            $this->akses_internet_tidak = ($row[$startcol + 29] !== null) ? (double) $row[$startcol + 29] : null;
            $this->mbs_ada = ($row[$startcol + 30] !== null) ? (double) $row[$startcol + 30] : null;
            $this->mbs_tidak = ($row[$startcol + 31] !== null) ? (double) $row[$startcol + 31] : null;
            $this->wilayah_khusus_ada = ($row[$startcol + 32] !== null) ? (double) $row[$startcol + 32] : null;
            $this->wilayah_khusus_tidak = ($row[$startcol + 33] !== null) ? (double) $row[$startcol + 33] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 34; // 34 = JumlahSekolahPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating JumlahSekolah object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = JumlahSekolahPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = JumlahSekolahQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                JumlahSekolahPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = JumlahSekolahPeer::JUMLAH_SEKOLAH_ID;
        if (null !== $this->jumlah_sekolah_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . JumlahSekolahPeer::JUMLAH_SEKOLAH_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`jumlah_sekolah_id`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::TANGGAL)) {
            $modifiedColumns[':p' . $index++]  = '`tanggal`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::ID_LEVEL_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`id_level_wilayah`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::KODE_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`kode_wilayah`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::MST_KODE_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`mst_kode_wilayah`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::SEMESTER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`semester_id`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_NEGERI)) {
            $modifiedColumns[':p' . $index++]  = '`status_negeri`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SWASTA)) {
            $modifiedColumns[':p' . $index++]  = '`status_swasta`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMA_NEGERI)) {
            $modifiedColumns[':p' . $index++]  = '`status_sma_negeri`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMA_SWASTA)) {
            $modifiedColumns[':p' . $index++]  = '`status_sma_swasta`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMK_NEGERI)) {
            $modifiedColumns[':p' . $index++]  = '`status_smk_negeri`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMK_SWASTA)) {
            $modifiedColumns[':p' . $index++]  = '`status_smk_swasta`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMLB_NEGERI)) {
            $modifiedColumns[':p' . $index++]  = '`status_smlb_negeri`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMLB_SWASTA)) {
            $modifiedColumns[':p' . $index++]  = '`status_smlb_swasta`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI)) {
            $modifiedColumns[':p' . $index++]  = '`waktu_penyelenggaraan_pagi`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG)) {
            $modifiedColumns[':p' . $index++]  = '`waktu_penyelenggaraan_siang`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE)) {
            $modifiedColumns[':p' . $index++]  = '`waktu_penyelenggaraan_sore`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM)) {
            $modifiedColumns[':p' . $index++]  = '`waktu_penyelenggaraan_malam`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI)) {
            $modifiedColumns[':p' . $index++]  = '`waktu_penyelenggaraan_kombinasi`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA)) {
            $modifiedColumns[':p' . $index++]  = '`waktu_penyelenggaraan_lainnya`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_A)) {
            $modifiedColumns[':p' . $index++]  = '`akreditasi_a`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_B)) {
            $modifiedColumns[':p' . $index++]  = '`akreditasi_b`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_C)) {
            $modifiedColumns[':p' . $index++]  = '`akreditasi_c`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_TIDAK)) {
            $modifiedColumns[':p' . $index++]  = '`akreditasi_tidak`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::LISTRIK_ADA)) {
            $modifiedColumns[':p' . $index++]  = '`listrik_ada`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::LISTRIK_TIDAK)) {
            $modifiedColumns[':p' . $index++]  = '`listrik_tidak`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::SANITASI_ADA)) {
            $modifiedColumns[':p' . $index++]  = '`sanitasi_ada`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::SANITASI_TIDAK)) {
            $modifiedColumns[':p' . $index++]  = '`sanitasi_tidak`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::AKSES_INTERNET_ADA)) {
            $modifiedColumns[':p' . $index++]  = '`akses_internet_ada`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::AKSES_INTERNET_TIDAK)) {
            $modifiedColumns[':p' . $index++]  = '`akses_internet_tidak`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::MBS_ADA)) {
            $modifiedColumns[':p' . $index++]  = '`mbs_ada`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::MBS_TIDAK)) {
            $modifiedColumns[':p' . $index++]  = '`mbs_tidak`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WILAYAH_KHUSUS_ADA)) {
            $modifiedColumns[':p' . $index++]  = '`wilayah_khusus_ada`';
        }
        if ($this->isColumnModified(JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK)) {
            $modifiedColumns[':p' . $index++]  = '`wilayah_khusus_tidak`';
        }

        $sql = sprintf(
            'INSERT INTO `jumlah_sekolah` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`jumlah_sekolah_id`':						
                        $stmt->bindValue($identifier, $this->jumlah_sekolah_id, PDO::PARAM_INT);
                        break;
                    case '`tanggal`':						
                        $stmt->bindValue($identifier, $this->tanggal, PDO::PARAM_STR);
                        break;
                    case '`id_level_wilayah`':						
                        $stmt->bindValue($identifier, $this->id_level_wilayah, PDO::PARAM_STR);
                        break;
                    case '`kode_wilayah`':						
                        $stmt->bindValue($identifier, $this->kode_wilayah, PDO::PARAM_STR);
                        break;
                    case '`mst_kode_wilayah`':						
                        $stmt->bindValue($identifier, $this->mst_kode_wilayah, PDO::PARAM_STR);
                        break;
                    case '`semester_id`':						
                        $stmt->bindValue($identifier, $this->semester_id, PDO::PARAM_STR);
                        break;
                    case '`status_negeri`':						
                        $stmt->bindValue($identifier, $this->status_negeri, PDO::PARAM_STR);
                        break;
                    case '`status_swasta`':						
                        $stmt->bindValue($identifier, $this->status_swasta, PDO::PARAM_STR);
                        break;
                    case '`status_sma_negeri`':						
                        $stmt->bindValue($identifier, $this->status_sma_negeri, PDO::PARAM_STR);
                        break;
                    case '`status_sma_swasta`':						
                        $stmt->bindValue($identifier, $this->status_sma_swasta, PDO::PARAM_STR);
                        break;
                    case '`status_smk_negeri`':						
                        $stmt->bindValue($identifier, $this->status_smk_negeri, PDO::PARAM_STR);
                        break;
                    case '`status_smk_swasta`':						
                        $stmt->bindValue($identifier, $this->status_smk_swasta, PDO::PARAM_STR);
                        break;
                    case '`status_smlb_negeri`':						
                        $stmt->bindValue($identifier, $this->status_smlb_negeri, PDO::PARAM_STR);
                        break;
                    case '`status_smlb_swasta`':						
                        $stmt->bindValue($identifier, $this->status_smlb_swasta, PDO::PARAM_STR);
                        break;
                    case '`waktu_penyelenggaraan_pagi`':						
                        $stmt->bindValue($identifier, $this->waktu_penyelenggaraan_pagi, PDO::PARAM_STR);
                        break;
                    case '`waktu_penyelenggaraan_siang`':						
                        $stmt->bindValue($identifier, $this->waktu_penyelenggaraan_siang, PDO::PARAM_STR);
                        break;
                    case '`waktu_penyelenggaraan_sore`':						
                        $stmt->bindValue($identifier, $this->waktu_penyelenggaraan_sore, PDO::PARAM_STR);
                        break;
                    case '`waktu_penyelenggaraan_malam`':						
                        $stmt->bindValue($identifier, $this->waktu_penyelenggaraan_malam, PDO::PARAM_STR);
                        break;
                    case '`waktu_penyelenggaraan_kombinasi`':						
                        $stmt->bindValue($identifier, $this->waktu_penyelenggaraan_kombinasi, PDO::PARAM_STR);
                        break;
                    case '`waktu_penyelenggaraan_lainnya`':						
                        $stmt->bindValue($identifier, $this->waktu_penyelenggaraan_lainnya, PDO::PARAM_STR);
                        break;
                    case '`akreditasi_a`':						
                        $stmt->bindValue($identifier, $this->akreditasi_a, PDO::PARAM_STR);
                        break;
                    case '`akreditasi_b`':						
                        $stmt->bindValue($identifier, $this->akreditasi_b, PDO::PARAM_STR);
                        break;
                    case '`akreditasi_c`':						
                        $stmt->bindValue($identifier, $this->akreditasi_c, PDO::PARAM_STR);
                        break;
                    case '`akreditasi_tidak`':						
                        $stmt->bindValue($identifier, $this->akreditasi_tidak, PDO::PARAM_STR);
                        break;
                    case '`listrik_ada`':						
                        $stmt->bindValue($identifier, $this->listrik_ada, PDO::PARAM_STR);
                        break;
                    case '`listrik_tidak`':						
                        $stmt->bindValue($identifier, $this->listrik_tidak, PDO::PARAM_STR);
                        break;
                    case '`sanitasi_ada`':						
                        $stmt->bindValue($identifier, $this->sanitasi_ada, PDO::PARAM_STR);
                        break;
                    case '`sanitasi_tidak`':						
                        $stmt->bindValue($identifier, $this->sanitasi_tidak, PDO::PARAM_STR);
                        break;
                    case '`akses_internet_ada`':						
                        $stmt->bindValue($identifier, $this->akses_internet_ada, PDO::PARAM_STR);
                        break;
                    case '`akses_internet_tidak`':						
                        $stmt->bindValue($identifier, $this->akses_internet_tidak, PDO::PARAM_STR);
                        break;
                    case '`mbs_ada`':						
                        $stmt->bindValue($identifier, $this->mbs_ada, PDO::PARAM_STR);
                        break;
                    case '`mbs_tidak`':						
                        $stmt->bindValue($identifier, $this->mbs_tidak, PDO::PARAM_STR);
                        break;
                    case '`wilayah_khusus_ada`':						
                        $stmt->bindValue($identifier, $this->wilayah_khusus_ada, PDO::PARAM_STR);
                        break;
                    case '`wilayah_khusus_tidak`':						
                        $stmt->bindValue($identifier, $this->wilayah_khusus_tidak, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setJumlahSekolahId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = JumlahSekolahPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JumlahSekolahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getJumlahSekolahId();
                break;
            case 1:
                return $this->getTanggal();
                break;
            case 2:
                return $this->getIdLevelWilayah();
                break;
            case 3:
                return $this->getKodeWilayah();
                break;
            case 4:
                return $this->getMstKodeWilayah();
                break;
            case 5:
                return $this->getSemesterId();
                break;
            case 6:
                return $this->getStatusNegeri();
                break;
            case 7:
                return $this->getStatusSwasta();
                break;
            case 8:
                return $this->getStatusSmaNegeri();
                break;
            case 9:
                return $this->getStatusSmaSwasta();
                break;
            case 10:
                return $this->getStatusSmkNegeri();
                break;
            case 11:
                return $this->getStatusSmkSwasta();
                break;
            case 12:
                return $this->getStatusSmlbNegeri();
                break;
            case 13:
                return $this->getStatusSmlbSwasta();
                break;
            case 14:
                return $this->getWaktuPenyelenggaraanPagi();
                break;
            case 15:
                return $this->getWaktuPenyelenggaraanSiang();
                break;
            case 16:
                return $this->getWaktuPenyelenggaraanSore();
                break;
            case 17:
                return $this->getWaktuPenyelenggaraanMalam();
                break;
            case 18:
                return $this->getWaktuPenyelenggaraanKombinasi();
                break;
            case 19:
                return $this->getWaktuPenyelenggaraanLainnya();
                break;
            case 20:
                return $this->getAkreditasiA();
                break;
            case 21:
                return $this->getAkreditasiB();
                break;
            case 22:
                return $this->getAkreditasiC();
                break;
            case 23:
                return $this->getAkreditasiTidak();
                break;
            case 24:
                return $this->getListrikAda();
                break;
            case 25:
                return $this->getListrikTidak();
                break;
            case 26:
                return $this->getSanitasiAda();
                break;
            case 27:
                return $this->getSanitasiTidak();
                break;
            case 28:
                return $this->getAksesInternetAda();
                break;
            case 29:
                return $this->getAksesInternetTidak();
                break;
            case 30:
                return $this->getMbsAda();
                break;
            case 31:
                return $this->getMbsTidak();
                break;
            case 32:
                return $this->getWilayahKhususAda();
                break;
            case 33:
                return $this->getWilayahKhususTidak();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['JumlahSekolah'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['JumlahSekolah'][$this->getPrimaryKey()] = true;
        $keys = JumlahSekolahPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getJumlahSekolahId(),
            $keys[1] => $this->getTanggal(),
            $keys[2] => $this->getIdLevelWilayah(),
            $keys[3] => $this->getKodeWilayah(),
            $keys[4] => $this->getMstKodeWilayah(),
            $keys[5] => $this->getSemesterId(),
            $keys[6] => $this->getStatusNegeri(),
            $keys[7] => $this->getStatusSwasta(),
            $keys[8] => $this->getStatusSmaNegeri(),
            $keys[9] => $this->getStatusSmaSwasta(),
            $keys[10] => $this->getStatusSmkNegeri(),
            $keys[11] => $this->getStatusSmkSwasta(),
            $keys[12] => $this->getStatusSmlbNegeri(),
            $keys[13] => $this->getStatusSmlbSwasta(),
            $keys[14] => $this->getWaktuPenyelenggaraanPagi(),
            $keys[15] => $this->getWaktuPenyelenggaraanSiang(),
            $keys[16] => $this->getWaktuPenyelenggaraanSore(),
            $keys[17] => $this->getWaktuPenyelenggaraanMalam(),
            $keys[18] => $this->getWaktuPenyelenggaraanKombinasi(),
            $keys[19] => $this->getWaktuPenyelenggaraanLainnya(),
            $keys[20] => $this->getAkreditasiA(),
            $keys[21] => $this->getAkreditasiB(),
            $keys[22] => $this->getAkreditasiC(),
            $keys[23] => $this->getAkreditasiTidak(),
            $keys[24] => $this->getListrikAda(),
            $keys[25] => $this->getListrikTidak(),
            $keys[26] => $this->getSanitasiAda(),
            $keys[27] => $this->getSanitasiTidak(),
            $keys[28] => $this->getAksesInternetAda(),
            $keys[29] => $this->getAksesInternetTidak(),
            $keys[30] => $this->getMbsAda(),
            $keys[31] => $this->getMbsTidak(),
            $keys[32] => $this->getWilayahKhususAda(),
            $keys[33] => $this->getWilayahKhususTidak(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JumlahSekolahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setJumlahSekolahId($value);
                break;
            case 1:
                $this->setTanggal($value);
                break;
            case 2:
                $this->setIdLevelWilayah($value);
                break;
            case 3:
                $this->setKodeWilayah($value);
                break;
            case 4:
                $this->setMstKodeWilayah($value);
                break;
            case 5:
                $this->setSemesterId($value);
                break;
            case 6:
                $this->setStatusNegeri($value);
                break;
            case 7:
                $this->setStatusSwasta($value);
                break;
            case 8:
                $this->setStatusSmaNegeri($value);
                break;
            case 9:
                $this->setStatusSmaSwasta($value);
                break;
            case 10:
                $this->setStatusSmkNegeri($value);
                break;
            case 11:
                $this->setStatusSmkSwasta($value);
                break;
            case 12:
                $this->setStatusSmlbNegeri($value);
                break;
            case 13:
                $this->setStatusSmlbSwasta($value);
                break;
            case 14:
                $this->setWaktuPenyelenggaraanPagi($value);
                break;
            case 15:
                $this->setWaktuPenyelenggaraanSiang($value);
                break;
            case 16:
                $this->setWaktuPenyelenggaraanSore($value);
                break;
            case 17:
                $this->setWaktuPenyelenggaraanMalam($value);
                break;
            case 18:
                $this->setWaktuPenyelenggaraanKombinasi($value);
                break;
            case 19:
                $this->setWaktuPenyelenggaraanLainnya($value);
                break;
            case 20:
                $this->setAkreditasiA($value);
                break;
            case 21:
                $this->setAkreditasiB($value);
                break;
            case 22:
                $this->setAkreditasiC($value);
                break;
            case 23:
                $this->setAkreditasiTidak($value);
                break;
            case 24:
                $this->setListrikAda($value);
                break;
            case 25:
                $this->setListrikTidak($value);
                break;
            case 26:
                $this->setSanitasiAda($value);
                break;
            case 27:
                $this->setSanitasiTidak($value);
                break;
            case 28:
                $this->setAksesInternetAda($value);
                break;
            case 29:
                $this->setAksesInternetTidak($value);
                break;
            case 30:
                $this->setMbsAda($value);
                break;
            case 31:
                $this->setMbsTidak($value);
                break;
            case 32:
                $this->setWilayahKhususAda($value);
                break;
            case 33:
                $this->setWilayahKhususTidak($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = JumlahSekolahPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setJumlahSekolahId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTanggal($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdLevelWilayah($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKodeWilayah($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMstKodeWilayah($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSemesterId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setStatusNegeri($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setStatusSwasta($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setStatusSmaNegeri($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setStatusSmaSwasta($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setStatusSmkNegeri($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setStatusSmkSwasta($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setStatusSmlbNegeri($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setStatusSmlbSwasta($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setWaktuPenyelenggaraanPagi($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setWaktuPenyelenggaraanSiang($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setWaktuPenyelenggaraanSore($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setWaktuPenyelenggaraanMalam($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setWaktuPenyelenggaraanKombinasi($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setWaktuPenyelenggaraanLainnya($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setAkreditasiA($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setAkreditasiB($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setAkreditasiC($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setAkreditasiTidak($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setListrikAda($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setListrikTidak($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setSanitasiAda($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setSanitasiTidak($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setAksesInternetAda($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setAksesInternetTidak($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setMbsAda($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setMbsTidak($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setWilayahKhususAda($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setWilayahKhususTidak($arr[$keys[33]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(JumlahSekolahPeer::DATABASE_NAME);

        if ($this->isColumnModified(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID)) $criteria->add(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $this->jumlah_sekolah_id);
        if ($this->isColumnModified(JumlahSekolahPeer::TANGGAL)) $criteria->add(JumlahSekolahPeer::TANGGAL, $this->tanggal);
        if ($this->isColumnModified(JumlahSekolahPeer::ID_LEVEL_WILAYAH)) $criteria->add(JumlahSekolahPeer::ID_LEVEL_WILAYAH, $this->id_level_wilayah);
        if ($this->isColumnModified(JumlahSekolahPeer::KODE_WILAYAH)) $criteria->add(JumlahSekolahPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(JumlahSekolahPeer::MST_KODE_WILAYAH)) $criteria->add(JumlahSekolahPeer::MST_KODE_WILAYAH, $this->mst_kode_wilayah);
        if ($this->isColumnModified(JumlahSekolahPeer::SEMESTER_ID)) $criteria->add(JumlahSekolahPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_NEGERI)) $criteria->add(JumlahSekolahPeer::STATUS_NEGERI, $this->status_negeri);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SWASTA)) $criteria->add(JumlahSekolahPeer::STATUS_SWASTA, $this->status_swasta);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMA_NEGERI)) $criteria->add(JumlahSekolahPeer::STATUS_SMA_NEGERI, $this->status_sma_negeri);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMA_SWASTA)) $criteria->add(JumlahSekolahPeer::STATUS_SMA_SWASTA, $this->status_sma_swasta);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMK_NEGERI)) $criteria->add(JumlahSekolahPeer::STATUS_SMK_NEGERI, $this->status_smk_negeri);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMK_SWASTA)) $criteria->add(JumlahSekolahPeer::STATUS_SMK_SWASTA, $this->status_smk_swasta);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMLB_NEGERI)) $criteria->add(JumlahSekolahPeer::STATUS_SMLB_NEGERI, $this->status_smlb_negeri);
        if ($this->isColumnModified(JumlahSekolahPeer::STATUS_SMLB_SWASTA)) $criteria->add(JumlahSekolahPeer::STATUS_SMLB_SWASTA, $this->status_smlb_swasta);
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI)) $criteria->add(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI, $this->waktu_penyelenggaraan_pagi);
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG)) $criteria->add(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG, $this->waktu_penyelenggaraan_siang);
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE)) $criteria->add(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE, $this->waktu_penyelenggaraan_sore);
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM)) $criteria->add(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM, $this->waktu_penyelenggaraan_malam);
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI)) $criteria->add(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI, $this->waktu_penyelenggaraan_kombinasi);
        if ($this->isColumnModified(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA)) $criteria->add(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA, $this->waktu_penyelenggaraan_lainnya);
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_A)) $criteria->add(JumlahSekolahPeer::AKREDITASI_A, $this->akreditasi_a);
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_B)) $criteria->add(JumlahSekolahPeer::AKREDITASI_B, $this->akreditasi_b);
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_C)) $criteria->add(JumlahSekolahPeer::AKREDITASI_C, $this->akreditasi_c);
        if ($this->isColumnModified(JumlahSekolahPeer::AKREDITASI_TIDAK)) $criteria->add(JumlahSekolahPeer::AKREDITASI_TIDAK, $this->akreditasi_tidak);
        if ($this->isColumnModified(JumlahSekolahPeer::LISTRIK_ADA)) $criteria->add(JumlahSekolahPeer::LISTRIK_ADA, $this->listrik_ada);
        if ($this->isColumnModified(JumlahSekolahPeer::LISTRIK_TIDAK)) $criteria->add(JumlahSekolahPeer::LISTRIK_TIDAK, $this->listrik_tidak);
        if ($this->isColumnModified(JumlahSekolahPeer::SANITASI_ADA)) $criteria->add(JumlahSekolahPeer::SANITASI_ADA, $this->sanitasi_ada);
        if ($this->isColumnModified(JumlahSekolahPeer::SANITASI_TIDAK)) $criteria->add(JumlahSekolahPeer::SANITASI_TIDAK, $this->sanitasi_tidak);
        if ($this->isColumnModified(JumlahSekolahPeer::AKSES_INTERNET_ADA)) $criteria->add(JumlahSekolahPeer::AKSES_INTERNET_ADA, $this->akses_internet_ada);
        if ($this->isColumnModified(JumlahSekolahPeer::AKSES_INTERNET_TIDAK)) $criteria->add(JumlahSekolahPeer::AKSES_INTERNET_TIDAK, $this->akses_internet_tidak);
        if ($this->isColumnModified(JumlahSekolahPeer::MBS_ADA)) $criteria->add(JumlahSekolahPeer::MBS_ADA, $this->mbs_ada);
        if ($this->isColumnModified(JumlahSekolahPeer::MBS_TIDAK)) $criteria->add(JumlahSekolahPeer::MBS_TIDAK, $this->mbs_tidak);
        if ($this->isColumnModified(JumlahSekolahPeer::WILAYAH_KHUSUS_ADA)) $criteria->add(JumlahSekolahPeer::WILAYAH_KHUSUS_ADA, $this->wilayah_khusus_ada);
        if ($this->isColumnModified(JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK)) $criteria->add(JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK, $this->wilayah_khusus_tidak);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(JumlahSekolahPeer::DATABASE_NAME);
        $criteria->add(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $this->jumlah_sekolah_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getJumlahSekolahId();
    }

    /**
     * Generic method to set the primary key (jumlah_sekolah_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setJumlahSekolahId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getJumlahSekolahId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of JumlahSekolah (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTanggal($this->getTanggal());
        $copyObj->setIdLevelWilayah($this->getIdLevelWilayah());
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setMstKodeWilayah($this->getMstKodeWilayah());
        $copyObj->setSemesterId($this->getSemesterId());
        $copyObj->setStatusNegeri($this->getStatusNegeri());
        $copyObj->setStatusSwasta($this->getStatusSwasta());
        $copyObj->setStatusSmaNegeri($this->getStatusSmaNegeri());
        $copyObj->setStatusSmaSwasta($this->getStatusSmaSwasta());
        $copyObj->setStatusSmkNegeri($this->getStatusSmkNegeri());
        $copyObj->setStatusSmkSwasta($this->getStatusSmkSwasta());
        $copyObj->setStatusSmlbNegeri($this->getStatusSmlbNegeri());
        $copyObj->setStatusSmlbSwasta($this->getStatusSmlbSwasta());
        $copyObj->setWaktuPenyelenggaraanPagi($this->getWaktuPenyelenggaraanPagi());
        $copyObj->setWaktuPenyelenggaraanSiang($this->getWaktuPenyelenggaraanSiang());
        $copyObj->setWaktuPenyelenggaraanSore($this->getWaktuPenyelenggaraanSore());
        $copyObj->setWaktuPenyelenggaraanMalam($this->getWaktuPenyelenggaraanMalam());
        $copyObj->setWaktuPenyelenggaraanKombinasi($this->getWaktuPenyelenggaraanKombinasi());
        $copyObj->setWaktuPenyelenggaraanLainnya($this->getWaktuPenyelenggaraanLainnya());
        $copyObj->setAkreditasiA($this->getAkreditasiA());
        $copyObj->setAkreditasiB($this->getAkreditasiB());
        $copyObj->setAkreditasiC($this->getAkreditasiC());
        $copyObj->setAkreditasiTidak($this->getAkreditasiTidak());
        $copyObj->setListrikAda($this->getListrikAda());
        $copyObj->setListrikTidak($this->getListrikTidak());
        $copyObj->setSanitasiAda($this->getSanitasiAda());
        $copyObj->setSanitasiTidak($this->getSanitasiTidak());
        $copyObj->setAksesInternetAda($this->getAksesInternetAda());
        $copyObj->setAksesInternetTidak($this->getAksesInternetTidak());
        $copyObj->setMbsAda($this->getMbsAda());
        $copyObj->setMbsTidak($this->getMbsTidak());
        $copyObj->setWilayahKhususAda($this->getWilayahKhususAda());
        $copyObj->setWilayahKhususTidak($this->getWilayahKhususTidak());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setJumlahSekolahId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return JumlahSekolah Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return JumlahSekolahPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new JumlahSekolahPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->jumlah_sekolah_id = null;
        $this->tanggal = null;
        $this->id_level_wilayah = null;
        $this->kode_wilayah = null;
        $this->mst_kode_wilayah = null;
        $this->semester_id = null;
        $this->status_negeri = null;
        $this->status_swasta = null;
        $this->status_sma_negeri = null;
        $this->status_sma_swasta = null;
        $this->status_smk_negeri = null;
        $this->status_smk_swasta = null;
        $this->status_smlb_negeri = null;
        $this->status_smlb_swasta = null;
        $this->waktu_penyelenggaraan_pagi = null;
        $this->waktu_penyelenggaraan_siang = null;
        $this->waktu_penyelenggaraan_sore = null;
        $this->waktu_penyelenggaraan_malam = null;
        $this->waktu_penyelenggaraan_kombinasi = null;
        $this->waktu_penyelenggaraan_lainnya = null;
        $this->akreditasi_a = null;
        $this->akreditasi_b = null;
        $this->akreditasi_c = null;
        $this->akreditasi_tidak = null;
        $this->listrik_ada = null;
        $this->listrik_tidak = null;
        $this->sanitasi_ada = null;
        $this->sanitasi_tidak = null;
        $this->akses_internet_ada = null;
        $this->akses_internet_tidak = null;
        $this->mbs_ada = null;
        $this->mbs_tidak = null;
        $this->wilayah_khusus_ada = null;
        $this->wilayah_khusus_tidak = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(JumlahSekolahPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
