<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Artikel;
use infopendataan\Model\ArtikelPeer;
use infopendataan\Model\ArtikelQuery;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'artikel' table.
 *
 * 
 *
 * @method ArtikelQuery orderByArtikelId($order = Criteria::ASC) Order by the ARTIKEL_ID column
 * @method ArtikelQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method ArtikelQuery orderByJudul($order = Criteria::ASC) Order by the JUDUL column
 * @method ArtikelQuery orderByIsi($order = Criteria::ASC) Order by the ISI column
 * @method ArtikelQuery orderByGambar($order = Criteria::ASC) Order by the GAMBAR column
 * @method ArtikelQuery orderByTanggalEdit($order = Criteria::ASC) Order by the TANGGAL_EDIT column
 *
 * @method ArtikelQuery groupByArtikelId() Group by the ARTIKEL_ID column
 * @method ArtikelQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method ArtikelQuery groupByJudul() Group by the JUDUL column
 * @method ArtikelQuery groupByIsi() Group by the ISI column
 * @method ArtikelQuery groupByGambar() Group by the GAMBAR column
 * @method ArtikelQuery groupByTanggalEdit() Group by the TANGGAL_EDIT column
 *
 * @method ArtikelQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ArtikelQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ArtikelQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ArtikelQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method ArtikelQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method ArtikelQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method Artikel findOne(PropelPDO $con = null) Return the first Artikel matching the query
 * @method Artikel findOneOrCreate(PropelPDO $con = null) Return the first Artikel matching the query, or a new Artikel object populated from the query conditions when no match is found
 *
 * @method Artikel findOneByPenggunaId(int $PENGGUNA_ID) Return the first Artikel filtered by the PENGGUNA_ID column
 * @method Artikel findOneByJudul(string $JUDUL) Return the first Artikel filtered by the JUDUL column
 * @method Artikel findOneByIsi(string $ISI) Return the first Artikel filtered by the ISI column
 * @method Artikel findOneByGambar(string $GAMBAR) Return the first Artikel filtered by the GAMBAR column
 * @method Artikel findOneByTanggalEdit(string $TANGGAL_EDIT) Return the first Artikel filtered by the TANGGAL_EDIT column
 *
 * @method array findByArtikelId(int $ARTIKEL_ID) Return Artikel objects filtered by the ARTIKEL_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return Artikel objects filtered by the PENGGUNA_ID column
 * @method array findByJudul(string $JUDUL) Return Artikel objects filtered by the JUDUL column
 * @method array findByIsi(string $ISI) Return Artikel objects filtered by the ISI column
 * @method array findByGambar(string $GAMBAR) Return Artikel objects filtered by the GAMBAR column
 * @method array findByTanggalEdit(string $TANGGAL_EDIT) Return Artikel objects filtered by the TANGGAL_EDIT column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseArtikelQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseArtikelQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\Artikel', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ArtikelQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ArtikelQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ArtikelQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ArtikelQuery) {
            return $criteria;
        }
        $query = new ArtikelQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Artikel|Artikel[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ArtikelPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ArtikelPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Artikel A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByArtikelId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Artikel A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ARTIKEL_ID`, `PENGGUNA_ID`, `JUDUL`, `ISI`, `GAMBAR`, `TANGGAL_EDIT` FROM `artikel` WHERE `ARTIKEL_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Artikel();
            $obj->hydrate($row);
            ArtikelPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Artikel|Artikel[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Artikel[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ArtikelPeer::ARTIKEL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ArtikelPeer::ARTIKEL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ARTIKEL_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByArtikelId(1234); // WHERE ARTIKEL_ID = 1234
     * $query->filterByArtikelId(array(12, 34)); // WHERE ARTIKEL_ID IN (12, 34)
     * $query->filterByArtikelId(array('min' => 12)); // WHERE ARTIKEL_ID >= 12
     * $query->filterByArtikelId(array('max' => 12)); // WHERE ARTIKEL_ID <= 12
     * </code>
     *
     * @param     mixed $artikelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByArtikelId($artikelId = null, $comparison = null)
    {
        if (is_array($artikelId)) {
            $useMinMax = false;
            if (isset($artikelId['min'])) {
                $this->addUsingAlias(ArtikelPeer::ARTIKEL_ID, $artikelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($artikelId['max'])) {
                $this->addUsingAlias(ArtikelPeer::ARTIKEL_ID, $artikelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArtikelPeer::ARTIKEL_ID, $artikelId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(ArtikelPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(ArtikelPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArtikelPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the JUDUL column
     *
     * Example usage:
     * <code>
     * $query->filterByJudul('fooValue');   // WHERE JUDUL = 'fooValue'
     * $query->filterByJudul('%fooValue%'); // WHERE JUDUL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $judul The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByJudul($judul = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($judul)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $judul)) {
                $judul = str_replace('*', '%', $judul);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ArtikelPeer::JUDUL, $judul, $comparison);
    }

    /**
     * Filter the query on the ISI column
     *
     * Example usage:
     * <code>
     * $query->filterByIsi('fooValue');   // WHERE ISI = 'fooValue'
     * $query->filterByIsi('%fooValue%'); // WHERE ISI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $isi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByIsi($isi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($isi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $isi)) {
                $isi = str_replace('*', '%', $isi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ArtikelPeer::ISI, $isi, $comparison);
    }

    /**
     * Filter the query on the GAMBAR column
     *
     * Example usage:
     * <code>
     * $query->filterByGambar('fooValue');   // WHERE GAMBAR = 'fooValue'
     * $query->filterByGambar('%fooValue%'); // WHERE GAMBAR LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gambar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByGambar($gambar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gambar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gambar)) {
                $gambar = str_replace('*', '%', $gambar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ArtikelPeer::GAMBAR, $gambar, $comparison);
    }

    /**
     * Filter the query on the TANGGAL_EDIT column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalEdit('2011-03-14'); // WHERE TANGGAL_EDIT = '2011-03-14'
     * $query->filterByTanggalEdit('now'); // WHERE TANGGAL_EDIT = '2011-03-14'
     * $query->filterByTanggalEdit(array('max' => 'yesterday')); // WHERE TANGGAL_EDIT > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggalEdit The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function filterByTanggalEdit($tanggalEdit = null, $comparison = null)
    {
        if (is_array($tanggalEdit)) {
            $useMinMax = false;
            if (isset($tanggalEdit['min'])) {
                $this->addUsingAlias(ArtikelPeer::TANGGAL_EDIT, $tanggalEdit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggalEdit['max'])) {
                $this->addUsingAlias(ArtikelPeer::TANGGAL_EDIT, $tanggalEdit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArtikelPeer::TANGGAL_EDIT, $tanggalEdit, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ArtikelQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(ArtikelPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ArtikelPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Artikel $artikel Object to remove from the list of results
     *
     * @return ArtikelQuery The current query, for fluid interface
     */
    public function prune($artikel = null)
    {
        if ($artikel) {
            $this->addUsingAlias(ArtikelPeer::ARTIKEL_ID, $artikel->getArtikelId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
