<?php

namespace infopendataan\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\MstWilayah;
use infopendataan\Model\MstWilayahPeer;
use infopendataan\Model\map\MstWilayahTableMap;

/**
 * Base static class for performing query and update operations on the 'mst_wilayah' table.
 *
 * 
 *
 * @package propel.generator.infopendataan.Model.om
 */
abstract class BaseMstWilayahPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'infopendataan';

    /** the table name for this class */
    const TABLE_NAME = 'mst_wilayah';

    /** the related Propel class for this table */
    const OM_CLASS = 'infopendataan\\Model\\MstWilayah';

    /** the related TableMap class for this table */
    const TM_CLASS = 'MstWilayahTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the wilayah_id field */
    const WILAYAH_ID = 'mst_wilayah.wilayah_id';

    /** the column name for the kode_wilayah field */
    const KODE_WILAYAH = 'mst_wilayah.kode_wilayah';

    /** the column name for the nama field */
    const NAMA = 'mst_wilayah.nama';

    /** the column name for the id_level_wilayah field */
    const ID_LEVEL_WILAYAH = 'mst_wilayah.id_level_wilayah';

    /** the column name for the mst_kode_wilayah field */
    const MST_KODE_WILAYAH = 'mst_wilayah.mst_kode_wilayah';

    /** the column name for the negara_id field */
    const NEGARA_ID = 'mst_wilayah.negara_id';

    /** the column name for the asal_wilayah field */
    const ASAL_WILAYAH = 'mst_wilayah.asal_wilayah';

    /** the column name for the kode_bps field */
    const KODE_BPS = 'mst_wilayah.kode_bps';

    /** the column name for the kode_dagri field */
    const KODE_DAGRI = 'mst_wilayah.kode_dagri';

    /** the column name for the kode_keu field */
    const KODE_KEU = 'mst_wilayah.kode_keu';

    /** the column name for the create_date field */
    const CREATE_DATE = 'mst_wilayah.create_date';

    /** the column name for the last_update field */
    const LAST_UPDATE = 'mst_wilayah.last_update';

    /** the column name for the expired_date field */
    const EXPIRED_DATE = 'mst_wilayah.expired_date';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'mst_wilayah.last_sync';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of MstWilayah objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array MstWilayah[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. MstWilayahPeer::$fieldNames[MstWilayahPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('WilayahId', 'KodeWilayah', 'Nama', 'IdLevelWilayah', 'MstKodeWilayah', 'NegaraId', 'AsalWilayah', 'KodeBps', 'KodeDagri', 'KodeKeu', 'CreateDate', 'LastUpdate', 'ExpiredDate', 'LastSync', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('wilayahId', 'kodeWilayah', 'nama', 'idLevelWilayah', 'mstKodeWilayah', 'negaraId', 'asalWilayah', 'kodeBps', 'kodeDagri', 'kodeKeu', 'createDate', 'lastUpdate', 'expiredDate', 'lastSync', ),
        BasePeer::TYPE_COLNAME => array (MstWilayahPeer::WILAYAH_ID, MstWilayahPeer::KODE_WILAYAH, MstWilayahPeer::NAMA, MstWilayahPeer::ID_LEVEL_WILAYAH, MstWilayahPeer::MST_KODE_WILAYAH, MstWilayahPeer::NEGARA_ID, MstWilayahPeer::ASAL_WILAYAH, MstWilayahPeer::KODE_BPS, MstWilayahPeer::KODE_DAGRI, MstWilayahPeer::KODE_KEU, MstWilayahPeer::CREATE_DATE, MstWilayahPeer::LAST_UPDATE, MstWilayahPeer::EXPIRED_DATE, MstWilayahPeer::LAST_SYNC, ),
        BasePeer::TYPE_RAW_COLNAME => array ('WILAYAH_ID', 'KODE_WILAYAH', 'NAMA', 'ID_LEVEL_WILAYAH', 'MST_KODE_WILAYAH', 'NEGARA_ID', 'ASAL_WILAYAH', 'KODE_BPS', 'KODE_DAGRI', 'KODE_KEU', 'CREATE_DATE', 'LAST_UPDATE', 'EXPIRED_DATE', 'LAST_SYNC', ),
        BasePeer::TYPE_FIELDNAME => array ('wilayah_id', 'kode_wilayah', 'nama', 'id_level_wilayah', 'mst_kode_wilayah', 'negara_id', 'asal_wilayah', 'kode_bps', 'kode_dagri', 'kode_keu', 'create_date', 'last_update', 'expired_date', 'last_sync', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. MstWilayahPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('WilayahId' => 0, 'KodeWilayah' => 1, 'Nama' => 2, 'IdLevelWilayah' => 3, 'MstKodeWilayah' => 4, 'NegaraId' => 5, 'AsalWilayah' => 6, 'KodeBps' => 7, 'KodeDagri' => 8, 'KodeKeu' => 9, 'CreateDate' => 10, 'LastUpdate' => 11, 'ExpiredDate' => 12, 'LastSync' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('wilayahId' => 0, 'kodeWilayah' => 1, 'nama' => 2, 'idLevelWilayah' => 3, 'mstKodeWilayah' => 4, 'negaraId' => 5, 'asalWilayah' => 6, 'kodeBps' => 7, 'kodeDagri' => 8, 'kodeKeu' => 9, 'createDate' => 10, 'lastUpdate' => 11, 'expiredDate' => 12, 'lastSync' => 13, ),
        BasePeer::TYPE_COLNAME => array (MstWilayahPeer::WILAYAH_ID => 0, MstWilayahPeer::KODE_WILAYAH => 1, MstWilayahPeer::NAMA => 2, MstWilayahPeer::ID_LEVEL_WILAYAH => 3, MstWilayahPeer::MST_KODE_WILAYAH => 4, MstWilayahPeer::NEGARA_ID => 5, MstWilayahPeer::ASAL_WILAYAH => 6, MstWilayahPeer::KODE_BPS => 7, MstWilayahPeer::KODE_DAGRI => 8, MstWilayahPeer::KODE_KEU => 9, MstWilayahPeer::CREATE_DATE => 10, MstWilayahPeer::LAST_UPDATE => 11, MstWilayahPeer::EXPIRED_DATE => 12, MstWilayahPeer::LAST_SYNC => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('WILAYAH_ID' => 0, 'KODE_WILAYAH' => 1, 'NAMA' => 2, 'ID_LEVEL_WILAYAH' => 3, 'MST_KODE_WILAYAH' => 4, 'NEGARA_ID' => 5, 'ASAL_WILAYAH' => 6, 'KODE_BPS' => 7, 'KODE_DAGRI' => 8, 'KODE_KEU' => 9, 'CREATE_DATE' => 10, 'LAST_UPDATE' => 11, 'EXPIRED_DATE' => 12, 'LAST_SYNC' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('wilayah_id' => 0, 'kode_wilayah' => 1, 'nama' => 2, 'id_level_wilayah' => 3, 'mst_kode_wilayah' => 4, 'negara_id' => 5, 'asal_wilayah' => 6, 'kode_bps' => 7, 'kode_dagri' => 8, 'kode_keu' => 9, 'create_date' => 10, 'last_update' => 11, 'expired_date' => 12, 'last_sync' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = MstWilayahPeer::getFieldNames($toType);
        $key = isset(MstWilayahPeer::$fieldKeys[$fromType][$name]) ? MstWilayahPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(MstWilayahPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, MstWilayahPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return MstWilayahPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. MstWilayahPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(MstWilayahPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MstWilayahPeer::WILAYAH_ID);
            $criteria->addSelectColumn(MstWilayahPeer::KODE_WILAYAH);
            $criteria->addSelectColumn(MstWilayahPeer::NAMA);
            $criteria->addSelectColumn(MstWilayahPeer::ID_LEVEL_WILAYAH);
            $criteria->addSelectColumn(MstWilayahPeer::MST_KODE_WILAYAH);
            $criteria->addSelectColumn(MstWilayahPeer::NEGARA_ID);
            $criteria->addSelectColumn(MstWilayahPeer::ASAL_WILAYAH);
            $criteria->addSelectColumn(MstWilayahPeer::KODE_BPS);
            $criteria->addSelectColumn(MstWilayahPeer::KODE_DAGRI);
            $criteria->addSelectColumn(MstWilayahPeer::KODE_KEU);
            $criteria->addSelectColumn(MstWilayahPeer::CREATE_DATE);
            $criteria->addSelectColumn(MstWilayahPeer::LAST_UPDATE);
            $criteria->addSelectColumn(MstWilayahPeer::EXPIRED_DATE);
            $criteria->addSelectColumn(MstWilayahPeer::LAST_SYNC);
        } else {
            $criteria->addSelectColumn($alias . '.wilayah_id');
            $criteria->addSelectColumn($alias . '.kode_wilayah');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.id_level_wilayah');
            $criteria->addSelectColumn($alias . '.mst_kode_wilayah');
            $criteria->addSelectColumn($alias . '.negara_id');
            $criteria->addSelectColumn($alias . '.asal_wilayah');
            $criteria->addSelectColumn($alias . '.kode_bps');
            $criteria->addSelectColumn($alias . '.kode_dagri');
            $criteria->addSelectColumn($alias . '.kode_keu');
            $criteria->addSelectColumn($alias . '.create_date');
            $criteria->addSelectColumn($alias . '.last_update');
            $criteria->addSelectColumn($alias . '.expired_date');
            $criteria->addSelectColumn($alias . '.last_sync');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(MstWilayahPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            MstWilayahPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(MstWilayahPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 MstWilayah
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = MstWilayahPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return MstWilayahPeer::populateObjects(MstWilayahPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            MstWilayahPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(MstWilayahPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      MstWilayah $obj A MstWilayah object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getWilayahId();
            } // if key === null
            MstWilayahPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A MstWilayah object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof MstWilayah) {
                $key = (string) $value->getWilayahId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or MstWilayah object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(MstWilayahPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   MstWilayah Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(MstWilayahPeer::$instances[$key])) {
                return MstWilayahPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (MstWilayahPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        MstWilayahPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to mst_wilayah
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = MstWilayahPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = MstWilayahPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = MstWilayahPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MstWilayahPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (MstWilayah object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = MstWilayahPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + MstWilayahPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MstWilayahPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            MstWilayahPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(MstWilayahPeer::DATABASE_NAME)->getTable(MstWilayahPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseMstWilayahPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseMstWilayahPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new MstWilayahTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return MstWilayahPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a MstWilayah or Criteria object.
     *
     * @param      mixed $values Criteria or MstWilayah object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from MstWilayah object
        }


        // Set the correct dbName
        $criteria->setDbName(MstWilayahPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a MstWilayah or Criteria object.
     *
     * @param      mixed $values Criteria or MstWilayah object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(MstWilayahPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(MstWilayahPeer::WILAYAH_ID);
            $value = $criteria->remove(MstWilayahPeer::WILAYAH_ID);
            if ($value) {
                $selectCriteria->add(MstWilayahPeer::WILAYAH_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(MstWilayahPeer::TABLE_NAME);
            }

        } else { // $values is MstWilayah object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(MstWilayahPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the mst_wilayah table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(MstWilayahPeer::TABLE_NAME, $con, MstWilayahPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MstWilayahPeer::clearInstancePool();
            MstWilayahPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a MstWilayah or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or MstWilayah object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            MstWilayahPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof MstWilayah) { // it's a model object
            // invalidate the cache for this single object
            MstWilayahPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);
            $criteria->add(MstWilayahPeer::WILAYAH_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                MstWilayahPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(MstWilayahPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            MstWilayahPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given MstWilayah object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      MstWilayah $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(MstWilayahPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(MstWilayahPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(MstWilayahPeer::DATABASE_NAME, MstWilayahPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return MstWilayah
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = MstWilayahPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);
        $criteria->add(MstWilayahPeer::WILAYAH_ID, $pk);

        $v = MstWilayahPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return MstWilayah[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);
            $criteria->add(MstWilayahPeer::WILAYAH_ID, $pks, Criteria::IN);
            $objs = MstWilayahPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseMstWilayahPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseMstWilayahPeer::buildTableMap();

