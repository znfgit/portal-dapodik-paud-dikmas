<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\MediaSosial;
use infopendataan\Model\MediaSosialPeer;
use infopendataan\Model\MediaSosialQuery;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'media_sosial' table.
 *
 * 
 *
 * @method MediaSosialQuery orderByMediaSosialId($order = Criteria::ASC) Order by the MEDIA_SOSIAL_ID column
 * @method MediaSosialQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method MediaSosialQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 * @method MediaSosialQuery orderByUrl($order = Criteria::ASC) Order by the URL column
 * @method MediaSosialQuery orderByLogo($order = Criteria::ASC) Order by the LOGO column
 * @method MediaSosialQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 *
 * @method MediaSosialQuery groupByMediaSosialId() Group by the MEDIA_SOSIAL_ID column
 * @method MediaSosialQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method MediaSosialQuery groupByNama() Group by the NAMA column
 * @method MediaSosialQuery groupByUrl() Group by the URL column
 * @method MediaSosialQuery groupByLogo() Group by the LOGO column
 * @method MediaSosialQuery groupByTanggal() Group by the TANGGAL column
 *
 * @method MediaSosialQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method MediaSosialQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method MediaSosialQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method MediaSosialQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method MediaSosialQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method MediaSosialQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method MediaSosial findOne(PropelPDO $con = null) Return the first MediaSosial matching the query
 * @method MediaSosial findOneOrCreate(PropelPDO $con = null) Return the first MediaSosial matching the query, or a new MediaSosial object populated from the query conditions when no match is found
 *
 * @method MediaSosial findOneByPenggunaId(int $PENGGUNA_ID) Return the first MediaSosial filtered by the PENGGUNA_ID column
 * @method MediaSosial findOneByNama(string $NAMA) Return the first MediaSosial filtered by the NAMA column
 * @method MediaSosial findOneByUrl(string $URL) Return the first MediaSosial filtered by the URL column
 * @method MediaSosial findOneByLogo(string $LOGO) Return the first MediaSosial filtered by the LOGO column
 * @method MediaSosial findOneByTanggal(string $TANGGAL) Return the first MediaSosial filtered by the TANGGAL column
 *
 * @method array findByMediaSosialId(int $MEDIA_SOSIAL_ID) Return MediaSosial objects filtered by the MEDIA_SOSIAL_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return MediaSosial objects filtered by the PENGGUNA_ID column
 * @method array findByNama(string $NAMA) Return MediaSosial objects filtered by the NAMA column
 * @method array findByUrl(string $URL) Return MediaSosial objects filtered by the URL column
 * @method array findByLogo(string $LOGO) Return MediaSosial objects filtered by the LOGO column
 * @method array findByTanggal(string $TANGGAL) Return MediaSosial objects filtered by the TANGGAL column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseMediaSosialQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseMediaSosialQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\MediaSosial', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MediaSosialQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   MediaSosialQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MediaSosialQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MediaSosialQuery) {
            return $criteria;
        }
        $query = new MediaSosialQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   MediaSosial|MediaSosial[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MediaSosialPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MediaSosialPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 MediaSosial A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByMediaSosialId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 MediaSosial A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `MEDIA_SOSIAL_ID`, `PENGGUNA_ID`, `NAMA`, `URL`, `LOGO`, `TANGGAL` FROM `media_sosial` WHERE `MEDIA_SOSIAL_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new MediaSosial();
            $obj->hydrate($row);
            MediaSosialPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return MediaSosial|MediaSosial[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|MediaSosial[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MediaSosialPeer::MEDIA_SOSIAL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MediaSosialPeer::MEDIA_SOSIAL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the MEDIA_SOSIAL_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByMediaSosialId(1234); // WHERE MEDIA_SOSIAL_ID = 1234
     * $query->filterByMediaSosialId(array(12, 34)); // WHERE MEDIA_SOSIAL_ID IN (12, 34)
     * $query->filterByMediaSosialId(array('min' => 12)); // WHERE MEDIA_SOSIAL_ID >= 12
     * $query->filterByMediaSosialId(array('max' => 12)); // WHERE MEDIA_SOSIAL_ID <= 12
     * </code>
     *
     * @param     mixed $mediaSosialId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByMediaSosialId($mediaSosialId = null, $comparison = null)
    {
        if (is_array($mediaSosialId)) {
            $useMinMax = false;
            if (isset($mediaSosialId['min'])) {
                $this->addUsingAlias(MediaSosialPeer::MEDIA_SOSIAL_ID, $mediaSosialId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mediaSosialId['max'])) {
                $this->addUsingAlias(MediaSosialPeer::MEDIA_SOSIAL_ID, $mediaSosialId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaSosialPeer::MEDIA_SOSIAL_ID, $mediaSosialId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(MediaSosialPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(MediaSosialPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaSosialPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MediaSosialPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the URL column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE URL = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE URL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $url)) {
                $url = str_replace('*', '%', $url);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MediaSosialPeer::URL, $url, $comparison);
    }

    /**
     * Filter the query on the LOGO column
     *
     * Example usage:
     * <code>
     * $query->filterByLogo('fooValue');   // WHERE LOGO = 'fooValue'
     * $query->filterByLogo('%fooValue%'); // WHERE LOGO LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByLogo($logo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $logo)) {
                $logo = str_replace('*', '%', $logo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MediaSosialPeer::LOGO, $logo, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(MediaSosialPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(MediaSosialPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaSosialPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MediaSosialQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(MediaSosialPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MediaSosialPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   MediaSosial $mediaSosial Object to remove from the list of results
     *
     * @return MediaSosialQuery The current query, for fluid interface
     */
    public function prune($mediaSosial = null)
    {
        if ($mediaSosial) {
            $this->addUsingAlias(MediaSosialPeer::MEDIA_SOSIAL_ID, $mediaSosial->getMediaSosialId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
