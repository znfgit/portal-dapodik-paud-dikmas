<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\JumlahPdStatus;
use infopendataan\Model\JumlahPdStatusPeer;
use infopendataan\Model\JumlahPdStatusQuery;

/**
 * Base class that represents a query for the 'jumlah_pd_status' table.
 *
 * 
 *
 * @method JumlahPdStatusQuery orderByJumlahPdStatusId($order = Criteria::ASC) Order by the jumlah_pd_status_id column
 * @method JumlahPdStatusQuery orderByTanggal($order = Criteria::ASC) Order by the tanggal column
 * @method JumlahPdStatusQuery orderByIdLevelWilayah($order = Criteria::ASC) Order by the id_level_wilayah column
 * @method JumlahPdStatusQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method JumlahPdStatusQuery orderByMstKodeWilayah($order = Criteria::ASC) Order by the mst_kode_wilayah column
 * @method JumlahPdStatusQuery orderByTahunAjaranId($order = Criteria::ASC) Order by the tahun_ajaran_id column
 * @method JumlahPdStatusQuery orderByPdTerdaftarNegeri($order = Criteria::ASC) Order by the pd_terdaftar_negeri column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSwasta($order = Criteria::ASC) Order by the pd_terdaftar_swasta column
 * @method JumlahPdStatusQuery orderByPdRombelNegeri($order = Criteria::ASC) Order by the pd_rombel_negeri column
 * @method JumlahPdStatusQuery orderByPdRombelSwasta($order = Criteria::ASC) Order by the pd_rombel_swasta column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSmaNegeri($order = Criteria::ASC) Order by the pd_terdaftar_sma_negeri column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSmaSwasta($order = Criteria::ASC) Order by the pd_terdaftar_sma_swasta column
 * @method JumlahPdStatusQuery orderByPdRombelSmaNegeri($order = Criteria::ASC) Order by the pd_rombel_sma_negeri column
 * @method JumlahPdStatusQuery orderByPdRombelSmaSwasta($order = Criteria::ASC) Order by the pd_rombel_sma_swasta column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSmkNegeri($order = Criteria::ASC) Order by the pd_terdaftar_smk_negeri column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSmkSwasta($order = Criteria::ASC) Order by the pd_terdaftar_smk_swasta column
 * @method JumlahPdStatusQuery orderByPdRombelSmkNegeri($order = Criteria::ASC) Order by the pd_rombel_smk_negeri column
 * @method JumlahPdStatusQuery orderByPdRombelSmkSwasta($order = Criteria::ASC) Order by the pd_rombel_smk_swasta column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSmlbNegeri($order = Criteria::ASC) Order by the pd_terdaftar_smlb_negeri column
 * @method JumlahPdStatusQuery orderByPdTerdaftarSmlbSwasta($order = Criteria::ASC) Order by the pd_terdaftar_smlb_swasta column
 * @method JumlahPdStatusQuery orderByPdRombelSmlbNegeri($order = Criteria::ASC) Order by the pd_rombel_smlb_negeri column
 * @method JumlahPdStatusQuery orderByPdRombelSmlbSwasta($order = Criteria::ASC) Order by the pd_rombel_smlb_swasta column
 *
 * @method JumlahPdStatusQuery groupByJumlahPdStatusId() Group by the jumlah_pd_status_id column
 * @method JumlahPdStatusQuery groupByTanggal() Group by the tanggal column
 * @method JumlahPdStatusQuery groupByIdLevelWilayah() Group by the id_level_wilayah column
 * @method JumlahPdStatusQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method JumlahPdStatusQuery groupByMstKodeWilayah() Group by the mst_kode_wilayah column
 * @method JumlahPdStatusQuery groupByTahunAjaranId() Group by the tahun_ajaran_id column
 * @method JumlahPdStatusQuery groupByPdTerdaftarNegeri() Group by the pd_terdaftar_negeri column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSwasta() Group by the pd_terdaftar_swasta column
 * @method JumlahPdStatusQuery groupByPdRombelNegeri() Group by the pd_rombel_negeri column
 * @method JumlahPdStatusQuery groupByPdRombelSwasta() Group by the pd_rombel_swasta column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSmaNegeri() Group by the pd_terdaftar_sma_negeri column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSmaSwasta() Group by the pd_terdaftar_sma_swasta column
 * @method JumlahPdStatusQuery groupByPdRombelSmaNegeri() Group by the pd_rombel_sma_negeri column
 * @method JumlahPdStatusQuery groupByPdRombelSmaSwasta() Group by the pd_rombel_sma_swasta column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSmkNegeri() Group by the pd_terdaftar_smk_negeri column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSmkSwasta() Group by the pd_terdaftar_smk_swasta column
 * @method JumlahPdStatusQuery groupByPdRombelSmkNegeri() Group by the pd_rombel_smk_negeri column
 * @method JumlahPdStatusQuery groupByPdRombelSmkSwasta() Group by the pd_rombel_smk_swasta column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSmlbNegeri() Group by the pd_terdaftar_smlb_negeri column
 * @method JumlahPdStatusQuery groupByPdTerdaftarSmlbSwasta() Group by the pd_terdaftar_smlb_swasta column
 * @method JumlahPdStatusQuery groupByPdRombelSmlbNegeri() Group by the pd_rombel_smlb_negeri column
 * @method JumlahPdStatusQuery groupByPdRombelSmlbSwasta() Group by the pd_rombel_smlb_swasta column
 *
 * @method JumlahPdStatusQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JumlahPdStatusQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JumlahPdStatusQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JumlahPdStatus findOne(PropelPDO $con = null) Return the first JumlahPdStatus matching the query
 * @method JumlahPdStatus findOneOrCreate(PropelPDO $con = null) Return the first JumlahPdStatus matching the query, or a new JumlahPdStatus object populated from the query conditions when no match is found
 *
 * @method JumlahPdStatus findOneByTanggal(string $tanggal) Return the first JumlahPdStatus filtered by the tanggal column
 * @method JumlahPdStatus findOneByIdLevelWilayah(double $id_level_wilayah) Return the first JumlahPdStatus filtered by the id_level_wilayah column
 * @method JumlahPdStatus findOneByKodeWilayah(string $kode_wilayah) Return the first JumlahPdStatus filtered by the kode_wilayah column
 * @method JumlahPdStatus findOneByMstKodeWilayah(string $mst_kode_wilayah) Return the first JumlahPdStatus filtered by the mst_kode_wilayah column
 * @method JumlahPdStatus findOneByTahunAjaranId(string $tahun_ajaran_id) Return the first JumlahPdStatus filtered by the tahun_ajaran_id column
 * @method JumlahPdStatus findOneByPdTerdaftarNegeri(double $pd_terdaftar_negeri) Return the first JumlahPdStatus filtered by the pd_terdaftar_negeri column
 * @method JumlahPdStatus findOneByPdTerdaftarSwasta(double $pd_terdaftar_swasta) Return the first JumlahPdStatus filtered by the pd_terdaftar_swasta column
 * @method JumlahPdStatus findOneByPdRombelNegeri(double $pd_rombel_negeri) Return the first JumlahPdStatus filtered by the pd_rombel_negeri column
 * @method JumlahPdStatus findOneByPdRombelSwasta(double $pd_rombel_swasta) Return the first JumlahPdStatus filtered by the pd_rombel_swasta column
 * @method JumlahPdStatus findOneByPdTerdaftarSmaNegeri(double $pd_terdaftar_sma_negeri) Return the first JumlahPdStatus filtered by the pd_terdaftar_sma_negeri column
 * @method JumlahPdStatus findOneByPdTerdaftarSmaSwasta(double $pd_terdaftar_sma_swasta) Return the first JumlahPdStatus filtered by the pd_terdaftar_sma_swasta column
 * @method JumlahPdStatus findOneByPdRombelSmaNegeri(double $pd_rombel_sma_negeri) Return the first JumlahPdStatus filtered by the pd_rombel_sma_negeri column
 * @method JumlahPdStatus findOneByPdRombelSmaSwasta(double $pd_rombel_sma_swasta) Return the first JumlahPdStatus filtered by the pd_rombel_sma_swasta column
 * @method JumlahPdStatus findOneByPdTerdaftarSmkNegeri(double $pd_terdaftar_smk_negeri) Return the first JumlahPdStatus filtered by the pd_terdaftar_smk_negeri column
 * @method JumlahPdStatus findOneByPdTerdaftarSmkSwasta(double $pd_terdaftar_smk_swasta) Return the first JumlahPdStatus filtered by the pd_terdaftar_smk_swasta column
 * @method JumlahPdStatus findOneByPdRombelSmkNegeri(double $pd_rombel_smk_negeri) Return the first JumlahPdStatus filtered by the pd_rombel_smk_negeri column
 * @method JumlahPdStatus findOneByPdRombelSmkSwasta(double $pd_rombel_smk_swasta) Return the first JumlahPdStatus filtered by the pd_rombel_smk_swasta column
 * @method JumlahPdStatus findOneByPdTerdaftarSmlbNegeri(double $pd_terdaftar_smlb_negeri) Return the first JumlahPdStatus filtered by the pd_terdaftar_smlb_negeri column
 * @method JumlahPdStatus findOneByPdTerdaftarSmlbSwasta(double $pd_terdaftar_smlb_swasta) Return the first JumlahPdStatus filtered by the pd_terdaftar_smlb_swasta column
 * @method JumlahPdStatus findOneByPdRombelSmlbNegeri(double $pd_rombel_smlb_negeri) Return the first JumlahPdStatus filtered by the pd_rombel_smlb_negeri column
 * @method JumlahPdStatus findOneByPdRombelSmlbSwasta(double $pd_rombel_smlb_swasta) Return the first JumlahPdStatus filtered by the pd_rombel_smlb_swasta column
 *
 * @method array findByJumlahPdStatusId(int $jumlah_pd_status_id) Return JumlahPdStatus objects filtered by the jumlah_pd_status_id column
 * @method array findByTanggal(string $tanggal) Return JumlahPdStatus objects filtered by the tanggal column
 * @method array findByIdLevelWilayah(double $id_level_wilayah) Return JumlahPdStatus objects filtered by the id_level_wilayah column
 * @method array findByKodeWilayah(string $kode_wilayah) Return JumlahPdStatus objects filtered by the kode_wilayah column
 * @method array findByMstKodeWilayah(string $mst_kode_wilayah) Return JumlahPdStatus objects filtered by the mst_kode_wilayah column
 * @method array findByTahunAjaranId(string $tahun_ajaran_id) Return JumlahPdStatus objects filtered by the tahun_ajaran_id column
 * @method array findByPdTerdaftarNegeri(double $pd_terdaftar_negeri) Return JumlahPdStatus objects filtered by the pd_terdaftar_negeri column
 * @method array findByPdTerdaftarSwasta(double $pd_terdaftar_swasta) Return JumlahPdStatus objects filtered by the pd_terdaftar_swasta column
 * @method array findByPdRombelNegeri(double $pd_rombel_negeri) Return JumlahPdStatus objects filtered by the pd_rombel_negeri column
 * @method array findByPdRombelSwasta(double $pd_rombel_swasta) Return JumlahPdStatus objects filtered by the pd_rombel_swasta column
 * @method array findByPdTerdaftarSmaNegeri(double $pd_terdaftar_sma_negeri) Return JumlahPdStatus objects filtered by the pd_terdaftar_sma_negeri column
 * @method array findByPdTerdaftarSmaSwasta(double $pd_terdaftar_sma_swasta) Return JumlahPdStatus objects filtered by the pd_terdaftar_sma_swasta column
 * @method array findByPdRombelSmaNegeri(double $pd_rombel_sma_negeri) Return JumlahPdStatus objects filtered by the pd_rombel_sma_negeri column
 * @method array findByPdRombelSmaSwasta(double $pd_rombel_sma_swasta) Return JumlahPdStatus objects filtered by the pd_rombel_sma_swasta column
 * @method array findByPdTerdaftarSmkNegeri(double $pd_terdaftar_smk_negeri) Return JumlahPdStatus objects filtered by the pd_terdaftar_smk_negeri column
 * @method array findByPdTerdaftarSmkSwasta(double $pd_terdaftar_smk_swasta) Return JumlahPdStatus objects filtered by the pd_terdaftar_smk_swasta column
 * @method array findByPdRombelSmkNegeri(double $pd_rombel_smk_negeri) Return JumlahPdStatus objects filtered by the pd_rombel_smk_negeri column
 * @method array findByPdRombelSmkSwasta(double $pd_rombel_smk_swasta) Return JumlahPdStatus objects filtered by the pd_rombel_smk_swasta column
 * @method array findByPdTerdaftarSmlbNegeri(double $pd_terdaftar_smlb_negeri) Return JumlahPdStatus objects filtered by the pd_terdaftar_smlb_negeri column
 * @method array findByPdTerdaftarSmlbSwasta(double $pd_terdaftar_smlb_swasta) Return JumlahPdStatus objects filtered by the pd_terdaftar_smlb_swasta column
 * @method array findByPdRombelSmlbNegeri(double $pd_rombel_smlb_negeri) Return JumlahPdStatus objects filtered by the pd_rombel_smlb_negeri column
 * @method array findByPdRombelSmlbSwasta(double $pd_rombel_smlb_swasta) Return JumlahPdStatus objects filtered by the pd_rombel_smlb_swasta column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJumlahPdStatusQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJumlahPdStatusQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JumlahPdStatus', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JumlahPdStatusQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JumlahPdStatusQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JumlahPdStatusQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JumlahPdStatusQuery) {
            return $criteria;
        }
        $query = new JumlahPdStatusQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JumlahPdStatus|JumlahPdStatus[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JumlahPdStatusPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JumlahPdStatus A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJumlahPdStatusId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JumlahPdStatus A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `jumlah_pd_status_id`, `tanggal`, `id_level_wilayah`, `kode_wilayah`, `mst_kode_wilayah`, `tahun_ajaran_id`, `pd_terdaftar_negeri`, `pd_terdaftar_swasta`, `pd_rombel_negeri`, `pd_rombel_swasta`, `pd_terdaftar_sma_negeri`, `pd_terdaftar_sma_swasta`, `pd_rombel_sma_negeri`, `pd_rombel_sma_swasta`, `pd_terdaftar_smk_negeri`, `pd_terdaftar_smk_swasta`, `pd_rombel_smk_negeri`, `pd_rombel_smk_swasta`, `pd_terdaftar_smlb_negeri`, `pd_terdaftar_smlb_swasta`, `pd_rombel_smlb_negeri`, `pd_rombel_smlb_swasta` FROM `jumlah_pd_status` WHERE `jumlah_pd_status_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JumlahPdStatus();
            $obj->hydrate($row);
            JumlahPdStatusPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JumlahPdStatus|JumlahPdStatus[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JumlahPdStatus[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the jumlah_pd_status_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahPdStatusId(1234); // WHERE jumlah_pd_status_id = 1234
     * $query->filterByJumlahPdStatusId(array(12, 34)); // WHERE jumlah_pd_status_id IN (12, 34)
     * $query->filterByJumlahPdStatusId(array('min' => 12)); // WHERE jumlah_pd_status_id >= 12
     * $query->filterByJumlahPdStatusId(array('max' => 12)); // WHERE jumlah_pd_status_id <= 12
     * </code>
     *
     * @param     mixed $jumlahPdStatusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByJumlahPdStatusId($jumlahPdStatusId = null, $comparison = null)
    {
        if (is_array($jumlahPdStatusId)) {
            $useMinMax = false;
            if (isset($jumlahPdStatusId['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $jumlahPdStatusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahPdStatusId['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $jumlahPdStatusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $jumlahPdStatusId, $comparison);
    }

    /**
     * Filter the query on the tanggal column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('fooValue');   // WHERE tanggal = 'fooValue'
     * $query->filterByTanggal('%fooValue%'); // WHERE tanggal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggal The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggal)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggal)) {
                $tanggal = str_replace('*', '%', $tanggal);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayah(1234); // WHERE id_level_wilayah = 1234
     * $query->filterByIdLevelWilayah(array(12, 34)); // WHERE id_level_wilayah IN (12, 34)
     * $query->filterByIdLevelWilayah(array('min' => 12)); // WHERE id_level_wilayah >= 12
     * $query->filterByIdLevelWilayah(array('max' => 12)); // WHERE id_level_wilayah <= 12
     * </code>
     *
     * @param     mixed $idLevelWilayah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayah($idLevelWilayah = null, $comparison = null)
    {
        if (is_array($idLevelWilayah)) {
            $useMinMax = false;
            if (isset($idLevelWilayah['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, $idLevelWilayah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLevelWilayah['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, $idLevelWilayah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, $idLevelWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayah('fooValue');   // WHERE mst_kode_wilayah = 'fooValue'
     * $query->filterByMstKodeWilayah('%fooValue%'); // WHERE mst_kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayah($mstKodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayah)) {
                $mstKodeWilayah = str_replace('*', '%', $mstKodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::MST_KODE_WILAYAH, $mstKodeWilayah, $comparison);
    }

    /**
     * Filter the query on the tahun_ajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunAjaranId('fooValue');   // WHERE tahun_ajaran_id = 'fooValue'
     * $query->filterByTahunAjaranId('%fooValue%'); // WHERE tahun_ajaran_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tahunAjaranId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByTahunAjaranId($tahunAjaranId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tahunAjaranId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tahunAjaranId)) {
                $tahunAjaranId = str_replace('*', '%', $tahunAjaranId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::TAHUN_AJARAN_ID, $tahunAjaranId, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarNegeri(1234); // WHERE pd_terdaftar_negeri = 1234
     * $query->filterByPdTerdaftarNegeri(array(12, 34)); // WHERE pd_terdaftar_negeri IN (12, 34)
     * $query->filterByPdTerdaftarNegeri(array('min' => 12)); // WHERE pd_terdaftar_negeri >= 12
     * $query->filterByPdTerdaftarNegeri(array('max' => 12)); // WHERE pd_terdaftar_negeri <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarNegeri($pdTerdaftarNegeri = null, $comparison = null)
    {
        if (is_array($pdTerdaftarNegeri)) {
            $useMinMax = false;
            if (isset($pdTerdaftarNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_NEGERI, $pdTerdaftarNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_NEGERI, $pdTerdaftarNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_NEGERI, $pdTerdaftarNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSwasta(1234); // WHERE pd_terdaftar_swasta = 1234
     * $query->filterByPdTerdaftarSwasta(array(12, 34)); // WHERE pd_terdaftar_swasta IN (12, 34)
     * $query->filterByPdTerdaftarSwasta(array('min' => 12)); // WHERE pd_terdaftar_swasta >= 12
     * $query->filterByPdTerdaftarSwasta(array('max' => 12)); // WHERE pd_terdaftar_swasta <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSwasta($pdTerdaftarSwasta = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSwasta)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SWASTA, $pdTerdaftarSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SWASTA, $pdTerdaftarSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SWASTA, $pdTerdaftarSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelNegeri(1234); // WHERE pd_rombel_negeri = 1234
     * $query->filterByPdRombelNegeri(array(12, 34)); // WHERE pd_rombel_negeri IN (12, 34)
     * $query->filterByPdRombelNegeri(array('min' => 12)); // WHERE pd_rombel_negeri >= 12
     * $query->filterByPdRombelNegeri(array('max' => 12)); // WHERE pd_rombel_negeri <= 12
     * </code>
     *
     * @param     mixed $pdRombelNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelNegeri($pdRombelNegeri = null, $comparison = null)
    {
        if (is_array($pdRombelNegeri)) {
            $useMinMax = false;
            if (isset($pdRombelNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_NEGERI, $pdRombelNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_NEGERI, $pdRombelNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_NEGERI, $pdRombelNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSwasta(1234); // WHERE pd_rombel_swasta = 1234
     * $query->filterByPdRombelSwasta(array(12, 34)); // WHERE pd_rombel_swasta IN (12, 34)
     * $query->filterByPdRombelSwasta(array('min' => 12)); // WHERE pd_rombel_swasta >= 12
     * $query->filterByPdRombelSwasta(array('max' => 12)); // WHERE pd_rombel_swasta <= 12
     * </code>
     *
     * @param     mixed $pdRombelSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSwasta($pdRombelSwasta = null, $comparison = null)
    {
        if (is_array($pdRombelSwasta)) {
            $useMinMax = false;
            if (isset($pdRombelSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SWASTA, $pdRombelSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SWASTA, $pdRombelSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SWASTA, $pdRombelSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_sma_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSmaNegeri(1234); // WHERE pd_terdaftar_sma_negeri = 1234
     * $query->filterByPdTerdaftarSmaNegeri(array(12, 34)); // WHERE pd_terdaftar_sma_negeri IN (12, 34)
     * $query->filterByPdTerdaftarSmaNegeri(array('min' => 12)); // WHERE pd_terdaftar_sma_negeri >= 12
     * $query->filterByPdTerdaftarSmaNegeri(array('max' => 12)); // WHERE pd_terdaftar_sma_negeri <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSmaNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSmaNegeri($pdTerdaftarSmaNegeri = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSmaNegeri)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSmaNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_NEGERI, $pdTerdaftarSmaNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSmaNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_NEGERI, $pdTerdaftarSmaNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_NEGERI, $pdTerdaftarSmaNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_sma_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSmaSwasta(1234); // WHERE pd_terdaftar_sma_swasta = 1234
     * $query->filterByPdTerdaftarSmaSwasta(array(12, 34)); // WHERE pd_terdaftar_sma_swasta IN (12, 34)
     * $query->filterByPdTerdaftarSmaSwasta(array('min' => 12)); // WHERE pd_terdaftar_sma_swasta >= 12
     * $query->filterByPdTerdaftarSmaSwasta(array('max' => 12)); // WHERE pd_terdaftar_sma_swasta <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSmaSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSmaSwasta($pdTerdaftarSmaSwasta = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSmaSwasta)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSmaSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_SWASTA, $pdTerdaftarSmaSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSmaSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_SWASTA, $pdTerdaftarSmaSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_SWASTA, $pdTerdaftarSmaSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_sma_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSmaNegeri(1234); // WHERE pd_rombel_sma_negeri = 1234
     * $query->filterByPdRombelSmaNegeri(array(12, 34)); // WHERE pd_rombel_sma_negeri IN (12, 34)
     * $query->filterByPdRombelSmaNegeri(array('min' => 12)); // WHERE pd_rombel_sma_negeri >= 12
     * $query->filterByPdRombelSmaNegeri(array('max' => 12)); // WHERE pd_rombel_sma_negeri <= 12
     * </code>
     *
     * @param     mixed $pdRombelSmaNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSmaNegeri($pdRombelSmaNegeri = null, $comparison = null)
    {
        if (is_array($pdRombelSmaNegeri)) {
            $useMinMax = false;
            if (isset($pdRombelSmaNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMA_NEGERI, $pdRombelSmaNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSmaNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMA_NEGERI, $pdRombelSmaNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMA_NEGERI, $pdRombelSmaNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_sma_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSmaSwasta(1234); // WHERE pd_rombel_sma_swasta = 1234
     * $query->filterByPdRombelSmaSwasta(array(12, 34)); // WHERE pd_rombel_sma_swasta IN (12, 34)
     * $query->filterByPdRombelSmaSwasta(array('min' => 12)); // WHERE pd_rombel_sma_swasta >= 12
     * $query->filterByPdRombelSmaSwasta(array('max' => 12)); // WHERE pd_rombel_sma_swasta <= 12
     * </code>
     *
     * @param     mixed $pdRombelSmaSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSmaSwasta($pdRombelSmaSwasta = null, $comparison = null)
    {
        if (is_array($pdRombelSmaSwasta)) {
            $useMinMax = false;
            if (isset($pdRombelSmaSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMA_SWASTA, $pdRombelSmaSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSmaSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMA_SWASTA, $pdRombelSmaSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMA_SWASTA, $pdRombelSmaSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_smk_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSmkNegeri(1234); // WHERE pd_terdaftar_smk_negeri = 1234
     * $query->filterByPdTerdaftarSmkNegeri(array(12, 34)); // WHERE pd_terdaftar_smk_negeri IN (12, 34)
     * $query->filterByPdTerdaftarSmkNegeri(array('min' => 12)); // WHERE pd_terdaftar_smk_negeri >= 12
     * $query->filterByPdTerdaftarSmkNegeri(array('max' => 12)); // WHERE pd_terdaftar_smk_negeri <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSmkNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSmkNegeri($pdTerdaftarSmkNegeri = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSmkNegeri)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSmkNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_NEGERI, $pdTerdaftarSmkNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSmkNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_NEGERI, $pdTerdaftarSmkNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_NEGERI, $pdTerdaftarSmkNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_smk_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSmkSwasta(1234); // WHERE pd_terdaftar_smk_swasta = 1234
     * $query->filterByPdTerdaftarSmkSwasta(array(12, 34)); // WHERE pd_terdaftar_smk_swasta IN (12, 34)
     * $query->filterByPdTerdaftarSmkSwasta(array('min' => 12)); // WHERE pd_terdaftar_smk_swasta >= 12
     * $query->filterByPdTerdaftarSmkSwasta(array('max' => 12)); // WHERE pd_terdaftar_smk_swasta <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSmkSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSmkSwasta($pdTerdaftarSmkSwasta = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSmkSwasta)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSmkSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_SWASTA, $pdTerdaftarSmkSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSmkSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_SWASTA, $pdTerdaftarSmkSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_SWASTA, $pdTerdaftarSmkSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_smk_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSmkNegeri(1234); // WHERE pd_rombel_smk_negeri = 1234
     * $query->filterByPdRombelSmkNegeri(array(12, 34)); // WHERE pd_rombel_smk_negeri IN (12, 34)
     * $query->filterByPdRombelSmkNegeri(array('min' => 12)); // WHERE pd_rombel_smk_negeri >= 12
     * $query->filterByPdRombelSmkNegeri(array('max' => 12)); // WHERE pd_rombel_smk_negeri <= 12
     * </code>
     *
     * @param     mixed $pdRombelSmkNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSmkNegeri($pdRombelSmkNegeri = null, $comparison = null)
    {
        if (is_array($pdRombelSmkNegeri)) {
            $useMinMax = false;
            if (isset($pdRombelSmkNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMK_NEGERI, $pdRombelSmkNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSmkNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMK_NEGERI, $pdRombelSmkNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMK_NEGERI, $pdRombelSmkNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_smk_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSmkSwasta(1234); // WHERE pd_rombel_smk_swasta = 1234
     * $query->filterByPdRombelSmkSwasta(array(12, 34)); // WHERE pd_rombel_smk_swasta IN (12, 34)
     * $query->filterByPdRombelSmkSwasta(array('min' => 12)); // WHERE pd_rombel_smk_swasta >= 12
     * $query->filterByPdRombelSmkSwasta(array('max' => 12)); // WHERE pd_rombel_smk_swasta <= 12
     * </code>
     *
     * @param     mixed $pdRombelSmkSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSmkSwasta($pdRombelSmkSwasta = null, $comparison = null)
    {
        if (is_array($pdRombelSmkSwasta)) {
            $useMinMax = false;
            if (isset($pdRombelSmkSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMK_SWASTA, $pdRombelSmkSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSmkSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMK_SWASTA, $pdRombelSmkSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMK_SWASTA, $pdRombelSmkSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_smlb_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSmlbNegeri(1234); // WHERE pd_terdaftar_smlb_negeri = 1234
     * $query->filterByPdTerdaftarSmlbNegeri(array(12, 34)); // WHERE pd_terdaftar_smlb_negeri IN (12, 34)
     * $query->filterByPdTerdaftarSmlbNegeri(array('min' => 12)); // WHERE pd_terdaftar_smlb_negeri >= 12
     * $query->filterByPdTerdaftarSmlbNegeri(array('max' => 12)); // WHERE pd_terdaftar_smlb_negeri <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSmlbNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSmlbNegeri($pdTerdaftarSmlbNegeri = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSmlbNegeri)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSmlbNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_NEGERI, $pdTerdaftarSmlbNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSmlbNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_NEGERI, $pdTerdaftarSmlbNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_NEGERI, $pdTerdaftarSmlbNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_terdaftar_smlb_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdTerdaftarSmlbSwasta(1234); // WHERE pd_terdaftar_smlb_swasta = 1234
     * $query->filterByPdTerdaftarSmlbSwasta(array(12, 34)); // WHERE pd_terdaftar_smlb_swasta IN (12, 34)
     * $query->filterByPdTerdaftarSmlbSwasta(array('min' => 12)); // WHERE pd_terdaftar_smlb_swasta >= 12
     * $query->filterByPdTerdaftarSmlbSwasta(array('max' => 12)); // WHERE pd_terdaftar_smlb_swasta <= 12
     * </code>
     *
     * @param     mixed $pdTerdaftarSmlbSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdTerdaftarSmlbSwasta($pdTerdaftarSmlbSwasta = null, $comparison = null)
    {
        if (is_array($pdTerdaftarSmlbSwasta)) {
            $useMinMax = false;
            if (isset($pdTerdaftarSmlbSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_SWASTA, $pdTerdaftarSmlbSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdTerdaftarSmlbSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_SWASTA, $pdTerdaftarSmlbSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_SWASTA, $pdTerdaftarSmlbSwasta, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_smlb_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSmlbNegeri(1234); // WHERE pd_rombel_smlb_negeri = 1234
     * $query->filterByPdRombelSmlbNegeri(array(12, 34)); // WHERE pd_rombel_smlb_negeri IN (12, 34)
     * $query->filterByPdRombelSmlbNegeri(array('min' => 12)); // WHERE pd_rombel_smlb_negeri >= 12
     * $query->filterByPdRombelSmlbNegeri(array('max' => 12)); // WHERE pd_rombel_smlb_negeri <= 12
     * </code>
     *
     * @param     mixed $pdRombelSmlbNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSmlbNegeri($pdRombelSmlbNegeri = null, $comparison = null)
    {
        if (is_array($pdRombelSmlbNegeri)) {
            $useMinMax = false;
            if (isset($pdRombelSmlbNegeri['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMLB_NEGERI, $pdRombelSmlbNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSmlbNegeri['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMLB_NEGERI, $pdRombelSmlbNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMLB_NEGERI, $pdRombelSmlbNegeri, $comparison);
    }

    /**
     * Filter the query on the pd_rombel_smlb_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByPdRombelSmlbSwasta(1234); // WHERE pd_rombel_smlb_swasta = 1234
     * $query->filterByPdRombelSmlbSwasta(array(12, 34)); // WHERE pd_rombel_smlb_swasta IN (12, 34)
     * $query->filterByPdRombelSmlbSwasta(array('min' => 12)); // WHERE pd_rombel_smlb_swasta >= 12
     * $query->filterByPdRombelSmlbSwasta(array('max' => 12)); // WHERE pd_rombel_smlb_swasta <= 12
     * </code>
     *
     * @param     mixed $pdRombelSmlbSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function filterByPdRombelSmlbSwasta($pdRombelSmlbSwasta = null, $comparison = null)
    {
        if (is_array($pdRombelSmlbSwasta)) {
            $useMinMax = false;
            if (isset($pdRombelSmlbSwasta['min'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMLB_SWASTA, $pdRombelSmlbSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdRombelSmlbSwasta['max'])) {
                $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMLB_SWASTA, $pdRombelSmlbSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahPdStatusPeer::PD_ROMBEL_SMLB_SWASTA, $pdRombelSmlbSwasta, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   JumlahPdStatus $jumlahPdStatus Object to remove from the list of results
     *
     * @return JumlahPdStatusQuery The current query, for fluid interface
     */
    public function prune($jumlahPdStatus = null)
    {
        if ($jumlahPdStatus) {
            $this->addUsingAlias(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $jumlahPdStatus->getJumlahPdStatusId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
