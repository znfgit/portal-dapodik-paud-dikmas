<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\SekolahHash;
use infopendataan\Model\SekolahHashPeer;
use infopendataan\Model\SekolahHashQuery;

/**
 * Base class that represents a query for the 'sekolah_hash' table.
 *
 * 
 *
 * @method SekolahHashQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SekolahHashQuery orderByHash($order = Criteria::ASC) Order by the hash column
 *
 * @method SekolahHashQuery groupBySekolahId() Group by the sekolah_id column
 * @method SekolahHashQuery groupByHash() Group by the hash column
 *
 * @method SekolahHashQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SekolahHashQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SekolahHashQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SekolahHash findOne(PropelPDO $con = null) Return the first SekolahHash matching the query
 * @method SekolahHash findOneOrCreate(PropelPDO $con = null) Return the first SekolahHash matching the query, or a new SekolahHash object populated from the query conditions when no match is found
 *
 * @method SekolahHash findOneByHash(string $hash) Return the first SekolahHash filtered by the hash column
 *
 * @method array findBySekolahId(string $sekolah_id) Return SekolahHash objects filtered by the sekolah_id column
 * @method array findByHash(string $hash) Return SekolahHash objects filtered by the hash column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseSekolahHashQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSekolahHashQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\SekolahHash', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SekolahHashQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SekolahHashQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SekolahHashQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SekolahHashQuery) {
            return $criteria;
        }
        $query = new SekolahHashQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SekolahHash|SekolahHash[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SekolahHashPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SekolahHashPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SekolahHash A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySekolahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SekolahHash A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `sekolah_id`, `hash` FROM `sekolah_hash` WHERE `sekolah_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SekolahHash();
            $obj->hydrate($row);
            SekolahHashPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SekolahHash|SekolahHash[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SekolahHash[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SekolahHashQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SekolahHashPeer::SEKOLAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SekolahHashQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SekolahHashPeer::SEKOLAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahHashQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahHashPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the hash column
     *
     * Example usage:
     * <code>
     * $query->filterByHash('fooValue');   // WHERE hash = 'fooValue'
     * $query->filterByHash('%fooValue%'); // WHERE hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahHashQuery The current query, for fluid interface
     */
    public function filterByHash($hash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hash)) {
                $hash = str_replace('*', '%', $hash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahHashPeer::HASH, $hash, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   SekolahHash $sekolahHash Object to remove from the list of results
     *
     * @return SekolahHashQuery The current query, for fluid interface
     */
    public function prune($sekolahHash = null)
    {
        if ($sekolahHash) {
            $this->addUsingAlias(SekolahHashPeer::SEKOLAH_ID, $sekolahHash->getSekolahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
