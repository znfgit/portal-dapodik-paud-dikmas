<?php

namespace infopendataan\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\RekapSekolah;
use infopendataan\Model\RekapSekolahPeer;
use infopendataan\Model\RekapSekolahQuery;

/**
 * Base class that represents a row from the 'rekap_sekolah' table.
 *
 * 
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseRekapSekolah extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'infopendataan\\Model\\RekapSekolahPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        RekapSekolahPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the rekap_sekolah_id field.
     * @var        int
     */
    protected $rekap_sekolah_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the tanggal field.
     * @var        string
     */
    protected $tanggal;

    /**
     * The value for the ptk field.
     * @var        int
     */
    protected $ptk;

    /**
     * The value for the pegawai field.
     * @var        int
     */
    protected $pegawai;

    /**
     * The value for the pd field.
     * @var        int
     */
    protected $pd;

    /**
     * The value for the rombel field.
     * @var        int
     */
    protected $rombel;

    /**
     * The value for the kode_wilayah_kecamatan field.
     * @var        string
     */
    protected $kode_wilayah_kecamatan;

    /**
     * The value for the kecamatan field.
     * @var        string
     */
    protected $kecamatan;

    /**
     * The value for the mst_kode_wilayah_kecamatan field.
     * @var        string
     */
    protected $mst_kode_wilayah_kecamatan;

    /**
     * The value for the id_level_wilayah_kecamatan field.
     * @var        int
     */
    protected $id_level_wilayah_kecamatan;

    /**
     * The value for the kode_wilayah_kabupaten field.
     * @var        string
     */
    protected $kode_wilayah_kabupaten;

    /**
     * The value for the kabupaten field.
     * @var        string
     */
    protected $kabupaten;

    /**
     * The value for the mst_kode_wilayah_kabupaten field.
     * @var        string
     */
    protected $mst_kode_wilayah_kabupaten;

    /**
     * The value for the id_level_wilayah_kabupaten field.
     * @var        int
     */
    protected $id_level_wilayah_kabupaten;

    /**
     * The value for the kode_wilayah_provinsi field.
     * @var        string
     */
    protected $kode_wilayah_provinsi;

    /**
     * The value for the provinsi field.
     * @var        string
     */
    protected $provinsi;

    /**
     * The value for the mst_kode_wilayah_provinsi field.
     * @var        string
     */
    protected $mst_kode_wilayah_provinsi;

    /**
     * The value for the id_level_wilayah_provinsi field.
     * @var        int
     */
    protected $id_level_wilayah_provinsi;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the npsn field.
     * @var        string
     */
    protected $npsn;

    /**
     * The value for the bentuk_pendidikan_id field.
     * @var        string
     */
    protected $bentuk_pendidikan_id;

    /**
     * The value for the status_sekolah field.
     * @var        string
     */
    protected $status_sekolah;

    /**
     * The value for the guru_matematika field.
     * @var        int
     */
    protected $guru_matematika;

    /**
     * The value for the guru_bahasa_indonesia field.
     * @var        int
     */
    protected $guru_bahasa_indonesia;

    /**
     * The value for the guru_bahasa_inggris field.
     * @var        int
     */
    protected $guru_bahasa_inggris;

    /**
     * The value for the guru_sejarah_indonesia field.
     * @var        int
     */
    protected $guru_sejarah_indonesia;

    /**
     * The value for the guru_pkn field.
     * @var        int
     */
    protected $guru_pkn;

    /**
     * The value for the guru_penjaskes field.
     * @var        int
     */
    protected $guru_penjaskes;

    /**
     * The value for the guru_agama_budi_pekerti field.
     * @var        int
     */
    protected $guru_agama_budi_pekerti;

    /**
     * The value for the guru_seni_budaya field.
     * @var        int
     */
    protected $guru_seni_budaya;

    /**
     * The value for the pd_kelas_10 field.
     * @var        int
     */
    protected $pd_kelas_10;

    /**
     * The value for the pd_kelas_11 field.
     * @var        int
     */
    protected $pd_kelas_11;

    /**
     * The value for the pd_kelas_12 field.
     * @var        int
     */
    protected $pd_kelas_12;

    /**
     * The value for the pd_kelas_13 field.
     * @var        int
     */
    protected $pd_kelas_13;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [rekap_sekolah_id] column value.
     * 
     * @return int
     */
    public function getRekapSekolahId()
    {
        return $this->rekap_sekolah_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [optionally formatted] temporal [tanggal] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTanggal($format = '%x')
    {
        if ($this->tanggal === null) {
            return null;
        }

        if ($this->tanggal === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->tanggal);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->tanggal, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [ptk] column value.
     * 
     * @return int
     */
    public function getPtk()
    {
        return $this->ptk;
    }

    /**
     * Get the [pegawai] column value.
     * 
     * @return int
     */
    public function getPegawai()
    {
        return $this->pegawai;
    }

    /**
     * Get the [pd] column value.
     * 
     * @return int
     */
    public function getPd()
    {
        return $this->pd;
    }

    /**
     * Get the [rombel] column value.
     * 
     * @return int
     */
    public function getRombel()
    {
        return $this->rombel;
    }

    /**
     * Get the [kode_wilayah_kecamatan] column value.
     * 
     * @return string
     */
    public function getKodeWilayahKecamatan()
    {
        return $this->kode_wilayah_kecamatan;
    }

    /**
     * Get the [kecamatan] column value.
     * 
     * @return string
     */
    public function getKecamatan()
    {
        return $this->kecamatan;
    }

    /**
     * Get the [mst_kode_wilayah_kecamatan] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayahKecamatan()
    {
        return $this->mst_kode_wilayah_kecamatan;
    }

    /**
     * Get the [id_level_wilayah_kecamatan] column value.
     * 
     * @return int
     */
    public function getIdLevelWilayahKecamatan()
    {
        return $this->id_level_wilayah_kecamatan;
    }

    /**
     * Get the [kode_wilayah_kabupaten] column value.
     * 
     * @return string
     */
    public function getKodeWilayahKabupaten()
    {
        return $this->kode_wilayah_kabupaten;
    }

    /**
     * Get the [kabupaten] column value.
     * 
     * @return string
     */
    public function getKabupaten()
    {
        return $this->kabupaten;
    }

    /**
     * Get the [mst_kode_wilayah_kabupaten] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayahKabupaten()
    {
        return $this->mst_kode_wilayah_kabupaten;
    }

    /**
     * Get the [id_level_wilayah_kabupaten] column value.
     * 
     * @return int
     */
    public function getIdLevelWilayahKabupaten()
    {
        return $this->id_level_wilayah_kabupaten;
    }

    /**
     * Get the [kode_wilayah_provinsi] column value.
     * 
     * @return string
     */
    public function getKodeWilayahProvinsi()
    {
        return $this->kode_wilayah_provinsi;
    }

    /**
     * Get the [provinsi] column value.
     * 
     * @return string
     */
    public function getProvinsi()
    {
        return $this->provinsi;
    }

    /**
     * Get the [mst_kode_wilayah_provinsi] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayahProvinsi()
    {
        return $this->mst_kode_wilayah_provinsi;
    }

    /**
     * Get the [id_level_wilayah_provinsi] column value.
     * 
     * @return int
     */
    public function getIdLevelWilayahProvinsi()
    {
        return $this->id_level_wilayah_provinsi;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [npsn] column value.
     * 
     * @return string
     */
    public function getNpsn()
    {
        return $this->npsn;
    }

    /**
     * Get the [bentuk_pendidikan_id] column value.
     * 
     * @return string
     */
    public function getBentukPendidikanId()
    {
        return $this->bentuk_pendidikan_id;
    }

    /**
     * Get the [status_sekolah] column value.
     * 
     * @return string
     */
    public function getStatusSekolah()
    {
        return $this->status_sekolah;
    }

    /**
     * Get the [guru_matematika] column value.
     * 
     * @return int
     */
    public function getGuruMatematika()
    {
        return $this->guru_matematika;
    }

    /**
     * Get the [guru_bahasa_indonesia] column value.
     * 
     * @return int
     */
    public function getGuruBahasaIndonesia()
    {
        return $this->guru_bahasa_indonesia;
    }

    /**
     * Get the [guru_bahasa_inggris] column value.
     * 
     * @return int
     */
    public function getGuruBahasaInggris()
    {
        return $this->guru_bahasa_inggris;
    }

    /**
     * Get the [guru_sejarah_indonesia] column value.
     * 
     * @return int
     */
    public function getGuruSejarahIndonesia()
    {
        return $this->guru_sejarah_indonesia;
    }

    /**
     * Get the [guru_pkn] column value.
     * 
     * @return int
     */
    public function getGuruPkn()
    {
        return $this->guru_pkn;
    }

    /**
     * Get the [guru_penjaskes] column value.
     * 
     * @return int
     */
    public function getGuruPenjaskes()
    {
        return $this->guru_penjaskes;
    }

    /**
     * Get the [guru_agama_budi_pekerti] column value.
     * 
     * @return int
     */
    public function getGuruAgamaBudiPekerti()
    {
        return $this->guru_agama_budi_pekerti;
    }

    /**
     * Get the [guru_seni_budaya] column value.
     * 
     * @return int
     */
    public function getGuruSeniBudaya()
    {
        return $this->guru_seni_budaya;
    }

    /**
     * Get the [pd_kelas_10] column value.
     * 
     * @return int
     */
    public function getPdKelas10()
    {
        return $this->pd_kelas_10;
    }

    /**
     * Get the [pd_kelas_11] column value.
     * 
     * @return int
     */
    public function getPdKelas11()
    {
        return $this->pd_kelas_11;
    }

    /**
     * Get the [pd_kelas_12] column value.
     * 
     * @return int
     */
    public function getPdKelas12()
    {
        return $this->pd_kelas_12;
    }

    /**
     * Get the [pd_kelas_13] column value.
     * 
     * @return int
     */
    public function getPdKelas13()
    {
        return $this->pd_kelas_13;
    }

    /**
     * Set the value of [rekap_sekolah_id] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setRekapSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->rekap_sekolah_id !== $v) {
            $this->rekap_sekolah_id = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::REKAP_SEKOLAH_ID;
        }


        return $this;
    } // setRekapSekolahId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::SEKOLAH_ID;
        }


        return $this;
    } // setSekolahId()

    /**
     * Sets the value of [tanggal] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setTanggal($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tanggal !== null || $dt !== null) {
            $currentDateAsString = ($this->tanggal !== null && $tmpDt = new DateTime($this->tanggal)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->tanggal = $newDateAsString;
                $this->modifiedColumns[] = RekapSekolahPeer::TANGGAL;
            }
        } // if either are not null


        return $this;
    } // setTanggal()

    /**
     * Set the value of [ptk] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPtk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ptk !== $v) {
            $this->ptk = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PTK;
        }


        return $this;
    } // setPtk()

    /**
     * Set the value of [pegawai] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPegawai($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pegawai !== $v) {
            $this->pegawai = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PEGAWAI;
        }


        return $this;
    } // setPegawai()

    /**
     * Set the value of [pd] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPd($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pd !== $v) {
            $this->pd = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PD;
        }


        return $this;
    } // setPd()

    /**
     * Set the value of [rombel] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setRombel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->rombel !== $v) {
            $this->rombel = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::ROMBEL;
        }


        return $this;
    } // setRombel()

    /**
     * Set the value of [kode_wilayah_kecamatan] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setKodeWilayahKecamatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah_kecamatan !== $v) {
            $this->kode_wilayah_kecamatan = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::KODE_WILAYAH_KECAMATAN;
        }


        return $this;
    } // setKodeWilayahKecamatan()

    /**
     * Set the value of [kecamatan] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setKecamatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kecamatan !== $v) {
            $this->kecamatan = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::KECAMATAN;
        }


        return $this;
    } // setKecamatan()

    /**
     * Set the value of [mst_kode_wilayah_kecamatan] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setMstKodeWilayahKecamatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah_kecamatan !== $v) {
            $this->mst_kode_wilayah_kecamatan = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN;
        }


        return $this;
    } // setMstKodeWilayahKecamatan()

    /**
     * Set the value of [id_level_wilayah_kecamatan] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setIdLevelWilayahKecamatan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_level_wilayah_kecamatan !== $v) {
            $this->id_level_wilayah_kecamatan = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN;
        }


        return $this;
    } // setIdLevelWilayahKecamatan()

    /**
     * Set the value of [kode_wilayah_kabupaten] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setKodeWilayahKabupaten($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah_kabupaten !== $v) {
            $this->kode_wilayah_kabupaten = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::KODE_WILAYAH_KABUPATEN;
        }


        return $this;
    } // setKodeWilayahKabupaten()

    /**
     * Set the value of [kabupaten] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setKabupaten($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kabupaten !== $v) {
            $this->kabupaten = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::KABUPATEN;
        }


        return $this;
    } // setKabupaten()

    /**
     * Set the value of [mst_kode_wilayah_kabupaten] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setMstKodeWilayahKabupaten($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah_kabupaten !== $v) {
            $this->mst_kode_wilayah_kabupaten = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN;
        }


        return $this;
    } // setMstKodeWilayahKabupaten()

    /**
     * Set the value of [id_level_wilayah_kabupaten] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setIdLevelWilayahKabupaten($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_level_wilayah_kabupaten !== $v) {
            $this->id_level_wilayah_kabupaten = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN;
        }


        return $this;
    } // setIdLevelWilayahKabupaten()

    /**
     * Set the value of [kode_wilayah_provinsi] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setKodeWilayahProvinsi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah_provinsi !== $v) {
            $this->kode_wilayah_provinsi = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::KODE_WILAYAH_PROVINSI;
        }


        return $this;
    } // setKodeWilayahProvinsi()

    /**
     * Set the value of [provinsi] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setProvinsi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->provinsi !== $v) {
            $this->provinsi = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PROVINSI;
        }


        return $this;
    } // setProvinsi()

    /**
     * Set the value of [mst_kode_wilayah_provinsi] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setMstKodeWilayahProvinsi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah_provinsi !== $v) {
            $this->mst_kode_wilayah_provinsi = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI;
        }


        return $this;
    } // setMstKodeWilayahProvinsi()

    /**
     * Set the value of [id_level_wilayah_provinsi] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setIdLevelWilayahProvinsi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_level_wilayah_provinsi !== $v) {
            $this->id_level_wilayah_provinsi = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI;
        }


        return $this;
    } // setIdLevelWilayahProvinsi()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [npsn] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setNpsn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->npsn !== $v) {
            $this->npsn = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::NPSN;
        }


        return $this;
    } // setNpsn()

    /**
     * Set the value of [bentuk_pendidikan_id] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setBentukPendidikanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->bentuk_pendidikan_id !== $v) {
            $this->bentuk_pendidikan_id = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::BENTUK_PENDIDIKAN_ID;
        }


        return $this;
    } // setBentukPendidikanId()

    /**
     * Set the value of [status_sekolah] column.
     * 
     * @param string $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setStatusSekolah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_sekolah !== $v) {
            $this->status_sekolah = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::STATUS_SEKOLAH;
        }


        return $this;
    } // setStatusSekolah()

    /**
     * Set the value of [guru_matematika] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruMatematika($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_matematika !== $v) {
            $this->guru_matematika = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_MATEMATIKA;
        }


        return $this;
    } // setGuruMatematika()

    /**
     * Set the value of [guru_bahasa_indonesia] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruBahasaIndonesia($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_bahasa_indonesia !== $v) {
            $this->guru_bahasa_indonesia = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_BAHASA_INDONESIA;
        }


        return $this;
    } // setGuruBahasaIndonesia()

    /**
     * Set the value of [guru_bahasa_inggris] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruBahasaInggris($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_bahasa_inggris !== $v) {
            $this->guru_bahasa_inggris = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_BAHASA_INGGRIS;
        }


        return $this;
    } // setGuruBahasaInggris()

    /**
     * Set the value of [guru_sejarah_indonesia] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruSejarahIndonesia($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_sejarah_indonesia !== $v) {
            $this->guru_sejarah_indonesia = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_SEJARAH_INDONESIA;
        }


        return $this;
    } // setGuruSejarahIndonesia()

    /**
     * Set the value of [guru_pkn] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruPkn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_pkn !== $v) {
            $this->guru_pkn = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_PKN;
        }


        return $this;
    } // setGuruPkn()

    /**
     * Set the value of [guru_penjaskes] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruPenjaskes($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_penjaskes !== $v) {
            $this->guru_penjaskes = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_PENJASKES;
        }


        return $this;
    } // setGuruPenjaskes()

    /**
     * Set the value of [guru_agama_budi_pekerti] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruAgamaBudiPekerti($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_agama_budi_pekerti !== $v) {
            $this->guru_agama_budi_pekerti = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI;
        }


        return $this;
    } // setGuruAgamaBudiPekerti()

    /**
     * Set the value of [guru_seni_budaya] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setGuruSeniBudaya($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->guru_seni_budaya !== $v) {
            $this->guru_seni_budaya = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::GURU_SENI_BUDAYA;
        }


        return $this;
    } // setGuruSeniBudaya()

    /**
     * Set the value of [pd_kelas_10] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPdKelas10($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pd_kelas_10 !== $v) {
            $this->pd_kelas_10 = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PD_KELAS_10;
        }


        return $this;
    } // setPdKelas10()

    /**
     * Set the value of [pd_kelas_11] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPdKelas11($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pd_kelas_11 !== $v) {
            $this->pd_kelas_11 = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PD_KELAS_11;
        }


        return $this;
    } // setPdKelas11()

    /**
     * Set the value of [pd_kelas_12] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPdKelas12($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pd_kelas_12 !== $v) {
            $this->pd_kelas_12 = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PD_KELAS_12;
        }


        return $this;
    } // setPdKelas12()

    /**
     * Set the value of [pd_kelas_13] column.
     * 
     * @param int $v new value
     * @return RekapSekolah The current object (for fluent API support)
     */
    public function setPdKelas13($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pd_kelas_13 !== $v) {
            $this->pd_kelas_13 = $v;
            $this->modifiedColumns[] = RekapSekolahPeer::PD_KELAS_13;
        }


        return $this;
    } // setPdKelas13()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->rekap_sekolah_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->tanggal = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->ptk = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->pegawai = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->pd = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->rombel = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->kode_wilayah_kecamatan = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->kecamatan = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->mst_kode_wilayah_kecamatan = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->id_level_wilayah_kecamatan = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->kode_wilayah_kabupaten = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->kabupaten = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->mst_kode_wilayah_kabupaten = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->id_level_wilayah_kabupaten = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->kode_wilayah_provinsi = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->provinsi = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->mst_kode_wilayah_provinsi = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->id_level_wilayah_provinsi = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->nama = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->npsn = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->bentuk_pendidikan_id = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->status_sekolah = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->guru_matematika = ($row[$startcol + 23] !== null) ? (int) $row[$startcol + 23] : null;
            $this->guru_bahasa_indonesia = ($row[$startcol + 24] !== null) ? (int) $row[$startcol + 24] : null;
            $this->guru_bahasa_inggris = ($row[$startcol + 25] !== null) ? (int) $row[$startcol + 25] : null;
            $this->guru_sejarah_indonesia = ($row[$startcol + 26] !== null) ? (int) $row[$startcol + 26] : null;
            $this->guru_pkn = ($row[$startcol + 27] !== null) ? (int) $row[$startcol + 27] : null;
            $this->guru_penjaskes = ($row[$startcol + 28] !== null) ? (int) $row[$startcol + 28] : null;
            $this->guru_agama_budi_pekerti = ($row[$startcol + 29] !== null) ? (int) $row[$startcol + 29] : null;
            $this->guru_seni_budaya = ($row[$startcol + 30] !== null) ? (int) $row[$startcol + 30] : null;
            $this->pd_kelas_10 = ($row[$startcol + 31] !== null) ? (int) $row[$startcol + 31] : null;
            $this->pd_kelas_11 = ($row[$startcol + 32] !== null) ? (int) $row[$startcol + 32] : null;
            $this->pd_kelas_12 = ($row[$startcol + 33] !== null) ? (int) $row[$startcol + 33] : null;
            $this->pd_kelas_13 = ($row[$startcol + 34] !== null) ? (int) $row[$startcol + 34] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 35; // 35 = RekapSekolahPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating RekapSekolah object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = RekapSekolahPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = RekapSekolahQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RekapSekolahPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = RekapSekolahPeer::REKAP_SEKOLAH_ID;
        if (null !== $this->rekap_sekolah_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . RekapSekolahPeer::REKAP_SEKOLAH_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(RekapSekolahPeer::REKAP_SEKOLAH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`rekap_sekolah_id`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::SEKOLAH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`sekolah_id`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::TANGGAL)) {
            $modifiedColumns[':p' . $index++]  = '`tanggal`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PTK)) {
            $modifiedColumns[':p' . $index++]  = '`ptk`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PEGAWAI)) {
            $modifiedColumns[':p' . $index++]  = '`pegawai`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PD)) {
            $modifiedColumns[':p' . $index++]  = '`pd`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::ROMBEL)) {
            $modifiedColumns[':p' . $index++]  = '`rombel`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::KODE_WILAYAH_KECAMATAN)) {
            $modifiedColumns[':p' . $index++]  = '`kode_wilayah_kecamatan`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::KECAMATAN)) {
            $modifiedColumns[':p' . $index++]  = '`kecamatan`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN)) {
            $modifiedColumns[':p' . $index++]  = '`mst_kode_wilayah_kecamatan`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN)) {
            $modifiedColumns[':p' . $index++]  = '`id_level_wilayah_kecamatan`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::KODE_WILAYAH_KABUPATEN)) {
            $modifiedColumns[':p' . $index++]  = '`kode_wilayah_kabupaten`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::KABUPATEN)) {
            $modifiedColumns[':p' . $index++]  = '`kabupaten`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN)) {
            $modifiedColumns[':p' . $index++]  = '`mst_kode_wilayah_kabupaten`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN)) {
            $modifiedColumns[':p' . $index++]  = '`id_level_wilayah_kabupaten`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::KODE_WILAYAH_PROVINSI)) {
            $modifiedColumns[':p' . $index++]  = '`kode_wilayah_provinsi`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PROVINSI)) {
            $modifiedColumns[':p' . $index++]  = '`provinsi`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI)) {
            $modifiedColumns[':p' . $index++]  = '`mst_kode_wilayah_provinsi`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI)) {
            $modifiedColumns[':p' . $index++]  = '`id_level_wilayah_provinsi`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::NAMA)) {
            $modifiedColumns[':p' . $index++]  = '`nama`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::NPSN)) {
            $modifiedColumns[':p' . $index++]  = '`npsn`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID)) {
            $modifiedColumns[':p' . $index++]  = '`bentuk_pendidikan_id`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::STATUS_SEKOLAH)) {
            $modifiedColumns[':p' . $index++]  = '`status_sekolah`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_MATEMATIKA)) {
            $modifiedColumns[':p' . $index++]  = '`guru_matematika`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_BAHASA_INDONESIA)) {
            $modifiedColumns[':p' . $index++]  = '`guru_bahasa_indonesia`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_BAHASA_INGGRIS)) {
            $modifiedColumns[':p' . $index++]  = '`guru_bahasa_inggris`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_SEJARAH_INDONESIA)) {
            $modifiedColumns[':p' . $index++]  = '`guru_sejarah_indonesia`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_PKN)) {
            $modifiedColumns[':p' . $index++]  = '`guru_pkn`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_PENJASKES)) {
            $modifiedColumns[':p' . $index++]  = '`guru_penjaskes`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI)) {
            $modifiedColumns[':p' . $index++]  = '`guru_agama_budi_pekerti`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::GURU_SENI_BUDAYA)) {
            $modifiedColumns[':p' . $index++]  = '`guru_seni_budaya`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_10)) {
            $modifiedColumns[':p' . $index++]  = '`pd_kelas_10`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_11)) {
            $modifiedColumns[':p' . $index++]  = '`pd_kelas_11`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_12)) {
            $modifiedColumns[':p' . $index++]  = '`pd_kelas_12`';
        }
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_13)) {
            $modifiedColumns[':p' . $index++]  = '`pd_kelas_13`';
        }

        $sql = sprintf(
            'INSERT INTO `rekap_sekolah` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`rekap_sekolah_id`':						
                        $stmt->bindValue($identifier, $this->rekap_sekolah_id, PDO::PARAM_INT);
                        break;
                    case '`sekolah_id`':						
                        $stmt->bindValue($identifier, $this->sekolah_id, PDO::PARAM_STR);
                        break;
                    case '`tanggal`':						
                        $stmt->bindValue($identifier, $this->tanggal, PDO::PARAM_STR);
                        break;
                    case '`ptk`':						
                        $stmt->bindValue($identifier, $this->ptk, PDO::PARAM_INT);
                        break;
                    case '`pegawai`':						
                        $stmt->bindValue($identifier, $this->pegawai, PDO::PARAM_INT);
                        break;
                    case '`pd`':						
                        $stmt->bindValue($identifier, $this->pd, PDO::PARAM_INT);
                        break;
                    case '`rombel`':						
                        $stmt->bindValue($identifier, $this->rombel, PDO::PARAM_INT);
                        break;
                    case '`kode_wilayah_kecamatan`':						
                        $stmt->bindValue($identifier, $this->kode_wilayah_kecamatan, PDO::PARAM_STR);
                        break;
                    case '`kecamatan`':						
                        $stmt->bindValue($identifier, $this->kecamatan, PDO::PARAM_STR);
                        break;
                    case '`mst_kode_wilayah_kecamatan`':						
                        $stmt->bindValue($identifier, $this->mst_kode_wilayah_kecamatan, PDO::PARAM_STR);
                        break;
                    case '`id_level_wilayah_kecamatan`':						
                        $stmt->bindValue($identifier, $this->id_level_wilayah_kecamatan, PDO::PARAM_INT);
                        break;
                    case '`kode_wilayah_kabupaten`':						
                        $stmt->bindValue($identifier, $this->kode_wilayah_kabupaten, PDO::PARAM_STR);
                        break;
                    case '`kabupaten`':						
                        $stmt->bindValue($identifier, $this->kabupaten, PDO::PARAM_STR);
                        break;
                    case '`mst_kode_wilayah_kabupaten`':						
                        $stmt->bindValue($identifier, $this->mst_kode_wilayah_kabupaten, PDO::PARAM_STR);
                        break;
                    case '`id_level_wilayah_kabupaten`':						
                        $stmt->bindValue($identifier, $this->id_level_wilayah_kabupaten, PDO::PARAM_INT);
                        break;
                    case '`kode_wilayah_provinsi`':						
                        $stmt->bindValue($identifier, $this->kode_wilayah_provinsi, PDO::PARAM_STR);
                        break;
                    case '`provinsi`':						
                        $stmt->bindValue($identifier, $this->provinsi, PDO::PARAM_STR);
                        break;
                    case '`mst_kode_wilayah_provinsi`':						
                        $stmt->bindValue($identifier, $this->mst_kode_wilayah_provinsi, PDO::PARAM_STR);
                        break;
                    case '`id_level_wilayah_provinsi`':						
                        $stmt->bindValue($identifier, $this->id_level_wilayah_provinsi, PDO::PARAM_INT);
                        break;
                    case '`nama`':						
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case '`npsn`':						
                        $stmt->bindValue($identifier, $this->npsn, PDO::PARAM_STR);
                        break;
                    case '`bentuk_pendidikan_id`':						
                        $stmt->bindValue($identifier, $this->bentuk_pendidikan_id, PDO::PARAM_STR);
                        break;
                    case '`status_sekolah`':						
                        $stmt->bindValue($identifier, $this->status_sekolah, PDO::PARAM_STR);
                        break;
                    case '`guru_matematika`':						
                        $stmt->bindValue($identifier, $this->guru_matematika, PDO::PARAM_INT);
                        break;
                    case '`guru_bahasa_indonesia`':						
                        $stmt->bindValue($identifier, $this->guru_bahasa_indonesia, PDO::PARAM_INT);
                        break;
                    case '`guru_bahasa_inggris`':						
                        $stmt->bindValue($identifier, $this->guru_bahasa_inggris, PDO::PARAM_INT);
                        break;
                    case '`guru_sejarah_indonesia`':						
                        $stmt->bindValue($identifier, $this->guru_sejarah_indonesia, PDO::PARAM_INT);
                        break;
                    case '`guru_pkn`':						
                        $stmt->bindValue($identifier, $this->guru_pkn, PDO::PARAM_INT);
                        break;
                    case '`guru_penjaskes`':						
                        $stmt->bindValue($identifier, $this->guru_penjaskes, PDO::PARAM_INT);
                        break;
                    case '`guru_agama_budi_pekerti`':						
                        $stmt->bindValue($identifier, $this->guru_agama_budi_pekerti, PDO::PARAM_INT);
                        break;
                    case '`guru_seni_budaya`':						
                        $stmt->bindValue($identifier, $this->guru_seni_budaya, PDO::PARAM_INT);
                        break;
                    case '`pd_kelas_10`':						
                        $stmt->bindValue($identifier, $this->pd_kelas_10, PDO::PARAM_INT);
                        break;
                    case '`pd_kelas_11`':						
                        $stmt->bindValue($identifier, $this->pd_kelas_11, PDO::PARAM_INT);
                        break;
                    case '`pd_kelas_12`':						
                        $stmt->bindValue($identifier, $this->pd_kelas_12, PDO::PARAM_INT);
                        break;
                    case '`pd_kelas_13`':						
                        $stmt->bindValue($identifier, $this->pd_kelas_13, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setRekapSekolahId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = RekapSekolahPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RekapSekolahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getRekapSekolahId();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getTanggal();
                break;
            case 3:
                return $this->getPtk();
                break;
            case 4:
                return $this->getPegawai();
                break;
            case 5:
                return $this->getPd();
                break;
            case 6:
                return $this->getRombel();
                break;
            case 7:
                return $this->getKodeWilayahKecamatan();
                break;
            case 8:
                return $this->getKecamatan();
                break;
            case 9:
                return $this->getMstKodeWilayahKecamatan();
                break;
            case 10:
                return $this->getIdLevelWilayahKecamatan();
                break;
            case 11:
                return $this->getKodeWilayahKabupaten();
                break;
            case 12:
                return $this->getKabupaten();
                break;
            case 13:
                return $this->getMstKodeWilayahKabupaten();
                break;
            case 14:
                return $this->getIdLevelWilayahKabupaten();
                break;
            case 15:
                return $this->getKodeWilayahProvinsi();
                break;
            case 16:
                return $this->getProvinsi();
                break;
            case 17:
                return $this->getMstKodeWilayahProvinsi();
                break;
            case 18:
                return $this->getIdLevelWilayahProvinsi();
                break;
            case 19:
                return $this->getNama();
                break;
            case 20:
                return $this->getNpsn();
                break;
            case 21:
                return $this->getBentukPendidikanId();
                break;
            case 22:
                return $this->getStatusSekolah();
                break;
            case 23:
                return $this->getGuruMatematika();
                break;
            case 24:
                return $this->getGuruBahasaIndonesia();
                break;
            case 25:
                return $this->getGuruBahasaInggris();
                break;
            case 26:
                return $this->getGuruSejarahIndonesia();
                break;
            case 27:
                return $this->getGuruPkn();
                break;
            case 28:
                return $this->getGuruPenjaskes();
                break;
            case 29:
                return $this->getGuruAgamaBudiPekerti();
                break;
            case 30:
                return $this->getGuruSeniBudaya();
                break;
            case 31:
                return $this->getPdKelas10();
                break;
            case 32:
                return $this->getPdKelas11();
                break;
            case 33:
                return $this->getPdKelas12();
                break;
            case 34:
                return $this->getPdKelas13();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['RekapSekolah'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['RekapSekolah'][$this->getPrimaryKey()] = true;
        $keys = RekapSekolahPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getRekapSekolahId(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getTanggal(),
            $keys[3] => $this->getPtk(),
            $keys[4] => $this->getPegawai(),
            $keys[5] => $this->getPd(),
            $keys[6] => $this->getRombel(),
            $keys[7] => $this->getKodeWilayahKecamatan(),
            $keys[8] => $this->getKecamatan(),
            $keys[9] => $this->getMstKodeWilayahKecamatan(),
            $keys[10] => $this->getIdLevelWilayahKecamatan(),
            $keys[11] => $this->getKodeWilayahKabupaten(),
            $keys[12] => $this->getKabupaten(),
            $keys[13] => $this->getMstKodeWilayahKabupaten(),
            $keys[14] => $this->getIdLevelWilayahKabupaten(),
            $keys[15] => $this->getKodeWilayahProvinsi(),
            $keys[16] => $this->getProvinsi(),
            $keys[17] => $this->getMstKodeWilayahProvinsi(),
            $keys[18] => $this->getIdLevelWilayahProvinsi(),
            $keys[19] => $this->getNama(),
            $keys[20] => $this->getNpsn(),
            $keys[21] => $this->getBentukPendidikanId(),
            $keys[22] => $this->getStatusSekolah(),
            $keys[23] => $this->getGuruMatematika(),
            $keys[24] => $this->getGuruBahasaIndonesia(),
            $keys[25] => $this->getGuruBahasaInggris(),
            $keys[26] => $this->getGuruSejarahIndonesia(),
            $keys[27] => $this->getGuruPkn(),
            $keys[28] => $this->getGuruPenjaskes(),
            $keys[29] => $this->getGuruAgamaBudiPekerti(),
            $keys[30] => $this->getGuruSeniBudaya(),
            $keys[31] => $this->getPdKelas10(),
            $keys[32] => $this->getPdKelas11(),
            $keys[33] => $this->getPdKelas12(),
            $keys[34] => $this->getPdKelas13(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RekapSekolahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setRekapSekolahId($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setTanggal($value);
                break;
            case 3:
                $this->setPtk($value);
                break;
            case 4:
                $this->setPegawai($value);
                break;
            case 5:
                $this->setPd($value);
                break;
            case 6:
                $this->setRombel($value);
                break;
            case 7:
                $this->setKodeWilayahKecamatan($value);
                break;
            case 8:
                $this->setKecamatan($value);
                break;
            case 9:
                $this->setMstKodeWilayahKecamatan($value);
                break;
            case 10:
                $this->setIdLevelWilayahKecamatan($value);
                break;
            case 11:
                $this->setKodeWilayahKabupaten($value);
                break;
            case 12:
                $this->setKabupaten($value);
                break;
            case 13:
                $this->setMstKodeWilayahKabupaten($value);
                break;
            case 14:
                $this->setIdLevelWilayahKabupaten($value);
                break;
            case 15:
                $this->setKodeWilayahProvinsi($value);
                break;
            case 16:
                $this->setProvinsi($value);
                break;
            case 17:
                $this->setMstKodeWilayahProvinsi($value);
                break;
            case 18:
                $this->setIdLevelWilayahProvinsi($value);
                break;
            case 19:
                $this->setNama($value);
                break;
            case 20:
                $this->setNpsn($value);
                break;
            case 21:
                $this->setBentukPendidikanId($value);
                break;
            case 22:
                $this->setStatusSekolah($value);
                break;
            case 23:
                $this->setGuruMatematika($value);
                break;
            case 24:
                $this->setGuruBahasaIndonesia($value);
                break;
            case 25:
                $this->setGuruBahasaInggris($value);
                break;
            case 26:
                $this->setGuruSejarahIndonesia($value);
                break;
            case 27:
                $this->setGuruPkn($value);
                break;
            case 28:
                $this->setGuruPenjaskes($value);
                break;
            case 29:
                $this->setGuruAgamaBudiPekerti($value);
                break;
            case 30:
                $this->setGuruSeniBudaya($value);
                break;
            case 31:
                $this->setPdKelas10($value);
                break;
            case 32:
                $this->setPdKelas11($value);
                break;
            case 33:
                $this->setPdKelas12($value);
                break;
            case 34:
                $this->setPdKelas13($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = RekapSekolahPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setRekapSekolahId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTanggal($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPtk($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPegawai($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPd($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setRombel($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setKodeWilayahKecamatan($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setKecamatan($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMstKodeWilayahKecamatan($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setIdLevelWilayahKecamatan($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setKodeWilayahKabupaten($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setKabupaten($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setMstKodeWilayahKabupaten($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setIdLevelWilayahKabupaten($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setKodeWilayahProvinsi($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setProvinsi($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setMstKodeWilayahProvinsi($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setIdLevelWilayahProvinsi($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setNama($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setNpsn($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setBentukPendidikanId($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setStatusSekolah($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setGuruMatematika($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setGuruBahasaIndonesia($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setGuruBahasaInggris($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setGuruSejarahIndonesia($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setGuruPkn($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setGuruPenjaskes($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setGuruAgamaBudiPekerti($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setGuruSeniBudaya($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setPdKelas10($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setPdKelas11($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setPdKelas12($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setPdKelas13($arr[$keys[34]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RekapSekolahPeer::DATABASE_NAME);

        if ($this->isColumnModified(RekapSekolahPeer::REKAP_SEKOLAH_ID)) $criteria->add(RekapSekolahPeer::REKAP_SEKOLAH_ID, $this->rekap_sekolah_id);
        if ($this->isColumnModified(RekapSekolahPeer::SEKOLAH_ID)) $criteria->add(RekapSekolahPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(RekapSekolahPeer::TANGGAL)) $criteria->add(RekapSekolahPeer::TANGGAL, $this->tanggal);
        if ($this->isColumnModified(RekapSekolahPeer::PTK)) $criteria->add(RekapSekolahPeer::PTK, $this->ptk);
        if ($this->isColumnModified(RekapSekolahPeer::PEGAWAI)) $criteria->add(RekapSekolahPeer::PEGAWAI, $this->pegawai);
        if ($this->isColumnModified(RekapSekolahPeer::PD)) $criteria->add(RekapSekolahPeer::PD, $this->pd);
        if ($this->isColumnModified(RekapSekolahPeer::ROMBEL)) $criteria->add(RekapSekolahPeer::ROMBEL, $this->rombel);
        if ($this->isColumnModified(RekapSekolahPeer::KODE_WILAYAH_KECAMATAN)) $criteria->add(RekapSekolahPeer::KODE_WILAYAH_KECAMATAN, $this->kode_wilayah_kecamatan);
        if ($this->isColumnModified(RekapSekolahPeer::KECAMATAN)) $criteria->add(RekapSekolahPeer::KECAMATAN, $this->kecamatan);
        if ($this->isColumnModified(RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN)) $criteria->add(RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN, $this->mst_kode_wilayah_kecamatan);
        if ($this->isColumnModified(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN)) $criteria->add(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN, $this->id_level_wilayah_kecamatan);
        if ($this->isColumnModified(RekapSekolahPeer::KODE_WILAYAH_KABUPATEN)) $criteria->add(RekapSekolahPeer::KODE_WILAYAH_KABUPATEN, $this->kode_wilayah_kabupaten);
        if ($this->isColumnModified(RekapSekolahPeer::KABUPATEN)) $criteria->add(RekapSekolahPeer::KABUPATEN, $this->kabupaten);
        if ($this->isColumnModified(RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN)) $criteria->add(RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN, $this->mst_kode_wilayah_kabupaten);
        if ($this->isColumnModified(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN)) $criteria->add(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN, $this->id_level_wilayah_kabupaten);
        if ($this->isColumnModified(RekapSekolahPeer::KODE_WILAYAH_PROVINSI)) $criteria->add(RekapSekolahPeer::KODE_WILAYAH_PROVINSI, $this->kode_wilayah_provinsi);
        if ($this->isColumnModified(RekapSekolahPeer::PROVINSI)) $criteria->add(RekapSekolahPeer::PROVINSI, $this->provinsi);
        if ($this->isColumnModified(RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI)) $criteria->add(RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI, $this->mst_kode_wilayah_provinsi);
        if ($this->isColumnModified(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI)) $criteria->add(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI, $this->id_level_wilayah_provinsi);
        if ($this->isColumnModified(RekapSekolahPeer::NAMA)) $criteria->add(RekapSekolahPeer::NAMA, $this->nama);
        if ($this->isColumnModified(RekapSekolahPeer::NPSN)) $criteria->add(RekapSekolahPeer::NPSN, $this->npsn);
        if ($this->isColumnModified(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID)) $criteria->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, $this->bentuk_pendidikan_id);
        if ($this->isColumnModified(RekapSekolahPeer::STATUS_SEKOLAH)) $criteria->add(RekapSekolahPeer::STATUS_SEKOLAH, $this->status_sekolah);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_MATEMATIKA)) $criteria->add(RekapSekolahPeer::GURU_MATEMATIKA, $this->guru_matematika);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_BAHASA_INDONESIA)) $criteria->add(RekapSekolahPeer::GURU_BAHASA_INDONESIA, $this->guru_bahasa_indonesia);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_BAHASA_INGGRIS)) $criteria->add(RekapSekolahPeer::GURU_BAHASA_INGGRIS, $this->guru_bahasa_inggris);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_SEJARAH_INDONESIA)) $criteria->add(RekapSekolahPeer::GURU_SEJARAH_INDONESIA, $this->guru_sejarah_indonesia);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_PKN)) $criteria->add(RekapSekolahPeer::GURU_PKN, $this->guru_pkn);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_PENJASKES)) $criteria->add(RekapSekolahPeer::GURU_PENJASKES, $this->guru_penjaskes);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI)) $criteria->add(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI, $this->guru_agama_budi_pekerti);
        if ($this->isColumnModified(RekapSekolahPeer::GURU_SENI_BUDAYA)) $criteria->add(RekapSekolahPeer::GURU_SENI_BUDAYA, $this->guru_seni_budaya);
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_10)) $criteria->add(RekapSekolahPeer::PD_KELAS_10, $this->pd_kelas_10);
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_11)) $criteria->add(RekapSekolahPeer::PD_KELAS_11, $this->pd_kelas_11);
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_12)) $criteria->add(RekapSekolahPeer::PD_KELAS_12, $this->pd_kelas_12);
        if ($this->isColumnModified(RekapSekolahPeer::PD_KELAS_13)) $criteria->add(RekapSekolahPeer::PD_KELAS_13, $this->pd_kelas_13);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(RekapSekolahPeer::DATABASE_NAME);
        $criteria->add(RekapSekolahPeer::REKAP_SEKOLAH_ID, $this->rekap_sekolah_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getRekapSekolahId();
    }

    /**
     * Generic method to set the primary key (rekap_sekolah_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setRekapSekolahId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getRekapSekolahId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of RekapSekolah (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setTanggal($this->getTanggal());
        $copyObj->setPtk($this->getPtk());
        $copyObj->setPegawai($this->getPegawai());
        $copyObj->setPd($this->getPd());
        $copyObj->setRombel($this->getRombel());
        $copyObj->setKodeWilayahKecamatan($this->getKodeWilayahKecamatan());
        $copyObj->setKecamatan($this->getKecamatan());
        $copyObj->setMstKodeWilayahKecamatan($this->getMstKodeWilayahKecamatan());
        $copyObj->setIdLevelWilayahKecamatan($this->getIdLevelWilayahKecamatan());
        $copyObj->setKodeWilayahKabupaten($this->getKodeWilayahKabupaten());
        $copyObj->setKabupaten($this->getKabupaten());
        $copyObj->setMstKodeWilayahKabupaten($this->getMstKodeWilayahKabupaten());
        $copyObj->setIdLevelWilayahKabupaten($this->getIdLevelWilayahKabupaten());
        $copyObj->setKodeWilayahProvinsi($this->getKodeWilayahProvinsi());
        $copyObj->setProvinsi($this->getProvinsi());
        $copyObj->setMstKodeWilayahProvinsi($this->getMstKodeWilayahProvinsi());
        $copyObj->setIdLevelWilayahProvinsi($this->getIdLevelWilayahProvinsi());
        $copyObj->setNama($this->getNama());
        $copyObj->setNpsn($this->getNpsn());
        $copyObj->setBentukPendidikanId($this->getBentukPendidikanId());
        $copyObj->setStatusSekolah($this->getStatusSekolah());
        $copyObj->setGuruMatematika($this->getGuruMatematika());
        $copyObj->setGuruBahasaIndonesia($this->getGuruBahasaIndonesia());
        $copyObj->setGuruBahasaInggris($this->getGuruBahasaInggris());
        $copyObj->setGuruSejarahIndonesia($this->getGuruSejarahIndonesia());
        $copyObj->setGuruPkn($this->getGuruPkn());
        $copyObj->setGuruPenjaskes($this->getGuruPenjaskes());
        $copyObj->setGuruAgamaBudiPekerti($this->getGuruAgamaBudiPekerti());
        $copyObj->setGuruSeniBudaya($this->getGuruSeniBudaya());
        $copyObj->setPdKelas10($this->getPdKelas10());
        $copyObj->setPdKelas11($this->getPdKelas11());
        $copyObj->setPdKelas12($this->getPdKelas12());
        $copyObj->setPdKelas13($this->getPdKelas13());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setRekapSekolahId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return RekapSekolah Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return RekapSekolahPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new RekapSekolahPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->rekap_sekolah_id = null;
        $this->sekolah_id = null;
        $this->tanggal = null;
        $this->ptk = null;
        $this->pegawai = null;
        $this->pd = null;
        $this->rombel = null;
        $this->kode_wilayah_kecamatan = null;
        $this->kecamatan = null;
        $this->mst_kode_wilayah_kecamatan = null;
        $this->id_level_wilayah_kecamatan = null;
        $this->kode_wilayah_kabupaten = null;
        $this->kabupaten = null;
        $this->mst_kode_wilayah_kabupaten = null;
        $this->id_level_wilayah_kabupaten = null;
        $this->kode_wilayah_provinsi = null;
        $this->provinsi = null;
        $this->mst_kode_wilayah_provinsi = null;
        $this->id_level_wilayah_provinsi = null;
        $this->nama = null;
        $this->npsn = null;
        $this->bentuk_pendidikan_id = null;
        $this->status_sekolah = null;
        $this->guru_matematika = null;
        $this->guru_bahasa_indonesia = null;
        $this->guru_bahasa_inggris = null;
        $this->guru_sejarah_indonesia = null;
        $this->guru_pkn = null;
        $this->guru_penjaskes = null;
        $this->guru_agama_budi_pekerti = null;
        $this->guru_seni_budaya = null;
        $this->pd_kelas_10 = null;
        $this->pd_kelas_11 = null;
        $this->pd_kelas_12 = null;
        $this->pd_kelas_13 = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RekapSekolahPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
