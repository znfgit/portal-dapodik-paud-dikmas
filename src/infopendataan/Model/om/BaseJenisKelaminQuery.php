<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\JenisKelamin;
use infopendataan\Model\JenisKelaminPeer;
use infopendataan\Model\JenisKelaminQuery;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'jenis_kelamin' table.
 *
 * 
 *
 * @method JenisKelaminQuery orderByJenisKelaminId($order = Criteria::ASC) Order by the JENIS_KELAMIN_ID column
 * @method JenisKelaminQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 *
 * @method JenisKelaminQuery groupByJenisKelaminId() Group by the JENIS_KELAMIN_ID column
 * @method JenisKelaminQuery groupByNama() Group by the NAMA column
 *
 * @method JenisKelaminQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JenisKelaminQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JenisKelaminQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JenisKelaminQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method JenisKelaminQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method JenisKelaminQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method JenisKelamin findOne(PropelPDO $con = null) Return the first JenisKelamin matching the query
 * @method JenisKelamin findOneOrCreate(PropelPDO $con = null) Return the first JenisKelamin matching the query, or a new JenisKelamin object populated from the query conditions when no match is found
 *
 * @method JenisKelamin findOneByNama(string $NAMA) Return the first JenisKelamin filtered by the NAMA column
 *
 * @method array findByJenisKelaminId(int $JENIS_KELAMIN_ID) Return JenisKelamin objects filtered by the JENIS_KELAMIN_ID column
 * @method array findByNama(string $NAMA) Return JenisKelamin objects filtered by the NAMA column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJenisKelaminQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJenisKelaminQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JenisKelamin', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JenisKelaminQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JenisKelaminQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JenisKelaminQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JenisKelaminQuery) {
            return $criteria;
        }
        $query = new JenisKelaminQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JenisKelamin|JenisKelamin[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JenisKelaminPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JenisKelaminPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisKelamin A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJenisKelaminId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisKelamin A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `JENIS_KELAMIN_ID`, `NAMA` FROM `jenis_kelamin` WHERE `JENIS_KELAMIN_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JenisKelamin();
            $obj->hydrate($row);
            JenisKelaminPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JenisKelamin|JenisKelamin[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JenisKelamin[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JenisKelaminQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JenisKelaminQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the JENIS_KELAMIN_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisKelaminId(1234); // WHERE JENIS_KELAMIN_ID = 1234
     * $query->filterByJenisKelaminId(array(12, 34)); // WHERE JENIS_KELAMIN_ID IN (12, 34)
     * $query->filterByJenisKelaminId(array('min' => 12)); // WHERE JENIS_KELAMIN_ID >= 12
     * $query->filterByJenisKelaminId(array('max' => 12)); // WHERE JENIS_KELAMIN_ID <= 12
     * </code>
     *
     * @param     mixed $jenisKelaminId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisKelaminQuery The current query, for fluid interface
     */
    public function filterByJenisKelaminId($jenisKelaminId = null, $comparison = null)
    {
        if (is_array($jenisKelaminId)) {
            $useMinMax = false;
            if (isset($jenisKelaminId['min'])) {
                $this->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $jenisKelaminId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisKelaminId['max'])) {
                $this->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $jenisKelaminId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $jenisKelaminId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisKelaminQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisKelaminPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JenisKelaminQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $pengguna->getJenisKelaminId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JenisKelaminQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JenisKelamin $jenisKelamin Object to remove from the list of results
     *
     * @return JenisKelaminQuery The current query, for fluid interface
     */
    public function prune($jenisKelamin = null)
    {
        if ($jenisKelamin) {
            $this->addUsingAlias(JenisKelaminPeer::JENIS_KELAMIN_ID, $jenisKelamin->getJenisKelaminId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
