<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\JumlahSekolah;
use infopendataan\Model\JumlahSekolahPeer;
use infopendataan\Model\JumlahSekolahQuery;

/**
 * Base class that represents a query for the 'jumlah_sekolah' table.
 *
 * 
 *
 * @method JumlahSekolahQuery orderByJumlahSekolahId($order = Criteria::ASC) Order by the jumlah_sekolah_id column
 * @method JumlahSekolahQuery orderByTanggal($order = Criteria::ASC) Order by the tanggal column
 * @method JumlahSekolahQuery orderByIdLevelWilayah($order = Criteria::ASC) Order by the id_level_wilayah column
 * @method JumlahSekolahQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method JumlahSekolahQuery orderByMstKodeWilayah($order = Criteria::ASC) Order by the mst_kode_wilayah column
 * @method JumlahSekolahQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method JumlahSekolahQuery orderByStatusNegeri($order = Criteria::ASC) Order by the status_negeri column
 * @method JumlahSekolahQuery orderByStatusSwasta($order = Criteria::ASC) Order by the status_swasta column
 * @method JumlahSekolahQuery orderByStatusSmaNegeri($order = Criteria::ASC) Order by the status_sma_negeri column
 * @method JumlahSekolahQuery orderByStatusSmaSwasta($order = Criteria::ASC) Order by the status_sma_swasta column
 * @method JumlahSekolahQuery orderByStatusSmkNegeri($order = Criteria::ASC) Order by the status_smk_negeri column
 * @method JumlahSekolahQuery orderByStatusSmkSwasta($order = Criteria::ASC) Order by the status_smk_swasta column
 * @method JumlahSekolahQuery orderByStatusSmlbNegeri($order = Criteria::ASC) Order by the status_smlb_negeri column
 * @method JumlahSekolahQuery orderByStatusSmlbSwasta($order = Criteria::ASC) Order by the status_smlb_swasta column
 * @method JumlahSekolahQuery orderByWaktuPenyelenggaraanPagi($order = Criteria::ASC) Order by the waktu_penyelenggaraan_pagi column
 * @method JumlahSekolahQuery orderByWaktuPenyelenggaraanSiang($order = Criteria::ASC) Order by the waktu_penyelenggaraan_siang column
 * @method JumlahSekolahQuery orderByWaktuPenyelenggaraanSore($order = Criteria::ASC) Order by the waktu_penyelenggaraan_sore column
 * @method JumlahSekolahQuery orderByWaktuPenyelenggaraanMalam($order = Criteria::ASC) Order by the waktu_penyelenggaraan_malam column
 * @method JumlahSekolahQuery orderByWaktuPenyelenggaraanKombinasi($order = Criteria::ASC) Order by the waktu_penyelenggaraan_kombinasi column
 * @method JumlahSekolahQuery orderByWaktuPenyelenggaraanLainnya($order = Criteria::ASC) Order by the waktu_penyelenggaraan_lainnya column
 * @method JumlahSekolahQuery orderByAkreditasiA($order = Criteria::ASC) Order by the akreditasi_a column
 * @method JumlahSekolahQuery orderByAkreditasiB($order = Criteria::ASC) Order by the akreditasi_b column
 * @method JumlahSekolahQuery orderByAkreditasiC($order = Criteria::ASC) Order by the akreditasi_c column
 * @method JumlahSekolahQuery orderByAkreditasiTidak($order = Criteria::ASC) Order by the akreditasi_tidak column
 * @method JumlahSekolahQuery orderByListrikAda($order = Criteria::ASC) Order by the listrik_ada column
 * @method JumlahSekolahQuery orderByListrikTidak($order = Criteria::ASC) Order by the listrik_tidak column
 * @method JumlahSekolahQuery orderBySanitasiAda($order = Criteria::ASC) Order by the sanitasi_ada column
 * @method JumlahSekolahQuery orderBySanitasiTidak($order = Criteria::ASC) Order by the sanitasi_tidak column
 * @method JumlahSekolahQuery orderByAksesInternetAda($order = Criteria::ASC) Order by the akses_internet_ada column
 * @method JumlahSekolahQuery orderByAksesInternetTidak($order = Criteria::ASC) Order by the akses_internet_tidak column
 * @method JumlahSekolahQuery orderByMbsAda($order = Criteria::ASC) Order by the mbs_ada column
 * @method JumlahSekolahQuery orderByMbsTidak($order = Criteria::ASC) Order by the mbs_tidak column
 * @method JumlahSekolahQuery orderByWilayahKhususAda($order = Criteria::ASC) Order by the wilayah_khusus_ada column
 * @method JumlahSekolahQuery orderByWilayahKhususTidak($order = Criteria::ASC) Order by the wilayah_khusus_tidak column
 *
 * @method JumlahSekolahQuery groupByJumlahSekolahId() Group by the jumlah_sekolah_id column
 * @method JumlahSekolahQuery groupByTanggal() Group by the tanggal column
 * @method JumlahSekolahQuery groupByIdLevelWilayah() Group by the id_level_wilayah column
 * @method JumlahSekolahQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method JumlahSekolahQuery groupByMstKodeWilayah() Group by the mst_kode_wilayah column
 * @method JumlahSekolahQuery groupBySemesterId() Group by the semester_id column
 * @method JumlahSekolahQuery groupByStatusNegeri() Group by the status_negeri column
 * @method JumlahSekolahQuery groupByStatusSwasta() Group by the status_swasta column
 * @method JumlahSekolahQuery groupByStatusSmaNegeri() Group by the status_sma_negeri column
 * @method JumlahSekolahQuery groupByStatusSmaSwasta() Group by the status_sma_swasta column
 * @method JumlahSekolahQuery groupByStatusSmkNegeri() Group by the status_smk_negeri column
 * @method JumlahSekolahQuery groupByStatusSmkSwasta() Group by the status_smk_swasta column
 * @method JumlahSekolahQuery groupByStatusSmlbNegeri() Group by the status_smlb_negeri column
 * @method JumlahSekolahQuery groupByStatusSmlbSwasta() Group by the status_smlb_swasta column
 * @method JumlahSekolahQuery groupByWaktuPenyelenggaraanPagi() Group by the waktu_penyelenggaraan_pagi column
 * @method JumlahSekolahQuery groupByWaktuPenyelenggaraanSiang() Group by the waktu_penyelenggaraan_siang column
 * @method JumlahSekolahQuery groupByWaktuPenyelenggaraanSore() Group by the waktu_penyelenggaraan_sore column
 * @method JumlahSekolahQuery groupByWaktuPenyelenggaraanMalam() Group by the waktu_penyelenggaraan_malam column
 * @method JumlahSekolahQuery groupByWaktuPenyelenggaraanKombinasi() Group by the waktu_penyelenggaraan_kombinasi column
 * @method JumlahSekolahQuery groupByWaktuPenyelenggaraanLainnya() Group by the waktu_penyelenggaraan_lainnya column
 * @method JumlahSekolahQuery groupByAkreditasiA() Group by the akreditasi_a column
 * @method JumlahSekolahQuery groupByAkreditasiB() Group by the akreditasi_b column
 * @method JumlahSekolahQuery groupByAkreditasiC() Group by the akreditasi_c column
 * @method JumlahSekolahQuery groupByAkreditasiTidak() Group by the akreditasi_tidak column
 * @method JumlahSekolahQuery groupByListrikAda() Group by the listrik_ada column
 * @method JumlahSekolahQuery groupByListrikTidak() Group by the listrik_tidak column
 * @method JumlahSekolahQuery groupBySanitasiAda() Group by the sanitasi_ada column
 * @method JumlahSekolahQuery groupBySanitasiTidak() Group by the sanitasi_tidak column
 * @method JumlahSekolahQuery groupByAksesInternetAda() Group by the akses_internet_ada column
 * @method JumlahSekolahQuery groupByAksesInternetTidak() Group by the akses_internet_tidak column
 * @method JumlahSekolahQuery groupByMbsAda() Group by the mbs_ada column
 * @method JumlahSekolahQuery groupByMbsTidak() Group by the mbs_tidak column
 * @method JumlahSekolahQuery groupByWilayahKhususAda() Group by the wilayah_khusus_ada column
 * @method JumlahSekolahQuery groupByWilayahKhususTidak() Group by the wilayah_khusus_tidak column
 *
 * @method JumlahSekolahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JumlahSekolahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JumlahSekolahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JumlahSekolah findOne(PropelPDO $con = null) Return the first JumlahSekolah matching the query
 * @method JumlahSekolah findOneOrCreate(PropelPDO $con = null) Return the first JumlahSekolah matching the query, or a new JumlahSekolah object populated from the query conditions when no match is found
 *
 * @method JumlahSekolah findOneByTanggal(string $tanggal) Return the first JumlahSekolah filtered by the tanggal column
 * @method JumlahSekolah findOneByIdLevelWilayah(string $id_level_wilayah) Return the first JumlahSekolah filtered by the id_level_wilayah column
 * @method JumlahSekolah findOneByKodeWilayah(string $kode_wilayah) Return the first JumlahSekolah filtered by the kode_wilayah column
 * @method JumlahSekolah findOneByMstKodeWilayah(string $mst_kode_wilayah) Return the first JumlahSekolah filtered by the mst_kode_wilayah column
 * @method JumlahSekolah findOneBySemesterId(string $semester_id) Return the first JumlahSekolah filtered by the semester_id column
 * @method JumlahSekolah findOneByStatusNegeri(double $status_negeri) Return the first JumlahSekolah filtered by the status_negeri column
 * @method JumlahSekolah findOneByStatusSwasta(double $status_swasta) Return the first JumlahSekolah filtered by the status_swasta column
 * @method JumlahSekolah findOneByStatusSmaNegeri(double $status_sma_negeri) Return the first JumlahSekolah filtered by the status_sma_negeri column
 * @method JumlahSekolah findOneByStatusSmaSwasta(double $status_sma_swasta) Return the first JumlahSekolah filtered by the status_sma_swasta column
 * @method JumlahSekolah findOneByStatusSmkNegeri(double $status_smk_negeri) Return the first JumlahSekolah filtered by the status_smk_negeri column
 * @method JumlahSekolah findOneByStatusSmkSwasta(double $status_smk_swasta) Return the first JumlahSekolah filtered by the status_smk_swasta column
 * @method JumlahSekolah findOneByStatusSmlbNegeri(double $status_smlb_negeri) Return the first JumlahSekolah filtered by the status_smlb_negeri column
 * @method JumlahSekolah findOneByStatusSmlbSwasta(double $status_smlb_swasta) Return the first JumlahSekolah filtered by the status_smlb_swasta column
 * @method JumlahSekolah findOneByWaktuPenyelenggaraanPagi(double $waktu_penyelenggaraan_pagi) Return the first JumlahSekolah filtered by the waktu_penyelenggaraan_pagi column
 * @method JumlahSekolah findOneByWaktuPenyelenggaraanSiang(double $waktu_penyelenggaraan_siang) Return the first JumlahSekolah filtered by the waktu_penyelenggaraan_siang column
 * @method JumlahSekolah findOneByWaktuPenyelenggaraanSore(double $waktu_penyelenggaraan_sore) Return the first JumlahSekolah filtered by the waktu_penyelenggaraan_sore column
 * @method JumlahSekolah findOneByWaktuPenyelenggaraanMalam(double $waktu_penyelenggaraan_malam) Return the first JumlahSekolah filtered by the waktu_penyelenggaraan_malam column
 * @method JumlahSekolah findOneByWaktuPenyelenggaraanKombinasi(double $waktu_penyelenggaraan_kombinasi) Return the first JumlahSekolah filtered by the waktu_penyelenggaraan_kombinasi column
 * @method JumlahSekolah findOneByWaktuPenyelenggaraanLainnya(double $waktu_penyelenggaraan_lainnya) Return the first JumlahSekolah filtered by the waktu_penyelenggaraan_lainnya column
 * @method JumlahSekolah findOneByAkreditasiA(double $akreditasi_a) Return the first JumlahSekolah filtered by the akreditasi_a column
 * @method JumlahSekolah findOneByAkreditasiB(double $akreditasi_b) Return the first JumlahSekolah filtered by the akreditasi_b column
 * @method JumlahSekolah findOneByAkreditasiC(double $akreditasi_c) Return the first JumlahSekolah filtered by the akreditasi_c column
 * @method JumlahSekolah findOneByAkreditasiTidak(double $akreditasi_tidak) Return the first JumlahSekolah filtered by the akreditasi_tidak column
 * @method JumlahSekolah findOneByListrikAda(double $listrik_ada) Return the first JumlahSekolah filtered by the listrik_ada column
 * @method JumlahSekolah findOneByListrikTidak(double $listrik_tidak) Return the first JumlahSekolah filtered by the listrik_tidak column
 * @method JumlahSekolah findOneBySanitasiAda(double $sanitasi_ada) Return the first JumlahSekolah filtered by the sanitasi_ada column
 * @method JumlahSekolah findOneBySanitasiTidak(double $sanitasi_tidak) Return the first JumlahSekolah filtered by the sanitasi_tidak column
 * @method JumlahSekolah findOneByAksesInternetAda(double $akses_internet_ada) Return the first JumlahSekolah filtered by the akses_internet_ada column
 * @method JumlahSekolah findOneByAksesInternetTidak(double $akses_internet_tidak) Return the first JumlahSekolah filtered by the akses_internet_tidak column
 * @method JumlahSekolah findOneByMbsAda(double $mbs_ada) Return the first JumlahSekolah filtered by the mbs_ada column
 * @method JumlahSekolah findOneByMbsTidak(double $mbs_tidak) Return the first JumlahSekolah filtered by the mbs_tidak column
 * @method JumlahSekolah findOneByWilayahKhususAda(double $wilayah_khusus_ada) Return the first JumlahSekolah filtered by the wilayah_khusus_ada column
 * @method JumlahSekolah findOneByWilayahKhususTidak(double $wilayah_khusus_tidak) Return the first JumlahSekolah filtered by the wilayah_khusus_tidak column
 *
 * @method array findByJumlahSekolahId(int $jumlah_sekolah_id) Return JumlahSekolah objects filtered by the jumlah_sekolah_id column
 * @method array findByTanggal(string $tanggal) Return JumlahSekolah objects filtered by the tanggal column
 * @method array findByIdLevelWilayah(string $id_level_wilayah) Return JumlahSekolah objects filtered by the id_level_wilayah column
 * @method array findByKodeWilayah(string $kode_wilayah) Return JumlahSekolah objects filtered by the kode_wilayah column
 * @method array findByMstKodeWilayah(string $mst_kode_wilayah) Return JumlahSekolah objects filtered by the mst_kode_wilayah column
 * @method array findBySemesterId(string $semester_id) Return JumlahSekolah objects filtered by the semester_id column
 * @method array findByStatusNegeri(double $status_negeri) Return JumlahSekolah objects filtered by the status_negeri column
 * @method array findByStatusSwasta(double $status_swasta) Return JumlahSekolah objects filtered by the status_swasta column
 * @method array findByStatusSmaNegeri(double $status_sma_negeri) Return JumlahSekolah objects filtered by the status_sma_negeri column
 * @method array findByStatusSmaSwasta(double $status_sma_swasta) Return JumlahSekolah objects filtered by the status_sma_swasta column
 * @method array findByStatusSmkNegeri(double $status_smk_negeri) Return JumlahSekolah objects filtered by the status_smk_negeri column
 * @method array findByStatusSmkSwasta(double $status_smk_swasta) Return JumlahSekolah objects filtered by the status_smk_swasta column
 * @method array findByStatusSmlbNegeri(double $status_smlb_negeri) Return JumlahSekolah objects filtered by the status_smlb_negeri column
 * @method array findByStatusSmlbSwasta(double $status_smlb_swasta) Return JumlahSekolah objects filtered by the status_smlb_swasta column
 * @method array findByWaktuPenyelenggaraanPagi(double $waktu_penyelenggaraan_pagi) Return JumlahSekolah objects filtered by the waktu_penyelenggaraan_pagi column
 * @method array findByWaktuPenyelenggaraanSiang(double $waktu_penyelenggaraan_siang) Return JumlahSekolah objects filtered by the waktu_penyelenggaraan_siang column
 * @method array findByWaktuPenyelenggaraanSore(double $waktu_penyelenggaraan_sore) Return JumlahSekolah objects filtered by the waktu_penyelenggaraan_sore column
 * @method array findByWaktuPenyelenggaraanMalam(double $waktu_penyelenggaraan_malam) Return JumlahSekolah objects filtered by the waktu_penyelenggaraan_malam column
 * @method array findByWaktuPenyelenggaraanKombinasi(double $waktu_penyelenggaraan_kombinasi) Return JumlahSekolah objects filtered by the waktu_penyelenggaraan_kombinasi column
 * @method array findByWaktuPenyelenggaraanLainnya(double $waktu_penyelenggaraan_lainnya) Return JumlahSekolah objects filtered by the waktu_penyelenggaraan_lainnya column
 * @method array findByAkreditasiA(double $akreditasi_a) Return JumlahSekolah objects filtered by the akreditasi_a column
 * @method array findByAkreditasiB(double $akreditasi_b) Return JumlahSekolah objects filtered by the akreditasi_b column
 * @method array findByAkreditasiC(double $akreditasi_c) Return JumlahSekolah objects filtered by the akreditasi_c column
 * @method array findByAkreditasiTidak(double $akreditasi_tidak) Return JumlahSekolah objects filtered by the akreditasi_tidak column
 * @method array findByListrikAda(double $listrik_ada) Return JumlahSekolah objects filtered by the listrik_ada column
 * @method array findByListrikTidak(double $listrik_tidak) Return JumlahSekolah objects filtered by the listrik_tidak column
 * @method array findBySanitasiAda(double $sanitasi_ada) Return JumlahSekolah objects filtered by the sanitasi_ada column
 * @method array findBySanitasiTidak(double $sanitasi_tidak) Return JumlahSekolah objects filtered by the sanitasi_tidak column
 * @method array findByAksesInternetAda(double $akses_internet_ada) Return JumlahSekolah objects filtered by the akses_internet_ada column
 * @method array findByAksesInternetTidak(double $akses_internet_tidak) Return JumlahSekolah objects filtered by the akses_internet_tidak column
 * @method array findByMbsAda(double $mbs_ada) Return JumlahSekolah objects filtered by the mbs_ada column
 * @method array findByMbsTidak(double $mbs_tidak) Return JumlahSekolah objects filtered by the mbs_tidak column
 * @method array findByWilayahKhususAda(double $wilayah_khusus_ada) Return JumlahSekolah objects filtered by the wilayah_khusus_ada column
 * @method array findByWilayahKhususTidak(double $wilayah_khusus_tidak) Return JumlahSekolah objects filtered by the wilayah_khusus_tidak column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJumlahSekolahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJumlahSekolahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JumlahSekolah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JumlahSekolahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JumlahSekolahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JumlahSekolahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JumlahSekolahQuery) {
            return $criteria;
        }
        $query = new JumlahSekolahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JumlahSekolah|JumlahSekolah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JumlahSekolahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JumlahSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JumlahSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJumlahSekolahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JumlahSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `jumlah_sekolah_id`, `tanggal`, `id_level_wilayah`, `kode_wilayah`, `mst_kode_wilayah`, `semester_id`, `status_negeri`, `status_swasta`, `status_sma_negeri`, `status_sma_swasta`, `status_smk_negeri`, `status_smk_swasta`, `status_smlb_negeri`, `status_smlb_swasta`, `waktu_penyelenggaraan_pagi`, `waktu_penyelenggaraan_siang`, `waktu_penyelenggaraan_sore`, `waktu_penyelenggaraan_malam`, `waktu_penyelenggaraan_kombinasi`, `waktu_penyelenggaraan_lainnya`, `akreditasi_a`, `akreditasi_b`, `akreditasi_c`, `akreditasi_tidak`, `listrik_ada`, `listrik_tidak`, `sanitasi_ada`, `sanitasi_tidak`, `akses_internet_ada`, `akses_internet_tidak`, `mbs_ada`, `mbs_tidak`, `wilayah_khusus_ada`, `wilayah_khusus_tidak` FROM `jumlah_sekolah` WHERE `jumlah_sekolah_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JumlahSekolah();
            $obj->hydrate($row);
            JumlahSekolahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JumlahSekolah|JumlahSekolah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JumlahSekolah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the jumlah_sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahSekolahId(1234); // WHERE jumlah_sekolah_id = 1234
     * $query->filterByJumlahSekolahId(array(12, 34)); // WHERE jumlah_sekolah_id IN (12, 34)
     * $query->filterByJumlahSekolahId(array('min' => 12)); // WHERE jumlah_sekolah_id >= 12
     * $query->filterByJumlahSekolahId(array('max' => 12)); // WHERE jumlah_sekolah_id <= 12
     * </code>
     *
     * @param     mixed $jumlahSekolahId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByJumlahSekolahId($jumlahSekolahId = null, $comparison = null)
    {
        if (is_array($jumlahSekolahId)) {
            $useMinMax = false;
            if (isset($jumlahSekolahId['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $jumlahSekolahId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahSekolahId['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $jumlahSekolahId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $jumlahSekolahId, $comparison);
    }

    /**
     * Filter the query on the tanggal column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('fooValue');   // WHERE tanggal = 'fooValue'
     * $query->filterByTanggal('%fooValue%'); // WHERE tanggal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggal The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggal)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggal)) {
                $tanggal = str_replace('*', '%', $tanggal);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayah('fooValue');   // WHERE id_level_wilayah = 'fooValue'
     * $query->filterByIdLevelWilayah('%fooValue%'); // WHERE id_level_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idLevelWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayah($idLevelWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idLevelWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idLevelWilayah)) {
                $idLevelWilayah = str_replace('*', '%', $idLevelWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::ID_LEVEL_WILAYAH, $idLevelWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayah('fooValue');   // WHERE mst_kode_wilayah = 'fooValue'
     * $query->filterByMstKodeWilayah('%fooValue%'); // WHERE mst_kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayah($mstKodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayah)) {
                $mstKodeWilayah = str_replace('*', '%', $mstKodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::MST_KODE_WILAYAH, $mstKodeWilayah, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the status_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusNegeri(1234); // WHERE status_negeri = 1234
     * $query->filterByStatusNegeri(array(12, 34)); // WHERE status_negeri IN (12, 34)
     * $query->filterByStatusNegeri(array('min' => 12)); // WHERE status_negeri >= 12
     * $query->filterByStatusNegeri(array('max' => 12)); // WHERE status_negeri <= 12
     * </code>
     *
     * @param     mixed $statusNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusNegeri($statusNegeri = null, $comparison = null)
    {
        if (is_array($statusNegeri)) {
            $useMinMax = false;
            if (isset($statusNegeri['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_NEGERI, $statusNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusNegeri['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_NEGERI, $statusNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_NEGERI, $statusNegeri, $comparison);
    }

    /**
     * Filter the query on the status_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSwasta(1234); // WHERE status_swasta = 1234
     * $query->filterByStatusSwasta(array(12, 34)); // WHERE status_swasta IN (12, 34)
     * $query->filterByStatusSwasta(array('min' => 12)); // WHERE status_swasta >= 12
     * $query->filterByStatusSwasta(array('max' => 12)); // WHERE status_swasta <= 12
     * </code>
     *
     * @param     mixed $statusSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSwasta($statusSwasta = null, $comparison = null)
    {
        if (is_array($statusSwasta)) {
            $useMinMax = false;
            if (isset($statusSwasta['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SWASTA, $statusSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSwasta['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SWASTA, $statusSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SWASTA, $statusSwasta, $comparison);
    }

    /**
     * Filter the query on the status_sma_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSmaNegeri(1234); // WHERE status_sma_negeri = 1234
     * $query->filterByStatusSmaNegeri(array(12, 34)); // WHERE status_sma_negeri IN (12, 34)
     * $query->filterByStatusSmaNegeri(array('min' => 12)); // WHERE status_sma_negeri >= 12
     * $query->filterByStatusSmaNegeri(array('max' => 12)); // WHERE status_sma_negeri <= 12
     * </code>
     *
     * @param     mixed $statusSmaNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSmaNegeri($statusSmaNegeri = null, $comparison = null)
    {
        if (is_array($statusSmaNegeri)) {
            $useMinMax = false;
            if (isset($statusSmaNegeri['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMA_NEGERI, $statusSmaNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSmaNegeri['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMA_NEGERI, $statusSmaNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMA_NEGERI, $statusSmaNegeri, $comparison);
    }

    /**
     * Filter the query on the status_sma_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSmaSwasta(1234); // WHERE status_sma_swasta = 1234
     * $query->filterByStatusSmaSwasta(array(12, 34)); // WHERE status_sma_swasta IN (12, 34)
     * $query->filterByStatusSmaSwasta(array('min' => 12)); // WHERE status_sma_swasta >= 12
     * $query->filterByStatusSmaSwasta(array('max' => 12)); // WHERE status_sma_swasta <= 12
     * </code>
     *
     * @param     mixed $statusSmaSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSmaSwasta($statusSmaSwasta = null, $comparison = null)
    {
        if (is_array($statusSmaSwasta)) {
            $useMinMax = false;
            if (isset($statusSmaSwasta['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMA_SWASTA, $statusSmaSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSmaSwasta['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMA_SWASTA, $statusSmaSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMA_SWASTA, $statusSmaSwasta, $comparison);
    }

    /**
     * Filter the query on the status_smk_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSmkNegeri(1234); // WHERE status_smk_negeri = 1234
     * $query->filterByStatusSmkNegeri(array(12, 34)); // WHERE status_smk_negeri IN (12, 34)
     * $query->filterByStatusSmkNegeri(array('min' => 12)); // WHERE status_smk_negeri >= 12
     * $query->filterByStatusSmkNegeri(array('max' => 12)); // WHERE status_smk_negeri <= 12
     * </code>
     *
     * @param     mixed $statusSmkNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSmkNegeri($statusSmkNegeri = null, $comparison = null)
    {
        if (is_array($statusSmkNegeri)) {
            $useMinMax = false;
            if (isset($statusSmkNegeri['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMK_NEGERI, $statusSmkNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSmkNegeri['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMK_NEGERI, $statusSmkNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMK_NEGERI, $statusSmkNegeri, $comparison);
    }

    /**
     * Filter the query on the status_smk_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSmkSwasta(1234); // WHERE status_smk_swasta = 1234
     * $query->filterByStatusSmkSwasta(array(12, 34)); // WHERE status_smk_swasta IN (12, 34)
     * $query->filterByStatusSmkSwasta(array('min' => 12)); // WHERE status_smk_swasta >= 12
     * $query->filterByStatusSmkSwasta(array('max' => 12)); // WHERE status_smk_swasta <= 12
     * </code>
     *
     * @param     mixed $statusSmkSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSmkSwasta($statusSmkSwasta = null, $comparison = null)
    {
        if (is_array($statusSmkSwasta)) {
            $useMinMax = false;
            if (isset($statusSmkSwasta['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMK_SWASTA, $statusSmkSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSmkSwasta['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMK_SWASTA, $statusSmkSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMK_SWASTA, $statusSmkSwasta, $comparison);
    }

    /**
     * Filter the query on the status_smlb_negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSmlbNegeri(1234); // WHERE status_smlb_negeri = 1234
     * $query->filterByStatusSmlbNegeri(array(12, 34)); // WHERE status_smlb_negeri IN (12, 34)
     * $query->filterByStatusSmlbNegeri(array('min' => 12)); // WHERE status_smlb_negeri >= 12
     * $query->filterByStatusSmlbNegeri(array('max' => 12)); // WHERE status_smlb_negeri <= 12
     * </code>
     *
     * @param     mixed $statusSmlbNegeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSmlbNegeri($statusSmlbNegeri = null, $comparison = null)
    {
        if (is_array($statusSmlbNegeri)) {
            $useMinMax = false;
            if (isset($statusSmlbNegeri['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMLB_NEGERI, $statusSmlbNegeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSmlbNegeri['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMLB_NEGERI, $statusSmlbNegeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMLB_NEGERI, $statusSmlbNegeri, $comparison);
    }

    /**
     * Filter the query on the status_smlb_swasta column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSmlbSwasta(1234); // WHERE status_smlb_swasta = 1234
     * $query->filterByStatusSmlbSwasta(array(12, 34)); // WHERE status_smlb_swasta IN (12, 34)
     * $query->filterByStatusSmlbSwasta(array('min' => 12)); // WHERE status_smlb_swasta >= 12
     * $query->filterByStatusSmlbSwasta(array('max' => 12)); // WHERE status_smlb_swasta <= 12
     * </code>
     *
     * @param     mixed $statusSmlbSwasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSmlbSwasta($statusSmlbSwasta = null, $comparison = null)
    {
        if (is_array($statusSmlbSwasta)) {
            $useMinMax = false;
            if (isset($statusSmlbSwasta['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMLB_SWASTA, $statusSmlbSwasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSmlbSwasta['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMLB_SWASTA, $statusSmlbSwasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::STATUS_SMLB_SWASTA, $statusSmlbSwasta, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_pagi column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanPagi(1234); // WHERE waktu_penyelenggaraan_pagi = 1234
     * $query->filterByWaktuPenyelenggaraanPagi(array(12, 34)); // WHERE waktu_penyelenggaraan_pagi IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanPagi(array('min' => 12)); // WHERE waktu_penyelenggaraan_pagi >= 12
     * $query->filterByWaktuPenyelenggaraanPagi(array('max' => 12)); // WHERE waktu_penyelenggaraan_pagi <= 12
     * </code>
     *
     * @param     mixed $waktuPenyelenggaraanPagi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanPagi($waktuPenyelenggaraanPagi = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanPagi)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanPagi['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI, $waktuPenyelenggaraanPagi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanPagi['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI, $waktuPenyelenggaraanPagi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_PAGI, $waktuPenyelenggaraanPagi, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_siang column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanSiang(1234); // WHERE waktu_penyelenggaraan_siang = 1234
     * $query->filterByWaktuPenyelenggaraanSiang(array(12, 34)); // WHERE waktu_penyelenggaraan_siang IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanSiang(array('min' => 12)); // WHERE waktu_penyelenggaraan_siang >= 12
     * $query->filterByWaktuPenyelenggaraanSiang(array('max' => 12)); // WHERE waktu_penyelenggaraan_siang <= 12
     * </code>
     *
     * @param     mixed $waktuPenyelenggaraanSiang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanSiang($waktuPenyelenggaraanSiang = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanSiang)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanSiang['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG, $waktuPenyelenggaraanSiang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanSiang['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG, $waktuPenyelenggaraanSiang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SIANG, $waktuPenyelenggaraanSiang, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_sore column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanSore(1234); // WHERE waktu_penyelenggaraan_sore = 1234
     * $query->filterByWaktuPenyelenggaraanSore(array(12, 34)); // WHERE waktu_penyelenggaraan_sore IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanSore(array('min' => 12)); // WHERE waktu_penyelenggaraan_sore >= 12
     * $query->filterByWaktuPenyelenggaraanSore(array('max' => 12)); // WHERE waktu_penyelenggaraan_sore <= 12
     * </code>
     *
     * @param     mixed $waktuPenyelenggaraanSore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanSore($waktuPenyelenggaraanSore = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanSore)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanSore['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE, $waktuPenyelenggaraanSore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanSore['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE, $waktuPenyelenggaraanSore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_SORE, $waktuPenyelenggaraanSore, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_malam column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanMalam(1234); // WHERE waktu_penyelenggaraan_malam = 1234
     * $query->filterByWaktuPenyelenggaraanMalam(array(12, 34)); // WHERE waktu_penyelenggaraan_malam IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanMalam(array('min' => 12)); // WHERE waktu_penyelenggaraan_malam >= 12
     * $query->filterByWaktuPenyelenggaraanMalam(array('max' => 12)); // WHERE waktu_penyelenggaraan_malam <= 12
     * </code>
     *
     * @param     mixed $waktuPenyelenggaraanMalam The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanMalam($waktuPenyelenggaraanMalam = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanMalam)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanMalam['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM, $waktuPenyelenggaraanMalam['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanMalam['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM, $waktuPenyelenggaraanMalam['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_MALAM, $waktuPenyelenggaraanMalam, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_kombinasi column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanKombinasi(1234); // WHERE waktu_penyelenggaraan_kombinasi = 1234
     * $query->filterByWaktuPenyelenggaraanKombinasi(array(12, 34)); // WHERE waktu_penyelenggaraan_kombinasi IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanKombinasi(array('min' => 12)); // WHERE waktu_penyelenggaraan_kombinasi >= 12
     * $query->filterByWaktuPenyelenggaraanKombinasi(array('max' => 12)); // WHERE waktu_penyelenggaraan_kombinasi <= 12
     * </code>
     *
     * @param     mixed $waktuPenyelenggaraanKombinasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanKombinasi($waktuPenyelenggaraanKombinasi = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanKombinasi)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanKombinasi['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI, $waktuPenyelenggaraanKombinasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanKombinasi['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI, $waktuPenyelenggaraanKombinasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_KOMBINASI, $waktuPenyelenggaraanKombinasi, $comparison);
    }

    /**
     * Filter the query on the waktu_penyelenggaraan_lainnya column
     *
     * Example usage:
     * <code>
     * $query->filterByWaktuPenyelenggaraanLainnya(1234); // WHERE waktu_penyelenggaraan_lainnya = 1234
     * $query->filterByWaktuPenyelenggaraanLainnya(array(12, 34)); // WHERE waktu_penyelenggaraan_lainnya IN (12, 34)
     * $query->filterByWaktuPenyelenggaraanLainnya(array('min' => 12)); // WHERE waktu_penyelenggaraan_lainnya >= 12
     * $query->filterByWaktuPenyelenggaraanLainnya(array('max' => 12)); // WHERE waktu_penyelenggaraan_lainnya <= 12
     * </code>
     *
     * @param     mixed $waktuPenyelenggaraanLainnya The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWaktuPenyelenggaraanLainnya($waktuPenyelenggaraanLainnya = null, $comparison = null)
    {
        if (is_array($waktuPenyelenggaraanLainnya)) {
            $useMinMax = false;
            if (isset($waktuPenyelenggaraanLainnya['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA, $waktuPenyelenggaraanLainnya['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($waktuPenyelenggaraanLainnya['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA, $waktuPenyelenggaraanLainnya['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WAKTU_PENYELENGGARAAN_LAINNYA, $waktuPenyelenggaraanLainnya, $comparison);
    }

    /**
     * Filter the query on the akreditasi_a column
     *
     * Example usage:
     * <code>
     * $query->filterByAkreditasiA(1234); // WHERE akreditasi_a = 1234
     * $query->filterByAkreditasiA(array(12, 34)); // WHERE akreditasi_a IN (12, 34)
     * $query->filterByAkreditasiA(array('min' => 12)); // WHERE akreditasi_a >= 12
     * $query->filterByAkreditasiA(array('max' => 12)); // WHERE akreditasi_a <= 12
     * </code>
     *
     * @param     mixed $akreditasiA The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByAkreditasiA($akreditasiA = null, $comparison = null)
    {
        if (is_array($akreditasiA)) {
            $useMinMax = false;
            if (isset($akreditasiA['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_A, $akreditasiA['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($akreditasiA['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_A, $akreditasiA['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_A, $akreditasiA, $comparison);
    }

    /**
     * Filter the query on the akreditasi_b column
     *
     * Example usage:
     * <code>
     * $query->filterByAkreditasiB(1234); // WHERE akreditasi_b = 1234
     * $query->filterByAkreditasiB(array(12, 34)); // WHERE akreditasi_b IN (12, 34)
     * $query->filterByAkreditasiB(array('min' => 12)); // WHERE akreditasi_b >= 12
     * $query->filterByAkreditasiB(array('max' => 12)); // WHERE akreditasi_b <= 12
     * </code>
     *
     * @param     mixed $akreditasiB The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByAkreditasiB($akreditasiB = null, $comparison = null)
    {
        if (is_array($akreditasiB)) {
            $useMinMax = false;
            if (isset($akreditasiB['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_B, $akreditasiB['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($akreditasiB['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_B, $akreditasiB['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_B, $akreditasiB, $comparison);
    }

    /**
     * Filter the query on the akreditasi_c column
     *
     * Example usage:
     * <code>
     * $query->filterByAkreditasiC(1234); // WHERE akreditasi_c = 1234
     * $query->filterByAkreditasiC(array(12, 34)); // WHERE akreditasi_c IN (12, 34)
     * $query->filterByAkreditasiC(array('min' => 12)); // WHERE akreditasi_c >= 12
     * $query->filterByAkreditasiC(array('max' => 12)); // WHERE akreditasi_c <= 12
     * </code>
     *
     * @param     mixed $akreditasiC The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByAkreditasiC($akreditasiC = null, $comparison = null)
    {
        if (is_array($akreditasiC)) {
            $useMinMax = false;
            if (isset($akreditasiC['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_C, $akreditasiC['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($akreditasiC['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_C, $akreditasiC['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_C, $akreditasiC, $comparison);
    }

    /**
     * Filter the query on the akreditasi_tidak column
     *
     * Example usage:
     * <code>
     * $query->filterByAkreditasiTidak(1234); // WHERE akreditasi_tidak = 1234
     * $query->filterByAkreditasiTidak(array(12, 34)); // WHERE akreditasi_tidak IN (12, 34)
     * $query->filterByAkreditasiTidak(array('min' => 12)); // WHERE akreditasi_tidak >= 12
     * $query->filterByAkreditasiTidak(array('max' => 12)); // WHERE akreditasi_tidak <= 12
     * </code>
     *
     * @param     mixed $akreditasiTidak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByAkreditasiTidak($akreditasiTidak = null, $comparison = null)
    {
        if (is_array($akreditasiTidak)) {
            $useMinMax = false;
            if (isset($akreditasiTidak['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_TIDAK, $akreditasiTidak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($akreditasiTidak['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_TIDAK, $akreditasiTidak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::AKREDITASI_TIDAK, $akreditasiTidak, $comparison);
    }

    /**
     * Filter the query on the listrik_ada column
     *
     * Example usage:
     * <code>
     * $query->filterByListrikAda(1234); // WHERE listrik_ada = 1234
     * $query->filterByListrikAda(array(12, 34)); // WHERE listrik_ada IN (12, 34)
     * $query->filterByListrikAda(array('min' => 12)); // WHERE listrik_ada >= 12
     * $query->filterByListrikAda(array('max' => 12)); // WHERE listrik_ada <= 12
     * </code>
     *
     * @param     mixed $listrikAda The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByListrikAda($listrikAda = null, $comparison = null)
    {
        if (is_array($listrikAda)) {
            $useMinMax = false;
            if (isset($listrikAda['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::LISTRIK_ADA, $listrikAda['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($listrikAda['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::LISTRIK_ADA, $listrikAda['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::LISTRIK_ADA, $listrikAda, $comparison);
    }

    /**
     * Filter the query on the listrik_tidak column
     *
     * Example usage:
     * <code>
     * $query->filterByListrikTidak(1234); // WHERE listrik_tidak = 1234
     * $query->filterByListrikTidak(array(12, 34)); // WHERE listrik_tidak IN (12, 34)
     * $query->filterByListrikTidak(array('min' => 12)); // WHERE listrik_tidak >= 12
     * $query->filterByListrikTidak(array('max' => 12)); // WHERE listrik_tidak <= 12
     * </code>
     *
     * @param     mixed $listrikTidak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByListrikTidak($listrikTidak = null, $comparison = null)
    {
        if (is_array($listrikTidak)) {
            $useMinMax = false;
            if (isset($listrikTidak['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::LISTRIK_TIDAK, $listrikTidak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($listrikTidak['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::LISTRIK_TIDAK, $listrikTidak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::LISTRIK_TIDAK, $listrikTidak, $comparison);
    }

    /**
     * Filter the query on the sanitasi_ada column
     *
     * Example usage:
     * <code>
     * $query->filterBySanitasiAda(1234); // WHERE sanitasi_ada = 1234
     * $query->filterBySanitasiAda(array(12, 34)); // WHERE sanitasi_ada IN (12, 34)
     * $query->filterBySanitasiAda(array('min' => 12)); // WHERE sanitasi_ada >= 12
     * $query->filterBySanitasiAda(array('max' => 12)); // WHERE sanitasi_ada <= 12
     * </code>
     *
     * @param     mixed $sanitasiAda The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterBySanitasiAda($sanitasiAda = null, $comparison = null)
    {
        if (is_array($sanitasiAda)) {
            $useMinMax = false;
            if (isset($sanitasiAda['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::SANITASI_ADA, $sanitasiAda['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sanitasiAda['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::SANITASI_ADA, $sanitasiAda['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::SANITASI_ADA, $sanitasiAda, $comparison);
    }

    /**
     * Filter the query on the sanitasi_tidak column
     *
     * Example usage:
     * <code>
     * $query->filterBySanitasiTidak(1234); // WHERE sanitasi_tidak = 1234
     * $query->filterBySanitasiTidak(array(12, 34)); // WHERE sanitasi_tidak IN (12, 34)
     * $query->filterBySanitasiTidak(array('min' => 12)); // WHERE sanitasi_tidak >= 12
     * $query->filterBySanitasiTidak(array('max' => 12)); // WHERE sanitasi_tidak <= 12
     * </code>
     *
     * @param     mixed $sanitasiTidak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterBySanitasiTidak($sanitasiTidak = null, $comparison = null)
    {
        if (is_array($sanitasiTidak)) {
            $useMinMax = false;
            if (isset($sanitasiTidak['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::SANITASI_TIDAK, $sanitasiTidak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sanitasiTidak['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::SANITASI_TIDAK, $sanitasiTidak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::SANITASI_TIDAK, $sanitasiTidak, $comparison);
    }

    /**
     * Filter the query on the akses_internet_ada column
     *
     * Example usage:
     * <code>
     * $query->filterByAksesInternetAda(1234); // WHERE akses_internet_ada = 1234
     * $query->filterByAksesInternetAda(array(12, 34)); // WHERE akses_internet_ada IN (12, 34)
     * $query->filterByAksesInternetAda(array('min' => 12)); // WHERE akses_internet_ada >= 12
     * $query->filterByAksesInternetAda(array('max' => 12)); // WHERE akses_internet_ada <= 12
     * </code>
     *
     * @param     mixed $aksesInternetAda The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByAksesInternetAda($aksesInternetAda = null, $comparison = null)
    {
        if (is_array($aksesInternetAda)) {
            $useMinMax = false;
            if (isset($aksesInternetAda['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKSES_INTERNET_ADA, $aksesInternetAda['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aksesInternetAda['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKSES_INTERNET_ADA, $aksesInternetAda['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::AKSES_INTERNET_ADA, $aksesInternetAda, $comparison);
    }

    /**
     * Filter the query on the akses_internet_tidak column
     *
     * Example usage:
     * <code>
     * $query->filterByAksesInternetTidak(1234); // WHERE akses_internet_tidak = 1234
     * $query->filterByAksesInternetTidak(array(12, 34)); // WHERE akses_internet_tidak IN (12, 34)
     * $query->filterByAksesInternetTidak(array('min' => 12)); // WHERE akses_internet_tidak >= 12
     * $query->filterByAksesInternetTidak(array('max' => 12)); // WHERE akses_internet_tidak <= 12
     * </code>
     *
     * @param     mixed $aksesInternetTidak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByAksesInternetTidak($aksesInternetTidak = null, $comparison = null)
    {
        if (is_array($aksesInternetTidak)) {
            $useMinMax = false;
            if (isset($aksesInternetTidak['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKSES_INTERNET_TIDAK, $aksesInternetTidak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aksesInternetTidak['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::AKSES_INTERNET_TIDAK, $aksesInternetTidak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::AKSES_INTERNET_TIDAK, $aksesInternetTidak, $comparison);
    }

    /**
     * Filter the query on the mbs_ada column
     *
     * Example usage:
     * <code>
     * $query->filterByMbsAda(1234); // WHERE mbs_ada = 1234
     * $query->filterByMbsAda(array(12, 34)); // WHERE mbs_ada IN (12, 34)
     * $query->filterByMbsAda(array('min' => 12)); // WHERE mbs_ada >= 12
     * $query->filterByMbsAda(array('max' => 12)); // WHERE mbs_ada <= 12
     * </code>
     *
     * @param     mixed $mbsAda The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByMbsAda($mbsAda = null, $comparison = null)
    {
        if (is_array($mbsAda)) {
            $useMinMax = false;
            if (isset($mbsAda['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::MBS_ADA, $mbsAda['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mbsAda['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::MBS_ADA, $mbsAda['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::MBS_ADA, $mbsAda, $comparison);
    }

    /**
     * Filter the query on the mbs_tidak column
     *
     * Example usage:
     * <code>
     * $query->filterByMbsTidak(1234); // WHERE mbs_tidak = 1234
     * $query->filterByMbsTidak(array(12, 34)); // WHERE mbs_tidak IN (12, 34)
     * $query->filterByMbsTidak(array('min' => 12)); // WHERE mbs_tidak >= 12
     * $query->filterByMbsTidak(array('max' => 12)); // WHERE mbs_tidak <= 12
     * </code>
     *
     * @param     mixed $mbsTidak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByMbsTidak($mbsTidak = null, $comparison = null)
    {
        if (is_array($mbsTidak)) {
            $useMinMax = false;
            if (isset($mbsTidak['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::MBS_TIDAK, $mbsTidak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mbsTidak['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::MBS_TIDAK, $mbsTidak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::MBS_TIDAK, $mbsTidak, $comparison);
    }

    /**
     * Filter the query on the wilayah_khusus_ada column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahKhususAda(1234); // WHERE wilayah_khusus_ada = 1234
     * $query->filterByWilayahKhususAda(array(12, 34)); // WHERE wilayah_khusus_ada IN (12, 34)
     * $query->filterByWilayahKhususAda(array('min' => 12)); // WHERE wilayah_khusus_ada >= 12
     * $query->filterByWilayahKhususAda(array('max' => 12)); // WHERE wilayah_khusus_ada <= 12
     * </code>
     *
     * @param     mixed $wilayahKhususAda The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayahKhususAda($wilayahKhususAda = null, $comparison = null)
    {
        if (is_array($wilayahKhususAda)) {
            $useMinMax = false;
            if (isset($wilayahKhususAda['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WILAYAH_KHUSUS_ADA, $wilayahKhususAda['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahKhususAda['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WILAYAH_KHUSUS_ADA, $wilayahKhususAda['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WILAYAH_KHUSUS_ADA, $wilayahKhususAda, $comparison);
    }

    /**
     * Filter the query on the wilayah_khusus_tidak column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahKhususTidak(1234); // WHERE wilayah_khusus_tidak = 1234
     * $query->filterByWilayahKhususTidak(array(12, 34)); // WHERE wilayah_khusus_tidak IN (12, 34)
     * $query->filterByWilayahKhususTidak(array('min' => 12)); // WHERE wilayah_khusus_tidak >= 12
     * $query->filterByWilayahKhususTidak(array('max' => 12)); // WHERE wilayah_khusus_tidak <= 12
     * </code>
     *
     * @param     mixed $wilayahKhususTidak The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayahKhususTidak($wilayahKhususTidak = null, $comparison = null)
    {
        if (is_array($wilayahKhususTidak)) {
            $useMinMax = false;
            if (isset($wilayahKhususTidak['min'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK, $wilayahKhususTidak['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wilayahKhususTidak['max'])) {
                $this->addUsingAlias(JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK, $wilayahKhususTidak['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JumlahSekolahPeer::WILAYAH_KHUSUS_TIDAK, $wilayahKhususTidak, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   JumlahSekolah $jumlahSekolah Object to remove from the list of results
     *
     * @return JumlahSekolahQuery The current query, for fluid interface
     */
    public function prune($jumlahSekolah = null)
    {
        if ($jumlahSekolah) {
            $this->addUsingAlias(JumlahSekolahPeer::JUMLAH_SEKOLAH_ID, $jumlahSekolah->getJumlahSekolahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
