<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\Dokumentasi;
use infopendataan\Model\DokumentasiPeer;
use infopendataan\Model\DokumentasiQuery;
use infopendataan\Model\JenisDokumentasi;
use infopendataan\Model\Pengguna;

/**
 * Base class that represents a query for the 'dokumentasi' table.
 *
 * 
 *
 * @method DokumentasiQuery orderByDokumentasiId($order = Criteria::ASC) Order by the DOKUMENTASI_ID column
 * @method DokumentasiQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method DokumentasiQuery orderByJenisDokumentasiId($order = Criteria::ASC) Order by the JENIS_DOKUMENTASI_ID column
 * @method DokumentasiQuery orderByJudul($order = Criteria::ASC) Order by the JUDUL column
 * @method DokumentasiQuery orderByDeskripsi($order = Criteria::ASC) Order by the DESKRIPSI column
 * @method DokumentasiQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method DokumentasiQuery orderByGambar($order = Criteria::ASC) Order by the GAMBAR column
 *
 * @method DokumentasiQuery groupByDokumentasiId() Group by the DOKUMENTASI_ID column
 * @method DokumentasiQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method DokumentasiQuery groupByJenisDokumentasiId() Group by the JENIS_DOKUMENTASI_ID column
 * @method DokumentasiQuery groupByJudul() Group by the JUDUL column
 * @method DokumentasiQuery groupByDeskripsi() Group by the DESKRIPSI column
 * @method DokumentasiQuery groupByTanggal() Group by the TANGGAL column
 * @method DokumentasiQuery groupByGambar() Group by the GAMBAR column
 *
 * @method DokumentasiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DokumentasiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DokumentasiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DokumentasiQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method DokumentasiQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method DokumentasiQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method DokumentasiQuery leftJoinJenisDokumentasi($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisDokumentasi relation
 * @method DokumentasiQuery rightJoinJenisDokumentasi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisDokumentasi relation
 * @method DokumentasiQuery innerJoinJenisDokumentasi($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisDokumentasi relation
 *
 * @method Dokumentasi findOne(PropelPDO $con = null) Return the first Dokumentasi matching the query
 * @method Dokumentasi findOneOrCreate(PropelPDO $con = null) Return the first Dokumentasi matching the query, or a new Dokumentasi object populated from the query conditions when no match is found
 *
 * @method Dokumentasi findOneByPenggunaId(int $PENGGUNA_ID) Return the first Dokumentasi filtered by the PENGGUNA_ID column
 * @method Dokumentasi findOneByJenisDokumentasiId(int $JENIS_DOKUMENTASI_ID) Return the first Dokumentasi filtered by the JENIS_DOKUMENTASI_ID column
 * @method Dokumentasi findOneByJudul(string $JUDUL) Return the first Dokumentasi filtered by the JUDUL column
 * @method Dokumentasi findOneByDeskripsi(string $DESKRIPSI) Return the first Dokumentasi filtered by the DESKRIPSI column
 * @method Dokumentasi findOneByTanggal(string $TANGGAL) Return the first Dokumentasi filtered by the TANGGAL column
 * @method Dokumentasi findOneByGambar(string $GAMBAR) Return the first Dokumentasi filtered by the GAMBAR column
 *
 * @method array findByDokumentasiId(int $DOKUMENTASI_ID) Return Dokumentasi objects filtered by the DOKUMENTASI_ID column
 * @method array findByPenggunaId(int $PENGGUNA_ID) Return Dokumentasi objects filtered by the PENGGUNA_ID column
 * @method array findByJenisDokumentasiId(int $JENIS_DOKUMENTASI_ID) Return Dokumentasi objects filtered by the JENIS_DOKUMENTASI_ID column
 * @method array findByJudul(string $JUDUL) Return Dokumentasi objects filtered by the JUDUL column
 * @method array findByDeskripsi(string $DESKRIPSI) Return Dokumentasi objects filtered by the DESKRIPSI column
 * @method array findByTanggal(string $TANGGAL) Return Dokumentasi objects filtered by the TANGGAL column
 * @method array findByGambar(string $GAMBAR) Return Dokumentasi objects filtered by the GAMBAR column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseDokumentasiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDokumentasiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\Dokumentasi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DokumentasiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DokumentasiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DokumentasiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DokumentasiQuery) {
            return $criteria;
        }
        $query = new DokumentasiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Dokumentasi|Dokumentasi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DokumentasiPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DokumentasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Dokumentasi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByDokumentasiId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Dokumentasi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `DOKUMENTASI_ID`, `PENGGUNA_ID`, `JENIS_DOKUMENTASI_ID`, `JUDUL`, `DESKRIPSI`, `TANGGAL`, `GAMBAR` FROM `dokumentasi` WHERE `DOKUMENTASI_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Dokumentasi();
            $obj->hydrate($row);
            DokumentasiPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Dokumentasi|Dokumentasi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Dokumentasi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DokumentasiPeer::DOKUMENTASI_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DokumentasiPeer::DOKUMENTASI_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the DOKUMENTASI_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByDokumentasiId(1234); // WHERE DOKUMENTASI_ID = 1234
     * $query->filterByDokumentasiId(array(12, 34)); // WHERE DOKUMENTASI_ID IN (12, 34)
     * $query->filterByDokumentasiId(array('min' => 12)); // WHERE DOKUMENTASI_ID >= 12
     * $query->filterByDokumentasiId(array('max' => 12)); // WHERE DOKUMENTASI_ID <= 12
     * </code>
     *
     * @param     mixed $dokumentasiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByDokumentasiId($dokumentasiId = null, $comparison = null)
    {
        if (is_array($dokumentasiId)) {
            $useMinMax = false;
            if (isset($dokumentasiId['min'])) {
                $this->addUsingAlias(DokumentasiPeer::DOKUMENTASI_ID, $dokumentasiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dokumentasiId['max'])) {
                $this->addUsingAlias(DokumentasiPeer::DOKUMENTASI_ID, $dokumentasiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::DOKUMENTASI_ID, $dokumentasiId, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId(1234); // WHERE PENGGUNA_ID = 1234
     * $query->filterByPenggunaId(array(12, 34)); // WHERE PENGGUNA_ID IN (12, 34)
     * $query->filterByPenggunaId(array('min' => 12)); // WHERE PENGGUNA_ID >= 12
     * $query->filterByPenggunaId(array('max' => 12)); // WHERE PENGGUNA_ID <= 12
     * </code>
     *
     * @see       filterByPengguna()
     *
     * @param     mixed $penggunaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (is_array($penggunaId)) {
            $useMinMax = false;
            if (isset($penggunaId['min'])) {
                $this->addUsingAlias(DokumentasiPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($penggunaId['max'])) {
                $this->addUsingAlias(DokumentasiPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the JENIS_DOKUMENTASI_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisDokumentasiId(1234); // WHERE JENIS_DOKUMENTASI_ID = 1234
     * $query->filterByJenisDokumentasiId(array(12, 34)); // WHERE JENIS_DOKUMENTASI_ID IN (12, 34)
     * $query->filterByJenisDokumentasiId(array('min' => 12)); // WHERE JENIS_DOKUMENTASI_ID >= 12
     * $query->filterByJenisDokumentasiId(array('max' => 12)); // WHERE JENIS_DOKUMENTASI_ID <= 12
     * </code>
     *
     * @see       filterByJenisDokumentasi()
     *
     * @param     mixed $jenisDokumentasiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByJenisDokumentasiId($jenisDokumentasiId = null, $comparison = null)
    {
        if (is_array($jenisDokumentasiId)) {
            $useMinMax = false;
            if (isset($jenisDokumentasiId['min'])) {
                $this->addUsingAlias(DokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisDokumentasiId['max'])) {
                $this->addUsingAlias(DokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasiId, $comparison);
    }

    /**
     * Filter the query on the JUDUL column
     *
     * Example usage:
     * <code>
     * $query->filterByJudul('fooValue');   // WHERE JUDUL = 'fooValue'
     * $query->filterByJudul('%fooValue%'); // WHERE JUDUL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $judul The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByJudul($judul = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($judul)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $judul)) {
                $judul = str_replace('*', '%', $judul);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::JUDUL, $judul, $comparison);
    }

    /**
     * Filter the query on the DESKRIPSI column
     *
     * Example usage:
     * <code>
     * $query->filterByDeskripsi('fooValue');   // WHERE DESKRIPSI = 'fooValue'
     * $query->filterByDeskripsi('%fooValue%'); // WHERE DESKRIPSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deskripsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByDeskripsi($deskripsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deskripsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $deskripsi)) {
                $deskripsi = str_replace('*', '%', $deskripsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::DESKRIPSI, $deskripsi, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(DokumentasiPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(DokumentasiPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the GAMBAR column
     *
     * Example usage:
     * <code>
     * $query->filterByGambar('fooValue');   // WHERE GAMBAR = 'fooValue'
     * $query->filterByGambar('%fooValue%'); // WHERE GAMBAR LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gambar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function filterByGambar($gambar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gambar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gambar)) {
                $gambar = str_replace('*', '%', $gambar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DokumentasiPeer::GAMBAR, $gambar, $comparison);
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DokumentasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPengguna($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(DokumentasiPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DokumentasiPeer::PENGGUNA_ID, $pengguna->toKeyValue('PrimaryKey', 'PenggunaId'), $comparison);
        } else {
            throw new PropelException('filterByPengguna() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pengguna relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function joinPengguna($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pengguna');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pengguna');
        }

        return $this;
    }

    /**
     * Use the Pengguna relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPengguna($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pengguna', '\infopendataan\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related JenisDokumentasi object
     *
     * @param   JenisDokumentasi|PropelObjectCollection $jenisDokumentasi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DokumentasiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisDokumentasi($jenisDokumentasi, $comparison = null)
    {
        if ($jenisDokumentasi instanceof JenisDokumentasi) {
            return $this
                ->addUsingAlias(DokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasi->getJenisDokumentasiId(), $comparison);
        } elseif ($jenisDokumentasi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DokumentasiPeer::JENIS_DOKUMENTASI_ID, $jenisDokumentasi->toKeyValue('PrimaryKey', 'JenisDokumentasiId'), $comparison);
        } else {
            throw new PropelException('filterByJenisDokumentasi() only accepts arguments of type JenisDokumentasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisDokumentasi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function joinJenisDokumentasi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisDokumentasi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisDokumentasi');
        }

        return $this;
    }

    /**
     * Use the JenisDokumentasi relation JenisDokumentasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\JenisDokumentasiQuery A secondary query class using the current class as primary query
     */
    public function useJenisDokumentasiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisDokumentasi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisDokumentasi', '\infopendataan\Model\JenisDokumentasiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Dokumentasi $dokumentasi Object to remove from the list of results
     *
     * @return DokumentasiQuery The current query, for fluid interface
     */
    public function prune($dokumentasi = null)
    {
        if ($dokumentasi) {
            $this->addUsingAlias(DokumentasiPeer::DOKUMENTASI_ID, $dokumentasi->getDokumentasiId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
