<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\File;
use infopendataan\Model\JenisFile;
use infopendataan\Model\JenisFilePeer;
use infopendataan\Model\JenisFileQuery;

/**
 * Base class that represents a query for the 'jenis_file' table.
 *
 * 
 *
 * @method JenisFileQuery orderByJenisFileId($order = Criteria::ASC) Order by the JENIS_FILE_ID column
 * @method JenisFileQuery orderByNama($order = Criteria::ASC) Order by the NAMA column
 *
 * @method JenisFileQuery groupByJenisFileId() Group by the JENIS_FILE_ID column
 * @method JenisFileQuery groupByNama() Group by the NAMA column
 *
 * @method JenisFileQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JenisFileQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JenisFileQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JenisFileQuery leftJoinFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the File relation
 * @method JenisFileQuery rightJoinFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the File relation
 * @method JenisFileQuery innerJoinFile($relationAlias = null) Adds a INNER JOIN clause to the query using the File relation
 *
 * @method JenisFile findOne(PropelPDO $con = null) Return the first JenisFile matching the query
 * @method JenisFile findOneOrCreate(PropelPDO $con = null) Return the first JenisFile matching the query, or a new JenisFile object populated from the query conditions when no match is found
 *
 * @method JenisFile findOneByNama(string $NAMA) Return the first JenisFile filtered by the NAMA column
 *
 * @method array findByJenisFileId(int $JENIS_FILE_ID) Return JenisFile objects filtered by the JENIS_FILE_ID column
 * @method array findByNama(string $NAMA) Return JenisFile objects filtered by the NAMA column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJenisFileQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJenisFileQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\JenisFile', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JenisFileQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JenisFileQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JenisFileQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JenisFileQuery) {
            return $criteria;
        }
        $query = new JenisFileQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JenisFile|JenisFile[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JenisFilePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JenisFilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisFile A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJenisFileId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisFile A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `JENIS_FILE_ID`, `NAMA` FROM `jenis_file` WHERE `JENIS_FILE_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JenisFile();
            $obj->hydrate($row);
            JenisFilePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JenisFile|JenisFile[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JenisFile[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JenisFileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JenisFileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the JENIS_FILE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisFileId(1234); // WHERE JENIS_FILE_ID = 1234
     * $query->filterByJenisFileId(array(12, 34)); // WHERE JENIS_FILE_ID IN (12, 34)
     * $query->filterByJenisFileId(array('min' => 12)); // WHERE JENIS_FILE_ID >= 12
     * $query->filterByJenisFileId(array('max' => 12)); // WHERE JENIS_FILE_ID <= 12
     * </code>
     *
     * @param     mixed $jenisFileId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisFileQuery The current query, for fluid interface
     */
    public function filterByJenisFileId($jenisFileId = null, $comparison = null)
    {
        if (is_array($jenisFileId)) {
            $useMinMax = false;
            if (isset($jenisFileId['min'])) {
                $this->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $jenisFileId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisFileId['max'])) {
                $this->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $jenisFileId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $jenisFileId, $comparison);
    }

    /**
     * Filter the query on the NAMA column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE NAMA = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE NAMA LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisFileQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisFilePeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query by a related File object
     *
     * @param   File|PropelObjectCollection $file  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JenisFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFile($file, $comparison = null)
    {
        if ($file instanceof File) {
            return $this
                ->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $file->getJenisFileId(), $comparison);
        } elseif ($file instanceof PropelObjectCollection) {
            return $this
                ->useFileQuery()
                ->filterByPrimaryKeys($file->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFile() only accepts arguments of type File or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the File relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JenisFileQuery The current query, for fluid interface
     */
    public function joinFile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('File');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'File');
        }

        return $this;
    }

    /**
     * Use the File relation File object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \infopendataan\Model\FileQuery A secondary query class using the current class as primary query
     */
    public function useFileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'File', '\infopendataan\Model\FileQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JenisFile $jenisFile Object to remove from the list of results
     *
     * @return JenisFileQuery The current query, for fluid interface
     */
    public function prune($jenisFile = null)
    {
        if ($jenisFile) {
            $this->addUsingAlias(JenisFilePeer::JENIS_FILE_ID, $jenisFile->getJenisFileId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
