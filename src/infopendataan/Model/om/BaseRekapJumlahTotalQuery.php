<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\RekapJumlahTotal;
use infopendataan\Model\RekapJumlahTotalPeer;
use infopendataan\Model\RekapJumlahTotalQuery;

/**
 * Base class that represents a query for the 'rekap_jumlah_total' table.
 *
 * 
 *
 * @method RekapJumlahTotalQuery orderByRekapJumlahTotalId($order = Criteria::ASC) Order by the rekap_jumlah_total_id column
 * @method RekapJumlahTotalQuery orderByTanggal($order = Criteria::ASC) Order by the tanggal column
 * @method RekapJumlahTotalQuery orderBySekolahNasional($order = Criteria::ASC) Order by the sekolah_nasional column
 * @method RekapJumlahTotalQuery orderByPtkNasional($order = Criteria::ASC) Order by the ptk_nasional column
 * @method RekapJumlahTotalQuery orderByPdNasional($order = Criteria::ASC) Order by the pd_nasional column
 * @method RekapJumlahTotalQuery orderBySmaNasional($order = Criteria::ASC) Order by the sma_nasional column
 * @method RekapJumlahTotalQuery orderBySmkNasional($order = Criteria::ASC) Order by the smk_nasional column
 * @method RekapJumlahTotalQuery orderBySmlbNasional($order = Criteria::ASC) Order by the smlb_nasional column
 * @method RekapJumlahTotalQuery orderByPtkSmaNasional($order = Criteria::ASC) Order by the ptk_sma_nasional column
 * @method RekapJumlahTotalQuery orderByPtkSmkNasional($order = Criteria::ASC) Order by the ptk_smk_nasional column
 * @method RekapJumlahTotalQuery orderByPtkSmlbNasional($order = Criteria::ASC) Order by the ptk_smlb_nasional column
 * @method RekapJumlahTotalQuery orderByPdSmaNasional($order = Criteria::ASC) Order by the pd_sma_nasional column
 * @method RekapJumlahTotalQuery orderByPdSmkNasional($order = Criteria::ASC) Order by the pd_smk_nasional column
 * @method RekapJumlahTotalQuery orderByPdSmlbNasional($order = Criteria::ASC) Order by the pd_smlb_nasional column
 * @method RekapJumlahTotalQuery orderByTahunAjaranId($order = Criteria::ASC) Order by the tahun_ajaran_id column
 * @method RekapJumlahTotalQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method RekapJumlahTotalQuery orderByUpdateTerakhir($order = Criteria::ASC) Order by the update_terakhir column
 *
 * @method RekapJumlahTotalQuery groupByRekapJumlahTotalId() Group by the rekap_jumlah_total_id column
 * @method RekapJumlahTotalQuery groupByTanggal() Group by the tanggal column
 * @method RekapJumlahTotalQuery groupBySekolahNasional() Group by the sekolah_nasional column
 * @method RekapJumlahTotalQuery groupByPtkNasional() Group by the ptk_nasional column
 * @method RekapJumlahTotalQuery groupByPdNasional() Group by the pd_nasional column
 * @method RekapJumlahTotalQuery groupBySmaNasional() Group by the sma_nasional column
 * @method RekapJumlahTotalQuery groupBySmkNasional() Group by the smk_nasional column
 * @method RekapJumlahTotalQuery groupBySmlbNasional() Group by the smlb_nasional column
 * @method RekapJumlahTotalQuery groupByPtkSmaNasional() Group by the ptk_sma_nasional column
 * @method RekapJumlahTotalQuery groupByPtkSmkNasional() Group by the ptk_smk_nasional column
 * @method RekapJumlahTotalQuery groupByPtkSmlbNasional() Group by the ptk_smlb_nasional column
 * @method RekapJumlahTotalQuery groupByPdSmaNasional() Group by the pd_sma_nasional column
 * @method RekapJumlahTotalQuery groupByPdSmkNasional() Group by the pd_smk_nasional column
 * @method RekapJumlahTotalQuery groupByPdSmlbNasional() Group by the pd_smlb_nasional column
 * @method RekapJumlahTotalQuery groupByTahunAjaranId() Group by the tahun_ajaran_id column
 * @method RekapJumlahTotalQuery groupBySemesterId() Group by the semester_id column
 * @method RekapJumlahTotalQuery groupByUpdateTerakhir() Group by the update_terakhir column
 *
 * @method RekapJumlahTotalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RekapJumlahTotalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RekapJumlahTotalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RekapJumlahTotal findOne(PropelPDO $con = null) Return the first RekapJumlahTotal matching the query
 * @method RekapJumlahTotal findOneOrCreate(PropelPDO $con = null) Return the first RekapJumlahTotal matching the query, or a new RekapJumlahTotal object populated from the query conditions when no match is found
 *
 * @method RekapJumlahTotal findOneByTanggal(string $tanggal) Return the first RekapJumlahTotal filtered by the tanggal column
 * @method RekapJumlahTotal findOneBySekolahNasional(double $sekolah_nasional) Return the first RekapJumlahTotal filtered by the sekolah_nasional column
 * @method RekapJumlahTotal findOneByPtkNasional(double $ptk_nasional) Return the first RekapJumlahTotal filtered by the ptk_nasional column
 * @method RekapJumlahTotal findOneByPdNasional(double $pd_nasional) Return the first RekapJumlahTotal filtered by the pd_nasional column
 * @method RekapJumlahTotal findOneBySmaNasional(double $sma_nasional) Return the first RekapJumlahTotal filtered by the sma_nasional column
 * @method RekapJumlahTotal findOneBySmkNasional(double $smk_nasional) Return the first RekapJumlahTotal filtered by the smk_nasional column
 * @method RekapJumlahTotal findOneBySmlbNasional(double $smlb_nasional) Return the first RekapJumlahTotal filtered by the smlb_nasional column
 * @method RekapJumlahTotal findOneByPtkSmaNasional(double $ptk_sma_nasional) Return the first RekapJumlahTotal filtered by the ptk_sma_nasional column
 * @method RekapJumlahTotal findOneByPtkSmkNasional(double $ptk_smk_nasional) Return the first RekapJumlahTotal filtered by the ptk_smk_nasional column
 * @method RekapJumlahTotal findOneByPtkSmlbNasional(double $ptk_smlb_nasional) Return the first RekapJumlahTotal filtered by the ptk_smlb_nasional column
 * @method RekapJumlahTotal findOneByPdSmaNasional(double $pd_sma_nasional) Return the first RekapJumlahTotal filtered by the pd_sma_nasional column
 * @method RekapJumlahTotal findOneByPdSmkNasional(double $pd_smk_nasional) Return the first RekapJumlahTotal filtered by the pd_smk_nasional column
 * @method RekapJumlahTotal findOneByPdSmlbNasional(double $pd_smlb_nasional) Return the first RekapJumlahTotal filtered by the pd_smlb_nasional column
 * @method RekapJumlahTotal findOneByTahunAjaranId(string $tahun_ajaran_id) Return the first RekapJumlahTotal filtered by the tahun_ajaran_id column
 * @method RekapJumlahTotal findOneBySemesterId(string $semester_id) Return the first RekapJumlahTotal filtered by the semester_id column
 * @method RekapJumlahTotal findOneByUpdateTerakhir(string $update_terakhir) Return the first RekapJumlahTotal filtered by the update_terakhir column
 *
 * @method array findByRekapJumlahTotalId(int $rekap_jumlah_total_id) Return RekapJumlahTotal objects filtered by the rekap_jumlah_total_id column
 * @method array findByTanggal(string $tanggal) Return RekapJumlahTotal objects filtered by the tanggal column
 * @method array findBySekolahNasional(double $sekolah_nasional) Return RekapJumlahTotal objects filtered by the sekolah_nasional column
 * @method array findByPtkNasional(double $ptk_nasional) Return RekapJumlahTotal objects filtered by the ptk_nasional column
 * @method array findByPdNasional(double $pd_nasional) Return RekapJumlahTotal objects filtered by the pd_nasional column
 * @method array findBySmaNasional(double $sma_nasional) Return RekapJumlahTotal objects filtered by the sma_nasional column
 * @method array findBySmkNasional(double $smk_nasional) Return RekapJumlahTotal objects filtered by the smk_nasional column
 * @method array findBySmlbNasional(double $smlb_nasional) Return RekapJumlahTotal objects filtered by the smlb_nasional column
 * @method array findByPtkSmaNasional(double $ptk_sma_nasional) Return RekapJumlahTotal objects filtered by the ptk_sma_nasional column
 * @method array findByPtkSmkNasional(double $ptk_smk_nasional) Return RekapJumlahTotal objects filtered by the ptk_smk_nasional column
 * @method array findByPtkSmlbNasional(double $ptk_smlb_nasional) Return RekapJumlahTotal objects filtered by the ptk_smlb_nasional column
 * @method array findByPdSmaNasional(double $pd_sma_nasional) Return RekapJumlahTotal objects filtered by the pd_sma_nasional column
 * @method array findByPdSmkNasional(double $pd_smk_nasional) Return RekapJumlahTotal objects filtered by the pd_smk_nasional column
 * @method array findByPdSmlbNasional(double $pd_smlb_nasional) Return RekapJumlahTotal objects filtered by the pd_smlb_nasional column
 * @method array findByTahunAjaranId(string $tahun_ajaran_id) Return RekapJumlahTotal objects filtered by the tahun_ajaran_id column
 * @method array findBySemesterId(string $semester_id) Return RekapJumlahTotal objects filtered by the semester_id column
 * @method array findByUpdateTerakhir(string $update_terakhir) Return RekapJumlahTotal objects filtered by the update_terakhir column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseRekapJumlahTotalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRekapJumlahTotalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\RekapJumlahTotal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RekapJumlahTotalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RekapJumlahTotalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RekapJumlahTotalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RekapJumlahTotalQuery) {
            return $criteria;
        }
        $query = new RekapJumlahTotalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RekapJumlahTotal|RekapJumlahTotal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RekapJumlahTotalPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RekapJumlahTotalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapJumlahTotal A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRekapJumlahTotalId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapJumlahTotal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `rekap_jumlah_total_id`, `tanggal`, `sekolah_nasional`, `ptk_nasional`, `pd_nasional`, `sma_nasional`, `smk_nasional`, `smlb_nasional`, `ptk_sma_nasional`, `ptk_smk_nasional`, `ptk_smlb_nasional`, `pd_sma_nasional`, `pd_smk_nasional`, `pd_smlb_nasional`, `tahun_ajaran_id`, `semester_id`, `update_terakhir` FROM `rekap_jumlah_total` WHERE `rekap_jumlah_total_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RekapJumlahTotal();
            $obj->hydrate($row);
            RekapJumlahTotalPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RekapJumlahTotal|RekapJumlahTotal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RekapJumlahTotal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RekapJumlahTotalPeer::REKAP_JUMLAH_TOTAL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RekapJumlahTotalPeer::REKAP_JUMLAH_TOTAL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the rekap_jumlah_total_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRekapJumlahTotalId(1234); // WHERE rekap_jumlah_total_id = 1234
     * $query->filterByRekapJumlahTotalId(array(12, 34)); // WHERE rekap_jumlah_total_id IN (12, 34)
     * $query->filterByRekapJumlahTotalId(array('min' => 12)); // WHERE rekap_jumlah_total_id >= 12
     * $query->filterByRekapJumlahTotalId(array('max' => 12)); // WHERE rekap_jumlah_total_id <= 12
     * </code>
     *
     * @param     mixed $rekapJumlahTotalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByRekapJumlahTotalId($rekapJumlahTotalId = null, $comparison = null)
    {
        if (is_array($rekapJumlahTotalId)) {
            $useMinMax = false;
            if (isset($rekapJumlahTotalId['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::REKAP_JUMLAH_TOTAL_ID, $rekapJumlahTotalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rekapJumlahTotalId['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::REKAP_JUMLAH_TOTAL_ID, $rekapJumlahTotalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::REKAP_JUMLAH_TOTAL_ID, $rekapJumlahTotalId, $comparison);
    }

    /**
     * Filter the query on the tanggal column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE tanggal > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the sekolah_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahNasional(1234); // WHERE sekolah_nasional = 1234
     * $query->filterBySekolahNasional(array(12, 34)); // WHERE sekolah_nasional IN (12, 34)
     * $query->filterBySekolahNasional(array('min' => 12)); // WHERE sekolah_nasional >= 12
     * $query->filterBySekolahNasional(array('max' => 12)); // WHERE sekolah_nasional <= 12
     * </code>
     *
     * @param     mixed $sekolahNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterBySekolahNasional($sekolahNasional = null, $comparison = null)
    {
        if (is_array($sekolahNasional)) {
            $useMinMax = false;
            if (isset($sekolahNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SEKOLAH_NASIONAL, $sekolahNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sekolahNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SEKOLAH_NASIONAL, $sekolahNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::SEKOLAH_NASIONAL, $sekolahNasional, $comparison);
    }

    /**
     * Filter the query on the ptk_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkNasional(1234); // WHERE ptk_nasional = 1234
     * $query->filterByPtkNasional(array(12, 34)); // WHERE ptk_nasional IN (12, 34)
     * $query->filterByPtkNasional(array('min' => 12)); // WHERE ptk_nasional >= 12
     * $query->filterByPtkNasional(array('max' => 12)); // WHERE ptk_nasional <= 12
     * </code>
     *
     * @param     mixed $ptkNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPtkNasional($ptkNasional = null, $comparison = null)
    {
        if (is_array($ptkNasional)) {
            $useMinMax = false;
            if (isset($ptkNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_NASIONAL, $ptkNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ptkNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_NASIONAL, $ptkNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PTK_NASIONAL, $ptkNasional, $comparison);
    }

    /**
     * Filter the query on the pd_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPdNasional(1234); // WHERE pd_nasional = 1234
     * $query->filterByPdNasional(array(12, 34)); // WHERE pd_nasional IN (12, 34)
     * $query->filterByPdNasional(array('min' => 12)); // WHERE pd_nasional >= 12
     * $query->filterByPdNasional(array('max' => 12)); // WHERE pd_nasional <= 12
     * </code>
     *
     * @param     mixed $pdNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPdNasional($pdNasional = null, $comparison = null)
    {
        if (is_array($pdNasional)) {
            $useMinMax = false;
            if (isset($pdNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_NASIONAL, $pdNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_NASIONAL, $pdNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PD_NASIONAL, $pdNasional, $comparison);
    }

    /**
     * Filter the query on the sma_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterBySmaNasional(1234); // WHERE sma_nasional = 1234
     * $query->filterBySmaNasional(array(12, 34)); // WHERE sma_nasional IN (12, 34)
     * $query->filterBySmaNasional(array('min' => 12)); // WHERE sma_nasional >= 12
     * $query->filterBySmaNasional(array('max' => 12)); // WHERE sma_nasional <= 12
     * </code>
     *
     * @param     mixed $smaNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterBySmaNasional($smaNasional = null, $comparison = null)
    {
        if (is_array($smaNasional)) {
            $useMinMax = false;
            if (isset($smaNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SMA_NASIONAL, $smaNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($smaNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SMA_NASIONAL, $smaNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::SMA_NASIONAL, $smaNasional, $comparison);
    }

    /**
     * Filter the query on the smk_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterBySmkNasional(1234); // WHERE smk_nasional = 1234
     * $query->filterBySmkNasional(array(12, 34)); // WHERE smk_nasional IN (12, 34)
     * $query->filterBySmkNasional(array('min' => 12)); // WHERE smk_nasional >= 12
     * $query->filterBySmkNasional(array('max' => 12)); // WHERE smk_nasional <= 12
     * </code>
     *
     * @param     mixed $smkNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterBySmkNasional($smkNasional = null, $comparison = null)
    {
        if (is_array($smkNasional)) {
            $useMinMax = false;
            if (isset($smkNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SMK_NASIONAL, $smkNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($smkNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SMK_NASIONAL, $smkNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::SMK_NASIONAL, $smkNasional, $comparison);
    }

    /**
     * Filter the query on the smlb_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterBySmlbNasional(1234); // WHERE smlb_nasional = 1234
     * $query->filterBySmlbNasional(array(12, 34)); // WHERE smlb_nasional IN (12, 34)
     * $query->filterBySmlbNasional(array('min' => 12)); // WHERE smlb_nasional >= 12
     * $query->filterBySmlbNasional(array('max' => 12)); // WHERE smlb_nasional <= 12
     * </code>
     *
     * @param     mixed $smlbNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterBySmlbNasional($smlbNasional = null, $comparison = null)
    {
        if (is_array($smlbNasional)) {
            $useMinMax = false;
            if (isset($smlbNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SMLB_NASIONAL, $smlbNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($smlbNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::SMLB_NASIONAL, $smlbNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::SMLB_NASIONAL, $smlbNasional, $comparison);
    }

    /**
     * Filter the query on the ptk_sma_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkSmaNasional(1234); // WHERE ptk_sma_nasional = 1234
     * $query->filterByPtkSmaNasional(array(12, 34)); // WHERE ptk_sma_nasional IN (12, 34)
     * $query->filterByPtkSmaNasional(array('min' => 12)); // WHERE ptk_sma_nasional >= 12
     * $query->filterByPtkSmaNasional(array('max' => 12)); // WHERE ptk_sma_nasional <= 12
     * </code>
     *
     * @param     mixed $ptkSmaNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPtkSmaNasional($ptkSmaNasional = null, $comparison = null)
    {
        if (is_array($ptkSmaNasional)) {
            $useMinMax = false;
            if (isset($ptkSmaNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMA_NASIONAL, $ptkSmaNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ptkSmaNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMA_NASIONAL, $ptkSmaNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMA_NASIONAL, $ptkSmaNasional, $comparison);
    }

    /**
     * Filter the query on the ptk_smk_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkSmkNasional(1234); // WHERE ptk_smk_nasional = 1234
     * $query->filterByPtkSmkNasional(array(12, 34)); // WHERE ptk_smk_nasional IN (12, 34)
     * $query->filterByPtkSmkNasional(array('min' => 12)); // WHERE ptk_smk_nasional >= 12
     * $query->filterByPtkSmkNasional(array('max' => 12)); // WHERE ptk_smk_nasional <= 12
     * </code>
     *
     * @param     mixed $ptkSmkNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPtkSmkNasional($ptkSmkNasional = null, $comparison = null)
    {
        if (is_array($ptkSmkNasional)) {
            $useMinMax = false;
            if (isset($ptkSmkNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMK_NASIONAL, $ptkSmkNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ptkSmkNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMK_NASIONAL, $ptkSmkNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMK_NASIONAL, $ptkSmkNasional, $comparison);
    }

    /**
     * Filter the query on the ptk_smlb_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkSmlbNasional(1234); // WHERE ptk_smlb_nasional = 1234
     * $query->filterByPtkSmlbNasional(array(12, 34)); // WHERE ptk_smlb_nasional IN (12, 34)
     * $query->filterByPtkSmlbNasional(array('min' => 12)); // WHERE ptk_smlb_nasional >= 12
     * $query->filterByPtkSmlbNasional(array('max' => 12)); // WHERE ptk_smlb_nasional <= 12
     * </code>
     *
     * @param     mixed $ptkSmlbNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPtkSmlbNasional($ptkSmlbNasional = null, $comparison = null)
    {
        if (is_array($ptkSmlbNasional)) {
            $useMinMax = false;
            if (isset($ptkSmlbNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMLB_NASIONAL, $ptkSmlbNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ptkSmlbNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMLB_NASIONAL, $ptkSmlbNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PTK_SMLB_NASIONAL, $ptkSmlbNasional, $comparison);
    }

    /**
     * Filter the query on the pd_sma_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPdSmaNasional(1234); // WHERE pd_sma_nasional = 1234
     * $query->filterByPdSmaNasional(array(12, 34)); // WHERE pd_sma_nasional IN (12, 34)
     * $query->filterByPdSmaNasional(array('min' => 12)); // WHERE pd_sma_nasional >= 12
     * $query->filterByPdSmaNasional(array('max' => 12)); // WHERE pd_sma_nasional <= 12
     * </code>
     *
     * @param     mixed $pdSmaNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPdSmaNasional($pdSmaNasional = null, $comparison = null)
    {
        if (is_array($pdSmaNasional)) {
            $useMinMax = false;
            if (isset($pdSmaNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMA_NASIONAL, $pdSmaNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdSmaNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMA_NASIONAL, $pdSmaNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMA_NASIONAL, $pdSmaNasional, $comparison);
    }

    /**
     * Filter the query on the pd_smk_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPdSmkNasional(1234); // WHERE pd_smk_nasional = 1234
     * $query->filterByPdSmkNasional(array(12, 34)); // WHERE pd_smk_nasional IN (12, 34)
     * $query->filterByPdSmkNasional(array('min' => 12)); // WHERE pd_smk_nasional >= 12
     * $query->filterByPdSmkNasional(array('max' => 12)); // WHERE pd_smk_nasional <= 12
     * </code>
     *
     * @param     mixed $pdSmkNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPdSmkNasional($pdSmkNasional = null, $comparison = null)
    {
        if (is_array($pdSmkNasional)) {
            $useMinMax = false;
            if (isset($pdSmkNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMK_NASIONAL, $pdSmkNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdSmkNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMK_NASIONAL, $pdSmkNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMK_NASIONAL, $pdSmkNasional, $comparison);
    }

    /**
     * Filter the query on the pd_smlb_nasional column
     *
     * Example usage:
     * <code>
     * $query->filterByPdSmlbNasional(1234); // WHERE pd_smlb_nasional = 1234
     * $query->filterByPdSmlbNasional(array(12, 34)); // WHERE pd_smlb_nasional IN (12, 34)
     * $query->filterByPdSmlbNasional(array('min' => 12)); // WHERE pd_smlb_nasional >= 12
     * $query->filterByPdSmlbNasional(array('max' => 12)); // WHERE pd_smlb_nasional <= 12
     * </code>
     *
     * @param     mixed $pdSmlbNasional The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByPdSmlbNasional($pdSmlbNasional = null, $comparison = null)
    {
        if (is_array($pdSmlbNasional)) {
            $useMinMax = false;
            if (isset($pdSmlbNasional['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMLB_NASIONAL, $pdSmlbNasional['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdSmlbNasional['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMLB_NASIONAL, $pdSmlbNasional['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::PD_SMLB_NASIONAL, $pdSmlbNasional, $comparison);
    }

    /**
     * Filter the query on the tahun_ajaran_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTahunAjaranId('fooValue');   // WHERE tahun_ajaran_id = 'fooValue'
     * $query->filterByTahunAjaranId('%fooValue%'); // WHERE tahun_ajaran_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tahunAjaranId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByTahunAjaranId($tahunAjaranId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tahunAjaranId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tahunAjaranId)) {
                $tahunAjaranId = str_replace('*', '%', $tahunAjaranId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::TAHUN_AJARAN_ID, $tahunAjaranId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the update_terakhir column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdateTerakhir('2011-03-14'); // WHERE update_terakhir = '2011-03-14'
     * $query->filterByUpdateTerakhir('now'); // WHERE update_terakhir = '2011-03-14'
     * $query->filterByUpdateTerakhir(array('max' => 'yesterday')); // WHERE update_terakhir > '2011-03-13'
     * </code>
     *
     * @param     mixed $updateTerakhir The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function filterByUpdateTerakhir($updateTerakhir = null, $comparison = null)
    {
        if (is_array($updateTerakhir)) {
            $useMinMax = false;
            if (isset($updateTerakhir['min'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::UPDATE_TERAKHIR, $updateTerakhir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updateTerakhir['max'])) {
                $this->addUsingAlias(RekapJumlahTotalPeer::UPDATE_TERAKHIR, $updateTerakhir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapJumlahTotalPeer::UPDATE_TERAKHIR, $updateTerakhir, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   RekapJumlahTotal $rekapJumlahTotal Object to remove from the list of results
     *
     * @return RekapJumlahTotalQuery The current query, for fluid interface
     */
    public function prune($rekapJumlahTotal = null)
    {
        if ($rekapJumlahTotal) {
            $this->addUsingAlias(RekapJumlahTotalPeer::REKAP_JUMLAH_TOTAL_ID, $rekapJumlahTotal->getRekapJumlahTotalId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
