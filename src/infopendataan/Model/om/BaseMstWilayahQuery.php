<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\MstWilayah;
use infopendataan\Model\MstWilayahPeer;
use infopendataan\Model\MstWilayahQuery;

/**
 * Base class that represents a query for the 'mst_wilayah' table.
 *
 * 
 *
 * @method MstWilayahQuery orderByWilayahId($order = Criteria::ASC) Order by the wilayah_id column
 * @method MstWilayahQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method MstWilayahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method MstWilayahQuery orderByIdLevelWilayah($order = Criteria::ASC) Order by the id_level_wilayah column
 * @method MstWilayahQuery orderByMstKodeWilayah($order = Criteria::ASC) Order by the mst_kode_wilayah column
 * @method MstWilayahQuery orderByNegaraId($order = Criteria::ASC) Order by the negara_id column
 * @method MstWilayahQuery orderByAsalWilayah($order = Criteria::ASC) Order by the asal_wilayah column
 * @method MstWilayahQuery orderByKodeBps($order = Criteria::ASC) Order by the kode_bps column
 * @method MstWilayahQuery orderByKodeDagri($order = Criteria::ASC) Order by the kode_dagri column
 * @method MstWilayahQuery orderByKodeKeu($order = Criteria::ASC) Order by the kode_keu column
 * @method MstWilayahQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method MstWilayahQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method MstWilayahQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method MstWilayahQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 *
 * @method MstWilayahQuery groupByWilayahId() Group by the wilayah_id column
 * @method MstWilayahQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method MstWilayahQuery groupByNama() Group by the nama column
 * @method MstWilayahQuery groupByIdLevelWilayah() Group by the id_level_wilayah column
 * @method MstWilayahQuery groupByMstKodeWilayah() Group by the mst_kode_wilayah column
 * @method MstWilayahQuery groupByNegaraId() Group by the negara_id column
 * @method MstWilayahQuery groupByAsalWilayah() Group by the asal_wilayah column
 * @method MstWilayahQuery groupByKodeBps() Group by the kode_bps column
 * @method MstWilayahQuery groupByKodeDagri() Group by the kode_dagri column
 * @method MstWilayahQuery groupByKodeKeu() Group by the kode_keu column
 * @method MstWilayahQuery groupByCreateDate() Group by the create_date column
 * @method MstWilayahQuery groupByLastUpdate() Group by the last_update column
 * @method MstWilayahQuery groupByExpiredDate() Group by the expired_date column
 * @method MstWilayahQuery groupByLastSync() Group by the last_sync column
 *
 * @method MstWilayahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method MstWilayahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method MstWilayahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method MstWilayah findOne(PropelPDO $con = null) Return the first MstWilayah matching the query
 * @method MstWilayah findOneOrCreate(PropelPDO $con = null) Return the first MstWilayah matching the query, or a new MstWilayah object populated from the query conditions when no match is found
 *
 * @method MstWilayah findOneByKodeWilayah(string $kode_wilayah) Return the first MstWilayah filtered by the kode_wilayah column
 * @method MstWilayah findOneByNama(string $nama) Return the first MstWilayah filtered by the nama column
 * @method MstWilayah findOneByIdLevelWilayah(string $id_level_wilayah) Return the first MstWilayah filtered by the id_level_wilayah column
 * @method MstWilayah findOneByMstKodeWilayah(string $mst_kode_wilayah) Return the first MstWilayah filtered by the mst_kode_wilayah column
 * @method MstWilayah findOneByNegaraId(string $negara_id) Return the first MstWilayah filtered by the negara_id column
 * @method MstWilayah findOneByAsalWilayah(string $asal_wilayah) Return the first MstWilayah filtered by the asal_wilayah column
 * @method MstWilayah findOneByKodeBps(string $kode_bps) Return the first MstWilayah filtered by the kode_bps column
 * @method MstWilayah findOneByKodeDagri(string $kode_dagri) Return the first MstWilayah filtered by the kode_dagri column
 * @method MstWilayah findOneByKodeKeu(string $kode_keu) Return the first MstWilayah filtered by the kode_keu column
 * @method MstWilayah findOneByCreateDate(string $create_date) Return the first MstWilayah filtered by the create_date column
 * @method MstWilayah findOneByLastUpdate(string $last_update) Return the first MstWilayah filtered by the last_update column
 * @method MstWilayah findOneByExpiredDate(string $expired_date) Return the first MstWilayah filtered by the expired_date column
 * @method MstWilayah findOneByLastSync(string $last_sync) Return the first MstWilayah filtered by the last_sync column
 *
 * @method array findByWilayahId(string $wilayah_id) Return MstWilayah objects filtered by the wilayah_id column
 * @method array findByKodeWilayah(string $kode_wilayah) Return MstWilayah objects filtered by the kode_wilayah column
 * @method array findByNama(string $nama) Return MstWilayah objects filtered by the nama column
 * @method array findByIdLevelWilayah(string $id_level_wilayah) Return MstWilayah objects filtered by the id_level_wilayah column
 * @method array findByMstKodeWilayah(string $mst_kode_wilayah) Return MstWilayah objects filtered by the mst_kode_wilayah column
 * @method array findByNegaraId(string $negara_id) Return MstWilayah objects filtered by the negara_id column
 * @method array findByAsalWilayah(string $asal_wilayah) Return MstWilayah objects filtered by the asal_wilayah column
 * @method array findByKodeBps(string $kode_bps) Return MstWilayah objects filtered by the kode_bps column
 * @method array findByKodeDagri(string $kode_dagri) Return MstWilayah objects filtered by the kode_dagri column
 * @method array findByKodeKeu(string $kode_keu) Return MstWilayah objects filtered by the kode_keu column
 * @method array findByCreateDate(string $create_date) Return MstWilayah objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return MstWilayah objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return MstWilayah objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return MstWilayah objects filtered by the last_sync column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseMstWilayahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseMstWilayahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\MstWilayah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MstWilayahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   MstWilayahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MstWilayahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MstWilayahQuery) {
            return $criteria;
        }
        $query = new MstWilayahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   MstWilayah|MstWilayah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MstWilayahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 MstWilayah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByWilayahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 MstWilayah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `wilayah_id`, `kode_wilayah`, `nama`, `id_level_wilayah`, `mst_kode_wilayah`, `negara_id`, `asal_wilayah`, `kode_bps`, `kode_dagri`, `kode_keu`, `create_date`, `last_update`, `expired_date`, `last_sync` FROM `mst_wilayah` WHERE `wilayah_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new MstWilayah();
            $obj->hydrate($row);
            MstWilayahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return MstWilayah|MstWilayah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|MstWilayah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MstWilayahPeer::WILAYAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MstWilayahPeer::WILAYAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the wilayah_id column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayahId('fooValue');   // WHERE wilayah_id = 'fooValue'
     * $query->filterByWilayahId('%fooValue%'); // WHERE wilayah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByWilayahId($wilayahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayahId)) {
                $wilayahId = str_replace('*', '%', $wilayahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::WILAYAH_ID, $wilayahId, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayah('fooValue');   // WHERE id_level_wilayah = 'fooValue'
     * $query->filterByIdLevelWilayah('%fooValue%'); // WHERE id_level_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idLevelWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayah($idLevelWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idLevelWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idLevelWilayah)) {
                $idLevelWilayah = str_replace('*', '%', $idLevelWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::ID_LEVEL_WILAYAH, $idLevelWilayah, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayah('fooValue');   // WHERE mst_kode_wilayah = 'fooValue'
     * $query->filterByMstKodeWilayah('%fooValue%'); // WHERE mst_kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayah($mstKodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayah)) {
                $mstKodeWilayah = str_replace('*', '%', $mstKodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::MST_KODE_WILAYAH, $mstKodeWilayah, $comparison);
    }

    /**
     * Filter the query on the negara_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNegaraId('fooValue');   // WHERE negara_id = 'fooValue'
     * $query->filterByNegaraId('%fooValue%'); // WHERE negara_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $negaraId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByNegaraId($negaraId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($negaraId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $negaraId)) {
                $negaraId = str_replace('*', '%', $negaraId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::NEGARA_ID, $negaraId, $comparison);
    }

    /**
     * Filter the query on the asal_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByAsalWilayah('fooValue');   // WHERE asal_wilayah = 'fooValue'
     * $query->filterByAsalWilayah('%fooValue%'); // WHERE asal_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $asalWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByAsalWilayah($asalWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($asalWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $asalWilayah)) {
                $asalWilayah = str_replace('*', '%', $asalWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::ASAL_WILAYAH, $asalWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_bps column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeBps('fooValue');   // WHERE kode_bps = 'fooValue'
     * $query->filterByKodeBps('%fooValue%'); // WHERE kode_bps LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeBps The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeBps($kodeBps = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeBps)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeBps)) {
                $kodeBps = str_replace('*', '%', $kodeBps);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_BPS, $kodeBps, $comparison);
    }

    /**
     * Filter the query on the kode_dagri column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeDagri('fooValue');   // WHERE kode_dagri = 'fooValue'
     * $query->filterByKodeDagri('%fooValue%'); // WHERE kode_dagri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeDagri The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeDagri($kodeDagri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeDagri)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeDagri)) {
                $kodeDagri = str_replace('*', '%', $kodeDagri);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_DAGRI, $kodeDagri, $comparison);
    }

    /**
     * Filter the query on the kode_keu column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeKeu('fooValue');   // WHERE kode_keu = 'fooValue'
     * $query->filterByKodeKeu('%fooValue%'); // WHERE kode_keu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeKeu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByKodeKeu($kodeKeu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeKeu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeKeu)) {
                $kodeKeu = str_replace('*', '%', $kodeKeu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::KODE_KEU, $kodeKeu, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('fooValue');   // WHERE create_date = 'fooValue'
     * $query->filterByCreateDate('%fooValue%'); // WHERE create_date LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createDate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createDate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createDate)) {
                $createDate = str_replace('*', '%', $createDate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('fooValue');   // WHERE last_update = 'fooValue'
     * $query->filterByLastUpdate('%fooValue%'); // WHERE last_update LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastUpdate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastUpdate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastUpdate)) {
                $lastUpdate = str_replace('*', '%', $lastUpdate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('fooValue');   // WHERE expired_date = 'fooValue'
     * $query->filterByExpiredDate('%fooValue%'); // WHERE expired_date LIKE '%fooValue%'
     * </code>
     *
     * @param     string $expiredDate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($expiredDate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $expiredDate)) {
                $expiredDate = str_replace('*', '%', $expiredDate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('fooValue');   // WHERE last_sync = 'fooValue'
     * $query->filterByLastSync('%fooValue%'); // WHERE last_sync LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastSync The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastSync)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastSync)) {
                $lastSync = str_replace('*', '%', $lastSync);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MstWilayahPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   MstWilayah $mstWilayah Object to remove from the list of results
     *
     * @return MstWilayahQuery The current query, for fluid interface
     */
    public function prune($mstWilayah = null)
    {
        if ($mstWilayah) {
            $this->addUsingAlias(MstWilayahPeer::WILAYAH_ID, $mstWilayah->getWilayahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
