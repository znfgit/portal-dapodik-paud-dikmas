<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\RekapSekolah;
use infopendataan\Model\RekapSekolahPeer;
use infopendataan\Model\RekapSekolahQuery;

/**
 * Base class that represents a query for the 'rekap_sekolah' table.
 *
 * 
 *
 * @method RekapSekolahQuery orderByRekapSekolahId($order = Criteria::ASC) Order by the rekap_sekolah_id column
 * @method RekapSekolahQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method RekapSekolahQuery orderByTanggal($order = Criteria::ASC) Order by the tanggal column
 * @method RekapSekolahQuery orderByPtk($order = Criteria::ASC) Order by the ptk column
 * @method RekapSekolahQuery orderByPegawai($order = Criteria::ASC) Order by the pegawai column
 * @method RekapSekolahQuery orderByPd($order = Criteria::ASC) Order by the pd column
 * @method RekapSekolahQuery orderByRombel($order = Criteria::ASC) Order by the rombel column
 * @method RekapSekolahQuery orderByKodeWilayahKecamatan($order = Criteria::ASC) Order by the kode_wilayah_kecamatan column
 * @method RekapSekolahQuery orderByKecamatan($order = Criteria::ASC) Order by the kecamatan column
 * @method RekapSekolahQuery orderByMstKodeWilayahKecamatan($order = Criteria::ASC) Order by the mst_kode_wilayah_kecamatan column
 * @method RekapSekolahQuery orderByIdLevelWilayahKecamatan($order = Criteria::ASC) Order by the id_level_wilayah_kecamatan column
 * @method RekapSekolahQuery orderByKodeWilayahKabupaten($order = Criteria::ASC) Order by the kode_wilayah_kabupaten column
 * @method RekapSekolahQuery orderByKabupaten($order = Criteria::ASC) Order by the kabupaten column
 * @method RekapSekolahQuery orderByMstKodeWilayahKabupaten($order = Criteria::ASC) Order by the mst_kode_wilayah_kabupaten column
 * @method RekapSekolahQuery orderByIdLevelWilayahKabupaten($order = Criteria::ASC) Order by the id_level_wilayah_kabupaten column
 * @method RekapSekolahQuery orderByKodeWilayahProvinsi($order = Criteria::ASC) Order by the kode_wilayah_provinsi column
 * @method RekapSekolahQuery orderByProvinsi($order = Criteria::ASC) Order by the provinsi column
 * @method RekapSekolahQuery orderByMstKodeWilayahProvinsi($order = Criteria::ASC) Order by the mst_kode_wilayah_provinsi column
 * @method RekapSekolahQuery orderByIdLevelWilayahProvinsi($order = Criteria::ASC) Order by the id_level_wilayah_provinsi column
 * @method RekapSekolahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method RekapSekolahQuery orderByNpsn($order = Criteria::ASC) Order by the npsn column
 * @method RekapSekolahQuery orderByBentukPendidikanId($order = Criteria::ASC) Order by the bentuk_pendidikan_id column
 * @method RekapSekolahQuery orderByStatusSekolah($order = Criteria::ASC) Order by the status_sekolah column
 * @method RekapSekolahQuery orderByGuruMatematika($order = Criteria::ASC) Order by the guru_matematika column
 * @method RekapSekolahQuery orderByGuruBahasaIndonesia($order = Criteria::ASC) Order by the guru_bahasa_indonesia column
 * @method RekapSekolahQuery orderByGuruBahasaInggris($order = Criteria::ASC) Order by the guru_bahasa_inggris column
 * @method RekapSekolahQuery orderByGuruSejarahIndonesia($order = Criteria::ASC) Order by the guru_sejarah_indonesia column
 * @method RekapSekolahQuery orderByGuruPkn($order = Criteria::ASC) Order by the guru_pkn column
 * @method RekapSekolahQuery orderByGuruPenjaskes($order = Criteria::ASC) Order by the guru_penjaskes column
 * @method RekapSekolahQuery orderByGuruAgamaBudiPekerti($order = Criteria::ASC) Order by the guru_agama_budi_pekerti column
 * @method RekapSekolahQuery orderByGuruSeniBudaya($order = Criteria::ASC) Order by the guru_seni_budaya column
 * @method RekapSekolahQuery orderByPdKelas10($order = Criteria::ASC) Order by the pd_kelas_10 column
 * @method RekapSekolahQuery orderByPdKelas11($order = Criteria::ASC) Order by the pd_kelas_11 column
 * @method RekapSekolahQuery orderByPdKelas12($order = Criteria::ASC) Order by the pd_kelas_12 column
 * @method RekapSekolahQuery orderByPdKelas13($order = Criteria::ASC) Order by the pd_kelas_13 column
 *
 * @method RekapSekolahQuery groupByRekapSekolahId() Group by the rekap_sekolah_id column
 * @method RekapSekolahQuery groupBySekolahId() Group by the sekolah_id column
 * @method RekapSekolahQuery groupByTanggal() Group by the tanggal column
 * @method RekapSekolahQuery groupByPtk() Group by the ptk column
 * @method RekapSekolahQuery groupByPegawai() Group by the pegawai column
 * @method RekapSekolahQuery groupByPd() Group by the pd column
 * @method RekapSekolahQuery groupByRombel() Group by the rombel column
 * @method RekapSekolahQuery groupByKodeWilayahKecamatan() Group by the kode_wilayah_kecamatan column
 * @method RekapSekolahQuery groupByKecamatan() Group by the kecamatan column
 * @method RekapSekolahQuery groupByMstKodeWilayahKecamatan() Group by the mst_kode_wilayah_kecamatan column
 * @method RekapSekolahQuery groupByIdLevelWilayahKecamatan() Group by the id_level_wilayah_kecamatan column
 * @method RekapSekolahQuery groupByKodeWilayahKabupaten() Group by the kode_wilayah_kabupaten column
 * @method RekapSekolahQuery groupByKabupaten() Group by the kabupaten column
 * @method RekapSekolahQuery groupByMstKodeWilayahKabupaten() Group by the mst_kode_wilayah_kabupaten column
 * @method RekapSekolahQuery groupByIdLevelWilayahKabupaten() Group by the id_level_wilayah_kabupaten column
 * @method RekapSekolahQuery groupByKodeWilayahProvinsi() Group by the kode_wilayah_provinsi column
 * @method RekapSekolahQuery groupByProvinsi() Group by the provinsi column
 * @method RekapSekolahQuery groupByMstKodeWilayahProvinsi() Group by the mst_kode_wilayah_provinsi column
 * @method RekapSekolahQuery groupByIdLevelWilayahProvinsi() Group by the id_level_wilayah_provinsi column
 * @method RekapSekolahQuery groupByNama() Group by the nama column
 * @method RekapSekolahQuery groupByNpsn() Group by the npsn column
 * @method RekapSekolahQuery groupByBentukPendidikanId() Group by the bentuk_pendidikan_id column
 * @method RekapSekolahQuery groupByStatusSekolah() Group by the status_sekolah column
 * @method RekapSekolahQuery groupByGuruMatematika() Group by the guru_matematika column
 * @method RekapSekolahQuery groupByGuruBahasaIndonesia() Group by the guru_bahasa_indonesia column
 * @method RekapSekolahQuery groupByGuruBahasaInggris() Group by the guru_bahasa_inggris column
 * @method RekapSekolahQuery groupByGuruSejarahIndonesia() Group by the guru_sejarah_indonesia column
 * @method RekapSekolahQuery groupByGuruPkn() Group by the guru_pkn column
 * @method RekapSekolahQuery groupByGuruPenjaskes() Group by the guru_penjaskes column
 * @method RekapSekolahQuery groupByGuruAgamaBudiPekerti() Group by the guru_agama_budi_pekerti column
 * @method RekapSekolahQuery groupByGuruSeniBudaya() Group by the guru_seni_budaya column
 * @method RekapSekolahQuery groupByPdKelas10() Group by the pd_kelas_10 column
 * @method RekapSekolahQuery groupByPdKelas11() Group by the pd_kelas_11 column
 * @method RekapSekolahQuery groupByPdKelas12() Group by the pd_kelas_12 column
 * @method RekapSekolahQuery groupByPdKelas13() Group by the pd_kelas_13 column
 *
 * @method RekapSekolahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RekapSekolahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RekapSekolahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RekapSekolah findOne(PropelPDO $con = null) Return the first RekapSekolah matching the query
 * @method RekapSekolah findOneOrCreate(PropelPDO $con = null) Return the first RekapSekolah matching the query, or a new RekapSekolah object populated from the query conditions when no match is found
 *
 * @method RekapSekolah findOneBySekolahId(string $sekolah_id) Return the first RekapSekolah filtered by the sekolah_id column
 * @method RekapSekolah findOneByTanggal(string $tanggal) Return the first RekapSekolah filtered by the tanggal column
 * @method RekapSekolah findOneByPtk(int $ptk) Return the first RekapSekolah filtered by the ptk column
 * @method RekapSekolah findOneByPegawai(int $pegawai) Return the first RekapSekolah filtered by the pegawai column
 * @method RekapSekolah findOneByPd(int $pd) Return the first RekapSekolah filtered by the pd column
 * @method RekapSekolah findOneByRombel(int $rombel) Return the first RekapSekolah filtered by the rombel column
 * @method RekapSekolah findOneByKodeWilayahKecamatan(string $kode_wilayah_kecamatan) Return the first RekapSekolah filtered by the kode_wilayah_kecamatan column
 * @method RekapSekolah findOneByKecamatan(string $kecamatan) Return the first RekapSekolah filtered by the kecamatan column
 * @method RekapSekolah findOneByMstKodeWilayahKecamatan(string $mst_kode_wilayah_kecamatan) Return the first RekapSekolah filtered by the mst_kode_wilayah_kecamatan column
 * @method RekapSekolah findOneByIdLevelWilayahKecamatan(int $id_level_wilayah_kecamatan) Return the first RekapSekolah filtered by the id_level_wilayah_kecamatan column
 * @method RekapSekolah findOneByKodeWilayahKabupaten(string $kode_wilayah_kabupaten) Return the first RekapSekolah filtered by the kode_wilayah_kabupaten column
 * @method RekapSekolah findOneByKabupaten(string $kabupaten) Return the first RekapSekolah filtered by the kabupaten column
 * @method RekapSekolah findOneByMstKodeWilayahKabupaten(string $mst_kode_wilayah_kabupaten) Return the first RekapSekolah filtered by the mst_kode_wilayah_kabupaten column
 * @method RekapSekolah findOneByIdLevelWilayahKabupaten(int $id_level_wilayah_kabupaten) Return the first RekapSekolah filtered by the id_level_wilayah_kabupaten column
 * @method RekapSekolah findOneByKodeWilayahProvinsi(string $kode_wilayah_provinsi) Return the first RekapSekolah filtered by the kode_wilayah_provinsi column
 * @method RekapSekolah findOneByProvinsi(string $provinsi) Return the first RekapSekolah filtered by the provinsi column
 * @method RekapSekolah findOneByMstKodeWilayahProvinsi(string $mst_kode_wilayah_provinsi) Return the first RekapSekolah filtered by the mst_kode_wilayah_provinsi column
 * @method RekapSekolah findOneByIdLevelWilayahProvinsi(int $id_level_wilayah_provinsi) Return the first RekapSekolah filtered by the id_level_wilayah_provinsi column
 * @method RekapSekolah findOneByNama(string $nama) Return the first RekapSekolah filtered by the nama column
 * @method RekapSekolah findOneByNpsn(string $npsn) Return the first RekapSekolah filtered by the npsn column
 * @method RekapSekolah findOneByBentukPendidikanId(string $bentuk_pendidikan_id) Return the first RekapSekolah filtered by the bentuk_pendidikan_id column
 * @method RekapSekolah findOneByStatusSekolah(string $status_sekolah) Return the first RekapSekolah filtered by the status_sekolah column
 * @method RekapSekolah findOneByGuruMatematika(int $guru_matematika) Return the first RekapSekolah filtered by the guru_matematika column
 * @method RekapSekolah findOneByGuruBahasaIndonesia(int $guru_bahasa_indonesia) Return the first RekapSekolah filtered by the guru_bahasa_indonesia column
 * @method RekapSekolah findOneByGuruBahasaInggris(int $guru_bahasa_inggris) Return the first RekapSekolah filtered by the guru_bahasa_inggris column
 * @method RekapSekolah findOneByGuruSejarahIndonesia(int $guru_sejarah_indonesia) Return the first RekapSekolah filtered by the guru_sejarah_indonesia column
 * @method RekapSekolah findOneByGuruPkn(int $guru_pkn) Return the first RekapSekolah filtered by the guru_pkn column
 * @method RekapSekolah findOneByGuruPenjaskes(int $guru_penjaskes) Return the first RekapSekolah filtered by the guru_penjaskes column
 * @method RekapSekolah findOneByGuruAgamaBudiPekerti(int $guru_agama_budi_pekerti) Return the first RekapSekolah filtered by the guru_agama_budi_pekerti column
 * @method RekapSekolah findOneByGuruSeniBudaya(int $guru_seni_budaya) Return the first RekapSekolah filtered by the guru_seni_budaya column
 * @method RekapSekolah findOneByPdKelas10(int $pd_kelas_10) Return the first RekapSekolah filtered by the pd_kelas_10 column
 * @method RekapSekolah findOneByPdKelas11(int $pd_kelas_11) Return the first RekapSekolah filtered by the pd_kelas_11 column
 * @method RekapSekolah findOneByPdKelas12(int $pd_kelas_12) Return the first RekapSekolah filtered by the pd_kelas_12 column
 * @method RekapSekolah findOneByPdKelas13(int $pd_kelas_13) Return the first RekapSekolah filtered by the pd_kelas_13 column
 *
 * @method array findByRekapSekolahId(int $rekap_sekolah_id) Return RekapSekolah objects filtered by the rekap_sekolah_id column
 * @method array findBySekolahId(string $sekolah_id) Return RekapSekolah objects filtered by the sekolah_id column
 * @method array findByTanggal(string $tanggal) Return RekapSekolah objects filtered by the tanggal column
 * @method array findByPtk(int $ptk) Return RekapSekolah objects filtered by the ptk column
 * @method array findByPegawai(int $pegawai) Return RekapSekolah objects filtered by the pegawai column
 * @method array findByPd(int $pd) Return RekapSekolah objects filtered by the pd column
 * @method array findByRombel(int $rombel) Return RekapSekolah objects filtered by the rombel column
 * @method array findByKodeWilayahKecamatan(string $kode_wilayah_kecamatan) Return RekapSekolah objects filtered by the kode_wilayah_kecamatan column
 * @method array findByKecamatan(string $kecamatan) Return RekapSekolah objects filtered by the kecamatan column
 * @method array findByMstKodeWilayahKecamatan(string $mst_kode_wilayah_kecamatan) Return RekapSekolah objects filtered by the mst_kode_wilayah_kecamatan column
 * @method array findByIdLevelWilayahKecamatan(int $id_level_wilayah_kecamatan) Return RekapSekolah objects filtered by the id_level_wilayah_kecamatan column
 * @method array findByKodeWilayahKabupaten(string $kode_wilayah_kabupaten) Return RekapSekolah objects filtered by the kode_wilayah_kabupaten column
 * @method array findByKabupaten(string $kabupaten) Return RekapSekolah objects filtered by the kabupaten column
 * @method array findByMstKodeWilayahKabupaten(string $mst_kode_wilayah_kabupaten) Return RekapSekolah objects filtered by the mst_kode_wilayah_kabupaten column
 * @method array findByIdLevelWilayahKabupaten(int $id_level_wilayah_kabupaten) Return RekapSekolah objects filtered by the id_level_wilayah_kabupaten column
 * @method array findByKodeWilayahProvinsi(string $kode_wilayah_provinsi) Return RekapSekolah objects filtered by the kode_wilayah_provinsi column
 * @method array findByProvinsi(string $provinsi) Return RekapSekolah objects filtered by the provinsi column
 * @method array findByMstKodeWilayahProvinsi(string $mst_kode_wilayah_provinsi) Return RekapSekolah objects filtered by the mst_kode_wilayah_provinsi column
 * @method array findByIdLevelWilayahProvinsi(int $id_level_wilayah_provinsi) Return RekapSekolah objects filtered by the id_level_wilayah_provinsi column
 * @method array findByNama(string $nama) Return RekapSekolah objects filtered by the nama column
 * @method array findByNpsn(string $npsn) Return RekapSekolah objects filtered by the npsn column
 * @method array findByBentukPendidikanId(string $bentuk_pendidikan_id) Return RekapSekolah objects filtered by the bentuk_pendidikan_id column
 * @method array findByStatusSekolah(string $status_sekolah) Return RekapSekolah objects filtered by the status_sekolah column
 * @method array findByGuruMatematika(int $guru_matematika) Return RekapSekolah objects filtered by the guru_matematika column
 * @method array findByGuruBahasaIndonesia(int $guru_bahasa_indonesia) Return RekapSekolah objects filtered by the guru_bahasa_indonesia column
 * @method array findByGuruBahasaInggris(int $guru_bahasa_inggris) Return RekapSekolah objects filtered by the guru_bahasa_inggris column
 * @method array findByGuruSejarahIndonesia(int $guru_sejarah_indonesia) Return RekapSekolah objects filtered by the guru_sejarah_indonesia column
 * @method array findByGuruPkn(int $guru_pkn) Return RekapSekolah objects filtered by the guru_pkn column
 * @method array findByGuruPenjaskes(int $guru_penjaskes) Return RekapSekolah objects filtered by the guru_penjaskes column
 * @method array findByGuruAgamaBudiPekerti(int $guru_agama_budi_pekerti) Return RekapSekolah objects filtered by the guru_agama_budi_pekerti column
 * @method array findByGuruSeniBudaya(int $guru_seni_budaya) Return RekapSekolah objects filtered by the guru_seni_budaya column
 * @method array findByPdKelas10(int $pd_kelas_10) Return RekapSekolah objects filtered by the pd_kelas_10 column
 * @method array findByPdKelas11(int $pd_kelas_11) Return RekapSekolah objects filtered by the pd_kelas_11 column
 * @method array findByPdKelas12(int $pd_kelas_12) Return RekapSekolah objects filtered by the pd_kelas_12 column
 * @method array findByPdKelas13(int $pd_kelas_13) Return RekapSekolah objects filtered by the pd_kelas_13 column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseRekapSekolahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRekapSekolahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\RekapSekolah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RekapSekolahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RekapSekolahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RekapSekolahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RekapSekolahQuery) {
            return $criteria;
        }
        $query = new RekapSekolahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RekapSekolah|RekapSekolah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RekapSekolahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRekapSekolahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `rekap_sekolah_id`, `sekolah_id`, `tanggal`, `ptk`, `pegawai`, `pd`, `rombel`, `kode_wilayah_kecamatan`, `kecamatan`, `mst_kode_wilayah_kecamatan`, `id_level_wilayah_kecamatan`, `kode_wilayah_kabupaten`, `kabupaten`, `mst_kode_wilayah_kabupaten`, `id_level_wilayah_kabupaten`, `kode_wilayah_provinsi`, `provinsi`, `mst_kode_wilayah_provinsi`, `id_level_wilayah_provinsi`, `nama`, `npsn`, `bentuk_pendidikan_id`, `status_sekolah`, `guru_matematika`, `guru_bahasa_indonesia`, `guru_bahasa_inggris`, `guru_sejarah_indonesia`, `guru_pkn`, `guru_penjaskes`, `guru_agama_budi_pekerti`, `guru_seni_budaya`, `pd_kelas_10`, `pd_kelas_11`, `pd_kelas_12`, `pd_kelas_13` FROM `rekap_sekolah` WHERE `rekap_sekolah_id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RekapSekolah();
            $obj->hydrate($row);
            RekapSekolahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RekapSekolah|RekapSekolah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RekapSekolah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RekapSekolahPeer::REKAP_SEKOLAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RekapSekolahPeer::REKAP_SEKOLAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the rekap_sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRekapSekolahId(1234); // WHERE rekap_sekolah_id = 1234
     * $query->filterByRekapSekolahId(array(12, 34)); // WHERE rekap_sekolah_id IN (12, 34)
     * $query->filterByRekapSekolahId(array('min' => 12)); // WHERE rekap_sekolah_id >= 12
     * $query->filterByRekapSekolahId(array('max' => 12)); // WHERE rekap_sekolah_id <= 12
     * </code>
     *
     * @param     mixed $rekapSekolahId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByRekapSekolahId($rekapSekolahId = null, $comparison = null)
    {
        if (is_array($rekapSekolahId)) {
            $useMinMax = false;
            if (isset($rekapSekolahId['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::REKAP_SEKOLAH_ID, $rekapSekolahId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rekapSekolahId['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::REKAP_SEKOLAH_ID, $rekapSekolahId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::REKAP_SEKOLAH_ID, $rekapSekolahId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the tanggal column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE tanggal > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the ptk column
     *
     * Example usage:
     * <code>
     * $query->filterByPtk(1234); // WHERE ptk = 1234
     * $query->filterByPtk(array(12, 34)); // WHERE ptk IN (12, 34)
     * $query->filterByPtk(array('min' => 12)); // WHERE ptk >= 12
     * $query->filterByPtk(array('max' => 12)); // WHERE ptk <= 12
     * </code>
     *
     * @param     mixed $ptk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPtk($ptk = null, $comparison = null)
    {
        if (is_array($ptk)) {
            $useMinMax = false;
            if (isset($ptk['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PTK, $ptk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ptk['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PTK, $ptk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PTK, $ptk, $comparison);
    }

    /**
     * Filter the query on the pegawai column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawai(1234); // WHERE pegawai = 1234
     * $query->filterByPegawai(array(12, 34)); // WHERE pegawai IN (12, 34)
     * $query->filterByPegawai(array('min' => 12)); // WHERE pegawai >= 12
     * $query->filterByPegawai(array('max' => 12)); // WHERE pegawai <= 12
     * </code>
     *
     * @param     mixed $pegawai The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPegawai($pegawai = null, $comparison = null)
    {
        if (is_array($pegawai)) {
            $useMinMax = false;
            if (isset($pegawai['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PEGAWAI, $pegawai['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pegawai['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PEGAWAI, $pegawai['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PEGAWAI, $pegawai, $comparison);
    }

    /**
     * Filter the query on the pd column
     *
     * Example usage:
     * <code>
     * $query->filterByPd(1234); // WHERE pd = 1234
     * $query->filterByPd(array(12, 34)); // WHERE pd IN (12, 34)
     * $query->filterByPd(array('min' => 12)); // WHERE pd >= 12
     * $query->filterByPd(array('max' => 12)); // WHERE pd <= 12
     * </code>
     *
     * @param     mixed $pd The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPd($pd = null, $comparison = null)
    {
        if (is_array($pd)) {
            $useMinMax = false;
            if (isset($pd['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD, $pd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pd['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD, $pd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PD, $pd, $comparison);
    }

    /**
     * Filter the query on the rombel column
     *
     * Example usage:
     * <code>
     * $query->filterByRombel(1234); // WHERE rombel = 1234
     * $query->filterByRombel(array(12, 34)); // WHERE rombel IN (12, 34)
     * $query->filterByRombel(array('min' => 12)); // WHERE rombel >= 12
     * $query->filterByRombel(array('max' => 12)); // WHERE rombel <= 12
     * </code>
     *
     * @param     mixed $rombel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByRombel($rombel = null, $comparison = null)
    {
        if (is_array($rombel)) {
            $useMinMax = false;
            if (isset($rombel['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::ROMBEL, $rombel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rombel['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::ROMBEL, $rombel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::ROMBEL, $rombel, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah_kecamatan column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayahKecamatan('fooValue');   // WHERE kode_wilayah_kecamatan = 'fooValue'
     * $query->filterByKodeWilayahKecamatan('%fooValue%'); // WHERE kode_wilayah_kecamatan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayahKecamatan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayahKecamatan($kodeWilayahKecamatan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayahKecamatan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayahKecamatan)) {
                $kodeWilayahKecamatan = str_replace('*', '%', $kodeWilayahKecamatan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH_KECAMATAN, $kodeWilayahKecamatan, $comparison);
    }

    /**
     * Filter the query on the kecamatan column
     *
     * Example usage:
     * <code>
     * $query->filterByKecamatan('fooValue');   // WHERE kecamatan = 'fooValue'
     * $query->filterByKecamatan('%fooValue%'); // WHERE kecamatan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kecamatan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByKecamatan($kecamatan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kecamatan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kecamatan)) {
                $kecamatan = str_replace('*', '%', $kecamatan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::KECAMATAN, $kecamatan, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah_kecamatan column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayahKecamatan('fooValue');   // WHERE mst_kode_wilayah_kecamatan = 'fooValue'
     * $query->filterByMstKodeWilayahKecamatan('%fooValue%'); // WHERE mst_kode_wilayah_kecamatan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayahKecamatan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayahKecamatan($mstKodeWilayahKecamatan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayahKecamatan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayahKecamatan)) {
                $mstKodeWilayahKecamatan = str_replace('*', '%', $mstKodeWilayahKecamatan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::MST_KODE_WILAYAH_KECAMATAN, $mstKodeWilayahKecamatan, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah_kecamatan column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayahKecamatan(1234); // WHERE id_level_wilayah_kecamatan = 1234
     * $query->filterByIdLevelWilayahKecamatan(array(12, 34)); // WHERE id_level_wilayah_kecamatan IN (12, 34)
     * $query->filterByIdLevelWilayahKecamatan(array('min' => 12)); // WHERE id_level_wilayah_kecamatan >= 12
     * $query->filterByIdLevelWilayahKecamatan(array('max' => 12)); // WHERE id_level_wilayah_kecamatan <= 12
     * </code>
     *
     * @param     mixed $idLevelWilayahKecamatan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayahKecamatan($idLevelWilayahKecamatan = null, $comparison = null)
    {
        if (is_array($idLevelWilayahKecamatan)) {
            $useMinMax = false;
            if (isset($idLevelWilayahKecamatan['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN, $idLevelWilayahKecamatan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLevelWilayahKecamatan['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN, $idLevelWilayahKecamatan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_KECAMATAN, $idLevelWilayahKecamatan, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah_kabupaten column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayahKabupaten('fooValue');   // WHERE kode_wilayah_kabupaten = 'fooValue'
     * $query->filterByKodeWilayahKabupaten('%fooValue%'); // WHERE kode_wilayah_kabupaten LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayahKabupaten The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayahKabupaten($kodeWilayahKabupaten = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayahKabupaten)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayahKabupaten)) {
                $kodeWilayahKabupaten = str_replace('*', '%', $kodeWilayahKabupaten);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH_KABUPATEN, $kodeWilayahKabupaten, $comparison);
    }

    /**
     * Filter the query on the kabupaten column
     *
     * Example usage:
     * <code>
     * $query->filterByKabupaten('fooValue');   // WHERE kabupaten = 'fooValue'
     * $query->filterByKabupaten('%fooValue%'); // WHERE kabupaten LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kabupaten The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByKabupaten($kabupaten = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kabupaten)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kabupaten)) {
                $kabupaten = str_replace('*', '%', $kabupaten);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::KABUPATEN, $kabupaten, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah_kabupaten column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayahKabupaten('fooValue');   // WHERE mst_kode_wilayah_kabupaten = 'fooValue'
     * $query->filterByMstKodeWilayahKabupaten('%fooValue%'); // WHERE mst_kode_wilayah_kabupaten LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayahKabupaten The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayahKabupaten($mstKodeWilayahKabupaten = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayahKabupaten)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayahKabupaten)) {
                $mstKodeWilayahKabupaten = str_replace('*', '%', $mstKodeWilayahKabupaten);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::MST_KODE_WILAYAH_KABUPATEN, $mstKodeWilayahKabupaten, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah_kabupaten column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayahKabupaten(1234); // WHERE id_level_wilayah_kabupaten = 1234
     * $query->filterByIdLevelWilayahKabupaten(array(12, 34)); // WHERE id_level_wilayah_kabupaten IN (12, 34)
     * $query->filterByIdLevelWilayahKabupaten(array('min' => 12)); // WHERE id_level_wilayah_kabupaten >= 12
     * $query->filterByIdLevelWilayahKabupaten(array('max' => 12)); // WHERE id_level_wilayah_kabupaten <= 12
     * </code>
     *
     * @param     mixed $idLevelWilayahKabupaten The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayahKabupaten($idLevelWilayahKabupaten = null, $comparison = null)
    {
        if (is_array($idLevelWilayahKabupaten)) {
            $useMinMax = false;
            if (isset($idLevelWilayahKabupaten['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN, $idLevelWilayahKabupaten['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLevelWilayahKabupaten['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN, $idLevelWilayahKabupaten['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_KABUPATEN, $idLevelWilayahKabupaten, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah_provinsi column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayahProvinsi('fooValue');   // WHERE kode_wilayah_provinsi = 'fooValue'
     * $query->filterByKodeWilayahProvinsi('%fooValue%'); // WHERE kode_wilayah_provinsi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayahProvinsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayahProvinsi($kodeWilayahProvinsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayahProvinsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayahProvinsi)) {
                $kodeWilayahProvinsi = str_replace('*', '%', $kodeWilayahProvinsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH_PROVINSI, $kodeWilayahProvinsi, $comparison);
    }

    /**
     * Filter the query on the provinsi column
     *
     * Example usage:
     * <code>
     * $query->filterByProvinsi('fooValue');   // WHERE provinsi = 'fooValue'
     * $query->filterByProvinsi('%fooValue%'); // WHERE provinsi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $provinsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByProvinsi($provinsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($provinsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $provinsi)) {
                $provinsi = str_replace('*', '%', $provinsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PROVINSI, $provinsi, $comparison);
    }

    /**
     * Filter the query on the mst_kode_wilayah_provinsi column
     *
     * Example usage:
     * <code>
     * $query->filterByMstKodeWilayahProvinsi('fooValue');   // WHERE mst_kode_wilayah_provinsi = 'fooValue'
     * $query->filterByMstKodeWilayahProvinsi('%fooValue%'); // WHERE mst_kode_wilayah_provinsi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mstKodeWilayahProvinsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByMstKodeWilayahProvinsi($mstKodeWilayahProvinsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mstKodeWilayahProvinsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mstKodeWilayahProvinsi)) {
                $mstKodeWilayahProvinsi = str_replace('*', '%', $mstKodeWilayahProvinsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::MST_KODE_WILAYAH_PROVINSI, $mstKodeWilayahProvinsi, $comparison);
    }

    /**
     * Filter the query on the id_level_wilayah_provinsi column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLevelWilayahProvinsi(1234); // WHERE id_level_wilayah_provinsi = 1234
     * $query->filterByIdLevelWilayahProvinsi(array(12, 34)); // WHERE id_level_wilayah_provinsi IN (12, 34)
     * $query->filterByIdLevelWilayahProvinsi(array('min' => 12)); // WHERE id_level_wilayah_provinsi >= 12
     * $query->filterByIdLevelWilayahProvinsi(array('max' => 12)); // WHERE id_level_wilayah_provinsi <= 12
     * </code>
     *
     * @param     mixed $idLevelWilayahProvinsi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByIdLevelWilayahProvinsi($idLevelWilayahProvinsi = null, $comparison = null)
    {
        if (is_array($idLevelWilayahProvinsi)) {
            $useMinMax = false;
            if (isset($idLevelWilayahProvinsi['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI, $idLevelWilayahProvinsi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLevelWilayahProvinsi['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI, $idLevelWilayahProvinsi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::ID_LEVEL_WILAYAH_PROVINSI, $idLevelWilayahProvinsi, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the npsn column
     *
     * Example usage:
     * <code>
     * $query->filterByNpsn('fooValue');   // WHERE npsn = 'fooValue'
     * $query->filterByNpsn('%fooValue%'); // WHERE npsn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $npsn The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByNpsn($npsn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($npsn)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $npsn)) {
                $npsn = str_replace('*', '%', $npsn);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::NPSN, $npsn, $comparison);
    }

    /**
     * Filter the query on the bentuk_pendidikan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBentukPendidikanId('fooValue');   // WHERE bentuk_pendidikan_id = 'fooValue'
     * $query->filterByBentukPendidikanId('%fooValue%'); // WHERE bentuk_pendidikan_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bentukPendidikanId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByBentukPendidikanId($bentukPendidikanId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bentukPendidikanId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bentukPendidikanId)) {
                $bentukPendidikanId = str_replace('*', '%', $bentukPendidikanId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId, $comparison);
    }

    /**
     * Filter the query on the status_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSekolah('fooValue');   // WHERE status_sekolah = 'fooValue'
     * $query->filterByStatusSekolah('%fooValue%'); // WHERE status_sekolah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statusSekolah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSekolah($statusSekolah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statusSekolah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statusSekolah)) {
                $statusSekolah = str_replace('*', '%', $statusSekolah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::STATUS_SEKOLAH, $statusSekolah, $comparison);
    }

    /**
     * Filter the query on the guru_matematika column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruMatematika(1234); // WHERE guru_matematika = 1234
     * $query->filterByGuruMatematika(array(12, 34)); // WHERE guru_matematika IN (12, 34)
     * $query->filterByGuruMatematika(array('min' => 12)); // WHERE guru_matematika >= 12
     * $query->filterByGuruMatematika(array('max' => 12)); // WHERE guru_matematika <= 12
     * </code>
     *
     * @param     mixed $guruMatematika The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruMatematika($guruMatematika = null, $comparison = null)
    {
        if (is_array($guruMatematika)) {
            $useMinMax = false;
            if (isset($guruMatematika['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_MATEMATIKA, $guruMatematika['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruMatematika['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_MATEMATIKA, $guruMatematika['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_MATEMATIKA, $guruMatematika, $comparison);
    }

    /**
     * Filter the query on the guru_bahasa_indonesia column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruBahasaIndonesia(1234); // WHERE guru_bahasa_indonesia = 1234
     * $query->filterByGuruBahasaIndonesia(array(12, 34)); // WHERE guru_bahasa_indonesia IN (12, 34)
     * $query->filterByGuruBahasaIndonesia(array('min' => 12)); // WHERE guru_bahasa_indonesia >= 12
     * $query->filterByGuruBahasaIndonesia(array('max' => 12)); // WHERE guru_bahasa_indonesia <= 12
     * </code>
     *
     * @param     mixed $guruBahasaIndonesia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruBahasaIndonesia($guruBahasaIndonesia = null, $comparison = null)
    {
        if (is_array($guruBahasaIndonesia)) {
            $useMinMax = false;
            if (isset($guruBahasaIndonesia['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_BAHASA_INDONESIA, $guruBahasaIndonesia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruBahasaIndonesia['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_BAHASA_INDONESIA, $guruBahasaIndonesia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_BAHASA_INDONESIA, $guruBahasaIndonesia, $comparison);
    }

    /**
     * Filter the query on the guru_bahasa_inggris column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruBahasaInggris(1234); // WHERE guru_bahasa_inggris = 1234
     * $query->filterByGuruBahasaInggris(array(12, 34)); // WHERE guru_bahasa_inggris IN (12, 34)
     * $query->filterByGuruBahasaInggris(array('min' => 12)); // WHERE guru_bahasa_inggris >= 12
     * $query->filterByGuruBahasaInggris(array('max' => 12)); // WHERE guru_bahasa_inggris <= 12
     * </code>
     *
     * @param     mixed $guruBahasaInggris The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruBahasaInggris($guruBahasaInggris = null, $comparison = null)
    {
        if (is_array($guruBahasaInggris)) {
            $useMinMax = false;
            if (isset($guruBahasaInggris['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_BAHASA_INGGRIS, $guruBahasaInggris['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruBahasaInggris['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_BAHASA_INGGRIS, $guruBahasaInggris['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_BAHASA_INGGRIS, $guruBahasaInggris, $comparison);
    }

    /**
     * Filter the query on the guru_sejarah_indonesia column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruSejarahIndonesia(1234); // WHERE guru_sejarah_indonesia = 1234
     * $query->filterByGuruSejarahIndonesia(array(12, 34)); // WHERE guru_sejarah_indonesia IN (12, 34)
     * $query->filterByGuruSejarahIndonesia(array('min' => 12)); // WHERE guru_sejarah_indonesia >= 12
     * $query->filterByGuruSejarahIndonesia(array('max' => 12)); // WHERE guru_sejarah_indonesia <= 12
     * </code>
     *
     * @param     mixed $guruSejarahIndonesia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruSejarahIndonesia($guruSejarahIndonesia = null, $comparison = null)
    {
        if (is_array($guruSejarahIndonesia)) {
            $useMinMax = false;
            if (isset($guruSejarahIndonesia['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_SEJARAH_INDONESIA, $guruSejarahIndonesia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruSejarahIndonesia['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_SEJARAH_INDONESIA, $guruSejarahIndonesia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_SEJARAH_INDONESIA, $guruSejarahIndonesia, $comparison);
    }

    /**
     * Filter the query on the guru_pkn column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruPkn(1234); // WHERE guru_pkn = 1234
     * $query->filterByGuruPkn(array(12, 34)); // WHERE guru_pkn IN (12, 34)
     * $query->filterByGuruPkn(array('min' => 12)); // WHERE guru_pkn >= 12
     * $query->filterByGuruPkn(array('max' => 12)); // WHERE guru_pkn <= 12
     * </code>
     *
     * @param     mixed $guruPkn The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruPkn($guruPkn = null, $comparison = null)
    {
        if (is_array($guruPkn)) {
            $useMinMax = false;
            if (isset($guruPkn['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_PKN, $guruPkn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruPkn['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_PKN, $guruPkn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_PKN, $guruPkn, $comparison);
    }

    /**
     * Filter the query on the guru_penjaskes column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruPenjaskes(1234); // WHERE guru_penjaskes = 1234
     * $query->filterByGuruPenjaskes(array(12, 34)); // WHERE guru_penjaskes IN (12, 34)
     * $query->filterByGuruPenjaskes(array('min' => 12)); // WHERE guru_penjaskes >= 12
     * $query->filterByGuruPenjaskes(array('max' => 12)); // WHERE guru_penjaskes <= 12
     * </code>
     *
     * @param     mixed $guruPenjaskes The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruPenjaskes($guruPenjaskes = null, $comparison = null)
    {
        if (is_array($guruPenjaskes)) {
            $useMinMax = false;
            if (isset($guruPenjaskes['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_PENJASKES, $guruPenjaskes['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruPenjaskes['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_PENJASKES, $guruPenjaskes['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_PENJASKES, $guruPenjaskes, $comparison);
    }

    /**
     * Filter the query on the guru_agama_budi_pekerti column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruAgamaBudiPekerti(1234); // WHERE guru_agama_budi_pekerti = 1234
     * $query->filterByGuruAgamaBudiPekerti(array(12, 34)); // WHERE guru_agama_budi_pekerti IN (12, 34)
     * $query->filterByGuruAgamaBudiPekerti(array('min' => 12)); // WHERE guru_agama_budi_pekerti >= 12
     * $query->filterByGuruAgamaBudiPekerti(array('max' => 12)); // WHERE guru_agama_budi_pekerti <= 12
     * </code>
     *
     * @param     mixed $guruAgamaBudiPekerti The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruAgamaBudiPekerti($guruAgamaBudiPekerti = null, $comparison = null)
    {
        if (is_array($guruAgamaBudiPekerti)) {
            $useMinMax = false;
            if (isset($guruAgamaBudiPekerti['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI, $guruAgamaBudiPekerti['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruAgamaBudiPekerti['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI, $guruAgamaBudiPekerti['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_AGAMA_BUDI_PEKERTI, $guruAgamaBudiPekerti, $comparison);
    }

    /**
     * Filter the query on the guru_seni_budaya column
     *
     * Example usage:
     * <code>
     * $query->filterByGuruSeniBudaya(1234); // WHERE guru_seni_budaya = 1234
     * $query->filterByGuruSeniBudaya(array(12, 34)); // WHERE guru_seni_budaya IN (12, 34)
     * $query->filterByGuruSeniBudaya(array('min' => 12)); // WHERE guru_seni_budaya >= 12
     * $query->filterByGuruSeniBudaya(array('max' => 12)); // WHERE guru_seni_budaya <= 12
     * </code>
     *
     * @param     mixed $guruSeniBudaya The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByGuruSeniBudaya($guruSeniBudaya = null, $comparison = null)
    {
        if (is_array($guruSeniBudaya)) {
            $useMinMax = false;
            if (isset($guruSeniBudaya['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_SENI_BUDAYA, $guruSeniBudaya['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($guruSeniBudaya['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::GURU_SENI_BUDAYA, $guruSeniBudaya['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::GURU_SENI_BUDAYA, $guruSeniBudaya, $comparison);
    }

    /**
     * Filter the query on the pd_kelas_10 column
     *
     * Example usage:
     * <code>
     * $query->filterByPdKelas10(1234); // WHERE pd_kelas_10 = 1234
     * $query->filterByPdKelas10(array(12, 34)); // WHERE pd_kelas_10 IN (12, 34)
     * $query->filterByPdKelas10(array('min' => 12)); // WHERE pd_kelas_10 >= 12
     * $query->filterByPdKelas10(array('max' => 12)); // WHERE pd_kelas_10 <= 12
     * </code>
     *
     * @param     mixed $pdKelas10 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPdKelas10($pdKelas10 = null, $comparison = null)
    {
        if (is_array($pdKelas10)) {
            $useMinMax = false;
            if (isset($pdKelas10['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_10, $pdKelas10['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdKelas10['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_10, $pdKelas10['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_10, $pdKelas10, $comparison);
    }

    /**
     * Filter the query on the pd_kelas_11 column
     *
     * Example usage:
     * <code>
     * $query->filterByPdKelas11(1234); // WHERE pd_kelas_11 = 1234
     * $query->filterByPdKelas11(array(12, 34)); // WHERE pd_kelas_11 IN (12, 34)
     * $query->filterByPdKelas11(array('min' => 12)); // WHERE pd_kelas_11 >= 12
     * $query->filterByPdKelas11(array('max' => 12)); // WHERE pd_kelas_11 <= 12
     * </code>
     *
     * @param     mixed $pdKelas11 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPdKelas11($pdKelas11 = null, $comparison = null)
    {
        if (is_array($pdKelas11)) {
            $useMinMax = false;
            if (isset($pdKelas11['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_11, $pdKelas11['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdKelas11['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_11, $pdKelas11['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_11, $pdKelas11, $comparison);
    }

    /**
     * Filter the query on the pd_kelas_12 column
     *
     * Example usage:
     * <code>
     * $query->filterByPdKelas12(1234); // WHERE pd_kelas_12 = 1234
     * $query->filterByPdKelas12(array(12, 34)); // WHERE pd_kelas_12 IN (12, 34)
     * $query->filterByPdKelas12(array('min' => 12)); // WHERE pd_kelas_12 >= 12
     * $query->filterByPdKelas12(array('max' => 12)); // WHERE pd_kelas_12 <= 12
     * </code>
     *
     * @param     mixed $pdKelas12 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPdKelas12($pdKelas12 = null, $comparison = null)
    {
        if (is_array($pdKelas12)) {
            $useMinMax = false;
            if (isset($pdKelas12['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_12, $pdKelas12['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdKelas12['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_12, $pdKelas12['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_12, $pdKelas12, $comparison);
    }

    /**
     * Filter the query on the pd_kelas_13 column
     *
     * Example usage:
     * <code>
     * $query->filterByPdKelas13(1234); // WHERE pd_kelas_13 = 1234
     * $query->filterByPdKelas13(array(12, 34)); // WHERE pd_kelas_13 IN (12, 34)
     * $query->filterByPdKelas13(array('min' => 12)); // WHERE pd_kelas_13 >= 12
     * $query->filterByPdKelas13(array('max' => 12)); // WHERE pd_kelas_13 <= 12
     * </code>
     *
     * @param     mixed $pdKelas13 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPdKelas13($pdKelas13 = null, $comparison = null)
    {
        if (is_array($pdKelas13)) {
            $useMinMax = false;
            if (isset($pdKelas13['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_13, $pdKelas13['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pdKelas13['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_13, $pdKelas13['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::PD_KELAS_13, $pdKelas13, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   RekapSekolah $rekapSekolah Object to remove from the list of results
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function prune($rekapSekolah = null)
    {
        if ($rekapSekolah) {
            $this->addUsingAlias(RekapSekolahPeer::REKAP_SEKOLAH_ID, $rekapSekolah->getRekapSekolahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
