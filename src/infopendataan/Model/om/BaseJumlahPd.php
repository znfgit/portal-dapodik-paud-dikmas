<?php

namespace infopendataan\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\JumlahPd;
use infopendataan\Model\JumlahPdPeer;
use infopendataan\Model\JumlahPdQuery;

/**
 * Base class that represents a row from the 'jumlah_pd' table.
 *
 * 
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseJumlahPd extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'infopendataan\\Model\\JumlahPdPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        JumlahPdPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the jumlah_pd_id field.
     * @var        int
     */
    protected $jumlah_pd_id;

    /**
     * The value for the tanggal field.
     * @var        string
     */
    protected $tanggal;

    /**
     * The value for the id_level_wilayah field.
     * @var        string
     */
    protected $id_level_wilayah;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the mst_kode_wilayah field.
     * @var        string
     */
    protected $mst_kode_wilayah;

    /**
     * The value for the tahun_ajaran_id field.
     * @var        string
     */
    protected $tahun_ajaran_id;

    /**
     * The value for the pd_di_rombel field.
     * @var        double
     */
    protected $pd_di_rombel;

    /**
     * The value for the pd_belum_di_rombel field.
     * @var        double
     */
    protected $pd_belum_di_rombel;

    /**
     * The value for the pd_total field.
     * @var        double
     */
    protected $pd_total;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [jumlah_pd_id] column value.
     * 
     * @return int
     */
    public function getJumlahPdId()
    {
        return $this->jumlah_pd_id;
    }

    /**
     * Get the [optionally formatted] temporal [tanggal] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTanggal($format = 'Y-m-d H:i:s')
    {
        if ($this->tanggal === null) {
            return null;
        }

        if ($this->tanggal === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->tanggal);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->tanggal, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [id_level_wilayah] column value.
     * 
     * @return string
     */
    public function getIdLevelWilayah()
    {
        return $this->id_level_wilayah;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [mst_kode_wilayah] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayah()
    {
        return $this->mst_kode_wilayah;
    }

    /**
     * Get the [tahun_ajaran_id] column value.
     * 
     * @return string
     */
    public function getTahunAjaranId()
    {
        return $this->tahun_ajaran_id;
    }

    /**
     * Get the [pd_di_rombel] column value.
     * 
     * @return double
     */
    public function getPdDiRombel()
    {
        return $this->pd_di_rombel;
    }

    /**
     * Get the [pd_belum_di_rombel] column value.
     * 
     * @return double
     */
    public function getPdBelumDiRombel()
    {
        return $this->pd_belum_di_rombel;
    }

    /**
     * Get the [pd_total] column value.
     * 
     * @return double
     */
    public function getPdTotal()
    {
        return $this->pd_total;
    }

    /**
     * Set the value of [jumlah_pd_id] column.
     * 
     * @param int $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setJumlahPdId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jumlah_pd_id !== $v) {
            $this->jumlah_pd_id = $v;
            $this->modifiedColumns[] = JumlahPdPeer::JUMLAH_PD_ID;
        }


        return $this;
    } // setJumlahPdId()

    /**
     * Sets the value of [tanggal] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setTanggal($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tanggal !== null || $dt !== null) {
            $currentDateAsString = ($this->tanggal !== null && $tmpDt = new DateTime($this->tanggal)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->tanggal = $newDateAsString;
                $this->modifiedColumns[] = JumlahPdPeer::TANGGAL;
            }
        } // if either are not null


        return $this;
    } // setTanggal()

    /**
     * Set the value of [id_level_wilayah] column.
     * 
     * @param string $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setIdLevelWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_level_wilayah !== $v) {
            $this->id_level_wilayah = $v;
            $this->modifiedColumns[] = JumlahPdPeer::ID_LEVEL_WILAYAH;
        }


        return $this;
    } // setIdLevelWilayah()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = JumlahPdPeer::KODE_WILAYAH;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [mst_kode_wilayah] column.
     * 
     * @param string $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setMstKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah !== $v) {
            $this->mst_kode_wilayah = $v;
            $this->modifiedColumns[] = JumlahPdPeer::MST_KODE_WILAYAH;
        }


        return $this;
    } // setMstKodeWilayah()

    /**
     * Set the value of [tahun_ajaran_id] column.
     * 
     * @param string $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setTahunAjaranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tahun_ajaran_id !== $v) {
            $this->tahun_ajaran_id = $v;
            $this->modifiedColumns[] = JumlahPdPeer::TAHUN_AJARAN_ID;
        }


        return $this;
    } // setTahunAjaranId()

    /**
     * Set the value of [pd_di_rombel] column.
     * 
     * @param double $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setPdDiRombel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->pd_di_rombel !== $v) {
            $this->pd_di_rombel = $v;
            $this->modifiedColumns[] = JumlahPdPeer::PD_DI_ROMBEL;
        }


        return $this;
    } // setPdDiRombel()

    /**
     * Set the value of [pd_belum_di_rombel] column.
     * 
     * @param double $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setPdBelumDiRombel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->pd_belum_di_rombel !== $v) {
            $this->pd_belum_di_rombel = $v;
            $this->modifiedColumns[] = JumlahPdPeer::PD_BELUM_DI_ROMBEL;
        }


        return $this;
    } // setPdBelumDiRombel()

    /**
     * Set the value of [pd_total] column.
     * 
     * @param double $v new value
     * @return JumlahPd The current object (for fluent API support)
     */
    public function setPdTotal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->pd_total !== $v) {
            $this->pd_total = $v;
            $this->modifiedColumns[] = JumlahPdPeer::PD_TOTAL;
        }


        return $this;
    } // setPdTotal()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->jumlah_pd_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->tanggal = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->id_level_wilayah = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->kode_wilayah = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->mst_kode_wilayah = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->tahun_ajaran_id = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->pd_di_rombel = ($row[$startcol + 6] !== null) ? (double) $row[$startcol + 6] : null;
            $this->pd_belum_di_rombel = ($row[$startcol + 7] !== null) ? (double) $row[$startcol + 7] : null;
            $this->pd_total = ($row[$startcol + 8] !== null) ? (double) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 9; // 9 = JumlahPdPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating JumlahPd object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahPdPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = JumlahPdPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahPdPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = JumlahPdQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahPdPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                JumlahPdPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = JumlahPdPeer::JUMLAH_PD_ID;
        if (null !== $this->jumlah_pd_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . JumlahPdPeer::JUMLAH_PD_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(JumlahPdPeer::JUMLAH_PD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`jumlah_pd_id`';
        }
        if ($this->isColumnModified(JumlahPdPeer::TANGGAL)) {
            $modifiedColumns[':p' . $index++]  = '`tanggal`';
        }
        if ($this->isColumnModified(JumlahPdPeer::ID_LEVEL_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`id_level_wilayah`';
        }
        if ($this->isColumnModified(JumlahPdPeer::KODE_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`kode_wilayah`';
        }
        if ($this->isColumnModified(JumlahPdPeer::MST_KODE_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`mst_kode_wilayah`';
        }
        if ($this->isColumnModified(JumlahPdPeer::TAHUN_AJARAN_ID)) {
            $modifiedColumns[':p' . $index++]  = '`tahun_ajaran_id`';
        }
        if ($this->isColumnModified(JumlahPdPeer::PD_DI_ROMBEL)) {
            $modifiedColumns[':p' . $index++]  = '`pd_di_rombel`';
        }
        if ($this->isColumnModified(JumlahPdPeer::PD_BELUM_DI_ROMBEL)) {
            $modifiedColumns[':p' . $index++]  = '`pd_belum_di_rombel`';
        }
        if ($this->isColumnModified(JumlahPdPeer::PD_TOTAL)) {
            $modifiedColumns[':p' . $index++]  = '`pd_total`';
        }

        $sql = sprintf(
            'INSERT INTO `jumlah_pd` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`jumlah_pd_id`':						
                        $stmt->bindValue($identifier, $this->jumlah_pd_id, PDO::PARAM_INT);
                        break;
                    case '`tanggal`':						
                        $stmt->bindValue($identifier, $this->tanggal, PDO::PARAM_STR);
                        break;
                    case '`id_level_wilayah`':						
                        $stmt->bindValue($identifier, $this->id_level_wilayah, PDO::PARAM_STR);
                        break;
                    case '`kode_wilayah`':						
                        $stmt->bindValue($identifier, $this->kode_wilayah, PDO::PARAM_STR);
                        break;
                    case '`mst_kode_wilayah`':						
                        $stmt->bindValue($identifier, $this->mst_kode_wilayah, PDO::PARAM_STR);
                        break;
                    case '`tahun_ajaran_id`':						
                        $stmt->bindValue($identifier, $this->tahun_ajaran_id, PDO::PARAM_STR);
                        break;
                    case '`pd_di_rombel`':						
                        $stmt->bindValue($identifier, $this->pd_di_rombel, PDO::PARAM_STR);
                        break;
                    case '`pd_belum_di_rombel`':						
                        $stmt->bindValue($identifier, $this->pd_belum_di_rombel, PDO::PARAM_STR);
                        break;
                    case '`pd_total`':						
                        $stmt->bindValue($identifier, $this->pd_total, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setJumlahPdId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = JumlahPdPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JumlahPdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getJumlahPdId();
                break;
            case 1:
                return $this->getTanggal();
                break;
            case 2:
                return $this->getIdLevelWilayah();
                break;
            case 3:
                return $this->getKodeWilayah();
                break;
            case 4:
                return $this->getMstKodeWilayah();
                break;
            case 5:
                return $this->getTahunAjaranId();
                break;
            case 6:
                return $this->getPdDiRombel();
                break;
            case 7:
                return $this->getPdBelumDiRombel();
                break;
            case 8:
                return $this->getPdTotal();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['JumlahPd'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['JumlahPd'][$this->getPrimaryKey()] = true;
        $keys = JumlahPdPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getJumlahPdId(),
            $keys[1] => $this->getTanggal(),
            $keys[2] => $this->getIdLevelWilayah(),
            $keys[3] => $this->getKodeWilayah(),
            $keys[4] => $this->getMstKodeWilayah(),
            $keys[5] => $this->getTahunAjaranId(),
            $keys[6] => $this->getPdDiRombel(),
            $keys[7] => $this->getPdBelumDiRombel(),
            $keys[8] => $this->getPdTotal(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JumlahPdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setJumlahPdId($value);
                break;
            case 1:
                $this->setTanggal($value);
                break;
            case 2:
                $this->setIdLevelWilayah($value);
                break;
            case 3:
                $this->setKodeWilayah($value);
                break;
            case 4:
                $this->setMstKodeWilayah($value);
                break;
            case 5:
                $this->setTahunAjaranId($value);
                break;
            case 6:
                $this->setPdDiRombel($value);
                break;
            case 7:
                $this->setPdBelumDiRombel($value);
                break;
            case 8:
                $this->setPdTotal($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = JumlahPdPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setJumlahPdId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTanggal($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdLevelWilayah($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKodeWilayah($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMstKodeWilayah($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTahunAjaranId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPdDiRombel($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPdBelumDiRombel($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPdTotal($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(JumlahPdPeer::DATABASE_NAME);

        if ($this->isColumnModified(JumlahPdPeer::JUMLAH_PD_ID)) $criteria->add(JumlahPdPeer::JUMLAH_PD_ID, $this->jumlah_pd_id);
        if ($this->isColumnModified(JumlahPdPeer::TANGGAL)) $criteria->add(JumlahPdPeer::TANGGAL, $this->tanggal);
        if ($this->isColumnModified(JumlahPdPeer::ID_LEVEL_WILAYAH)) $criteria->add(JumlahPdPeer::ID_LEVEL_WILAYAH, $this->id_level_wilayah);
        if ($this->isColumnModified(JumlahPdPeer::KODE_WILAYAH)) $criteria->add(JumlahPdPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(JumlahPdPeer::MST_KODE_WILAYAH)) $criteria->add(JumlahPdPeer::MST_KODE_WILAYAH, $this->mst_kode_wilayah);
        if ($this->isColumnModified(JumlahPdPeer::TAHUN_AJARAN_ID)) $criteria->add(JumlahPdPeer::TAHUN_AJARAN_ID, $this->tahun_ajaran_id);
        if ($this->isColumnModified(JumlahPdPeer::PD_DI_ROMBEL)) $criteria->add(JumlahPdPeer::PD_DI_ROMBEL, $this->pd_di_rombel);
        if ($this->isColumnModified(JumlahPdPeer::PD_BELUM_DI_ROMBEL)) $criteria->add(JumlahPdPeer::PD_BELUM_DI_ROMBEL, $this->pd_belum_di_rombel);
        if ($this->isColumnModified(JumlahPdPeer::PD_TOTAL)) $criteria->add(JumlahPdPeer::PD_TOTAL, $this->pd_total);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(JumlahPdPeer::DATABASE_NAME);
        $criteria->add(JumlahPdPeer::JUMLAH_PD_ID, $this->jumlah_pd_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getJumlahPdId();
    }

    /**
     * Generic method to set the primary key (jumlah_pd_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setJumlahPdId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getJumlahPdId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of JumlahPd (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTanggal($this->getTanggal());
        $copyObj->setIdLevelWilayah($this->getIdLevelWilayah());
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setMstKodeWilayah($this->getMstKodeWilayah());
        $copyObj->setTahunAjaranId($this->getTahunAjaranId());
        $copyObj->setPdDiRombel($this->getPdDiRombel());
        $copyObj->setPdBelumDiRombel($this->getPdBelumDiRombel());
        $copyObj->setPdTotal($this->getPdTotal());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setJumlahPdId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return JumlahPd Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return JumlahPdPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new JumlahPdPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->jumlah_pd_id = null;
        $this->tanggal = null;
        $this->id_level_wilayah = null;
        $this->kode_wilayah = null;
        $this->mst_kode_wilayah = null;
        $this->tahun_ajaran_id = null;
        $this->pd_di_rombel = null;
        $this->pd_belum_di_rombel = null;
        $this->pd_total = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(JumlahPdPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
