<?php

namespace infopendataan\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\MstWilayah;
use infopendataan\Model\MstWilayahPeer;
use infopendataan\Model\MstWilayahQuery;

/**
 * Base class that represents a row from the 'mst_wilayah' table.
 *
 * 
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseMstWilayah extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'infopendataan\\Model\\MstWilayahPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        MstWilayahPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the wilayah_id field.
     * @var        string
     */
    protected $wilayah_id;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the id_level_wilayah field.
     * @var        string
     */
    protected $id_level_wilayah;

    /**
     * The value for the mst_kode_wilayah field.
     * @var        string
     */
    protected $mst_kode_wilayah;

    /**
     * The value for the negara_id field.
     * @var        string
     */
    protected $negara_id;

    /**
     * The value for the asal_wilayah field.
     * @var        string
     */
    protected $asal_wilayah;

    /**
     * The value for the kode_bps field.
     * @var        string
     */
    protected $kode_bps;

    /**
     * The value for the kode_dagri field.
     * @var        string
     */
    protected $kode_dagri;

    /**
     * The value for the kode_keu field.
     * @var        string
     */
    protected $kode_keu;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [wilayah_id] column value.
     * 
     * @return string
     */
    public function getWilayahId()
    {
        return $this->wilayah_id;
    }

    /**
     * Get the [kode_wilayah] column value.
     * 
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [id_level_wilayah] column value.
     * 
     * @return string
     */
    public function getIdLevelWilayah()
    {
        return $this->id_level_wilayah;
    }

    /**
     * Get the [mst_kode_wilayah] column value.
     * 
     * @return string
     */
    public function getMstKodeWilayah()
    {
        return $this->mst_kode_wilayah;
    }

    /**
     * Get the [negara_id] column value.
     * 
     * @return string
     */
    public function getNegaraId()
    {
        return $this->negara_id;
    }

    /**
     * Get the [asal_wilayah] column value.
     * 
     * @return string
     */
    public function getAsalWilayah()
    {
        return $this->asal_wilayah;
    }

    /**
     * Get the [kode_bps] column value.
     * 
     * @return string
     */
    public function getKodeBps()
    {
        return $this->kode_bps;
    }

    /**
     * Get the [kode_dagri] column value.
     * 
     * @return string
     */
    public function getKodeDagri()
    {
        return $this->kode_dagri;
    }

    /**
     * Get the [kode_keu] column value.
     * 
     * @return string
     */
    public function getKodeKeu()
    {
        return $this->kode_keu;
    }

    /**
     * Get the [create_date] column value.
     * 
     * @return string
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Get the [last_update] column value.
     * 
     * @return string
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Get the [expired_date] column value.
     * 
     * @return string
     */
    public function getExpiredDate()
    {
        return $this->expired_date;
    }

    /**
     * Get the [last_sync] column value.
     * 
     * @return string
     */
    public function getLastSync()
    {
        return $this->last_sync;
    }

    /**
     * Set the value of [wilayah_id] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setWilayahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->wilayah_id !== $v) {
            $this->wilayah_id = $v;
            $this->modifiedColumns[] = MstWilayahPeer::WILAYAH_ID;
        }


        return $this;
    } // setWilayahId()

    /**
     * Set the value of [kode_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_WILAYAH;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = MstWilayahPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [id_level_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setIdLevelWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_level_wilayah !== $v) {
            $this->id_level_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::ID_LEVEL_WILAYAH;
        }


        return $this;
    } // setIdLevelWilayah()

    /**
     * Set the value of [mst_kode_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setMstKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mst_kode_wilayah !== $v) {
            $this->mst_kode_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::MST_KODE_WILAYAH;
        }


        return $this;
    } // setMstKodeWilayah()

    /**
     * Set the value of [negara_id] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setNegaraId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->negara_id !== $v) {
            $this->negara_id = $v;
            $this->modifiedColumns[] = MstWilayahPeer::NEGARA_ID;
        }


        return $this;
    } // setNegaraId()

    /**
     * Set the value of [asal_wilayah] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setAsalWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->asal_wilayah !== $v) {
            $this->asal_wilayah = $v;
            $this->modifiedColumns[] = MstWilayahPeer::ASAL_WILAYAH;
        }


        return $this;
    } // setAsalWilayah()

    /**
     * Set the value of [kode_bps] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeBps($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_bps !== $v) {
            $this->kode_bps = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_BPS;
        }


        return $this;
    } // setKodeBps()

    /**
     * Set the value of [kode_dagri] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeDagri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_dagri !== $v) {
            $this->kode_dagri = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_DAGRI;
        }


        return $this;
    } // setKodeDagri()

    /**
     * Set the value of [kode_keu] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setKodeKeu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_keu !== $v) {
            $this->kode_keu = $v;
            $this->modifiedColumns[] = MstWilayahPeer::KODE_KEU;
        }


        return $this;
    } // setKodeKeu()

    /**
     * Set the value of [create_date] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->create_date !== $v) {
            $this->create_date = $v;
            $this->modifiedColumns[] = MstWilayahPeer::CREATE_DATE;
        }


        return $this;
    } // setCreateDate()

    /**
     * Set the value of [last_update] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->last_update !== $v) {
            $this->last_update = $v;
            $this->modifiedColumns[] = MstWilayahPeer::LAST_UPDATE;
        }


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [expired_date] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->expired_date !== $v) {
            $this->expired_date = $v;
            $this->modifiedColumns[] = MstWilayahPeer::EXPIRED_DATE;
        }


        return $this;
    } // setExpiredDate()

    /**
     * Set the value of [last_sync] column.
     * 
     * @param string $v new value
     * @return MstWilayah The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->last_sync !== $v) {
            $this->last_sync = $v;
            $this->modifiedColumns[] = MstWilayahPeer::LAST_SYNC;
        }


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->wilayah_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->kode_wilayah = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nama = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->id_level_wilayah = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->mst_kode_wilayah = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->negara_id = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->asal_wilayah = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->kode_bps = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->kode_dagri = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->kode_keu = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->create_date = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->last_update = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->expired_date = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->last_sync = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = MstWilayahPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating MstWilayah object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = MstWilayahPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = MstWilayahQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MstWilayahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MstWilayahPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MstWilayahPeer::WILAYAH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`wilayah_id`';
        }
        if ($this->isColumnModified(MstWilayahPeer::KODE_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`kode_wilayah`';
        }
        if ($this->isColumnModified(MstWilayahPeer::NAMA)) {
            $modifiedColumns[':p' . $index++]  = '`nama`';
        }
        if ($this->isColumnModified(MstWilayahPeer::ID_LEVEL_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`id_level_wilayah`';
        }
        if ($this->isColumnModified(MstWilayahPeer::MST_KODE_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`mst_kode_wilayah`';
        }
        if ($this->isColumnModified(MstWilayahPeer::NEGARA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`negara_id`';
        }
        if ($this->isColumnModified(MstWilayahPeer::ASAL_WILAYAH)) {
            $modifiedColumns[':p' . $index++]  = '`asal_wilayah`';
        }
        if ($this->isColumnModified(MstWilayahPeer::KODE_BPS)) {
            $modifiedColumns[':p' . $index++]  = '`kode_bps`';
        }
        if ($this->isColumnModified(MstWilayahPeer::KODE_DAGRI)) {
            $modifiedColumns[':p' . $index++]  = '`kode_dagri`';
        }
        if ($this->isColumnModified(MstWilayahPeer::KODE_KEU)) {
            $modifiedColumns[':p' . $index++]  = '`kode_keu`';
        }
        if ($this->isColumnModified(MstWilayahPeer::CREATE_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`create_date`';
        }
        if ($this->isColumnModified(MstWilayahPeer::LAST_UPDATE)) {
            $modifiedColumns[':p' . $index++]  = '`last_update`';
        }
        if ($this->isColumnModified(MstWilayahPeer::EXPIRED_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`expired_date`';
        }
        if ($this->isColumnModified(MstWilayahPeer::LAST_SYNC)) {
            $modifiedColumns[':p' . $index++]  = '`last_sync`';
        }

        $sql = sprintf(
            'INSERT INTO `mst_wilayah` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`wilayah_id`':						
                        $stmt->bindValue($identifier, $this->wilayah_id, PDO::PARAM_STR);
                        break;
                    case '`kode_wilayah`':						
                        $stmt->bindValue($identifier, $this->kode_wilayah, PDO::PARAM_STR);
                        break;
                    case '`nama`':						
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case '`id_level_wilayah`':						
                        $stmt->bindValue($identifier, $this->id_level_wilayah, PDO::PARAM_STR);
                        break;
                    case '`mst_kode_wilayah`':						
                        $stmt->bindValue($identifier, $this->mst_kode_wilayah, PDO::PARAM_STR);
                        break;
                    case '`negara_id`':						
                        $stmt->bindValue($identifier, $this->negara_id, PDO::PARAM_STR);
                        break;
                    case '`asal_wilayah`':						
                        $stmt->bindValue($identifier, $this->asal_wilayah, PDO::PARAM_STR);
                        break;
                    case '`kode_bps`':						
                        $stmt->bindValue($identifier, $this->kode_bps, PDO::PARAM_STR);
                        break;
                    case '`kode_dagri`':						
                        $stmt->bindValue($identifier, $this->kode_dagri, PDO::PARAM_STR);
                        break;
                    case '`kode_keu`':						
                        $stmt->bindValue($identifier, $this->kode_keu, PDO::PARAM_STR);
                        break;
                    case '`create_date`':						
                        $stmt->bindValue($identifier, $this->create_date, PDO::PARAM_STR);
                        break;
                    case '`last_update`':						
                        $stmt->bindValue($identifier, $this->last_update, PDO::PARAM_STR);
                        break;
                    case '`expired_date`':						
                        $stmt->bindValue($identifier, $this->expired_date, PDO::PARAM_STR);
                        break;
                    case '`last_sync`':						
                        $stmt->bindValue($identifier, $this->last_sync, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = MstWilayahPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MstWilayahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getWilayahId();
                break;
            case 1:
                return $this->getKodeWilayah();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getIdLevelWilayah();
                break;
            case 4:
                return $this->getMstKodeWilayah();
                break;
            case 5:
                return $this->getNegaraId();
                break;
            case 6:
                return $this->getAsalWilayah();
                break;
            case 7:
                return $this->getKodeBps();
                break;
            case 8:
                return $this->getKodeDagri();
                break;
            case 9:
                return $this->getKodeKeu();
                break;
            case 10:
                return $this->getCreateDate();
                break;
            case 11:
                return $this->getLastUpdate();
                break;
            case 12:
                return $this->getExpiredDate();
                break;
            case 13:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['MstWilayah'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['MstWilayah'][$this->getPrimaryKey()] = true;
        $keys = MstWilayahPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getWilayahId(),
            $keys[1] => $this->getKodeWilayah(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getIdLevelWilayah(),
            $keys[4] => $this->getMstKodeWilayah(),
            $keys[5] => $this->getNegaraId(),
            $keys[6] => $this->getAsalWilayah(),
            $keys[7] => $this->getKodeBps(),
            $keys[8] => $this->getKodeDagri(),
            $keys[9] => $this->getKodeKeu(),
            $keys[10] => $this->getCreateDate(),
            $keys[11] => $this->getLastUpdate(),
            $keys[12] => $this->getExpiredDate(),
            $keys[13] => $this->getLastSync(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MstWilayahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setWilayahId($value);
                break;
            case 1:
                $this->setKodeWilayah($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setIdLevelWilayah($value);
                break;
            case 4:
                $this->setMstKodeWilayah($value);
                break;
            case 5:
                $this->setNegaraId($value);
                break;
            case 6:
                $this->setAsalWilayah($value);
                break;
            case 7:
                $this->setKodeBps($value);
                break;
            case 8:
                $this->setKodeDagri($value);
                break;
            case 9:
                $this->setKodeKeu($value);
                break;
            case 10:
                $this->setCreateDate($value);
                break;
            case 11:
                $this->setLastUpdate($value);
                break;
            case 12:
                $this->setExpiredDate($value);
                break;
            case 13:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = MstWilayahPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setWilayahId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setKodeWilayah($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIdLevelWilayah($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMstKodeWilayah($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNegaraId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAsalWilayah($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setKodeBps($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setKodeDagri($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setKodeKeu($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCreateDate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setLastUpdate($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setExpiredDate($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setLastSync($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);

        if ($this->isColumnModified(MstWilayahPeer::WILAYAH_ID)) $criteria->add(MstWilayahPeer::WILAYAH_ID, $this->wilayah_id);
        if ($this->isColumnModified(MstWilayahPeer::KODE_WILAYAH)) $criteria->add(MstWilayahPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::NAMA)) $criteria->add(MstWilayahPeer::NAMA, $this->nama);
        if ($this->isColumnModified(MstWilayahPeer::ID_LEVEL_WILAYAH)) $criteria->add(MstWilayahPeer::ID_LEVEL_WILAYAH, $this->id_level_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::MST_KODE_WILAYAH)) $criteria->add(MstWilayahPeer::MST_KODE_WILAYAH, $this->mst_kode_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::NEGARA_ID)) $criteria->add(MstWilayahPeer::NEGARA_ID, $this->negara_id);
        if ($this->isColumnModified(MstWilayahPeer::ASAL_WILAYAH)) $criteria->add(MstWilayahPeer::ASAL_WILAYAH, $this->asal_wilayah);
        if ($this->isColumnModified(MstWilayahPeer::KODE_BPS)) $criteria->add(MstWilayahPeer::KODE_BPS, $this->kode_bps);
        if ($this->isColumnModified(MstWilayahPeer::KODE_DAGRI)) $criteria->add(MstWilayahPeer::KODE_DAGRI, $this->kode_dagri);
        if ($this->isColumnModified(MstWilayahPeer::KODE_KEU)) $criteria->add(MstWilayahPeer::KODE_KEU, $this->kode_keu);
        if ($this->isColumnModified(MstWilayahPeer::CREATE_DATE)) $criteria->add(MstWilayahPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(MstWilayahPeer::LAST_UPDATE)) $criteria->add(MstWilayahPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(MstWilayahPeer::EXPIRED_DATE)) $criteria->add(MstWilayahPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(MstWilayahPeer::LAST_SYNC)) $criteria->add(MstWilayahPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(MstWilayahPeer::DATABASE_NAME);
        $criteria->add(MstWilayahPeer::WILAYAH_ID, $this->wilayah_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getWilayahId();
    }

    /**
     * Generic method to set the primary key (wilayah_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setWilayahId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getWilayahId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of MstWilayah (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setNama($this->getNama());
        $copyObj->setIdLevelWilayah($this->getIdLevelWilayah());
        $copyObj->setMstKodeWilayah($this->getMstKodeWilayah());
        $copyObj->setNegaraId($this->getNegaraId());
        $copyObj->setAsalWilayah($this->getAsalWilayah());
        $copyObj->setKodeBps($this->getKodeBps());
        $copyObj->setKodeDagri($this->getKodeDagri());
        $copyObj->setKodeKeu($this->getKodeKeu());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setWilayahId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return MstWilayah Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return MstWilayahPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new MstWilayahPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->wilayah_id = null;
        $this->kode_wilayah = null;
        $this->nama = null;
        $this->id_level_wilayah = null;
        $this->mst_kode_wilayah = null;
        $this->negara_id = null;
        $this->asal_wilayah = null;
        $this->kode_bps = null;
        $this->kode_dagri = null;
        $this->kode_keu = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MstWilayahPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
