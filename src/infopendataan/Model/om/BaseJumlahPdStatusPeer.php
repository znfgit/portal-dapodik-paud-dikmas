<?php

namespace infopendataan\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use infopendataan\Model\JumlahPdStatus;
use infopendataan\Model\JumlahPdStatusPeer;
use infopendataan\Model\map\JumlahPdStatusTableMap;

/**
 * Base static class for performing query and update operations on the 'jumlah_pd_status' table.
 *
 * 
 *
 * @package propel.generator.infopendataan.Model.om
 */
abstract class BaseJumlahPdStatusPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'infopendataan';

    /** the table name for this class */
    const TABLE_NAME = 'jumlah_pd_status';

    /** the related Propel class for this table */
    const OM_CLASS = 'infopendataan\\Model\\JumlahPdStatus';

    /** the related TableMap class for this table */
    const TM_CLASS = 'JumlahPdStatusTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 22;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 22;

    /** the column name for the jumlah_pd_status_id field */
    const JUMLAH_PD_STATUS_ID = 'jumlah_pd_status.jumlah_pd_status_id';

    /** the column name for the tanggal field */
    const TANGGAL = 'jumlah_pd_status.tanggal';

    /** the column name for the id_level_wilayah field */
    const ID_LEVEL_WILAYAH = 'jumlah_pd_status.id_level_wilayah';

    /** the column name for the kode_wilayah field */
    const KODE_WILAYAH = 'jumlah_pd_status.kode_wilayah';

    /** the column name for the mst_kode_wilayah field */
    const MST_KODE_WILAYAH = 'jumlah_pd_status.mst_kode_wilayah';

    /** the column name for the tahun_ajaran_id field */
    const TAHUN_AJARAN_ID = 'jumlah_pd_status.tahun_ajaran_id';

    /** the column name for the pd_terdaftar_negeri field */
    const PD_TERDAFTAR_NEGERI = 'jumlah_pd_status.pd_terdaftar_negeri';

    /** the column name for the pd_terdaftar_swasta field */
    const PD_TERDAFTAR_SWASTA = 'jumlah_pd_status.pd_terdaftar_swasta';

    /** the column name for the pd_rombel_negeri field */
    const PD_ROMBEL_NEGERI = 'jumlah_pd_status.pd_rombel_negeri';

    /** the column name for the pd_rombel_swasta field */
    const PD_ROMBEL_SWASTA = 'jumlah_pd_status.pd_rombel_swasta';

    /** the column name for the pd_terdaftar_sma_negeri field */
    const PD_TERDAFTAR_SMA_NEGERI = 'jumlah_pd_status.pd_terdaftar_sma_negeri';

    /** the column name for the pd_terdaftar_sma_swasta field */
    const PD_TERDAFTAR_SMA_SWASTA = 'jumlah_pd_status.pd_terdaftar_sma_swasta';

    /** the column name for the pd_rombel_sma_negeri field */
    const PD_ROMBEL_SMA_NEGERI = 'jumlah_pd_status.pd_rombel_sma_negeri';

    /** the column name for the pd_rombel_sma_swasta field */
    const PD_ROMBEL_SMA_SWASTA = 'jumlah_pd_status.pd_rombel_sma_swasta';

    /** the column name for the pd_terdaftar_smk_negeri field */
    const PD_TERDAFTAR_SMK_NEGERI = 'jumlah_pd_status.pd_terdaftar_smk_negeri';

    /** the column name for the pd_terdaftar_smk_swasta field */
    const PD_TERDAFTAR_SMK_SWASTA = 'jumlah_pd_status.pd_terdaftar_smk_swasta';

    /** the column name for the pd_rombel_smk_negeri field */
    const PD_ROMBEL_SMK_NEGERI = 'jumlah_pd_status.pd_rombel_smk_negeri';

    /** the column name for the pd_rombel_smk_swasta field */
    const PD_ROMBEL_SMK_SWASTA = 'jumlah_pd_status.pd_rombel_smk_swasta';

    /** the column name for the pd_terdaftar_smlb_negeri field */
    const PD_TERDAFTAR_SMLB_NEGERI = 'jumlah_pd_status.pd_terdaftar_smlb_negeri';

    /** the column name for the pd_terdaftar_smlb_swasta field */
    const PD_TERDAFTAR_SMLB_SWASTA = 'jumlah_pd_status.pd_terdaftar_smlb_swasta';

    /** the column name for the pd_rombel_smlb_negeri field */
    const PD_ROMBEL_SMLB_NEGERI = 'jumlah_pd_status.pd_rombel_smlb_negeri';

    /** the column name for the pd_rombel_smlb_swasta field */
    const PD_ROMBEL_SMLB_SWASTA = 'jumlah_pd_status.pd_rombel_smlb_swasta';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of JumlahPdStatus objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array JumlahPdStatus[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. JumlahPdStatusPeer::$fieldNames[JumlahPdStatusPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('JumlahPdStatusId', 'Tanggal', 'IdLevelWilayah', 'KodeWilayah', 'MstKodeWilayah', 'TahunAjaranId', 'PdTerdaftarNegeri', 'PdTerdaftarSwasta', 'PdRombelNegeri', 'PdRombelSwasta', 'PdTerdaftarSmaNegeri', 'PdTerdaftarSmaSwasta', 'PdRombelSmaNegeri', 'PdRombelSmaSwasta', 'PdTerdaftarSmkNegeri', 'PdTerdaftarSmkSwasta', 'PdRombelSmkNegeri', 'PdRombelSmkSwasta', 'PdTerdaftarSmlbNegeri', 'PdTerdaftarSmlbSwasta', 'PdRombelSmlbNegeri', 'PdRombelSmlbSwasta', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('jumlahPdStatusId', 'tanggal', 'idLevelWilayah', 'kodeWilayah', 'mstKodeWilayah', 'tahunAjaranId', 'pdTerdaftarNegeri', 'pdTerdaftarSwasta', 'pdRombelNegeri', 'pdRombelSwasta', 'pdTerdaftarSmaNegeri', 'pdTerdaftarSmaSwasta', 'pdRombelSmaNegeri', 'pdRombelSmaSwasta', 'pdTerdaftarSmkNegeri', 'pdTerdaftarSmkSwasta', 'pdRombelSmkNegeri', 'pdRombelSmkSwasta', 'pdTerdaftarSmlbNegeri', 'pdTerdaftarSmlbSwasta', 'pdRombelSmlbNegeri', 'pdRombelSmlbSwasta', ),
        BasePeer::TYPE_COLNAME => array (JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, JumlahPdStatusPeer::TANGGAL, JumlahPdStatusPeer::ID_LEVEL_WILAYAH, JumlahPdStatusPeer::KODE_WILAYAH, JumlahPdStatusPeer::MST_KODE_WILAYAH, JumlahPdStatusPeer::TAHUN_AJARAN_ID, JumlahPdStatusPeer::PD_TERDAFTAR_NEGERI, JumlahPdStatusPeer::PD_TERDAFTAR_SWASTA, JumlahPdStatusPeer::PD_ROMBEL_NEGERI, JumlahPdStatusPeer::PD_ROMBEL_SWASTA, JumlahPdStatusPeer::PD_TERDAFTAR_SMA_NEGERI, JumlahPdStatusPeer::PD_TERDAFTAR_SMA_SWASTA, JumlahPdStatusPeer::PD_ROMBEL_SMA_NEGERI, JumlahPdStatusPeer::PD_ROMBEL_SMA_SWASTA, JumlahPdStatusPeer::PD_TERDAFTAR_SMK_NEGERI, JumlahPdStatusPeer::PD_TERDAFTAR_SMK_SWASTA, JumlahPdStatusPeer::PD_ROMBEL_SMK_NEGERI, JumlahPdStatusPeer::PD_ROMBEL_SMK_SWASTA, JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_NEGERI, JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_SWASTA, JumlahPdStatusPeer::PD_ROMBEL_SMLB_NEGERI, JumlahPdStatusPeer::PD_ROMBEL_SMLB_SWASTA, ),
        BasePeer::TYPE_RAW_COLNAME => array ('JUMLAH_PD_STATUS_ID', 'TANGGAL', 'ID_LEVEL_WILAYAH', 'KODE_WILAYAH', 'MST_KODE_WILAYAH', 'TAHUN_AJARAN_ID', 'PD_TERDAFTAR_NEGERI', 'PD_TERDAFTAR_SWASTA', 'PD_ROMBEL_NEGERI', 'PD_ROMBEL_SWASTA', 'PD_TERDAFTAR_SMA_NEGERI', 'PD_TERDAFTAR_SMA_SWASTA', 'PD_ROMBEL_SMA_NEGERI', 'PD_ROMBEL_SMA_SWASTA', 'PD_TERDAFTAR_SMK_NEGERI', 'PD_TERDAFTAR_SMK_SWASTA', 'PD_ROMBEL_SMK_NEGERI', 'PD_ROMBEL_SMK_SWASTA', 'PD_TERDAFTAR_SMLB_NEGERI', 'PD_TERDAFTAR_SMLB_SWASTA', 'PD_ROMBEL_SMLB_NEGERI', 'PD_ROMBEL_SMLB_SWASTA', ),
        BasePeer::TYPE_FIELDNAME => array ('jumlah_pd_status_id', 'tanggal', 'id_level_wilayah', 'kode_wilayah', 'mst_kode_wilayah', 'tahun_ajaran_id', 'pd_terdaftar_negeri', 'pd_terdaftar_swasta', 'pd_rombel_negeri', 'pd_rombel_swasta', 'pd_terdaftar_sma_negeri', 'pd_terdaftar_sma_swasta', 'pd_rombel_sma_negeri', 'pd_rombel_sma_swasta', 'pd_terdaftar_smk_negeri', 'pd_terdaftar_smk_swasta', 'pd_rombel_smk_negeri', 'pd_rombel_smk_swasta', 'pd_terdaftar_smlb_negeri', 'pd_terdaftar_smlb_swasta', 'pd_rombel_smlb_negeri', 'pd_rombel_smlb_swasta', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. JumlahPdStatusPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('JumlahPdStatusId' => 0, 'Tanggal' => 1, 'IdLevelWilayah' => 2, 'KodeWilayah' => 3, 'MstKodeWilayah' => 4, 'TahunAjaranId' => 5, 'PdTerdaftarNegeri' => 6, 'PdTerdaftarSwasta' => 7, 'PdRombelNegeri' => 8, 'PdRombelSwasta' => 9, 'PdTerdaftarSmaNegeri' => 10, 'PdTerdaftarSmaSwasta' => 11, 'PdRombelSmaNegeri' => 12, 'PdRombelSmaSwasta' => 13, 'PdTerdaftarSmkNegeri' => 14, 'PdTerdaftarSmkSwasta' => 15, 'PdRombelSmkNegeri' => 16, 'PdRombelSmkSwasta' => 17, 'PdTerdaftarSmlbNegeri' => 18, 'PdTerdaftarSmlbSwasta' => 19, 'PdRombelSmlbNegeri' => 20, 'PdRombelSmlbSwasta' => 21, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('jumlahPdStatusId' => 0, 'tanggal' => 1, 'idLevelWilayah' => 2, 'kodeWilayah' => 3, 'mstKodeWilayah' => 4, 'tahunAjaranId' => 5, 'pdTerdaftarNegeri' => 6, 'pdTerdaftarSwasta' => 7, 'pdRombelNegeri' => 8, 'pdRombelSwasta' => 9, 'pdTerdaftarSmaNegeri' => 10, 'pdTerdaftarSmaSwasta' => 11, 'pdRombelSmaNegeri' => 12, 'pdRombelSmaSwasta' => 13, 'pdTerdaftarSmkNegeri' => 14, 'pdTerdaftarSmkSwasta' => 15, 'pdRombelSmkNegeri' => 16, 'pdRombelSmkSwasta' => 17, 'pdTerdaftarSmlbNegeri' => 18, 'pdTerdaftarSmlbSwasta' => 19, 'pdRombelSmlbNegeri' => 20, 'pdRombelSmlbSwasta' => 21, ),
        BasePeer::TYPE_COLNAME => array (JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID => 0, JumlahPdStatusPeer::TANGGAL => 1, JumlahPdStatusPeer::ID_LEVEL_WILAYAH => 2, JumlahPdStatusPeer::KODE_WILAYAH => 3, JumlahPdStatusPeer::MST_KODE_WILAYAH => 4, JumlahPdStatusPeer::TAHUN_AJARAN_ID => 5, JumlahPdStatusPeer::PD_TERDAFTAR_NEGERI => 6, JumlahPdStatusPeer::PD_TERDAFTAR_SWASTA => 7, JumlahPdStatusPeer::PD_ROMBEL_NEGERI => 8, JumlahPdStatusPeer::PD_ROMBEL_SWASTA => 9, JumlahPdStatusPeer::PD_TERDAFTAR_SMA_NEGERI => 10, JumlahPdStatusPeer::PD_TERDAFTAR_SMA_SWASTA => 11, JumlahPdStatusPeer::PD_ROMBEL_SMA_NEGERI => 12, JumlahPdStatusPeer::PD_ROMBEL_SMA_SWASTA => 13, JumlahPdStatusPeer::PD_TERDAFTAR_SMK_NEGERI => 14, JumlahPdStatusPeer::PD_TERDAFTAR_SMK_SWASTA => 15, JumlahPdStatusPeer::PD_ROMBEL_SMK_NEGERI => 16, JumlahPdStatusPeer::PD_ROMBEL_SMK_SWASTA => 17, JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_NEGERI => 18, JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_SWASTA => 19, JumlahPdStatusPeer::PD_ROMBEL_SMLB_NEGERI => 20, JumlahPdStatusPeer::PD_ROMBEL_SMLB_SWASTA => 21, ),
        BasePeer::TYPE_RAW_COLNAME => array ('JUMLAH_PD_STATUS_ID' => 0, 'TANGGAL' => 1, 'ID_LEVEL_WILAYAH' => 2, 'KODE_WILAYAH' => 3, 'MST_KODE_WILAYAH' => 4, 'TAHUN_AJARAN_ID' => 5, 'PD_TERDAFTAR_NEGERI' => 6, 'PD_TERDAFTAR_SWASTA' => 7, 'PD_ROMBEL_NEGERI' => 8, 'PD_ROMBEL_SWASTA' => 9, 'PD_TERDAFTAR_SMA_NEGERI' => 10, 'PD_TERDAFTAR_SMA_SWASTA' => 11, 'PD_ROMBEL_SMA_NEGERI' => 12, 'PD_ROMBEL_SMA_SWASTA' => 13, 'PD_TERDAFTAR_SMK_NEGERI' => 14, 'PD_TERDAFTAR_SMK_SWASTA' => 15, 'PD_ROMBEL_SMK_NEGERI' => 16, 'PD_ROMBEL_SMK_SWASTA' => 17, 'PD_TERDAFTAR_SMLB_NEGERI' => 18, 'PD_TERDAFTAR_SMLB_SWASTA' => 19, 'PD_ROMBEL_SMLB_NEGERI' => 20, 'PD_ROMBEL_SMLB_SWASTA' => 21, ),
        BasePeer::TYPE_FIELDNAME => array ('jumlah_pd_status_id' => 0, 'tanggal' => 1, 'id_level_wilayah' => 2, 'kode_wilayah' => 3, 'mst_kode_wilayah' => 4, 'tahun_ajaran_id' => 5, 'pd_terdaftar_negeri' => 6, 'pd_terdaftar_swasta' => 7, 'pd_rombel_negeri' => 8, 'pd_rombel_swasta' => 9, 'pd_terdaftar_sma_negeri' => 10, 'pd_terdaftar_sma_swasta' => 11, 'pd_rombel_sma_negeri' => 12, 'pd_rombel_sma_swasta' => 13, 'pd_terdaftar_smk_negeri' => 14, 'pd_terdaftar_smk_swasta' => 15, 'pd_rombel_smk_negeri' => 16, 'pd_rombel_smk_swasta' => 17, 'pd_terdaftar_smlb_negeri' => 18, 'pd_terdaftar_smlb_swasta' => 19, 'pd_rombel_smlb_negeri' => 20, 'pd_rombel_smlb_swasta' => 21, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = JumlahPdStatusPeer::getFieldNames($toType);
        $key = isset(JumlahPdStatusPeer::$fieldKeys[$fromType][$name]) ? JumlahPdStatusPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(JumlahPdStatusPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, JumlahPdStatusPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return JumlahPdStatusPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. JumlahPdStatusPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(JumlahPdStatusPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID);
            $criteria->addSelectColumn(JumlahPdStatusPeer::TANGGAL);
            $criteria->addSelectColumn(JumlahPdStatusPeer::ID_LEVEL_WILAYAH);
            $criteria->addSelectColumn(JumlahPdStatusPeer::KODE_WILAYAH);
            $criteria->addSelectColumn(JumlahPdStatusPeer::MST_KODE_WILAYAH);
            $criteria->addSelectColumn(JumlahPdStatusPeer::TAHUN_AJARAN_ID);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SMA_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SMA_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SMA_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SMK_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SMK_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SMK_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_TERDAFTAR_SMLB_SWASTA);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SMLB_NEGERI);
            $criteria->addSelectColumn(JumlahPdStatusPeer::PD_ROMBEL_SMLB_SWASTA);
        } else {
            $criteria->addSelectColumn($alias . '.jumlah_pd_status_id');
            $criteria->addSelectColumn($alias . '.tanggal');
            $criteria->addSelectColumn($alias . '.id_level_wilayah');
            $criteria->addSelectColumn($alias . '.kode_wilayah');
            $criteria->addSelectColumn($alias . '.mst_kode_wilayah');
            $criteria->addSelectColumn($alias . '.tahun_ajaran_id');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_negeri');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_swasta');
            $criteria->addSelectColumn($alias . '.pd_rombel_negeri');
            $criteria->addSelectColumn($alias . '.pd_rombel_swasta');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_sma_negeri');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_sma_swasta');
            $criteria->addSelectColumn($alias . '.pd_rombel_sma_negeri');
            $criteria->addSelectColumn($alias . '.pd_rombel_sma_swasta');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_smk_negeri');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_smk_swasta');
            $criteria->addSelectColumn($alias . '.pd_rombel_smk_negeri');
            $criteria->addSelectColumn($alias . '.pd_rombel_smk_swasta');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_smlb_negeri');
            $criteria->addSelectColumn($alias . '.pd_terdaftar_smlb_swasta');
            $criteria->addSelectColumn($alias . '.pd_rombel_smlb_negeri');
            $criteria->addSelectColumn($alias . '.pd_rombel_smlb_swasta');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(JumlahPdStatusPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            JumlahPdStatusPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(JumlahPdStatusPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 JumlahPdStatus
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = JumlahPdStatusPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return JumlahPdStatusPeer::populateObjects(JumlahPdStatusPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            JumlahPdStatusPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(JumlahPdStatusPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      JumlahPdStatus $obj A JumlahPdStatus object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getJumlahPdStatusId();
            } // if key === null
            JumlahPdStatusPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A JumlahPdStatus object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof JumlahPdStatus) {
                $key = (string) $value->getJumlahPdStatusId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or JumlahPdStatus object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(JumlahPdStatusPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   JumlahPdStatus Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(JumlahPdStatusPeer::$instances[$key])) {
                return JumlahPdStatusPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (JumlahPdStatusPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        JumlahPdStatusPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to jumlah_pd_status
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = JumlahPdStatusPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = JumlahPdStatusPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = JumlahPdStatusPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                JumlahPdStatusPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (JumlahPdStatus object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = JumlahPdStatusPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = JumlahPdStatusPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + JumlahPdStatusPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = JumlahPdStatusPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            JumlahPdStatusPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(JumlahPdStatusPeer::DATABASE_NAME)->getTable(JumlahPdStatusPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseJumlahPdStatusPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseJumlahPdStatusPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new JumlahPdStatusTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return JumlahPdStatusPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a JumlahPdStatus or Criteria object.
     *
     * @param      mixed $values Criteria or JumlahPdStatus object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from JumlahPdStatus object
        }

        if ($criteria->containsKey(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID) && $criteria->keyContainsValue(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(JumlahPdStatusPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a JumlahPdStatus or Criteria object.
     *
     * @param      mixed $values Criteria or JumlahPdStatus object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(JumlahPdStatusPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID);
            $value = $criteria->remove(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID);
            if ($value) {
                $selectCriteria->add(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(JumlahPdStatusPeer::TABLE_NAME);
            }

        } else { // $values is JumlahPdStatus object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(JumlahPdStatusPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the jumlah_pd_status table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(JumlahPdStatusPeer::TABLE_NAME, $con, JumlahPdStatusPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            JumlahPdStatusPeer::clearInstancePool();
            JumlahPdStatusPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a JumlahPdStatus or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or JumlahPdStatus object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            JumlahPdStatusPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof JumlahPdStatus) { // it's a model object
            // invalidate the cache for this single object
            JumlahPdStatusPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(JumlahPdStatusPeer::DATABASE_NAME);
            $criteria->add(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                JumlahPdStatusPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(JumlahPdStatusPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            JumlahPdStatusPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given JumlahPdStatus object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      JumlahPdStatus $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(JumlahPdStatusPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(JumlahPdStatusPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(JumlahPdStatusPeer::DATABASE_NAME, JumlahPdStatusPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return JumlahPdStatus
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = JumlahPdStatusPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(JumlahPdStatusPeer::DATABASE_NAME);
        $criteria->add(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $pk);

        $v = JumlahPdStatusPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return JumlahPdStatus[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(JumlahPdStatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(JumlahPdStatusPeer::DATABASE_NAME);
            $criteria->add(JumlahPdStatusPeer::JUMLAH_PD_STATUS_ID, $pks, Criteria::IN);
            $objs = JumlahPdStatusPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseJumlahPdStatusPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseJumlahPdStatusPeer::buildTableMap();

