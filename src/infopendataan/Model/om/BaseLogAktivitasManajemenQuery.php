<?php

namespace infopendataan\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use infopendataan\Model\LogAktivitasManajemen;
use infopendataan\Model\LogAktivitasManajemenPeer;
use infopendataan\Model\LogAktivitasManajemenQuery;

/**
 * Base class that represents a query for the 'log_aktivitas_manajemen' table.
 *
 * 
 *
 * @method LogAktivitasManajemenQuery orderByLogAktivitasManajemenId($order = Criteria::ASC) Order by the LOG_AKTIVITAS_MANAJEMEN_ID column
 * @method LogAktivitasManajemenQuery orderByKodeWilayah($order = Criteria::ASC) Order by the KODE_WILAYAH column
 * @method LogAktivitasManajemenQuery orderByPenggunaId($order = Criteria::ASC) Order by the PENGGUNA_ID column
 * @method LogAktivitasManajemenQuery orderByTanggal($order = Criteria::ASC) Order by the TANGGAL column
 * @method LogAktivitasManajemenQuery orderByIp($order = Criteria::ASC) Order by the IP column
 * @method LogAktivitasManajemenQuery orderByDeskripsi($order = Criteria::ASC) Order by the DESKRIPSI column
 * @method LogAktivitasManajemenQuery orderByTabel($order = Criteria::ASC) Order by the TABEL column
 *
 * @method LogAktivitasManajemenQuery groupByLogAktivitasManajemenId() Group by the LOG_AKTIVITAS_MANAJEMEN_ID column
 * @method LogAktivitasManajemenQuery groupByKodeWilayah() Group by the KODE_WILAYAH column
 * @method LogAktivitasManajemenQuery groupByPenggunaId() Group by the PENGGUNA_ID column
 * @method LogAktivitasManajemenQuery groupByTanggal() Group by the TANGGAL column
 * @method LogAktivitasManajemenQuery groupByIp() Group by the IP column
 * @method LogAktivitasManajemenQuery groupByDeskripsi() Group by the DESKRIPSI column
 * @method LogAktivitasManajemenQuery groupByTabel() Group by the TABEL column
 *
 * @method LogAktivitasManajemenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LogAktivitasManajemenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LogAktivitasManajemenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LogAktivitasManajemen findOne(PropelPDO $con = null) Return the first LogAktivitasManajemen matching the query
 * @method LogAktivitasManajemen findOneOrCreate(PropelPDO $con = null) Return the first LogAktivitasManajemen matching the query, or a new LogAktivitasManajemen object populated from the query conditions when no match is found
 *
 * @method LogAktivitasManajemen findOneByKodeWilayah(string $KODE_WILAYAH) Return the first LogAktivitasManajemen filtered by the KODE_WILAYAH column
 * @method LogAktivitasManajemen findOneByPenggunaId(string $PENGGUNA_ID) Return the first LogAktivitasManajemen filtered by the PENGGUNA_ID column
 * @method LogAktivitasManajemen findOneByTanggal(string $TANGGAL) Return the first LogAktivitasManajemen filtered by the TANGGAL column
 * @method LogAktivitasManajemen findOneByIp(string $IP) Return the first LogAktivitasManajemen filtered by the IP column
 * @method LogAktivitasManajemen findOneByDeskripsi(string $DESKRIPSI) Return the first LogAktivitasManajemen filtered by the DESKRIPSI column
 * @method LogAktivitasManajemen findOneByTabel(string $TABEL) Return the first LogAktivitasManajemen filtered by the TABEL column
 *
 * @method array findByLogAktivitasManajemenId(int $LOG_AKTIVITAS_MANAJEMEN_ID) Return LogAktivitasManajemen objects filtered by the LOG_AKTIVITAS_MANAJEMEN_ID column
 * @method array findByKodeWilayah(string $KODE_WILAYAH) Return LogAktivitasManajemen objects filtered by the KODE_WILAYAH column
 * @method array findByPenggunaId(string $PENGGUNA_ID) Return LogAktivitasManajemen objects filtered by the PENGGUNA_ID column
 * @method array findByTanggal(string $TANGGAL) Return LogAktivitasManajemen objects filtered by the TANGGAL column
 * @method array findByIp(string $IP) Return LogAktivitasManajemen objects filtered by the IP column
 * @method array findByDeskripsi(string $DESKRIPSI) Return LogAktivitasManajemen objects filtered by the DESKRIPSI column
 * @method array findByTabel(string $TABEL) Return LogAktivitasManajemen objects filtered by the TABEL column
 *
 * @package    propel.generator.infopendataan.Model.om
 */
abstract class BaseLogAktivitasManajemenQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLogAktivitasManajemenQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'infopendataan', $modelName = 'infopendataan\\Model\\LogAktivitasManajemen', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LogAktivitasManajemenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LogAktivitasManajemenQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LogAktivitasManajemenQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LogAktivitasManajemenQuery) {
            return $criteria;
        }
        $query = new LogAktivitasManajemenQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   LogAktivitasManajemen|LogAktivitasManajemen[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LogAktivitasManajemenPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LogAktivitasManajemenPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogAktivitasManajemen A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByLogAktivitasManajemenId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 LogAktivitasManajemen A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `LOG_AKTIVITAS_MANAJEMEN_ID`, `KODE_WILAYAH`, `PENGGUNA_ID`, `TANGGAL`, `IP`, `DESKRIPSI`, `TABEL` FROM `log_aktivitas_manajemen` WHERE `LOG_AKTIVITAS_MANAJEMEN_ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new LogAktivitasManajemen();
            $obj->hydrate($row);
            LogAktivitasManajemenPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return LogAktivitasManajemen|LogAktivitasManajemen[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|LogAktivitasManajemen[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogAktivitasManajemenPeer::LOG_AKTIVITAS_MANAJEMEN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogAktivitasManajemenPeer::LOG_AKTIVITAS_MANAJEMEN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LOG_AKTIVITAS_MANAJEMEN_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByLogAktivitasManajemenId(1234); // WHERE LOG_AKTIVITAS_MANAJEMEN_ID = 1234
     * $query->filterByLogAktivitasManajemenId(array(12, 34)); // WHERE LOG_AKTIVITAS_MANAJEMEN_ID IN (12, 34)
     * $query->filterByLogAktivitasManajemenId(array('min' => 12)); // WHERE LOG_AKTIVITAS_MANAJEMEN_ID >= 12
     * $query->filterByLogAktivitasManajemenId(array('max' => 12)); // WHERE LOG_AKTIVITAS_MANAJEMEN_ID <= 12
     * </code>
     *
     * @param     mixed $logAktivitasManajemenId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByLogAktivitasManajemenId($logAktivitasManajemenId = null, $comparison = null)
    {
        if (is_array($logAktivitasManajemenId)) {
            $useMinMax = false;
            if (isset($logAktivitasManajemenId['min'])) {
                $this->addUsingAlias(LogAktivitasManajemenPeer::LOG_AKTIVITAS_MANAJEMEN_ID, $logAktivitasManajemenId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logAktivitasManajemenId['max'])) {
                $this->addUsingAlias(LogAktivitasManajemenPeer::LOG_AKTIVITAS_MANAJEMEN_ID, $logAktivitasManajemenId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::LOG_AKTIVITAS_MANAJEMEN_ID, $logAktivitasManajemenId, $comparison);
    }

    /**
     * Filter the query on the KODE_WILAYAH column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE KODE_WILAYAH = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE KODE_WILAYAH LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the PENGGUNA_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE PENGGUNA_ID = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE PENGGUNA_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the TANGGAL column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE TANGGAL = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE TANGGAL > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(LogAktivitasManajemenPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(LogAktivitasManajemenPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the IP column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE IP = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE IP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the DESKRIPSI column
     *
     * Example usage:
     * <code>
     * $query->filterByDeskripsi('fooValue');   // WHERE DESKRIPSI = 'fooValue'
     * $query->filterByDeskripsi('%fooValue%'); // WHERE DESKRIPSI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deskripsi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByDeskripsi($deskripsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deskripsi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $deskripsi)) {
                $deskripsi = str_replace('*', '%', $deskripsi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::DESKRIPSI, $deskripsi, $comparison);
    }

    /**
     * Filter the query on the TABEL column
     *
     * Example usage:
     * <code>
     * $query->filterByTabel('fooValue');   // WHERE TABEL = 'fooValue'
     * $query->filterByTabel('%fooValue%'); // WHERE TABEL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tabel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function filterByTabel($tabel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tabel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tabel)) {
                $tabel = str_replace('*', '%', $tabel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LogAktivitasManajemenPeer::TABEL, $tabel, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   LogAktivitasManajemen $logAktivitasManajemen Object to remove from the list of results
     *
     * @return LogAktivitasManajemenQuery The current query, for fluid interface
     */
    public function prune($logAktivitasManajemen = null)
    {
        if ($logAktivitasManajemen) {
            $this->addUsingAlias(LogAktivitasManajemenPeer::LOG_AKTIVITAS_MANAJEMEN_ID, $logAktivitasManajemen->getLogAktivitasManajemenId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
