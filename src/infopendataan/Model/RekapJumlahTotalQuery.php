<?php

namespace infopendataan\Model;

use infopendataan\Model\om\BaseRekapJumlahTotalQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'rekap_jumlah_total' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.infopendataan.Model
 */
class RekapJumlahTotalQuery extends BaseRekapJumlahTotalQuery
{
}
