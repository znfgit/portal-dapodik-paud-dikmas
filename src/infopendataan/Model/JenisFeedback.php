<?php

namespace infopendataan\Model;

use infopendataan\Model\om\BaseJenisFeedback;


/**
 * Skeleton subclass for representing a row from the 'jenis_feedback' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.infopendataan.Model
 */
class JenisFeedback extends BaseJenisFeedback
{
}
