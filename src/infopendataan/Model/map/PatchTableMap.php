<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'patch' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class PatchTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.PatchTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('patch');
        $this->setPhpName('Patch');
        $this->setClassname('infopendataan\\Model\\Patch');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('PATCH_ID', 'PatchId', 'INTEGER', true, null, null);
        $this->addForeignKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', 'pengguna', 'PENGGUNA_ID', false, null, null);
        $this->addForeignKey('INSTALLER_ID', 'InstallerId', 'INTEGER', 'installer', 'INSTALLER_ID', false, null, null);
        $this->addColumn('NAMA', 'Nama', 'VARCHAR', false, 100, null);
        $this->addColumn('FILE', 'File', 'VARCHAR', false, 100, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('VERSI', 'Versi', 'VARCHAR', false, 20, null);
        $this->addColumn('ACTIVE', 'Active', 'INTEGER', false, null, null);
        $this->addColumn('DESKRIPSI', 'Deskripsi', 'VARCHAR', false, 5000, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pengguna', 'infopendataan\\Model\\Pengguna', RelationMap::MANY_TO_ONE, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Installer', 'infopendataan\\Model\\Installer', RelationMap::MANY_TO_ONE, array('INSTALLER_ID' => 'INSTALLER_ID', ), 'CASCADE', 'CASCADE');
        $this->addRelation('LogDownloadPatch', 'infopendataan\\Model\\LogDownloadPatch', RelationMap::ONE_TO_MANY, array('PATCH_ID' => 'PATCH_ID', ), null, null, 'LogDownloadPatchs');
    } // buildRelations()

} // PatchTableMap
