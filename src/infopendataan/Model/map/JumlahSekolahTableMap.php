<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jumlah_sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class JumlahSekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.JumlahSekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jumlah_sekolah');
        $this->setPhpName('JumlahSekolah');
        $this->setClassname('infopendataan\\Model\\JumlahSekolah');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('jumlah_sekolah_id', 'JumlahSekolahId', 'INTEGER', true, null, null);
        $this->addColumn('tanggal', 'Tanggal', 'VARCHAR', false, 20, null);
        $this->addColumn('id_level_wilayah', 'IdLevelWilayah', 'VARCHAR', false, 3, null);
        $this->addColumn('kode_wilayah', 'KodeWilayah', 'VARCHAR', false, 10, null);
        $this->addColumn('mst_kode_wilayah', 'MstKodeWilayah', 'VARCHAR', false, 10, null);
        $this->addColumn('semester_id', 'SemesterId', 'VARCHAR', false, 10, null);
        $this->addColumn('status_negeri', 'StatusNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('status_swasta', 'StatusSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('status_sma_negeri', 'StatusSmaNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('status_sma_swasta', 'StatusSmaSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('status_smk_negeri', 'StatusSmkNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('status_smk_swasta', 'StatusSmkSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('status_smlb_negeri', 'StatusSmlbNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('status_smlb_swasta', 'StatusSmlbSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('waktu_penyelenggaraan_pagi', 'WaktuPenyelenggaraanPagi', 'DOUBLE', false, null, null);
        $this->addColumn('waktu_penyelenggaraan_siang', 'WaktuPenyelenggaraanSiang', 'DOUBLE', false, null, null);
        $this->addColumn('waktu_penyelenggaraan_sore', 'WaktuPenyelenggaraanSore', 'DOUBLE', false, null, null);
        $this->addColumn('waktu_penyelenggaraan_malam', 'WaktuPenyelenggaraanMalam', 'DOUBLE', false, null, null);
        $this->addColumn('waktu_penyelenggaraan_kombinasi', 'WaktuPenyelenggaraanKombinasi', 'DOUBLE', false, null, null);
        $this->addColumn('waktu_penyelenggaraan_lainnya', 'WaktuPenyelenggaraanLainnya', 'DOUBLE', false, null, null);
        $this->addColumn('akreditasi_a', 'AkreditasiA', 'DOUBLE', false, null, null);
        $this->addColumn('akreditasi_b', 'AkreditasiB', 'DOUBLE', false, null, null);
        $this->addColumn('akreditasi_c', 'AkreditasiC', 'DOUBLE', false, null, null);
        $this->addColumn('akreditasi_tidak', 'AkreditasiTidak', 'DOUBLE', false, null, null);
        $this->addColumn('listrik_ada', 'ListrikAda', 'DOUBLE', false, null, null);
        $this->addColumn('listrik_tidak', 'ListrikTidak', 'DOUBLE', false, null, null);
        $this->addColumn('sanitasi_ada', 'SanitasiAda', 'DOUBLE', false, null, null);
        $this->addColumn('sanitasi_tidak', 'SanitasiTidak', 'DOUBLE', false, null, null);
        $this->addColumn('akses_internet_ada', 'AksesInternetAda', 'DOUBLE', false, null, null);
        $this->addColumn('akses_internet_tidak', 'AksesInternetTidak', 'DOUBLE', false, null, null);
        $this->addColumn('mbs_ada', 'MbsAda', 'DOUBLE', false, null, null);
        $this->addColumn('mbs_tidak', 'MbsTidak', 'DOUBLE', false, null, null);
        $this->addColumn('wilayah_khusus_ada', 'WilayahKhususAda', 'DOUBLE', false, null, null);
        $this->addColumn('wilayah_khusus_tidak', 'WilayahKhususTidak', 'DOUBLE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // JumlahSekolahTableMap
