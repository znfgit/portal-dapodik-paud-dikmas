<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jumlah_pd' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class JumlahPdTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.JumlahPdTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jumlah_pd');
        $this->setPhpName('JumlahPd');
        $this->setClassname('infopendataan\\Model\\JumlahPd');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('jumlah_pd_id', 'JumlahPdId', 'INTEGER', true, null, null);
        $this->addColumn('tanggal', 'Tanggal', 'TIMESTAMP', false, null, null);
        $this->addColumn('id_level_wilayah', 'IdLevelWilayah', 'VARCHAR', false, 5, null);
        $this->addColumn('kode_wilayah', 'KodeWilayah', 'VARCHAR', false, 10, null);
        $this->addColumn('mst_kode_wilayah', 'MstKodeWilayah', 'VARCHAR', false, 10, null);
        $this->addColumn('tahun_ajaran_id', 'TahunAjaranId', 'VARCHAR', false, 255, null);
        $this->addColumn('pd_di_rombel', 'PdDiRombel', 'DOUBLE', false, null, null);
        $this->addColumn('pd_belum_di_rombel', 'PdBelumDiRombel', 'DOUBLE', false, null, null);
        $this->addColumn('pd_total', 'PdTotal', 'DOUBLE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // JumlahPdTableMap
