<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jumlah_pd_status' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class JumlahPdStatusTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.JumlahPdStatusTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jumlah_pd_status');
        $this->setPhpName('JumlahPdStatus');
        $this->setClassname('infopendataan\\Model\\JumlahPdStatus');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('jumlah_pd_status_id', 'JumlahPdStatusId', 'INTEGER', true, null, null);
        $this->addColumn('tanggal', 'Tanggal', 'VARCHAR', false, 20, null);
        $this->addColumn('id_level_wilayah', 'IdLevelWilayah', 'DOUBLE', false, null, null);
        $this->addColumn('kode_wilayah', 'KodeWilayah', 'VARCHAR', false, 10, null);
        $this->addColumn('mst_kode_wilayah', 'MstKodeWilayah', 'VARCHAR', false, 10, null);
        $this->addColumn('tahun_ajaran_id', 'TahunAjaranId', 'VARCHAR', true, 10, null);
        $this->addColumn('pd_terdaftar_negeri', 'PdTerdaftarNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_swasta', 'PdTerdaftarSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_negeri', 'PdRombelNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_swasta', 'PdRombelSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_sma_negeri', 'PdTerdaftarSmaNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_sma_swasta', 'PdTerdaftarSmaSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_sma_negeri', 'PdRombelSmaNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_sma_swasta', 'PdRombelSmaSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_smk_negeri', 'PdTerdaftarSmkNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_smk_swasta', 'PdTerdaftarSmkSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_smk_negeri', 'PdRombelSmkNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_smk_swasta', 'PdRombelSmkSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_smlb_negeri', 'PdTerdaftarSmlbNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_terdaftar_smlb_swasta', 'PdTerdaftarSmlbSwasta', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_smlb_negeri', 'PdRombelSmlbNegeri', 'DOUBLE', false, null, null);
        $this->addColumn('pd_rombel_smlb_swasta', 'PdRombelSmlbSwasta', 'DOUBLE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // JumlahPdStatusTableMap
