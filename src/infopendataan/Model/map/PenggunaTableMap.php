<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pengguna' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class PenggunaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.PenggunaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pengguna');
        $this->setPhpName('Pengguna');
        $this->setClassname('infopendataan\\Model\\Pengguna');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', true, null, null);
        $this->addForeignKey('JENIS_KELAMIN_ID', 'JenisKelaminId', 'INTEGER', 'jenis_kelamin', 'JENIS_KELAMIN_ID', false, null, null);
        $this->addColumn('NAMA', 'Nama', 'VARCHAR', false, 100, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', false, 100, null);
        $this->addColumn('USERNAME', 'Username', 'VARCHAR', false, 100, null);
        $this->addColumn('PASSWORD', 'Password', 'VARCHAR', false, 100, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JenisKelamin', 'infopendataan\\Model\\JenisKelamin', RelationMap::MANY_TO_ONE, array('JENIS_KELAMIN_ID' => 'JENIS_KELAMIN_ID', ), null, null);
        $this->addRelation('Artikel', 'infopendataan\\Model\\Artikel', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'Artikels');
        $this->addRelation('Berita', 'infopendataan\\Model\\Berita', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'Beritas');
        $this->addRelation('Dokumentasi', 'infopendataan\\Model\\Dokumentasi', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'Dokumentasis');
        $this->addRelation('Faq', 'infopendataan\\Model\\Faq', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'Faqs');
        $this->addRelation('File', 'infopendataan\\Model\\File', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'Files');
        $this->addRelation('Installer', 'infopendataan\\Model\\Installer', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), 'CASCADE', 'CASCADE', 'Installers');
        $this->addRelation('LogLogin', 'infopendataan\\Model\\LogLogin', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'LogLogins');
        $this->addRelation('MediaSosial', 'infopendataan\\Model\\MediaSosial', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), null, null, 'MediaSosials');
        $this->addRelation('Patch', 'infopendataan\\Model\\Patch', RelationMap::ONE_TO_MANY, array('PENGGUNA_ID' => 'PENGGUNA_ID', ), 'CASCADE', 'CASCADE', 'Patchs');
    } // buildRelations()

} // PenggunaTableMap
