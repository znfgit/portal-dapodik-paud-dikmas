<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'log_aktivitas_manajemen' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class LogAktivitasManajemenTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.LogAktivitasManajemenTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('log_aktivitas_manajemen');
        $this->setPhpName('LogAktivitasManajemen');
        $this->setClassname('infopendataan\\Model\\LogAktivitasManajemen');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('LOG_AKTIVITAS_MANAJEMEN_ID', 'LogAktivitasManajemenId', 'INTEGER', true, null, null);
        $this->addColumn('KODE_WILAYAH', 'KodeWilayah', 'VARCHAR', true, 10, null);
        $this->addColumn('PENGGUNA_ID', 'PenggunaId', 'VARCHAR', false, 30, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('IP', 'Ip', 'VARCHAR', false, 20, null);
        $this->addColumn('DESKRIPSI', 'Deskripsi', 'VARCHAR', false, 5000, null);
        $this->addColumn('TABEL', 'Tabel', 'VARCHAR', false, 20, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // LogAktivitasManajemenTableMap
