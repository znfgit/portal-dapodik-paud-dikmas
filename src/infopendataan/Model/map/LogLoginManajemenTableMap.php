<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'log_login_manajemen' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class LogLoginManajemenTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.LogLoginManajemenTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('log_login_manajemen');
        $this->setPhpName('LogLoginManajemen');
        $this->setClassname('infopendataan\\Model\\LogLoginManajemen');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('LOG_LOGIN_MANAJEMEN_ID', 'LogLoginManajemenId', 'INTEGER', true, null, null);
        $this->addColumn('IP', 'Ip', 'VARCHAR', true, 50, null);
        $this->addColumn('BROWSER', 'Browser', 'VARCHAR', false, 50, null);
        $this->addColumn('OS', 'Os', 'VARCHAR', false, 50, null);
        $this->addColumn('KODE_WILAYAH', 'KodeWilayah', 'VARCHAR', true, 10, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('PENGGUNA_ID', 'PenggunaId', 'VARCHAR', false, 30, null);
        $this->addColumn('ID_LEVEL_WILAYAH', 'IdLevelWilayah', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // LogLoginManajemenTableMap
