<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rekap_jumlah_total' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class RekapJumlahTotalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.RekapJumlahTotalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rekap_jumlah_total');
        $this->setPhpName('RekapJumlahTotal');
        $this->setClassname('infopendataan\\Model\\RekapJumlahTotal');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('rekap_jumlah_total_id', 'RekapJumlahTotalId', 'INTEGER', true, null, null);
        $this->addColumn('tanggal', 'Tanggal', 'DATE', true, null, null);
        $this->addColumn('sekolah_nasional', 'SekolahNasional', 'DOUBLE', true, 100, null);
        $this->addColumn('ptk_nasional', 'PtkNasional', 'DOUBLE', true, 100, null);
        $this->addColumn('pd_nasional', 'PdNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('sma_nasional', 'SmaNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('smk_nasional', 'SmkNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('smlb_nasional', 'SmlbNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('ptk_sma_nasional', 'PtkSmaNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('ptk_smk_nasional', 'PtkSmkNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('ptk_smlb_nasional', 'PtkSmlbNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('pd_sma_nasional', 'PdSmaNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('pd_smk_nasional', 'PdSmkNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('pd_smlb_nasional', 'PdSmlbNasional', 'DOUBLE', false, 100, null);
        $this->addColumn('tahun_ajaran_id', 'TahunAjaranId', 'VARCHAR', false, 10, null);
        $this->addColumn('semester_id', 'SemesterId', 'VARCHAR', false, 10, null);
        $this->addColumn('update_terakhir', 'UpdateTerakhir', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // RekapJumlahTotalTableMap
