<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'log_download_installer' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class LogDownloadInstallerTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.LogDownloadInstallerTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('log_download_installer');
        $this->setPhpName('LogDownloadInstaller');
        $this->setClassname('infopendataan\\Model\\LogDownloadInstaller');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('LOG_DOWNLOAD_INSTALLER_ID', 'LogDownloadInstallerId', 'INTEGER', true, null, null);
        $this->addForeignKey('INSTALLER_ID', 'InstallerId', 'INTEGER', 'installer', 'INSTALLER_ID', false, null, null);
        $this->addColumn('IP', 'Ip', 'VARCHAR', false, 100, null);
        $this->addColumn('BROWSER', 'Browser', 'VARCHAR', false, 100, null);
        $this->addColumn('OS', 'Os', 'VARCHAR', false, 100, null);
        $this->addColumn('TANGGAL', 'Tanggal', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Installer', 'infopendataan\\Model\\Installer', RelationMap::MANY_TO_ONE, array('INSTALLER_ID' => 'INSTALLER_ID', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // LogDownloadInstallerTableMap
