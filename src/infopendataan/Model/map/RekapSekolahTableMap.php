<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rekap_sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class RekapSekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.RekapSekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rekap_sekolah');
        $this->setPhpName('RekapSekolah');
        $this->setClassname('infopendataan\\Model\\RekapSekolah');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('rekap_sekolah_id', 'RekapSekolahId', 'INTEGER', true, null, null);
        $this->addColumn('sekolah_id', 'SekolahId', 'VARCHAR', true, 100, null);
        $this->addColumn('tanggal', 'Tanggal', 'DATE', false, null, null);
        $this->addColumn('ptk', 'Ptk', 'INTEGER', false, null, null);
        $this->addColumn('pegawai', 'Pegawai', 'INTEGER', false, null, null);
        $this->addColumn('pd', 'Pd', 'INTEGER', false, null, null);
        $this->addColumn('rombel', 'Rombel', 'INTEGER', false, null, null);
        $this->addColumn('kode_wilayah_kecamatan', 'KodeWilayahKecamatan', 'VARCHAR', false, 20, null);
        $this->addColumn('kecamatan', 'Kecamatan', 'VARCHAR', false, 100, null);
        $this->addColumn('mst_kode_wilayah_kecamatan', 'MstKodeWilayahKecamatan', 'VARCHAR', false, 20, null);
        $this->addColumn('id_level_wilayah_kecamatan', 'IdLevelWilayahKecamatan', 'INTEGER', false, 5, null);
        $this->addColumn('kode_wilayah_kabupaten', 'KodeWilayahKabupaten', 'VARCHAR', false, 20, null);
        $this->addColumn('kabupaten', 'Kabupaten', 'VARCHAR', false, 100, null);
        $this->addColumn('mst_kode_wilayah_kabupaten', 'MstKodeWilayahKabupaten', 'VARCHAR', false, 20, null);
        $this->addColumn('id_level_wilayah_kabupaten', 'IdLevelWilayahKabupaten', 'INTEGER', false, null, null);
        $this->addColumn('kode_wilayah_provinsi', 'KodeWilayahProvinsi', 'VARCHAR', false, 20, null);
        $this->addColumn('provinsi', 'Provinsi', 'VARCHAR', false, 100, null);
        $this->addColumn('mst_kode_wilayah_provinsi', 'MstKodeWilayahProvinsi', 'VARCHAR', false, 20, null);
        $this->addColumn('id_level_wilayah_provinsi', 'IdLevelWilayahProvinsi', 'INTEGER', false, null, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 100, null);
        $this->addColumn('npsn', 'Npsn', 'VARCHAR', false, 100, null);
        $this->addColumn('bentuk_pendidikan_id', 'BentukPendidikanId', 'VARCHAR', false, 10, null);
        $this->addColumn('status_sekolah', 'StatusSekolah', 'VARCHAR', false, 5, null);
        $this->addColumn('guru_matematika', 'GuruMatematika', 'INTEGER', false, null, null);
        $this->addColumn('guru_bahasa_indonesia', 'GuruBahasaIndonesia', 'INTEGER', false, null, null);
        $this->addColumn('guru_bahasa_inggris', 'GuruBahasaInggris', 'INTEGER', false, null, null);
        $this->addColumn('guru_sejarah_indonesia', 'GuruSejarahIndonesia', 'INTEGER', false, null, null);
        $this->addColumn('guru_pkn', 'GuruPkn', 'INTEGER', false, null, null);
        $this->addColumn('guru_penjaskes', 'GuruPenjaskes', 'INTEGER', false, null, null);
        $this->addColumn('guru_agama_budi_pekerti', 'GuruAgamaBudiPekerti', 'INTEGER', false, null, null);
        $this->addColumn('guru_seni_budaya', 'GuruSeniBudaya', 'INTEGER', false, null, null);
        $this->addColumn('pd_kelas_10', 'PdKelas10', 'INTEGER', false, null, null);
        $this->addColumn('pd_kelas_11', 'PdKelas11', 'INTEGER', false, null, null);
        $this->addColumn('pd_kelas_12', 'PdKelas12', 'INTEGER', false, null, null);
        $this->addColumn('pd_kelas_13', 'PdKelas13', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // RekapSekolahTableMap
