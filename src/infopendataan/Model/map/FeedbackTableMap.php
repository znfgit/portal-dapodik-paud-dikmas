<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'feedback' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class FeedbackTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.FeedbackTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('feedback');
        $this->setPhpName('Feedback');
        $this->setClassname('infopendataan\\Model\\Feedback');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('FEEDBACK_ID', 'FeedbackId', 'INTEGER', true, null, null);
        $this->addForeignKey('JENIS_FEEDBACK_ID', 'JenisFeedbackId', 'INTEGER', 'jenis_feedback', 'JENIS_FEEDBACK_ID', false, null, null);
        $this->addColumn('JUDUL', 'Judul', 'VARCHAR', false, 100, null);
        $this->addColumn('DESKRIPSI', 'Deskripsi', 'VARCHAR', false, 5000, null);
        $this->addColumn('GAMBAR', 'Gambar', 'VARCHAR', false, 50, null);
        $this->addColumn('EMAIL_PENGIRIM', 'EmailPengirim', 'VARCHAR', false, 30, null);
        $this->addColumn('INSTANSI', 'Instansi', 'VARCHAR', false, 100, null);
        $this->addColumn('NOMOR_TELEPON', 'NomorTelepon', 'VARCHAR', false, 14, null);
        $this->addColumn('TANGGAL_LAPOR', 'TanggalLapor', 'TIMESTAMP', false, null, null);
        $this->addColumn('STATUS', 'Status', 'INTEGER', false, null, null);
        $this->addColumn('TANGGAL_UPDATE', 'TanggalUpdate', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JenisFeedback', 'infopendataan\\Model\\JenisFeedback', RelationMap::MANY_TO_ONE, array('JENIS_FEEDBACK_ID' => 'JENIS_FEEDBACK_ID', ), null, null);
    } // buildRelations()

} // FeedbackTableMap
