<?php

namespace infopendataan\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'mst_wilayah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.infopendataan.Model.map
 */
class MstWilayahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'infopendataan.Model.map.MstWilayahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mst_wilayah');
        $this->setPhpName('MstWilayah');
        $this->setClassname('infopendataan\\Model\\MstWilayah');
        $this->setPackage('infopendataan.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('wilayah_id', 'WilayahId', 'VARCHAR', true, 255, null);
        $this->addColumn('kode_wilayah', 'KodeWilayah', 'VARCHAR', false, 255, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 255, null);
        $this->addColumn('id_level_wilayah', 'IdLevelWilayah', 'VARCHAR', false, 255, null);
        $this->addColumn('mst_kode_wilayah', 'MstKodeWilayah', 'VARCHAR', false, 255, null);
        $this->addColumn('negara_id', 'NegaraId', 'VARCHAR', false, 255, null);
        $this->addColumn('asal_wilayah', 'AsalWilayah', 'VARCHAR', false, 255, null);
        $this->addColumn('kode_bps', 'KodeBps', 'VARCHAR', false, 255, null);
        $this->addColumn('kode_dagri', 'KodeDagri', 'VARCHAR', false, 255, null);
        $this->addColumn('kode_keu', 'KodeKeu', 'VARCHAR', false, 255, null);
        $this->addColumn('create_date', 'CreateDate', 'VARCHAR', false, 255, null);
        $this->addColumn('last_update', 'LastUpdate', 'VARCHAR', false, 255, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'VARCHAR', false, 255, null);
        $this->addColumn('last_sync', 'LastSync', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // MstWilayahTableMap
