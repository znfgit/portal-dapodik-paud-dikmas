<?php

namespace library;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Report{
	public function initdb(){
		$serverName = "172.17.3.19"; //serverName\instanceName
		$connectionInfo = array( "Database"=>"Dapodikmen", "UID"=>"UserRpt", "PWD"=>"Reportd1km3n");
		$conn = sqlsrv_connect( $serverName, $connectionInfo);

		return $conn;
	}

	public function testing(){
		$con = $this->initdb();

		$sql = "SELECT * FROM pengguna";
		$stmt = sqlsrv_query( $con, $sql );
		if( $stmt === false) {
		    die( print_r( sqlsrv_errors(), true) );
		}

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		      
		      $return_arr[] = $row;
		}

		sqlsrv_free_stmt( $stmt);

		return json_encode( $return_arr );
	}

	public function geojson(){
		$con = $this->initdb();

		$strings = 'var statesData = {
					type: "FeatureCollection",
					features: [
					';

		//looping untuk semua propinsi
		$i = 1;
		$sqlProp = "select * from ref.mst_wilayah where id_level_wilayah = 1";
		$stmtProp = sqlsrv_query( $con, $sqlProp );
		while ( $rowProp = sqlsrv_fetch_array( $stmtProp, SQLSRV_FETCH_ASSOC) ){

			$sql = "SELECT w3.nama,w3.kode_wilayah,	
					(
						(
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) + 
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END)
						) /
						CAST(
							(SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
						)
					) * 100 as persen,
				SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) AS sma,
				SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) AS smk,
				SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END) AS smlb,
				SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sma,
				SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smk,
				SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smlb
				FROM sekolah r
					INNER JOIN dbo.sekolah s ON r.sekolah_id=s.sekolah_id
					INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
					LEFT JOIN (
						SELECT 
							sekolah_id,
							MAX(begin_sync) AS begin_sync
						FROM dbo.sync_log
						WHERE alamat_ip NOT LIKE 'prefil%'
						GROUP BY sekolah_id) l ON r.sekolah_id=l.sekolah_id
				WHERE w3.id_level_wilayah=1
				AND w3.kode_wilayah = ".$rowProp['kode_wilayah']."
				GROUP BY w3.nama,w3.kode_wilayah";

				$stmt = sqlsrv_query( $con, $sql );
				$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
				$kowil = substr($row['kode_wilayah'], 0, -1);

				$geometry = file_get_contents('http://dapo.dikmen.kemdikbud.go.id/portal/src/library/geo/'.$kowil.'.txt', true);

				if(!empty($geometry)){
					$geomet = ",".file_get_contents('http://dapo.dikmen.kemdikbud.go.id/portal/src/library/geo/'.$kowil.'.txt', true);
				}else{
					$geomet = "";
				}

				$strings .= '{
				        type: "Feature",
				        id: '.$i.',
				        properties: {
				            name: "'.$row['nama'].'",
				            kode_wilayah: "'.$row['kode_wilayah'].'",
				            persen: "'.$row['persen'].'",
				            sma: "'.$row['sma'].'",
				            smk: "'.$row['smk'].'",
				            smlb: "'.$row['smlb'].'",
				            kirim_sma: "'.$row['kirim_sma'].'",
				            kirim_smk: "'.$row['kirim_smk'].'",
				            kirim_smlb: "'.$row['kirim_smlb'].'"
				        }
				        '.$geomet.'
                   },';	

				$i++;

		}
		
		$strings = substr($strings, 0, -1);

		$strings .= "]
				};";		

		return $strings;
	}

	public function getLastSync(){
		$con = $this->initdb();

		$sql = "SELECT
			x.sekolah_id,
			s.nama,
			CONVERT (
				VARCHAR (20),
				x.begin_sync,
				113
			) AS begin_sync
		FROM
			sekolah s
		INNER JOIN (
			SELECT
				TOP 10 sekolah_id,
				MAX (begin_sync) AS begin_sync
			FROM
				table_sync_log
			GROUP BY
				sekolah_id
			ORDER BY
				begin_sync DESC
		) x ON s.sekolah_id = x.sekolah_id
		ORDER BY
			x.begin_sync DESC";

		$stmt = sqlsrv_query( $con, $sql );

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		      
		      $return_arr[] = $row;
		}

		sqlsrv_free_stmt( $stmt);

		return json_encode($return_arr);

	}

}

?>