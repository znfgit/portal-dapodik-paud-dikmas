<?php

namespace library;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use infopendataan\Model as M;

use infopendataan\Model\Berita;
use infopendataan\Model\BeritaPeer;

class Rss{
	function get(Request $request, Application $app){
		$model = $request->get('model');
		$return = array();

		$con = Rest::initdbSqlServerLokal();

		switch ($model) {
			case 'berita':
				
				$sql = "select * from web.berita order by berita_id desc";
				
				$stmt = sqlsrv_query( $con, $sql );
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$arr['judul'] = $row['JUDUL'];
					$arr['tanggal'] = $row['TANGGAL'];
					$arr['isi'] = substr(strip_tags(str_replace('&nbsp;', ' ', $row['ISI'])), 0, 100);
					$arr['link'] = 'http://dapo.dikmen.kemdikbud.go.id/portal/web/laman/detailBerita/'.substr($row['TANGGAL'], 0,10).'/'.strtolower(space_char_remover($row['JUDUL']));

					array_push($return, $arr);
				}
				break;
			
			default:
				# code...
				break;
		}

		return json_encode($return);
	}
}