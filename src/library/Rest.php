<?php

namespace library;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use infopendataan\Model as M;

use infopendataan\Model\Pengguna;
use infopendataan\Model\PenggunaPeer;

use infopendataan\Model\Installer;
use infopendataan\Model\InstallerPeer;

use infopendataan\Model\LogDownloadInstaller;
use infopendataan\Model\LogDownloadInstallerPeer;

use infopendataan\Model\LogVisitor;
use infopendataan\Model\LogVisitorPeer;

use infopendataan\Model\Patch;
use infopendataan\Model\PatchPeer;

use infopendataan\Model\Faq;
use infopendataan\Model\FaqPeer;

use infopendataan\Model\File;
use infopendataan\Model\FilePeer;

use infopendataan\Model\JenisFile;
use infopendataan\Model\JenisFilePeer;

use infopendataan\Model\Dokumentasi;
use infopendataan\Model\DokumentasiPeer;

use infopendataan\Model\JenisDokumentasi;
use infopendataan\Model\JenisDokumentasiPeer;

use infopendataan\Model\JenisFeedback;
use infopendataan\Model\JenisFeedbackPeer;

use infopendataan\Model\Feedback;
use infopendataan\Model\FeedbackPeer;

use infopendataan\Model\Artikel;
use infopendataan\Model\ArtikelPeer;

use infopendataan\Model\LogDownloadPatch;
use infopendataan\Model\LogDownloadPatchPeer;

use infopendataan\Model\Berita;
use infopendataan\Model\BeritaPeer;

use infopendataan\Model\LogLoginManajemen;
use infopendataan\Model\LogLoginManajemenPeer;

use infopendataan\Model\LogAktivitasManajemen;
use infopendataan\Model\LogAktivitasManajemenPeer;

use infopendataan\Model\RekapJumlahTotal;
use infopendataan\Model\RekapJumlahTotalPeer;

use infopendataan\Model\JumlahPd;
use infopendataan\Model\JumlahPdPeer;

use infopendataan\Model\JumlahPdStatus;
use infopendataan\Model\JumlahPdStatusPeer;

use infopendataan\Model\JumlahSekolah;
use infopendataan\Model\JumlahSekolahPeer;

use infopendataan\Model\RekapSekolah;
use infopendataan\Model\RekapSekolahPeer;

use infopendataan\Model\SekolahHash;
use infopendataan\Model\SekolahHashPeer;

class Rest
{
	public function getToday(){
		return date('Y-m-d');
	}

	public function initdb(){
		$serverName = "223.27.152.200"; //serverName\instanceName
		$connectionInfo = array( "Database"=>"dapodikdasmen_batam", "UID"=>"sa", "PWD"=>"r00t_db",'ReturnDatesAsStrings'=>true);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);

		return $conn;
	}

	public function initDbMysql(){
		$con_mysql=mysql_connect("localhost","root","acikiwir");
		$db = mysql_select_db('infopendataan', $con_mysql);

		return $db;
	}

	public function initdbSqlServerLokal(){
		$serverName = "223.27.152.200";
		$connectionInfo = array( "Database"=>"rekap_dapodikpauddikmas", "UID"=>"sa", "PWD"=>"r00t_db",'ReturnDatesAsStrings'=>true);
		/* Connect using Windows Authentication. */
		$con_rekap = sqlsrv_connect( $serverName, $connectionInfo);

		return $con_rekap;
	}

	public function getUnHash($sekolah_id){
		$con = $this->initdbSqlServerLokal();

		$sql = "select sekolah_id from sekolah_hash where hash = '{$sekolah_id}'";

		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return $row['sekolah_id'];
	}



	public function getLastSync(){
		$hari_ini = $this->getToday();

		// $filename = 'cache/getLastSync/getLastSync_'.$hari_ini;

  //       if (!file_exists($filename)){

        	$con = $this->initdb();

			$sql = "SELECT
				TOP 10 b.nama, b.sekolah_id,
				CONVERT (
					VARCHAR (20),
					MAX (begin_sync),
					113
				) AS tgl
			FROM
				dbo.sync_log a
			JOIN dbo.sekolah b ON a.sekolah_id = b.sekolah_id
			WHERE
				end_sync IS NOT NULL
				-- and alamat_ip NOT LIKE 'prefil%'
				and sync_media=1
			GROUP BY
				b.nama, b.sekolah_id
			ORDER BY
				MAX (begin_sync) DESC";
					
			$stmt = sqlsrv_query( $con, $sql );

			$templates = '<table class="table table-striped table-hover ">
			                <thead>
			                  <tr>
			                    <th>#</th>
			                    <th>Nama Sekolah</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';
			  
			$i = 1;

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

				$enkrip = $row['sekolah_id'];

				$sql_enkrip = "SELECT
						CONVERT (
							VARCHAR (20),
							HashBytes ('MD5', CAST('".$enkrip."' AS VARCHAR(50))),
							2
						) AS enkripsi";
				$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
				$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);
			      
			      //$return_arr[] = $row;
				$templates .= '<tr>
			                    <td>'.$i.'</td>
			                    <td><a href="laman/datasekolah/'.$row_enkrip['enkripsi'].'"><b>'.$row["nama"].'</b></a><br>'.$row["tgl"].'</td>
			                  </tr>';

			    $i++;
			}

			$templates .= ' </tbody>
			              </table>';

			sqlsrv_free_stmt( $stmt);

			// return $templates;
			$templates .= "<div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div>";
			
        // 	file_put_contents($filename, $templates);
        // }

        // $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $templates;

	}

	public function validasiKoreg(Request $request, Application $app){
		$koreg = $request->get('koreg');
		$sekolah_id = $request->get('sekolah_id');

		$con = $this->initdb();

		$sql = "SELECT
				kode_registrasi,
				nama,
				sekolah_id
			FROM
				dbo.sekolah
			WHERE
				CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST(sekolah_id AS VARCHAR(50))),
					2
				) = '".$sekolah_id."'";

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		if($row){
			$koreggen 	= strtoupper(base_convert($row['kode_registrasi'], 10, 32));

			//return $koreggen;

			if($koreggen == $koreg){
				return '{"success" : true}';
			}else{
				return '{"success" : false}';
			}
		}

	}

	public function breadcrumbProp(Request $request, Application $app){
		
		$kode_wilayah = $request->get('kode_wilayah');
		$hari_ini = $this->getToday();

		$filename = 'cache/breadcrumbProp/breadcrumbProp_'.$kode_wilayah.'_'.$hari_ini;

        if (!file_exists($filename)){
        	$con = $this->initdb();

			$sql = "select nama from ref.mst_wilayah where kode_wilayah = ".$kode_wilayah;

			$stmt = sqlsrv_query( $con, $sql );

			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			// return $row['nama'];

			// $templates .= "<div style=font-size:10px>cache per tanggal ".date('Y-m-d H:i:s')."</div>";
			
        	file_put_contents($filename, $row['nama']);
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;

	}

	public function getRekapAll3(Request $request, Application $app){
		$con = $this->initdbSqlServerLokal();

		// if($con){
		// 	return 'oke';
		// }else{
		// 	return 'nggak oke';
		// }
		$hari_ini_banget = date('Y-m-d');

		$semester_id = $request->get('semester_id') ? $request->get('semester_id') : 20142;
		$tahun_ajaran_id = substr($semester_id, 0,4);
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : '000000';
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : 0;

		switch ($id_level_wilayah) {
			case 0:
				$params_lv = 'and s.mst_kode_wilayah_provinsi = '.$kode_wilayah;
				break;
			case 1:
				$params_lv = 'and s.mst_kode_wilayah_kabupaten = '.$kode_wilayah;
				break;
			case 2:
				$params_lv = 'and s.mst_kode_wilayah_kecamatan = '.$kode_wilayah;
				break;
			default:
				# code...
				break;
		}
		

		$sql = "select 
					sum(1) as sekolah_total,
					sum(pd) as pd_total,
					sum(ptk) as guru_total,
					sum(pegawai) as pegawai_total,
					sum(rombel) as rombel_total,
					sum(case when bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
					sum(case when bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
					sum(case when bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
					sum(case when bentuk_pendidikan_id = 29 then 1 else 0 end) as slb,
					sum(case when bentuk_pendidikan_id = 13 then ptk else 0 end) as guru_sma,
					sum(case when bentuk_pendidikan_id = 15 then ptk else 0 end) as guru_smk,
					sum(case when bentuk_pendidikan_id = 14 then ptk else 0 end) as guru_smlb,
					sum(case when bentuk_pendidikan_id = 29 then ptk else 0 end) as guru_slb,
					sum(case when bentuk_pendidikan_id = 13 then pegawai else 0 end) as pegawai_sma,
					sum(case when bentuk_pendidikan_id = 15 then pegawai else 0 end) as pegawai_smk,
					sum(case when bentuk_pendidikan_id = 14 then pegawai else 0 end) as pegawai_smlb,
					sum(case when bentuk_pendidikan_id = 29 then pegawai else 0 end) as pegawai_slb,
					sum(case when bentuk_pendidikan_id = 13 then pd else 0 end) as pd_sma,
					sum(case when bentuk_pendidikan_id = 15 then pd else 0 end) as pd_smk,
					sum(case when bentuk_pendidikan_id = 14 then pd else 0 end) as pd_smlb,
					sum(case when bentuk_pendidikan_id = 29 then pd else 0 end) as pd_slb,
					sum(case when bentuk_pendidikan_id = 13 then rombel else 0 end) as rombel_sma,
					sum(case when bentuk_pendidikan_id = 15 then rombel else 0 end) as rombel_smk,
					sum(case when bentuk_pendidikan_id = 14 then rombel else 0 end) as rombel_smlb,
					sum(case when bentuk_pendidikan_id = 29 then rombel else 0 end) as rombel_slb
				from 
					rekap_sekolah 
				where 
					semester_id = {$semester_id}";

					// return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		// return var_dump($row);

		// return "tes broh";die;

		if($semester_id == 20141){
			$semester = 'Semester 2014/2015 Ganjil';
		}else if($semester_id = 20142){
			$semester = 'Semester 2014/2015 Genap';
		}else if($semester_id == 20132){
			$semester = 'Semester 2013/2014 Genap';
		}
		
		$templates = '<div style="width:100%;margin:auto;text-align:center;padding-bottom:10px;font-size:14px">Rekap Nasional <b>'.$semester.'</b> <br>Per '.$hari_ini_banget.' 00:00:00</b></div>
					<table class="table table-striped table-hover ">
					  <thead>
					    <tr align="right">
					      <th width="20%"> </th>
					      <th style="text-align:right" width="16%">Satuan Pendidikan</th>
					      <th style="text-align:right" width="16%">Guru</th>
					      <th style="text-align:right" width="16%">Pegawai</th>
					      <th style="text-align:right" width="16%">Peserta Didik</th>
					      <th style="text-align:right" width="16%">Rombel</th>
					    </tr>
					  </thead>
					  <tbody>
					    
					    <tr>
					      <td><b>PAUD</b></td>
					      <td align="right">'.moneyConverter($row['sma'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['guru_sma'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pegawai_sma'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pd_sma'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['rombel_sma'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					    </tr>
					    <tr>
					      <td><b>PKBM</b></td>
					      <td align="right">'.moneyConverter($row['smk'], $jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['guru_smk'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pegawai_smk'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pd_smk'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['rombel_smk'],$jumdes = 0,$pemisah_ribuan = ".","").'</td></tr>
						<tr>
					      <td><b>LKP</b></td>
					      <td align="right">'.moneyConverter($row['smlb'], $jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['guru_smlb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pegawai_smlb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pd_smlb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['rombel_smlb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td></tr>
					    <tr>
					      <td><b>TBM</b></td>
					      <td align="right">'.moneyConverter($row['slb'], $jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['guru_slb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pegawai_slb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pd_slb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['rombel_slb'],$jumdes = 0,$pemisah_ribuan = ".","").'</td></tr>
					    <tr>
					      <td><b>Total</b></td>
					      <td align="right">'.moneyConverter($row['sekolah_total'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['guru_total'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pegawai_total'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['pd_total'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($row['rombel_total'],$jumdes = 0,$pemisah_ribuan = ".","").'</td></tr>
					  </tbody>
					</table>';

		return $templates;
					// return json_encode($row);
				
	}

	public function getRekapAll2(){

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini);

		$rekap = RekapJumlahTotalPeer::doSelect($c);

		$count = RekapJumlahTotalPeer::doCount($c);

		// spesial buat sekolah

		$skh = new \Criteria();
		$skh->add(JumlahSekolahPeer::TANGGAL, $hari_ini);
		$skh->add(JumlahSekolahPeer::SEMESTER_ID, '20141');

		$rekapSekolah = JumlahSekolahPeer::doSelect($skh);

		foreach ($rekapSekolah as $r) {
			$jumSmaNegeri = $jumSmaNegeri + $r->getStatusSmaNegeri();
			$jumSmaSwasta = $jumSmaSwasta + $r->getStatusSmaSwasta();

			$jumSmkNegeri = $jumSmkNegeri + $r->getStatusSmkNegeri();
			$jumSmkSwasta = $jumSmkSwasta + $r->getStatusSmkSwasta();

			$jumSmlbNegeri = $jumSmlbNegeri + $r->getStatusSmlbNegeri();
			$jumSmlbSwasta = $jumSmlbSwasta + $r->getStatusSmlbSwasta();
		}

		$jumSma = $jumSmaNegeri + $jumSmaSwasta;
		$jumSmk = $jumSmkNegeri + $jumSmkSwasta;
		$jumSmlb = $jumSmlbNegeri + $jumSmlbSwasta;

		// end of spesial buat sekolah

		// spesial buat peserta didik
		$hari_ini = date('Y-m-d');

		$d = new \Criteria();
		$d->add(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$d->add(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekapPd = JumlahPdStatusPeer::doSelect($d);

		$i = 0;
		$j = 0;
		$k = 0;

		foreach ($rekapPd as $rpd) {
			
			$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
			$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
			$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());

		}


		foreach ($rekap as $r) {
			$templates = '<div style="width:100%;margin:auto;text-align:center;padding-bottom:10px;font-size:14px">Rekap Nasional <b>Tahun Ajaran 2014/2015 Semester Ganjil</b> <br>Per <b>'.DateToIndoReversedWithTime($r->getUpdateTerakhir()).'</b></div>
					<table class="table table-striped table-hover ">
					  <thead>
					    <tr align="right">
					      <th width="20%"> </th>
					      <th style="text-align:right" width="20%">Sekolah</th>
					      <th style="text-align:right" width="20%">PTK</th>
					      <th style="text-align:right" width="20%">Peserta Didik</th>
					    </tr>
					  </thead>
					  <tbody>
					    
					    <tr>
					      <td><b>SMA</b></td>
					      <td align="right">'.moneyConverter($jumSma,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($r->getPtkSmaNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right"><a href="laman/rekappdperstatussekolah">'.moneyConverter($i,$jumdes = 0,$pemisah_ribuan = ".","").'</a></td>
					    </tr>
					    <tr>
					      <td><b>SMK</b></td>
					      <td align="right">'.moneyConverter($jumSmk,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($r->getPtkSmkNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right"><a href="laman/rekappdperstatussekolah">'.moneyConverter($j,$jumdes = 0,$pemisah_ribuan = ".","").'</a></td>
					    </tr>
						<tr>
					      <td><b>SMLB</b></td>
					      <td align="right">'.moneyConverter($jumSmlb,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($r->getPtkSmlbNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right"><a href="laman/rekappdperstatussekolah">'.moneyConverter($k,$jumdes = 0,$pemisah_ribuan = ".","").'</a></a></td>
					    </tr>

					    <tr>
					      <td><b>Total</b></td>
					      <td align="right">'.moneyConverter($jumSma+$jumSmk+$jumSmlb,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right">'.moneyConverter($r->getPtkNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					      <td align="right"><a href="laman/rekappdnasional">'.moneyConverter(($i+$j+$k),$jumdes = 0,$pemisah_ribuan = ".","").'</a></td>
					    </tr>
					  </tbody>
					</table>';

			// $templates = '<div style="width:100%;margin:auto;text-align:center;padding-bottom:10px;font-size:14px">Rekap Nasional <b>Tahun Ajaran 2014/2015 Semester Ganjil</b> <br>Per <b>'.DateToIndoReversedWithTime($r->getUpdateTerakhir()).'</b></div>
						
			// 			<div class="panel panel-default rekap_all_panel">
			// 			  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap SMA</div>
			// 			  <div class="panel-body">
			// 			    <table class="table table-striped table-hover ">
			// 	                <tbody>
			// 	                	<tr>
			// 		                    <td width="70%">Sekolah</td>
			// 		                    <td align="right">'.moneyConverter($jumSma,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PTK</td>
			// 		                    <td align="right">'.moneyConverter($r->getPtkSmaNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Terdaftar</td>
			// 		                    <td align="right">'.moneyConverter($r->getPdSmaNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Telah Masuk Rombel</td>
			// 		                    <td align="right">'.moneyConverter($i,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		            </body>
			// 		        </table>
			// 			  </div>
			// 			</div> 

			// 			<div class="panel panel-default rekap_all_panel">
			// 			  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap SMK</div>
			// 			  <div class="panel-body">
			// 			    <table class="table table-striped table-hover ">
			// 	                <tbody>
			// 	                	<tr>
			// 		                    <td width="70%">Sekolah</td>
			// 		                    <td align="right">'.moneyConverter($jumSmk,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PTK</td>
			// 		                    <td align="right">'.moneyConverter($r->getPtkSmkNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Terdaftar</td>
			// 		                    <td align="right">'.moneyConverter($r->getPdSmkNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Telah Masuk Rombel</td>
			// 		                    <td align="right">'.moneyConverter($j,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		            </body>
			// 		        </table>
			// 			  </div>
			// 			</div>
			// 			<div class="panel panel-default rekap_all_panel">
			// 			  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap SMLB</div>
			// 			  <div class="panel-body">
			// 			    <table class="table table-striped table-hover ">
			// 	                <tbody>
			// 	                	<tr>
			// 		                    <td width="70%">Sekolah</td>
			// 		                    <td align="right">'.moneyConverter($jumSmlb,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PTK</td>
			// 		                    <td align="right">'.moneyConverter($r->getPtkSmlbNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Terdaftar</td>
			// 		                    <td align="right">'.moneyConverter($r->getPdSmlbNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Telah Masuk Rombel</td>
			// 		                    <td align="right">'.moneyConverter($k,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		            </body>
			// 		        </table>
			// 			  </div>
			// 			</div>
			// 			<div class="panel panel-default rekap_all_panel">
			// 			  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap Nasional</div>
			// 			  <div class="panel-body">
			// 			    <table class="table table-striped table-hover ">
			// 	                <tbody>
			// 	                	<tr>
			// 		                    <td width="70%">Sekolah</td>
			// 		                    <td align="right">'.moneyConverter($jumSma+$jumSmk+$jumSmlb,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PTK</td>
			// 		                    <td align="right">'.moneyConverter($r->getPtkNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Terdaftar</td>
			// 		                    <td align="right">'.moneyConverter($r->getPdNasional(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		                <tr>
			// 		                    <td>PD Telah Masuk Rombel</td>
			// 		                    <td align="right">'.moneyConverter(($i+$j+$k),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			// 		                </tr>
			// 		            </tbody>
			// 		        </table>
			// 			  </div>
			// 			</div>
			// 			<div class="clear"></div>';
		}
		// $arr = $dl->toArray(\BasePeer::TYPE_FIELDNAME);
		// $outArr[] = $arr;

		// if($i+$j+$k == 0){
		// 	return "<div style=width:100%;text-align:center;font-size:30px>Untuk sementara sistem belum bisa menampilkan rekap. Terima Kasih</div>";die;
		// }else{
			return $templates;
		// }
	}

	public function getRekapAll(){
		$sql = "SELECT
				'PTK' AS rekap_total,
				COUNT (*) AS jumlah
			FROM
				ptk ptk
			LEFT JOIN ptk_terdaftar ptkt ON ptk.ptk_id = ptkt.ptk_id
			LEFT JOIN sekolah s ON s.sekolah_id = ptkt.sekolah_id
			INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
			INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
			INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
			WHERE
				ptk.Soft_delete = 0
			AND ptkt.Soft_delete = 0
			AND ptkt.jenis_keluar_id IS NULL
			AND ptkt.tahun_ajaran_id = (select top 1 tahun_ajaran_id from ref.tahun_ajaran where expired_date is null)
			AND s.bentuk_pendidikan_id in (13,14,15)
			UNION
				SELECT
					'Peserta Didik' AS rekap_total,
					COUNT (*)
				FROM
					peserta_didik pd
				LEFT JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
				LEFT JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
				INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
				INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
				INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
				WHERE
					pd.Soft_delete = 0
				AND rpd.Soft_delete = 0
				AND rpd.jenis_keluar_id IS NULL
				AND s.bentuk_pendidikan_id in (13,14,15)
				UNION
					SELECT
						'Sekolah' AS rekap_total,
						COUNT (*)
					FROM
						sekolah sekolah
					INNER JOIN ref.mst_wilayah w1 ON sekolah.kode_wilayah=w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
					WHERE
						sekolah.soft_delete = 0
						and bentuk_pendidikan_id in (13,14,15)
					UNION
						SELECT
							'SMK' AS rekap_total,
							COUNT (*)
						FROM
							sekolah sekolah
						INNER JOIN ref.mst_wilayah w1 ON sekolah.kode_wilayah=w1.kode_wilayah
						INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
						INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
						WHERE
							sekolah.bentuk_pendidikan_id = 15
						AND sekolah.Soft_delete = 0
						UNION
							SELECT
								'SMLB' AS rekap_total,
								COUNT (*)
							FROM
								sekolah sekolah
							INNER JOIN ref.mst_wilayah w1 ON sekolah.kode_wilayah=w1.kode_wilayah
							INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
							INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
							WHERE
								sekolah.bentuk_pendidikan_id = 14
							AND sekolah.Soft_delete = 0
							UNION
								SELECT
									'SMA' AS rekap_total,
									COUNT (*)
								FROM
									sekolah sekolah
								INNER JOIN ref.mst_wilayah w1 ON sekolah.kode_wilayah=w1.kode_wilayah
								INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
								INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
								WHERE
									sekolah.bentuk_pendidikan_id = 13
								AND sekolah.Soft_delete = 0
								UNION
									SELECT
										'PTK SMK' AS rekap_total,
										COUNT (*) AS jumlah
									FROM
										ptk ptk
									LEFT JOIN ptk_terdaftar ptkt ON ptk.ptk_id = ptkt.ptk_id
									LEFT JOIN sekolah s ON s.sekolah_id = ptkt.sekolah_id
									INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
									INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
									INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
									WHERE
										ptk.Soft_delete = 0
									AND ptkt.Soft_delete = 0
									AND ptkt.jenis_keluar_id IS NULL
									AND ptkt.tahun_ajaran_id = (select top 1 tahun_ajaran_id from ref.tahun_ajaran where expired_date is null)
									AND s.bentuk_pendidikan_id = 15
									UNION
										SELECT
											'PTK SMLB' AS rekap_total,
											COUNT (*) AS jumlah
										FROM
											ptk ptk
										LEFT JOIN ptk_terdaftar ptkt ON ptk.ptk_id = ptkt.ptk_id
										LEFT JOIN sekolah s ON s.sekolah_id = ptkt.sekolah_id
										INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
										INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
										INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
										WHERE
											ptk.Soft_delete = 0
										AND ptkt.Soft_delete = 0
										AND ptkt.jenis_keluar_id IS NULL
										AND ptkt.tahun_ajaran_id = (select top 1 tahun_ajaran_id from ref.tahun_ajaran where expired_date is null)
										AND s.bentuk_pendidikan_id = 14
										UNION
											SELECT
												'PTK SMA' AS rekap_total,
												COUNT (*) AS jumlah
											FROM
												ptk ptk
											LEFT JOIN ptk_terdaftar ptkt ON ptk.ptk_id = ptkt.ptk_id
											LEFT JOIN sekolah s ON s.sekolah_id = ptkt.sekolah_id
											INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
											INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
											INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
											WHERE
												ptk.Soft_delete = 0
											AND ptkt.Soft_delete = 0
											AND ptkt.jenis_keluar_id IS NULL
											AND ptkt.tahun_ajaran_id = (select top 1 tahun_ajaran_id from ref.tahun_ajaran where expired_date is null)
											AND s.bentuk_pendidikan_id = 13
											UNION
												SELECT
													'Peserta Didik SMK' AS rekap_total,
													COUNT (*)
												FROM
													peserta_didik pd
												LEFT JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
												LEFT JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
												INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
												INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
												INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
												WHERE
													pd.Soft_delete = 0
												AND rpd.Soft_delete = 0
												AND rpd.jenis_keluar_id IS NULL
												AND s.bentuk_pendidikan_id = 15
												UNION
													SELECT
														'Peserta Didik SMLB' AS rekap_total,
														COUNT (*)
													FROM
														peserta_didik pd
													LEFT JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
													LEFT JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
													INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
													INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
													INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
													WHERE
														pd.Soft_delete = 0
													AND rpd.Soft_delete = 0
													AND rpd.jenis_keluar_id IS NULL
													AND s.bentuk_pendidikan_id = 14
													UNION
														SELECT
															'Peserta Didik SMA' AS rekap_total,
															COUNT (*)
														FROM
															peserta_didik pd
														LEFT JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
														LEFT JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
														INNER JOIN ref.mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
														INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
														INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
														WHERE
															pd.Soft_delete = 0
														AND rpd.Soft_delete = 0
														AND rpd.jenis_keluar_id IS NULL
														AND s.bentuk_pendidikan_id = 13";

		$con = $this->initdb();
		$stmt = sqlsrv_query( $con, $sql );

		$return_arr = array();

		/*$templates = '<table class="table table-striped table-hover ">
		                <thead>
		                  <tr>
		                    <th>Nama</th>
		                    <th>Jumlah</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';
		*/

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			$return_arr[$row['rekap_total']] = $row['jumlah'];
		      /*
		    
			$templates .= '<tr>
		                    <td>'.$row["rekap_total"].'</td>
		                    <td align="right">'.moneyConverter($row["jumlah"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                  </tr>';
		                 */
		}
		/*
		$templates .= ' </tbody>
		              </table>';
		*/

		sqlsrv_free_stmt( $stmt);

		$templates = '<div class="panel panel-default rekap_all_panel">
						  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap Nasional</div>
						  <div class="panel-body">
						    <table class="table table-striped table-hover ">
				                <tbody>
				                	<tr>
					                    <td>Sekolah</td>
					                    <td align="right">'.moneyConverter($return_arr['Sekolah'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>PTK</td>
					                    <td align="right">'.moneyConverter($return_arr['PTK'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>Peserta Didik</td>
					                    <td align="right">'.moneyConverter($return_arr['Peserta Didik'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					            </body>
					        </table>
						  </div>
						</div>
						<div class="panel panel-default rekap_all_panel">
						  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap SMK</div>
						  <div class="panel-body">
						    <table class="table table-striped table-hover ">
				                <tbody>
				                	<tr>
					                    <td>Sekolah</td>
					                    <td align="right">'.moneyConverter($return_arr['SMK'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>PTK</td>
					                    <td align="right">'.moneyConverter($return_arr['PTK SMK'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>Peserta Didik</td>
					                    <td align="right">'.moneyConverter($return_arr['Peserta Didik SMK'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					            </body>
					        </table>
						  </div>
						</div>
						<div class="panel panel-default rekap_all_panel">
						  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap SMLB</div>
						  <div class="panel-body">
						    <table class="table table-striped table-hover ">
				                <tbody>
				                	<tr>
					                    <td>Sekolah</td>
					                    <td align="right">'.moneyConverter($return_arr['SMLB'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>PTK</td>
					                    <td align="right">'.moneyConverter($return_arr['PTK SMLB'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>Peserta Didik</td>
					                    <td align="right">'.moneyConverter($return_arr['Peserta Didik SMLB'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					            </body>
					        </table>
						  </div>
						</div><div class="panel panel-default rekap_all_panel">
						  <div class="panel-heading" style="font-weight:bold;font-size:15px;color:#434343">Rekap SMA</div>
						  <div class="panel-body">
						    <table class="table table-striped table-hover ">
				                <tbody>
				                	<tr>
					                    <td>Sekolah</td>
					                    <td align="right">'.moneyConverter($return_arr['SMA'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>PTK</td>
					                    <td align="right">'.moneyConverter($return_arr['PTK SMA'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					                <tr>
					                    <td>Peserta Didik</td>
					                    <td align="right">'.moneyConverter($return_arr['Peserta Didik SMA'],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
					                </tr>
					            </body>
					        </table>
						  </div>
						</div>
						<div class="clear"></div>';

		//return json_encode($return_arr);
		return $templates;
	}

	public function breadcrumbKab(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah');
		$con = $this->initdb();

		$sql = "select mst1.nama, mst2.nama as induk, mst2.kode_wilayah as kode_induk from ref.mst_wilayah mst1 join ref.mst_wilayah mst2 on mst1.mst_kode_wilayah = mst2.kode_wilayah where mst1.kode_wilayah = ".$kode_wilayah;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
	}

	public function breadcrumbKec(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah');
		$con = $this->initdb();

		$sql = "SELECT
					mst1.nama,
					mst2.nama AS induk,
					mst2.kode_wilayah as kode_induk,
					mst3.nama AS induk_upper,
					mst3.kode_wilayah as kode_induk_upper
				FROM
					ref.mst_wilayah mst1
				JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
				join ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
				WHERE
					mst1.kode_wilayah = ".$kode_wilayah;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
	}

	public function top5Prop(Request $request, Application $app){

		// $hari_ini = $this->getToday();

		// $filename = 'cache/top5Prop/top5Prop'.$hari_ini;

  //       if (!file_exists($filename)){


			$con = $this->initdbSqlServerLokal();
			$semester_id = $request->get('semester_id');

			$sql = "SELECT
					semester_id,
					provinsi as nama,
					kode_wilayah_provinsi as kode_wilayah,
					id_level_wilayah_provinsi as id_level_wilayah,
					sum(case when bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
					sum(case when bentuk_pendidikan_id = 13 and jumlah_kirim > 0 then 1 else 0 end) as kirim_sma,
					sum(case when bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
					sum(case when bentuk_pendidikan_id = 15 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smk,
					sum(case when bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
					sum(case when bentuk_pendidikan_id = 14 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smlb,
					sum(case when bentuk_pendidikan_id = 29 then 1 else 0 end) as slb,
					sum(case when bentuk_pendidikan_id = 29 and jumlah_kirim > 0 then 1 else 0 end) as kirim_slb,
					SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END) AS kirim_total,
					COUNT (1) AS sekolah_total,
					(((SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END)) / CAST((count(1)) as NUMERIC(9,2))) * 100) as persen
				FROM
					rekap_sekolah
				WHERE
					semester_id = {$semester_id}
				GROUP BY
					provinsi,
					kode_wilayah_provinsi,
					id_level_wilayah_provinsi,
					semester_id
				order by persen desc";

			$stmt = sqlsrv_query( $con, $sql );

			$templates = '<table class="table table-striped table-hover ">
			                <thead>
			                  <tr>
			                    <th>#</th>
			                    <th>Propinsi</th>
			                    <th>%</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';
			  
			$i = 1;

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {
			      
			      //$return_arr[] = $row;
				if($i <= 10){
					$templates .= '<tr>
				                    <td>'.$i.'</td>
				                    <td><a href="laman/detailprop/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
				                    <td>'.round($row["persen"],2).'</td>
				                  </tr>';
				}

			    $i++;
			}

			$templates .= ' </tbody>
			              </table>';

			sqlsrv_free_stmt( $stmt);

			// $templates .= "<div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			// file_put_contents($filename, $templates);
   //      }

   //      $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $templates;

	}

	public function top10Kab(Request $request, Application $app){

		$con = $this->initdbSqlServerLokal();
		$semester_id = $request->get('semester_id');

		$sql = "SELECT
				semester_id,
				kabupaten as nama,
				kode_wilayah_kabupaten as kode_wilayah,
				id_level_wilayah_kabupaten as id_level_wilayah,
				sum(case when bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
				sum(case when bentuk_pendidikan_id = 13 and jumlah_kirim > 0 then 1 else 0 end) as kirim_sma,
				sum(case when bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
				sum(case when bentuk_pendidikan_id = 15 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smk,
				sum(case when bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
				sum(case when bentuk_pendidikan_id = 14 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smlb,
				sum(case when bentuk_pendidikan_id = 29 then 1 else 0 end) as slb,
				sum(case when bentuk_pendidikan_id = 29 and jumlah_kirim > 0 then 1 else 0 end) as kirim_slb,
				SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END) AS kirim_total,
				COUNT (1) AS sekolah_total,
				(((SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END)) / CAST((count(1)) as NUMERIC(9,2))) * 100) as persen
			FROM
				rekap_sekolah
			WHERE
				semester_id = {$semester_id}
			GROUP BY
				kabupaten,
				kode_wilayah_kabupaten,
				id_level_wilayah_kabupaten,
				semester_id
			order by persen desc";

			// return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

			$templates = '<table class="table table-striped table-hover ">
			                <thead>
			                  <tr>
			                    <th>#</th>
			                    <th>Kabupaten / Kota</th>
			                    <th>%</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';
			  
			$i = 1;

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

				if($row["persen"] < 100){

					if($i <= 10){
						$templates .= '<tr>
			                    <td>'.$i.'</td>
			                    <td><a href="laman/detailkab/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
			                    <td>'.round($row["persen"],2).'</td>
			                  </tr>';

			    		$i++;
					}

				}
			      
			      //$return_arr[] = $row;
				
			}

			$templates .= ' </tbody>
			              </table>';

			sqlsrv_free_stmt( $stmt);

			// $templates .= "<div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			// file_put_contents($filename, $templates);
   //      }

   //      $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $templates;

	}

	public function reportSekolahPerKabupaten(Request $request, Application $app){
		$con = $this->initdbSqlServerLokal();
		$semester_id = $request->get('semester_id');
		$kode_wilayah = $request->get('kode_wilayah');

		$sql = "SELECT
					*
				FROM
					rekap_sekolah
				WHERE
					kode_wilayah_kabupaten = {$kode_wilayah}
				AND semester_id = {$semester_id}
				order by jumlah_kirim desc";

		$stmt = sqlsrv_query( $con, $sql );

		$i = 1;

		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th rowspan=2>#</th>
		                    <th rowspan=2>Satuan Pendidikan</th>
		                    <th rowspan=2>Kecamatan</th>
		                    <th rowspan=2>NPSN</th>
		                    <th rowspan=2>Status Sekolah</th>
		                    <th rowspan=2>Bentuk Pendidikan</th>
		                    <th rowspan=2>Jml Sinkronisasi</th>
		                    <th colspan=2>PTK</th>
		                    <th colspan=5>Peserta Didik</th>
		                  </tr>
		                  <tr>
		                    <th>Guru</th>
		                    <th>Pegawai</th>
		                    <th>X</th>
		                    <th>XI</th>
		                    <th>XII</th>
		                    <th>XIII</th>
		                    <th>Total</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		if($row["status_sekolah"] == 1){
			$status = 'Negeri';
		}else if($row["status_sekolah"] == 2){
			$status = 'Swasta';
		}

		if($row["bentuk_pendidikan_id"] == 13){
			$bentuk = 'SMA';
		}else if($row["bentuk_pendidikan_id"] == 14){
			$bentuk = 'SMLB';
		}else if ($row["bentuk_pendidikan_id"] == 15){
			$bentuk = 'SMK';
		}

		$enkrip = $row['sekolah_id'];

		$sql_enkrip = "SELECT
				CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST('".$enkrip."' AS VARCHAR(50))),
					2
				) AS enkripsi";
		$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
		$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);

		$templates .= '<tr>
	                    <td>'.$i.'</td>
	                    <td><a href="../datasekolah/'.$row_enkrip['enkripsi'].'">'.$row["nama"].'</a></td>
	                    <td>'.$row["kecamatan"].'</td>
	                    <td>'.$row["npsn"].'</td>
	                    <td>'.$status.'</td>
	                    <td>'.$bentuk.'</td>
	                    <td>'.$row["jumlah_kirim"].'</td>
	                    <td>'.$row["ptk"].'</td>
	                    <td>'.$row["pegawai"].'</td>
	                    <td>'.$row["pd_kelas_10"].'</td>
	                    <td>'.$row["pd_kelas_11"].'</td>
	                    <td>'.$row["pd_kelas_12"].'</td>
	                    <td>'.$row["pd_kelas_13"].'</td>
	                    <td>'.$row["pd"].'</td>
	                    
	                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;
	}

	public function reportKabPeriodik(Request $request, Application $app){
		$con = $this->initdbSqlServerLokal();
		$semester_id = $request->get('semester_id');
		$kode_wilayah = $request->get('kode_wilayah');

		$sql = "SELECT
				semester_id,
				kabupaten as nama,
				kode_wilayah_kabupaten as kode_wilayah,
				id_level_wilayah_kabupaten as id_level_wilayah,
				sum(case when bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
				sum(case when bentuk_pendidikan_id = 13 and jumlah_kirim > 0 then 1 else 0 end) as kirim_sma,
				sum(case when bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
				sum(case when bentuk_pendidikan_id = 15 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smk,
				sum(case when bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
				sum(case when bentuk_pendidikan_id = 14 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smlb,
				sum(case when bentuk_pendidikan_id = 29 then 1 else 0 end) as slb,
				sum(case when bentuk_pendidikan_id = 29 and jumlah_kirim > 0 then 1 else 0 end) as kirim_slb,
				SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END) AS kirim_total,
				COUNT (1) AS sekolah_total,
				(((SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END)) / CAST((count(1)) as NUMERIC(9,2))) * 100) as persen
			FROM
				rekap_sekolah
			WHERE
				semester_id = {$semester_id}
				and mst_kode_wilayah_kabupaten = {$kode_wilayah}
			GROUP BY
				kabupaten,
				kode_wilayah_kabupaten,
				id_level_wilayah_kabupaten,
				semester_id
			order by persen desc";

			// return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr align="right">
		                    <th rowspan=2>#</th>
		                    <th rowspan=2>Propinsi</th>
		                    <th colspan=3>SMA</th>
		                    <th colspan=3>SMK</th>
		                    <th colspan=3>SMLB</th>
		                    <th colspan=3>SLB</th>
		                    <th rowspan=2>Terkirim</th>
		                    <th rowspan=2>%</th>
		                  </tr>
		                  <tr>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		$i = 1;

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		      
		   	$sisa_sma = $row['sma'] - $row['kirim_sma'];   
		   	$sisa_smk = $row['smk'] - $row['kirim_smk'];   
		   	$sisa_smlb = $row['smlb'] - $row['kirim_smlb'];  
		   	$sisa_slb = $row['slb'] - $row['kirim_slb'];  

		   	$total = $row['sma'] + $row['smk'] + $row['smlb'] + $row['slb']; 
		   	$total_kirim = $row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb'] + $row['kirim_slb']; 

		   	// $persen = ($total_kirim / $total) * 100;
		   	$persen = $row['persen'];

		   	$persen_bulat = round($persen, 2);

		   	
			$templates .= '<tr align="right">
		                    <td align="left">'.$i.'</td>
		                    <td align="left"><a href="../detailkab/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
		                    <td>'.$row["sma"].'</td>
		                    <td>'.$row["kirim_sma"].'</td>
		                    <td>'.$sisa_sma.'</td>
		                    <td>'.$row["smk"].'</td>
		                    <td>'.$row["kirim_smk"].'</td>
		                    <td>'.$sisa_smk.'</td>
		                    <td>'.$row["smlb"].'</td>
		                    <td>'.$row["kirim_smlb"].'</td>
		                    <td>'.$sisa_smlb.'</td>
		                    <td>'.$row["slb"].'</td>
		                    <td>'.$row["kirim_slb"].'</td>
		                    <td>'.$sisa_slb.'</td>
		                    <td>'.$total_kirim.'</td>
		                    <td>'.$persen_bulat.' %</td>
		                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;
	}	


	public function reportPropPeriodik(Request $request, Application $app){
		$con = $this->initdbSqlServerLokal();
		$semester_id = $request->get('semester_id');

		$sql = "SELECT
				semester_id,
				provinsi as nama,
				kode_wilayah_provinsi as kode_wilayah,
				id_level_wilayah_provinsi as id_level_wilayah,
				sum(case when bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
				sum(case when bentuk_pendidikan_id = 13 and jumlah_kirim > 0 then 1 else 0 end) as kirim_sma,
				sum(case when bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
				sum(case when bentuk_pendidikan_id = 15 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smk,
				sum(case when bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
				sum(case when bentuk_pendidikan_id = 14 and jumlah_kirim > 0 then 1 else 0 end) as kirim_smlb,
				sum(case when bentuk_pendidikan_id = 29 then 1 else 0 end) as slb,
				sum(case when bentuk_pendidikan_id = 29 and jumlah_kirim > 0 then 1 else 0 end) as kirim_slb,
				SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END) AS kirim_total,
				COUNT (1) AS sekolah_total,
				(((SUM (CASE WHEN jumlah_kirim > 0 THEN 1 ELSE 0 END)) / CAST((count(1)) as NUMERIC(9,2))) * 100) as persen
			FROM
				rekap_sekolah
			WHERE
				semester_id = {$semester_id}
			GROUP BY
				provinsi,
				kode_wilayah_provinsi,
				id_level_wilayah_provinsi,
				semester_id
			order by persen desc";

		$stmt = sqlsrv_query( $con, $sql );

		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th rowspan=2>#</th>
		                    <th rowspan=2>Propinsi</th>
		                    <th colspan=3>SMA</th>
		                    <th colspan=3>SMK</th>
		                    <th colspan=3>SMLB</th>
		                    <th colspan=3>SLB</th>
		                    <th rowspan=2>Terkirim</th>
		                    <th rowspan=2>%</th>
		                  </tr>
		                  <tr>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  	<th>Jumlah</th>
		                  	<th>Kirim</th>
		                  	<th>Sisa</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		$i = 1;

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		      
		   	$sisa_sma = $row['sma'] - $row['kirim_sma'];   
		   	$sisa_smk = $row['smk'] - $row['kirim_smk'];   
		   	$sisa_smlb = $row['smlb'] - $row['kirim_smlb'];  
		   	$sisa_slb = $row['slb'] - $row['kirim_slb'];  

		   	$total = $row['sma'] + $row['smk'] + $row['smlb'] + $row['slb']; 
		   	$total_kirim = $row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb'] + $row['kirim_slb']; 

		   	// $persen = ($total_kirim / $total) * 100;
		   	$persen = $row['persen'];

		   	$persen_bulat = round($persen, 2);

		   	
			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><a href="../laman/detailprop/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
		                    <td>'.$row["sma"].'</td>
		                    <td>'.$row["kirim_sma"].'</td>
		                    <td>'.$sisa_sma.'</td>
		                    <td>'.$row["smk"].'</td>
		                    <td>'.$row["kirim_smk"].'</td>
		                    <td>'.$sisa_smk.'</td>
		                    <td>'.$row["smlb"].'</td>
		                    <td>'.$row["kirim_smlb"].'</td>
		                    <td>'.$sisa_smlb.'</td>
		                    <td>'.$row["slb"].'</td>
		                    <td>'.$row["kirim_slb"].'</td>
		                    <td>'.$sisa_slb.'</td>
		                    <td>'.$total_kirim.'</td>
		                    <td>'.$persen_bulat.' %</td>
		                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;
	}	


	public function reportProp(Request $request, Application $app){

		$hari_ini = $this->getToday();
		$semester_id = $request->get('semester_id');

		$filename = 'cache/reportProp/reportProp_'.$semester_id.'_'.$hari_ini;

        if (!file_exists($filename)){

        	$con = $this->initdb();

			$sql = "SELECT w3.nama,w3.kode_wilayah,
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) AS sma,
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) AS smk,
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END) AS smlb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) AS slb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sma,
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smk,
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smlb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_slb,
							(
								(
									SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END)
								) /
								CAST(
									(SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
								)
							) * 100 as persen
						FROM dbo.sekolah r
							INNER JOIN ref.mst_wilayah w1 ON r.kode_wilayah=w1.kode_wilayah
							INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
							INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
							LEFT JOIN (
								SELECT 
									sekolah_id,
									MAX(begin_sync) AS begin_sync
								FROM dbo.sync_log
								WHERE alamat_ip NOT LIKE 'prefil%'
								GROUP BY sekolah_id) l ON r.sekolah_id=l.sekolah_id
						WHERE w3.id_level_wilayah=1
						AND (w3.expired_date is null or w3.expired_date > GETDATE())
						AND r.Soft_delete = 0
						GROUP BY w3.nama,w3.kode_wilayah
						order by persen desc";

			$stmt = sqlsrv_query( $con, $sql );

			$templates = '<table class="table table-striped table-hover " style="font-size:12px">
			                <thead>
			                  <tr>
			                    <th rowspan=2>#</th>
			                    <th rowspan=2>Propinsi</th>
			                    <th colspan=3>SMA</th>
			                    <th colspan=3>SMK</th>
			                    <th colspan=3>SMLB</th>
			                    <th colspan=3>SLB</th>
			                    <th rowspan=2>Terkirim</th>
			                    <th rowspan=2>%</th>
			                  </tr>
			                  <tr>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';

			$i = 1;

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			      
			   	$sisa_sma = $row['sma'] - $row['kirim_sma'];   
			   	$sisa_smk = $row['smk'] - $row['kirim_smk'];   
			   	$sisa_smlb = $row['smlb'] - $row['kirim_smlb'];  
			   	$sisa_slb = $row['slb'] - $row['kirim_slb'];  

			   	$total = $row['sma'] + $row['smk'] + $row['smlb'] + $row['slb']; 
			   	$total_kirim = $row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb'] + $row['kirim_slb']; 

			   	// $persen = ($total_kirim / $total) * 100;
			   	$persen = $row['persen'];

			   	$persen_bulat = round($persen, 2);

			   	// tambahan untuk ngitung jumlah siswa per semester
			 //   	$sql_jml_siswa = "SELECT
				// 					COUNT (*) as jumlah
				// 				FROM
				// 					registrasi_peserta_didik rpd
				// 				INNER JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
				// 				INNER JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
				// 				INNER JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				// 				INNER JOIN sekolah s on s.sekolah_id = rb.sekolah_id
				// 				JOIN ref.mst_wilayah mst1 ON s.kode_wilayah = mst1.kode_wilayah
				// 				JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
				// 				JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
				// 				WHERE
				// 				rpd.Soft_delete = 0
				// 				AND pd.Soft_delete = 0
				// 				AND rb.soft_delete = 0
				// 				AND ar.soft_delete = 0
				// 				and s.Soft_delete = 0
				// 				AND rpd.jenis_keluar_id IS NULL
				// 				AND rb.semester_id = '".$semester_id."'
				// 				and mst3.kode_wilayah = ".$row['kode_wilayah'];

				// $stmt_jml_siswa = sqlsrv_query( $con, $sql_jml_siswa );
				// $row_jml_siswa = sqlsrv_fetch_array( $stmt_jml_siswa, SQLSRV_FETCH_ASSOC);


			      //$return_arr[] = $row;
				$templates .= '<tr>
			                    <td>'.$i.'</td>
			                    <td><a href="../laman/detailprop/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
			                    <td>'.$row["sma"].'</td>
			                    <td>'.$row["kirim_sma"].'</td>
			                    <td>'.$sisa_sma.'</td>
			                    <td>'.$row["smk"].'</td>
			                    <td>'.$row["kirim_smk"].'</td>
			                    <td>'.$sisa_smk.'</td>
			                    <td>'.$row["smlb"].'</td>
			                    <td>'.$row["kirim_smlb"].'</td>
			                    <td>'.$sisa_smlb.'</td>
			                    <td>'.$row["slb"].'</td>
			                    <td>'.$row["kirim_slb"].'</td>
			                    <td>'.$sisa_slb.'</td>
			                    <td>'.$total_kirim.'</td>
			                    <td>'.$persen_bulat.' %</td>
			                  </tr>';

			    $i++;
			}

			$templates .= ' </tbody>
			              </table>';

			sqlsrv_free_stmt( $stmt);

			$templates .= "<br><div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			file_put_contents($filename, $templates);
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;

		// return $sql_jml_siswa;
	}

	public function reportKab(Request $request, Application $app){
		$hari_ini = $this->getToday();
		$semester_id = $request->get('semester_id');
		$kode_wilayah = $request->get('kode_wilayah');

		$filename = 'cache/reportKab/reportKab_'.$kode_wilayah.'_'.$semester_id.'_'.$hari_ini;

        if (!file_exists($filename)){
        	$con = $this->initdb();

			$sql = "SELECT w2.nama,w2.kode_wilayah,
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) AS sma,
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) AS smk,
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END) AS smlb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) AS slb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sma,
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smk,
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smlb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_slb,
							(
								(
									SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END)
								) /
								CAST(
									(SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
								)
							) * 100 as persen
						FROM dbo.sekolah r
							INNER JOIN ref.mst_wilayah w1 ON r.kode_wilayah=w1.kode_wilayah
							INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
							INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
							LEFT JOIN (
								SELECT 
									sekolah_id,
									MAX(begin_sync) AS begin_sync
								FROM dbo.sync_log
								WHERE alamat_ip NOT LIKE 'prefil%'
								GROUP BY sekolah_id) l ON r.sekolah_id=l.sekolah_id
						WHERE 
						w2.mst_kode_wilayah = ".$kode_wilayah."
						AND (w2.expired_date is null or w2.expired_date > GETDATE())
						AND r.Soft_delete = 0
						GROUP BY w2.nama,w2.kode_wilayah
						order by persen desc";

			$stmt = sqlsrv_query( $con, $sql );

			$templates = '<table class="table table-striped table-hover " style="font-size:12px">
			                <thead>
			                  <tr>
			                    <th rowspan=2>#</th>
			                    <th rowspan=2>Kabupaten/Kota</th>
			                    <th colspan=3>SMA</th>
			                    <th colspan=3>SMK</th>
			                    <th colspan=3>SMLB</th>
			                    <th colspan=3>SLB</th>
			                    <th rowspan=2>Terkirim</th>
			                    <th rowspan=2>%</th>
			                  </tr>
			                  <tr>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';

			$i = 1;

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			      
			   	$sisa_sma = $row['sma'] - $row['kirim_sma'];   
			   	$sisa_smk = $row['smk'] - $row['kirim_smk'];   
			   	$sisa_smlb = $row['smlb'] - $row['kirim_smlb'];  
			   	$sisa_slb = $row['slb'] - $row['kirim_slb'];  

			   	$total = $row['sma'] + $row['smk'] + $row['smlb'] + $row['slb']; 
			   	$total_kirim = $row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb'] + $row['kirim_slb']; 

			   	$persen = ($total_kirim / $total) * 100;

			   	$persen_bulat = round($persen, 2);
			      //$return_arr[] = $row;
				$templates .= '<tr>
			                    <td>'.$i.'</td>
			                    <td><a href="../detailkab/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
			                    <td>'.$row["sma"].'</td>
			                    <td>'.$row["kirim_sma"].'</td>
			                    <td>'.$sisa_sma.'</td>
			                    <td>'.$row["smk"].'</td>
			                    <td>'.$row["kirim_smk"].'</td>
			                    <td>'.$sisa_smk.'</td>
			                    <td>'.$row["smlb"].'</td>
			                    <td>'.$row["kirim_smlb"].'</td>
			                    <td>'.$sisa_smlb.'</td>
			                    <td>'.$row["slb"].'</td>
			                    <td>'.$row["kirim_slb"].'</td>
			                    <td>'.$sisa_slb.'</td>
			                    <td>'.$total_kirim.'</td>
			                    <td>'.$persen_bulat.' %</td>
			                  </tr>';

			    $i++;
			}

			$templates .= ' </tbody>
			              </table>';

			sqlsrv_free_stmt( $stmt);

			$templates .= "<br><div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			file_put_contents($filename, $templates);
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;

	}

	public function reportKec(Request $request, Application $app){
		$hari_ini = $this->getToday();
		$semester_id = $request->get('semester_id');
		$kode_wilayah = $request->get('kode_wilayah');

		$filename = 'cache/reportKec/reportKec_'.$kode_wilayah.'_'.$semester_id.'_'.$hari_ini;

        if (!file_exists($filename)){

        	$con = $this->initdb();

			$sql = "SELECT w1.nama,w1.kode_wilayah,
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) AS sma,
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) AS smk,
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END) AS smlb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) AS slb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sma,
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smk,
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smlb,
							SUM(CASE WHEN r.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_slb,
							CASE WHEN CAST(
									(SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
								) = 0 THEN 0 ELSE
							((
								(
									SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END)
								) /
								CAST(
									(SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
								)
							) * 100) END as persen
						FROM dbo.sekolah r INNER JOIN ref.mst_wilayah w1 ON r.kode_wilayah=w1.kode_wilayah
							INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
							INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
							LEFT JOIN (
								SELECT 
									sekolah_id,
									MAX(begin_sync) AS begin_sync
								FROM dbo.sync_log
								WHERE alamat_ip NOT LIKE 'prefil%'
								GROUP BY sekolah_id) l ON r.sekolah_id=l.sekolah_id
						WHERE 
						w1.mst_kode_wilayah = ".$kode_wilayah."
						AND (w1.expired_date is null or w1.expired_date > GETDATE())
						AND r.Soft_delete = 0
						GROUP BY w1.nama,w1.kode_wilayah
						order by persen desc";

			$stmt = sqlsrv_query( $con, $sql );

			$templates = '<table class="table table-striped table-hover " style="font-size:12px">
			                <thead>
			                  <tr>
			                    <th rowspan=2>#</th>
			                    <th rowspan=2>Kecamatan</th>
			                    <th colspan=3>SMA</th>
			                    <th colspan=3>SMK</th>
			                    <th colspan=3>SMLB</th>
			                    <th colspan=3>SLB</th>
			                    <th rowspan=2>Terkirim</th>
			                    <th rowspan=2>%</th>
			                  </tr>
			                  <tr>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  	<th>Jumlah</th>
			                  	<th>Kirim</th>
			                  	<th>Sisa</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';

			$i = 1;

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			      
			   	$sisa_sma = $row['sma'] - $row['kirim_sma'];   
			   	$sisa_smk = $row['smk'] - $row['kirim_smk'];   
			   	$sisa_smlb = $row['smlb'] - $row['kirim_smlb'];  
			   	$sisa_slb = $row['slb'] - $row['kirim_slb'];  

			   	$total = $row['sma'] + $row['smk'] + $row['smlb']+ $row['slb']; 
			   	$total_kirim = $row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb'] + $row['kirim_slb']; 

			   	$persen = ($total_kirim / $total) * 100;

			   	$persen_bulat = round($persen, 2);
			      //$return_arr[] = $row;
				$templates .= '<tr>
			                    <td>'.$i.'</td>
			                    <td><a href="../detailkec/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
			                    <td>'.$row["sma"].'</td>
			                    <td>'.$row["kirim_sma"].'</td>
			                    <td>'.$sisa_sma.'</td>
			                    <td>'.$row["smk"].'</td>
			                    <td>'.$row["kirim_smk"].'</td>
			                    <td>'.$sisa_smk.'</td>
			                    <td>'.$row["smlb"].'</td>
			                    <td>'.$row["kirim_smlb"].'</td>
			                    <td>'.$sisa_smlb.'</td>
			                    <td>'.$row["slb"].'</td>
			                    <td>'.$row["kirim_slb"].'</td>
			                    <td>'.$sisa_slb.'</td>
			                    <td>'.$total_kirim.'</td>
			                    <td>'.$persen_bulat.' %</td>
			                  </tr>';

			    $i++;
			}

			$templates .= ' </tbody>
			              </table>';

			sqlsrv_free_stmt( $stmt);

			$templates .= "<br><div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			file_put_contents($filename, $templates);
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;		
		
	}

	public function reportSekolahPerKecamatan(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah');

		$con = $this->initdb();

		$sql = "SELECT
					s.nama,
					s.sekolah_id,
					s.kode_wilayah,
					s.npsn,
					s.status_sekolah,
					s.bentuk_pendidikan_id,
					(
						SELECT
							COUNT (*)
						FROM
							ptk_terdaftar ptd
						JOIN ptk p ON ptd.ptk_id = p.ptk_id
						WHERE
							ptd.Soft_delete = 0
						AND p.soft_delete = 0
						AND ptd.sekolah_id = s.sekolah_id
						AND ptd.tahun_ajaran_id = ".TAHUN_AJARAN_ID."
						AND ptd.jenis_keluar_id IS NULL
					) AS jml_ptk,
					(
						SELECT
							COUNT (*)
						FROM
							registrasi_peserta_didik rpd
						JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar on ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb on rb.rombongan_belajar_id = ar.rombongan_belajar_id

						WHERE
							rpd.sekolah_id = s.sekolah_id
						AND pd.sekolah_id = s.sekolah_id
						AND rpd.Soft_delete = 0
						AND pd.Soft_delete = 0
						AND rpd.jenis_keluar_id IS NULL
						AND ar.soft_delete = 0
						AND rb.soft_delete = 0
						AND rb.semester_id = 20141
					) AS jml_pd,
					(
						select count (*) from dbo.sync_log where sekolah_id = s.sekolah_id  and alamat_ip NOT LIKE 'prefil%' 
					) as jumlah_kirim
				FROM
					dbo.sekolah s
				WHERE
					s.kode_wilayah = ".$kode_wilayah."
				AND s.soft_delete = 0 
				order by jumlah_kirim desc
				";

		$stmt = sqlsrv_query( $con, $sql );

		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th>#</th>
		                    <th>Sekolah</th>
		                    <th>Jml Sinkronisasi</th>
		                    <th>NPSN</th>
		                    <th>Status Sekolah</th>
		                    <th>Bentuk Pendidikan</th>
		                    <th>PTK</th>
		                    <th>Siswa</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		$i = 1;

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			if($row["status_sekolah"] == 1){
				$status = 'Negeri';
			}else if($row["status_sekolah"] == 2){
				$status = 'Swasta';
			}

			if($row["bentuk_pendidikan_id"] == 13){
				$bentuk = 'SMA';
			}else if($row["bentuk_pendidikan_id"] == 14){
				$bentuk = 'SMLB';
			}else if ($row["bentuk_pendidikan_id"] == 15){
				$bentuk = 'SMK';
			}

			$enkrip = $row['sekolah_id'];

			$sql_enkrip = "SELECT
					CONVERT (
						VARCHAR (20),
						HashBytes ('MD5', CAST('".$enkrip."' AS VARCHAR(50))),
						2
					) AS enkripsi";
			$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
			$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);

			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><a href="../datasekolah/'.$row_enkrip['enkripsi'].'">'.$row["nama"].'</a></td>
		                    <td>'.$row["jumlah_kirim"].'</td>
		                    <td>'.$row["npsn"].'</td>
		                    <td>'.$status.'</td>
		                    <td>'.$bentuk.'</td>
		                    <td>'.$row["jml_ptk"].'</td>
		                    <td>'.$row["jml_pd"].'</td>
		                    
		                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;
	}

	public function detailSekolah(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');

		$con = $this->initdb();
		$unhash = $this->getUnHash($sekolah_id);

		$sql = "SELECT
					s.nama,
					s.npsn,
					s.nss,
					s.bentuk_pendidikan_id,
					s.alamat_jalan,
					s.kode_pos,
					s.status_sekolah,
					s.status_kepemilikan_id,
					s.kode_registrasi,
					s.lintang,
					s.bujur,
					kec.nama AS kecamatan,
					kab.nama AS kota,
					pro.nama AS propinsi
				FROM dbo.sekolah s
					INNER JOIN ref.mst_wilayah kec ON s.kode_wilayah=kec.kode_wilayah
					INNER JOIN ref.mst_wilayah kab ON kec.mst_kode_wilayah=kab.kode_wilayah
					INNER JOIN ref.mst_wilayah pro ON kab.mst_kode_wilayah=pro.kode_wilayah
				WHERE 
					s.sekolah_id = '".$unhash."'";
					// sekolah_id = '".$sekolah_id."'";

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		if($row['bentuk_pendidikan_id'] == 15){
			$bentuk = 'SMK';
		}else if ($row['bentuk_pendidikan_id'] == 14){
			$bentuk = 'SMLB';
		}else if($row['bentuk_pendidikan_id'] == 13){
			$bentuk = 'SMA';
		}

		if($row['status_kepemilikan_id'] == 1){
			$status_milik = 'Pemerintah Pusat';
		}else if ($row['status_kepemilikan_id'] == 2){
			$status_milik = 'Pemerintah Daerah';
		}else if($row['status_kepemilikan_id'] == 3){
			$status_milik = 'Yayasan';
		}else{
			$status_milik = 'Tidak Tercantum';
		}

		$templates = '<div class="panel panel-primary">
		                <div class="panel-heading">
		                  <h3 class="panel-title">Identitas Sekolah</h3>
		                </div>
		                <div class="panel-body" style="font-size:14px">
			                <form class="form-horizontal">
	                			<fieldset>
				                 	<div class="form-group">
					                    <label class="col-lg-2">Nama</label>
					                    <div class="col-lg-10">
					                      	'.$row["nama"].'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">NPSN</label>
					                    <div class="col-lg-10">
					                      	'.$row["npsn"].'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">NSS</label>
					                    <div class="col-lg-10">
					                      	'.$row["nss"].'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">Bentuk Pendidikan</label>
					                    <div class="col-lg-10">
					                      	'.$bentuk.'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">Alamat Jalan</label>
					                    <div class="col-lg-10">
					                      	'.$row["alamat_jalan"].'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">Status Kepemilikan</label>
					                    <div class="col-lg-10">
					                      	'.$status_milik.'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">Kecamatan</label>
					                    <div class="col-lg-10">
					                      	'.$row["kecamatan"].'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">Kabupaten / Kota</label>
					                    <div class="col-lg-10">
					                      	'.$row["kota"].'
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label class="col-lg-2">Propinsi</label>
					                    <div class="col-lg-10">
					                      	'.$row["propinsi"].'
					                    </div>
					                </div>
					            </fieldset>
					        </form>
		                </div>
		              </div>' ;

		return $templates;
	}

	public function petaSekolah($sekolah_id,$con){
		//$sekolah_id = $request->get('sekolah_id');

		//$con = $this->initdb();
		$unhash = $this->getUnHash($sekolah_id);

		$sql = "SELECT
				nama,
				lintang,
				bujur
			FROM
				sekolah
			WHERE
				sekolah_id = '".$unhash."'";

		return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		if(empty($row['lintang']) && empty($row['bujur'])){
			$label = 'Koordinat lokasi '.$row['nama'].' belum diset';
		}else{
			$label = 'Koordinat Lokasi '.$row['nama'];
		}

		if(empty($row['lintang'])){
			$lintang = '-5.443757';
		}else{
			$lintang = $row['lintang'];
		}

		if(empty($row['bujur'])){
			$bujur = '114.421127';
		}else{
			$bujur = $row['bujur'];
		}

		$array = array();

		$array['lintang'] = $lintang;
		$array['bujur'] = $bujur;
		$array['label'] = $label;

		return $array;
		
	}

	public function daftarPdJson(Request $request, Application $app){
		if ($app['session']->get('user')){

			$user = $app['session']->get('user');

			if($user['peran_id'] == 10){
				$sekolah_id = $request->get('sekolah_id');
				$semester_id = $request->get('semester_id');
				$tahun_ajaran = substr($semester_id, 0, 4);

				if($sekolah_id == $user['sekolah_id']){
					$con = $this->initdb();
					$unhash = $this->getUnHash($sekolah_id);

					$sql = "SELECT
						pd.nama,
						pd.jenis_kelamin,
						pd.nisn,
						pd.nik,
						rpd.tanggal_masuk_sekolah
					FROM
						registrasi_peserta_didik rpd
					INNER JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
					INNER JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
					INNER JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
					JOIN ref.semester sm ON sm.semester_id = rb.semester_id
					WHERE
						rpd.Soft_delete = 0
					AND pd.Soft_delete = 0
					AND (
						(
							rpd.jenis_keluar_id IS NULL
							AND rpd.tanggal_keluar IS NULL
						)
						OR (
							rpd.jenis_keluar_id IS NOT NULL
							AND rpd.tanggal_keluar > sm.tanggal_selesai
						)
						OR (rpd.jenis_keluar_id IS NULL)
					)
					AND rb.semester_id = {$semester_id}
					AND rpd.sekolah_id = '{$unhash}'
					AND rb.soft_delete = 0
					AND ar.soft_delete = 0
					AND rb.jenis_rombel = 1";

					// return $sql;die;

					$stmt = sqlsrv_query( $con, $sql );

					$i = 1;

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
						$row['no'] = $i;
						//$row['tanggal'] = $row['tanggal_masuk_sekolah'][0]['date'];

						$return_arr[] = $row;

					    $i++;
					}

					sqlsrv_free_stmt( $stmt);

					if($i > 1){
						$final_arr['draw'] = 1;
						$final_arr['data'] = $return_arr;
					}else{
						$return_arr['no'] = '-';
						$return_arr['nama'] = 'Data Tidak Ditemukan';

						$final_arr['draw'] = 1;
						$final_arr['data'] = $return_arr;
					}

					return json_encode($final_arr);
				}else{
					return "Anda Tidak berhak melihat data ini";
				}

			}else{
				return "Anda Tidak berhak melihat data ini";
			}
		}else{
			return "Anda Tidak berhak melihat data ini";
		}

	}

	public function daftarPtkJson(Request $request, Application $app){
		if ($app['session']->get('user')){

			$user = $app['session']->get('user');

			if($user['peran_id'] == 10){
				$sekolah_id = $request->get('sekolah_id');
				$semester_id = $request->get('semester_id');
				$tahun_ajaran = substr($semester_id, 0, 4);

				if($sekolah_id == $user['sekolah_id']){

					$con = $this->initdb();
					$unhash = $this->getUnHash($sekolah_id);

					$sql = "SELECT
								p.ptk_id,
								p.ptk_id as 'enkrip_ptk_id',
								p.nama,
								p.jenis_kelamin,
								p.nuptk,
								p.status_kepegawaian_id
							FROM
								ptk_terdaftar ptd
							JOIN ptk p ON ptd.ptk_id = p.ptk_id
							JOIN sekolah sk ON sk.sekolah_id = ptd.sekolah_id
							WHERE
								ptd.Soft_delete = 0
							AND p.Soft_delete = 0
							AND sk.sekolah_id = '".$unhash."'
							AND ptd.tahun_ajaran_id = '".$tahun_ajaran."'
							AND ptd.jenis_keluar_id IS NULL";

					$stmt = sqlsrv_query( $con, $sql );

					$i = 1;

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
						$enkrip = $row['enkrip_ptk_id'];

						$sql_enkrip = "SELECT
								CONVERT (
									VARCHAR (20),
									HashBytes ('MD5', CAST('".$enkrip."' AS VARCHAR(50))),
									2
								) AS enkripsi";
						$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
						$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);

						$status_pg = $this->getStatusKepegawaian($row['status_kepegawaian_id']);
						$row['no'] = $i;
						$row['status_kepegawaian_id_str'] = $status_pg;
						$row['ptk_id'] = $row_enkrip['enkripsi'];
						$row['nama'] = '<a href="../detailptk/'.$row_enkrip['enkripsi'].'">'.$row['nama'].'</a>';

						// $row['do5'] = '<a>'.$row_enkrip['enkripsi'].'</a>';

						$return_arr[] = $row;

					    $i++;
					}

					sqlsrv_free_stmt( $stmt);

					if($i > 1){
						$final_arr['draw'] = 1;
						$final_arr['data'] = $return_arr;
					}else{
						$return_arr['no'] = '-';
						$return_arr['nama'] = 'Data Tidak Ditemukan';

						$final_arr['draw'] = 1;
						$final_arr['data'] = $return_arr;
					}

					return json_encode($final_arr);
				}else{
					return "Anda Tidak berhak melihat data ini";
				}
			}else{
				return "Anda Tidak berhak melihat data ini";
			}
		}else{
			return "Anda Tidak berhak melihat data ini";
		}

	}

	public function daftarRombelJson(Request $request, Application $app){
		if ($app['session']->get('user')){

			$user = $app['session']->get('user');

			if($user['peran_id'] == 10){
				$sekolah_id = $request->get('sekolah_id');
				$semester_id = $request->get('semester_id');
				$tahun_ajaran = substr($semester_id, 0, 4);

				if($sekolah_id == $user['sekolah_id']){
					$con = $this->initdb();

					$sql = "SELECT
								ref.tingkat_pendidikan.nama AS tingkat_pendidikan,
								ref.semester.nama AS semester,
								jurusan_sp.nama_jurusan_sp AS jurusan_sp,
								ref.kurikulum.nama_kurikulum AS kurikulum,
								ptk.nama as wali_kelas,
								prasarana.nama as prasarana,
								rombongan_belajar.nama as nama
							FROM
								rombongan_belajar
							JOIN ref.tingkat_pendidikan ON rombongan_belajar.tingkat_pendidikan_id = ref.tingkat_pendidikan.tingkat_pendidikan_id
							JOIN ref.semester ON rombongan_belajar.semester_id = ref.semester.semester_id
							JOIN jurusan_sp ON rombongan_belajar.jurusan_sp_id = jurusan_sp.jurusan_sp_id
							JOIN ref.kurikulum ON rombongan_belajar.kurikulum_id = ref.kurikulum.kurikulum_id
							join prasarana on rombongan_belajar.prasarana_id = prasarana.prasarana_id
							join ptk on rombongan_belajar.ptk_id = ptk.ptk_id
							WHERE
								rombongan_belajar.soft_delete = 0
							AND '".$sekolah_id."' = CONVERT (
								VARCHAR (20),
								HashBytes (
									'MD5',
									CAST (
										rombongan_belajar.sekolah_id AS VARCHAR (50)
									)
								),
								2
							)
							AND rombongan_belajar.semester_id = '".$semester_id."'";

					$stmt = sqlsrv_query( $con, $sql );

					$i = 1;

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
						$row['no'] = $i;
						//$row['tanggal'] = $row['tanggal_masuk_sekolah'][0]['date'];

						$return_arr[] = $row;

					    $i++;
					}

					sqlsrv_free_stmt( $stmt);

					if($i > 1){
						$final_arr['draw'] = 1;
						$final_arr['data'] = $return_arr;
					}else{
						$return_arr['no'] = '-';
						$return_arr['nama'] = 'Data Tidak Ditemukan';

						$final_arr['draw'] = 1;
						$final_arr['data'] = $return_arr;
					}

					return json_encode($final_arr);
				}else{
					return "Anda Tidak berhak melihat data ini";
				}

			}else{
				return "Anda Tidak berhak melihat data ini";
			}
		}else{
			return "Anda Tidak berhak melihat data ini";
		}

	}

	public function daftarPtk(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');

		$con = $this->initdb();

		$sql = "select 
					p.ptk_id, 
					p.nama,
					p.jenis_kelamin,
					p.nuptk,
					p.status_kepegawaian_id
				from ptk_terdaftar ptd JOIN ptk p on ptd.ptk_id = p.ptk_id 
				WHERE
					ptd.Soft_delete = 0
				and
					ptd.sekolah_id = '".$sekolah_id."'
				AND
					ptd.tahun_ajaran_id = 2013
				and
					ptd.jenis_keluar_id IS NULL";

		$stmt = sqlsrv_query( $con, $sql );

		$i = 1;

		$templates = '<table class="table table-striped table-hover" id="ptk_tabel" style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th>#</th>
		                    <th>Nama</th>
		                    <th>Jenis Kelamin</th>
		                    <th>NUPTK</th>
		                    <th>Status Kepegawaian</th>
		             
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			$status_pg = $this->getStatusKepegawaian($row['status_kepegawaian_id']);

			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td>'.$row["nama"].'</td>
		                    <td>'.$row["jenis_kelamin"].'</td>
		                    <td>'.$row["nuptk"].'</td>
		                    <td>'.$status_pg.'</td>
		                    
		                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;
	}



	public function getStatusKepegawaian($id){
		$con = $this->initdb();

		$sql = "select nama from ref.status_kepegawaian where status_kepegawaian_id = ".$id;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return $row['nama'];
	}

	public function logVisitor(){
		$log = new LogVisitor();

		$ip = $_SERVER['REMOTE_ADDR'];
		$u_agent = $_SERVER['HTTP_USER_AGENT'];

		if($ip == '::1'){
			$ip = '127.0.0.1';
		}

		$bname = 'Unknown';
		$platform = 'Unknown';

		//FIND THE OS
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}

		//FIND THE BROWSER
		if (preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}
		elseif (preg_match('/Firefox/i',$u_agent))
		{
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		}
		elseif (preg_match('/Chrome/i',$u_agent))
		{
			$bname = 'Google Chrome';
			$ub = "Chrome";
		}
		elseif (preg_match('/Safari/i',$u_agent))
		{
			$bname = 'Apple Safari';
			$ub = "Safari";
		}
		elseif (preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif (preg_match('/Netscape/i',$u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		} 

		$tanggal = date('Y-m-d');

		try {
			$log->setIp($ip);
			$log->setBrowser($bname);
			$log->setOs($platform);
			$log->setTanggal($tanggal);

			if($log->save()){
				//return new RedirectResponse("../../files/".$file);
				return "{'success' : 'true'}";
			}else{
				return "{'success' : 'false'}";
			}

		} catch (Exception $e) {
			return "{'success' : 'false'}";
		}
	}

	public function pengguna() {
		$c = new \Criteria();

		$penggunas = PenggunaPeer::doSelect($c);

		$outArr = array();

		foreach ($penggunas as $p) {

			$arr = $p->toArray(\BasePeer::TYPE_FIELDNAME);

			$outArr[] = $arr;

		}
	
		return json_encode($outArr);
	}

	public function latestDownload() {
		$con = $this->initdbSqlServerLokal();

		$sql = "select * from web.installer where active = 1";

		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
	}

	public function prevDownload() {
		$c = new \Criteria();

		$c->add(InstallerPeer::ACTIVE, 1, \Criteria::LESS_THAN);
		$c->setOffset(0);
        $c->setLimit(5);
		$c->addDescendingOrderByColumn(InstallerPeer::INSTALLER_ID);

		$downloads = InstallerPeer::doSelect($c);

		$count = InstallerPeer::doCount($c);

		$outArr = array();
		
		foreach ($downloads as $dl) {

			$arr = $dl->toArray(\BasePeer::TYPE_FIELDNAME);

			$outArr[] = $arr;

		}		
	
		return $outArr;
	}

	public function latestPatch() {
		$c = new \Criteria();

		$c->add(PatchPeer::ACTIVE, 1);

		$downloads = PatchPeer::doSelect($c);

		$count = PatchPeer::doCount($c);

		//$outArr = array();
		if($count > 0){
			foreach ($downloads as $dl) {

				$arr = $dl->toArray(\BasePeer::TYPE_FIELDNAME);

				$d = new \Criteria();
				$d->add(InstallerPeer::INSTALLER_ID, $dl->getInstallerId());

				$installer = InstallerPeer::doSelectOne($d);

				$arr['deskripsi_installer'] = $installer->getDeskripsi();

				$outArr[] = $arr;

			}
		}else{
			$arr = NULL;
		}
	
		return $arr;
	}

	public function previousPatch(){
		$c = new \Criteria();

		$c->add(PatchPeer::ACTIVE, 1, \Criteria::LESS_THAN);
		$c->setOffset(0);
        $c->setLimit(5);
		$c->addDescendingOrderByColumn(PatchPeer::INSTALLER_ID);

		$downloads = PatchPeer::doSelect($c);

		$count = PatchPeer::doCount($c);

		$outArr = array();
		
		foreach ($downloads as $dl) {

			$arr = $dl->toArray(\BasePeer::TYPE_FIELDNAME);

			$d = new \Criteria();
			$d->add(InstallerPeer::INSTALLER_ID, $dl->getInstallerId());

			$installer = InstallerPeer::doSelectOne($d);

			$arr['deskripsi_installer'] = $installer->getDeskripsi();

			$outArr[] = $arr;

		}
	
		return $outArr;
	}

	public function getFaq() {
		$c = new \Criteria();

		$faqs = FaqPeer::doSelect($c);

		$count = FaqPeer::doCount($c);

		$outArr = array();

		if($count > 0){
			foreach ($faqs as $f) {

				$arr = $f->toArray(\BasePeer::TYPE_FIELDNAME);

				$outArr[] = $arr;

			}
		}else{
			$outArr = NULL;
		}
	
		return $outArr;
	}

	public function getDokumentasi() {
		$con = $this->initdbSqlServerLokal();
		$outArr = array();

		$sql = "select * from web.[file] where JENIS_FILE_ID = 1";

		$stmt = sqlsrv_query($con, $sql);

		while ($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			array_push($outArr, $row);
		}
	
		return json_encode($outArr);
	}



	public function getBerita() {
		$outArr = array();
		$con = $this->initdbSqlServerLokal();

		$sql = "select top 5 * from web.berita order by berita_id desc";

		$stmt = sqlsrv_query( $con, $sql );
		
		$templates = '<table class="table table-striped table-hover ">
		                <tbody>
		                  ';

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			
			$gambar = $row['GAMBAR'] ? $row['GAMBAR'] : 'no.jpg';

			$templates .= '<tr>
		                    <td>
		                    	<div style="float:left;width:15%;height:120px;border:1px solid #ccc;border-radius:4px;background:url(assets/img/berita/'.$gambar.');background-size:cover;background-repeat:no-repeat;background-position:center center;">
		                    	</div>
		                    	<div style="float:right;width:83%">
			                    	<a href="laman/detailBerita/'.substr($row['TANGGAL'], 0,10).'/'.strtolower(space_char_remover($row['JUDUL'])).'"><b style="font-size:22px">'.$row['JUDUL'].'</b></a><br>
				                    Tanggal: '.DateToIndoReversed($row['TANGGAL']).'<br>'
				                    .substr(strip_tags($row['ISI']), 0,200).'... (<a href="laman/detailBerita/'.substr($row['TANGGAL'], 0,10).'/'.strtolower(space_char_remover($row['JUDUL'])).'">Baca Selengkapnya</a>)
		                    	</div>
		                    	<div class="clear"></div>
			                    
		                    </td>
		                  </tr>';		  


		}
		
		return $templates;
	
	}

	public function getBeritaAll() {
		$outArr = array();

		$con = $this->initdbSqlServerLokal();

		$sql = "select * from web.berita order by berita_id desc";

		$stmt = sqlsrv_query( $con, $sql );
		
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

			$gambar = $row['GAMBAR'] ? $row['GAMBAR'] : 'no.jpg';

			$templates = '<div style="float:left;width:15%;height:120px;border:0px solid #ccc;border-radius:4px;background:url(../assets/img/berita/'.$gambar.');background-size:contain;background-repeat:no-repeat;background-position:center center;">
	                    	</div>
	                    	<div style="float:right;width:83%">
		                    	<a href="../laman/detailBerita/'.substr($row['TANGGAL'], 0,10).'/'.strtolower(space_char_remover($row['JUDUL'])).'"><b style="font-size:22px">'.$row['JUDUL'].'</b></a><br>
			                    Tanggal: '.DateToIndoReversed($row['TANGGAL']).'<br>'
			                    .substr(strip_tags($row['ISI']), 0,300).'... (<a href="../laman/detailBerita/'.substr($row['TANGGAL'], 0,10).'/'.strtolower(space_char_remover($row['JUDUL'])).'">Baca Selengkapnya</a>)
	                    	</div>
	                    	<div class="clear"></div>';
			
			$arr['berita'] = $templates;

			array_push($outArr, $arr); 
		}

		$final_arr['aaData'] = $outArr;

		return json_encode($final_arr);
	
	}

	public function getDetailBerita(Request $request, Application $app){
		$tanggal  = $request->get('tanggal');
		$judul = spasi_kembali($request->get('judul'));

		$con = $this->initdbSqlServerLokal();

		$sql = "SELECT
					*
				FROM
					web.berita
				WHERE
					'{$judul}' = LOWER(judul)
				AND '{$tanggal}' = SUBSTRING(cast(tanggal as varchar(100)), 1, 10)";
		
		$stmt = sqlsrv_query( $con, $sql );

		// return $sql;die;

		$i = 0;
		
		while( $s = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			$id 			= $s['BERITA_ID'];
    		$judul 			= $s['JUDUL'];
    		$pengguna_id 	= $s['PENGGUNA_ID'];
    		$isi 			= $s['ISI'];
    		$tanggal 		= $s['TANGGAL'];
    		$gambar 		= $s['GAMBAR'];
    		$i++;

    		$templates = '<h1 style="font-size:50px">'.$judul.'</h1>';
			$templates .= '<font style="font-size:14px">Diposting Tanggal: '.DateToIndoReversed($tanggal).'</font><hr>';

			if(!empty($gambar)){
				$templates .= '<div style="width:100%;"><img src="../../../assets/img/berita/'.$gambar.'" width="100%" /></div><hr>';
			}
			
			$templates .= '<div style="font-size:16px;line-height:2;text-align:justify">'.$isi.'</div><hr>';
			}
 

		return $templates;

	}


	public function getFormulir() {
		$con = $this->initdbSqlServerLokal();
		$outArr = array();

		$sql = "select * from web.[file] where JENIS_FILE_ID = 2";

		$stmt = sqlsrv_query($con, $sql);

		while ($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			array_push($outArr, $row);
		}
	
		return json_encode($outArr);
	}

	public function getJenisDokumentasi() {
		$c = new \Criteria();

		$jds = JenisDokumentasiPeer::doSelect($c);

		$count = JenisDokumentasiPeer::doCount($c);

		$outArr = array();

		if($count > 0){
			foreach ($jds as $j) {

				$arr = $j->toArray(\BasePeer::TYPE_FIELDNAME);

				$d = new \Criteria();
				$d->add(DokumentasiPeer::JENIS_DOKUMENTASI_ID, $j->getJenisDokumentasiId());

				$dk = DokumentasiPeer::doSelect($d);

				$outArr2 = array();

				foreach ($dk as $dok) {
					$arr2 = $dok->toArray(\BasePeer::TYPE_FIELDNAME);

					$gbr = $dok->getGambar();

					if(empty($gbr)){
						$arr2['GAMBAR'] = 'a.jpg';
					}

					array_push($outArr2, $arr2);
				}

				$arr['ITEMS'] = $outArr2;

				$outArr[] = $arr;
			}
		}else{
			$outArr = NULL;
		}
	
		return $outArr;
	}

	public function getDownload(Request $request, Application $app) {
		$id = $request->get('installer_id');

		$c = new \Criteria();
		$installer = InstallerPeer::retrieveByPK($id);

		$file = $installer->getFile();

		$ip = $_SERVER['REMOTE_ADDR'];
		$u_agent = $_SERVER['HTTP_USER_AGENT'];

		if($ip == '::1'){
			$ip = '127.0.0.1';
		}

		$bname = 'Unknown';
		$platform = 'Unknown';

		//FIND THE OS
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}

		//FIND THE BROWSER
		if (preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}
		elseif (preg_match('/Firefox/i',$u_agent))
		{
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		}
		elseif (preg_match('/Chrome/i',$u_agent))
		{
			$bname = 'Google Chrome';
			$ub = "Chrome";
		}
		elseif (preg_match('/Safari/i',$u_agent))
		{
			$bname = 'Apple Safari';
			$ub = "Safari";
		}
		elseif (preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif (preg_match('/Netscape/i',$u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		}

		$log = new LogDownloadInstaller();

		$tanggal = date('Y-m-d');

		try {
			$log->setInstallerId($id);
			$log->setIp($ip);
			$log->setBrowser($bname);
			$log->setOs($platform);
			$log->setTanggal($tanggal);

			if($log->save()){
				//return new RedirectResponse("../../files/".$file);
				// return $app->redirect("../files/".$file);
				return $app->redirect("http://sourceforge.net/projects/dapodikmen/files/".$file."/download");
			}else{
				return $app->redirect("../");
			}

		} catch (Exception $e) {
			return $app->redirect("../");
		}
	}

	public function getPatch(Request $request, Application $app) {
		$id = $request->get('patch_id');

		$c = new \Criteria();
		$patch = PatchPeer::retrieveByPK($id);

		$file = $patch->getFile();
		$versi = $patch->getVersi();

		$ip = $_SERVER['REMOTE_ADDR'];
		$u_agent = $_SERVER['HTTP_USER_AGENT'];

		if($ip == '::1'){
			$ip = '127.0.0.1';
		}

		$bname = 'Unknown';
		$platform = 'Unknown';

		//FIND THE OS
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}

		//FIND THE BROWSER
		if (preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}
		elseif (preg_match('/Firefox/i',$u_agent))
		{
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		}
		elseif (preg_match('/Chrome/i',$u_agent))
		{
			$bname = 'Google Chrome';
			$ub = "Chrome";
		}
		elseif (preg_match('/Safari/i',$u_agent))
		{
			$bname = 'Apple Safari';
			$ub = "Safari";
		}
		elseif (preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif (preg_match('/Netscape/i',$u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		}

		$log = new LogDownloadPatch();

		$tanggal = date('Y-m-d');

		try {
			$log->setPatchId($id);
			$log->setIp($ip);
			$log->setBrowser($bname);
			$log->setOs($platform);
			$log->setTanggal($tanggal);

			if($log->save()){
				//return new RedirectResponse("../../files/".$file);
				// return $app->redirect("../files/".$file);
				return $app->redirect("http://sourceforge.net/projects/dapodikmen/files/".$file."/download");
				// return $app->redirect("http://master.dl.sourceforge.net/project/dapodikmen/Dapodikmen_".$versi."/".$files);
			}else{
				return $app->redirect("../");
			}

		} catch (Exception $e) {
			return $app->redirect("../");
		}
	}


	public function getDownloadCountToday($id,$app) {
		$c = new \Criteria();

		$c->addAnd(LogDownloadInstallerPeer::INSTALLER_ID, $id);
		$c->addAnd(LogDownloadInstallerPeer::TANGGAL, date('Y-m-d')." 00:00:00", \Criteria::LIKE);

		$counts = LogDownloadInstallerPeer::doCount($c);

		return $counts;
	}

	public function getDownloadCountAll($id,$app) {
		$c = new \Criteria();

		$c->addAnd(LogDownloadInstallerPeer::INSTALLER_ID, $id);
		
		$counts = LogDownloadInstallerPeer::doCount($c);

		return $counts;
	}

	public function getPatchCountToday($patch_id,$app) {
		$c = new \Criteria();

		$c->addAnd(LogDownloadPatchPeer::PATCH_ID, $patch_id);
		$c->addAnd(LogDownloadPatchPeer::TANGGAL, date('Y-m-d')." 00:00:00", \Criteria::LIKE);

		$counts = LogDownloadPatchPeer::doCount($c);

		return $counts;
	}

	public function getPatchCountAll($patch_id,$app) {
		$c = new \Criteria();

		$c->addAnd(LogDownloadPatchPeer::PATCH_ID, $patch_id);
		
		$counts = LogDownloadPatchPeer::doCount($c);

		return $counts;
	}

	public function getJenisFeedback() {
		$c = new \Criteria();

		$jfs = JenisFeedbackPeer::doSelect($c);

		$count = JenisFeedbackPeer::doCount($c);

		$outArr = array();

		if($count > 0){
			foreach ($jfs as $f) {

				$arr = $f->toArray(\BasePeer::TYPE_FIELDNAME);

				$outArr[] = $arr;

			}
		}else{
			$outArr = NULL;
		}
	
		return $outArr;
	}

	public function kirimFeedback(Request $request, Application $app){
		$email = $request->get('txtemail');
		$nama = $request->get('txtnama');
		$sekolah = $request->get('txtsekolah');
		$judul = $request->get('txtjudul');
		$telepon = $request->get('txttelepon');
		$jenis = $request->get('selectjenis');
		$deskripsi = $request->get('textdeskripsi');
		//$gambar = space_char_remover($request->get('filegambar'));

		$files = $request->files->get('filegambar');

		$sekarang = date('Y-m-d H:i:s');

		$feedback = new Feedback();

		try {
			if(!empty($files)){
				$nama_gambar = space_char_remover($files->getClientOriginalName());
			}else{
				$nama_gambar = "";
			}

			$feedback->setEmailPengirim($email);
			$feedback->setJudul($judul);
			$feedback->setDeskripsi($deskripsi);
			$feedback->setGambar($nama_gambar);
			$feedback->setInstansi($sekolah);
			$feedback->setNomorTelepon($telepon);
			$feedback->setJenisFeedbackId($jenis);
			$feedback->setTanggalLapor($sekarang);
			$feedback->setStatus(1);

			if($feedback->save()){

				if($files){
					$path = "screenshot/";
		        	$filename = space_char_remover($files->getClientOriginalName());

		        	if($files->move($path,$filename)){
		        		return $app->redirect("./laman/feedback/berhasil");
		        	}else{
		        		return $app->redirect("./laman/feedback/gagal_upload_gambar");
		        	}
				}else{
					return $app->redirect("./laman/feedback/berhasil");
				}
			}else{
				return $app->redirect("./laman/feedback/gagal");
			}
		} catch (Exception $e) {
			return $app->redirect("/laman/feedback/gagal");
		}
	}

	public function getTentang(){
		$con = $this->initdbSqlServerLokal();

		$sql = "select * from web.artikel where artikel_id = 9";

		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
	
		return json_encode($row);
	}


	public function getCountSyncPerDay(){


		$hari_ini = $this->getToday();

		$filename = 'cache/getCountSyncPerDay/getCountSyncPerDay_'.$hari_ini;

		if (!file_exists($filename)){

			$con = $this->initdb();

			$sql = "SELECT
						TOP 10 COUNT (*) AS value,
						CONVERT (VARCHAR(10), begin_sync, 110) AS label
					FROM
						dbo.sync_log
					where dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
					and sync_media=1
					and begin_sync > '2015'
					and begin_sync < cast (GETDATE() as DATE)
					GROUP BY
						CONVERT (VARCHAR(10), begin_sync, 110)
					ORDER BY
						CONVERT (VARCHAR(10), begin_sync, 110) DESC";

			$stmt = sqlsrv_query( $con, $sql );
			  
			$i = 1;

			$templates = '<table class="table table-striped table-hover ">
			                <thead>
			                  <tr>
			                    <th>#</th>
			                    <th>Tanggal</th>
			                    <th width="20%">Jumlah Sync</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  ';

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

				$label = $row["label"];
				
				$templates .= '<tr>
			                    <td>'.$i.'</td>
			                    <td><b>'.DateToIndo($row["label"]).'</b></td>
			                    <td align="right">'.moneyConverter($row["value"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
			                  </tr>';

			    $i++;
			}

			$templates .= ' </tbody>
			              </table>';

			$templates .= "<div style=font-size:10px>Perubahan per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			sqlsrv_free_stmt( $stmt);

			file_put_contents($filename, $templates);
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;

	}

	function getChart(Request $request, Application $app){
		$con = $this->initdb();

		$model = $request->get('model');

		
		switch ($model) {
			case 'pengirimanNasional':
				
				$sql = "SELECT
						'Terkirim' AS Nama,
						COUNT (DISTINCT(sekolah_id)) AS 'jumlah'
					FROM
						dbo.sync_log
					where dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
					UNION
						SELECT
							'Sisa' AS Nama,
							(
								SELECT
									COUNT (DISTINCT(npsn))
								FROM
									dbo.sekolah
								WHERE
									Soft_delete = 0
							) - (
								SELECT
									COUNT (DISTINCT(sekolah_id)) AS 'jumlah'
								FROM
									dbo.sync_log
						where dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
							) AS 'jumlah'
						FROM
							dbo.sekolah
						WHERE
							Soft_delete = 0";

				$stmt = sqlsrv_query( $con, $sql );

				$return = array();
	
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['key'] = $row['Nama'];
					$array['y'] = $row['jumlah'];

					array_push($return, $array);
				}


				sqlsrv_free_stmt( $stmt);

				return json_encode($return);
				//return $sql;

				break;
			case 'pengirimanSmk':
				$sql = "SELECT
							'Terkirim' AS Nama,
							COUNT (
								DISTINCT (sync_log.sekolah_id)
							) AS 'jumlah'
						FROM
							dbo.sync_log
						JOIN sekolah ON sync_log.sekolah_id = sekolah.sekolah_id
						WHERE
							sekolah.bentuk_pendidikan_id = 15
							and dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
						UNION
							SELECT
							'Sisa' as Nama,
							(select COUNT (DISTINCT(npsn)) from dbo.sekolah WHERE
								Soft_delete = 0
							AND bentuk_pendidikan_id = 15) - (SELECT
							COUNT (
								DISTINCT (sync_log.sekolah_id)
							) AS 'jumlah'
						FROM
							dbo.sync_log

						JOIN sekolah ON sync_log.sekolah_id = sekolah.sekolah_id
						WHERE
							sekolah.bentuk_pendidikan_id = 15
							and dbo.sync_log.alamat_ip NOT LIKE 'prefil%') as 'Jumlah'";

				$stmt = sqlsrv_query( $con, $sql );

				$return = array();
	
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['key'] = $row['Nama'];
					$array['y'] = $row['jumlah'];

					array_push($return, $array);
				}


				sqlsrv_free_stmt( $stmt);

				return json_encode($return);

				break;
			case 'pengirimanSmlb':
				$sql = "SELECT
							'Terkirim' AS Nama,
							COUNT (
								DISTINCT (sync_log.sekolah_id)
							) AS 'jumlah'
						FROM
							dbo.sync_log
						JOIN sekolah ON sync_log.sekolah_id = sekolah.sekolah_id
						WHERE
							sekolah.bentuk_pendidikan_id = 14
							and dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
						UNION
							SELECT
							'Sisa' as Nama,
							(select COUNT (DISTINCT(npsn)) from dbo.sekolah WHERE
								Soft_delete = 0
							AND bentuk_pendidikan_id = 14) - (SELECT
							COUNT (
								DISTINCT (sync_log.sekolah_id)
							) AS 'jumlah'
						FROM
							dbo.sync_log
						JOIN sekolah ON sync_log.sekolah_id = sekolah.sekolah_id
						WHERE
							sekolah.bentuk_pendidikan_id = 14
							and dbo.sync_log.alamat_ip NOT LIKE 'prefil%') as 'jumlah'";

				$stmt = sqlsrv_query( $con, $sql );

				$return = array();
	
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['key'] = $row['Nama'];
					$array['y'] = $row['jumlah'];

					array_push($return, $array);
				}


				sqlsrv_free_stmt( $stmt);

				return json_encode($return);
				break;

			case 'pengirimanSma':
				$sql = "SELECT
							'Terkirim' AS Nama,
							COUNT (
								DISTINCT (sync_log.sekolah_id)
							) AS 'jumlah'
						FROM
							dbo.sync_log
						JOIN sekolah ON sync_log.sekolah_id = sekolah.sekolah_id
						WHERE
							sekolah.bentuk_pendidikan_id = 13
							and dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
						UNION
							SELECT
							'Sisa' as Nama,
							(select COUNT (DISTINCT(npsn)) from dbo.sekolah WHERE
								Soft_delete = 0
							AND bentuk_pendidikan_id = 13) - (SELECT
							COUNT (
								DISTINCT (sync_log.sekolah_id)
							) AS 'jumlah'
						FROM
							dbo.sync_log
						JOIN sekolah ON sync_log.sekolah_id = sekolah.sekolah_id
						WHERE
							sekolah.bentuk_pendidikan_id = 13
							and dbo.sync_log.alamat_ip NOT LIKE 'prefil%') as 'jumlah'";

				$stmt = sqlsrv_query( $con, $sql );

				$return = array();
	
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['key'] = $row['Nama'];
					$array['y'] = $row['jumlah'];

					array_push($return, $array);
				}


				sqlsrv_free_stmt( $stmt);

				return json_encode($return);
				break;
			case 'jumlahSinkron':
				$batas_hari = date('Y-m-d H:i:s',strtotime("-90 days"));
				//return $batas_hari;
				//exit;
				$combinedArr = array();

				$sql = "SELECT
					COUNT (*) AS value,
					CONVERT (VARCHAR(10), begin_sync, 110) AS label
				FROM
					dbo.sync_log
				WHERE begin_sync > '".$batas_hari."'
				AND sync_media = 1
				and dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
				GROUP BY
					CONVERT (VARCHAR(10), begin_sync, 110)	
				ORDER BY CONVERT (VARCHAR(10), begin_sync, 110) ASC --OFFSET 0 ROWS FETCH NEXT 30 ROWS ONLY";

				$stmt = sqlsrv_query( $con, $sql );

				$return = array();
	
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['x'] = DateToIndo($row['label']);
					$array['y'] = $row['value'];

					array_push($return, $array);
				}


				sqlsrv_free_stmt( $stmt);
				
				$finalArr = array();
				$finalArr['key'] = 'Sinkronisasi Online';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				$sql2 = "SELECT
					COUNT (1) AS value,
					CONVERT (VARCHAR(10), begin_sync, 110) AS label
				FROM
					dbo.sync_log
				WHERE begin_sync > '".$batas_hari."'
				AND sync_media = 2
				and dbo.sync_log.alamat_ip NOT LIKE 'prefil%'
				GROUP BY
					CONVERT (VARCHAR(10), begin_sync, 110)	
				ORDER BY CONVERT (VARCHAR(10), begin_sync, 110) ASC --OFFSET 0 ROWS FETCH NEXT 30 ROWS ONLY";

				$stmt2 = sqlsrv_query( $con, $sql2 );

				$return2 = array();
	
				while($row2 = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['x'] = DateToIndo($row2['label']);
					
					// if(!$row2['value']){
					// 	$value = 1;
					// }else{
					// 	$value = $row2['value'];
					// }

					// $array['y'] = $value;
					$array['y'] = $row2['value'];

					array_push($return2, $array);
				}

				// sqlsrv_free_stmt( $stmt2);
				
				// $finalArr2 = array();
				// $finalArr2['key'] = 'Sinkronisasi Offline';
				// $finalArr2['values'] = $return2;

				// array_push($combinedArr, $finalArr2);

				return json_encode($combinedArr);
		  		//return $sql;

				break;

			case 'rekapPropinsi':
				$combinedArr = array();

				$sql = "SELECT
						mst.nama as label,
						(
							SELECT
								COUNT (*)
							FROM
								dbo.sekolah s
							JOIN ref.mst_wilayah mst0 ON s.kode_wilayah = mst0.kode_wilayah
							JOIN ref.mst_wilayah mst1 ON mst0.mst_kode_wilayah = mst1.kode_wilayah
							JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
							WHERE
								mst2.kode_wilayah = mst.kode_wilayah
							AND s.bentuk_pendidikan_id = 14
						) AS smlb_total,
						(
							SELECT
								COUNT (DISTINCT(sl.sekolah_id))
							FROM
								dbo.sync_log sl
							JOIN dbo.sekolah ss ON ss.sekolah_id = sl.sekolah_id
							JOIN ref.mst_wilayah mst3 ON ss.kode_wilayah = mst3.kode_wilayah
							JOIN ref.mst_wilayah mst4 ON mst3.mst_kode_wilayah = mst4.kode_wilayah
							JOIN ref.mst_wilayah mst5 ON mst4.mst_kode_wilayah = mst5.kode_wilayah
							WHERE
								ss.bentuk_pendidikan_id = 14
							AND mst5.kode_wilayah = mst.kode_wilayah
							and sl.alamat_ip NOT LIKE 'prefil%'
						) AS smlb_kirim
					FROM
						ref.mst_wilayah mst
					WHERE
						mst.id_level_wilayah = 1";

				$stmt = sqlsrv_query( $con, $sql );

				$return = array();
	
				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['x'] = $row['label'];

					$persen = ($row['smlb_kirim'] / $row['smlb_total']) * 100;

					//$array['y'] = round($persen,2);
					// $array['y'] = ceil($persen);
					$array['y'] = $row['smlb_kirim'];

					array_push($return, $array);
				}


				sqlsrv_free_stmt( $stmt);

				$finalArr = array();
				$finalArr['key'] = 'SMLB';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				// SMK
				$sql2 = "SELECT
						mst.nama as label,
						(
							SELECT
								COUNT (*)
							FROM
								dbo.sekolah s
							JOIN ref.mst_wilayah mst0 ON s.kode_wilayah = mst0.kode_wilayah
							JOIN ref.mst_wilayah mst1 ON mst0.mst_kode_wilayah = mst1.kode_wilayah
							JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
							WHERE
								mst2.kode_wilayah = mst.kode_wilayah
							AND s.bentuk_pendidikan_id = 15
						) AS smk_total,
						(
							SELECT
								COUNT sl.sekolah_id
							FROM
								dbo.sync_log sl
							JOIN dbo.sekolah ss ON ss.sekolah_id = sl.sekolah_id
							JOIN ref.mst_wilayah mst3 ON ss.kode_wilayah = mst3.kode_wilayah
							JOIN ref.mst_wilayah mst4 ON mst3.mst_kode_wilayah = mst4.kode_wilayah
							JOIN ref.mst_wilayah mst5 ON mst4.mst_kode_wilayah = mst5.kode_wilayah
							WHERE
								ss.bentuk_pendidikan_id = 15
							AND mst5.kode_wilayah = mst.kode_wilayah
							and sl.alamat_ip NOT LIKE 'prefil%'
						) AS smk_kirim
					FROM
						ref.mst_wilayah mst
					WHERE
						mst.id_level_wilayah = 1";

				$stmt2 = sqlsrv_query( $con, $sql2 );

				$return2 = array();
	
				while($row2 = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['x'] = $row2['label'];

					$persen = ($row2['smk_kirim'] / $row2['smk_total']) * 100;

					//$array['y'] = round($persen,2);
					// $array['y'] = ceil($persen);
					$array['y'] = $row2['smk_kirim'];

					array_push($return2, $array);
				}


				sqlsrv_free_stmt( $stmt2);

				$finalArr2 = array();
				$finalArr2['key'] = 'SMK';
				$finalArr2['values'] = $return2;

				array_push($combinedArr, $finalArr2);

				// SMA
				$sql3 = "SELECT
						mst.nama as label,
						(
							SELECT
								COUNT (*)
							FROM
								dbo.sekolah s
							JOIN ref.mst_wilayah mst0 ON s.kode_wilayah = mst0.kode_wilayah
							JOIN ref.mst_wilayah mst1 ON mst0.mst_kode_wilayah = mst1.kode_wilayah
							JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
							WHERE
								mst2.kode_wilayah = mst.kode_wilayah
							AND s.bentuk_pendidikan_id = 13
						) AS sma_total,
						(
							SELECT
								COUNT (DISTINCT(sl.sekolah_id))
							FROM
								dbo.sync_log sl
							JOIN dbo.sekolah ss ON ss.sekolah_id = sl.sekolah_id
							JOIN ref.mst_wilayah mst3 ON ss.kode_wilayah = mst3.kode_wilayah
							JOIN ref.mst_wilayah mst4 ON mst3.mst_kode_wilayah = mst4.kode_wilayah
							JOIN ref.mst_wilayah mst5 ON mst4.mst_kode_wilayah = mst5.kode_wilayah
							WHERE
								ss.bentuk_pendidikan_id = 13
							AND mst5.kode_wilayah = mst.kode_wilayah
							and sl.alamat_ip NOT LIKE 'prefil%'
						) AS sma_kirim
					FROM
						ref.mst_wilayah mst
					WHERE
						mst.id_level_wilayah = 1";

				$stmt3 = sqlsrv_query( $con, $sql3 );

				$return3 = array();
	
				while($row3 = sqlsrv_fetch_array( $stmt3, SQLSRV_FETCH_ASSOC)){
					$array = array();

					$array['x'] = $row3['label'];

					$persen = ($row3['sma_kirim'] / $row3['sma_total']) * 100;

					//$array['y'] = round($persen,2);
					// $array['y'] = ceil($persen);
					$array['y'] = $row['sma_kirim'];

					array_push($return3, $array);
				}


				sqlsrv_free_stmt( $stmt3 );

				$finalArr3 = array();
				$finalArr3['key'] = 'SMA';
				$finalArr3['values'] = $return3;

				array_push($combinedArr, $finalArr3);

				return json_encode($combinedArr);

				break;

			case 'rekapPdNasional':
				
				// $batas_hari = date('Y-m-d H:i:s',strtotime("-20 days"));
				//return $batas_hari;
				//exit;
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d',strtotime("-30 days"));

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini, \Criteria::GREATER_THAN);
				$c->addAscendingOrderByColumn(RekapJumlahTotalPeer::TANGGAL);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				$count = RekapJumlahTotalPeer::doCount($c);

				$return  = array();
				$return2 = array();

				$nilai_kemarin = 0;

				foreach ($rekap as $r) {

					if($nilai_kemarin > 0){

						$naikya = $r->getPdNasional() - $nilai_kemarin;

						$persentase = ( $naikya / $r->getPdNasional() ) * 100;

						$array = array();

						$array['x'] = DateToIndo($r->getTanggal());
						$array['y'] = round($persentase, 3);

						array_push($return, $array);

					}

					$nilai_kemarin = $r->getPdNasional();
					
					// $array = array();

					// $array['x'] = DateToIndo($r->getTanggal());
					// $array['y'] = $r->getPdNasional();

					// array_push($return, $array);

					// $array2 = array();

					// $array2['x'] = DateToIndo($r->getTanggal());
					// $array2['y'] = $r->getPtkNasional();

					// array_push($return2, $array2);


				}

				$finalArr = array();
				$finalArr['key'] = 'Peningkatan PD Per Hari (%)';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				// $finalArr2 = array();
				// $finalArr2['key'] = 'Jumlah PTK';
				// $finalArr2['values'] = $return2;

				// array_push($combinedArr, $finalArr2);

				return json_encode($combinedArr);

				break;

			case 'rekapPtkNasional':
				
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d',strtotime("-30 days"));

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini, \Criteria::GREATER_THAN);
				$c->addAscendingOrderByColumn(RekapJumlahTotalPeer::TANGGAL);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				$count = RekapJumlahTotalPeer::doCount($c);

				$return  = array();
				$return2 = array();

				$nilai_kemarin = 0;

				foreach ($rekap as $r) {

					if($nilai_kemarin > 0){

						$naikya = $r->getPtkNasional() - $nilai_kemarin;

						$persentase = ( $naikya / $r->getPtkNasional() ) * 100;

						$array = array();

						$array['x'] = DateToIndo($r->getTanggal());
						$array['y'] = round($persentase, 3);

						array_push($return, $array);

					}

					$nilai_kemarin = $r->getPtkNasional();
					
					// $array = array();

					// $array['x'] = DateToIndo($r->getTanggal());
					// $array['y'] = $r->getPtkNasional();

					// array_push($return, $array);

					// $array2 = array();

					// $array2['x'] = DateToIndo($r->getTanggal());
					// $array2['y'] = $r->getPtkNasional();

					// array_push($return2, $array2);


				}

				$finalArr = array();
				$finalArr['key'] = 'Peningkatan PTK Per Hari (%)';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				// $finalArr2 = array();
				// $finalArr2['key'] = 'Jumlah PTK';
				// $finalArr2['values'] = $return2;

				// array_push($combinedArr, $finalArr2);

				return json_encode($combinedArr);

				break;

			case 'rekapSekolahNasional':
				
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d',strtotime("-30 days"));

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini, \Criteria::GREATER_THAN);
				$c->addAscendingOrderByColumn(RekapJumlahTotalPeer::TANGGAL);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				$count = RekapJumlahTotalPeer::doCount($c);

				$return  = array();
				$return2 = array();

				$nilai_kemarin = 0;

				foreach ($rekap as $r) {

					if($nilai_kemarin > 0){

						$naikya = $r->getSekolahNasional() - $nilai_kemarin;

						$persentase = ( $naikya / $r->getSekolahNasional() ) * 100;

						$array = array();

						$array['x'] = DateToIndo($r->getTanggal());
						$array['y'] = round($persentase, 3);

						array_push($return, $array);

					}

					$nilai_kemarin = $r->getSekolahNasional();
					
					// $array = array();

					// $array['x'] = DateToIndo($r->getTanggal());
					// $array['y'] = $r->getSekolahNasional();
				}

				$finalArr = array();
				$finalArr['key'] = 'Peningkatan Sekolah Per Hari (%)';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				return json_encode($combinedArr);

				break;

			case 'persentaseSekolahNasional':
				
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d');

				$c->add(JumlahSekolahPeer::TANGGAL, $hari_ini);
				$c->add(JumlahSekolahPeer::ID_LEVEL_WILAYAH, 1);
				$c->add(JumlahSekolahPeer::SEMESTER_ID, '20141');

				$rekap = JumlahSekolahPeer::doSelect($c);

				$count = JumlahSekolahPeer::doCount($c);

				$return = array();
	
				foreach ($rekap as $r) {
					$i = $i + $r->getStatusSmaNegeri();
					$j = $j + $r->getStatusSmaSwasta();

					$k = $k + $r->getStatusSmkNegeri();
					$l = $l + $r->getStatusSmkSwasta();

					$m = $m + $r->getStatusSmlbNegeri();
					$n = $n + $r->getStatusSmlbSwasta();
				}

				$array = array();

				$array['key'] = 'SMA';
				$array['y'] = $i + $j;

				array_push($return, $array);

				$array2 = array();

				$array2['key'] = 'SMK';
				$array2['y'] = $k + $l;

				array_push($return, $array2);

				$array3 = array();

				$array3['key'] = 'SMLB';
				$array3['y'] = $m + $n;

				array_push($return, $array3);

				return json_encode($return);

				break;
			case 'persentasePtkNasional':
				
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d');

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				$count = RekapJumlahTotalPeer::doCount($c);

				$return = array();
	
				foreach ($rekap as $r) {
					$array = array();

					$array['key'] = 'PTK SMA';
					$array['y'] = $r->getPtkSmaNasional();

					array_push($return, $array);

					$array2 = array();

					$array2['key'] = 'PTK SMK';
					$array2['y'] = $r->getPtkSmkNasional();

					array_push($return, $array2);

					$array3 = array();

					$array3['key'] = 'PTK SMLB';
					$array3['y'] = $r->getPtkSmlbNasional();

					array_push($return, $array3);
				}

				return json_encode($return);

				break;

			case 'persentasePdNasional':
				
				$combinedArr = array();

				// $c = new \Criteria();

				// $hari_ini = date('Y-m-d');

				// $c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini);

				// $rekap = RekapJumlahTotalPeer::doSelect($c);

				// $count = RekapJumlahTotalPeer::doCount($c);

				$hari_ini = date('Y-m-d');

				$d = new \Criteria();
				$d->add(JumlahPdStatusPeer::TANGGAL, $hari_ini);
				$d->add(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

				$rekapPd = JumlahPdStatusPeer::doSelect($d);

				$i = 0;
				$j = 0;
				$k = 0;

				foreach ($rekapPd as $rpd) {
					
					$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
					$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
					$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());

				}

				$return = array();

				$array = array();

				$array['key'] = 'PD SMA';
				$array['y'] = $i;

				array_push($return, $array);

				$array2 = array();

				$array2['key'] = 'PD SMK';
				$array2['y'] = $j;

				array_push($return, $array2);

				$array3 = array();

				$array3['key'] = 'PD SMLB';
				$array3['y'] = $k;

				array_push($return, $array3);
	
				// foreach ($rekap as $r) {
				// 	$array = array();

				// 	$array['key'] = 'Peserta Didik SMA';
				// 	$array['y'] = $r->getPdSmaNasional();

				// 	array_push($return, $array);

				// 	$array2 = array();

				// 	$array2['key'] = 'Peserta Didik SMK';
				// 	$array2['y'] = $r->getPdSmkNasional();

				// 	array_push($return, $array2);

				// 	$array3 = array();

				// 	$array3['key'] = 'Peserta Didik SMLB';
				// 	$array3['y'] = $r->getPdSmlbNasional();

				// 	array_push($return, $array3);
				// }

				return json_encode($return);

				break;

			case 'rekapSekolahNasionalPerBp':
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d',strtotime("-90 days"));

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini, \Criteria::GREATER_THAN);
				$c->addAscendingOrderByColumn(RekapJumlahTotalPeer::TANGGAL);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				$count = RekapJumlahTotalPeer::doCount($c);

				$return  = array();
				$return2 = array();
				$return3 = array();

				foreach ($rekap as $r) {
					
					$array = array();

					$array['x'] = DateToIndo($r->getTanggal());
					$array['y'] = $r->getSmaNasional();

					array_push($return, $array);

					$array2 = array();

					$array2['x'] = DateToIndo($r->getTanggal());
					$array2['y'] = $r->getSmkNasional();

					array_push($return2, $array2);

					$array3 = array();

					$array3['x'] = DateToIndo($r->getTanggal());
					$array3['y'] = $r->getSmlbNasional();

					array_push($return3, $array3);
				}

				$finalArr = array();
				$finalArr['key'] = 'SMA';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				$finalArr2 = array();
				$finalArr2['key'] = 'SMK';
				$finalArr2['values'] = $return2;

				array_push($combinedArr, $finalArr2);

				$finalArr3 = array();
				$finalArr3['key'] = 'SMLB';
				$finalArr3['values'] = $return3;

				array_push($combinedArr, $finalArr3);

				return json_encode($combinedArr);

				break;

			case 'rekapPtkNasionalPerBp':
				$combinedArr = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d',strtotime("-70 days"));

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini, \Criteria::GREATER_THAN);
				$c->addAscendingOrderByColumn(RekapJumlahTotalPeer::TANGGAL);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				$count = RekapJumlahTotalPeer::doCount($c);

				$return  = array();
				$return2 = array();
				$return3 = array();

				foreach ($rekap as $r) {
					
					$array = array();

					$array['x'] = $r->getTanggal();
					// $array['x'] = DateToIndo($r->getTanggal());
					$array['y'] = $r->getPtkSmaNasional();

					array_push($return, $array);

					$array2 = array();

					$array2['x'] = $r->getTanggal();
					$array2['y'] = $r->getPtkSmkNasional();

					array_push($return2, $array2);

					$array3 = array();

					$array3['x'] = $r->getTanggal();
					$array3['y'] = $r->getPtkSmlbNasional();

					array_push($return3, $array3);
				}

				$finalArr = array();
				$finalArr['key'] = 'PTK SMA';
				$finalArr['values'] = $return;

				array_push($combinedArr, $finalArr);

				$finalArr2 = array();
				$finalArr2['key'] = 'PTK SMK';
				$finalArr2['values'] = $return2;

				array_push($combinedArr, $finalArr2);

				$finalArr3 = array();
				$finalArr3['key'] = 'PTK SMLB';
				$finalArr3['values'] = $return3;

				array_push($combinedArr, $finalArr3);

				return json_encode($combinedArr);

				break;
			case 'baselineSekolahNasional':

				$return  = array();
		
				$i = 0;
				$j = 0;
				$k = 0;

				$c = new \Criteria();

				$hari_ini = date('Y-m-d');

				// $hari_ini .= " 00:00:00";

				$c->addAnd(JumlahSekolahPeer::TANGGAL, $hari_ini);
				$c->addAnd(JumlahSekolahPeer::ID_LEVEL_WILAYAH, 1);
				$c->addAnd(JumlahSekolahPeer::SEMESTER_ID, '20141');

				$rekap = JumlahSekolahPeer::doSelect($c);

				foreach ($rekap as $rpd) {
					// $i = $i + $r->getPdDiRombel();
					// $j = $j + $r->getPdBelumDiRombel();
					$i = $i+ ($rpd->getStatusSmaNegeri() + $rpd->getStatusSmaSwasta());
					$j = $j+ ($rpd->getStatusSmkNegeri() + $rpd->getStatusSmkSwasta());
					$k = $k+ ($rpd->getStatusSmlbNegeri() + $rpd->getStatusSmlbSwasta());
				}

				// -- start
				$values = array();

				$a = array();
				$a['x'] = '2014/2015';
				$a['y'] = $i;

				$a2 = array();
				$a2['x'] = '2013/2014';
				$a2['y'] = 12409;

				array_push($values, $a2);
				array_push($values, $a);

				$array = array();
				$array['key'] = 'SMA';
				$array['values'] = $values;

				array_push($return, $array);
				// -- end

				// -- start
				$values2 = array();

				$b2 = array();
				$b2['x'] = '2013/2014';
				$b2['y'] = 11726;

				$b = array();
				$b['x'] = '2014/2015';
				$b['y'] = $j;

				array_push($values2, $b2);
				array_push($values2, $b);

				$array2 = array();
				$array2['key'] = 'SMK';
				$array2['values'] = $values2;

				array_push($return, $array2);
				// -- end

				// -- start
				$values3 = array();

				$c = array();
				$c['x'] = '2014/2015';
				$c['y'] = $k;

				$c2 = array();
				$c2['x'] = '2013/2014';
				$c2['y'] = 983;

				array_push($values3, $c2);
				array_push($values3, $c);

				$array3 = array();
				$array3['key'] = 'SMLB';
				$array3['values'] = $values3;

				array_push($return, $array3);
				// -- end

				return json_encode($return);
				break;

			case 'baselinePdNasional':
				$return  = array();

				$hari_ini = date('Y-m-d');

				$d = new \Criteria();
				$d->add(JumlahPdStatusPeer::TANGGAL, $hari_ini);
				$d->add(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

				$rekapPd = JumlahPdStatusPeer::doSelect($d);

				$i = 0;
				$j = 0;
				$k = 0;

				foreach ($rekapPd as $rpd) {
					
					$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
					$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
					$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());

				}

				// -- start
				$values = array();

				$a = array();
				$a['x'] = '2014/2015';
				$a['y'] = $i;

				$a2 = array();
				$a2['x'] = '2013/2014';
				$a2['y'] = 4230314;

				array_push($values, $a2);
				array_push($values, $a);

				$array = array();
				$array['key'] = 'PD SMA';
				$array['values'] = $values;

				array_push($return, $array);
				// -- end

				// -- start
				$values2 = array();

				$b2 = array();
				$b2['x'] = '2013/2014';
				$b2['y'] = 4179025;

				$b = array();
				$b['x'] = '2014/2015';
				$b['y'] = $j;

				array_push($values2, $b2);
				array_push($values2, $b);

				$array2 = array();
				$array2['key'] = 'PD SMK';
				$array2['values'] = $values2;

				array_push($return, $array2);
				// -- end

				// -- start
				$values3 = array();

				$c = array();
				$c['x'] = '2014/2015';
				$c['y'] = $k;

				$c2 = array();
				$c2['x'] = '2013/2014';
				$c2['y'] = 10297;

				array_push($values3, $c2);
				array_push($values3, $c);

				$array3 = array();
				$array3['key'] = 'PD SMLB';
				$array3['values'] = $values3;

				array_push($return, $array3);
				// -- end

				return json_encode($return);
				break;

			case 'baselinePtkNasional':
				$return  = array();

				$c = new \Criteria();

				$hari_ini = date('Y-m-d');

				$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini);

				$rekap = RekapJumlahTotalPeer::doSelect($c);

				foreach ($rekap as $r) {
					$i = $r->getPtkSmaNasional();
					$j = $r->getPtkSmkNasional();
					$k = $r->getPtkSmlbNasional();
				}
				

				// -- start
				$values = array();

				$a = array();
				$a['x'] = '2014/2015';
				$a['y'] = $i;

				$a2 = array();
				$a2['x'] = '2013/2014';
				$a2['y'] = 291120+82256;

				array_push($values, $a2);
				array_push($values, $a);

				$array = array();
				$array['key'] = 'PTK SMA';
				$array['values'] = $values;

				array_push($return, $array);
				// -- end

				// -- start
				$values2 = array();

				$b2 = array();
				$b2['x'] = '2013/2014';
				$b2['y'] = 305647+68897;

				$b = array();
				$b['x'] = '2014/2015';
				$b['y'] = $j;

				array_push($values2, $b2);
				array_push($values2, $b);

				$array2 = array();
				$array2['key'] = 'PTK SMK';
				$array2['values'] = $values2;

				array_push($return, $array2);
				// -- end

				// -- start
				$values3 = array();

				$c = array();
				$c['x'] = '2014/2015';
				$c['y'] = $k;

				$c2 = array();
				$c2['x'] = '2013/2014';
				$c2['y'] = 16102+360;

				array_push($values3, $c2);
				array_push($values3, $c);

				$array3 = array();
				$array3['key'] = 'PTK SMLB';
				$array3['values'] = $values3;

				array_push($return, $array3);
				// -- end

				return json_encode($return);
				break;

			default:
				# code...
				break;
		}

		
	}

	public function getListBerita(Request $request, Application $app){
		$offset = $request->get('offset');

		$con = $this->initdb();

		$sql = "SELECT w2.nama,w2.kode_wilayah, w3.nama as prop,
					(
						(
							SUM(CASE WHEN r.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) + 
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END)
						) /
						CAST(
							(SUM(CASE WHEN r.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
							SUM(CASE WHEN r.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
							SUM(CASE WHEN r.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
						)
					) * 100 as persen

				FROM dbo.sekolah r
					INNER JOIN ref.mst_wilayah w1 ON r.kode_wilayah=w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
					LEFT JOIN (
						SELECT 
							sekolah_id,
							MAX(begin_sync) AS begin_sync
						FROM dbo.sync_log
						WHERE alamat_ip NOT LIKE 'prefil%'
						GROUP BY sekolah_id) l ON r.sekolah_id=l.sekolah_id
				WHERE w2.id_level_wilayah=2
				GROUP BY w2.nama,w2.kode_wilayah,w3.nama
				order by persen desc OFFSET ".$offset." ROWS FETCH NEXT 50 ROWS ONLY";

		$stmt = sqlsrv_query( $con, $sql );

		$templates = '';
		$i = $offset;

		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			$i++;

			$templates .= '<a href="#!infografik-'.$row['kode_wilayah'].'" class="list-group-item 541_menu">
			    <div style="float:left;width:5%">'.$i.'.</div>
			    <div style="float:left;width:40%">'.$row['nama'].'</div>
			    <div style="float:left;width:30%">'.$row['prop'].'</div>
			    <div style="float:right;width:19%;text-align:right"><b>'.round($row['persen'],2).' %</b></div>
			    <div class="clear"></div>
			  </a>';
		}

		if($i > $offset){
			$templates .= '<a class="list-group-item load-list" style="text-align:center"><img src="../assets/img/loading8.gif" height="50px"/>&nbsp; Memuat Daftar...</a>';

		}else{
			$templates .= '<a class="list-group-item load-list" style="text-align:center"><b>Akhir dari Daftar</b></a>';

		}

		
		return $templates;
	}

	function resting(Request $request, Application $app){
		$model = $request->get('model');
		$kode = $request->get('kode');

		$con = $this->initdb();

		switch ($model) {
			case 'propinsi':
				
				$sql = "select * from ref.mst_wilayah where id_level_wilayah = 1";

				$stmt = sqlsrv_query( $con, $sql );

				$templates = '<option value="0">Tampilkan Semua</option>';
				$i = 0;

				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
					$i++;

					$templates .= '<option value="'.$row['kode_wilayah'].'">'.$row['nama'].'</option>';
				}


				break;
			case 'kabkota':
				
				$sql = "select * from ref.mst_wilayah where id_level_wilayah = 2 and mst_kode_wilayah = ".$kode;

				$stmt = sqlsrv_query( $con, $sql );

				$templates = '<option value="0">Tampilkan Semua</option>';
				$i = 0;

				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
					$i++;

					$templates .= '<option value="'.$row['kode_wilayah'].'">'.$row['nama'].'</option>';
				}

				break;
			
			case 'kecamatan':
				
				$sql = "select * from ref.mst_wilayah where id_level_wilayah = 3 and mst_kode_wilayah = ".$kode;

				$stmt = sqlsrv_query( $con, $sql );

				$templates = '<option value="0">Tampilkan Semua</option>';
				$i = 0;

				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
					$i++;

					$templates .= '<option value="'.$row['kode_wilayah'].'">'.$row['nama'].'</option>';
				}

				break;

			case 'paketkeahlian':

				if($kode == 15){
					$sql = "select * from ref.jurusan where untuk_smk = 1 and level_bidang_id = 12 and expired_date is null";

				}else if($kode == 13){
					$sql = "select * from ref.jurusan where untuk_sma = 1 and level_bidang_id = 11 and expired_date is null";

				}else if($kode == 14){
					$sql = "select * from ref.jurusan where untuk_sma = 1 and level_bidang_id = 12 and expired_date is null";

				}
				
				
				$stmt = sqlsrv_query( $con, $sql );

				$templates = '<option value="0">Tampilkan Semua</option>';
				$i = 0;

				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
					$i++;

					$templates .= '<option value="'.$row['jurusan_id'].'">'.$row['nama_jurusan'].'</option>';
				}

				break;
			
			case 'hitungtotal':
				$i = 0;
				
				$bentuk_pendidikan = $request->get('bentuk_pendidikan');
				$status = $request->get('status');
				$paketkeahlian = $request->get('paket_keahlian');
				$propinsi = $request->get('propinsi');
				$kabkota = $request->get('kabkota');
				$kecamatan = $request->get('kecamatan');
				$keyword = $request->get('keyword_sekolah');

				if($paketkeahlian != '0'){

					$sql = "SELECT
						a.nama as 'nama_sekolah',
						mst1.nama as 'kecamatan',
						mst2.nama as 'kabupaten',
						mst3.nama as 'propinsi',
						a.alamat_jalan,
						a.nomor_telepon,
						j.nama_jurusan,
						a.status_sekolah
					FROM
						jurusan_sp jsp
					JOIN sekolah a ON a.sekolah_id = jsp.sekolah_id
					JOIN ref.mst_wilayah mst1 ON a.kode_wilayah = mst1.kode_wilayah
					JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
					JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
					join ref.jurusan j on jsp.jurusan_id = j.jurusan_id
					where jsp.jurusan_id = '".$paketkeahlian."'";

					if($bentuk_pendidikan != 0){
						$sql .= " and a.bentuk_pendidikan_id = ".$bentuk_pendidikan;
					}

					if($status != 0){
						$sql .= " and a.status_sekolah = ".$status;
					}

					if($propinsi != 0){
						$sql .= " and mst3.kode_wilayah = ".$propinsi;
					}

					if($kabkota != 0){
						$sql .= " and mst2.kode_wilayah = ".$kabkota;
					}

					if($kecamatan != 0){
						$sql .= " and mst1.kode_wilayah = ".$kecamatan;
					}

					if(!empty($keyword)){
						$sql .= " and (a.nama like '%".$keyword."%' OR a.npsn like '%".$keyword."%')";
					}

					$stmt = sqlsrv_query( $con, $sql );

					$templates = '';

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {						

						$i++;

					}

				}else{

					$sql = "SELECT
						mst1.nama AS kecamatan,
						mst2.nama AS kabupaten,
						mst3.nama AS propinsi,
						a.kode_wilayah AS kode_kecamatan,
						mst2.kode_wilayah AS kode_kab,
						mst3.kode_wilayah AS kode_prop,
						a.nama as nama_sekolah,
						*
					FROM
						sekolah a
					JOIN ref.mst_wilayah mst1 ON a.kode_wilayah = mst1.kode_wilayah
					JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
					JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
					WHERE
						a.Soft_delete = 0";

					if($bentuk_pendidikan != 0){
						$sql .= " and a.bentuk_pendidikan_id = ".$bentuk_pendidikan;
					}

					if($status != 0){
						$sql .= " and a.status_sekolah = ".$status;
					}

					if($propinsi != 0){
						$sql .= " and mst3.kode_wilayah = ".$propinsi;
					}

					if($kabkota != 0){
						$sql .= " and mst2.kode_wilayah = ".$kabkota;
					}

					if($kecamatan != 0){
						$sql .= " and mst1.kode_wilayah = ".$kecamatan;
					}

					if(!empty($keyword)){
						$sql .= " and (a.nama like '%".$keyword."%' OR a.npsn like '%".$keyword."%')";
					}

					$stmt = sqlsrv_query( $con, $sql );

					$templates = '';

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {						

						$i++;	

					}

				}

				if($bentuk_pendidikan == 13){
					$bp = 'SMA';
				}else if ($bentuk_pendidikan == 14){
					$bp = 'SMLB';
				}else if ($bentuk_pendidikan == 15){
					$bp = 'SMK';
				}else{
					$bp = 'Semua Bentuk Pendidikan';
				}

				if($status == 1){
					$st = 'Negeri';
				}else if ($status == 2){
					$st = 'Swasta';
				}else{
					$st = 'Semua Status Sekolah';
				}

				$sql_pk = "select * from ref.jurusan where jurusan_id = '".$paketkeahlian."'";
				$stmt_pk = sqlsrv_query( $con, $sql_pk );
				$row_pk = sqlsrv_fetch_array( $stmt_pk, SQLSRV_FETCH_ASSOC);

				$sql_prop = "select * from ref.mst_wilayah where kode_wilayah = '".$propinsi."'";
				$stmt_prop = sqlsrv_query( $con, $sql_prop );
				$row_prop = sqlsrv_fetch_array( $stmt_prop, SQLSRV_FETCH_ASSOC);

				$sql_kab = "select * from ref.mst_wilayah where kode_wilayah = '".$kabkota."'";
				$stmt_kab = sqlsrv_query( $con, $sql_kab );
				$row_kab = sqlsrv_fetch_array( $stmt_kab, SQLSRV_FETCH_ASSOC);

				$sql_kec = "select * from ref.mst_wilayah where kode_wilayah = '".$kecamatan."'";
				$stmt_kec = sqlsrv_query( $con, $sql_kec );
				$row_kec = sqlsrv_fetch_array( $stmt_kec, SQLSRV_FETCH_ASSOC);



				$templates .= "Hasil Pencarian: <hr>";
				$templates .= "Bentuk Pendidikan: <b>".$bp."</b><br>";
				$templates .= "Status Sekolah: <b>".$st."</b><br>";

				if($bentuk_pendidikan == 13){
					if(!empty($row_pk)){
						$templates .= 'Program Pengajaran: <b>'.$row_pk['nama_jurusan'].'</b><br>';	
					}else{
						$templates .= 'Program Pengajaran: <b>Semua Program Pengajaran</b><br>';
					}
					
				}else if($bentuk_pendidikan == 14){
					if(!empty($row_pk)){
						$templates .= 'Jenis Ketunaan: <b>'.$row_pk['nama_jurusan'].'</b><br>';
					}else{
						$templates .= 'Jenis Ketunaan: <b>Semua Jenis Ketunaan</b><br>';
					}
				}else if($bentuk_pendidikan == 15){
					if(!empty($row_pk)){
						$templates .= 'Paket Keahlian: <b>'.$row_pk['nama_jurusan'].'</b><br>';
					}else{
						$templates .= 'Paket Keahlian: <b>Semua Paket Keahlian</b>';
					}
				}

				if(!empty($row_prop)){
					$templates .= "Propinsi: <b>".$row_prop['nama']."</b></br>";
				}else{
					$templates .= "Propinsi: <b>Semua Propinsi</b><br>";
				}

				if(!empty($row_kab)){
					$templates .= "Kabupaten / Kota: <b>".$row_kab['nama']."</b></br>";
				}else{
					if(!empty($row_prop)){
						$templates .= "Kabupaten / Kota: <b>Semua Kabupaten / Kota di ".$row_prop['nama']."</b><br>";	
					}else{
						$templates .= "Kabupaten / Kota: <b>Semua Kabupaten / Kota</b><br>";
					}
				}

				if(!empty($row_kec)){
					$templates .= "Kecamatan: <b>".$row_kec['nama']."</b></br>";
				}else{
					if(!empty($row_kab)){
						$templates .= "Kecamatan: <b>Semua Kecamatan di ".$row_kab['nama']."</b><br>";	
					}else{
						$templates .= "Kecamatan: <b>Semua Kecamatan</b><br>";	
					}
					
				}

				if(!empty($keyword)){
						$templates .= "Kata Kunci: <b>". $keyword."</b><br>";
					}

				$templates .= "Jumlah Total: <b>". moneyConverter($i,0,'.','')."</b>";

				break;

			case 'tampilkanhasil':
				$offset = $request->get('offset');
				
				$bentuk_pendidikan = $request->get('bentuk_pendidikan');
				$status = $request->get('status');
				$paketkeahlian = $request->get('paket_keahlian');
				$propinsi = $request->get('propinsi');
				$kabkota = $request->get('kabkota');
				$kecamatan = $request->get('kecamatan');
				$keyword = $request->get('keyword_sekolah');

				if($paketkeahlian != '0'){

					$sql = "SELECT
						a.sekolah_id,
						a.nama as 'nama_sekolah',
						mst1.nama as 'kecamatan',
						mst2.nama as 'kabupaten',
						mst3.nama as 'propinsi',
						a.alamat_jalan,
						a.nomor_telepon,
						j.nama_jurusan,
						a.status_sekolah,
						a.npsn
					FROM
						jurusan_sp jsp
					JOIN sekolah a ON a.sekolah_id = jsp.sekolah_id
					JOIN ref.mst_wilayah mst1 ON a.kode_wilayah = mst1.kode_wilayah
					JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
					JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
					join ref.jurusan j on jsp.jurusan_id = j.jurusan_id
					where jsp.jurusan_id = '".$paketkeahlian."'";

					if($bentuk_pendidikan != 0){
						$sql .= " and a.bentuk_pendidikan_id = ".$bentuk_pendidikan;
					}

					if($status != 0){
						$sql .= " and a.status_sekolah = ".$status;
					}

					if($propinsi != 0){
						$sql .= " and mst3.kode_wilayah = ".$propinsi;
					}

					if($kabkota != 0){
						$sql .= " and mst2.kode_wilayah = ".$kabkota;
					}

					if($kecamatan != 0){
						$sql .= " and mst1.kode_wilayah = ".$kecamatan;
					}

					if(!empty($keyword)){
						$sql .= " and (a.nama like '%".$keyword."%' OR a.npsn like '%".$keyword."%')";
					}


					$sql .= " order by mst3.nama, mst2.nama, mst1.nama, a.nama asc OFFSET ".$offset." ROWS FETCH NEXT 50 ROWS ONLY";

					$stmt = sqlsrv_query( $con, $sql );

					$templates = '';
					$i = $offset;

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {						

						$i++;

						if($row['status_sekolah'] == 1){
							$status_sekolah = 'Negeri';
						}else{
							$status_sekolah = 'Swasta';
						}

						$enkrip = $row['sekolah_id'];

						$sql_enkrip = "SELECT
								CONVERT (
									VARCHAR (20),
									HashBytes ('MD5', CAST('".$enkrip."' AS VARCHAR(50))),
									2
								) AS enkripsi";
						$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
						$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);

						$templates .= '<a class="list-group-item " href="datasekolah/'.$row_enkrip['enkripsi'].'">
							<div style="float:left;width:20%">'.$row['npsn'].'.</div>
						    <div style="float:left;width:30%"><b>'.$row['nama_sekolah'].'</b></div>
						    <div style="float:left;width:20%">'.$status_sekolah.'</div>
						    <div style="float:left;width:30%;">'.$row['alamat_jalan'].'<br>'.$row['kecamatan'].'<br>'.$row['kabupaten'].'</div>
						   	<div class="clear"></div>
						</a>';

					}

					if($i > $offset){
						$templates .= '<a class="list-group-item load-list" style="text-align:center"><img src="../assets/img/loading8.gif" height="50px"/>&nbsp; Memuat Daftar...</a>';

					}else{
						$templates .= '<a class="list-group-item load-list" style="text-align:center"><b>Akhir dari Daftar</b></a>';

					}


				}else{

					$sql = "SELECT
						a.sekolah_id,
						mst1.nama AS kecamatan,
						mst2.nama AS kabupaten,
						mst3.nama AS propinsi,
						a.kode_wilayah AS kode_kecamatan,
						mst2.kode_wilayah AS kode_kab,
						mst3.kode_wilayah AS kode_prop,
						a.nama as nama_sekolah,
						*
					FROM
						sekolah a
					JOIN ref.mst_wilayah mst1 ON a.kode_wilayah = mst1.kode_wilayah
					JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
					JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
					WHERE
						a.Soft_delete = 0";

					if($bentuk_pendidikan != 0){
						$sql .= " and a.bentuk_pendidikan_id = ".$bentuk_pendidikan;
					}

					if($status != 0){
						$sql .= " and a.status_sekolah = ".$status;
					}

					if($propinsi != 0){
						$sql .= " and mst3.kode_wilayah = ".$propinsi;
					}

					if($kabkota != 0){
						$sql .= " and mst2.kode_wilayah = ".$kabkota;
					}

					if($kecamatan != 0){
						$sql .= " and mst1.kode_wilayah = ".$kecamatan;
					}

					if(!empty($keyword)){
						$sql .= " and (a.nama like '%".$keyword."%' OR a.npsn like '%".$keyword."%')";
					}


					$sql .= " order by mst3.nama, mst2.nama, mst1.nama, a.nama asc OFFSET ".$offset." ROWS FETCH NEXT 50 ROWS ONLY";
					// $templates = $sql;

					$stmt = sqlsrv_query( $con, $sql );

					$templates = '';
					$i = $offset;

					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {						

						$i++;

						if($row['status_sekolah'] == 1){
							$status_sekolah = 'Negeri';
						}else{
							$status_sekolah = 'Swasta';
						}

						$enkrip = $row['sekolah_id'];

						$sql_enkrip = "SELECT
								CONVERT (
									VARCHAR (20),
									HashBytes ('MD5', CAST('".$enkrip."' AS VARCHAR(50))),
									2
								) AS enkripsi";
						$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
						$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);

						$templates .= '<a class="list-group-item " href="datasekolah/'.$row_enkrip['enkripsi'].'">
							<div style="float:left;width:20%">'.$row['npsn'].'.</div>
						    <div style="float:left;width:30%"><b>'.$row['nama_sekolah'].'</b></div>
						    <div style="float:left;width:20%">'.$status_sekolah.'</div>
						    <div style="float:left;width:30%;">'.$row['alamat_jalan'].'<br>'.$row['kecamatan'].'<br>'.$row['kabupaten'].'</div>
						    <div class="clear"></div>
						</a>';

					}

					if($i > $offset){
						$templates .= '<a class="list-group-item load-list" style="text-align:center"><img src="../assets/img/loading8.gif" height="50px"/>&nbsp; Memuat Daftar...</a>';

					}else{
						$templates .= '<a class="list-group-item load-list" style="text-align:center"><b>Akhir dari Daftar</b></a>';

					}

				}


				break;

			default:
				# code...
				break;
		}

		return $templates;
	}

	function getDataSekolah(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');

		$con = $this->initdb();

		$sql = "select nama from dbo.sekolah where CONVERT (
									VARCHAR (20),
									HashBytes ('MD5', CAST(sekolah_id AS VARCHAR(50))),
									2
								) = '".$sekolah_id."'";

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
	}

	public function breadcrumbDataSekolah(Request $request, Application $app){
		$hari_ini = $this->getToday();
		$sekolah_id = $request->get('sekolah_id');

		$filename = 'cache/breadcrumbDataSekolah/breadcrumbDataSekolah_'.$sekolah_id.'_'.$hari_ini;

        if (!file_exists($filename)){

        	$con = $this->initdb();

			$sql = "SELECT
				s.nama,
				s.npsn,
				s.status_sekolah,
				s.alamat_jalan,
				s.rt,
				s.rw,
				s.nama_dusun,
				s.desa_kelurahan,
				mst1.nama as 'kecamatan',
				mst2.nama as 'kabupaten',
				mst3.nama as 'propinsi',
				bp.nama as 'bentuk_pendidikan_id_str',
				s.kode_pos,
				s.lintang,
				s.bujur,
				s.nomor_telepon,
				s.nomor_fax,
				s.website,
				s.email,
				s.sk_pendirian_sekolah,
				s.tanggal_sk_pendirian,
				sk.nama as 'status_kepemilikan_id_str',
				s.sk_izin_operasional,
				s.tanggal_sk_izin_operasional
			FROM
				dbo.sekolah s
			join ref.bentuk_pendidikan bp on s.bentuk_pendidikan_id = bp.bentuk_pendidikan_id
			JOIN ref.mst_wilayah mst1 ON s.kode_wilayah = mst1.kode_wilayah
			JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
			JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
			join ref.status_kepemilikan sk on sk.status_kepemilikan_id = s.status_kepemilikan_id
			WHERE
				CONVERT (
					VARCHAR (20),
					HashBytes (
						'MD5',
						CAST (s.sekolah_id AS VARCHAR(50))
					),
					2
				) = '".$sekolah_id."'";

			$stmt = sqlsrv_query( $con, $sql );

			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			if($row['status_sekolah'] == 1){
				$row['status'] = 'Negeri';
			}else{
				$row['status'] = 'Swasta';
			}

			// return json_encode($row);

			file_put_contents($filename, json_encode($row));
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;

	}

	public function breadcrumbDataSekolahJumlahPd(Request $request, Application $app){
		$hari_ini = $this->getToday();
		$sekolah_id = $request->get('sekolah_id');
		$semester_id = $request->get('semester_id');

		$con = $this->initdbSqlServerLokal();

		$unhash = $this->getUnHash($sekolah_id);

			/*
			CONVERT (
					VARCHAR (20),
					HashBytes (
						'MD5',
						CAST (rb.sekolah_id AS VARCHAR(50))
					),
					2
				)
			*/

		$sql = "select 
					pd as jumlah_pd, 
					(pd_kelas_10_laki + pd_kelas_11_laki + pd_kelas_12_laki + pd_kelas_13_laki) as jumlah_pd_lk,
					(pd_kelas_10_perempuan + pd_kelas_11_perempuan + pd_kelas_12_perempuan + pd_kelas_13_perempuan) as jumlah_pd_pr
				from rekap_sekolah
				where
					sekolah_id = '{$unhash}'
					and semester_id = {$semester_id}";

		// return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
			
	}

	public function breadcrumbDataSekolahJumlahPdTotal(Request $request, Application $app){
		$hari_ini = $this->getToday();
		$sekolah_id = $request->get('sekolah_id');

		$filename = 'cache/breadcrumbDataSekolahJumlahPdTotal/breadcrumbDataSekolahJumlahPdTotal_'.$sekolah_id.'_'.$hari_ini;

        if (!file_exists($filename)){
			$con = $this->initdb();

			$unhash = $this->getUnHash($sekolah_id);

			$sql = "SELECT
						COUNT (*) as 'jumlah_pd',
						SUM(CASE WHEN pd.jenis_kelamin='L' THEN 1 ELSE 0 END) as 'jumlah_pd_lk',
						SUM(CASE WHEN pd.jenis_kelamin='P' THEN 1 ELSE 0 END) as 'jumlah_pd_pr'
					FROM
						registrasi_peserta_didik rpd
					INNER JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
					JOIN sekolah sk ON sk.sekolah_id = rpd.sekolah_id
					WHERE
						sk.sekolah_id = '".$unhash."'
					AND rpd.Soft_delete = 0
					AND pd.Soft_delete = 0
					AND rpd.jenis_keluar_id IS NULL";

			$stmt = sqlsrv_query( $con, $sql );

			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			// return json_encode($row);
			file_put_contents($filename, json_encode($row));
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;
	}


	public function breadcrumbDataSekolahJumlahPtk(Request $request, Application $app){
		$hari_ini = $this->getToday();
		$sekolah_id = $request->get('sekolah_id');
		$semester_id = $request->get('semester_id');
		$tahun_ajaran = substr($semester_id, 0,4);

		// return 'tes';die;

		$unhash = $this->getUnHash($sekolah_id);

		// return $unhash;die;
		$con = $this->initdbSqlServerLokal();

		$sql = "select 
					(ptk + pegawai) as jumlah_ptk,
					ptk as jumlah_guru,
					pegawai as jumlah_administrasi
				from rekap_sekolah
					where semester_id = {$semester_id}
					and sekolah_id = '{$unhash}'";

					// return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
	}

	function getJumlahSiswaPerRombel(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');
		$semester_id = $request->get('semester_id');
		$con = $this->initdb();

		$unhash = $this->getUnHash($sekolah_id);

		$sql = "SELECT
					rb.tingkat_pendidikan_id,
					count(anggota_rombel_id) as 'jumlah'
				FROM
					rombongan_belajar rb
				JOIN anggota_rombel ar ON ar.rombongan_belajar_id = rb.rombongan_belajar_id
				WHERE
					rb.sekolah_id = '".$unhash."'
				and semester_id = '".$semester_id."'
				GROUP BY
					rb.tingkat_pendidikan_id";

		$stmt = sqlsrv_query( $con, $sql );

		$returnArr = array();

		while ($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {
			
			array_push($returnArr, $row);

		}

		return json_encode($returnArr);
	}

	function getSemesterTerakhir(Request $request, Application $app){
		$hari_ini = $this->getToday();

		$filename = 'cache/getSemesterTerakhir/getSemesterTerakhir_'.$hari_ini;

        if (!file_exists($filename)){
			$con = $this->initdb();

			$sql = "SELECT
						TOP 1 ta.nama AS 'tahun_ajaran',
						*
					FROM
						ref.semester s
					JOIN ref.tahun_ajaran ta ON s.tahun_ajaran_id = ta.tahun_ajaran_id
					WHERE
						s.periode_aktif = 1
						and s.expired_date is null
					ORDER BY
						s.semester_id DESC";

			$stmt = sqlsrv_query( $con, $sql );
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			// return json_encode($row);	
			// $templates .= "<div style=font-size:10px>cache per tanggal ".date('Y-m-d H:i:s')."</div><br>";

			file_put_contents($filename, json_encode($row));
        }

        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

        return $return;

	}

	function getSemester(Request $request, Application $app){
		$con = $this->initdb();

		$sql = "SELECT
					*
				FROM
					ref.semester
				WHERE
					periode_aktif = 1
				and tahun_ajaran_id = 2014
				AND (
					expired_date IS NULL
					OR expired_date < CAST (GETDATE() AS DATE))";

		$stmt = sqlsrv_query( $con, $sql );

		$array = array();

		while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){

			array_push($array, $row);

		}

		return json_encode($array);
	}

	function getTahunAjaran(Request $request, Application $app){
		$con = $this->initdb();

		$sql = "SELECT
					*
				FROM
					ref.tahun_ajaran
				WHERE
					periode_aktif = 1
				AND (
					expired_date IS NULL
					OR expired_date > CAST (GETDATE() AS DATE)
				)";

		$stmt = sqlsrv_query( $con, $sql );

		$array = array();

		while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){

			array_push($array, $row);

		}

		return json_encode($array);
	}

	public function breadcrumbSekolahAsal(Request $request, Application $app){
		$ptk_id = $request->get('ptk_id');
		$con = $this->initdb();

		$sql = "select entry_sekolah_id from dbo.ptk where '".$ptk_id."' = (SELECT
				CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST(ptk_id AS VARCHAR(50))),
					2
				))";

		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		$sql_sekolah = "select nama, CONVERT (
						VARCHAR (20),
						HashBytes ('MD5', CAST(sekolah_id AS VARCHAR(50))),
						2
					) AS enkripsi from dbo.sekolah where sekolah_id = '".$row['entry_sekolah_id']."'";
		$stmt_sekolah = sqlsrv_query( $con, $sql_sekolah );
		$row_sekolah = sqlsrv_fetch_array( $stmt_sekolah, SQLSRV_FETCH_ASSOC);

		if ($app['session']->get('user')) {
			return '<a href="../detailSekolah/'.$row_sekolah['enkripsi'].'">'.$row_sekolah['nama']."</a>";
		}else{
			return '<a href="../datasekolah/'.$row_sekolah['enkripsi'].'">'.$row_sekolah['nama']."</a>";
		}
	}

	public function breadcrumbGetDetailPtk(Request $request, Application $app){

		$ptk_id = $request->get('ptk_id');
		$con = $this->initdb();

		$sql = "select *,
				ag.nama as 'agama',
				ptk.nama as 'nama_ptk',
				mst1.nama as 'kecamatan',
				mst2.nama as 'kabkota',
				skp.nama as 'status_kepegawaian',
				jptk.jenis_ptk as 'jenis_ptk',
				lp.nama as 'lembaga_pengangkat',
				sg.nama as 'sumber_gaji' ,
				mst3.nama as 'propinsi' 
				from dbo.ptk ptk
				JOIN ref.mst_wilayah mst1 ON ptk.kode_wilayah = mst1.kode_wilayah
				JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
				JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
				join ref.agama ag on ptk.agama_id = ag.agama_id
				join ref.status_kepegawaian skp on skp.status_kepegawaian_id = ptk.status_kepegawaian_id
				join ref.jenis_ptk jptk on jptk.jenis_ptk_id = ptk.jenis_ptk_id
				join ref.lembaga_pengangkat lp on lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
				join ref.sumber_gaji sg on sg.sumber_gaji_id = ptk.sumber_gaji_id
				where '".$ptk_id."' = (SELECT
				CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST(ptk_id AS VARCHAR(50))),
					2
				))";
		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		// $row['tanggal_lahir'] = $row['tanggal_lahir']['date'];

		$sql_pg = "select nama from ref.pangkat_golongan where pangkat_golongan_id = '".$row['pangkat_golongan_id']."'";
		$stmt_pg = sqlsrv_query( $con, $sql_pg );
		$row_pg = sqlsrv_fetch_array( $stmt_pg, SQLSRV_FETCH_ASSOC);

		$row['pangkat_golongan'] = $row_pg['nama'];

		$sql_kl = "select nama from ref.keahlian_laboratorium where keahlian_laboratorium_id = '".$row['keahlian_laboratorium_id']."'";
		$stmt_kl = sqlsrv_query( $con, $sql_kl );
		$row_kl = sqlsrv_fetch_array( $stmt_kl, SQLSRV_FETCH_ASSOC);

		$row['pangkat_golongan'] = $row_pg['nama'];
		$row['keahlian_laboratorium'] = $row_kl['nama'];

		return json_encode($row);
	}

	public function sekolahIdPtk($ptk_id,$con){

		$sql = "select CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST(entry_sekolah_id AS VARCHAR(50))),
					2
				) as sekolah_id from dbo.ptk where '".$ptk_id."' = (SELECT
				CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST(ptk_id AS VARCHAR(50))),
					2
				))";
		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return $row['sekolah_id'];

	}

	public function getDetailSekolahAsalPtk(Request $request, Application $app){
		$ptk_id = $request->get('ptk_id');
		$con = $this->initdb();

		$sql = "select entry_sekolah_id
				from dbo.ptk where '".$ptk_id."' = (SELECT
				CONVERT (
					VARCHAR (20),
					HashBytes ('MD5', CAST(ptk_id AS VARCHAR(50))),
					2
				))";
		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		$sekolah_id = $row['entry_sekolah_id'];

		$sql_sekolah = "SELECT
			s.nama,
			s.npsn,
			s.status_sekolah,
			s.alamat_jalan,
			s.rt,
			s.rw,
			s.nama_dusun,
			s.desa_kelurahan,
			mst1.nama as 'kecamatan',
			mst2.nama as 'kabupaten',
			mst3.nama as 'propinsi',
			bp.nama as 'bentuk_pendidikan_id_str',
			s.kode_pos,
			s.lintang,
			s.bujur,
			s.nomor_telepon,
			s.nomor_fax,
			s.website,
			s.email,
			s.sk_pendirian_sekolah,
			s.tanggal_sk_pendirian,
			sk.nama as 'status_kepemilikan_id_str',
			s.sk_izin_operasional,
			s.tanggal_sk_izin_operasional
		FROM
			dbo.sekolah s
		join ref.bentuk_pendidikan bp on s.bentuk_pendidikan_id = bp.bentuk_pendidikan_id
		JOIN ref.mst_wilayah mst1 ON s.kode_wilayah = mst1.kode_wilayah
		JOIN ref.mst_wilayah mst2 ON mst1.mst_kode_wilayah = mst2.kode_wilayah
		JOIN ref.mst_wilayah mst3 ON mst2.mst_kode_wilayah = mst3.kode_wilayah
		join ref.status_kepemilikan sk on sk.status_kepemilikan_id = s.status_kepemilikan_id
		WHERE
			s.sekolah_id = '".$sekolah_id."'";
		$stmt_sekolah = sqlsrv_query( $con, $sql_sekolah );
		$row_sekolah = sqlsrv_fetch_array( $stmt_sekolah, SQLSRV_FETCH_ASSOC);

		if($row_sekolah['status_sekolah'] == 1){
			$row_sekolah['status'] = 'Negeri';
		}else if($row_sekolah['status_sekolah'] == 2){
			$row_sekolah['status'] = 'Swasta';
		}

		return json_encode($row_sekolah);
	}

	public function getPenugasanPtk(Request $request, Application $app){
		$ptk_id = $request->get('ptk_id');
		$ta = $request->get('ta');
		$con = $this->initdb();

		$sql = "SELECT
					s.nama AS 'nama_sekolah',
					*
				FROM
					ptk_terdaftar ptkd
				JOIN sekolah s ON s.sekolah_id = ptkd.sekolah_id
				WHERE
					ptkd.tahun_ajaran_id = '".$ta."'
				AND '".$ptk_id."' = (
					SELECT
						CONVERT (
							VARCHAR (20),
							HashBytes (
								'MD5',
								CAST (ptk_id AS VARCHAR(50))
							),
							2
						)
				)
				AND ptkd.Soft_delete = 0";
		$stmt = sqlsrv_query( $con, $sql);
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);

	}

	function rekapPdNasional(Request $request, Application $app){
		$semester_id = $request->get('semester_id');
		$con = $this->initdb();

		// return $semester_id;

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$hari_ini .= " 00:00:00";

		$c->addAnd(JumlahPdPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdPeer::TAHUN_AJARAN_ID, $semester_id);
		$c->addAnd(JumlahPdPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdPeer::MST_KODE_WILAYAH, '000000 ');

		$rekap = JumlahPdPeer::doSelect($c);

		$count = JumlahPdPeer::doCount($c);

		// return $count;

		$templates = '<table class="table table-striped table-hover " style="font-size:14px">
		                <thead>
		                  <tr>
		                    <th>#</th>
		                    <th style="text-align:center">Propinsi</th>
		                    <th style="text-align:center">Telah Masuk Rombel</th>
		                    <th style="text-align:center">Belum Masuk Rombel</th>
		                    <th style="text-align:center">Total</th>
		                    <th style="text-align:center">Progress</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';
		$i = 0;

		foreach ($rekap as $r) {

			$i++;

			$sql = "select nama from ref.mst_wilayah where kode_wilayah = ".$r->getKodeWilayah();
			$stmt = sqlsrv_query( $con, $sql);
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			if($r->getPdBelumDiRombel() < 0){
				$belumdiRombel = 0;
				$sudahdirombel =$r->getPdDiRombel();
				$pdtotal = $r->getPdDiRombel();
			}else{
				$belumdiRombel = $r->getPdBelumDiRombel();
				$sudahdirombel = $r->getPdDiRombel();
				$pdtotal = $r->getPdTotal();
			}

			$progress = ( $sudahdirombel / $pdtotal ) * 100;

			
			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><a class="link_propinsi" href="rekappdpropinsi/'.$r->getKodeWilayah().'"><b>'.$row['nama'].'</b></a></td>
		                    <td style="text-align:right">'.moneyConverter($sudahdirombel,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($belumdiRombel,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($pdtotal,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.round($progress,2).' %</td>
		                  </tr>';
			
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;


	}

	function rekapPdPropinsi(Request $request, Application $app){
		$semester_id = $request->get('semester_id');
		$kode_wilayah = $request->get('kode_wilayah');
		$con = $this->initdb();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$hari_ini .= " 00:00:00";

		$c->addAnd(JumlahPdPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdPeer::TAHUN_AJARAN_ID, $semester_id);
		$c->addAnd(JumlahPdPeer::ID_LEVEL_WILAYAH, 2);
		$c->addAnd(JumlahPdPeer::MST_KODE_WILAYAH, $kode_wilayah);

		$rekap = JumlahPdPeer::doSelect($c);

		$count = JumlahPdPeer::doCount($c);

		$templates = '<table class="table table-striped table-hover " style="font-size:14px">
		                <thead>
		                  <tr>
		                    <th>#</th>
		                    <th style="text-align:center">Kabupaten / Kota</th>
		                    <th style="text-align:center">Telah Masuk Rombel</th>
		                    <th style="text-align:center">Belum Masuk Rombel</th>
		                    <th style="text-align:center">Total</th>
		                    <th style="text-align:center">Progress</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';
		$i = 0;

		foreach ($rekap as $r) {

			$i++;

			$sql = "select nama from ref.mst_wilayah where kode_wilayah = ".$r->getKodeWilayah();
			$stmt = sqlsrv_query( $con, $sql);
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			if($r->getPdBelumDiRombel() < 0){
				$belumdiRombel = 0;
				$sudahdirombel =$r->getPdDiRombel();
				$pdtotal = $r->getPdDiRombel();
			}else{
				$belumdiRombel = $r->getPdBelumDiRombel();
				$sudahdirombel = $r->getPdDiRombel();
				$pdtotal = $r->getPdTotal();
			}

			$progress = ( $sudahdirombel / $pdtotal ) * 100;

			
			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><b>'.$row['nama'].'</b></td>
		                    <td style="text-align:right">'.moneyConverter($sudahdirombel,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($belumdiRombel,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($pdtotal,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.round($progress,2).' %</td>
		                  </tr>';
			
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;


		// return $count;
	}

	function rekapPdKabkota(Request $request, Application $app){
		$semester_id = $request->get('semester_id');
		$kode_wilayah = $request->get('kode_wilayah');
		$con = $this->initdb();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$hari_ini .= " 00:00:00";

		$c->addAnd(JumlahPdPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdPeer::TAHUN_AJARAN_ID, $semester_id);
		$c->addAnd(JumlahPdPeer::ID_LEVEL_WILAYAH, 3);
		$c->addAnd(JumlahPdPeer::MST_KODE_WILAYAH, $kode_wilayah);

		$rekap = JumlahPdPeer::doSelect($c);

		$count = JumlahPdPeer::doCount($c);

		$templates = '<table class="table table-striped table-hover " style="font-size:14px">
		                <thead>
		                  <tr>
		                    <th>#</th>
		                    <th style="text-align:center">Kecamatan</th>
		                    <th style="text-align:center">Telah Masuk Rombel</th>
		                    <th style="text-align:center">Belum Masuk Rombel</th>
		                    <th style="text-align:center">Total</th>
		                    <th style="text-align:center">Progress</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';
		$i = 0;

		foreach ($rekap as $r) {

			$i++;

			$sql = "select nama from ref.mst_wilayah where kode_wilayah = ".$r->getKodeWilayah();
			$stmt = sqlsrv_query( $con, $sql);
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			if($r->getPdBelumDiRombel() < 0){
				$belumdiRombel = 0;
				$sudahdirombel =$r->getPdDiRombel();
				$pdtotal = $r->getPdDiRombel();
			}else{
				$belumdiRombel = $r->getPdBelumDiRombel();
				$sudahdirombel = $r->getPdDiRombel();
				$pdtotal = $r->getPdTotal();
			}

			$progress = ( $sudahdirombel / $pdtotal ) * 100;

			
			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><b>'.$row['nama'].'</b></td>
		                    <td style="text-align:right">'.moneyConverter($sudahdirombel, $jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($belumdiRombel, $jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($pdtotal, $jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.round($progress,2).' %</td>
		                  </tr>';
			
		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;


		// return $count;
	}

	function breadcrumbrekappdpropinsi(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah');
		$con = $this->initdb();
		
		$sql = "select nama from ref.mst_wilayah where kode_wilayah = ".$kode_wilayah;
		$stmt = sqlsrv_query( $con, $sql);
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
		sqlsrv_free_stmt( $stmt);

		return $row['nama'];
	}

	function breadcrumbrekappdkabkota(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah');
		$con = $this->initdb();
		
		$sql = "SELECT
					w1.nama as 'kabkota',
				w2.nama as 'propinsi',
				w2.kode_wilayah as 'kode_propinsi'
				FROM
					ref.mst_wilayah w1
				join ref.mst_wilayah w2 on w1.mst_kode_wilayah = w2.kode_wilayah
				WHERE
				w1.kode_wilayah = ".$kode_wilayah;
		$stmt = sqlsrv_query( $con, $sql);
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
		sqlsrv_free_stmt( $stmt);

		return "<a href='../rekappdpropinsi/".$row['kode_propinsi']."'>".$row['propinsi']."</a>  /  ".$row['kabkota'];
	}

	function breadcrumbpdperrombel(Request $request, Application $app){

		$hari_ini = $this->getToday();
		$sekolah_id = $request->get('sekolah_id');
		$semester_id = $request->get('semester_id');

		$con = $this->initdbSqlServerLokal();
		
		$unhash = $this->getUnHash($sekolah_id);

		$sql = "select 
				pd_kelas_10 as pd_kelas_x,
				pd_kelas_11 as pd_kelas_xi,
				pd_kelas_12 as pd_kelas_xii,
				pd_kelas_13 as pd_kelas_xiii
			from rekap_sekolah
			where
				sekolah_id = '{$unhash}'
				and semester_id = {$semester_id}";
	
		$stmt = sqlsrv_query( $con, $sql);
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
		sqlsrv_free_stmt( $stmt);

        return json_encode($row);		
	}

	function getRekapDapodikCore2(){

		$hari_ini_batas = date('Y-m-d 05:00:00');
		$tgl = date('Y-m-d');
		$hari_ini_banget = date('Y-m-d H:i:s');

		$filename = 'cache/getRekapDapodikCore2/getRekapDapodikCore2_'.$tgl;

		if($hari_ini_banget > $hari_ini_batas){

			if (!file_exists($filename)){
				// return "code 084";die;
        		$c = new \Criteria();

				$hari_ini = date('Y-m-d');

				$c->add(RekapSekolahPeer::TANGGAL, $hari_ini);

				$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, array(13,14,15,29), \Criteria::IN);

				$sekolahCount = RekapSekolahPeer::doCount($c);
				$sekolah = RekapSekolahPeer::doSelect($c);

				// $ptk = 0;
				foreach ($sekolah as $s) {
					$ptk = $ptk + $s->getPtk();
					$pd = $pd + $s->getPd();
				}

				$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 13);

				$sekolahSmaCount = RekapSekolahPeer::doCount($c);
				$sekolahSma = RekapSekolahPeer::doSelect($c);

				foreach ($sekolahSma as $s) {
					$ptkSma = $ptkSma + $s->getPtk();
					$pdSma = $pdSma + $s->getPd();
				}
				
				$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, array(14,29), \Criteria::IN);

				$sekolahSmlbCount = RekapSekolahPeer::doCount($c);
				$sekolahSmlb = RekapSekolahPeer::doSelect($c);

				foreach ($sekolahSmlb as $s) {
					$ptkSmlb = $ptkSmlb + $s->getPtk();
					$pdSmlb = $pdSmlb + $s->getPd();
				}

				$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 15);

				$sekolahSmkCount = RekapSekolahPeer::doCount($c);
				$sekolahSmk = RekapSekolahPeer::doSelect($c);

				foreach ($sekolahSmk as $s) {
					$ptkSmk = $ptkSmk + $s->getPtk();
					$pdSmk = $pdSmk + $s->getPd();
				}

				// return moneyConverter($sekolahSmaCount,$jumdes = 0,$pemisah_ribuan = ".","");die;

				// return $sekolahCount;
				$array['ptk_nasional'] = moneyConverter($ptk, $jumdes = 0,$pemisah_ribuan = ".","");
				$array['ptk_sma_nasional'] = moneyConverter($ptkSma, $jumdes = 0,$pemisah_ribuan = ".","");
				$array['ptk_smk_nasional'] = moneyConverter($ptkSmk, $jumdes = 0,$pemisah_ribuan = ".","");
				$array['ptk_smlb_nasional'] = moneyConverter($ptkSmlb, $jumdes = 0,$pemisah_ribuan = ".","");

				$array['sekolah_nasional'] = moneyConverter($sekolahCount,$jumdes = 0,$pemisah_ribuan = ".","");
				$array['sma_nasional'] = moneyConverter($sekolahSmaCount,$jumdes = 0,$pemisah_ribuan = ".","");
				$array['smk_nasional'] = moneyConverter($sekolahSmkCount,$jumdes = 0,$pemisah_ribuan = ".","");
				$array['smlb_nasional'] = moneyConverter($sekolahSmlbCount,$jumdes = 0,$pemisah_ribuan = ".","");
				
				$array['pd_sma_nasional'] = moneyConverter($pdSma, $jumdes = 0,$pemisah_ribuan = ".","");
				$array['pd_smk_nasional'] = moneyConverter($pdSmk, $jumdes = 0,$pemisah_ribuan = ".","");
				$array['pd_smlb_nasional'] = moneyConverter($pdSmlb, $jumdes = 0,$pemisah_ribuan = ".","");
				$array['pd_nasional'] = moneyConverter($pd, $jumdes = 0,$pemisah_ribuan = ".","");

				
				// return $templates;
				file_put_contents($filename, json_encode($array));
	        }

	        $return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

	        return $return;

		} else {
			

    		$c = new \Criteria();

			$hari_ini = date('Y-m-d');

			$c->add(RekapSekolahPeer::TANGGAL, $hari_ini);

			$sekolahCount = RekapSekolahPeer::doCount($c);
			$sekolah = RekapSekolahPeer::doSelect($c);

			// $ptk = 0;
			foreach ($sekolah as $s) {
				$ptk = $ptk + $s->getPtk();
				$pd = $pd + $s->getPd();
			}

			$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 13);

			$sekolahSmaCount = RekapSekolahPeer::doCount($c);
			$sekolahSma = RekapSekolahPeer::doSelect($c);

			foreach ($sekolahSma as $s) {
				$ptkSma = $ptkSma + $s->getPtk();
				$pdSma = $pdSma + $s->getPd();
			}
			
			$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 14);

			$sekolahSmlbCount = RekapSekolahPeer::doCount($c);
			$sekolahSmlb = RekapSekolahPeer::doSelect($c);

			foreach ($sekolahSmlb as $s) {
				$ptkSmlb = $ptkSmlb + $s->getPtk();
				$pdSmlb = $pdSmlb + $s->getPd();
			}

			$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 15);

			$sekolahSmkCount = RekapSekolahPeer::doCount($c);
			$sekolahSmk = RekapSekolahPeer::doSelect($c);

			foreach ($sekolahSmk as $s) {
				$ptkSmk = $ptkSmk + $s->getPtk();
				$pdSmk = $pdSmk + $s->getPd();
			}


			$array['ptk_nasional'] = moneyConverter($ptk, $jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_sma_nasional'] = moneyConverter($ptkSma, $jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_smk_nasional'] = moneyConverter($ptkSmk, $jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_smlb_nasional'] = moneyConverter($ptkSmlb, $jumdes = 0,$pemisah_ribuan = ".","");

			$array['sekolah_nasional'] = moneyConverter($sekolahCount,$jumdes = 0,$pemisah_ribuan = ".","");
			$array['sma_nasional'] = moneyConverter($sekolahSmaCount,$jumdes = 0,$pemisah_ribuan = ".","");
			$array['smk_nasional'] = moneyConverter($sekolahSmkCount,$jumdes = 0,$pemisah_ribuan = ".","");
			$array['smlb_nasional'] = moneyConverter($sekolahSmlbCount,$jumdes = 0,$pemisah_ribuan = ".","");
			
			$array['pd_sma_nasional'] = moneyConverter($pdSma, $jumdes = 0,$pemisah_ribuan = ".","");
			$array['pd_smk_nasional'] = moneyConverter($pdSmk, $jumdes = 0,$pemisah_ribuan = ".","");
			$array['pd_smlb_nasional'] = moneyConverter($pdSmlb, $jumdes = 0,$pemisah_ribuan = ".","");
			$array['pd_nasional'] = moneyConverter($pd, $jumdes = 0,$pemisah_ribuan = ".","");
			
			return json_encode($array);				
		}

		// $c = new \Criteria();

		// $hari_ini = date('Y-m-d');

		// $c->add(RekapSekolahPeer::TANGGAL, $hari_ini);

		// $sekolahCount = RekapSekolahPeer::doCount($c);
		// $sekolah = RekapSekolahPeer::doSelect($c);

		// // $ptk = 0;
		// foreach ($sekolah as $s) {
		// 	$ptk = $ptk + $s->getPtk();
		// 	$pd = $pd + $s->getPd();
		// }

		// $c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 13);

		// $sekolahSmaCount = RekapSekolahPeer::doCount($c);
		// $sekolahSma = RekapSekolahPeer::doSelect($c);

		// foreach ($sekolahSma as $s) {
		// 	$ptkSma = $ptkSma + $s->getPtk();
		// 	$pdSma = $pdSma + $s->getPd();
		// }
		
		// $c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 14);

		// $sekolahSmlbCount = RekapSekolahPeer::doCount($c);
		// $sekolahSmlb = RekapSekolahPeer::doSelect($c);

		// foreach ($sekolahSmlb as $s) {
		// 	$ptkSmlb = $ptkSmlb + $s->getPtk();
		// 	$pdSmlb = $pdSmlb + $s->getPd();
		// }

		// $c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, 15);

		// $sekolahSmkCount = RekapSekolahPeer::doCount($c);
		// $sekolahSmk = RekapSekolahPeer::doSelect($c);

		// foreach ($sekolahSmk as $s) {
		// 	$ptkSmk = $ptkSmk + $s->getPtk();
		// 	$pdSmk = $pdSmk + $s->getPd();
		// }

		// $array['ptk_nasional'] = moneyConverter($ptk, $jumdes = 0,$pemisah_ribuan = ".","");
		// $array['ptk_sma_nasional'] = moneyConverter($ptkSma, $jumdes = 0,$pemisah_ribuan = ".","");
		// $array['ptk_smk_nasional'] = moneyConverter($ptkSmk, $jumdes = 0,$pemisah_ribuan = ".","");
		// $array['ptk_smlb_nasional'] = moneyConverter($ptkSmlb, $jumdes = 0,$pemisah_ribuan = ".","");

		// $array['sekolah_nasional'] = moneyConverter($sekolahCount,$jumdes = 0,$pemisah_ribuan = ".","");
		// $array['sma_nasional'] = moneyConverter($sekolahSmaCount,$jumdes = 0,$pemisah_ribuan = ".","");
		// $array['smk_nasional'] = moneyConverter($sekolahSmkCount,$jumdes = 0,$pemisah_ribuan = ".","");
		// $array['smlb_nasional'] = moneyConverter($sekolahSmlbCount,$jumdes = 0,$pemisah_ribuan = ".","");
		
		// $array['pd_sma_nasional'] = moneyConverter($pdSma, $jumdes = 0,$pemisah_ribuan = ".","");
		// $array['pd_smk_nasional'] = moneyConverter($pdSmk, $jumdes = 0,$pemisah_ribuan = ".","");
		// $array['pd_smlb_nasional'] = moneyConverter($pdSmlb, $jumdes = 0,$pemisah_ribuan = ".","");
		// $array['pd_nasional'] = moneyConverter($pd, $jumdes = 0,$pemisah_ribuan = ".","");

		// return json_encode($array);

	}

	function getRekapDapodikCore(){

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->add(RekapJumlahTotalPeer::TANGGAL, $hari_ini);

		$rekap = RekapJumlahTotalPeer::doSelect($c);

		$count = RekapJumlahTotalPeer::doCount($c);

		$array = array();

		foreach ($rekap as $r) {

			// $array['sekolah_nasional'] = moneyConverter($r->getSekolahNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['sma_nasional'] = moneyConverter($r->getSmaNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['smk_nasional'] = moneyConverter($r->getSmkNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['smlb_nasional'] = moneyConverter($r->getSmlbNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['pd_nasional'] = moneyConverter($r->getPdNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['pd_sma_nasional'] = moneyConverter($r->getPdSmaNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['pd_smk_nasional'] = moneyConverter($r->getPdSmkNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			// $array['pd_smlb_nasional'] = moneyConverter($r->getPdSmlbNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_nasional'] = moneyConverter($r->getPtkNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_sma_nasional'] = moneyConverter($r->getPtkSmaNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_smk_nasional'] = moneyConverter($r->getPtkSmkNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			$array['ptk_smlb_nasional'] = moneyConverter($r->getPtkSmlbNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
		}

		// jumlah sekolah
		$skh = new \Criteria();
		$skh->add(JumlahSekolahPeer::TANGGAL, $hari_ini);
		$skh->add(JumlahSekolahPeer::SEMESTER_ID, '20141');

		$rekapSekolah = JumlahSekolahPeer::doSelect($skh);

		foreach ($rekapSekolah as $r) {
			$jumSmaNegeri = $jumSmaNegeri + $r->getStatusSmaNegeri();
			$jumSmaSwasta = $jumSmaSwasta + $r->getStatusSmaSwasta();

			$jumSmkNegeri = $jumSmkNegeri + $r->getStatusSmkNegeri();
			$jumSmkSwasta = $jumSmkSwasta + $r->getStatusSmkSwasta();

			$jumSmlbNegeri = $jumSmlbNegeri + $r->getStatusSmlbNegeri();
			$jumSmlbSwasta = $jumSmlbSwasta + $r->getStatusSmlbSwasta();
		}

		$jumSma = $jumSmaNegeri + $jumSmaSwasta;
		$jumSmk = $jumSmkNegeri + $jumSmkSwasta;
		$jumSmlb = $jumSmlbNegeri + $jumSmlbSwasta;

		$array['sekolah_nasional'] = moneyConverter(($jumSma+$jumSmk+$jumSmlb),$jumdes = 0,$pemisah_ribuan = ".","");
		$array['sma_nasional'] = moneyConverter($jumSma,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['smk_nasional'] = moneyConverter($jumSmk,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['smlb_nasional'] = moneyConverter($jumSmlb,$jumdes = 0,$pemisah_ribuan = ".","");

		// end of jumlah sekolah

		// jumlah peserta didik
		$d = new \Criteria();
		$d->add(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$d->add(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekapPd = JumlahPdStatusPeer::doSelect($d);

		$i = 0;
		$j = 0;
		$k = 0;

		foreach ($rekapPd as $rpd) {
			
			$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
			$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
			$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());

		}

		$array['pd_sma_nasional'] = moneyConverter($i, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_smk_nasional'] = moneyConverter($j, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_smlb_nasional'] = moneyConverter($k, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_nasional'] = moneyConverter(($k + $j + $i), $jumdes = 0,$pemisah_ribuan = ".","");

		return json_encode($array);

	}

	function getRekapDapodikPdRombel(){
		$c = new \Criteria();
		$i = 0;
		$j = 0;
		$k = 0;
		$array = array();

		$hari_ini = date('Y-m-d');

		// $hari_ini .= " 00:00:00";

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $rpd) {
			$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
			$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
			$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());
		}

		$array['pd_rombel'] = moneyConverter(($i+$j+$k),$jumdes = 0,$pemisah_ribuan = ".","");
		// $array['pd_belum_rombel'] = moneyConverter($j,$jumdes = 0,$pemisah_ribuan = ".","");
		// $array['pd_terdaftar'] = moneyConverter($j+$i,$jumdes = 0,$pemisah_ribuan = ".","");

		$d = new \Criteria();

		$sekarang = date('Y-m-d');

		$d->add(RekapJumlahTotalPeer::TANGGAL, $sekarang);

		$rekapTotal = RekapJumlahTotalPeer::doSelect($d);

		foreach ($rekapTotal as $r) {

			$array['pd_terdaftar'] = moneyConverter($r->getPdNasional(),$jumdes = 0,$pemisah_ribuan = ".","");
			$array['pd_belum_rombel'] = moneyConverter($r->getPdNasional() - ($i+$j+$k), $jumdes = 0,$pemisah_ribuan = ".","");

			$array['pd_terdaftar_persen'] = 100;
			$array['pd_rombel_persen'] = round(((($i+$j+$k)/$r->getPdNasional())*100),2 );
			$array['pd_belum_rombel_persen'] = 100 - $array['pd_rombel_persen'];
		}

		return json_encode($array);

	}

	function getRekapDapodikPdNegeriSwasta(){
		$i = 0;
		$j = 0;
		$k = 0;
		$m = 0;
		$n = 0;
		$o = 0;
		$p = 0;
		$q = 0;
		$array = array();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getPdRombelNegeri();
			$j = $j + $r->getPdRombelSwasta();
			$k = $k + $r->getPdRombelSmaNegeri();
			$m = $m + $r->getPdRombelSmaSwasta();
			$n = $n + $r->getPdRombelSmkNegeri();
			$o = $o + $r->getPdRombelSmkSwasta();
			$p = $p + $r->getPdRombelSmlbNegeri();
			$q = $q + $r->getPdRombelSmlbSwasta();
		}
		

		$array['pd_rombel_negeri'] = moneyConverter($i, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_swasta'] = moneyConverter($j, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_sma_negeri'] = moneyConverter($k, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_sma_swasta'] = moneyConverter($m, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_smk_negeri'] = moneyConverter($n, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_smk_swasta'] = moneyConverter($o, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_smlb_negeri'] = moneyConverter($p, $jumdes = 0,$pemisah_ribuan = ".","");
		$array['pd_rombel_smlb_swasta'] = moneyConverter($q, $jumdes = 0,$pemisah_ribuan = ".","");

		$array['pd_rombel_negeri_persen'] = round(((($i/($i+$j))*100)),2);
		$array['pd_rombel_swasta_persen'] = round(((($j/($i+$j))*100)),2);
		$array['pd_rombel_sma_negeri_persen'] = round(((($k/($k+$m))*100)),2);
		$array['pd_rombel_sma_swasta_persen'] = round(((($m/($k+$m))*100)),2);
		$array['pd_rombel_smk_negeri_persen'] = round(((($n/($n+$o))*100)),2);
		$array['pd_rombel_smk_swasta_persen'] = round(((($o/($n+$o))*100)),2);
		$array['pd_rombel_smlb_negeri_persen'] = round(((($p/($p+$q))*100)),2);
		$array['pd_rombel_smlb_swasta_persen'] = round(((($q/($p+$q))*100)),2);

		return json_encode($array);

		
	}

	function getRekapPesertaDidikStatus(Request $request, Application $app){
		$semester_id = $request->get('semester_id');
		$con = $this->initdb();

		// return $semester_id;

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, $semester_id);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::MST_KODE_WILAYAH, '000000 ');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		$count = JumlahPdStatusPeer::doCount($c);

		$templates = '<table class="table table-striped table-hover " style="font-size:14px">
		                <thead>
		                  <tr>
		                    <th rowspan="2">#</th>
		                    <th rowspan="2" style="width:30%;text-align:center">Propinsi</th>
		                    <th colspan="2" style="text-align:center">SMA</th>
		                    <th colspan="2" style="text-align:center">SMK</th>
		                    <th colspan="2" style="text-align:center">SMLB</th>
		                    <th colspan="2" style="text-align:center">Total</th>
		                  </tr>
		                  <tr>
		                    <th style="text-align:center">Negeri</th>
		                    <th style="text-align:center">Swasta</th>
		                    <th style="text-align:center">Negeri</th>
		                    <th style="text-align:center">Swasta</th>
		                    <th style="text-align:center">Negeri</th>
		                    <th style="text-align:center">Swasta</th>
		                    <th style="text-align:center">Negeri</th>
		                    <th style="text-align:center">Swasta</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';
		$i = 0;

		foreach ($rekap as $r) {

			$i++;

			$sql = "select nama from ref.mst_wilayah where kode_wilayah = ".$r->getKodeWilayah();
			$stmt = sqlsrv_query( $con, $sql);
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><b>'.$row['nama'].'</b></td>
		                    <td style="text-align:right">'.moneyConverter($r->getPdRombelSmaNegeri(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($r->getPdRombelSmaSwasta(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($r->getPdRombelSmkNegeri(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($r->getPdRombelSmkSwasta(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($r->getPdRombelSmlbNegeri(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right">'.moneyConverter($r->getPdRombelSmlbSwasta(),$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td style="text-align:right"><b>'.moneyConverter($r->getPdRombelNegeri(),$jumdes = 0,$pemisah_ribuan = ".","").'</b></td>
		                    <td style="text-align:right"><b>'.moneyConverter($r->getPdRombelSwasta(),$jumdes = 0,$pemisah_ribuan = ".","").'</b></td>
		                  </tr>';

		}

		$templates .= ' </tbody>
		              </table>';

		sqlsrv_free_stmt( $stmt);

		return $templates;
		
	}

	function chartBarPdRombel(){
		$return  = array();
		
		$i = 0;
		$j = 0;
		$k = 0;

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		// $hari_ini .= " 00:00:00";

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $rpd) {
			// $i = $i + $r->getPdDiRombel();
			// $j = $j + $r->getPdBelumDiRombel();
			$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
			$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
			$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());
		}

		$d = new \Criteria();

		$sekarang = date('Y-m-d');

		$d->add(RekapJumlahTotalPeer::TANGGAL, $sekarang);

		$rekapTotal = RekapJumlahTotalPeer::doSelect($d);

		foreach ($rekapTotal as $r) {

			$pd_terdaftar = $r->getPdNasional();
			$pd_belum_di_rombel = $r->getPdNasional() - ($i+$j+$k);
		}

		// -- start
		$values = array();

		$a = array();
		$a['x'] = 'Peserta Didik';
		$a['y'] = $pd_terdaftar;

		array_push($values, $a);

		$array = array();
		$array['key'] = 'Terdaftar';
		$array['values'] = $values;

		array_push($return, $array);
		// -- end

		// -- start
		$values2 = array();

		$b = array();
		$b['x'] = 'Peserta Didik';
		$b['y'] = $i+$j+$k;

		array_push($values2, $b);

		$array2 = array();
		$array2['key'] = 'Telah Masuk Rombel';
		$array2['values'] = $values2;

		array_push($return, $array2);
		// -- end

		// -- start
		$values3 = array();

		$c = array();
		$c['x'] = 'Peserta Didik';
		$c['y'] = $pd_belum_di_rombel;

		array_push($values3, $c);

		$array3 = array();
		$array3['key'] = 'Belum Masuk Rombel';
		$array3['values'] = $values3;

		array_push($return, $array3);
		// -- end

		return json_encode($return);
	}

	function chartPiePdRombel(){
		$return  = array();
		
		$i = 0;
		$j = 0;
		$k = 0;

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		// $hari_ini .= " 00:00:00";

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $rpd) {
			$i = $i+ ($rpd->getPdRombelSmaNegeri() + $rpd->getPdRombelSmaSwasta());
			$j = $j+ ($rpd->getPdRombelSmkNegeri() + $rpd->getPdRombelSmkSwasta());
			$k = $k+ ($rpd->getPdRombelSmlbNegeri() + $rpd->getPdRombelSmlbSwasta());
		}

		$d = new \Criteria();

		$sekarang = date('Y-m-d');

		$d->add(RekapJumlahTotalPeer::TANGGAL, $sekarang);

		$rekapTotal = RekapJumlahTotalPeer::doSelect($d);

		foreach ($rekapTotal as $r) {

			$pd_terdaftar = $r->getPdNasional();
			$pd_belum_di_rombel = $r->getPdNasional() - ($i+$j+$k);
		}

		$array = array();
		$array['key'] = 'Telah Masuk di Rombel';
		$array['y'] = $i+$j+$k;

		$array2 = array();
		$array2['key'] = 'Belum Masuk di Rombel';
		$array2['y'] = $pd_belum_di_rombel;

		array_push($return, $array);
		array_push($return, $array2);

		return json_encode($return);
	}

	function chartPiePdStatusTotal(){
		$i = 0;
		$j = 0;
		// $k = 0;
		// $m = 0;
		// $n = 0;
		// $o = 0;
		// $p = 0;
		// $q = 0;
		$array = array();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getPdRombelNegeri();
			$j = $j + $r->getPdRombelSwasta();
			// $k = $k + $r->getPdTerdaftarSmaNegeri();
			// $m = $m + $r->getPdTerdaftarSmaSwasta();
			// $n = $n + $r->getPdTerdaftarSmkNegeri();
			// $o = $o + $r->getPdTerdaftarSmkSwasta();
			// $p = $p + $r->getPdTerdaftarSmlbNegeri();
			// $q = $q + $r->getPdTerdaftarSmlbSwasta();
		}

		$a1 = array();
		$a1['key'] = 'PD Negeri';
		$a1['y'] = $i;

		$a2 = array();
		$a2['key'] = 'PD Swasta';
		$a2['y'] = $j;

		array_push($array, $a1);
		array_push($array, $a2);

		return json_encode($array);
	}

	function chartPiePdStatusSma(){
		$i = 0;
		$j = 0;
		
		$array = array();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getPdRombelSmaNegeri();
			$j = $j + $r->getPdRombelSmaSwasta();
		}

		$a1 = array();
		$a1['key'] = 'PD SMA Negeri';
		$a1['y'] = $i;

		$a2 = array();
		$a2['key'] = 'PD SMA Swasta';
		$a2['y'] = $j;

		array_push($array, $a1);
		array_push($array, $a2);

		return json_encode($array);
	}

	function chartPiePdStatusSmk(){
		$i = 0;
		$j = 0;
		
		$array = array();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getPdRombelSmkNegeri();
			$j = $j + $r->getPdRombelSmkSwasta();
		}

		$a1 = array();
		$a1['key'] = 'PD SMK Negeri';
		$a1['y'] = $i;

		$a2 = array();
		$a2['key'] = 'PD SMK Swasta';
		$a2['y'] = $j;

		array_push($array, $a1);
		array_push($array, $a2);

		return json_encode($array);
	}

	function chartPiePdStatusSmlb(){
		$i = 0;
		$j = 0;
		
		$array = array();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getPdRombelSmlbNegeri();
			$j = $j + $r->getPdRombelSmlbSwasta();
		}

		$a1 = array();
		$a1['key'] = 'PD SMLB Negeri';
		$a1['y'] = $i;

		$a2 = array();
		$a2['key'] = 'PD SMLB Swasta';
		$a2['y'] = $j;

		array_push($array, $a1);
		array_push($array, $a2);

		return json_encode($array);
	}

	function chartBarPdStatusTotal(){
		$i = 0;
		$j = 0;
		$k = 0;
		$m = 0;
		$n = 0;
		$o = 0;
		$p = 0;
		$q = 0;
		$array = array();
		$return  = array();

		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->addAnd(JumlahPdStatusPeer::TANGGAL, $hari_ini);
		$c->addAnd(JumlahPdStatusPeer::ID_LEVEL_WILAYAH, 1);
		$c->addAnd(JumlahPdStatusPeer::TAHUN_AJARAN_ID, '20141');

		$rekap = JumlahPdStatusPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getPdRombelNegeri();
			$j = $j + $r->getPdRombelSwasta();
			$k = $k + $r->getPdRombelSmaNegeri();
			$m = $m + $r->getPdRombelSmaSwasta();
			$n = $n + $r->getPdRombelSmkNegeri();
			$o = $o + $r->getPdRombelSmkSwasta();
			$p = $p + $r->getPdRombelSmlbNegeri();
			$q = $q + $r->getPdRombelSmlbSwasta();
		}

		// -- start
		$values = array();

		$a = array();
		$a['x'] = 'PD Total';
		$a['y'] = $i;

		$b = array();
		$b['x'] = 'PD SMA';
		$b['y'] = $k;

		$c = array();
		$c['x'] = 'PD SMK';
		$c['y'] = $n;

		$d = array();
		$d['x'] = 'PD SMLB';
		$d['y'] = $p;

		array_push($values, $a);
		array_push($values, $b);
		array_push($values, $c);
		array_push($values, $d);

		$array = array();
		$array['key'] = 'Negeri';
		$array['values'] = $values;

		array_push($return, $array);
		// -- end

		// -- start
		$values2 = array();

		$a = array();
		$a['x'] = 'PD Total';
		$a['y'] = $j;

		$b = array();
		$b['x'] = 'PD SMA';
		$b['y'] = $m;

		$c = array();
		$c['x'] = 'PD SMK';
		$c['y'] = $o;

		$d = array();
		$d['x'] = 'PD SMLB';
		$d['y'] = $q;

		array_push($values2, $a);
		array_push($values2, $b);
		array_push($values2, $c);
		array_push($values2, $d);

		$array2 = array();
		$array2['key'] = 'Swasta';
		$array2['values'] = $values2;

		array_push($return, $array2);
		// -- end

		return json_encode($return);

		
	}

	function getLogSinkronisasi(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');
		$con = $this->initdb();
		$unhash = $this->getUnHash($sekolah_id);

		$sql = "SELECT
					TOP 10 b.nama,
					b.sekolah_id,
					CONVERT (
						VARCHAR (20),
						MAX (begin_sync),
						113
					) AS tgl
				FROM
					dbo.sync_log a
				JOIN dbo.sekolah b ON a.sekolah_id = b.sekolah_id
				WHERE
					end_sync IS NOT NULL
				AND sync_media=1
				AND b.sekolah_id = '".$unhash."'
				GROUP BY
					b.nama,
					b.sekolah_id
				ORDER BY
					MAX (begin_sync) DESC";
				
		$stmt = sqlsrv_query( $con, $sql );
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);

		// return $unhash;
	}

	function getRekapDapodikSekolahStatus(){
		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		$c->add(RekapSekolahPeer::TANGGAL, $hari_ini);

		$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, array(13,14,15,29), \Criteria::IN);
		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 1);

		$sekolahNegeriCount = RekapSekolahPeer::doCount($c);
		// $sekolah = RekapSekolahPeer::doSelect($c);

		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 2);
		$sekolahSwastaCount = RekapSekolahPeer::doCount($c);

		$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, array(13), \Criteria::IN);
		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 1);
		$sekolahSmaNegeriCount = RekapSekolahPeer::doCount($c);

		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 2);
		$sekolahSmaSwastaCount = RekapSekolahPeer::doCount($c);

		$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, array(14,29), \Criteria::IN);
		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 1);
		$sekolahSmlbNegeriCount = RekapSekolahPeer::doCount($c);

		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 2);
		$sekolahSmlbSwastaCount = RekapSekolahPeer::doCount($c);

		$c->add(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, array(15), \Criteria::IN);
		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 1);
		$sekolahSmkNegeriCount = RekapSekolahPeer::doCount($c);

		$c->add(RekapSekolahPeer::STATUS_SEKOLAH, 2);
		$sekolahSmkSwastaCount = RekapSekolahPeer::doCount($c);
		// $array = array();
		
		// $c = new \Criteria();

		// $hari_ini = date('Y-m-d');

		// // $c->add(JumlahSekolahPeer::TANGGAL, $hari_ini);
		// $c->add(JumlahSekolahPeer::ID_LEVEL_WILAYAH, '1');
		// $c->add(JumlahSekolahPeer::SEMESTER_ID, '20141');

		// $rekap = JumlahSekolahPeer::doSelect($c);

		// foreach ($rekap as $r) {
		// 	$i = $i + $r->getStatusNegeri();
		// 	$j = $j + $r->getStatusSwasta();
		// 	$k = $k + $r->getStatusSmaNegeri();
		// 	$l = $l + $r->getStatusSmaSwasta();
		// 	$m = $m + $r->getStatusSmkNegeri();
		// 	$n = $n + $r->getStatusSmkSwasta();
		// 	$o = $o + $r->getStatusSmlbNegeri();
		// 	$p = $p + $r->getStatusSmlbSwasta();
		// }

		$array['status_negeri'] = moneyConverter($sekolahNegeriCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_swasta'] = moneyConverter($sekolahSwastaCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_sma_negeri'] =moneyConverter($sekolahSmaNegeriCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_sma_swasta'] = moneyConverter($sekolahSmaSwastaCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_smk_negeri'] = moneyConverter($sekolahSmkNegeriCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_smk_swasta'] = moneyConverter($sekolahSmkSwastaCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_smlb_negeri'] = moneyConverter($sekolahSmlbNegeriCount,$jumdes = 0,$pemisah_ribuan = ".","");
		$array['status_smlb_swasta'] = moneyConverter($sekolahSmlbSwastaCount,$jumdes = 0,$pemisah_ribuan = ".","");

		$array['status_negeri_persen'] = round((($sekolahNegeriCount / ($sekolahNegeriCount+ $sekolahSwastaCount)) * 100),2);
		$array['status_swasta_persen'] = round((($sekolahSwastaCount / ($sekolahNegeriCount+ $sekolahSwastaCount)) * 100),2);
		$array['status_sma_negeri_persen'] = round((($sekolahSmaNegeriCount / ($sekolahSmaNegeriCount+ $sekolahSmaSwastaCount)) * 100),2);
		$array['status_sma_swasta_persen'] = round((($sekolahSmaSwastaCount / ($sekolahSmaNegeriCount+ $sekolahSmaSwastaCount)) * 100),2);
		$array['status_smk_negeri_persen'] = round((($sekolahSmkNegeriCount / ($sekolahSmkNegeriCount+ $sekolahSmkSwastaCount)) * 100),2);
		$array['status_smk_swasta_persen'] = round((($sekolahSmkSwastaCount / ($sekolahSmkNegeriCount+ $sekolahSmkSwastaCount)) * 100),2);
		$array['status_smlb_negeri_persen'] = round((($sekolahSmlbNegeriCount / ($sekolahSmlbNegeriCount+ $sekolahSmlbSwastaCount)) * 100),2);
		$array['status_smlb_swasta_persen'] = round((($sekolahSmlbSwastaCount / ($sekolahSmlbNegeriCount+ $sekolahSmlbSwastaCount)) * 100),2);

		return json_encode($array);
	}

	function chartPieSekolahStatus(Request $request, Application $app){
		$jenis = $request->get('jenis');

		$array = array();
		$array2 = array();
		$return = array();
		
		$c = new \Criteria();

		$hari_ini = date('Y-m-d');

		// $c->add(JumlahSekolahPeer::TANGGAL, $hari_ini);
		$c->add(JumlahSekolahPeer::ID_LEVEL_WILAYAH, '1');
		$c->add(JumlahSekolahPeer::SEMESTER_ID, '20141');

		$rekap = JumlahSekolahPeer::doSelect($c);

		foreach ($rekap as $r) {
			$i = $i + $r->getStatusNegeri();
			$j = $j + $r->getStatusSwasta();
			$k = $k + $r->getStatusSmaNegeri();
			$l = $l + $r->getStatusSmaSwasta();
			$m = $m + $r->getStatusSmkNegeri();
			$n = $n + $r->getStatusSmkSwasta();
			$o = $o + $r->getStatusSmlbNegeri();
			$p = $p + $r->getStatusSmlbSwasta();
		}


		switch ($jenis) {
			case 'total':

				$array['key'] = 'Sekolah Negeri';
				$array['y'] = $i;

				$array2['key'] = 'Sekolah Swasta';
				$array2['y'] = $j;

				break;
			case 'sma':
				
				$array['key'] = 'SMA Negeri';
				$array['y'] = $k;

				$array2['key'] = 'SMA Swasta';
				$array2['y'] = $l;
				break;

			case 'smk':
				
				$array['key'] = 'SMK Negeri';
				$array['y'] = $m;

				$array2['key'] = 'SMK Swasta';
				$array2['y'] = $n;
				break;

			case 'smlb':
				
				$array['key'] = 'SMLB Negeri';
				$array['y'] = $o;

				$array2['key'] = 'SMLB Swasta';
				$array2['y'] = $p;
				break;
			
			default:
				
				$array['key'] = 'Sekolah Negeri';
				$array['y'] = $i;

				$array2['key'] = 'Sekolah Swasta';
				$array2['y'] = $j;

				break;
		}

		array_push($return, $array);
		array_push($return, $array2);

		return json_encode($return);
	}

	function cekdataun_nasional(Request $request, Application $app){
		
		$this->initDbMysql();
		$hari_ini = date('Y-m-d');
		$kode_wilayah = $request->get('kode_wilayah');
		$id_level = $request->get('id_level');

		if(empty($kode_wilayah)){
			$wilayah_str = 'provinsi';
			$kode_wilayah_str = 'kode_wilayah_provinsi';
			$id_level_wilayah_str = 'id_level_wilayah_provinsi';
			$mst_kode_wilayah_str = 'mst_kode_wilayah_provinsi = 000000';
			$lm = 'cekdataun_propinsi';
			$cn = '../';
			$lcn = '';
		}else{
			if($id_level == 1){
				$wilayah_str = 'kabupaten';
				$kode_wilayah_str = 'kode_wilayah_kabupaten';
				$id_level_wilayah_str = 'id_level_wilayah_kabupaten';
				$mst_kode_wilayah_str = 'mst_kode_wilayah_kabupaten = ' . $kode_wilayah;
				$lm = 'cekdataun_kabupaten';
				$cn = '../../../';
				$lcn = '../../';
			}else if($id_level == 2){
				$wilayah_str = 'kecamatan';
				$kode_wilayah_str = 'kode_wilayah_kecamatan';
				$id_level_wilayah_str = 'id_level_wilayah_kecamatan';
				$mst_kode_wilayah_str = 'mst_kode_wilayah_kecamatan = ' . $kode_wilayah;
				$lm = 'cekdataun_kecamatan';
				$cn = '../../../';
				$lcn = '../../';
			}
		}

		$sql = sprintf("SELECT
					%s as nama,
					%s as kode_wilayah,
					%s as id_level_wilayah,
					sum(pd_kelas_10) as pd_kelas_10,
					sum(pd_kelas_11) as pd_kelas_11,
					sum(pd_kelas_12) as pd_kelas_12,
					sum(pd_kelas_13) as pd_kelas_13,
					sum(pd) as pd
				FROM
					rekap_sekolah
				where tanggal = '%s'
				and %s
				and kode_wilayah_provinsi != 000000
				GROUP BY
					%s",
			mysql_real_escape_string($wilayah_str),
			mysql_real_escape_string($kode_wilayah_str),
			mysql_real_escape_string($id_level_wilayah_str),
			mysql_real_escape_string($hari_ini),
			mysql_real_escape_string($mst_kode_wilayah_str),
			mysql_real_escape_string($kode_wilayah_str)
		);	

		// return $sql;die;
		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th rowspan=2>#</th>
		                    <th rowspan=2>Wilayah</th>
		                    <th style="text-align:center" colspan=4>Kelas</th>
		                    <th style="text-align:right" rowspan=2>Total</th>
		                  </tr>
		                  <tr >
		                  	<th style="text-align:right">10</th>
		                  	<th style="text-align:right">11</th>
		                  	<th style="text-align:right">12</th>
		                  	<th style="text-align:right">13</th>
		                  	<th style="text-align:right">Data Sekolah</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		$i = 1;

		// return $templates;die;

		$result = mysql_query($sql);

		while($row = mysql_fetch_array($result, MYSQL_ASSOC)){

			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td><a href="'.$cn.'laman/'.$lm.'/'.$row['id_level_wilayah'].'/'.$row["kode_wilayah"].'">'.$row["nama"].'</a></td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_10"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_11"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_12"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_13"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>';
		    
		    if($id_level != 2){
		    	$templates .= '<td align="right"><a href="'.$lcn.'lihatSekolah/'.$row['id_level_wilayah'].'/'.$row["kode_wilayah"].'" class="btn btn-default btn-xs">Lihat</a></td>';
		    }else{
		    	$templates .= '<td align="right"><a href="'.$cn.'laman/'.$lm.'/'.$row['id_level_wilayah'].'/'.$row["kode_wilayah"].'" class="btn btn-default btn-xs">Lihat</a></td>';
		    }              
		    
		    $templates .= '</tr>';

		    $pd_kelas_10_total = $pd_kelas_10_total + $row["pd_kelas_10"];
		    $pd_kelas_11_total = $pd_kelas_11_total + $row["pd_kelas_11"];
		    $pd_kelas_12_total = $pd_kelas_12_total + $row["pd_kelas_12"];
		    $pd_kelas_13_total = $pd_kelas_13_total + $row["pd_kelas_13"];
		    $pd_total = $pd_total + $row["pd"];

		    $i++;
		}

		$templates .= '<tr>
	                    <td>'.$i.'</td>
	                    <td><b>Total</b></td>
	                    <td align="right">'.moneyConverter($pd_kelas_10_total,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
	                    <td align="right">'.moneyConverter($pd_kelas_11_total,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
	                    <td align="right">'.moneyConverter($pd_kelas_12_total,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
	                    <td align="right">'.moneyConverter($pd_kelas_13_total,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
	                    <td align="right">'.moneyConverter($pd_total,$jumdes = 0,$pemisah_ribuan = ".","").'</td>
	                  </tr>';

		$templates .= ' </tbody>
		              </table>';

		return $templates;
		// return $sql_jml_siswa;
	}

	function cekdataun_kecamatan(Request $request, Application $app){
		$this->initDbMysql();
		$hari_ini = date('Y-m-d');
		$kode_wilayah = $request->get('kode_wilayah');
		$id_level = $request->get('id_level');

		$sql = sprintf("select * from rekap_sekolah where kode_wilayah_kecamatan = %s and tanggal = '%s'",
			mysql_real_escape_string($kode_wilayah),
			mysql_real_escape_string($hari_ini)
			
		);	

		// return $sql;die;
		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th rowspan=2>#</th>
		                    <th rowspan=2>Sekolah</th>
		                    <th style="text-align:center" colspan=4>Kelas</th>
		                    <th style="text-align:right" rowspan=2>Total</th>
		                  </tr>
		                  <tr >
		                  	<th style="text-align:right">10</th>
		                  	<th style="text-align:right">11</th>
		                  	<th style="text-align:right">12</th>
		                  	<th style="text-align:right">13</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		$i = 1;

		// return $templates;die;

		$result = mysql_query($sql);

		while($row = mysql_fetch_array($result, MYSQL_ASSOC)){

			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td>'.$row["nama"].'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_10"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_11"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_12"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_13"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		return $templates;
	}

	function lihatSekolah(Request $request, Application $app){
		$id_level_wilayah = $request->get('id_level_wilayah');
		$kode_wilayah = $request->get('kode_wilayah');

		$this->initDbMysql();
		$hari_ini = date('Y-m-d');

		$templates = '<table class="table table-striped table-hover " style="font-size:12px">
		                <thead>
		                  <tr>
		                    <th rowspan=2>#</th>
		                    <th rowspan=2>Sekolah</th>
		                    <th rowspan=2>Kecamatan</th>
		                    <th rowspan=2>Kabupaten</th>
		                    <th style="text-align:center" colspan=4>Kelas</th>
		                    <th style="text-align:right" rowspan=2>Total</th>
		                  </tr>
		                  <tr >
		                  	<th style="text-align:right">10</th>
		                  	<th style="text-align:right">11</th>
		                  	<th style="text-align:right">12</th>
		                  	<th style="text-align:right">13</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  ';

		$i = 1;

		if($id_level_wilayah == 1){
			$sql = "select * from rekap_sekolah where kode_wilayah_provinsi = {$kode_wilayah} and tanggal = '{$hari_ini}' order by kabupaten, kecamatan asc";
		}else if($id_level_wilayah == 2){
			$sql = "select * from rekap_sekolah where kode_wilayah_kabupaten = {$kode_wilayah} and tanggal = '{$hari_ini}' order by kabupaten, kecamatan asc";
		}

		$result = mysql_query($sql);

		while($row = mysql_fetch_array($result, MYSQL_ASSOC)){

			$templates .= '<tr>
		                    <td>'.$i.'</td>
		                    <td>'.$row["nama"].'</td>
		                    <td>'.$row["kecamatan"].'</td>
		                    <td>'.$row["kabupaten"].'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_10"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_11"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_12"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd_kelas_13"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                    <td align="right">'.moneyConverter($row["pd"],$jumdes = 0,$pemisah_ribuan = ".","").'</td>
		                  </tr>';

		    $i++;
		}

		$templates .= ' </tbody>
		              </table>';

		return $templates;

		return $kode_wilayah;
	}

	function beritaFeed(Request $request, Application $app){
		$this->initDbMysql();

		$return = array();
		$callBack = array();

		$sql = "select * from berita order by berita_id desc";

		$result = mysql_query($sql);

		while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$arr = array();

			$arr['berita_id'] = $row['BERITA_ID'];
			$arr['judul'] = $row['JUDUL'];
			// $arr['isi'] = $row['ISI'];
			$arr['tanggal'] = $row['TANGGAL'];

			array_push($return, $arr);

			$i++;
		}

		$callBack['results'] = $i;
		$callBack['rows'] = $return;

		return json_encode($callBack);
	}

	function getSekolahLongitudinal(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');
		$semester_id = $request->get('semester_id');

		$con = $this->initdb();
		$unhash = $this->getUnHash($sekolah_id);

		$sql = "select 
					wp.nama as waktu_penyelenggaraan_id_str,
					sl.* 
				from 
					sekolah_longitudinal sl 
				JOIN ref.waktu_penyelenggaraan wp on wp.waktu_penyelenggaraan_id = sl.waktu_penyelenggaraan_id
				where 
					sl.soft_delete = 0 
				and sl.sekolah_id = '{$unhash}' 
				and sl.semester_id = {$semester_id}";

		// return $sql;die;

		$stmt = sqlsrv_query( $con, $sql );

		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);

		return json_encode($row);
	}

}