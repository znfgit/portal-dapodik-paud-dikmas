<?php

namespace library;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use infopendataan\Model as M;

use infopendataan\Model\LogLoginManajemen;
use infopendataan\Model\LogLoginManajemenPeer;

use infopendataan\Model\LogAktivitasManajemen;
use infopendataan\Model\LogAktivitasManajemenPeer;

class Login {
	public function initdb(){
		$serverName = "172.17.3.18"; //serverName\instanceName
		$connectionInfo = array( "Database"=>"Dapodikmen", "UID"=>"UserRpt", "PWD"=>"Reportd1km3n");
		$conn = sqlsrv_connect( $serverName, $connectionInfo);

		return $conn;
	}

	function prosesLogout(Request $request, Application $app){
		$app['session']->clear();
		return $app->redirect('.');
	}

	function proseslogin(Request $request, Application $app){
		$con = $this->initdb();

		// level 1: menghilangkan "karakter ilegal" dari dalam string username dan password
		$user = illegal_char_remover($request->get('email'));
		$pass = illegal_char_remover($request->get('password'));

		//level 2: mengecek apakah string kosong atau tidak
		if(empty($user)){

			$outArr = array();

			$outArr['success'] = false;
			$outArr['pesan'] = 'Kolom Username/Email tidak boleh kosong!';
			
			return json_encode($outArr);

		}else if(empty($pass)){

			$outArr = array();

			$outArr['success'] = false;
			$outArr['pesan'] = 'Kolom Password tidak boleh kosong!';
			
			return json_encode($outArr);

		}else{

			$sqlp = "SELECT
				*,
				lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '".$pass."'), 2) ) as iPassword
			FROM
				pengguna
			WHERE
				username = '".$user."'
			AND Soft_delete = 0
			AND aktif = 1";

			$stmtp = sqlsrv_query( $con, $sqlp );

			$z = 0;

			while( $rowp = sqlsrv_fetch_array( $stmtp, SQLSRV_FETCH_ASSOC) ) {
			    $z++;

			    $ipassword = $rowp['iPassword'];
			    $password = $rowp['password'];
			    $username = $rowp['username'];
			    $sekolah_id = $rowp['sekolah_id'];
			    $nama = $rowp['nama'];
			    $pengguna_id = $rowp['pengguna_id'];
			    $kode_wilayah = $rowp['kode_wilayah'];
			    $peran_id = $rowp['peran_id'];
			    $pengguna_id = $rowp['pengguna_id'];
			   
			    #
			}

			if($z == 1){

				// level 4: mengecek apakah password yang ada sesuai dengan yang di database
				if($ipassword == $password){
					$sql_wilayah = "select * from ref.mst_wilayah where kode_wilayah = '".$kode_wilayah."'";
					$stmt_wilayah = sqlsrv_query( $con, $sql_wilayah );
					$row_wilayah = sqlsrv_fetch_array( $stmt_wilayah, SQLSRV_FETCH_ASSOC);

					// return $sql_wilayah;die;

					if($peran_id == 1 || $peran_id == 2 || $peran_id == 3 || $peran_id == 4 || $peran_id == 5 || $peran_id == 40 || $peran_id == 41 || $peran_id == 42 || $peran_id == 43 || $peran_id == 44){
				    	$id_level_wilayah = 0;
				    	$id_level_wilayah_str = 'Nasional';

				    	$induk = 'Indonesia';
						$induk_id = '000000 ';

				    }else if($peran_id == 6 || $peran_id == 7 || $peran_id == 45 || $peran_id == 46 || $peran_id == 47 || $peran_id == 48){
				    	$id_level_wilayah = 1;
				    	$id_level_wilayah_str = 'Propinsi';
				    }else if($peran_id == 8 || $peran_id == 9 || $peran_id == 49 || $peran_id == 50 || $peran_id == 51 || $peran_id == 52){
				    	$id_level_wilayah = 2;
				    	$id_level_wilayah_str = 'Kabupaten / Kota';
				    }else{
				    	$id_level_wilayah = 99;
				    }


					if($id_level_wilayah == 1 || $id_level_wilayah == 2 || $id_level_wilayah == 99){

						$sql_mst = "select * from ref.mst_wilayah where kode_wilayah = '".$row_wilayah['mst_kode_wilayah']."'";
						$stmt_mst = sqlsrv_query( $con, $sql_mst );
						$row_mst = sqlsrv_fetch_array( $stmt_mst, SQLSRV_FETCH_ASSOC);

						$induk = $row_mst['nama'];
						$induk_id = $row_mst['kode_wilayah'];
					}

					$sql_peran = "select * from ref.peran where peran_id = ".$peran_id;
					$stmt_peran = sqlsrv_query( $con, $sql_peran );
					$row_peran = sqlsrv_fetch_array( $stmt_peran, SQLSRV_FETCH_ASSOC);


					$sql_sekolah = "select * from sekolah where sekolah_id = '".$sekolah_id."'";
					$stmt_sekolah = sqlsrv_query( $con, $sql_sekolah );
					$row_sekolah = sqlsrv_fetch_array( $stmt_sekolah, SQLSRV_FETCH_ASSOC);

					$sql_enkrip = "SELECT
							CONVERT (
								VARCHAR (20),
								HashBytes ('MD5', CAST('".$sekolah_id."' AS VARCHAR(50))),
								2
							) AS enkripsi";
					$stmt_enkrip = sqlsrv_query( $con, $sql_enkrip );
					$row_enkrip = sqlsrv_fetch_array( $stmt_enkrip, SQLSRV_FETCH_ASSOC);

					if($peran_id == 41 || $peran_id == 45 || $peran_id == 49 || $peran_id == 47 || $peran_id == 51){
						$bp = 'SMA';
					}else if($peran_id == 42 || $peran_id == 46 || $peran_id == 52 || $peran_id == 48 || $peran_id == 50){
						$bp = 'SMK';
					}else{
						$bp = 'DIKMEN';
					}

					$app['session']->set('user', array(
						'pengguna_id' => $pengguna_id,
						'username' => $username, 
						'nama' => $nama,
						'kode_wilayah' => $kode_wilayah,
						'sekolah_id' => $row_enkrip['enkripsi'],
						'sekolah_id_str' => $row_sekolah['nama'],
						'peran_id' => $peran_id,
						'peran_id_str' => $row_peran['nama'],
						'id_level_wilayah' => $id_level_wilayah,
						'id_level_wilayah_str' => $id_level_wilayah_str,
						'wilayah_id' => $row_wilayah['kode_wilayah'],
						'wilayah_id_str' => $row_wilayah['nama'],
						'mst_kode_wilayah' => $induk_id,
						'mst_kode_wilayah_str' => $induk,
						'bp' => $bp
					));

					//sekarang masukkan ke log login

					//$ip = $_SERVER['REMOTE_ADDR'];
					//$ip = $this->container->get('request')->getClientIp();

					if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					    $ip = $_SERVER['HTTP_CLIENT_IP'];
					} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					} else {
					    $ip = $_SERVER['REMOTE_ADDR'];
					}

					$u_agent = $_SERVER['HTTP_USER_AGENT'];
					$tanggal = date('Y-m-d H:i:s');

					if($ip == '::1'){
						$ip = '127.0.0.1';
					}

					$bname = 'Unknown';
					$platform = 'Unknown';
					
					//First get the platform?
        
					if (preg_match('/linux/i', $u_agent)) {
						$platform = 'linux';
					}
					elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
						$platform = 'mac';
					}
					elseif (preg_match('/windows|win32/i', $u_agent)) {
						$platform = 'windows';
					}
					
					// Next get the name of the useragent yes separately and for good reason.
					if (preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
					{
						$bname = 'Internet Explorer';
						$ub = "MSIE";
					}
					elseif (preg_match('/Firefox/i',$u_agent))
					{
						$bname = 'Mozilla Firefox';
						$ub = "Firefox";
					}
					elseif (preg_match('/Chrome/i',$u_agent))
					{
						$bname = 'Google Chrome';
						$ub = "Chrome";
					}
					elseif (preg_match('/Safari/i',$u_agent))
					{
						$bname = 'Apple Safari';
						$ub = "Safari";
					}
					elseif (preg_match('/Opera/i',$u_agent))
					{
						$bname = 'Opera';
						$ub = "Opera";
					}
					elseif (preg_match('/Netscape/i',$u_agent))
					{
						$bname = 'Netscape';
						$ub = "Netscape";
					}
					
					// $logLogin = new LogLoginManajemen();
					// $logLogin->setIp($ip);
					// $logLogin->setBrowser($bname);
					// $logLogin->setOs($platform);
					// $logLogin->setKodeWilayah($kode_wilayah);
					// $logLogin->setTanggal($tanggal);
					// $logLogin->setPenggunaId($username);
					// $logLogin->setIdLevelWilayah($id_level_wilayah);

					// $logLogin->save();
	                
					$outArr = array();

					$outArr['success'] = true;
					$outArr['pesan'] = 'Anda Berhasil Masuk. Mohon tunggu sebelum halaman dimuat ulang';

					return json_encode($outArr);
				}else{
					$outArr = array();

					$outArr['success'] = false;
					$outArr['pesan'] = 'Password untuk username '.$username.' tidak sesuai!';

					return json_encode($outArr);
				}
			}else{

				$outArr = array();

				$outArr['success'] = false;
				$outArr['pesan'] = 'Username/Email tidak ditemukan!';

				return json_encode($outArr);
		

			}

		//return '{"success" : false }';
		}
	}
}