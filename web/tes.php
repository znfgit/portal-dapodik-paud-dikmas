<html>
	<head>
		<link rel="stylesheet" type="text/css" href="assets/nvd3/src/nv.d3.css">
		<script src="assets/js/jquery.js"></script>
		<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="assets/nvd3/lib/d3.v3.js" type="text/javascript"></script>
		<script src="assets/nvd3/nv.d3.min.js" type="text/javascript"></script>
	</head>
	<body>
		<style type="text/css">
			#chart1{
				height:500px;
				width:400px
			}
		</style>
		<svg id="chart1"></svg>

		<script type="text/javascript">
			function getTotalCount(datas) {
				  // console.log(datas);
				  var sum = 0;
				  var length = datas.length;
				  for (var i = 0; i < length; i++) {
				    sum += datas[i].y;
				    // console.log(i);
				  }
				  // console.log(sum);
				  return sum;
				};

				d3.json('http://dapo.dikmen.kemdikbud.go.id/portal/web/tesdata', function(json) {
					// d3.json('http://223.27.144.201:8888/chart/data/profilsekolah_chart1_jumlah_sekolah.json', function(json) {
					  //console.log(json);
					  var countData = getTotalCount(json);
					  nv.addGraph(function() {
					      var width = 600,
					          height = 500
					      ;

					      var chart = nv.models.pieChart()
					          .x(function(d) { return d.key })
					          .y(function(d) { return d.y })
					          .showLabels(false)
					          .values(function(d) {
					            //console.log(d);
					            return d;
					          })
					          .color(d3.scale.category10().range())
					      ;

					      d3.select("#chart1")
					        .datum([json])
					        .transition().duration(500)
					        .call(chart)
					      ;
					      nv.utils.windowResize(chart.update);

					      return chart;
					  });
					});
		</script>
	</body>
</html>

