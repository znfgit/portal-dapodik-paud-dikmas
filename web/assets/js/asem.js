$(document).ready(function(){ 

    $('.login-btn').click(function (e){

        $('.peringatan').remove();

        $('.login-btn').html('Memproses...');
        $('.login-btn').removeClass( "btn-primary" );

        $('.login-btn').addClass( "btn-default disabled" );
        $('.form-control').prop("disabled", true);

        var email = $('#inputEmail').val();
        var pass = $('#inputPassword').val();

        $.ajax({
            data: { email: email, password: pass },
            type: "POST",
            url: "proseslogin",
            success: function(isi){

                var obj = jQuery.parseJSON(isi);

                if(obj.success == true){
                    $('.form-login-panel').append('<div class="alert alert-dismissable alert-success peringatan">' + 
                        obj.pesan + 
                    '</div>');

                    setTimeout("location.href = '/';",3000);


                }else{
                    $('.form-login-panel').append('<div class="alert alert-dismissable alert-danger peringatan">' + obj.pesan + '</div>');
                }
               
                $('.login-btn').removeClass( "btn-default disabled" ); 
                $('.login-btn').addClass( "btn-primary" );

                $('.form-control').prop("disabled", false);
                $('.login-btn').html('Masuk');
            },
            error: function(){
                $('.form-login-panel').append('<div class="alert alert-dismissable alert-danger peringatan">Ada Kesalahan pada sistem. Mohon tunggu beberapa saat</div>');
                
                $('.login-btn').removeClass( "btn-default disabled" ); 
                $('.login-btn').addClass( "btn-primary" );

                $('.form-control').prop("disabled", false);
                $('.login-btn').html('Masuk');

            }            
        });
      });
    });