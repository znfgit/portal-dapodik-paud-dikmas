d3.json('API/1', function(json) {
  var countData = getTotalCountPie(json);
  var jWarna = ucolor(json);
  nv.addGraph(function() {
      var width = 600,
          height = 600
      ;

      var chart = nv.models.pieChart()
          .x(function(d) { return d.key })
          .y(function(d) { return d.y })
          .tooltipContent(function(key, y, e, graph) {
            var re = /,/gi;
            var re2 = /\.00/gi;
            // replace .00
            var x = y.replace(re2, "");
            // replace , to .
            var x = x.replace(re, "");
            var percent = Math.round((x/countData)*100);
            return '<h3 class="btn-sm"><strong>' + key + '</strong></h3>' +
                   '<p>' +  x + ' <strong>(' + percent + '%)</strong>' + '</p>'
          })
          .values(function(d) {
            return d;
          })
		  .color(jWarna);
      ;

      d3.select("#chart1")
        .datum([json])
        .transition().duration(500)
        .call(chart)
      ;
      nv.utils.windowResize(chart.update);
      return chart;
  });
});