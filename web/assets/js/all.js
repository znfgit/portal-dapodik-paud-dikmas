
(function(){var nv=window.nv||{};nv.version='0.0.1a';nv.dev=true
window.nv=nv;nv.tooltip={};nv.utils={};nv.models={};nv.charts={};nv.graphs=[];nv.logs={};nv.dispatch=d3.dispatch('render_start','render_end');if(nv.dev){nv.dispatch.on('render_start',function(e){nv.logs.startTime=+new Date();});nv.dispatch.on('render_end',function(e){nv.logs.endTime=+new Date();nv.logs.totalTime=nv.logs.endTime-nv.logs.startTime;nv.log('total',nv.logs.totalTime);});}
nv.log=function(){if(nv.dev&&console.log&&console.log.apply)
console.log.apply(console,arguments)
else if(nv.dev&&console.log&&Function.prototype.bind){var log=Function.prototype.bind.call(console.log,console);log.apply(console,arguments);}
return arguments[arguments.length-1];};nv.render=function render(step){step=step||1;nv.render.active=true;nv.dispatch.render_start();setTimeout(function(){var chart,graph;for(var i=0;i<step&&(graph=nv.render.queue[i]);i++){chart=graph.generate();if(typeof graph.callback==typeof(Function))graph.callback(chart);nv.graphs.push(chart);}
nv.render.queue.splice(0,i);if(nv.render.queue.length)setTimeout(arguments.callee,0);else{nv.render.active=false;nv.dispatch.render_end();}},0);};nv.render.active=false;nv.render.queue=[];nv.addGraph=function(obj){if(typeof arguments[0]===typeof(Function))
obj={generate:arguments[0],callback:arguments[1]};nv.render.queue.push(obj);if(!nv.render.active)nv.render();};nv.identity=function(d){return d;};nv.strip=function(s){return s.replace(/(\s|&)/g,'');};function daysInMonth(month,year){return(new Date(year,month+1,0)).getDate();}
function d3_time_range(floor,step,number){return function(t0,t1,dt){var time=floor(t0),times=[];if(time<t0)step(time);if(dt>1){while(time<t1){var date=new Date(+time);if((number(date)%dt===0))times.push(date);step(time);}}else{while(time<t1){times.push(new Date(+time));step(time);}}
return times;};}
d3.time.monthEnd=function(date){return new Date(date.getFullYear(),date.getMonth(),0);};d3.time.monthEnds=d3_time_range(d3.time.monthEnd,function(date){date.setUTCDate(date.getUTCDate()+1);date.setDate(daysInMonth(date.getMonth()+1,date.getFullYear()));},function(date){return date.getMonth();});(function(){var nvtooltip=window.nv.tooltip={};nvtooltip.show=function(pos,content,gravity,dist,parentContainer,classes){var container=document.createElement('div');container.className='nvtooltip '+(classes?classes:'xy-tooltip');gravity=gravity||'s';dist=dist||20;var body=parentContainer;if(!parentContainer||parentContainer.tagName.match(/g|svg/i)){body=document.getElementsByTagName('body')[0];}
container.innerHTML=content;container.style.left=0;container.style.top=0;container.style.opacity=0;body.appendChild(container);var height=parseInt(container.offsetHeight),width=parseInt(container.offsetWidth),windowWidth=nv.utils.windowSize().width,windowHeight=nv.utils.windowSize().height,scrollTop=window.scrollY,scrollLeft=window.scrollX,left,top;windowHeight=window.innerWidth>=document.body.scrollWidth?windowHeight:windowHeight-16;windowWidth=window.innerHeight>=document.body.scrollHeight?windowWidth:windowWidth-16;var tooltipTop=function(Elem){var offsetTop=top;do{if(!isNaN(Elem.offsetTop)){offsetTop+=(Elem.offsetTop);}}while(Elem=Elem.offsetParent);return offsetTop;}
var tooltipLeft=function(Elem){var offsetLeft=left;do{if(!isNaN(Elem.offsetLeft)){offsetLeft+=(Elem.offsetLeft);}}while(Elem=Elem.offsetParent);return offsetLeft;}
switch(gravity){case'e':left=pos[0]-width-dist;top=pos[1]-(height/2);var tLeft=tooltipLeft(container);var tTop=tooltipTop(container);if(tLeft<scrollLeft)left=pos[0]+dist>scrollLeft?pos[0]+dist:scrollLeft-tLeft+left;if(tTop<scrollTop)top=scrollTop-tTop+top;if(tTop+height>scrollTop+windowHeight)top=scrollTop+windowHeight-tTop+top-height;break;case'w':left=pos[0]+dist;top=pos[1]-(height/2);if(tLeft+width>windowWidth)left=pos[0]-width-dist;if(tTop<scrollTop)top=scrollTop+5;if(tTop+height>scrollTop+windowHeight)top=scrollTop-height-5;break;case'n':left=pos[0]-(width/2)-5;top=pos[1]+dist;var tLeft=tooltipLeft(container);var tTop=tooltipTop(container);if(tLeft<scrollLeft)left=scrollLeft+5;if(tLeft+width>windowWidth)left=left-width/2+5;if(tTop+height>scrollTop+windowHeight)top=scrollTop+windowHeight-tTop+top-height;break;case's':left=pos[0]-(width/2);top=pos[1]-height-dist;var tLeft=tooltipLeft(container);var tTop=tooltipTop(container);if(tLeft<scrollLeft)left=scrollLeft+5;if(tLeft+width>windowWidth)left=left-width/2+5;if(scrollTop>tTop)top=scrollTop;break;}
container.style.left=left+'px';container.style.top=top+'px';container.style.opacity=1;container.style.position='absolute';container.style.pointerEvents='none';return container;};nvtooltip.cleanup=function(){var tooltips=document.getElementsByClassName('nvtooltip');var purging=[];while(tooltips.length){purging.push(tooltips[0]);tooltips[0].style.transitionDelay='0 !important';tooltips[0].style.opacity=0;tooltips[0].className='nvtooltip-pending-removal';}
setTimeout(function(){while(purging.length){var removeMe=purging.pop();removeMe.parentNode.removeChild(removeMe);}},500);};})();nv.utils.windowSize=function(){var size={width:640,height:480};if(document.body&&document.body.offsetWidth){size.width=document.body.offsetWidth;size.height=document.body.offsetHeight;}
if(document.compatMode=='CSS1Compat'&&document.documentElement&&document.documentElement.offsetWidth){size.width=document.documentElement.offsetWidth;size.height=document.documentElement.offsetHeight;}
if(window.innerWidth&&window.innerHeight){size.width=window.innerWidth;size.height=window.innerHeight;}
return(size);};nv.utils.windowResize=function(fun){var oldresize=window.onresize;window.onresize=function(e){if(typeof oldresize=='function')oldresize(e);fun(e);}}
nv.utils.getColor=function(color){if(!arguments.length)return nv.utils.defaultColor();if(Object.prototype.toString.call(color)==='[object Array]')
return function(d,i){return d.color||color[i%color.length];};else
return color;}
nv.utils.defaultColor=function(){var colors=d3.scale.category20().range();return function(d,i){return d.color||colors[i%colors.length]};}
nv.utils.customTheme=function(dictionary,getKey,defaultColors){getKey=getKey||function(series){return series.key};defaultColors=defaultColors||d3.scale.category20().range();var defIndex=defaultColors.length;return function(series,index){var key=getKey(series);if(!defIndex)defIndex=defaultColors.length;if(typeof dictionary[key]!=="undefined")
return(typeof dictionary[key]==="function")?dictionary[key]():dictionary[key];else
return defaultColors[--defIndex];}}
nv.utils.pjax=function(links,content){d3.selectAll(links).on("click",function(){history.pushState(this.href,this.textContent,this.href);load(this.href);d3.event.preventDefault();});function load(href){d3.html(href,function(fragment){var target=d3.select(content).node();target.parentNode.replaceChild(d3.select(fragment).select(content).node(),target);nv.utils.pjax(links,content);});}
d3.select(window).on("popstate",function(){if(d3.event.state)load(d3.event.state);});}
nv.utils.calcApproxTextWidth=function(svgTextElem){if(svgTextElem instanceof d3.selection){var fontSize=parseInt(svgTextElem.style("font-size").replace("px",""));var textLength=svgTextElem.text().length;return textLength*fontSize*0.5;}
return 0;};nv.models.axis=function(){var axis=d3.svg.axis();var margin={top:0,right:0,bottom:0,left:0},width=75,height=60,scale=d3.scale.linear(),axisLabelText=null,showMaxMin=true,highlightZero=true,rotateLabels=0,rotateYLabel=true,staggerLabels=false,isOrdinal=false,ticks=null;axis.scale(scale).orient('bottom').tickFormat(function(d){return d});var scale0;function chart(selection){selection.each(function(data){var container=d3.select(this);var wrap=container.selectAll('g.nv-wrap.nv-axis').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-axis');var gEnter=wrapEnter.append('g');var g=wrap.select('g')
if(ticks!==null)
axis.ticks(ticks);else if(axis.orient()=='top'||axis.orient()=='bottom')
axis.ticks(Math.abs(scale.range()[1]-scale.range()[0])/100);d3.transition(g).call(axis);scale0=scale0||axis.scale();var fmt=axis.tickFormat();if(fmt==null){fmt=scale0.tickFormat();}
var axisLabel=g.selectAll('text.nv-axislabel').data([axisLabelText||null]);axisLabel.exit().remove();switch(axis.orient()){case'top':axisLabel.enter().append('text').attr('class','nv-axislabel');var w=(scale.range().length==2)?scale.range()[1]:(scale.range()[scale.range().length-1]+(scale.range()[1]-scale.range()[0]));axisLabel.attr('text-anchor','middle').attr('y',0).attr('x',w/2);if(showMaxMin){var axisMaxMin=wrap.selectAll('g.nv-axisMaxMin').data(scale.domain());axisMaxMin.enter().append('g').attr('class','nv-axisMaxMin').append('text');axisMaxMin.exit().remove();axisMaxMin.attr('transform',function(d,i){return'translate('+scale(d)+',0)'}).select('text').attr('dy','0em').attr('y',-axis.tickPadding()).attr('text-anchor','middle').text(function(d,i){var v=fmt(d);return(''+v).match('NaN')?'':v;});d3.transition(axisMaxMin).attr('transform',function(d,i){return'translate('+scale.range()[i]+',0)'});}
break;case'bottom':var xLabelMargin=36;var maxTextWidth=30;var xTicks=g.selectAll('g').select("text");if(rotateLabels%360){xTicks.each(function(d,i){var width=this.getBBox().width;if(width>maxTextWidth)maxTextWidth=width;});var sin=Math.abs(Math.sin(rotateLabels*Math.PI/180));var xLabelMargin=(sin?sin*maxTextWidth:maxTextWidth)+30;xTicks.attr('transform',function(d,i,j){return'rotate('+rotateLabels+' 0,0)'}).attr('text-anchor',rotateLabels%360>0?'start':'end');}
axisLabel.enter().append('text').attr('class','nv-axislabel');var w=(scale.range().length==2)?scale.range()[1]:(scale.range()[scale.range().length-1]+(scale.range()[1]-scale.range()[0]));axisLabel.attr('text-anchor','middle').attr('y',xLabelMargin).attr('x',w/2);if(showMaxMin){var axisMaxMin=wrap.selectAll('g.nv-axisMaxMin').data([scale.domain()[0],scale.domain()[scale.domain().length-1]]);axisMaxMin.enter().append('g').attr('class','nv-axisMaxMin').append('text');axisMaxMin.exit().remove();axisMaxMin.attr('transform',function(d,i){return'translate('+(scale(d)+(isOrdinal?scale.rangeBand()/2:0))+',0)'}).select('text').attr('dy','.71em').attr('y',axis.tickPadding()).attr('transform',function(d,i,j){return'rotate('+rotateLabels+' 0,0)'}).attr('text-anchor',rotateLabels?(rotateLabels%360>0?'start':'end'):'middle').text(function(d,i){var v=fmt(d);return(''+v).match('NaN')?'':v;});d3.transition(axisMaxMin).attr('transform',function(d,i){return'translate('+(scale(d)+(isOrdinal?scale.rangeBand()/2:0))+',0)'});}
if(staggerLabels)
xTicks.attr('transform',function(d,i){return'translate(0,'+(i%2==0?'0':'12')+')'});break;case'right':axisLabel.enter().append('text').attr('class','nv-axislabel');axisLabel.attr('text-anchor',rotateYLabel?'middle':'begin').attr('transform',rotateYLabel?'rotate(90)':'').attr('y',rotateYLabel?(-Math.max(margin.right,width)+12):-10).attr('x',rotateYLabel?(scale.range()[0]/2):axis.tickPadding());if(showMaxMin){var axisMaxMin=wrap.selectAll('g.nv-axisMaxMin').data(scale.domain());axisMaxMin.enter().append('g').attr('class','nv-axisMaxMin').append('text').style('opacity',0);axisMaxMin.exit().remove();axisMaxMin.attr('transform',function(d,i){return'translate(0,'+scale(d)+')'}).select('text').attr('dy','.32em').attr('y',0).attr('x',axis.tickPadding()).attr('text-anchor','start').text(function(d,i){var v=fmt(d);return(''+v).match('NaN')?'':v;});d3.transition(axisMaxMin).attr('transform',function(d,i){return'translate(0,'+scale.range()[i]+')'}).select('text').style('opacity',1);}
break;case'left':axisLabel.enter().append('text').attr('class','nv-axislabel');axisLabel.attr('text-anchor',rotateYLabel?'middle':'end').attr('transform',rotateYLabel?'rotate(-90)':'').attr('y',rotateYLabel?(-Math.max(margin.left,width)+12):-10).attr('x',rotateYLabel?(-scale.range()[0]/2):-axis.tickPadding());if(showMaxMin){var axisMaxMin=wrap.selectAll('g.nv-axisMaxMin').data(scale.domain());axisMaxMin.enter().append('g').attr('class','nv-axisMaxMin').append('text').style('opacity',0);axisMaxMin.exit().remove();axisMaxMin.attr('transform',function(d,i){return'translate(0,'+scale0(d)+')'}).select('text').attr('dy','.32em').attr('y',0).attr('x',-axis.tickPadding()).attr('text-anchor','end').text(function(d,i){var v=fmt(d);return(''+v).match('NaN')?'':v;});d3.transition(axisMaxMin).attr('transform',function(d,i){return'translate(0,'+scale.range()[i]+')'}).select('text').style('opacity',1);}
break;}
axisLabel.text(function(d){return d});if(showMaxMin&&(axis.orient()==='left'||axis.orient()==='right')){g.selectAll('g').each(function(d,i){d3.select(this).select('text').attr('opacity',1);if(scale(d)<scale.range()[1]+10||scale(d)>scale.range()[0]-10){if(d>1e-10||d<-1e-10)
d3.select(this).attr('opacity',0);d3.select(this).select('text').attr('opacity',0);}});if(scale.domain()[0]==scale.domain()[1]&&scale.domain()[0]==0)
wrap.selectAll('g.nv-axisMaxMin').style('opacity',function(d,i){return!i?1:0});}
if(showMaxMin&&(axis.orient()==='top'||axis.orient()==='bottom')){var maxMinRange=[];wrap.selectAll('g.nv-axisMaxMin').each(function(d,i){try{if(i)
maxMinRange.push(scale(d)-this.getBBox().width-4)
else
maxMinRange.push(scale(d)+this.getBBox().width+4)}catch(err){if(i)
maxMinRange.push(scale(d)-4)
else
maxMinRange.push(scale(d)+4)}});g.selectAll('g').each(function(d,i){if(scale(d)<maxMinRange[0]||scale(d)>maxMinRange[1]){if(d>1e-10||d<-1e-10)
d3.select(this).remove();else
d3.select(this).select('text').remove();}});}
if(highlightZero)
g.selectAll('line.tick').filter(function(d){return!parseFloat(Math.round(d*100000)/1000000)}).classed('zero',true);scale0=scale.copy();});return chart;}
chart.axis=axis;d3.rebind(chart,axis,'orient','tickValues','tickSubdivide','tickSize','tickPadding','tickFormat');d3.rebind(chart,scale,'domain','range','rangeBand','rangeBands');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;}
chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.ticks=function(_){if(!arguments.length)return ticks;ticks=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.axisLabel=function(_){if(!arguments.length)return axisLabelText;axisLabelText=_;return chart;}
chart.showMaxMin=function(_){if(!arguments.length)return showMaxMin;showMaxMin=_;return chart;}
chart.highlightZero=function(_){if(!arguments.length)return highlightZero;highlightZero=_;return chart;}
chart.scale=function(_){if(!arguments.length)return scale;scale=_;axis.scale(scale);isOrdinal=typeof scale.rangeBands==='function';d3.rebind(chart,scale,'domain','range','rangeBand','rangeBands');return chart;}
chart.rotateYLabel=function(_){if(!arguments.length)return rotateYLabel;rotateYLabel=_;return chart;}
chart.rotateLabels=function(_){if(!arguments.length)return rotateLabels;rotateLabels=_;return chart;}
chart.staggerLabels=function(_){if(!arguments.length)return staggerLabels;staggerLabels=_;return chart;};return chart;}
nv.models.historicalBar=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,id=Math.floor(Math.random()*10000),x=d3.scale.linear(),y=d3.scale.linear(),getX=function(d){return d.x},getY=function(d){return d.y},forceX=[],forceY=[0],padData=false,clipEdge=true,color=nv.utils.defaultColor(),xDomain,yDomain,dispatch=d3.dispatch('chartClick','elementClick','elementDblClick','elementMouseover','elementMouseout');function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);x.domain(xDomain||d3.extent(data[0].values.map(getX).concat(forceX)))
if(padData)
x.range([availableWidth*.5/data[0].values.length,availableWidth*(data[0].values.length-.5)/data[0].values.length]);else
x.range([0,availableWidth]);y.domain(yDomain||d3.extent(data[0].values.map(getY).concat(forceY))).range([availableHeight,0]);if(x.domain()[0]===x.domain()[1]||y.domain()[0]===y.domain()[1])singlePoint=true;if(x.domain()[0]===x.domain()[1])
x.domain()[0]?x.domain([x.domain()[0]-x.domain()[0]*0.01,x.domain()[1]+x.domain()[1]*0.01]):x.domain([-1,1]);if(y.domain()[0]===y.domain()[1])
y.domain()[0]?y.domain([y.domain()[0]+y.domain()[0]*0.01,y.domain()[1]-y.domain()[1]*0.01]):y.domain([-1,1]);var wrap=container.selectAll('g.nv-wrap.nv-bar').data([data[0].values]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-bar');var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-bars');wrap.attr('transform','translate('+margin.left+','+margin.top+')');container.on('click',function(d,i){dispatch.chartClick({data:d,index:i,pos:d3.event,id:id});});defsEnter.append('clipPath').attr('id','nv-chart-clip-path-'+id).append('rect');wrap.select('#nv-chart-clip-path-'+id+' rect').attr('width',availableWidth).attr('height',availableHeight);g.attr('clip-path',clipEdge?'url(#nv-chart-clip-path-'+id+')':'');var bars=wrap.select('.nv-bars').selectAll('.nv-bar').data(function(d){return d});bars.exit().remove();var barsEnter=bars.enter().append('rect').attr('x',0).attr('y',function(d,i){return y(Math.max(0,getY(d,i)))}).attr('height',function(d,i){return Math.abs(y(getY(d,i))-y(0))}).on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.elementMouseover({point:d,series:data[0],pos:[x(getX(d,i)),y(getY(d,i))],pointIndex:i,seriesIndex:0,e:d3.event});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.elementMouseout({point:d,series:data[0],pointIndex:i,seriesIndex:0,e:d3.event});}).on('click',function(d,i){dispatch.elementClick({value:getY(d,i),data:d,index:i,pos:[x(getX(d,i)),y(getY(d,i))],e:d3.event,id:id});d3.event.stopPropagation();}).on('dblclick',function(d,i){dispatch.elementDblClick({value:getY(d,i),data:d,index:i,pos:[x(getX(d,i)),y(getY(d,i))],e:d3.event,id:id});d3.event.stopPropagation();});bars.attr('fill',function(d,i){return color(d,i);}).attr('class',function(d,i,j){return(getY(d,i)<0?'nv-bar negative':'nv-bar positive')+' nv-bar-'+j+'-'+i}).attr('transform',function(d,i){return'translate('+(x(getX(d,i))-availableWidth/data[0].values.length*.45)+',0)';}).attr('width',(availableWidth/data[0].values.length)*.9)
d3.transition(bars).attr('y',function(d,i){return getY(d,i)<0?y(0):y(0)-y(getY(d,i))<1?y(0)-1:y(getY(d,i))}).attr('height',function(d,i){return Math.max(Math.abs(y(getY(d,i))-y(0)),1)});});return chart;}
chart.dispatch=dispatch;chart.x=function(_){if(!arguments.length)return getX;getX=_;return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.forceX=function(_){if(!arguments.length)return forceX;forceX=_;return chart;};chart.forceY=function(_){if(!arguments.length)return forceY;forceY=_;return chart;};chart.padData=function(_){if(!arguments.length)return padData;padData=_;return chart;};chart.clipEdge=function(_){if(!arguments.length)return clipEdge;clipEdge=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};return chart;}
nv.models.bullet=function(){var margin={top:0,right:0,bottom:0,left:0},orient='left',reverse=false,ranges=function(d){return d.ranges},markers=function(d){return d.markers},measures=function(d){return d.measures},forceX=[0],width=380,height=30,tickFormat=null,color=nv.utils.getColor(['#1f77b4']),dispatch=d3.dispatch('elementMouseover','elementMouseout');function chart(selection){selection.each(function(d,i){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);var rangez=ranges.call(this,d,i).slice().sort(d3.descending),markerz=markers.call(this,d,i).slice().sort(d3.descending),measurez=measures.call(this,d,i).slice().sort(d3.descending);var x1=d3.scale.linear().domain(d3.extent(d3.merge([forceX,rangez]))).range(reverse?[availableWidth,0]:[0,availableWidth]);var x0=this.__chart__||d3.scale.linear().domain([0,Infinity]).range(x1.range());this.__chart__=x1;var rangeMin=d3.min(rangez),rangeMax=d3.max(rangez),rangeAvg=rangez[1];var wrap=container.selectAll('g.nv-wrap.nv-bullet').data([d]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-bullet');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('rect').attr('class','nv-range nv-rangeMax');gEnter.append('rect').attr('class','nv-range nv-rangeAvg');gEnter.append('rect').attr('class','nv-range nv-rangeMin');gEnter.append('rect').attr('class','nv-measure');gEnter.append('path').attr('class','nv-markerTriangle');wrap.attr('transform','translate('+margin.left+','+margin.top+')');var w0=function(d){return Math.abs(x0(d)-x0(0))},w1=function(d){return Math.abs(x1(d)-x1(0))};var xp0=function(d){return d<0?x0(d):x0(0)},xp1=function(d){return d<0?x1(d):x1(0)};g.select('rect.nv-rangeMax').attr('height',availableHeight).attr('width',w1(rangeMax>0?rangeMax:rangeMin)).attr('x',xp1(rangeMax>0?rangeMax:rangeMin)).datum(rangeMax>0?rangeMax:rangeMin)
g.select('rect.nv-rangeAvg').attr('height',availableHeight).attr('width',w1(rangeAvg)).attr('x',xp1(rangeAvg)).datum(rangeAvg)
g.select('rect.nv-rangeMin').attr('height',availableHeight).attr('width',w1(rangeMax)).attr('x',xp1(rangeMax)).attr('width',w1(rangeMax>0?rangeMin:rangeMax)).attr('x',xp1(rangeMax>0?rangeMin:rangeMax)).datum(rangeMax>0?rangeMin:rangeMax)
g.select('rect.nv-measure').style('fill',color).attr('height',availableHeight/3).attr('y',availableHeight/3).attr('width',measurez<0?x1(0)-x1(measurez[0]):x1(measurez[0])-x1(0)).attr('x',xp1(measurez)).on('mouseover',function(){dispatch.elementMouseover({value:measurez[0],label:'Current',pos:[x1(measurez[0]),availableHeight/2]})}).on('mouseout',function(){dispatch.elementMouseout({value:measurez[0],label:'Current'})})
var h3=availableHeight/6;if(markerz[0]){g.selectAll('path.nv-markerTriangle').attr('transform',function(d){return'translate('+x1(markerz[0])+','+(availableHeight/2)+')'}).attr('d','M0,'+h3+'L'+h3+','+(-h3)+' '+(-h3)+','+(-h3)+'Z').on('mouseover',function(){dispatch.elementMouseover({value:markerz[0],label:'Previous',pos:[x1(markerz[0]),availableHeight/2]})}).on('mouseout',function(){dispatch.elementMouseout({value:markerz[0],label:'Previous'})});}else{g.selectAll('path.nv-markerTriangle').remove();}
wrap.selectAll('.nv-range').on('mouseover',function(d,i){var label=!i?"Maximum":i==1?"Mean":"Minimum";dispatch.elementMouseover({value:d,label:label,pos:[x1(d),availableHeight/2]})}).on('mouseout',function(d,i){var label=!i?"Maximum":i==1?"Mean":"Minimum";dispatch.elementMouseout({value:d,label:label})})});return chart;}
chart.dispatch=dispatch;chart.orient=function(_){if(!arguments.length)return orient;orient=_;reverse=orient=='right'||orient=='bottom';return chart;};chart.ranges=function(_){if(!arguments.length)return ranges;ranges=_;return chart;};chart.markers=function(_){if(!arguments.length)return markers;markers=_;return chart;};chart.measures=function(_){if(!arguments.length)return measures;measures=_;return chart;};chart.forceX=function(_){if(!arguments.length)return forceX;forceX=_;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.tickFormat=function(_){if(!arguments.length)return tickFormat;tickFormat=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};return chart;};nv.models.bulletChart=function(){var bullet=nv.models.bullet();var orient='left',reverse=false,margin={top:5,right:40,bottom:20,left:120},ranges=function(d){return d.ranges},markers=function(d){return d.markers},measures=function(d){return d.measures},width=null,height=55,tickFormat=null,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+x+'</h3>'+'<p>'+y+'</p>'},noData='No Data Available.',dispatch=d3.dispatch('tooltipShow','tooltipHide');var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0)+margin.left,top=e.pos[1]+(offsetElement.offsetTop||0)+margin.top,content=tooltip(e.key,e.label,e.value,e,chart);nv.tooltip.show([left,top],content,e.value<0?'e':'w',null,offsetElement);};function chart(selection){selection.each(function(d,i){var container=d3.select(this);var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,that=this;chart.update=function(){chart(selection)};chart.container=this;if(!d||!ranges.call(this,d,i)){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',18+margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
var rangez=ranges.call(this,d,i).slice().sort(d3.descending),markerz=markers.call(this,d,i).slice().sort(d3.descending),measurez=measures.call(this,d,i).slice().sort(d3.descending);var wrap=container.selectAll('g.nv-wrap.nv-bulletChart').data([d]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-bulletChart');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-bulletWrap');gEnter.append('g').attr('class','nv-titles');wrap.attr('transform','translate('+margin.left+','+margin.top+')');var x1=d3.scale.linear().domain([0,Math.max(rangez[0],markerz[0],measurez[0])]).range(reverse?[availableWidth,0]:[0,availableWidth]);var x0=this.__chart__||d3.scale.linear().domain([0,Infinity]).range(x1.range());this.__chart__=x1;var w0=function(d){return Math.abs(x0(d)-x0(0))},w1=function(d){return Math.abs(x1(d)-x1(0))};var title=gEnter.select('.nv-titles').append('g').attr('text-anchor','end').attr('transform','translate(-6,'+(height-margin.top-margin.bottom)/2+')');title.append('text').attr('class','nv-title').text(function(d){return d.title;});title.append('text').attr('class','nv-subtitle').attr('dy','1em').text(function(d){return d.subtitle;});bullet.width(availableWidth).height(availableHeight)
var bulletWrap=g.select('.nv-bulletWrap');d3.transition(bulletWrap).call(bullet);var format=tickFormat||x1.tickFormat(availableWidth/100);var tick=g.selectAll('g.nv-tick').data(x1.ticks(availableWidth/50),function(d){return this.textContent||format(d);});var tickEnter=tick.enter().append('g').attr('class','nv-tick').attr('transform',function(d){return'translate('+x0(d)+',0)'}).style('opacity',1e-6);tickEnter.append('line').attr('y1',availableHeight).attr('y2',availableHeight*7/6);tickEnter.append('text').attr('text-anchor','middle').attr('dy','1em').attr('y',availableHeight*7/6).text(format);var tickUpdate=d3.transition(tick).attr('transform',function(d){return'translate('+x1(d)+',0)'}).style('opacity',1);tickUpdate.select('line').attr('y1',availableHeight).attr('y2',availableHeight*7/6);tickUpdate.select('text').attr('y',availableHeight*7/6);d3.transition(tick.exit()).attr('transform',function(d){return'translate('+x1(d)+',0)'}).style('opacity',1e-6).remove();dispatch.on('tooltipShow',function(e){e.key=d.title;if(tooltips)showTooltip(e,that.parentNode);});});d3.timer.flush();return chart;}
bullet.dispatch.on('elementMouseover.tooltip',function(e){dispatch.tooltipShow(e);});bullet.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.bullet=bullet;d3.rebind(chart,bullet,'color');chart.orient=function(x){if(!arguments.length)return orient;orient=x;reverse=orient=='right'||orient=='bottom';return chart;};chart.ranges=function(x){if(!arguments.length)return ranges;ranges=x;return chart;};chart.markers=function(x){if(!arguments.length)return markers;markers=x;return chart;};chart.measures=function(x){if(!arguments.length)return measures;measures=x;return chart;};chart.width=function(x){if(!arguments.length)return width;width=x;return chart;};chart.height=function(x){if(!arguments.length)return height;height=x;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.tickFormat=function(x){if(!arguments.length)return tickFormat;tickFormat=x;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;};nv.models.cumulativeLineChart=function(){var lines=nv.models.line(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend(),controls=nv.models.legend();var margin={top:30,right:30,bottom:50,left:60},color=nv.utils.defaultColor(),width=null,height=null,showLegend=true,tooltips=true,showControls=true,rescaleY=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' at '+x+'</p>'},x,y,id=lines.id(),state={index:0,rescaleY:rescaleY},defaultState=null,noData='No Data Available.',average=function(d){return d.average},dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState');xAxis.orient('bottom').tickPadding(7);yAxis.orient('left');var dx=d3.scale.linear(),index={i:0,x:0};var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(lines.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(lines.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,null,null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this).classed('nv-chart-'+id,true),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
var indexDrag=d3.behavior.drag().on('dragstart',dragStart).on('drag',dragMove).on('dragend',dragEnd);function dragStart(d,i){d3.select(chart.container).style('cursor','ew-resize');}
function dragMove(d,i){index.x=d3.event.x;index.i=Math.round(dx.invert(index.x));updateZero();}
function dragEnd(d,i){d3.select(chart.container).style('cursor','auto');state.index=index.i;dispatch.stateChange(state);}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=lines.xScale();y=lines.yScale();if(!rescaleY){var seriesDomains=data.filter(function(series){return!series.disabled}).map(function(series,i){var initialDomain=d3.extent(series.values,lines.y());if(initialDomain[0]<-.95)initialDomain[0]=-.95;return[(initialDomain[0]-initialDomain[1])/(1+initialDomain[1]),(initialDomain[1]-initialDomain[0])/(1+initialDomain[0])];});var completeDomain=[d3.min(seriesDomains,function(d){return d[0]}),d3.max(seriesDomains,function(d){return d[1]})]
lines.yDomain(completeDomain);}else{lines.yDomain(null);}
dx.domain([0,data[0].values.length-1]).range([0,availableWidth]).clamp(true);var data=indexify(index.i,data);var wrap=container.selectAll('g.nv-wrap.nv-cumulativeLine').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-cumulativeLine').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-background');gEnter.append('g').attr('class','nv-linesWrap');gEnter.append('g').attr('class','nv-avgLinesWrap');gEnter.append('g').attr('class','nv-legendWrap');gEnter.append('g').attr('class','nv-controlsWrap');if(showLegend){legend.width(availableWidth);g.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.nv-legendWrap').attr('transform','translate(0,'+(-margin.top)+')')}
if(showControls){var controlsData=[{key:'Re-scale y-axis',disabled:!rescaleY}];controls.width(140).color(['#444','#444','#444']);g.select('.nv-controlsWrap').datum(controlsData).attr('transform','translate(0,'+(-margin.top)+')').call(controls);}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');var tempDisabled=data.filter(function(d){return d.tempDisabled});wrap.select('.tempDisabled').remove();if(tempDisabled.length){wrap.append('text').attr('class','tempDisabled').attr('x',availableWidth/2).attr('y','-.71em').style('text-anchor','end').text(tempDisabled.map(function(d){return d.key}).join(', ')+' values cannot be calculated for this time period.');}
gEnter.select('.nv-background').append('rect');g.select('.nv-background rect').attr('width',availableWidth).attr('height',availableHeight);lines.y(function(d){return d.display.y}).width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&!data[i].tempDisabled;}));var linesWrap=g.select('.nv-linesWrap').datum(data.filter(function(d){return!d.disabled&&!d.tempDisabled}));linesWrap.call(lines);data.forEach(function(d,i){d.seriesIndex=i;});var avgLineData=data.filter(function(d){return!d.disabled&&!!average(d);});var avgLines=g.select(".nv-avgLinesWrap").selectAll("line").data(avgLineData,function(d){return d.key;});avgLines.enter().append('line').style('stroke-width',2).style('stroke-dasharray','10,10').style('stroke',function(d,i){return lines.color()(d,d.seriesIndex);}).attr('x1',0).attr('x2',availableWidth).attr('y1',function(d){return y(average(d));}).attr('y2',function(d){return y(average(d));});avgLines.attr('x1',0).attr('x2',availableWidth).attr('y1',function(d){return y(average(d));}).attr('y2',function(d){return y(average(d));});avgLines.exit().remove();var indexLine=linesWrap.selectAll('.nv-indexLine').data([index]);indexLine.enter().append('rect').attr('class','nv-indexLine').attr('width',3).attr('x',-2).attr('fill','red').attr('fill-opacity',.5).call(indexDrag)
indexLine.attr('transform',function(d){return'translate('+dx(d.i)+',0)'}).attr('height',availableHeight)
xAxis.scale(x).ticks(Math.min(data[0].values.length,availableWidth/70)).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+y.range()[0]+')');d3.transition(g.select('.nv-x.nv-axis')).call(xAxis);yAxis.scale(y).ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.nv-y.nv-axis')).call(yAxis);function updateZero(){indexLine.data([index]);chart.update();}
g.select('.nv-background rect').on('click',function(){index.x=d3.mouse(this)[0];index.i=Math.round(dx.invert(index.x));state.index=index.i;dispatch.stateChange(state);updateZero();});lines.dispatch.on('elementClick',function(e){index.i=e.pointIndex;index.x=dx(index.i);state.index=index.i;dispatch.stateChange(state);updateZero();});controls.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;rescaleY=!d.disabled;state.rescaleY=rescaleY;dispatch.stateChange(state);selection.call(chart);});legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);selection.call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
if(typeof e.index!=='undefined'){index.i=e.index;index.x=dx(index.i);state.index=e.index;indexLine.data([index]);}
if(typeof e.rescaleY!=='undefined'){rescaleY=e.rescaleY;}
selection.call(chart);});});return chart;}
lines.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.lines=lines;chart.legend=legend;chart.xAxis=xAxis;chart.yAxis=yAxis;d3.rebind(chart,lines,'defined','isArea','x','y','size','xDomain','yDomain','forceX','forceY','interactive','clipEdge','clipVoronoi','id');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.rescaleY=function(_){if(!arguments.length)return rescaleY;rescaleY=_
return rescaleY;};chart.showControls=function(_){if(!arguments.length)return showControls;showControls=_;return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};chart.average=function(_){if(!arguments.length)return average;average=_;return chart;};function indexify(idx,data){return data.map(function(line,i){if(!line.values){return line;}
var v=lines.y()(line.values[idx],idx);if(v<-.95){line.tempDisabled=true;return line;}
line.tempDisabled=false;line.values=line.values.map(function(point,pointIndex){point.display={'y':(lines.y()(point,pointIndex)-v)/(1+v)};return point;})
return line;})}
return chart;}
nv.models.discreteBar=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,id=Math.floor(Math.random()*10000),x=d3.scale.ordinal(),y=d3.scale.linear(),getX=function(d){return d.x},getY=function(d){return d.y},forceY=[0],color=nv.utils.defaultColor(),showValues=false,valueFormat=d3.format(',.2f'),xDomain,yDomain,dispatch=d3.dispatch('chartClick','elementClick','elementDblClick','elementMouseover','elementMouseout'),rectClass='discreteBar';var x0,y0;function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);data=data.map(function(series,i){series.values=series.values.map(function(point){point.series=i;return point;});return series;});var seriesData=(xDomain&&yDomain)?[]:data.map(function(d){return d.values.map(function(d,i){return{x:getX(d,i),y:getY(d,i),y0:d.y0}})});x.domain(xDomain||d3.merge(seriesData).map(function(d){return d.x})).rangeBands([0,availableWidth],.1);y.domain(yDomain||d3.extent(d3.merge(seriesData).map(function(d){return d.y}).concat(forceY)));if(showValues)y.range([availableHeight-(y.domain()[0]<0?12:0),y.domain()[1]>0?12:0]);else y.range([availableHeight,0]);x0=x0||x;y0=y0||y.copy().range([y(0),y(0)]);var wrap=container.selectAll('g.nv-wrap.nv-discretebar').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-discretebar');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-groups');wrap.attr('transform','translate('+margin.left+','+margin.top+')');var groups=wrap.select('.nv-groups').selectAll('.nv-group').data(function(d){return d},function(d){return d.key});groups.enter().append('g').style('stroke-opacity',1e-6).style('fill-opacity',1e-6);d3.transition(groups.exit()).style('stroke-opacity',1e-6).style('fill-opacity',1e-6).remove();groups.attr('class',function(d,i){return'nv-group nv-series-'+i}).classed('hover',function(d){return d.hover});d3.transition(groups).style('stroke-opacity',1).style('fill-opacity',.75);var bars=groups.selectAll('g.nv-bar').data(function(d){return d.values});bars.exit().remove();var barsEnter=bars.enter().append('g').attr('transform',function(d,i,j){return'translate('+(x(getX(d,i))+x.rangeBand()*.05)+', '+y(0)+')'}).on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.elementMouseover({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(d.series+.5)/data.length),y(getY(d,i))],pointIndex:i,seriesIndex:d.series,e:d3.event});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.elementMouseout({value:getY(d,i),point:d,series:data[d.series],pointIndex:i,seriesIndex:d.series,e:d3.event});}).on('click',function(d,i){dispatch.elementClick({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(d.series+.5)/data.length),y(getY(d,i))],pointIndex:i,seriesIndex:d.series,e:d3.event});d3.event.stopPropagation();}).on('dblclick',function(d,i){dispatch.elementDblClick({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(d.series+.5)/data.length),y(getY(d,i))],pointIndex:i,seriesIndex:d.series,e:d3.event});d3.event.stopPropagation();});barsEnter.append('rect').attr('height',0).attr('width',x.rangeBand()*.9/data.length)
if(showValues){barsEnter.append('text').attr('text-anchor','middle')
bars.select('text').attr('x',x.rangeBand()*.9/2).attr('y',function(d,i){return getY(d,i)<0?y(getY(d,i))-y(0)+12:-4}).text(function(d,i){return valueFormat(getY(d,i))});}else{bars.selectAll('text').remove();}
bars.attr('class',function(d,i){return getY(d,i)<0?'nv-bar negative':'nv-bar positive'}).style('fill',function(d,i){return d.color||color(d,i)}).style('stroke',function(d,i){return d.color||color(d,i)}).select('rect').attr('class',rectClass).attr('width',x.rangeBand()*.9/data.length);d3.transition(bars).attr('transform',function(d,i){var left=x(getX(d,i))+x.rangeBand()*.05,top=getY(d,i)<0?y(0):y(0)-y(getY(d,i))<1?y(0)-1:y(getY(d,i));return'translate('+left+', '+top+')'}).select('rect').attr('height',function(d,i){return Math.max(Math.abs(y(getY(d,i))-y(0))||1)});x0=x.copy();y0=y.copy();});return chart;}
chart.dispatch=dispatch;chart.x=function(_){if(!arguments.length)return getX;getX=_;return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.forceY=function(_){if(!arguments.length)return forceY;forceY=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};chart.showValues=function(_){if(!arguments.length)return showValues;showValues=_;return chart;};chart.valueFormat=function(_){if(!arguments.length)return valueFormat;valueFormat=_;return chart;};chart.rectClass=function(_){if(!arguments.length)return rectClass;rectClass=_;return chart;}
return chart;}
nv.models.discreteBarChart=function(){var discretebar=nv.models.discreteBar(),xAxis=nv.models.axis(),yAxis=nv.models.axis();var margin={top:15,right:10,bottom:50,left:60},width=null,height=null,color=nv.utils.getColor(),staggerLabels=false,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+x+'</h3>'+'<p>'+y+'</p>'},x,y,noData="No Data Available.",dispatch=d3.dispatch('tooltipShow','tooltipHide','beforeUpdate');xAxis.orient('bottom').highlightZero(false).showMaxMin(false).tickFormat(function(d){return d});yAxis.orient('left').tickFormat(d3.format(',.1f'));var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(discretebar.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(discretebar.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'n':'s',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){dispatch.beforeUpdate();selection.transition().call(chart);};chart.container=this;if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=discretebar.xScale();y=discretebar.yScale();var wrap=container.selectAll('g.nv-wrap.nv-discreteBarWithAxes').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-discreteBarWithAxes').append('g');var defsEnter=gEnter.append('defs');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-barsWrap');g.attr('transform','translate('+margin.left+','+margin.top+')');discretebar.width(availableWidth).height(availableHeight);var barsWrap=g.select('.nv-barsWrap').datum(data.filter(function(d){return!d.disabled}))
d3.transition(barsWrap).call(discretebar);defsEnter.append('clipPath').attr('id','nv-x-label-clip-'+discretebar.id()).append('rect');g.select('#nv-x-label-clip-'+discretebar.id()+' rect').attr('width',x.rangeBand()*(staggerLabels?2:1)).attr('height',16).attr('x',-x.rangeBand()/(staggerLabels?1:2));xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+(y.range()[0]+((discretebar.showValues()&&y.domain()[0]<0)?16:0))+')');g.select('.nv-x.nv-axis').transition().duration(0).call(xAxis);var xTicks=g.select('.nv-x.nv-axis').selectAll('g');if(staggerLabels){xTicks.selectAll('text').attr('transform',function(d,i,j){return'translate(0,'+(j%2==0?'5':'17')+')'})}
yAxis.scale(y).ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.nv-y.nv-axis')).call(yAxis);dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});});return chart;}
discretebar.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});discretebar.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.discretebar=discretebar;chart.xAxis=xAxis;chart.yAxis=yAxis;d3.rebind(chart,discretebar,'x','y','xDomain','yDomain','forceX','forceY','id','showValues','valueFormat');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);discretebar.color(color);return chart;};chart.staggerLabels=function(_){if(!arguments.length)return staggerLabels;staggerLabels=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.distribution=function(){var margin={top:0,right:0,bottom:0,left:0},width=400,size=8,axis='x',getData=function(d){return d[axis]},color=nv.utils.defaultColor(),scale=d3.scale.linear(),domain;var scale0;function chart(selection){selection.each(function(data){var availableLength=width-(axis==='x'?margin.left+margin.right:margin.top+margin.bottom),naxis=axis=='x'?'y':'x',container=d3.select(this);scale0=scale0||scale;var wrap=container.selectAll('g.nv-distribution').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-distribution');var gEnter=wrapEnter.append('g');var g=wrap.select('g');wrap.attr('transform','translate('+margin.left+','+margin.top+')')
var distWrap=g.selectAll('g.nv-dist').data(function(d){return d},function(d){return d.key});distWrap.enter().append('g');distWrap.attr('class',function(d,i){return'nv-dist nv-series-'+i}).style('stroke',function(d,i){return color(d,i)});var dist=distWrap.selectAll('line.nv-dist'+axis).data(function(d){return d.values})
dist.enter().append('line').attr(axis+'1',function(d,i){return scale0(getData(d,i))}).attr(axis+'2',function(d,i){return scale0(getData(d,i))})
d3.transition(distWrap.exit().selectAll('line.nv-dist'+axis)).attr(axis+'1',function(d,i){return scale(getData(d,i))}).attr(axis+'2',function(d,i){return scale(getData(d,i))}).style('stroke-opacity',0).remove();dist.attr('class',function(d,i){return'nv-dist'+axis+' nv-dist'+axis+'-'+i}).attr(naxis+'1',0).attr(naxis+'2',size);d3.transition(dist).attr(axis+'1',function(d,i){return scale(getData(d,i))}).attr(axis+'2',function(d,i){return scale(getData(d,i))})
scale0=scale.copy();});return chart;}
chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.axis=function(_){if(!arguments.length)return axis;axis=_;return chart;};chart.size=function(_){if(!arguments.length)return size;size=_;return chart;};chart.getData=function(_){if(!arguments.length)return getData;getData=d3.functor(_);return chart;};chart.scale=function(_){if(!arguments.length)return scale;scale=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};return chart;}
nv.models.indentedTree=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,color=nv.utils.defaultColor(),id=Math.floor(Math.random()*10000),header=true,filterZero=false,noData="No Data Available.",childIndent=20,columns=[{key:'key',label:'Name',type:'text'}],tableClass=null,iconOpen='images/grey-plus.png',iconClose='images/grey-minus.png',dispatch=d3.dispatch('elementClick','elementDblclick','elementMouseover','elementMouseout');var idx=0;function chart(selection){selection.each(function(data){var depth=1,container=d3.select(this);var tree=d3.layout.tree().children(function(d){return d.values}).size([height,childIndent]);chart.update=function(){container.transition().duration(600).call(chart)};if(!data[0])data[0]={key:noData};var nodes=tree.nodes(data[0]);var wrap=d3.select(this).selectAll('div').data([[nodes]]);var wrapEnter=wrap.enter().append('div').attr('class','nvd3 nv-wrap nv-indentedtree');var tableEnter=wrapEnter.append('table');var table=wrap.select('table').attr('width','100%').attr('class',tableClass);if(header){var thead=tableEnter.append('thead');var theadRow1=thead.append('tr');columns.forEach(function(column){theadRow1.append('th').attr('width',column.width?column.width:'10%').style('text-align',column.type=='numeric'?'right':'left').append('span').text(column.label);});}
var tbody=table.selectAll('tbody').data(function(d){return d});tbody.enter().append('tbody');depth=d3.max(nodes,function(node){return node.depth});tree.size([height,depth*childIndent]);var node=tbody.selectAll('tr').data(function(d){return d.filter(function(d){return(filterZero&&!d.children)?filterZero(d):true;})},function(d,i){return d.id||(d.id||++idx)});node.exit().remove();node.select('img.nv-treeicon').attr('src',icon).classed('folded',folded);var nodeEnter=node.enter().append('tr');columns.forEach(function(column,index){var nodeName=nodeEnter.append('td').style('padding-left',function(d){return(index?0:d.depth*childIndent+12+(icon(d)?0:16))+'px'},'important').style('text-align',column.type=='numeric'?'right':'left');if(index==0){nodeName.append('img').classed('nv-treeicon',true).classed('nv-folded',folded).attr('src',icon).style('width','14px').style('height','14px').style('padding','0 1px').style('display',function(d){return icon(d)?'inline-block':'none';}).on('click',click);}
nodeName.append('span').attr('class',d3.functor(column.classes)).text(function(d){return column.format?column.format(d):(d[column.key]||'-')});if(column.showCount){nodeName.append('span').attr('class','nv-childrenCount');node.selectAll('span.nv-childrenCount').text(function(d){return((d.values&&d.values.length)||(d._values&&d._values.length))?'('+((d.values&&(d.values.filter(function(d){return filterZero?filterZero(d):true;}).length))||(d._values&&d._values.filter(function(d){return filterZero?filterZero(d):true;}).length)||0)+')':''});}
if(column.click)
nodeName.select('span').on('click',column.click);});node.order().on('click',function(d){dispatch.elementClick({row:this,data:d,pos:[d.x,d.y]});}).on('dblclick',function(d){dispatch.elementDblclick({row:this,data:d,pos:[d.x,d.y]});}).on('mouseover',function(d){dispatch.elementMouseover({row:this,data:d,pos:[d.x,d.y]});}).on('mouseout',function(d){dispatch.elementMouseout({row:this,data:d,pos:[d.x,d.y]});});function click(d,_,unshift){d3.event.stopPropagation();if(d3.event.shiftKey&&!unshift){d3.event.shiftKey=false;d.values&&d.values.forEach(function(node){if(node.values||node._values){click(node,0,true);}});return true;}
if(!hasChildren(d)){return true;}
if(d.values){d._values=d.values;d.values=null;}else{d.values=d._values;d._values=null;}
chart.update();}
function icon(d){return(d._values&&d._values.length)?iconOpen:(d.values&&d.values.length)?iconClose:'';}
function folded(d){return(d._values&&d._values.length);}
function hasChildren(d){var values=d.values||d._values;return(values&&values.length);}});return chart;}
chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);scatter.color(color);return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};chart.header=function(_){if(!arguments.length)return header;header=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};chart.filterZero=function(_){if(!arguments.length)return filterZero;filterZero=_;return chart;};chart.columns=function(_){if(!arguments.length)return columns;columns=_;return chart;};chart.tableClass=function(_){if(!arguments.length)return tableClass;tableClass=_;return chart;};chart.iconOpen=function(_){if(!arguments.length)return iconOpen;iconOpen=_;return chart;}
chart.iconClose=function(_){if(!arguments.length)return iconClose;iconClose=_;return chart;}
return chart;};nv.models.legend=function(){var margin={top:5,right:0,bottom:5,left:0},width=400,height=20,getKey=function(d){return d.key},color=nv.utils.defaultColor(),align=true,dispatch=d3.dispatch('legendClick','legendDblclick','legendMouseover','legendMouseout');function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,container=d3.select(this);var wrap=container.selectAll('g.nv-legend').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-legend').append('g');var g=wrap.select('g');wrap.attr('transform','translate('+margin.left+','+margin.top+')');var series=g.selectAll('.nv-series').data(function(d){return d});var seriesEnter=series.enter().append('g').attr('class','nv-series').on('mouseover',function(d,i){dispatch.legendMouseover(d,i);}).on('mouseout',function(d,i){dispatch.legendMouseout(d,i);}).on('click',function(d,i){dispatch.legendClick(d,i);}).on('dblclick',function(d,i){dispatch.legendDblclick(d,i);});seriesEnter.append('circle').style('stroke-width',2).attr('r',5);seriesEnter.append('text').attr('text-anchor','start').attr('dy','.32em').attr('dx','8');series.classed('disabled',function(d){return d.disabled});series.exit().remove();series.select('circle').style('fill',function(d,i){return d.color||color(d,i)}).style('stroke',function(d,i){return d.color||color(d,i)});series.select('text').text(getKey);if(align){var seriesWidths=[];series.each(function(d,i){var legendText=d3.select(this).select('text');var svgComputedTextLength=legendText.node().getComputedTextLength()||nv.utils.calcApproxTextWidth(legendText);seriesWidths.push(svgComputedTextLength+28);});var seriesPerRow=0;var legendWidth=0;var columnWidths=[];while(legendWidth<availableWidth&&seriesPerRow<seriesWidths.length){columnWidths[seriesPerRow]=seriesWidths[seriesPerRow];legendWidth+=seriesWidths[seriesPerRow++];}
while(legendWidth>availableWidth&&seriesPerRow>1){columnWidths=[];seriesPerRow--;for(k=0;k<seriesWidths.length;k++){if(seriesWidths[k]>(columnWidths[k%seriesPerRow]||0))
columnWidths[k%seriesPerRow]=seriesWidths[k];}
legendWidth=columnWidths.reduce(function(prev,cur,index,array){return prev+cur;});}
var xPositions=[];for(var i=0,curX=0;i<seriesPerRow;i++){xPositions[i]=curX;curX+=columnWidths[i];}
series.attr('transform',function(d,i){return'translate('+xPositions[i%seriesPerRow]+','+(5+Math.floor(i/seriesPerRow)*20)+')';});g.attr('transform','translate('+(width-margin.right-legendWidth)+','+margin.top+')');height=margin.top+margin.bottom+(Math.ceil(seriesWidths.length/seriesPerRow)*20);}else{var ypos=5,newxpos=5,maxwidth=0,xpos;series.attr('transform',function(d,i){var length=d3.select(this).select('text').node().getComputedTextLength()+28;xpos=newxpos;if(width<margin.left+margin.right+xpos+length){newxpos=xpos=5;ypos+=20;}
newxpos+=length;if(newxpos>maxwidth)maxwidth=newxpos;return'translate('+xpos+','+ypos+')';});g.attr('transform','translate('+(width-margin.right-maxwidth)+','+margin.top+')');height=margin.top+margin.bottom+ypos+15;}});return chart;}
chart.dispatch=dispatch;chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.key=function(_){if(!arguments.length)return getKey;getKey=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.align=function(_){if(!arguments.length)return align;align=_;return chart;};return chart;}
nv.models.line=function(){var scatter=nv.models.scatter();var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,color=nv.utils.defaultColor(),getX=function(d){return d.x},getY=function(d){return d.y},defined=function(d,i){return!isNaN(getY(d,i))&&getY(d,i)!==null},isArea=function(d){return d.area},clipEdge=false,x,y,interpolate="linear";scatter.size(16).sizeDomain([16,256]);var x0,y0;function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);x=scatter.xScale();y=scatter.yScale();x0=x0||x;y0=y0||y;var wrap=container.selectAll('g.nv-wrap.nv-line').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-line');var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g')
gEnter.append('g').attr('class','nv-groups');gEnter.append('g').attr('class','nv-scatterWrap');wrap.attr('transform','translate('+margin.left+','+margin.top+')');scatter.width(availableWidth).height(availableHeight)
var scatterWrap=wrap.select('.nv-scatterWrap');d3.transition(scatterWrap).call(scatter);defsEnter.append('clipPath').attr('id','nv-edge-clip-'+scatter.id()).append('rect');wrap.select('#nv-edge-clip-'+scatter.id()+' rect').attr('width',availableWidth).attr('height',availableHeight);g.attr('clip-path',clipEdge?'url(#nv-edge-clip-'+scatter.id()+')':'');scatterWrap.attr('clip-path',clipEdge?'url(#nv-edge-clip-'+scatter.id()+')':'');var groups=wrap.select('.nv-groups').selectAll('.nv-group').data(function(d){return d},function(d){return d.key});groups.enter().append('g').style('stroke-opacity',1e-6).style('fill-opacity',1e-6);d3.transition(groups.exit()).style('stroke-opacity',1e-6).style('fill-opacity',1e-6).remove();groups.attr('class',function(d,i){return'nv-group nv-series-'+i}).classed('hover',function(d){return d.hover}).style('fill',function(d,i){return color(d,i)}).style('stroke',function(d,i){return color(d,i)});d3.transition(groups).style('stroke-opacity',1).style('fill-opacity',.5);var areaPaths=groups.selectAll('path.nv-area').data(function(d){return isArea(d)?[d]:[]});areaPaths.enter().append('path').attr('class','nv-area').attr('d',function(d){return d3.svg.area().interpolate(interpolate).defined(defined).x(function(d,i){return x0(getX(d,i))}).y0(function(d,i){return y0(getY(d,i))}).y1(function(d,i){return y0(y.domain()[0]<=0?y.domain()[1]>=0?0:y.domain()[1]:y.domain()[0])}).apply(this,[d.values])});d3.transition(groups.exit().selectAll('path.nv-area')).attr('d',function(d){return d3.svg.area().interpolate(interpolate).defined(defined).x(function(d,i){return x0(getX(d,i))}).y0(function(d,i){return y0(getY(d,i))}).y1(function(d,i){return y0(y.domain()[0]<=0?y.domain()[1]>=0?0:y.domain()[1]:y.domain()[0])}).apply(this,[d.values])});d3.transition(areaPaths).attr('d',function(d){return d3.svg.area().interpolate(interpolate).defined(defined).x(function(d,i){return x0(getX(d,i))}).y0(function(d,i){return y0(getY(d,i))}).y1(function(d,i){return y0(y.domain()[0]<=0?y.domain()[1]>=0?0:y.domain()[1]:y.domain()[0])}).apply(this,[d.values])});var linePaths=groups.selectAll('path.nv-line').data(function(d){return[d.values]});linePaths.enter().append('path').attr('class','nv-line').attr('d',d3.svg.line().interpolate(interpolate).defined(defined).x(function(d,i){return x0(getX(d,i))}).y(function(d,i){return y0(getY(d,i))}));d3.transition(groups.exit().selectAll('path.nv-line')).attr('d',d3.svg.line().interpolate(interpolate).defined(defined).x(function(d,i){return x(getX(d,i))}).y(function(d,i){return y(getY(d,i))}));d3.transition(linePaths).attr('d',d3.svg.line().interpolate(interpolate).defined(defined).x(function(d,i){return x(getX(d,i))}).y(function(d,i){return y(getY(d,i))}));x0=x.copy();y0=y.copy();});return chart;}
chart.dispatch=scatter.dispatch;chart.scatter=scatter;d3.rebind(chart,scatter,'id','interactive','size','xScale','yScale','zScale','xDomain','yDomain','sizeDomain','forceX','forceY','forceSize','clipVoronoi','clipRadius','padData');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.x=function(_){if(!arguments.length)return getX;getX=_;scatter.x(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;scatter.y(_);return chart;};chart.clipEdge=function(_){if(!arguments.length)return clipEdge;clipEdge=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);scatter.color(color);return chart;};chart.interpolate=function(_){if(!arguments.length)return interpolate;interpolate=_;return chart;};chart.defined=function(_){if(!arguments.length)return defined;defined=_;return chart;};chart.isArea=function(_){if(!arguments.length)return isArea;isArea=d3.functor(_);return chart;};return chart;}
nv.models.lineChart=function(){var lines=nv.models.line(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend();var margin={top:30,right:20,bottom:50,left:60},color=nv.utils.defaultColor(),width=null,height=null,showLegend=true,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' at '+x+'</p>'},x,y,state={},defaultState=null,noData='No Data Available.',dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState');xAxis.orient('bottom').tickPadding(7);yAxis.orient('left');var showTooltip=function(e,offsetElement){if(offsetElement){var svg=d3.select(offsetElement).select('svg');var viewBox=(svg.node())?svg.attr('viewBox'):null;if(viewBox){viewBox=viewBox.split(' ');var ratio=parseInt(svg.style('width'))/viewBox[2];e.pos[0]=e.pos[0]*ratio;e.pos[1]=e.pos[1]*ratio;}}
var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(lines.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(lines.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,null,null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=lines.xScale();y=lines.yScale();var wrap=container.selectAll('g.nv-wrap.nv-lineChart').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-lineChart').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-linesWrap');gEnter.append('g').attr('class','nv-legendWrap');if(showLegend){legend.width(availableWidth);g.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
wrap.select('.nv-legendWrap').attr('transform','translate(0,'+(-margin.top)+')')}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');lines.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}));var linesWrap=g.select('.nv-linesWrap').datum(data.filter(function(d){return!d.disabled}))
d3.transition(linesWrap).call(lines);xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+y.range()[0]+')');d3.transition(g.select('.nv-x.nv-axis')).call(xAxis);yAxis.scale(y).ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.nv-y.nv-axis')).call(yAxis);legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);selection.transition().call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
selection.call(chart);});});return chart;}
lines.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.lines=lines;chart.legend=legend;chart.xAxis=xAxis;chart.yAxis=yAxis;d3.rebind(chart,lines,'defined','isArea','x','y','size','xScale','yScale','xDomain','yDomain','forceX','forceY','interactive','clipEdge','clipVoronoi','id','interpolate');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.linePlusBarChart=function(){var lines=nv.models.line(),bars=nv.models.historicalBar(),xAxis=nv.models.axis(),y1Axis=nv.models.axis(),y2Axis=nv.models.axis(),legend=nv.models.legend();var margin={top:30,right:60,bottom:50,left:60},width=null,height=null,getX=function(d){return d.x},getY=function(d){return d.y},color=nv.utils.defaultColor(),showLegend=true,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' at '+x+'</p>';},x,y1,y2,state={},defaultState=null,noData="No Data Available.",dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState');bars.padData(true);lines.clipEdge(false).padData(true);xAxis.orient('bottom').tickPadding(7).highlightZero(false);y1Axis.orient('left');y2Axis.orient('right');var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(lines.x()(e.point,e.pointIndex)),y=(e.series.bar?y1Axis:y2Axis).tickFormat()(lines.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'n':'s',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
var dataBars=data.filter(function(d){return!d.disabled&&d.bar});var dataLines=data.filter(function(d){return!d.bar});x=dataLines.filter(function(d){return!d.disabled;}).length&&dataLines.filter(function(d){return!d.disabled;})[0].values.length?lines.xScale():bars.xScale();y1=bars.yScale();y2=lines.yScale();var wrap=d3.select(this).selectAll('g.nv-wrap.nv-linePlusBar').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-linePlusBar').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y1 nv-axis');gEnter.append('g').attr('class','nv-y2 nv-axis');gEnter.append('g').attr('class','nv-barsWrap');gEnter.append('g').attr('class','nv-linesWrap');gEnter.append('g').attr('class','nv-legendWrap');if(showLegend){legend.width(availableWidth/2);g.select('.nv-legendWrap').datum(data.map(function(series){series.originalKey=series.originalKey===undefined?series.key:series.originalKey;series.key=series.originalKey+(series.bar?' (left axis)':' (right axis)');return series;})).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.nv-legendWrap').attr('transform','translate('+(availableWidth/2)+','+(-margin.top)+')');}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');lines.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&!data[i].bar}))
bars.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&data[i].bar}))
var barsWrap=g.select('.nv-barsWrap').datum(dataBars.length?dataBars:[{values:[]}])
var linesWrap=g.select('.nv-linesWrap').datum(dataLines[0]&&!dataLines[0].disabled?dataLines:[{values:[]}]);d3.transition(barsWrap).call(bars);d3.transition(linesWrap).call(lines);xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+y1.range()[0]+')');d3.transition(g.select('.nv-x.nv-axis')).call(xAxis);y1Axis.scale(y1).ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.nv-y1.nv-axis')).style('opacity',dataBars.length?1:0).call(y1Axis);y2Axis.scale(y2).ticks(availableHeight/36).tickSize(dataBars.length?0:-availableWidth,0);g.select('.nv-y2.nv-axis').style('opacity',dataLines.length?1:0).attr('transform','translate('+availableWidth+',0)');d3.transition(g.select('.nv-y2.nv-axis')).call(y2Axis);legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);selection.transition().call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
selection.call(chart);});});return chart;}
lines.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});bars.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});bars.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.legend=legend;chart.lines=lines;chart.bars=bars;chart.xAxis=xAxis;chart.y1Axis=y1Axis;chart.y2Axis=y2Axis;d3.rebind(chart,lines,'defined','size','clipVoronoi','interpolate');chart.x=function(_){if(!arguments.length)return getX;getX=_;lines.x(_);bars.x(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;lines.y(_);bars.y(_);return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.lineWithFocusChart=function(){var lines=nv.models.line(),lines2=nv.models.line(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),x2Axis=nv.models.axis(),y2Axis=nv.models.axis(),legend=nv.models.legend(),brush=d3.svg.brush();var margin={top:30,right:30,bottom:30,left:60},margin2={top:0,right:30,bottom:20,left:60},color=nv.utils.defaultColor(),width=null,height=null,height2=100,x,y,x2,y2,showLegend=true,brushExtent=null,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' at '+x+'</p>'},noData="No Data Available.",dispatch=d3.dispatch('tooltipShow','tooltipHide','brush');lines.clipEdge(true);lines2.interactive(false);xAxis.orient('bottom').tickPadding(5);yAxis.orient('left');x2Axis.orient('bottom').tickPadding(5);y2Axis.orient('left');var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(lines.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(lines.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,null,null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight1=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom-height2,availableHeight2=height2-margin2.top-margin2.bottom;chart.update=function(){chart(selection)};chart.container=this;if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight1/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=lines.xScale();y=lines.yScale();x2=lines2.xScale();y2=lines2.yScale();var wrap=container.selectAll('g.nv-wrap.nv-lineWithFocusChart').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-lineWithFocusChart').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-legendWrap');var focusEnter=gEnter.append('g').attr('class','nv-focus');focusEnter.append('g').attr('class','nv-x nv-axis');focusEnter.append('g').attr('class','nv-y nv-axis');focusEnter.append('g').attr('class','nv-linesWrap');var contextEnter=gEnter.append('g').attr('class','nv-context');contextEnter.append('g').attr('class','nv-x nv-axis');contextEnter.append('g').attr('class','nv-y nv-axis');contextEnter.append('g').attr('class','nv-linesWrap');contextEnter.append('g').attr('class','nv-brushBackground');contextEnter.append('g').attr('class','nv-x nv-brush');if(showLegend){legend.width(availableWidth);g.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight1=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom-height2;}
g.select('.nv-legendWrap').attr('transform','translate(0,'+(-margin.top)+')')}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');lines.width(availableWidth).height(availableHeight1).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled;}));lines2.defined(lines.defined()).width(availableWidth).height(availableHeight2).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled;}));g.select('.nv-context').attr('transform','translate(0,'+(availableHeight1+margin.bottom+margin2.top)+')')
var contextLinesWrap=g.select('.nv-context .nv-linesWrap').datum(data.filter(function(d){return!d.disabled}))
d3.transition(contextLinesWrap).call(lines2);xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight1,0);yAxis.scale(y).ticks(availableHeight1/36).tickSize(-availableWidth,0);g.select('.nv-focus .nv-x.nv-axis').attr('transform','translate(0,'+availableHeight1+')');brush.x(x2).on('brush',onBrush);if(brushExtent)brush.extent(brushExtent);var brushBG=g.select('.nv-brushBackground').selectAll('g').data([brushExtent||brush.extent()])
var brushBGenter=brushBG.enter().append('g');brushBGenter.append('rect').attr('class','left').attr('x',0).attr('y',0).attr('height',availableHeight2);brushBGenter.append('rect').attr('class','right').attr('x',0).attr('y',0).attr('height',availableHeight2);gBrush=g.select('.nv-x.nv-brush').call(brush);gBrush.selectAll('rect').attr('height',availableHeight2);gBrush.selectAll('.resize').append('path').attr('d',resizePath);onBrush();x2Axis.scale(x2).ticks(availableWidth/100).tickSize(-availableHeight2,0);g.select('.nv-context .nv-x.nv-axis').attr('transform','translate(0,'+y2.range()[0]+')');d3.transition(g.select('.nv-context .nv-x.nv-axis')).call(x2Axis);y2Axis.scale(y2).ticks(availableHeight2/36).tickSize(-availableWidth,0);d3.transition(g.select('.nv-context .nv-y.nv-axis')).call(y2Axis);g.select('.nv-context .nv-x.nv-axis').attr('transform','translate(0,'+y2.range()[0]+')');legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
selection.transition().call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});function resizePath(d){var e=+(d=='e'),x=e?1:-1,y=availableHeight2/3;return'M'+(.5*x)+','+y
+'A6,6 0 0 '+e+' '+(6.5*x)+','+(y+6)
+'V'+(2*y-6)
+'A6,6 0 0 '+e+' '+(.5*x)+','+(2*y)
+'Z'
+'M'+(2.5*x)+','+(y+8)
+'V'+(2*y-8)
+'M'+(4.5*x)+','+(y+8)
+'V'+(2*y-8);}
function updateBrushBG(){if(!brush.empty())brush.extent(brushExtent);brushBG.data([brush.empty()?x2.domain():brushExtent]).each(function(d,i){var leftWidth=x2(d[0])-x.range()[0],rightWidth=x.range()[1]-x2(d[1]);d3.select(this).select('.left').attr('width',leftWidth<0?0:leftWidth);d3.select(this).select('.right').attr('x',x2(d[1])).attr('width',rightWidth<0?0:rightWidth);});}
function onBrush(){brushExtent=brush.empty()?null:brush.extent();extent=brush.empty()?x2.domain():brush.extent();dispatch.brush({extent:extent,brush:brush});updateBrushBG();var focusLinesWrap=g.select('.nv-focus .nv-linesWrap').datum(data.filter(function(d){return!d.disabled}).map(function(d,i){return{key:d.key,values:d.values.filter(function(d,i){return lines.x()(d,i)>=extent[0]&&lines.x()(d,i)<=extent[1];})}}));d3.transition(focusLinesWrap).call(lines);d3.transition(g.select('.nv-focus .nv-x.nv-axis')).call(xAxis);d3.transition(g.select('.nv-focus .nv-y.nv-axis')).call(yAxis);}});return chart;}
lines.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.legend=legend;chart.lines=lines;chart.lines2=lines2;chart.xAxis=xAxis;chart.yAxis=yAxis;chart.x2Axis=x2Axis;chart.y2Axis=y2Axis;d3.rebind(chart,lines,'defined','isArea','size','xDomain','yDomain','forceX','forceY','interactive','clipEdge','clipVoronoi','id');chart.x=function(_){if(!arguments.length)return lines.x;lines.x(_);lines2.x(_);return chart;};chart.y=function(_){if(!arguments.length)return lines.y;lines.y(_);lines2.y(_);return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.margin2=function(_){if(!arguments.length)return margin2;margin2=_;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.height2=function(_){if(!arguments.length)return height2;height2=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.interpolate=function(_){if(!arguments.length)return lines.interpolate();lines.interpolate(_);lines2.interpolate(_);return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};chart.xTickFormat=function(_){if(!arguments.length)return xAxis.tickFormat();xAxis.tickFormat(_);x2Axis.tickFormat(_);return chart;};chart.yTickFormat=function(_){if(!arguments.length)return yAxis.tickFormat();yAxis.tickFormat(_);y2Axis.tickFormat(_);return chart;};return chart;}
nv.models.linePlusBarWithFocusChart=function(){var lines=nv.models.line(),lines2=nv.models.line(),bars=nv.models.historicalBar(),bars2=nv.models.historicalBar(),xAxis=nv.models.axis(),x2Axis=nv.models.axis(),y1Axis=nv.models.axis(),y2Axis=nv.models.axis(),y3Axis=nv.models.axis(),y4Axis=nv.models.axis(),legend=nv.models.legend(),brush=d3.svg.brush();var margin={top:30,right:30,bottom:30,left:60},margin2={top:0,right:30,bottom:20,left:60},width=null,height=null,height2=100,getX=function(d){return d.x},getY=function(d){return d.y},color=nv.utils.defaultColor(),showLegend=true,extent,brushExtent=null,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' at '+x+'</p>';},x,x2,y1,y2,y3,y4,noData="No Data Available.",dispatch=d3.dispatch('tooltipShow','tooltipHide','brush');lines.clipEdge(true);lines2.interactive(false);xAxis.orient('bottom').tickPadding(5);y1Axis.orient('left');y2Axis.orient('right');x2Axis.orient('bottom').tickPadding(5);y3Axis.orient('left');y4Axis.orient('right');var showTooltip=function(e,offsetElement){if(extent){e.pointIndex+=Math.ceil(extent[0]);}
var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(lines.x()(e.point,e.pointIndex)),y=(e.series.bar?y1Axis:y2Axis).tickFormat()(lines.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'n':'s',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight1=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom-height2,availableHeight2=height2-margin2.top-margin2.bottom;chart.update=function(){chart(selection)};chart.container=this;if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight1/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
var dataBars=data.filter(function(d){return!d.disabled&&d.bar});var dataLines=data.filter(function(d){return!d.bar});x=bars.xScale();x2=x2Axis.scale();y1=bars.yScale();y2=lines.yScale();y3=bars2.yScale();y4=lines2.yScale();var series1=data.filter(function(d){return!d.disabled&&d.bar}).map(function(d){return d.values.map(function(d,i){return{x:getX(d,i),y:getY(d,i)}})});var series2=data.filter(function(d){return!d.disabled&&!d.bar}).map(function(d){return d.values.map(function(d,i){return{x:getX(d,i),y:getY(d,i)}})});x.range([0,availableWidth]);x2.domain(d3.extent(d3.merge(series1.concat(series2)),function(d){return d.x})).range([0,availableWidth]);var wrap=container.selectAll('g.nv-wrap.nv-linePlusBar').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-linePlusBar').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-legendWrap');var focusEnter=gEnter.append('g').attr('class','nv-focus');focusEnter.append('g').attr('class','nv-x nv-axis');focusEnter.append('g').attr('class','nv-y1 nv-axis');focusEnter.append('g').attr('class','nv-y2 nv-axis');focusEnter.append('g').attr('class','nv-barsWrap');focusEnter.append('g').attr('class','nv-linesWrap');var contextEnter=gEnter.append('g').attr('class','nv-context');contextEnter.append('g').attr('class','nv-x nv-axis');contextEnter.append('g').attr('class','nv-y1 nv-axis');contextEnter.append('g').attr('class','nv-y2 nv-axis');contextEnter.append('g').attr('class','nv-barsWrap');contextEnter.append('g').attr('class','nv-linesWrap');contextEnter.append('g').attr('class','nv-brushBackground');contextEnter.append('g').attr('class','nv-x nv-brush');if(showLegend){legend.width(availableWidth/2);g.select('.nv-legendWrap').datum(data.map(function(series){series.originalKey=series.originalKey===undefined?series.key:series.originalKey;series.key=series.originalKey+(series.bar?' (left axis)':' (right axis)');return series;})).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight1=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom-height2;}
g.select('.nv-legendWrap').attr('transform','translate('+(availableWidth/2)+','+(-margin.top)+')');}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');bars2.width(availableWidth).height(availableHeight2).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&data[i].bar}));lines2.width(availableWidth).height(availableHeight2).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&!data[i].bar}));var bars2Wrap=g.select('.nv-context .nv-barsWrap').datum(dataBars.length?dataBars:[{values:[]}]);var lines2Wrap=g.select('.nv-context .nv-linesWrap').datum(!dataLines[0].disabled?dataLines:[{values:[]}]);g.select('.nv-context').attr('transform','translate(0,'+(availableHeight1+margin.bottom+margin2.top)+')')
d3.transition(bars2Wrap).call(bars2);d3.transition(lines2Wrap).call(lines2);brush.x(x2).on('brush',onBrush);if(brushExtent)brush.extent(brushExtent);var brushBG=g.select('.nv-brushBackground').selectAll('g').data([brushExtent||brush.extent()])
var brushBGenter=brushBG.enter().append('g');brushBGenter.append('rect').attr('class','left').attr('x',0).attr('y',0).attr('height',availableHeight2);brushBGenter.append('rect').attr('class','right').attr('x',0).attr('y',0).attr('height',availableHeight2);var gBrush=g.select('.nv-x.nv-brush').call(brush);gBrush.selectAll('rect').attr('height',availableHeight2);gBrush.selectAll('.resize').append('path').attr('d',resizePath);x2Axis.ticks(availableWidth/100).tickSize(-availableHeight2,0);g.select('.nv-context .nv-x.nv-axis').attr('transform','translate(0,'+y3.range()[0]+')');d3.transition(g.select('.nv-context .nv-x.nv-axis')).call(x2Axis);y3Axis.scale(y3).ticks(availableHeight2/36).tickSize(-availableWidth,0);g.select('.nv-context .nv-y1.nv-axis').style('opacity',dataBars.length?1:0).attr('transform','translate(0,'+x2.range()[0]+')');d3.transition(g.select('.nv-context .nv-y1.nv-axis')).call(y3Axis);y4Axis.scale(y4).ticks(availableHeight2/36).tickSize(dataBars.length?0:-availableWidth,0);g.select('.nv-context .nv-y2.nv-axis').style('opacity',dataLines.length?1:0).attr('transform','translate('+x2.range()[1]+',0)');d3.transition(g.select('.nv-context .nv-y2.nv-axis')).call(y4Axis);legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
selection.call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});function resizePath(d){var e=+(d=='e'),x=e?1:-1,y=availableHeight2/3;return'M'+(.5*x)+','+y
+'A6,6 0 0 '+e+' '+(6.5*x)+','+(y+6)
+'V'+(2*y-6)
+'A6,6 0 0 '+e+' '+(.5*x)+','+(2*y)
+'Z'
+'M'+(2.5*x)+','+(y+8)
+'V'+(2*y-8)
+'M'+(4.5*x)+','+(y+8)
+'V'+(2*y-8);}
function updateBrushBG(){if(!brush.empty())brush.extent(brushExtent);brushBG.data([brush.empty()?x2.domain():brushExtent]).each(function(d,i){var leftWidth=x2(d[0])-x2.range()[0],rightWidth=x2.range()[1]-x2(d[1]);d3.select(this).select('.left').attr('width',leftWidth<0?0:leftWidth);d3.select(this).select('.right').attr('x',x2(d[1])).attr('width',rightWidth<0?0:rightWidth);});}
function onBrush(){brushExtent=brush.empty()?null:brush.extent();extent=brush.empty()?x2.domain():brush.extent();dispatch.brush({extent:extent,brush:brush});updateBrushBG();bars.width(availableWidth).height(availableHeight1).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&data[i].bar}));lines.width(availableWidth).height(availableHeight1).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled&&!data[i].bar}));var focusBarsWrap=g.select('.nv-focus .nv-barsWrap').datum(!dataBars.length?[{values:[]}]:dataBars.map(function(d,i){return{key:d.key,values:d.values.filter(function(d,i){return bars.x()(d,i)>=extent[0]&&bars.x()(d,i)<=extent[1];})}}));var focusLinesWrap=g.select('.nv-focus .nv-linesWrap').datum(dataLines[0].disabled?[{values:[]}]:dataLines.map(function(d,i){return{key:d.key,values:d.values.filter(function(d,i){return lines.x()(d,i)>=extent[0]&&lines.x()(d,i)<=extent[1];})}}));if(dataBars.length){x=bars.xScale();}else{x=lines.xScale();}
xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight1,0);xAxis.domain([Math.ceil(extent[0]),Math.floor(extent[1])]);d3.transition(g.select('.nv-x.nv-axis')).call(xAxis);d3.transition(focusBarsWrap).call(bars);d3.transition(focusLinesWrap).call(lines);g.select('.nv-focus .nv-x.nv-axis').attr('transform','translate(0,'+y1.range()[0]+')');y1Axis.scale(y1).ticks(availableHeight1/36).tickSize(-availableWidth,0);g.select('.nv-focus .nv-y1.nv-axis').style('opacity',dataBars.length?1:0);y2Axis.scale(y2).ticks(availableHeight1/36).tickSize(dataBars.length?0:-availableWidth,0);g.select('.nv-focus .nv-y2.nv-axis').style('opacity',dataLines.length?1:0).attr('transform','translate('+x.range()[1]+',0)');d3.transition(g.select('.nv-focus .nv-y1.nv-axis')).call(y1Axis);d3.transition(g.select('.nv-focus .nv-y2.nv-axis')).call(y2Axis);}
onBrush();});return chart;}
lines.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});bars.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});bars.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.legend=legend;chart.lines=lines;chart.lines2=lines2;chart.bars=bars;chart.bars2=bars2;chart.xAxis=xAxis;chart.x2Axis=x2Axis;chart.y1Axis=y1Axis;chart.y2Axis=y2Axis;chart.y3Axis=y3Axis;chart.y4Axis=y4Axis;d3.rebind(chart,lines,'defined','size','clipVoronoi','interpolate');chart.x=function(_){if(!arguments.length)return getX;getX=_;lines.x(_);bars.x(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;lines.y(_);bars.y(_);return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};chart.brushExtent=function(_){if(!arguments.length)return brushExtent;brushExtent=_;return chart;};return chart;}
nv.models.multiBar=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,x=d3.scale.ordinal(),y=d3.scale.linear(),id=Math.floor(Math.random()*10000),getX=function(d){return d.x},getY=function(d){return d.y},forceY=[0],clipEdge=true,stacked=false,color=nv.utils.defaultColor(),hideable=false,barColor=null,disabled,delay=1200,xDomain,yDomain,dispatch=d3.dispatch('chartClick','elementClick','elementDblClick','elementMouseover','elementMouseout');var x0,y0;function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);if(hideable&&data.length)hideable=[{values:data[0].values.map(function(d){return{x:d.x,y:0,series:d.series,size:0.01};})}];if(stacked)
data=d3.layout.stack().offset('zero').values(function(d){return d.values}).y(getY)
(!data.length&&hideable?hideable:data);data=data.map(function(series,i){series.values=series.values.map(function(point){point.series=i;return point;});return series;});if(stacked)
data[0].values.map(function(d,i){var posBase=0,negBase=0;data.map(function(d){var f=d.values[i]
f.size=Math.abs(f.y);if(f.y<0){f.y1=negBase;negBase=negBase-f.size;}else
{f.y1=f.size+posBase;posBase=posBase+f.size;}});});var seriesData=(xDomain&&yDomain)?[]:data.map(function(d){return d.values.map(function(d,i){return{x:getX(d,i),y:getY(d,i),y0:d.y0,y1:d.y1}})});x.domain(d3.merge(seriesData).map(function(d){return d.x})).rangeBands([0,availableWidth],.1);y.domain(yDomain||d3.extent(d3.merge(seriesData).map(function(d){return stacked?(d.y>0?d.y1:d.y1+d.y):d.y}).concat(forceY))).range([availableHeight,0]);if(x.domain()[0]===x.domain()[1]||y.domain()[0]===y.domain()[1])singlePoint=true;if(x.domain()[0]===x.domain()[1])
x.domain()[0]?x.domain([x.domain()[0]-x.domain()[0]*0.01,x.domain()[1]+x.domain()[1]*0.01]):x.domain([-1,1]);if(y.domain()[0]===y.domain()[1])
y.domain()[0]?y.domain([y.domain()[0]+y.domain()[0]*0.01,y.domain()[1]-y.domain()[1]*0.01]):y.domain([-1,1]);x0=x0||x;y0=y0||y;var wrap=container.selectAll('g.nv-wrap.nv-multibar').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-multibar');var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g')
gEnter.append('g').attr('class','nv-groups');wrap.attr('transform','translate('+margin.left+','+margin.top+')');defsEnter.append('clipPath').attr('id','nv-edge-clip-'+id).append('rect');wrap.select('#nv-edge-clip-'+id+' rect').attr('width',availableWidth).attr('height',availableHeight);g.attr('clip-path',clipEdge?'url(#nv-edge-clip-'+id+')':'');var groups=wrap.select('.nv-groups').selectAll('.nv-group').data(function(d){return d},function(d){return d.key});groups.enter().append('g').style('stroke-opacity',1e-6).style('fill-opacity',1e-6);groups.exit().selectAll('rect.nv-bar').transition().delay(function(d,i){return i*delay/data[0].values.length}).attr('y',function(d){return stacked?y0(d.y0):y0(0)}).attr('height',0).remove();groups.attr('class',function(d,i){return'nv-group nv-series-'+i}).classed('hover',function(d){return d.hover}).style('fill',function(d,i){return color(d,i)}).style('stroke',function(d,i){return color(d,i)});d3.transition(groups).style('stroke-opacity',1).style('fill-opacity',.75);var bars=groups.selectAll('rect.nv-bar').data(function(d){return(hideable&&!data.length)?hideable.values:d.values});bars.exit().remove();var barsEnter=bars.enter().append('rect').attr('class',function(d,i){return getY(d,i)<0?'nv-bar negative':'nv-bar positive'}).attr('x',function(d,i,j){return stacked?0:(j*x.rangeBand()/data.length)}).attr('y',function(d){return y0(stacked?d.y0:0)}).attr('height',0).attr('width',x.rangeBand()/(stacked?1:data.length));bars.style('fill',function(d,i,j){return color(d,j,i);}).style('stroke',function(d,i,j){return color(d,j,i);}).on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.elementMouseover({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(stacked?data.length/2:d.series+.5)/data.length),y(getY(d,i)+(stacked?d.y0:0))],pointIndex:i,seriesIndex:d.series,e:d3.event});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.elementMouseout({value:getY(d,i),point:d,series:data[d.series],pointIndex:i,seriesIndex:d.series,e:d3.event});}).on('click',function(d,i){dispatch.elementClick({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(stacked?data.length/2:d.series+.5)/data.length),y(getY(d,i)+(stacked?d.y0:0))],pointIndex:i,seriesIndex:d.series,e:d3.event});d3.event.stopPropagation();}).on('dblclick',function(d,i){dispatch.elementDblClick({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(stacked?data.length/2:d.series+.5)/data.length),y(getY(d,i)+(stacked?d.y0:0))],pointIndex:i,seriesIndex:d.series,e:d3.event});d3.event.stopPropagation();});bars.attr('class',function(d,i){return getY(d,i)<0?'nv-bar negative':'nv-bar positive'}).attr('transform',function(d,i){return'translate('+x(getX(d,i))+',0)';})
if(barColor){if(!disabled)disabled=data.map(function(){return true});bars.style('fill',function(d,i,j){return d3.rgb(barColor(d,i)).darker(disabled.map(function(d,i){return i}).filter(function(d,i){return!disabled[i]})[j]).toString();}).style('stroke',function(d,i,j){return d3.rgb(barColor(d,i)).darker(disabled.map(function(d,i){return i}).filter(function(d,i){return!disabled[i]})[j]).toString();});}
if(stacked)
bars.transition().delay(function(d,i){return i*delay/data[0].values.length}).attr('y',function(d,i){return y((stacked?d.y1:0));}).attr('height',function(d,i){return Math.max(Math.abs(y(d.y+(stacked?d.y0:0))-y((stacked?d.y0:0))),1);}).each('end',function(){d3.transition(d3.select(this)).attr('x',function(d,i){return stacked?0:(d.series*x.rangeBand()/data.length)}).attr('width',x.rangeBand()/(stacked?1:data.length));})
else
d3.transition(bars).delay(function(d,i){return i*delay/data[0].values.length}).attr('x',function(d,i){return d.series*x.rangeBand()/data.length}).attr('width',x.rangeBand()/data.length).each('end',function(){d3.transition(d3.select(this)).attr('y',function(d,i){return getY(d,i)<0?y(0):y(0)-y(getY(d,i))<1?y(0)-1:y(getY(d,i))||0;}).attr('height',function(d,i){return Math.max(Math.abs(y(getY(d,i))-y(0)),1)||0;});})
x0=x.copy();y0=y.copy();});return chart;}
chart.dispatch=dispatch;chart.x=function(_){if(!arguments.length)return getX;getX=_;return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.forceY=function(_){if(!arguments.length)return forceY;forceY=_;return chart;};chart.stacked=function(_){if(!arguments.length)return stacked;stacked=_;return chart;};chart.clipEdge=function(_){if(!arguments.length)return clipEdge;clipEdge=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.barColor=function(_){if(!arguments.length)return barColor;barColor=nv.utils.getColor(_);return chart;};chart.disabled=function(_){if(!arguments.length)return disabled;disabled=_;return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};chart.hideable=function(_){if(!arguments.length)return hideable;hideable=_;return chart;};chart.delay=function(_){if(!arguments.length)return delay;delay=_;return chart;};return chart;}
nv.models.multiBarChart=function(){var multibar=nv.models.multiBar(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend(),controls=nv.models.legend();var margin={top:30,right:20,bottom:50,left:60},width=null,height=null,color=nv.utils.defaultColor(),showControls=true,showLegend=true,reduceXTicks=true,staggerLabels=false,rotateLabels=0,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' on '+x+'</p>'},x,y,state={stacked:false},defaultState=null,noData="No Data Available.",dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState'),controlWidth=function(){return showControls?180:0};multibar.stacked(false);xAxis.orient('bottom').tickPadding(7).highlightZero(false).showMaxMin(false).tickFormat(function(d){return d});yAxis.orient('left').tickFormat(d3.format(',.1f'));var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(multibar.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(multibar.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'n':'s',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){selection.transition().call(chart)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=multibar.xScale();y=multibar.yScale();var wrap=container.selectAll('g.nv-wrap.nv-multiBarWithLegend').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-multiBarWithLegend').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-barsWrap');gEnter.append('g').attr('class','nv-legendWrap');gEnter.append('g').attr('class','nv-controlsWrap');if(showLegend){legend.width(availableWidth-controlWidth());if(multibar.barColor())
data.forEach(function(series,i){series.color=d3.rgb('#ccc').darker(i*1.5).toString();})
g.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.nv-legendWrap').attr('transform','translate('+controlWidth()+','+(-margin.top)+')');}
if(showControls){var controlsData=[{key:'Grouped',disabled:multibar.stacked()},{key:'Stacked',disabled:!multibar.stacked()}];controls.width(controlWidth()).color(['#444','#444','#444']);g.select('.nv-controlsWrap').datum(controlsData).attr('transform','translate(0,'+(-margin.top)+')').call(controls);}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');multibar.disabled(data.map(function(series){return series.disabled})).width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}))
var barsWrap=g.select('.nv-barsWrap').datum(data.filter(function(d){return!d.disabled}))
d3.transition(barsWrap).call(multibar);xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+y.range()[0]+')');d3.transition(g.select('.nv-x.nv-axis')).call(xAxis);var xTicks=g.select('.nv-x.nv-axis > g').selectAll('g');xTicks.selectAll('line, text').style('opacity',1)
if(staggerLabels){var getTranslate=function(x,y){return"translate("+x+","+y+")";};var staggerUp=5,staggerDown=17;xTicks.selectAll("text").attr('transform',function(d,i,j){return getTranslate(0,(j%2==0?staggerUp:staggerDown));});var totalInBetweenTicks=d3.selectAll(".nv-x.nv-axis .nv-wrap g g text")[0].length;g.selectAll(".nv-x.nv-axis .nv-axisMaxMin text").attr("transform",function(d,i){return getTranslate(0,(i===0||totalInBetweenTicks%2!==0)?staggerDown:staggerUp);});}
if(reduceXTicks)
xTicks.filter(function(d,i){return i%Math.ceil(data[0].values.length/(availableWidth/100))!==0;}).selectAll('text, line').style('opacity',0);if(rotateLabels)
xTicks.selectAll('text').attr('transform','rotate('+rotateLabels+' 0,0)').attr('text-anchor',rotateLabels>0?'start':'end');g.select('.nv-x.nv-axis').selectAll('g.nv-axisMaxMin text').style('opacity',1);yAxis.scale(y).ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.nv-y.nv-axis')).call(yAxis);legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);selection.transition().call(chart);});controls.dispatch.on('legendClick',function(d,i){if(!d.disabled)return;controlsData=controlsData.map(function(s){s.disabled=true;return s;});d.disabled=false;switch(d.key){case'Grouped':multibar.stacked(false);break;case'Stacked':multibar.stacked(true);break;}
state.stacked=multibar.stacked();dispatch.stateChange(state);selection.transition().call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode)});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
if(typeof e.stacked!=='undefined'){multibar.stacked(e.stacked);state.stacked=e.stacked;}
selection.call(chart);});});return chart;}
multibar.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});multibar.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.multibar=multibar;chart.legend=legend;chart.xAxis=xAxis;chart.yAxis=yAxis;d3.rebind(chart,multibar,'x','y','xDomain','yDomain','forceX','forceY','clipEdge','id','stacked','delay','barColor');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.showControls=function(_){if(!arguments.length)return showControls;showControls=_;return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.reduceXTicks=function(_){if(!arguments.length)return reduceXTicks;reduceXTicks=_;return chart;};chart.rotateLabels=function(_){if(!arguments.length)return rotateLabels;rotateLabels=_;return chart;}
chart.staggerLabels=function(_){if(!arguments.length)return staggerLabels;staggerLabels=_;return chart;};chart.tooltip=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.multiBarHorizontal=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,id=Math.floor(Math.random()*10000),x=d3.scale.ordinal(),y=d3.scale.linear(),getX=function(d){return d.x},getY=function(d){return d.y},forceY=[0],color=nv.utils.defaultColor(),barColor=null,disabled,stacked=false,showValues=false,valuePadding=60,valueFormat=d3.format(',.2f'),delay=1200,xDomain,yDomain,dispatch=d3.dispatch('chartClick','elementClick','elementDblClick','elementMouseover','elementMouseout');var x0,y0;function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);if(stacked)
data=d3.layout.stack().offset('zero').values(function(d){return d.values}).y(getY)
(data);data=data.map(function(series,i){series.values=series.values.map(function(point){point.series=i;return point;});return series;});if(stacked)
data[0].values.map(function(d,i){var posBase=0,negBase=0;data.map(function(d){var f=d.values[i]
f.size=Math.abs(f.y);if(f.y<0){f.y1=negBase-f.size;negBase=negBase-f.size;}else
{f.y1=posBase;posBase=posBase+f.size;}});});var seriesData=(xDomain&&yDomain)?[]:data.map(function(d){return d.values.map(function(d,i){return{x:getX(d,i),y:getY(d,i),y0:d.y0,y1:d.y1}})});x.domain(xDomain||d3.merge(seriesData).map(function(d){return d.x})).rangeBands([0,availableHeight],.1);y.domain(yDomain||d3.extent(d3.merge(seriesData).map(function(d){return stacked?(d.y>0?d.y1+d.y:d.y1):d.y}).concat(forceY)))
if(showValues&&!stacked)
y.range([(y.domain()[0]<0?valuePadding:0),availableWidth-(y.domain()[1]>0?valuePadding:0)]);else
y.range([0,availableWidth]);x0=x0||x;y0=y0||d3.scale.linear().domain(y.domain()).range([y(0),y(0)]);var wrap=d3.select(this).selectAll('g.nv-wrap.nv-multibarHorizontal').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-multibarHorizontal');var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-groups');wrap.attr('transform','translate('+margin.left+','+margin.top+')');var groups=wrap.select('.nv-groups').selectAll('.nv-group').data(function(d){return d},function(d){return d.key});groups.enter().append('g').style('stroke-opacity',1e-6).style('fill-opacity',1e-6);d3.transition(groups.exit()).style('stroke-opacity',1e-6).style('fill-opacity',1e-6).remove();groups.attr('class',function(d,i){return'nv-group nv-series-'+i}).classed('hover',function(d){return d.hover}).style('fill',function(d,i){return color(d,i)}).style('stroke',function(d,i){return color(d,i)});d3.transition(groups).style('stroke-opacity',1).style('fill-opacity',.75);var bars=groups.selectAll('g.nv-bar').data(function(d){return d.values});bars.exit().remove();var barsEnter=bars.enter().append('g').attr('transform',function(d,i,j){return'translate('+y0(stacked?d.y0:0)+','+(stacked?0:(j*x.rangeBand()/data.length)+x(getX(d,i)))+')'});barsEnter.append('rect').attr('width',0).attr('height',x.rangeBand()/(stacked?1:data.length))
bars.on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.elementMouseover({value:getY(d,i),point:d,series:data[d.series],pos:[y(getY(d,i)+(stacked?d.y0:0)),x(getX(d,i))+(x.rangeBand()*(stacked?data.length/2:d.series+.5)/data.length)],pointIndex:i,seriesIndex:d.series,e:d3.event});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.elementMouseout({value:getY(d,i),point:d,series:data[d.series],pointIndex:i,seriesIndex:d.series,e:d3.event});}).on('click',function(d,i){dispatch.elementClick({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(stacked?data.length/2:d.series+.5)/data.length),y(getY(d,i)+(stacked?d.y0:0))],pointIndex:i,seriesIndex:d.series,e:d3.event});d3.event.stopPropagation();}).on('dblclick',function(d,i){dispatch.elementDblClick({value:getY(d,i),point:d,series:data[d.series],pos:[x(getX(d,i))+(x.rangeBand()*(stacked?data.length/2:d.series+.5)/data.length),y(getY(d,i)+(stacked?d.y0:0))],pointIndex:i,seriesIndex:d.series,e:d3.event});d3.event.stopPropagation();});barsEnter.append('text');if(showValues&&!stacked){bars.select('text').attr('text-anchor',function(d,i){return getY(d,i)<0?'end':'start'}).attr('y',x.rangeBand()/(data.length*2)).attr('dy','.32em').text(function(d,i){return valueFormat(getY(d,i))})
d3.transition(bars).select('text').attr('x',function(d,i){return getY(d,i)<0?-4:y(getY(d,i))-y(0)+4})}else{bars.selectAll('text').text('');}
bars.attr('class',function(d,i){return getY(d,i)<0?'nv-bar negative':'nv-bar positive'})
if(barColor){if(!disabled)disabled=data.map(function(){return true});bars.style('fill',function(d,i,j){return d3.rgb(barColor(d,i)).darker(disabled.map(function(d,i){return i}).filter(function(d,i){return!disabled[i]})[j]).toString();}).style('stroke',function(d,i,j){return d3.rgb(barColor(d,i)).darker(disabled.map(function(d,i){return i}).filter(function(d,i){return!disabled[i]})[j]).toString();});}
if(stacked)
d3.transition(bars).attr('transform',function(d,i){return'translate('+y(d.y1)+','+x(getX(d,i))+')'}).select('rect').attr('width',function(d,i){return Math.abs(y(getY(d,i)+d.y0)-y(d.y0))}).attr('height',x.rangeBand());else
d3.transition(bars).attr('transform',function(d,i){return'translate('+
(getY(d,i)<0?y(getY(d,i)):y(0))
+','+
(d.series*x.rangeBand()/data.length
+
x(getX(d,i)))
+')'}).select('rect').attr('height',x.rangeBand()/data.length).attr('width',function(d,i){return Math.max(Math.abs(y(getY(d,i))-y(0)),1)});x0=x.copy();y0=y.copy();});return chart;}
chart.dispatch=dispatch;chart.x=function(_){if(!arguments.length)return getX;getX=_;return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.forceY=function(_){if(!arguments.length)return forceY;forceY=_;return chart;};chart.stacked=function(_){if(!arguments.length)return stacked;stacked=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.barColor=function(_){if(!arguments.length)return barColor;barColor=nv.utils.getColor(_);return chart;};chart.disabled=function(_){if(!arguments.length)return disabled;disabled=_;return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};chart.delay=function(_){if(!arguments.length)return delay;delay=_;return chart;};chart.showValues=function(_){if(!arguments.length)return showValues;showValues=_;return chart;};chart.valueFormat=function(_){if(!arguments.length)return valueFormat;valueFormat=_;return chart;};chart.valuePadding=function(_){if(!arguments.length)return valuePadding;valuePadding=_;return chart;};return chart;}
nv.models.multiBarHorizontalChart=function(){var multibar=nv.models.multiBarHorizontal(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend().height(30),controls=nv.models.legend().height(30);var margin={top:30,right:20,bottom:50,left:60},width=null,height=null,color=nv.utils.defaultColor(),showControls=true,showLegend=true,stacked=false,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+' - '+x+'</h3>'+'<p>'+y+'</p>'},x,y,state={stacked:stacked},defaultState=null,noData='No Data Available.',dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState'),controlWidth=function(){return showControls?180:0};multibar.stacked(stacked);xAxis.orient('left').tickPadding(5).highlightZero(false).showMaxMin(false).tickFormat(function(d){return d});yAxis.orient('bottom').tickFormat(d3.format(',.1f'));var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(multibar.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(multibar.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'e':'w',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){selection.transition().call(chart)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=multibar.xScale();y=multibar.yScale();var wrap=container.selectAll('g.nv-wrap.nv-multiBarHorizontalChart').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-multiBarHorizontalChart').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-barsWrap');gEnter.append('g').attr('class','nv-legendWrap');gEnter.append('g').attr('class','nv-controlsWrap');if(showLegend){legend.width(availableWidth-controlWidth());if(multibar.barColor())
data.forEach(function(series,i){series.color=d3.rgb('#ccc').darker(i*1.5).toString();})
g.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.nv-legendWrap').attr('transform','translate('+controlWidth()+','+(-margin.top)+')');}
if(showControls){var controlsData=[{key:'Grouped',disabled:multibar.stacked()},{key:'Stacked',disabled:!multibar.stacked()}];controls.width(controlWidth()).color(['#444','#444','#444']);g.select('.nv-controlsWrap').datum(controlsData).attr('transform','translate(0,'+(-margin.top)+')').call(controls);}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');multibar.disabled(data.map(function(series){return series.disabled})).width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}))
var barsWrap=g.select('.nv-barsWrap').datum(data.filter(function(d){return!d.disabled}))
d3.transition(barsWrap).call(multibar);xAxis.scale(x).ticks(availableHeight/24).tickSize(-availableWidth,0);d3.transition(g.select('.nv-x.nv-axis')).call(xAxis);var xTicks=g.select('.nv-x.nv-axis').selectAll('g');xTicks.selectAll('line, text').style('opacity',1)
yAxis.scale(y).ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-y.nv-axis').attr('transform','translate(0,'+availableHeight+')');d3.transition(g.select('.nv-y.nv-axis')).call(yAxis);legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);selection.transition().call(chart);});controls.dispatch.on('legendClick',function(d,i){if(!d.disabled)return;controlsData=controlsData.map(function(s){s.disabled=true;return s;});d.disabled=false;switch(d.key){case'Grouped':multibar.stacked(false);break;case'Stacked':multibar.stacked(true);break;}
state.stacked=multibar.stacked();dispatch.stateChange(state);selection.transition().call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
if(typeof e.stacked!=='undefined'){multibar.stacked(e.stacked);state.stacked=e.stacked;}
selection.call(chart);});});return chart;}
multibar.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});multibar.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.multibar=multibar;chart.legend=legend;chart.xAxis=xAxis;chart.yAxis=yAxis;d3.rebind(chart,multibar,'x','y','xDomain','yDomain','forceX','forceY','clipEdge','id','delay','showValues','valueFormat','stacked','barColor');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);return chart;};chart.showControls=function(_){if(!arguments.length)return showControls;showControls=_;return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltip=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.multiChart=function(){var margin={top:30,right:20,bottom:50,left:60},color=d3.scale.category20().range(),width=null,height=null,showLegend=true,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' at '+x+'</p>'},x,y;var x=d3.scale.linear(),yScale1=d3.scale.linear(),yScale2=d3.scale.linear(),lines1=nv.models.line().yScale(yScale1),lines2=nv.models.line().yScale(yScale2),bars1=nv.models.multiBar().stacked(false).yScale(yScale1),bars2=nv.models.multiBar().stacked(false).yScale(yScale2),stack1=nv.models.stackedArea().yScale(yScale1),stack2=nv.models.stackedArea().yScale(yScale2),xAxis=nv.models.axis().scale(x).orient('bottom').tickPadding(5),yAxis1=nv.models.axis().scale(yScale1).orient('left'),yAxis2=nv.models.axis().scale(yScale2).orient('right'),legend=nv.models.legend().height(30),dispatch=d3.dispatch('tooltipShow','tooltipHide');var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(lines1.x()(e.point,e.pointIndex)),y=((e.series.yAxis==2)?yAxis2:yAxis1).tickFormat()(lines1.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,undefined,undefined,offsetElement.offsetParent);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;var dataLines1=data.filter(function(d){return!d.disabled&&d.type=='line'&&d.yAxis==1})
var dataLines2=data.filter(function(d){return!d.disabled&&d.type=='line'&&d.yAxis==2})
var dataBars1=data.filter(function(d){return!d.disabled&&d.type=='bar'&&d.yAxis==1})
var dataBars2=data.filter(function(d){return!d.disabled&&d.type=='bar'&&d.yAxis==2})
var dataStack1=data.filter(function(d){return!d.disabled&&d.type=='area'&&d.yAxis==1})
var dataStack2=data.filter(function(d){return!d.disabled&&d.type=='area'&&d.yAxis==2})
var series1=data.filter(function(d){return!d.disabled&&d.yAxis==1}).map(function(d){return d.values.map(function(d,i){return{x:d.x,y:d.y}})})
var series2=data.filter(function(d){return!d.disabled&&d.yAxis==2}).map(function(d){return d.values.map(function(d,i){return{x:d.x,y:d.y}})})
x.domain(d3.extent(d3.merge(series1.concat(series2)),function(d){return d.x})).range([0,availableWidth]);var wrap=container.selectAll('g.wrap.multiChart').data([data]);var gEnter=wrap.enter().append('g').attr('class','wrap nvd3 multiChart').append('g');gEnter.append('g').attr('class','x axis');gEnter.append('g').attr('class','y1 axis');gEnter.append('g').attr('class','y2 axis');gEnter.append('g').attr('class','lines1Wrap');gEnter.append('g').attr('class','lines2Wrap');gEnter.append('g').attr('class','bars1Wrap');gEnter.append('g').attr('class','bars2Wrap');gEnter.append('g').attr('class','stack1Wrap');gEnter.append('g').attr('class','stack2Wrap');gEnter.append('g').attr('class','legendWrap');var g=wrap.select('g');if(showLegend){legend.width(availableWidth/2);g.select('.legendWrap').datum(data.map(function(series){series.originalKey=series.originalKey===undefined?series.key:series.originalKey;series.key=series.originalKey+(series.yAxis==1?'':' (right axis)');return series;})).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.legendWrap').attr('transform','translate('+(availableWidth/2)+','+(-margin.top)+')');}
lines1.width(availableWidth).height(availableHeight).interpolate("monotone").color(data.map(function(d,i){return d.color||color[i%color.length];}).filter(function(d,i){return!data[i].disabled&&data[i].yAxis==1&&data[i].type=='line'}));lines2.width(availableWidth).height(availableHeight).interpolate("monotone").color(data.map(function(d,i){return d.color||color[i%color.length];}).filter(function(d,i){return!data[i].disabled&&data[i].yAxis==2&&data[i].type=='line'}));bars1.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color[i%color.length];}).filter(function(d,i){return!data[i].disabled&&data[i].yAxis==1&&data[i].type=='bar'}));bars2.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color[i%color.length];}).filter(function(d,i){return!data[i].disabled&&data[i].yAxis==2&&data[i].type=='bar'}));stack1.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color[i%color.length];}).filter(function(d,i){return!data[i].disabled&&data[i].yAxis==1&&data[i].type=='area'}));stack2.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color[i%color.length];}).filter(function(d,i){return!data[i].disabled&&data[i].yAxis==2&&data[i].type=='area'}));g.attr('transform','translate('+margin.left+','+margin.top+')');var lines1Wrap=g.select('.lines1Wrap').datum(dataLines1)
var bars1Wrap=g.select('.bars1Wrap').datum(dataBars1)
var stack1Wrap=g.select('.stack1Wrap').datum(dataStack1)
var lines2Wrap=g.select('.lines2Wrap').datum(dataLines2)
var bars2Wrap=g.select('.bars2Wrap').datum(dataBars2)
var stack2Wrap=g.select('.stack2Wrap').datum(dataStack2)
var extraValue1=dataStack1.length?dataStack1.map(function(a){return a.values}).reduce(function(a,b){return a.map(function(aVal,i){return{x:aVal.x,y:aVal.y+b[i].y}})}).concat([{x:0,y:0}]):[]
var extraValue2=dataStack2.length?dataStack2.map(function(a){return a.values}).reduce(function(a,b){return a.map(function(aVal,i){return{x:aVal.x,y:aVal.y+b[i].y}})}).concat([{x:0,y:0}]):[]
yScale1.domain(d3.extent(d3.merge(series1).concat(extraValue1),function(d){return d.y})).range([0,availableHeight])
yScale2.domain(d3.extent(d3.merge(series2).concat(extraValue2),function(d){return d.y})).range([0,availableHeight])
lines1.yDomain(yScale1.domain())
bars1.yDomain(yScale1.domain())
stack1.yDomain(yScale1.domain())
lines2.yDomain(yScale2.domain())
bars2.yDomain(yScale2.domain())
stack2.yDomain(yScale2.domain())
if(dataStack1.length){d3.transition(stack1Wrap).call(stack1);}
if(dataStack2.length){d3.transition(stack2Wrap).call(stack2);}
if(dataBars1.length){d3.transition(bars1Wrap).call(bars1);}
if(dataBars2.length){d3.transition(bars2Wrap).call(bars2);}
if(dataLines1.length){d3.transition(lines1Wrap).call(lines1);}
if(dataLines2.length){d3.transition(lines2Wrap).call(lines2);}
xAxis.ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.x.axis').attr('transform','translate(0,'+availableHeight+')');d3.transition(g.select('.x.axis')).call(xAxis);yAxis1.ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.y1.axis')).call(yAxis1);yAxis2.ticks(availableHeight/36).tickSize(-availableWidth,0);d3.transition(g.select('.y2.axis')).call(yAxis2);g.select('.y2.axis').style('opacity',series2.length?1:0).attr('transform','translate('+x.range()[1]+',0)');legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.series').classed('disabled',false);return d;});}
selection.transition().call(chart);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});});chart.update=function(){chart(selection)};chart.container=this;return chart;}
lines1.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines1.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});lines2.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines2.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});bars1.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});bars1.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});bars2.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});bars2.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});stack1.dispatch.on('tooltipShow',function(e){if(!Math.round(stack1.y()(e.point)*100)){setTimeout(function(){d3.selectAll('.point.hover').classed('hover',false)},0);return false;}
e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top],dispatch.tooltipShow(e);});stack1.dispatch.on('tooltipHide',function(e){dispatch.tooltipHide(e);});stack2.dispatch.on('tooltipShow',function(e){if(!Math.round(stack2.y()(e.point)*100)){setTimeout(function(){d3.selectAll('.point.hover').classed('hover',false)},0);return false;}
e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top],dispatch.tooltipShow(e);});stack2.dispatch.on('tooltipHide',function(e){dispatch.tooltipHide(e);});lines1.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines1.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});lines2.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});lines2.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.lines1=lines1;chart.lines2=lines2;chart.bars1=bars1;chart.bars2=bars2;chart.stack1=stack1;chart.stack2=stack2;chart.xAxis=xAxis;chart.yAxis1=yAxis1;chart.yAxis2=yAxis2;chart.x=function(_){if(!arguments.length)return getX;getX=_;lines1.x(_);bars1.x(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;lines1.y(_);bars1.y(_);return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin=_;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=_;legend.color(_);return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};return chart;}
nv.models.ohlcBar=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,id=Math.floor(Math.random()*10000),x=d3.scale.linear(),y=d3.scale.linear(),getX=function(d){return d.x},getY=function(d){return d.y},getOpen=function(d){return d.open},getClose=function(d){return d.close},getHigh=function(d){return d.high},getLow=function(d){return d.low},forceX=[],forceY=[],padData=false,clipEdge=true,color=nv.utils.defaultColor(),xDomain,yDomain,dispatch=d3.dispatch('chartClick','elementClick','elementDblClick','elementMouseover','elementMouseout');function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);x.domain(xDomain||d3.extent(data[0].values.map(getX).concat(forceX)));if(padData)
x.range([availableWidth*.5/data[0].values.length,availableWidth*(data[0].values.length-.5)/data[0].values.length]);else
x.range([0,availableWidth]);y.domain(yDomain||[d3.min(data[0].values.map(getLow).concat(forceY)),d3.max(data[0].values.map(getHigh).concat(forceY))]).range([availableHeight,0]);if(x.domain()[0]===x.domain()[1]||y.domain()[0]===y.domain()[1])singlePoint=true;if(x.domain()[0]===x.domain()[1])
x.domain()[0]?x.domain([x.domain()[0]-x.domain()[0]*0.01,x.domain()[1]+x.domain()[1]*0.01]):x.domain([-1,1]);if(y.domain()[0]===y.domain()[1])
y.domain()[0]?y.domain([y.domain()[0]+y.domain()[0]*0.01,y.domain()[1]-y.domain()[1]*0.01]):y.domain([-1,1]);var wrap=d3.select(this).selectAll('g.nv-wrap.nv-ohlcBar').data([data[0].values]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-ohlcBar');var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-ticks');wrap.attr('transform','translate('+margin.left+','+margin.top+')');container.on('click',function(d,i){dispatch.chartClick({data:d,index:i,pos:d3.event,id:id});});defsEnter.append('clipPath').attr('id','nv-chart-clip-path-'+id).append('rect');wrap.select('#nv-chart-clip-path-'+id+' rect').attr('width',availableWidth).attr('height',availableHeight);g.attr('clip-path',clipEdge?'url(#nv-chart-clip-path-'+id+')':'');var ticks=wrap.select('.nv-ticks').selectAll('.nv-tick').data(function(d){return d});ticks.exit().remove();var ticksEnter=ticks.enter().append('path').attr('class',function(d,i,j){return(getOpen(d,i)>getClose(d,i)?'nv-tick negative':'nv-tick positive')+' nv-tick-'+j+'-'+i}).attr('d',function(d,i){var w=(availableWidth/data[0].values.length)*.9;return'm0,0l0,'
+(y(getOpen(d,i))
-y(getHigh(d,i)))
+'l'
+(-w/2)
+',0l'
+(w/2)
+',0l0,'
+(y(getLow(d,i))-y(getOpen(d,i)))
+'l0,'
+(y(getClose(d,i))
-y(getLow(d,i)))
+'l'
+(w/2)
+',0l'
+(-w/2)
+',0z';}).attr('transform',function(d,i){return'translate('+x(getX(d,i))+','+y(getHigh(d,i))+')';}).on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.elementMouseover({point:d,series:data[0],pos:[x(getX(d,i)),y(getY(d,i))],pointIndex:i,seriesIndex:0,e:d3.event});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.elementMouseout({point:d,series:data[0],pointIndex:i,seriesIndex:0,e:d3.event});}).on('click',function(d,i){dispatch.elementClick({value:getY(d,i),data:d,index:i,pos:[x(getX(d,i)),y(getY(d,i))],e:d3.event,id:id});d3.event.stopPropagation();}).on('dblclick',function(d,i){dispatch.elementDblClick({value:getY(d,i),data:d,index:i,pos:[x(getX(d,i)),y(getY(d,i))],e:d3.event,id:id});d3.event.stopPropagation();});ticks.attr('class',function(d,i,j){return(getOpen(d,i)>getClose(d,i)?'nv-tick negative':'nv-tick positive')+' nv-tick-'+j+'-'+i})
d3.transition(ticks).attr('transform',function(d,i){return'translate('+x(getX(d,i))+','+y(getHigh(d,i))+')';}).attr('d',function(d,i){var w=(availableWidth/data[0].values.length)*.9;return'm0,0l0,'
+(y(getOpen(d,i))
-y(getHigh(d,i)))
+'l'
+(-w/2)
+',0l'
+(w/2)
+',0l0,'
+(y(getLow(d,i))
-y(getOpen(d,i)))
+'l0,'
+(y(getClose(d,i))
-y(getLow(d,i)))
+'l'
+(w/2)
+',0l'
+(-w/2)
+',0z';})});return chart;}
chart.dispatch=dispatch;chart.x=function(_){if(!arguments.length)return getX;getX=_;return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=_;return chart;};chart.open=function(_){if(!arguments.length)return getOpen;getOpen=_;return chart;};chart.close=function(_){if(!arguments.length)return getClose;getClose=_;return chart;};chart.high=function(_){if(!arguments.length)return getHigh;getHigh=_;return chart;};chart.low=function(_){if(!arguments.length)return getLow;getLow=_;return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.forceX=function(_){if(!arguments.length)return forceX;forceX=_;return chart;};chart.forceY=function(_){if(!arguments.length)return forceY;forceY=_;return chart;};chart.padData=function(_){if(!arguments.length)return padData;padData=_;return chart;};chart.clipEdge=function(_){if(!arguments.length)return clipEdge;clipEdge=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};return chart;}
nv.models.pie=function(){var margin={top:0,right:0,bottom:0,left:0},width=500,height=500,getValues=function(d){return d.values},getX=function(d){return d.x},getY=function(d){return d.y},getDescription=function(d){return d.description},id=Math.floor(Math.random()*10000),color=nv.utils.defaultColor(),valueFormat=d3.format(',.2f'),showLabels=true,pieLabelsOutside=true,donutLabelsOutside=false,labelThreshold=.02,donut=false,labelSunbeamLayout=false,startAngle=false,endAngle=false,donutRatio=0.5,dispatch=d3.dispatch('chartClick','elementClick','elementDblClick','elementMouseover','elementMouseout');function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,radius=Math.min(availableWidth,availableHeight)/2,arcRadius=radius-(radius/5),container=d3.select(this);var wrap=container.selectAll('.nv-wrap.nv-pie').data([getValues(data[0])]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-pie nv-chart-'+id);var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-pie');wrap.attr('transform','translate('+margin.left+','+margin.top+')');g.select('.nv-pie').attr('transform','translate('+availableWidth/2+','+availableHeight/2+')');container.on('click',function(d,i){dispatch.chartClick({data:d,index:i,pos:d3.event,id:id});});var arc=d3.svg.arc().outerRadius(arcRadius);if(startAngle)arc.startAngle(startAngle)
if(endAngle)arc.endAngle(endAngle);if(donut)arc.innerRadius(radius*donutRatio);var pie=d3.layout.pie().sort(null).value(function(d){return d.disabled?0:getY(d)});var slices=wrap.select('.nv-pie').selectAll('.nv-slice').data(pie);slices.exit().remove();var ae=slices.enter().append('g').attr('class','nv-slice').on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.elementMouseover({label:getX(d.data),value:getY(d.data),point:d.data,pointIndex:i,pos:[d3.event.pageX,d3.event.pageY],id:id});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.elementMouseout({label:getX(d.data),value:getY(d.data),point:d.data,index:i,id:id});}).on('click',function(d,i){dispatch.elementClick({label:getX(d.data),value:getY(d.data),point:d.data,index:i,pos:d3.event,id:id});d3.event.stopPropagation();}).on('dblclick',function(d,i){dispatch.elementDblClick({label:getX(d.data),value:getY(d.data),point:d.data,index:i,pos:d3.event,id:id});d3.event.stopPropagation();});slices.attr('fill',function(d,i){return color(d,i);}).attr('stroke',function(d,i){return color(d,i);});var paths=ae.append('path').each(function(d){this._current=d;});d3.transition(slices.select('path')).attr('d',arc).attrTween('d',arcTween);if(showLabels){var labelsArc=d3.svg.arc().innerRadius(0);if(pieLabelsOutside){labelsArc=arc;}
if(donutLabelsOutside){labelsArc=d3.svg.arc().outerRadius(arc.outerRadius());}
ae.append("g").classed("nv-label",true).each(function(d,i){var group=d3.select(this);group.attr('transform',function(d){if(labelSunbeamLayout){d.outerRadius=arcRadius+10;d.innerRadius=arcRadius+15;var rotateAngle=(d.startAngle+d.endAngle)/2*(180/Math.PI);if((d.startAngle+d.endAngle)/2<Math.PI){rotateAngle-=90;}else{rotateAngle+=90;}
return'translate('+labelsArc.centroid(d)+') rotate('+rotateAngle+')';}else{d.outerRadius=radius+10;d.innerRadius=radius+15;return'translate('+labelsArc.centroid(d)+')'}});group.append('rect').style('stroke','#fff').style('fill','#fff').attr("rx",3).attr("ry",3);group.append('text').style('text-anchor',labelSunbeamLayout?((d.startAngle+d.endAngle)/2<Math.PI?'start':'end'):'middle').style('fill','#000')});slices.select(".nv-label").transition().attr('transform',function(d){if(labelSunbeamLayout){d.outerRadius=arcRadius+10;d.innerRadius=arcRadius+15;var rotateAngle=(d.startAngle+d.endAngle)/2*(180/Math.PI);if((d.startAngle+d.endAngle)/2<Math.PI){rotateAngle-=90;}else{rotateAngle+=90;}
return'translate('+labelsArc.centroid(d)+') rotate('+rotateAngle+')';}else{d.outerRadius=radius+10;d.innerRadius=radius+15;return'translate('+labelsArc.centroid(d)+')'}});slices.each(function(d,i){var slice=d3.select(this);slice.select(".nv-label text").style('text-anchor',labelSunbeamLayout?((d.startAngle+d.endAngle)/2<Math.PI?'start':'end'):'middle').text(function(d,i){var percent=(d.endAngle-d.startAngle)/(2*Math.PI);return(d.value&&percent>labelThreshold)?getX(d.data):'';});var textBox=slice.select('text').node().getBBox();slice.select(".nv-label rect").attr("width",textBox.width+10).attr("height",textBox.height+10).attr("transform",function(){return"translate("+[textBox.x-5,textBox.y-5]+")";});});}
function angle(d){var a=(d.startAngle+d.endAngle)*90/Math.PI-90;return a>90?a-180:a;}
function arcTween(a){if(!donut)a.innerRadius=0;var i=d3.interpolate(this._current,a);this._current=i(0);return function(t){return arc(i(t));};}
function tweenPie(b){b.innerRadius=0;var i=d3.interpolate({startAngle:0,endAngle:0},b);return function(t){return arc(i(t));};}});return chart;}
chart.dispatch=dispatch;chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.values=function(_){if(!arguments.length)return getValues;getValues=_;return chart;};chart.x=function(_){if(!arguments.length)return getX;getX=_;return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=d3.functor(_);return chart;};chart.description=function(_){if(!arguments.length)return getDescription;getDescription=_;return chart;};chart.showLabels=function(_){if(!arguments.length)return showLabels;showLabels=_;return chart;};chart.labelSunbeamLayout=function(_){if(!arguments.length)return labelSunbeamLayout;labelSunbeamLayout=_;return chart;};chart.donutLabelsOutside=function(_){if(!arguments.length)return donutLabelsOutside;donutLabelsOutside=_;return chart;};chart.pieLabelsOutside=function(_){if(!arguments.length)return pieLabelsOutside;pieLabelsOutside=_;return chart;};chart.donut=function(_){if(!arguments.length)return donut;donut=_;return chart;};chart.donutRatio=function(_){if(!arguments.length)return donutRatio;donutRatio=_;return chart;};chart.startAngle=function(_){if(!arguments.length)return startAngle;startAngle=_;return chart;};chart.endAngle=function(_){if(!arguments.length)return endAngle;endAngle=_;return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.valueFormat=function(_){if(!arguments.length)return valueFormat;valueFormat=_;return chart;};chart.labelThreshold=function(_){if(!arguments.length)return labelThreshold;labelThreshold=_;return chart;};return chart;}
nv.models.pieChart=function(){var pie=nv.models.pie(),legend=nv.models.legend();var margin={top:30,right:20,bottom:20,left:20},width=null,height=null,showLegend=true,color=nv.utils.defaultColor(),tooltips=true,tooltip=function(key,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+'</p>'},state={},defaultState=null,noData="No Data Available.",dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState');var showTooltip=function(e,offsetElement){var tooltipLabel=pie.description()(e.point)||pie.x()(e.point)
var left=e.pos[0]+((offsetElement&&offsetElement.offsetLeft)||0),top=e.pos[1]+((offsetElement&&offsetElement.offsetTop)||0),y=pie.valueFormat()(pie.y()(e.point)),content=tooltip(tooltipLabel,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'n':'s',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection);};chart.container=this;state.disabled=data[0].map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data[0]||!data[0].length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
var wrap=container.selectAll('g.nv-wrap.nv-pieChart').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-pieChart').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-pieWrap');gEnter.append('g').attr('class','nv-legendWrap');if(showLegend){legend.width(availableWidth).key(pie.x());wrap.select('.nv-legendWrap').datum(pie.values()(data[0])).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
wrap.select('.nv-legendWrap').attr('transform','translate(0,'+(-margin.top)+')');}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');pie.width(availableWidth).height(availableHeight);var pieWrap=g.select('.nv-pieWrap').datum(data);d3.transition(pieWrap).call(pie);legend.dispatch.on('legendClick',function(d,i,that){d.disabled=!d.disabled;if(!pie.values()(data[0]).filter(function(d){return!d.disabled}).length){pie.values()(data[0]).map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data[0].map(function(d){return!!d.disabled});dispatch.stateChange(state);selection.transition().call(chart)});pie.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data[0].forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
selection.call(chart);});});return chart;}
pie.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.legend=legend;chart.dispatch=dispatch;chart.pie=pie;d3.rebind(chart,pie,'valueFormat','values','x','y','description','id','showLabels','donutLabelsOutside','pieLabelsOutside','donut','donutRatio','labelThreshold');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);pie.color(color);return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.scatter=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,color=nv.utils.defaultColor(),id=Math.floor(Math.random()*100000),x=d3.scale.linear(),y=d3.scale.linear(),z=d3.scale.linear(),getX=function(d){return d.x},getY=function(d){return d.y},getSize=function(d){return d.size||1},getShape=function(d){return d.shape||'circle'},onlyCircles=true,forceX=[],forceY=[],forceSize=[],interactive=true,pointActive=function(d){return!d.notActive},padData=false,padDataOuter=.1,clipEdge=false,clipVoronoi=true,clipRadius=function(){return 25},xDomain=null,yDomain=null,sizeDomain=null,sizeRange=null,singlePoint=false,dispatch=d3.dispatch('elementClick','elementMouseover','elementMouseout'),useVoronoi=true;var x0,y0,z0,timeoutID,needsUpdate=false;function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);data=data.map(function(series,i){series.values=series.values.map(function(point){point.series=i;return point;});return series;});var seriesData=(xDomain&&yDomain&&sizeDomain)?[]:d3.merge(data.map(function(d){return d.values.map(function(d,i){return{x:getX(d,i),y:getY(d,i),size:getSize(d,i)}})}));x.domain(xDomain||d3.extent(seriesData.map(function(d){return d.x;}).concat(forceX)))
if(padData&&data[0])
x.range([(availableWidth*padDataOuter+availableWidth)/(2*data[0].values.length),availableWidth-availableWidth*(1+padDataOuter)/(2*data[0].values.length)]);else
x.range([0,availableWidth]);y.domain(yDomain||d3.extent(seriesData.map(function(d){return d.y}).concat(forceY))).range([availableHeight,0]);z.domain(sizeDomain||d3.extent(seriesData.map(function(d){return d.size}).concat(forceSize))).range(sizeRange||[16,256]);if(x.domain()[0]===x.domain()[1]||y.domain()[0]===y.domain()[1])singlePoint=true;if(x.domain()[0]===x.domain()[1])
x.domain()[0]?x.domain([x.domain()[0]-x.domain()[0]*0.01,x.domain()[1]+x.domain()[1]*0.01]):x.domain([-1,1]);if(y.domain()[0]===y.domain()[1])
y.domain()[0]?y.domain([y.domain()[0]+y.domain()[0]*0.01,y.domain()[1]-y.domain()[1]*0.01]):y.domain([-1,1]);if(isNaN(x.domain()[0])){x.domain([-1,1]);}
if(isNaN(y.domain()[0])){y.domain([-1,1]);}
x0=x0||x;y0=y0||y;z0=z0||z;var wrap=container.selectAll('g.nv-wrap.nv-scatter').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-scatter nv-chart-'+id+(singlePoint?' nv-single-point':''));var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-groups');gEnter.append('g').attr('class','nv-point-paths');wrap.attr('transform','translate('+margin.left+','+margin.top+')');defsEnter.append('clipPath').attr('id','nv-edge-clip-'+id).append('rect');wrap.select('#nv-edge-clip-'+id+' rect').attr('width',availableWidth).attr('height',availableHeight);g.attr('clip-path',clipEdge?'url(#nv-edge-clip-'+id+')':'');function updateInteractiveLayer(){if(!interactive)return false;var eventElements;var vertices=d3.merge(data.map(function(group,groupIndex){return group.values.map(function(point,pointIndex){var pX=getX(point,pointIndex)+Math.random()*1e-10;var pY=getY(point,pointIndex)+Math.random()*1e-10;return[x(pX),y(pY),groupIndex,pointIndex,point];}).filter(function(pointArray,pointIndex){return pointActive(pointArray[4],pointIndex);})}));if(useVoronoi===true){if(clipVoronoi){var pointClipsEnter=wrap.select('defs').selectAll('.nv-point-clips').data([id]).enter();pointClipsEnter.append('clipPath').attr('class','nv-point-clips').attr('id','nv-points-clip-'+id);var pointClips=wrap.select('#nv-points-clip-'+id).selectAll('circle').data(vertices);pointClips.enter().append('circle').attr('r',clipRadius);pointClips.exit().remove();pointClips.attr('cx',function(d){return d[0]}).attr('cy',function(d){return d[1]});wrap.select('.nv-point-paths').attr('clip-path','url(#nv-points-clip-'+id+')');}
vertices.push([x.range()[0]-20,y.range()[0]-20,null,null]);vertices.push([x.range()[1]+20,y.range()[1]+20,null,null]);vertices.push([x.range()[0]-20,y.range()[0]+20,null,null]);vertices.push([x.range()[1]+20,y.range()[1]-20,null,null]);var bounds=d3.geom.polygon([[-10,-10],[-10,height+10],[width+10,height+10],[width+10,-10]]);var voronoi=d3.geom.voronoi(vertices).map(function(d,i){return{'data':bounds.clip(d),'series':vertices[i][2],'point':vertices[i][3]}});var pointPaths=wrap.select('.nv-point-paths').selectAll('path').data(voronoi);pointPaths.enter().append('path').attr('class',function(d,i){return'nv-path-'+i;});pointPaths.exit().remove();pointPaths.attr('d',function(d){if(d.data.length===0)
return'M 0 0'
else
return'M'+d.data.join('L')+'Z';});pointPaths.on('click',function(d){if(needsUpdate)return 0;var series=data[d.series],point=series.values[d.point];dispatch.elementClick({point:point,series:series,pos:[x(getX(point,d.point))+margin.left,y(getY(point,d.point))+margin.top],seriesIndex:d.series,pointIndex:d.point});}).on('mouseover',function(d){if(needsUpdate)return 0;var series=data[d.series],point=series.values[d.point];dispatch.elementMouseover({point:point,series:series,pos:[x(getX(point,d.point))+margin.left,y(getY(point,d.point))+margin.top],seriesIndex:d.series,pointIndex:d.point});}).on('mouseout',function(d,i){if(needsUpdate)return 0;var series=data[d.series],point=series.values[d.point];dispatch.elementMouseout({point:point,series:series,seriesIndex:d.series,pointIndex:d.point});});}else{wrap.select('.nv-groups').selectAll('.nv-group').selectAll('.nv-point').on('click',function(d,i){if(needsUpdate||!data[d.series])return 0;var series=data[d.series],point=series.values[i];dispatch.elementClick({point:point,series:series,pos:[x(getX(point,i))+margin.left,y(getY(point,i))+margin.top],seriesIndex:d.series,pointIndex:i});}).on('mouseover',function(d,i){if(needsUpdate||!data[d.series])return 0;var series=data[d.series],point=series.values[i];dispatch.elementMouseover({point:point,series:series,pos:[x(getX(point,i))+margin.left,y(getY(point,i))+margin.top],seriesIndex:d.series,pointIndex:i});}).on('mouseout',function(d,i){if(needsUpdate||!data[d.series])return 0;var series=data[d.series],point=series.values[i];dispatch.elementMouseout({point:point,series:series,seriesIndex:d.series,pointIndex:i});});}
needsUpdate=false;}
needsUpdate=true;var groups=wrap.select('.nv-groups').selectAll('.nv-group').data(function(d){return d},function(d){return d.key});groups.enter().append('g').style('stroke-opacity',1e-6).style('fill-opacity',1e-6);d3.transition(groups.exit()).style('stroke-opacity',1e-6).style('fill-opacity',1e-6).remove();groups.attr('class',function(d,i){return'nv-group nv-series-'+i}).classed('hover',function(d){return d.hover});d3.transition(groups).style('fill',function(d,i){return color(d,i)}).style('stroke',function(d,i){return color(d,i)}).style('stroke-opacity',1).style('fill-opacity',.5);if(onlyCircles){var points=groups.selectAll('circle.nv-point').data(function(d){return d.values});points.enter().append('circle').attr('cx',function(d,i){return x0(getX(d,i))}).attr('cy',function(d,i){return y0(getY(d,i))}).attr('r',function(d,i){return Math.sqrt(z(getSize(d,i))/Math.PI)});points.exit().remove();d3.transition(groups.exit().selectAll('path.nv-point')).attr('cx',function(d,i){return x(getX(d,i))}).attr('cy',function(d,i){return y(getY(d,i))}).remove();points.attr('class',function(d,i){return'nv-point nv-point-'+i});d3.transition(points).attr('cx',function(d,i){return x(getX(d,i))}).attr('cy',function(d,i){return y(getY(d,i))}).attr('r',function(d,i){return Math.sqrt(z(getSize(d,i))/Math.PI)});}else{var points=groups.selectAll('path.nv-point').data(function(d){return d.values});points.enter().append('path').attr('transform',function(d,i){return'translate('+x0(getX(d,i))+','+y0(getY(d,i))+')'}).attr('d',d3.svg.symbol().type(getShape).size(function(d,i){return z(getSize(d,i))}));points.exit().remove();d3.transition(groups.exit().selectAll('path.nv-point')).attr('transform',function(d,i){return'translate('+x(getX(d,i))+','+y(getY(d,i))+')'}).remove();points.attr('class',function(d,i){return'nv-point nv-point-'+i});d3.transition(points).attr('transform',function(d,i){return'translate('+x(getX(d,i))+','+y(getY(d,i))+')'}).attr('d',d3.svg.symbol().type(getShape).size(function(d,i){return z(getSize(d,i))}));}
clearTimeout(timeoutID);timeoutID=setTimeout(updateInteractiveLayer,300);x0=x.copy();y0=y.copy();z0=z.copy();});return chart;}
dispatch.on('elementMouseover.point',function(d){if(interactive)
d3.select('.nv-chart-'+id+' .nv-series-'+d.seriesIndex+' .nv-point-'+d.pointIndex).classed('hover',true);});dispatch.on('elementMouseout.point',function(d){if(interactive)
d3.select('.nv-chart-'+id+' .nv-series-'+d.seriesIndex+' .nv-point-'+d.pointIndex).classed('hover',false);});chart.dispatch=dispatch;chart.x=function(_){if(!arguments.length)return getX;getX=d3.functor(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=d3.functor(_);return chart;};chart.size=function(_){if(!arguments.length)return getSize;getSize=d3.functor(_);return chart;};chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.zScale=function(_){if(!arguments.length)return z;z=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.sizeDomain=function(_){if(!arguments.length)return sizeDomain;sizeDomain=_;return chart;};chart.sizeRange=function(_){if(!arguments.length)return sizeRange;sizeRange=_;return chart;};chart.forceX=function(_){if(!arguments.length)return forceX;forceX=_;return chart;};chart.forceY=function(_){if(!arguments.length)return forceY;forceY=_;return chart;};chart.forceSize=function(_){if(!arguments.length)return forceSize;forceSize=_;return chart;};chart.interactive=function(_){if(!arguments.length)return interactive;interactive=_;return chart;};chart.pointActive=function(_){if(!arguments.length)return pointActive;pointActive=_;return chart;};chart.padData=function(_){if(!arguments.length)return padData;padData=_;return chart;};chart.padDataOuter=function(_){if(!arguments.length)return padDataOuter;padDataOuter=_;return chart;};chart.clipEdge=function(_){if(!arguments.length)return clipEdge;clipEdge=_;return chart;};chart.clipVoronoi=function(_){if(!arguments.length)return clipVoronoi;clipVoronoi=_;return chart;};chart.useVoronoi=function(_){if(!arguments.length)return useVoronoi;useVoronoi=_;if(useVoronoi===false){clipVoronoi=false;}
return chart;};chart.clipRadius=function(_){if(!arguments.length)return clipRadius;clipRadius=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.shape=function(_){if(!arguments.length)return getShape;getShape=_;return chart;};chart.onlyCircles=function(_){if(!arguments.length)return onlyCircles;onlyCircles=_;return chart;};chart.id=function(_){if(!arguments.length)return id;id=_;return chart;};chart.singlePoint=function(_){if(!arguments.length)return singlePoint;singlePoint=_;return chart;};return chart;}
nv.models.scatterChart=function(){var scatter=nv.models.scatter(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend(),controls=nv.models.legend(),distX=nv.models.distribution(),distY=nv.models.distribution();var margin={top:30,right:20,bottom:50,left:75},width=null,height=null,color=nv.utils.defaultColor(),x=d3.fisheye?d3.fisheye.scale(d3.scale.linear).distortion(0):scatter.xScale(),y=d3.fisheye?d3.fisheye.scale(d3.scale.linear).distortion(0):scatter.yScale(),xPadding=0,yPadding=0,showDistX=false,showDistY=false,showLegend=true,showControls=!!d3.fisheye,fisheye=0,pauseFisheye=false,tooltips=true,tooltipX=function(key,x,y){return'<strong>'+x+'</strong>'},tooltipY=function(key,x,y){return'<strong>'+y+'</strong>'},tooltip=null,state={},defaultState=null,dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState'),noData="No Data Available.";scatter.xScale(x).yScale(y);xAxis.orient('bottom').tickPadding(10);yAxis.orient('left').tickPadding(10);distX.axis('x');distY.axis('y');var x0,y0;var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),leftX=e.pos[0]+(offsetElement.offsetLeft||0),topX=y.range()[0]+margin.top+(offsetElement.offsetTop||0),leftY=x.range()[0]+margin.left+(offsetElement.offsetLeft||0),topY=e.pos[1]+(offsetElement.offsetTop||0),xVal=xAxis.tickFormat()(scatter.x()(e.point,e.pointIndex)),yVal=yAxis.tickFormat()(scatter.y()(e.point,e.pointIndex));if(tooltipX!=null)
nv.tooltip.show([leftX,topX],tooltipX(e.series.key,xVal,yVal,e,chart),'n',1,offsetElement,'x-nvtooltip');if(tooltipY!=null)
nv.tooltip.show([leftY,topY],tooltipY(e.series.key,xVal,yVal,e,chart),'e',1,offsetElement,'y-nvtooltip');if(tooltip!=null)
nv.tooltip.show([left,top],tooltip(e.series.key,xVal,yVal,e,chart),e.value<0?'n':'s',null,offsetElement);};var controlsData=[{key:'Magnify',disabled:true}];function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x0=x0||x;y0=y0||y;var wrap=container.selectAll('g.nv-wrap.nv-scatterChart').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-scatterChart nv-chart-'+scatter.id());var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('rect').attr('class','nvd3 nv-background');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-scatterWrap');gEnter.append('g').attr('class','nv-distWrap');gEnter.append('g').attr('class','nv-legendWrap');gEnter.append('g').attr('class','nv-controlsWrap');if(showLegend){legend.width(availableWidth/2);wrap.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
wrap.select('.nv-legendWrap').attr('transform','translate('+(availableWidth/2)+','+(-margin.top)+')');}
if(showControls){controls.width(180).color(['#444']);g.select('.nv-controlsWrap').datum(controlsData).attr('transform','translate(0,'+(-margin.top)+')').call(controls);}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');scatter.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}))
wrap.select('.nv-scatterWrap').datum(data.filter(function(d){return!d.disabled})).call(scatter);if(xPadding){var xRange=x.domain()[1]-x.domain()[0];x.domain([x.domain()[0]-(xPadding*xRange),x.domain()[1]+(xPadding*xRange)]);}
if(yPadding){var yRange=y.domain()[1]-y.domain()[0];y.domain([y.domain()[0]-(yPadding*yRange),y.domain()[1]+(yPadding*yRange)]);}
xAxis.scale(x).ticks(xAxis.ticks()&&xAxis.ticks().length?xAxis.ticks():availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+y.range()[0]+')').call(xAxis);yAxis.scale(y).ticks(yAxis.ticks()&&yAxis.ticks().length?yAxis.ticks():availableHeight/36).tickSize(-availableWidth,0);g.select('.nv-y.nv-axis').call(yAxis);if(showDistX){distX.getData(scatter.x()).scale(x).width(availableWidth).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}));gEnter.select('.nv-distWrap').append('g').attr('class','nv-distributionX');g.select('.nv-distributionX').attr('transform','translate(0,'+y.range()[0]+')').datum(data.filter(function(d){return!d.disabled})).call(distX);}
if(showDistY){distY.getData(scatter.y()).scale(y).width(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}));gEnter.select('.nv-distWrap').append('g').attr('class','nv-distributionY');g.select('.nv-distributionY').attr('transform','translate(-'+distY.size()+',0)').datum(data.filter(function(d){return!d.disabled})).call(distY);}
if(d3.fisheye){g.select('.nv-background').attr('width',availableWidth).attr('height',availableHeight);g.select('.nv-background').on('mousemove',updateFisheye);g.select('.nv-background').on('click',function(){pauseFisheye=!pauseFisheye;});scatter.dispatch.on('elementClick.freezeFisheye',function(){pauseFisheye=!pauseFisheye;});}
function updateFisheye(){if(pauseFisheye){g.select('.nv-point-paths').style('pointer-events','all');return false;}
g.select('.nv-point-paths').style('pointer-events','none');var mouse=d3.mouse(this);x.distortion(fisheye).focus(mouse[0]);y.distortion(fisheye).focus(mouse[1]);g.select('.nv-scatterWrap').call(scatter);g.select('.nv-x.nv-axis').call(xAxis);g.select('.nv-y.nv-axis').call(yAxis);g.select('.nv-distributionX').datum(data.filter(function(d){return!d.disabled})).call(distX);g.select('.nv-distributionY').datum(data.filter(function(d){return!d.disabled})).call(distY);}
controls.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;fisheye=d.disabled?0:2.5;g.select('.nv-background').style('pointer-events',d.disabled?'none':'all');g.select('.nv-point-paths').style('pointer-events',d.disabled?'all':'none');if(d.disabled){x.distortion(fisheye).focus(0);y.distortion(fisheye).focus(0);g.select('.nv-scatterWrap').call(scatter);g.select('.nv-x.nv-axis').call(xAxis);g.select('.nv-y.nv-axis').call(yAxis);}else{pauseFisheye=false;}
chart(selection);});legend.dispatch.on('legendClick',function(d,i,that){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);chart(selection);});scatter.dispatch.on('elementMouseover.tooltip',function(e){d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-distx-'+e.pointIndex).attr('y1',e.pos[1]-availableHeight);d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-disty-'+e.pointIndex).attr('x2',e.pos[0]+distX.size());e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
selection.call(chart);});x0=x.copy();y0=y.copy();});return chart;}
scatter.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-distx-'+e.pointIndex).attr('y1',0);d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-disty-'+e.pointIndex).attr('x2',distY.size());});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.scatter=scatter;chart.legend=legend;chart.controls=controls;chart.xAxis=xAxis;chart.yAxis=yAxis;chart.distX=distX;chart.distY=distY;d3.rebind(chart,scatter,'id','interactive','pointActive','x','y','shape','size','xScale','yScale','zScale','xDomain','yDomain','sizeDomain','sizeRange','forceX','forceY','forceSize','clipVoronoi','clipRadius','useVoronoi');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);distX.color(color);distY.color(color);return chart;};chart.showDistX=function(_){if(!arguments.length)return showDistX;showDistX=_;return chart;};chart.showDistY=function(_){if(!arguments.length)return showDistY;showDistY=_;return chart;};chart.showControls=function(_){if(!arguments.length)return showControls;showControls=_;return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.fisheye=function(_){if(!arguments.length)return fisheye;fisheye=_;return chart;};chart.xPadding=function(_){if(!arguments.length)return xPadding;xPadding=_;return chart;};chart.yPadding=function(_){if(!arguments.length)return yPadding;yPadding=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.tooltipXContent=function(_){if(!arguments.length)return tooltipX;tooltipX=_;return chart;};chart.tooltipYContent=function(_){if(!arguments.length)return tooltipY;tooltipY=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.scatterPlusLineChart=function(){var scatter=nv.models.scatter(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend(),controls=nv.models.legend(),distX=nv.models.distribution(),distY=nv.models.distribution();var margin={top:30,right:20,bottom:50,left:75},width=null,height=null,color=nv.utils.defaultColor(),x=d3.fisheye?d3.fisheye.scale(d3.scale.linear).distortion(0):scatter.xScale(),y=d3.fisheye?d3.fisheye.scale(d3.scale.linear).distortion(0):scatter.yScale(),showDistX=false,showDistY=false,showLegend=true,showControls=!!d3.fisheye,fisheye=0,pauseFisheye=false,tooltips=true,tooltipX=function(key,x,y){return'<strong>'+x+'</strong>'},tooltipY=function(key,x,y){return'<strong>'+y+'</strong>'},tooltip=function(key,x,y,date){return'<h3>'+key+'</h3>'
+'<p>'+date+'</p>'},state={},defaultState=null,dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState'),noData="No Data Available.";scatter.xScale(x).yScale(y);xAxis.orient('bottom').tickPadding(10);yAxis.orient('left').tickPadding(10);distX.axis('x');distY.axis('y');var x0,y0;var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),leftX=e.pos[0]+(offsetElement.offsetLeft||0),topX=y.range()[0]+margin.top+(offsetElement.offsetTop||0),leftY=x.range()[0]+margin.left+(offsetElement.offsetLeft||0),topY=e.pos[1]+(offsetElement.offsetTop||0),xVal=xAxis.tickFormat()(scatter.x()(e.point,e.pointIndex)),yVal=yAxis.tickFormat()(scatter.y()(e.point,e.pointIndex));if(tooltipX!=null)
nv.tooltip.show([leftX,topX],tooltipX(e.series.key,xVal,yVal,e,chart),'n',1,offsetElement,'x-nvtooltip');if(tooltipY!=null)
nv.tooltip.show([leftY,topY],tooltipY(e.series.key,xVal,yVal,e,chart),'e',1,offsetElement,'y-nvtooltip');if(tooltip!=null)
nv.tooltip.show([left,top],tooltip(e.series.key,xVal,yVal,e.point.tooltip,e,chart),e.value<0?'n':'s',null,offsetElement);};var controlsData=[{key:'Magnify',disabled:true}];function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=scatter.xScale();y=scatter.yScale();x0=x0||x;y0=y0||y;var wrap=container.selectAll('g.nv-wrap.nv-scatterChart').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-scatterChart nv-chart-'+scatter.id());var gEnter=wrapEnter.append('g');var g=wrap.select('g')
gEnter.append('rect').attr('class','nvd3 nv-background')
gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-scatterWrap');gEnter.append('g').attr('class','nv-regressionLinesWrap');gEnter.append('g').attr('class','nv-distWrap');gEnter.append('g').attr('class','nv-legendWrap');gEnter.append('g').attr('class','nv-controlsWrap');wrap.attr('transform','translate('+margin.left+','+margin.top+')');if(showLegend){legend.width(availableWidth/2);wrap.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
wrap.select('.nv-legendWrap').attr('transform','translate('+(availableWidth/2)+','+(-margin.top)+')');}
if(showControls){controls.width(180).color(['#444']);g.select('.nv-controlsWrap').datum(controlsData).attr('transform','translate(0,'+(-margin.top)+')').call(controls);}
scatter.width(availableWidth).height(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}))
wrap.select('.nv-scatterWrap').datum(data.filter(function(d){return!d.disabled})).call(scatter);wrap.select('.nv-regressionLinesWrap').attr('clip-path','url(#nv-edge-clip-'+scatter.id()+')');var regWrap=wrap.select('.nv-regressionLinesWrap').selectAll('.nv-regLines').data(function(d){return d});var reglines=regWrap.enter().append('g').attr('class','nv-regLines').append('line').attr('class','nv-regLine').style('stroke-opacity',0);regWrap.selectAll('.nv-regLines line').attr('x1',x.range()[0]).attr('x2',x.range()[1]).attr('y1',function(d,i){return y(x.domain()[0]*d.slope+d.intercept)}).attr('y2',function(d,i){return y(x.domain()[1]*d.slope+d.intercept)}).style('stroke',function(d,i,j){return color(d,j)}).style('stroke-opacity',function(d,i){return(d.disabled||typeof d.slope==='undefined'||typeof d.intercept==='undefined')?0:1});xAxis.scale(x).ticks(xAxis.ticks()?xAxis.ticks():availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+y.range()[0]+')').call(xAxis);yAxis.scale(y).ticks(yAxis.ticks()?yAxis.ticks():availableHeight/36).tickSize(-availableWidth,0);g.select('.nv-y.nv-axis').call(yAxis);if(showDistX){distX.getData(scatter.x()).scale(x).width(availableWidth).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}));gEnter.select('.nv-distWrap').append('g').attr('class','nv-distributionX');g.select('.nv-distributionX').attr('transform','translate(0,'+y.range()[0]+')').datum(data.filter(function(d){return!d.disabled})).call(distX);}
if(showDistY){distY.getData(scatter.y()).scale(y).width(availableHeight).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}));gEnter.select('.nv-distWrap').append('g').attr('class','nv-distributionY');g.select('.nv-distributionY').attr('transform','translate(-'+distY.size()+',0)').datum(data.filter(function(d){return!d.disabled})).call(distY);}
if(d3.fisheye){g.select('.nv-background').attr('width',availableWidth).attr('height',availableHeight);g.select('.nv-background').on('mousemove',updateFisheye);g.select('.nv-background').on('click',function(){pauseFisheye=!pauseFisheye;});scatter.dispatch.on('elementClick.freezeFisheye',function(){pauseFisheye=!pauseFisheye;});}
function updateFisheye(){if(pauseFisheye){g.select('.nv-point-paths').style('pointer-events','all');return false;}
g.select('.nv-point-paths').style('pointer-events','none');var mouse=d3.mouse(this);x.distortion(fisheye).focus(mouse[0]);y.distortion(fisheye).focus(mouse[1]);g.select('.nv-scatterWrap').datum(data.filter(function(d){return!d.disabled})).call(scatter);g.select('.nv-x.nv-axis').call(xAxis);g.select('.nv-y.nv-axis').call(yAxis);g.select('.nv-distributionX').datum(data.filter(function(d){return!d.disabled})).call(distX);g.select('.nv-distributionY').datum(data.filter(function(d){return!d.disabled})).call(distY);}
controls.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;fisheye=d.disabled?0:2.5;g.select('.nv-background').style('pointer-events',d.disabled?'none':'all');g.select('.nv-point-paths').style('pointer-events',d.disabled?'all':'none');if(d.disabled){x.distortion(fisheye).focus(0);y.distortion(fisheye).focus(0);g.select('.nv-scatterWrap').call(scatter);g.select('.nv-x.nv-axis').call(xAxis);g.select('.nv-y.nv-axis').call(yAxis);}else{pauseFisheye=false;}
chart(selection);});legend.dispatch.on('legendClick',function(d,i,that){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;wrap.selectAll('.nv-series').classed('disabled',false);return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);chart(selection);});scatter.dispatch.on('elementMouseover.tooltip',function(e){d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-distx-'+e.pointIndex).attr('y1',e.pos[1]-availableHeight);d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-disty-'+e.pointIndex).attr('x2',e.pos[0]+distX.size());e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top];dispatch.tooltipShow(e);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
selection.call(chart);});x0=x.copy();y0=y.copy();});return chart;}
scatter.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-distx-'+e.pointIndex).attr('y1',0);d3.select('.nv-chart-'+scatter.id()+' .nv-series-'+e.seriesIndex+' .nv-disty-'+e.pointIndex).attr('x2',distY.size());});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.scatter=scatter;chart.legend=legend;chart.controls=controls;chart.xAxis=xAxis;chart.yAxis=yAxis;chart.distX=distX;chart.distY=distY;d3.rebind(chart,scatter,'id','interactive','pointActive','x','y','shape','size','xScale','yScale','zScale','xDomain','yDomain','sizeDomain','sizeRange','forceX','forceY','forceSize','clipVoronoi','clipRadius','useVoronoi');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);distX.color(color);distY.color(color);return chart;};chart.showDistX=function(_){if(!arguments.length)return showDistX;showDistX=_;return chart;};chart.showDistY=function(_){if(!arguments.length)return showDistY;showDistY=_;return chart;};chart.showControls=function(_){if(!arguments.length)return showControls;showControls=_;return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.fisheye=function(_){if(!arguments.length)return fisheye;fisheye=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.tooltipXContent=function(_){if(!arguments.length)return tooltipX;tooltipX=_;return chart;};chart.tooltipYContent=function(_){if(!arguments.length)return tooltipY;tooltipY=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.sparkline=function(){var margin={top:2,right:0,bottom:2,left:0},width=400,height=32,animate=true,x=d3.scale.linear(),y=d3.scale.linear(),getX=function(d){return d.x},getY=function(d){return d.y},color=nv.utils.getColor(['#000']),xDomain,yDomain;function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);x.domain(xDomain||d3.extent(data,getX)).range([0,availableWidth]);y.domain(yDomain||d3.extent(data,getY)).range([availableHeight,0]);var wrap=container.selectAll('g.nv-wrap.nv-sparkline').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-sparkline');var gEnter=wrapEnter.append('g');var g=wrap.select('g');wrap.attr('transform','translate('+margin.left+','+margin.top+')')
var paths=wrap.selectAll('path').data(function(d){return[d]});paths.enter().append('path');paths.exit().remove();paths.style('stroke',function(d,i){return d.color||color(d,i)}).attr('d',d3.svg.line().x(function(d,i){return x(getX(d,i))}).y(function(d,i){return y(getY(d,i))}));var points=wrap.selectAll('circle.nv-point').data(function(data){var yValues=data.map(function(d,i){return getY(d,i);});function pointIndex(index){if(index!=-1){var result=data[index];result.pointIndex=index;return result;}else{return null;}}
var maxPoint=pointIndex(yValues.lastIndexOf(y.domain()[1])),minPoint=pointIndex(yValues.indexOf(y.domain()[0])),currentPoint=pointIndex(yValues.length-1);return[minPoint,maxPoint,currentPoint].filter(function(d){return d!=null;});});points.enter().append('circle');points.exit().remove();points.attr('cx',function(d,i){return x(getX(d,d.pointIndex))}).attr('cy',function(d,i){return y(getY(d,d.pointIndex))}).attr('r',2).attr('class',function(d,i){return getX(d,d.pointIndex)==x.domain()[1]?'nv-point nv-currentValue':getY(d,d.pointIndex)==y.domain()[0]?'nv-point nv-minValue':'nv-point nv-maxValue'});});return chart;}
chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.x=function(_){if(!arguments.length)return getX;getX=d3.functor(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=d3.functor(_);return chart;};chart.xScale=function(_){if(!arguments.length)return x;x=_;return chart;};chart.yScale=function(_){if(!arguments.length)return y;y=_;return chart;};chart.xDomain=function(_){if(!arguments.length)return xDomain;xDomain=_;return chart;};chart.yDomain=function(_){if(!arguments.length)return yDomain;yDomain=_;return chart;};chart.animate=function(_){if(!arguments.length)return animate;animate=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};return chart;}
nv.models.sparklinePlus=function(){var sparkline=nv.models.sparkline();var margin={top:15,right:100,bottom:10,left:50},width=null,height=null,x,y,index=[],paused=false,xTickFormat=d3.format(',r'),yTickFormat=d3.format(',.2f'),showValue=true,alignValue=true,rightAlignValue=false,noData="No Data Available.";function chart(selection){selection.each(function(data){var container=d3.select(this);var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;if(!data||!data.length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
var currentValue=sparkline.y()(data[data.length-1],data.length-1);x=sparkline.xScale();y=sparkline.yScale();var wrap=container.selectAll('g.nv-wrap.nv-sparklineplus').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-sparklineplus');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-sparklineWrap');gEnter.append('g').attr('class','nv-valueWrap');gEnter.append('g').attr('class','nv-hoverArea');wrap.attr('transform','translate('+margin.left+','+margin.top+')');var sparklineWrap=g.select('.nv-sparklineWrap');sparkline.width(availableWidth).height(availableHeight);sparklineWrap.call(sparkline);var valueWrap=g.select('.nv-valueWrap');var value=valueWrap.selectAll('.nv-currentValue').data([currentValue]);value.enter().append('text').attr('class','nv-currentValue').attr('dx',rightAlignValue?-8:8).attr('dy','.9em').style('text-anchor',rightAlignValue?'end':'start');value.attr('x',availableWidth+(rightAlignValue?margin.right:0)).attr('y',alignValue?function(d){return y(d)}:0).style('fill',sparkline.color()(data[data.length-1],data.length-1)).text(yTickFormat(currentValue));gEnter.select('.nv-hoverArea').append('rect').on('mousemove',sparklineHover).on('click',function(){paused=!paused}).on('mouseout',function(){index=[];updateValueLine();});g.select('.nv-hoverArea rect').attr('transform',function(d){return'translate('+-margin.left+','+-margin.top+')'}).attr('width',availableWidth+margin.left+margin.right).attr('height',availableHeight+margin.top);function updateValueLine(){if(paused)return;var hoverValue=g.selectAll('.nv-hoverValue').data(index)
var hoverEnter=hoverValue.enter().append('g').attr('class','nv-hoverValue').style('stroke-opacity',0).style('fill-opacity',0);hoverValue.exit().transition().duration(250).style('stroke-opacity',0).style('fill-opacity',0).remove();hoverValue.attr('transform',function(d){return'translate('+x(sparkline.x()(data[d],d))+',0)'}).transition().duration(250).style('stroke-opacity',1).style('fill-opacity',1);if(!index.length)return;hoverEnter.append('line').attr('x1',0).attr('y1',-margin.top).attr('x2',0).attr('y2',availableHeight);hoverEnter.append('text').attr('class','nv-xValue').attr('x',-6).attr('y',-margin.top).attr('text-anchor','end').attr('dy','.9em')
g.select('.nv-hoverValue .nv-xValue').text(xTickFormat(sparkline.x()(data[index[0]],index[0])));hoverEnter.append('text').attr('class','nv-yValue').attr('x',6).attr('y',-margin.top).attr('text-anchor','start').attr('dy','.9em')
g.select('.nv-hoverValue .nv-yValue').text(yTickFormat(sparkline.y()(data[index[0]],index[0])));}
function sparklineHover(){if(paused)return;var pos=d3.mouse(this)[0]-margin.left;function getClosestIndex(data,x){var distance=Math.abs(sparkline.x()(data[0],0)-x);var closestIndex=0;for(var i=0;i<data.length;i++){if(Math.abs(sparkline.x()(data[i],i)-x)<distance){distance=Math.abs(sparkline.x()(data[i],i)-x);closestIndex=i;}}
return closestIndex;}
index=[getClosestIndex(data,Math.round(x.invert(pos)))];updateValueLine();}});return chart;}
chart.sparkline=sparkline;d3.rebind(chart,sparkline,'x','y','xScale','yScale','color');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.xTickFormat=function(_){if(!arguments.length)return xTickFormat;xTickFormat=_;return chart;};chart.yTickFormat=function(_){if(!arguments.length)return yTickFormat;yTickFormat=_;return chart;};chart.showValue=function(_){if(!arguments.length)return showValue;showValue=_;return chart;};chart.alignValue=function(_){if(!arguments.length)return alignValue;alignValue=_;return chart;};chart.rightAlignValue=function(_){if(!arguments.length)return rightAlignValue;rightAlignValue=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};return chart;}
nv.models.stackedArea=function(){var margin={top:0,right:0,bottom:0,left:0},width=960,height=500,color=nv.utils.defaultColor(),id=Math.floor(Math.random()*100000),getX=function(d){return d.x},getY=function(d){return d.y},style='stack',offset='zero',order='default',interpolate='linear',clipEdge=false,x,y,scatter=nv.models.scatter(),dispatch=d3.dispatch('tooltipShow','tooltipHide','areaClick','areaMouseover','areaMouseout');scatter.size(2.2).sizeDomain([2.2,2.2]);function chart(selection){selection.each(function(data){var availableWidth=width-margin.left-margin.right,availableHeight=height-margin.top-margin.bottom,container=d3.select(this);x=scatter.xScale();y=scatter.yScale();data=data.map(function(aseries,i){aseries.values=aseries.values.map(function(d,j){d.index=j;d.stackedY=aseries.disabled?0:getY(d,j);return d;})
return aseries;});data=d3.layout.stack().order(order).offset(offset).values(function(d){return d.values}).x(getX).y(function(d){return d.stackedY}).out(function(d,y0,y){d.display={y:y,y0:y0};})
(data);var wrap=container.selectAll('g.nv-wrap.nv-stackedarea').data([data]);var wrapEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-stackedarea');var defsEnter=wrapEnter.append('defs');var gEnter=wrapEnter.append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-areaWrap');gEnter.append('g').attr('class','nv-scatterWrap');wrap.attr('transform','translate('+margin.left+','+margin.top+')');scatter.width(availableWidth).height(availableHeight).x(getX).y(function(d){return d.display.y+d.display.y0}).forceY([0]).color(data.map(function(d,i){return d.color||color(d,i);}).filter(function(d,i){return!data[i].disabled}));var scatterWrap=g.select('.nv-scatterWrap').datum(data.filter(function(d){return!d.disabled}))
scatterWrap.call(scatter);defsEnter.append('clipPath').attr('id','nv-edge-clip-'+id).append('rect');wrap.select('#nv-edge-clip-'+id+' rect').attr('width',availableWidth).attr('height',availableHeight);g.attr('clip-path',clipEdge?'url(#nv-edge-clip-'+id+')':'');var area=d3.svg.area().x(function(d,i){return x(getX(d,i))}).y0(function(d){return y(d.display.y0)}).y1(function(d){return y(d.display.y+d.display.y0)}).interpolate(interpolate);var zeroArea=d3.svg.area().x(function(d,i){return x(getX(d,i))}).y0(function(d){return y(d.display.y0)}).y1(function(d){return y(d.display.y0)});var path=g.select('.nv-areaWrap').selectAll('path.nv-area').data(function(d){return d});path.enter().append('path').attr('class',function(d,i){return'nv-area nv-area-'+i}).on('mouseover',function(d,i){d3.select(this).classed('hover',true);dispatch.areaMouseover({point:d,series:d.key,pos:[d3.event.pageX,d3.event.pageY],seriesIndex:i});}).on('mouseout',function(d,i){d3.select(this).classed('hover',false);dispatch.areaMouseout({point:d,series:d.key,pos:[d3.event.pageX,d3.event.pageY],seriesIndex:i});}).on('click',function(d,i){d3.select(this).classed('hover',false);dispatch.areaClick({point:d,series:d.key,pos:[d3.event.pageX,d3.event.pageY],seriesIndex:i});})
path.exit().attr('d',function(d,i){return zeroArea(d.values,i)}).remove();path.style('fill',function(d,i){return d.color||color(d,i)}).style('stroke',function(d,i){return d.color||color(d,i)});path.attr('d',function(d,i){return area(d.values,i)})
scatter.dispatch.on('elementMouseover.area',function(e){g.select('.nv-chart-'+id+' .nv-area-'+e.seriesIndex).classed('hover',true);});scatter.dispatch.on('elementMouseout.area',function(e){g.select('.nv-chart-'+id+' .nv-area-'+e.seriesIndex).classed('hover',false);});});return chart;}
scatter.dispatch.on('elementClick.area',function(e){dispatch.areaClick(e);})
scatter.dispatch.on('elementMouseover.tooltip',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top],dispatch.tooltipShow(e);});scatter.dispatch.on('elementMouseout.tooltip',function(e){dispatch.tooltipHide(e);});chart.dispatch=dispatch;chart.scatter=scatter;d3.rebind(chart,scatter,'interactive','size','xScale','yScale','zScale','xDomain','yDomain','sizeDomain','forceX','forceY','forceSize','clipVoronoi','clipRadius');chart.x=function(_){if(!arguments.length)return getX;getX=d3.functor(_);return chart;};chart.y=function(_){if(!arguments.length)return getY;getY=d3.functor(_);return chart;}
chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return width;width=_;return chart;};chart.height=function(_){if(!arguments.length)return height;height=_;return chart;};chart.clipEdge=function(_){if(!arguments.length)return clipEdge;clipEdge=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);return chart;};chart.offset=function(_){if(!arguments.length)return offset;offset=_;return chart;};chart.order=function(_){if(!arguments.length)return order;order=_;return chart;};chart.style=function(_){if(!arguments.length)return style;style=_;switch(style){case'stack':chart.offset('zero');chart.order('default');break;case'stream':chart.offset('wiggle');chart.order('inside-out');break;case'stream-center':chart.offset('silhouette');chart.order('inside-out');break;case'expand':chart.offset('expand');chart.order('default');break;}
return chart;};chart.interpolate=function(_){if(!arguments.length)return interpolate;interpolate=_;return interpolate;};return chart;}
nv.models.stackedAreaChart=function(){var stacked=nv.models.stackedArea(),xAxis=nv.models.axis(),yAxis=nv.models.axis(),legend=nv.models.legend(),controls=nv.models.legend();var margin={top:30,right:25,bottom:50,left:60},width=null,height=null,color=nv.utils.defaultColor(),showControls=true,showLegend=true,tooltips=true,tooltip=function(key,x,y,e,graph){return'<h3>'+key+'</h3>'+'<p>'+y+' on '+x+'</p>'},x,y,yAxisTickFormat=d3.format(',.2f'),state={style:stacked.style()},defaultState=null,noData='No Data Available.',dispatch=d3.dispatch('tooltipShow','tooltipHide','stateChange','changeState'),controlWidth=250;xAxis.orient('bottom').tickPadding(7);yAxis.orient('left');stacked.scatter.pointActive(function(d){return!!Math.round(stacked.y()(d)*100);});var showTooltip=function(e,offsetElement){var left=e.pos[0]+(offsetElement.offsetLeft||0),top=e.pos[1]+(offsetElement.offsetTop||0),x=xAxis.tickFormat()(stacked.x()(e.point,e.pointIndex)),y=yAxis.tickFormat()(stacked.y()(e.point,e.pointIndex)),content=tooltip(e.series.key,x,y,e,chart);nv.tooltip.show([left,top],content,e.value<0?'n':'s',null,offsetElement);};function chart(selection){selection.each(function(data){var container=d3.select(this),that=this;var availableWidth=(width||parseInt(container.style('width'))||960)
-margin.left-margin.right,availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;chart.update=function(){chart(selection)};chart.container=this;state.disabled=data.map(function(d){return!!d.disabled});if(!defaultState){var key;defaultState={};for(key in state){if(state[key]instanceof Array)
defaultState[key]=state[key].slice(0);else
defaultState[key]=state[key];}}
if(!data||!data.length||!data.filter(function(d){return d.values.length}).length){var noDataText=container.selectAll('.nv-noData').data([noData]);noDataText.enter().append('text').attr('class','nvd3 nv-noData').attr('dy','-.7em').style('text-anchor','middle');noDataText.attr('x',margin.left+availableWidth/2).attr('y',margin.top+availableHeight/2).text(function(d){return d});return chart;}else{container.selectAll('.nv-noData').remove();}
x=stacked.xScale();y=stacked.yScale();var wrap=container.selectAll('g.nv-wrap.nv-stackedAreaChart').data([data]);var gEnter=wrap.enter().append('g').attr('class','nvd3 nv-wrap nv-stackedAreaChart').append('g');var g=wrap.select('g');gEnter.append('g').attr('class','nv-x nv-axis');gEnter.append('g').attr('class','nv-y nv-axis');gEnter.append('g').attr('class','nv-stackedWrap');gEnter.append('g').attr('class','nv-legendWrap');gEnter.append('g').attr('class','nv-controlsWrap');if(showLegend){legend.width(availableWidth-controlWidth);g.select('.nv-legendWrap').datum(data).call(legend);if(margin.top!=legend.height()){margin.top=legend.height();availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.nv-legendWrap').attr('transform','translate('+controlWidth+','+(-margin.top)+')');}
if(showControls){var controlsData=[{key:'Stacked',disabled:stacked.offset()!='zero'},{key:'Stream',disabled:stacked.offset()!='wiggle'},{key:'Expanded',disabled:stacked.offset()!='expand'}];controls.width(controlWidth).color(['#444','#444','#444']);g.select('.nv-controlsWrap').datum(controlsData).call(controls);if(margin.top!=Math.max(controls.height(),legend.height())){margin.top=Math.max(controls.height(),legend.height());availableHeight=(height||parseInt(container.style('height'))||400)
-margin.top-margin.bottom;}
g.select('.nv-controlsWrap').attr('transform','translate(0,'+(-margin.top)+')');}
wrap.attr('transform','translate('+margin.left+','+margin.top+')');stacked.width(availableWidth).height(availableHeight)
var stackedWrap=g.select('.nv-stackedWrap').datum(data);stackedWrap.call(stacked);xAxis.scale(x).ticks(availableWidth/100).tickSize(-availableHeight,0);g.select('.nv-x.nv-axis').attr('transform','translate(0,'+availableHeight+')');g.select('.nv-x.nv-axis').transition().duration(0).call(xAxis);yAxis.scale(y).ticks(stacked.offset()=='wiggle'?0:availableHeight/36).tickSize(-availableWidth,0).setTickFormat(stacked.offset()=='expand'?d3.format('%'):yAxisTickFormat);g.select('.nv-y.nv-axis').transition().duration(0).call(yAxis);stacked.dispatch.on('areaClick.toggle',function(e){if(data.filter(function(d){return!d.disabled}).length===1)
data=data.map(function(d){d.disabled=false;return d});else
data=data.map(function(d,i){d.disabled=(i!=e.seriesIndex);return d});state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);chart(selection);});legend.dispatch.on('legendClick',function(d,i){d.disabled=!d.disabled;if(!data.filter(function(d){return!d.disabled}).length){data.map(function(d){d.disabled=false;return d;});}
state.disabled=data.map(function(d){return!!d.disabled});dispatch.stateChange(state);chart(selection);});controls.dispatch.on('legendClick',function(d,i){if(!d.disabled)return;controlsData=controlsData.map(function(s){s.disabled=true;return s;});d.disabled=false;switch(d.key){case'Stacked':stacked.style('stack');break;case'Stream':stacked.style('stream');break;case'Expanded':stacked.style('expand');break;}
state.style=stacked.style();dispatch.stateChange(state);chart(selection);});dispatch.on('tooltipShow',function(e){if(tooltips)showTooltip(e,that.parentNode);});dispatch.on('changeState',function(e){if(typeof e.disabled!=='undefined'){data.forEach(function(series,i){series.disabled=e.disabled[i];});state.disabled=e.disabled;}
if(typeof e.style!=='undefined'){stacked.style(e.style);}
selection.call(chart);});});return chart;}
stacked.dispatch.on('tooltipShow',function(e){e.pos=[e.pos[0]+margin.left,e.pos[1]+margin.top],dispatch.tooltipShow(e);});stacked.dispatch.on('tooltipHide',function(e){dispatch.tooltipHide(e);});dispatch.on('tooltipHide',function(){if(tooltips)nv.tooltip.cleanup();});chart.dispatch=dispatch;chart.stacked=stacked;chart.legend=legend;chart.controls=controls;chart.xAxis=xAxis;chart.yAxis=yAxis;d3.rebind(chart,stacked,'x','y','size','xScale','yScale','xDomain','yDomain','sizeDomain','interactive','offset','order','style','clipEdge','forceX','forceY','forceSize','interpolate');chart.margin=function(_){if(!arguments.length)return margin;margin.top=typeof _.top!='undefined'?_.top:margin.top;margin.right=typeof _.right!='undefined'?_.right:margin.right;margin.bottom=typeof _.bottom!='undefined'?_.bottom:margin.bottom;margin.left=typeof _.left!='undefined'?_.left:margin.left;return chart;};chart.width=function(_){if(!arguments.length)return getWidth;width=_;return chart;};chart.height=function(_){if(!arguments.length)return getHeight;height=_;return chart;};chart.color=function(_){if(!arguments.length)return color;color=nv.utils.getColor(_);legend.color(color);stacked.color(color);return chart;};chart.showControls=function(_){if(!arguments.length)return showControls;showControls=_;return chart;};chart.showLegend=function(_){if(!arguments.length)return showLegend;showLegend=_;return chart;};chart.tooltip=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.tooltips=function(_){if(!arguments.length)return tooltips;tooltips=_;return chart;};chart.tooltipContent=function(_){if(!arguments.length)return tooltip;tooltip=_;return chart;};chart.state=function(_){if(!arguments.length)return state;state=_;return chart;};chart.defaultState=function(_){if(!arguments.length)return defaultState;defaultState=_;return chart;};chart.noData=function(_){if(!arguments.length)return noData;noData=_;return chart;};yAxis.setTickFormat=yAxis.tickFormat;yAxis.tickFormat=function(_){if(!arguments.length)return yAxisTickFormat;yAxisTickFormat=_;return yAxis;};return chart;}})();

(function(){var nvtooltip=window.nv.tooltip={};nvtooltip.show=function(pos,content,gravity,dist,parentContainer,classes){var container=document.createElement('div');container.className='nvtooltip '+(classes?classes:'xy-tooltip');gravity=gravity||'s';dist=dist||20;var body=parentContainer;if(!parentContainer||parentContainer.tagName.match(/g|svg/i)){body=document.getElementsByTagName('body')[0];}
container.innerHTML=content;container.style.left=0;container.style.top=0;container.style.opacity=0;body.appendChild(container);var height=parseInt(container.offsetHeight),width=parseInt(container.offsetWidth),windowWidth=nv.utils.windowSize().width,windowHeight=nv.utils.windowSize().height,scrollTop=window.scrollY,scrollLeft=window.scrollX,left,top;windowHeight=window.innerWidth>=document.body.scrollWidth?windowHeight:windowHeight-16;windowWidth=window.innerHeight>=document.body.scrollHeight?windowWidth:windowWidth-16;var tooltipTop=function(Elem){var offsetTop=top;do{if(!isNaN(Elem.offsetTop)){offsetTop+=(Elem.offsetTop);}}while(Elem=Elem.offsetParent);return offsetTop;}
var tooltipLeft=function(Elem){var offsetLeft=left;do{if(!isNaN(Elem.offsetLeft)){offsetLeft+=(Elem.offsetLeft);}}while(Elem=Elem.offsetParent);return offsetLeft;}
switch(gravity){case'e':left=pos[0]-width-dist;top=pos[1]-(height/2);var tLeft=tooltipLeft(container);var tTop=tooltipTop(container);if(tLeft<scrollLeft)left=pos[0]+dist>scrollLeft?pos[0]+dist:scrollLeft-tLeft+left;if(tTop<scrollTop)top=scrollTop-tTop+top;if(tTop+height>scrollTop+windowHeight)top=scrollTop+windowHeight-tTop+top-height;break;case'w':left=pos[0]+dist;top=pos[1]-(height/2);if(tLeft+width>windowWidth)left=pos[0]-width-dist;if(tTop<scrollTop)top=scrollTop+5;if(tTop+height>scrollTop+windowHeight)top=scrollTop-height-5;break;case'n':left=pos[0]-(width/2)-5;top=pos[1]+dist;var tLeft=tooltipLeft(container);var tTop=tooltipTop(container);if(tLeft<scrollLeft)left=scrollLeft+5;if(tLeft+width>windowWidth)left=left-width/2+5;if(tTop+height>scrollTop+windowHeight)top=scrollTop+windowHeight-tTop+top-height;break;case's':left=pos[0]-(width/2);top=pos[1]-height-dist;var tLeft=tooltipLeft(container);var tTop=tooltipTop(container);if(tLeft<scrollLeft)left=scrollLeft+5;if(tLeft+width>windowWidth)left=left-width/2+5;if(scrollTop>tTop)top=scrollTop;break;}
container.style.left=left+'px';container.style.top=top+'px';container.style.opacity=1;container.style.position='absolute';container.style.pointerEvents='none';return container;};nvtooltip.cleanup=function(){var tooltips=document.getElementsByClassName('nvtooltip');var purging=[];while(tooltips.length){purging.push(tooltips[0]);tooltips[0].style.transitionDelay='0 !important';tooltips[0].style.opacity=0;tooltips[0].className='nvtooltip-pending-removal';}
setTimeout(function(){while(purging.length){var removeMe=purging.pop();removeMe.parentNode.removeChild(removeMe);}},500);};})();

nv.utils.windowSize=function(){var size={width:640,height:480};if(document.body&&document.body.offsetWidth){size.width=document.body.offsetWidth;size.height=document.body.offsetHeight;}
if(document.compatMode=='CSS1Compat'&&document.documentElement&&document.documentElement.offsetWidth){size.width=document.documentElement.offsetWidth;size.height=document.documentElement.offsetHeight;}
if(window.innerWidth&&window.innerHeight){size.width=window.innerWidth;size.height=window.innerHeight;}
return(size);};nv.utils.windowResize=function(fun){var oldresize=window.onresize;window.onresize=function(e){if(typeof oldresize=='function')oldresize(e);fun(e);}}
nv.utils.getColor=function(color){if(!arguments.length)return nv.utils.defaultColor();if(Object.prototype.toString.call(color)==='[object Array]')
return function(d,i){return d.color||color[i%color.length];};else
return color;}
nv.utils.defaultColor=function(){var colors=d3.scale.category20().range();return function(d,i){return d.color||colors[i%colors.length]};}
nv.utils.customTheme=function(dictionary,getKey,defaultColors){getKey=getKey||function(series){return series.key};defaultColors=defaultColors||d3.scale.category20().range();var defIndex=defaultColors.length;return function(series,index){var key=getKey(series);if(!defIndex)defIndex=defaultColors.length;if(typeof dictionary[key]!=="undefined")
return(typeof dictionary[key]==="function")?dictionary[key]():dictionary[key];else
return defaultColors[--defIndex];}}
nv.utils.pjax=function(links,content){d3.selectAll(links).on("click",function(){history.pushState(this.href,this.textContent,this.href);load(this.href);d3.event.preventDefault();});function load(href){d3.html(href,function(fragment){var target=d3.select(content).node();target.parentNode.replaceChild(d3.select(fragment).select(content).node(),target);nv.utils.pjax(links,content);});}
d3.select(window).on("popstate",function(){if(d3.event.state)load(d3.event.state);});}
nv.utils.calcApproxTextWidth=function(svgTextElem){if(svgTextElem instanceof d3.selection){var fontSize=parseInt(svgTextElem.style("font-size").replace("px",""));var textLength=svgTextElem.text().length;return textLength*fontSize*0.5;}
return 0;};

function getTotalCountPie(datas){var sum=0;var length=datas.length;for(var i=0;i<length;i++){sum+=datas[i].y;}
return sum;};function getTotalCount(datas){var sum={};var length=datas.length;for(var i=0;i<length;i++){var length2=datas[i].values.length;var sumTemp=0;for(var j=0;j<length2;j++){sumTemp+=datas[i].values[j].y;}
var x=datas[i].key.replace(/ /gi,"");sum[x]=sumTemp;}
return sum;};function ucolor(jsonku){var length=jsonku.length;var warna=[];for(var i=0;i<length;i++){warna.push(jsonku[i].color);}
return warna;};

(function(p,z){function q(a){return!!(""===a||a&&a.charCodeAt&&a.substr)}function m(a){return u?u(a):"[object Array]"===v.call(a)}function r(a){return"[object Object]"===v.call(a)}function s(a,b){var d,a=a||{},b=b||{};for(d in b)b.hasOwnProperty(d)&&null==a[d]&&(a[d]=b[d]);return a}function j(a,b,d){var c=[],e,h;if(!a)return c;if(w&&a.map===w)return a.map(b,d);for(e=0,h=a.length;e<h;e++)c[e]=b.call(d,a[e],e,a);return c}function n(a,b){a=Math.round(Math.abs(a));return isNaN(a)?b:a}function x(a){var b=c.settings.currency.format;"function"===typeof a&&(a=a());return q(a)&&a.match("%v")?{pos:a,neg:a.replace("-","").replace("%v","-%v"),zero:a}:!a||!a.pos||!a.pos.match("%v")?!q(b)?b:c.settings.currency.format={pos:b,neg:b.replace("%v","-%v"),zero:b}:a}var c={version:"0.3.2",settings:{currency:{symbol:"$",format:"%s%v",decimal:".",thousand:",",precision:2,grouping:3},number:{precision:0,grouping:3,thousand:",",decimal:"."}}},w=Array.prototype.map,u=Array.isArray,v=Object.prototype.toString,o=c.unformat=c.parse=function(a,b){if(m(a))return j(a,function(a){return o(a,b)});a=a||0;if("number"===typeof a)return a;var b=b||".",c=RegExp("[^0-9-"+b+"]",["g"]),c=parseFloat((""+a).replace(/\((.*)\)/,"-$1").replace(c,"").replace(b,"."));return!isNaN(c)?c:0},y=c.toFixed=function(a,b){var b=n(b,c.settings.number.precision),d=Math.pow(10,b);return(Math.round(c.unformat(a)*d)/d).toFixed(b)},t=c.formatNumber=function(a,b,d,i){if(m(a))return j(a,function(a){return t(a,b,d,i)});var a=o(a),e=s(r(b)?b:{precision:b,thousand:d,decimal:i},c.settings.number),h=n(e.precision),f=0>a?"-":"",g=parseInt(y(Math.abs(a||0),h),10)+"",l=3<g.length?g.length%3:0;return f+(l?g.substr(0,l)+e.thousand:"")+g.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+e.thousand)+(h?e.decimal+y(Math.abs(a),h).split(".")[1]:"")},A=c.formatMoney=function(a,b,d,i,e,h){if(m(a))return j(a,function(a){return A(a,b,d,i,e,h)});var a=o(a),f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format);return(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal))};c.formatColumn=function(a,b,d,i,e,h){if(!a)return[];var f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format),l=g.pos.indexOf("%s")<g.pos.indexOf("%v")?!0:!1,k=0,a=j(a,function(a){if(m(a))return c.formatColumn(a,f);a=o(a);a=(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal));if(a.length>k)k=a.length;return a});return j(a,function(a){return q(a)&&a.length<k?l?a.replace(f.symbol,f.symbol+Array(k-a.length+1).join(" ")):Array(k-a.length+1).join(" ")+a:a})};if("undefined"!==typeof exports){if("undefined"!==typeof module&&module.exports)exports=module.exports=c;exports.accounting=c}else"function"===typeof define&&define.amd?define([],function(){return c}):(c.noConflict=function(a){return function(){p.accounting=a;c.noConflict=z;return c}}(p.accounting),p.accounting=c)})(this);

(function(window,document,undefined){(function(factory){"use strict";if(typeof define==='function'&&define.amd)
{define(['jquery'],factory);}
else if(jQuery&&!jQuery.fn.dataTable)
{factory(jQuery);}}
(function($){"use strict";var DataTable=function(oInit)
{function _fnAddColumn(oSettings,nTh)
{var oDefaults=DataTable.defaults.columns;var iCol=oSettings.aoColumns.length;var oCol=$.extend({},DataTable.models.oColumn,oDefaults,{"sSortingClass":oSettings.oClasses.sSortable,"sSortingClassJUI":oSettings.oClasses.sSortJUI,"nTh":nTh?nTh:document.createElement('th'),"sTitle":oDefaults.sTitle?oDefaults.sTitle:nTh?nTh.innerHTML:'',"aDataSort":oDefaults.aDataSort?oDefaults.aDataSort:[iCol],"mData":oDefaults.mData?oDefaults.oDefaults:iCol});oSettings.aoColumns.push(oCol);if(oSettings.aoPreSearchCols[iCol]===undefined||oSettings.aoPreSearchCols[iCol]===null)
{oSettings.aoPreSearchCols[iCol]=$.extend({},DataTable.models.oSearch);}
else
{var oPre=oSettings.aoPreSearchCols[iCol];if(oPre.bRegex===undefined)
{oPre.bRegex=true;}
if(oPre.bSmart===undefined)
{oPre.bSmart=true;}
if(oPre.bCaseInsensitive===undefined)
{oPre.bCaseInsensitive=true;}}
_fnColumnOptions(oSettings,iCol,null);}
function _fnColumnOptions(oSettings,iCol,oOptions)
{var oCol=oSettings.aoColumns[iCol];if(oOptions!==undefined&&oOptions!==null)
{if(oOptions.mDataProp&&!oOptions.mData)
{oOptions.mData=oOptions.mDataProp;}
if(oOptions.sType!==undefined)
{oCol.sType=oOptions.sType;oCol._bAutoType=false;}
$.extend(oCol,oOptions);_fnMap(oCol,oOptions,"sWidth","sWidthOrig");if(oOptions.iDataSort!==undefined)
{oCol.aDataSort=[oOptions.iDataSort];}
_fnMap(oCol,oOptions,"aDataSort");}
var mRender=oCol.mRender?_fnGetObjectDataFn(oCol.mRender):null;var mData=_fnGetObjectDataFn(oCol.mData);oCol.fnGetData=function(oData,sSpecific){var innerData=mData(oData,sSpecific);if(oCol.mRender&&(sSpecific&&sSpecific!==''))
{return mRender(innerData,sSpecific,oData);}
return innerData;};oCol.fnSetData=_fnSetObjectDataFn(oCol.mData);if(!oSettings.oFeatures.bSort)
{oCol.bSortable=false;}
if(!oCol.bSortable||($.inArray('asc',oCol.asSorting)==-1&&$.inArray('desc',oCol.asSorting)==-1))
{oCol.sSortingClass=oSettings.oClasses.sSortableNone;oCol.sSortingClassJUI="";}
else if($.inArray('asc',oCol.asSorting)==-1&&$.inArray('desc',oCol.asSorting)==-1)
{oCol.sSortingClass=oSettings.oClasses.sSortable;oCol.sSortingClassJUI=oSettings.oClasses.sSortJUI;}
else if($.inArray('asc',oCol.asSorting)!=-1&&$.inArray('desc',oCol.asSorting)==-1)
{oCol.sSortingClass=oSettings.oClasses.sSortableAsc;oCol.sSortingClassJUI=oSettings.oClasses.sSortJUIAscAllowed;}
else if($.inArray('asc',oCol.asSorting)==-1&&$.inArray('desc',oCol.asSorting)!=-1)
{oCol.sSortingClass=oSettings.oClasses.sSortableDesc;oCol.sSortingClassJUI=oSettings.oClasses.sSortJUIDescAllowed;}}
function _fnAdjustColumnSizing(oSettings)
{if(oSettings.oFeatures.bAutoWidth===false)
{return false;}
_fnCalculateColumnWidths(oSettings);for(var i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{oSettings.aoColumns[i].nTh.style.width=oSettings.aoColumns[i].sWidth;}}
function _fnVisibleToColumnIndex(oSettings,iMatch)
{var aiVis=_fnGetColumns(oSettings,'bVisible');return typeof aiVis[iMatch]==='number'?aiVis[iMatch]:null;}
function _fnColumnIndexToVisible(oSettings,iMatch)
{var aiVis=_fnGetColumns(oSettings,'bVisible');var iPos=$.inArray(iMatch,aiVis);return iPos!==-1?iPos:null;}
function _fnVisbleColumns(oSettings)
{return _fnGetColumns(oSettings,'bVisible').length;}
function _fnGetColumns(oSettings,sParam)
{var a=[];$.map(oSettings.aoColumns,function(val,i){if(val[sParam]){a.push(i);}});return a;}
function _fnDetectType(sData)
{var aTypes=DataTable.ext.aTypes;var iLen=aTypes.length;for(var i=0;i<iLen;i++)
{var sType=aTypes[i](sData);if(sType!==null)
{return sType;}}
return'string';}
function _fnReOrderIndex(oSettings,sColumns)
{var aColumns=sColumns.split(',');var aiReturn=[];for(var i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{for(var j=0;j<iLen;j++)
{if(oSettings.aoColumns[i].sName==aColumns[j])
{aiReturn.push(j);break;}}}
return aiReturn;}
function _fnColumnOrdering(oSettings)
{var sNames='';for(var i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{sNames+=oSettings.aoColumns[i].sName+',';}
if(sNames.length==iLen)
{return"";}
return sNames.slice(0,-1);}
function _fnApplyColumnDefs(oSettings,aoColDefs,aoCols,fn)
{var i,iLen,j,jLen,k,kLen;if(aoColDefs)
{for(i=aoColDefs.length-1;i>=0;i--)
{var aTargets=aoColDefs[i].aTargets;if(!$.isArray(aTargets))
{_fnLog(oSettings,1,'aTargets must be an array of targets, not a '+(typeof aTargets));}
for(j=0,jLen=aTargets.length;j<jLen;j++)
{if(typeof aTargets[j]==='number'&&aTargets[j]>=0)
{while(oSettings.aoColumns.length<=aTargets[j])
{_fnAddColumn(oSettings);}
fn(aTargets[j],aoColDefs[i]);}
else if(typeof aTargets[j]==='number'&&aTargets[j]<0)
{fn(oSettings.aoColumns.length+aTargets[j],aoColDefs[i]);}
else if(typeof aTargets[j]==='string')
{for(k=0,kLen=oSettings.aoColumns.length;k<kLen;k++)
{if(aTargets[j]=="_all"||$(oSettings.aoColumns[k].nTh).hasClass(aTargets[j]))
{fn(k,aoColDefs[i]);}}}}}}
if(aoCols)
{for(i=0,iLen=aoCols.length;i<iLen;i++)
{fn(i,aoCols[i]);}}}
function _fnAddData(oSettings,aDataSupplied)
{var oCol;var aDataIn=($.isArray(aDataSupplied))?aDataSupplied.slice():$.extend(true,{},aDataSupplied);var iRow=oSettings.aoData.length;var oData=$.extend(true,{},DataTable.models.oRow);oData._aData=aDataIn;oSettings.aoData.push(oData);var nTd,sThisType;for(var i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{oCol=oSettings.aoColumns[i];if(typeof oCol.fnRender==='function'&&oCol.bUseRendered&&oCol.mData!==null)
{_fnSetCellData(oSettings,iRow,i,_fnRender(oSettings,iRow,i));}
else
{_fnSetCellData(oSettings,iRow,i,_fnGetCellData(oSettings,iRow,i));}
if(oCol._bAutoType&&oCol.sType!='string')
{var sVarType=_fnGetCellData(oSettings,iRow,i,'type');if(sVarType!==null&&sVarType!=='')
{sThisType=_fnDetectType(sVarType);if(oCol.sType===null)
{oCol.sType=sThisType;}
else if(oCol.sType!=sThisType&&oCol.sType!="html")
{oCol.sType='string';}}}}
oSettings.aiDisplayMaster.push(iRow);if(!oSettings.oFeatures.bDeferRender)
{_fnCreateTr(oSettings,iRow);}
return iRow;}
function _fnGatherData(oSettings)
{var iLoop,i,iLen,j,jLen,jInner,nTds,nTrs,nTd,nTr,aLocalData,iThisIndex,iRow,iRows,iColumn,iColumns,sNodeName,oCol,oData;if(oSettings.bDeferLoading||oSettings.sAjaxSource===null)
{nTr=oSettings.nTBody.firstChild;while(nTr)
{if(nTr.nodeName.toUpperCase()=="TR")
{iThisIndex=oSettings.aoData.length;nTr._DT_RowIndex=iThisIndex;oSettings.aoData.push($.extend(true,{},DataTable.models.oRow,{"nTr":nTr}));oSettings.aiDisplayMaster.push(iThisIndex);nTd=nTr.firstChild;jInner=0;while(nTd)
{sNodeName=nTd.nodeName.toUpperCase();if(sNodeName=="TD"||sNodeName=="TH")
{_fnSetCellData(oSettings,iThisIndex,jInner,$.trim(nTd.innerHTML));jInner++;}
nTd=nTd.nextSibling;}}
nTr=nTr.nextSibling;}}
nTrs=_fnGetTrNodes(oSettings);nTds=[];for(i=0,iLen=nTrs.length;i<iLen;i++)
{nTd=nTrs[i].firstChild;while(nTd)
{sNodeName=nTd.nodeName.toUpperCase();if(sNodeName=="TD"||sNodeName=="TH")
{nTds.push(nTd);}
nTd=nTd.nextSibling;}}
for(iColumn=0,iColumns=oSettings.aoColumns.length;iColumn<iColumns;iColumn++)
{oCol=oSettings.aoColumns[iColumn];if(oCol.sTitle===null)
{oCol.sTitle=oCol.nTh.innerHTML;}
var
bAutoType=oCol._bAutoType,bRender=typeof oCol.fnRender==='function',bClass=oCol.sClass!==null,bVisible=oCol.bVisible,nCell,sThisType,sRendered,sValType;if(bAutoType||bRender||bClass||!bVisible)
{for(iRow=0,iRows=oSettings.aoData.length;iRow<iRows;iRow++)
{oData=oSettings.aoData[iRow];nCell=nTds[(iRow*iColumns)+iColumn];if(bAutoType&&oCol.sType!='string')
{sValType=_fnGetCellData(oSettings,iRow,iColumn,'type');if(sValType!=='')
{sThisType=_fnDetectType(sValType);if(oCol.sType===null)
{oCol.sType=sThisType;}
else if(oCol.sType!=sThisType&&oCol.sType!="html")
{oCol.sType='string';}}}
if(oCol.mRender)
{nCell.innerHTML=_fnGetCellData(oSettings,iRow,iColumn,'display');}
else if(oCol.mData!==iColumn)
{nCell.innerHTML=_fnGetCellData(oSettings,iRow,iColumn,'display');}
if(bRender)
{sRendered=_fnRender(oSettings,iRow,iColumn);nCell.innerHTML=sRendered;if(oCol.bUseRendered)
{_fnSetCellData(oSettings,iRow,iColumn,sRendered);}}
if(bClass)
{nCell.className+=' '+oCol.sClass;}
if(!bVisible)
{oData._anHidden[iColumn]=nCell;nCell.parentNode.removeChild(nCell);}
else
{oData._anHidden[iColumn]=null;}
if(oCol.fnCreatedCell)
{oCol.fnCreatedCell.call(oSettings.oInstance,nCell,_fnGetCellData(oSettings,iRow,iColumn,'display'),oData._aData,iRow,iColumn);}}}}
if(oSettings.aoRowCreatedCallback.length!==0)
{for(i=0,iLen=oSettings.aoData.length;i<iLen;i++)
{oData=oSettings.aoData[i];_fnCallbackFire(oSettings,'aoRowCreatedCallback',null,[oData.nTr,oData._aData,i]);}}}
function _fnNodeToDataIndex(oSettings,n)
{return(n._DT_RowIndex!==undefined)?n._DT_RowIndex:null;}
function _fnNodeToColumnIndex(oSettings,iRow,n)
{var anCells=_fnGetTdNodes(oSettings,iRow);for(var i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{if(anCells[i]===n)
{return i;}}
return-1;}
function _fnGetRowData(oSettings,iRow,sSpecific,aiColumns)
{var out=[];for(var i=0,iLen=aiColumns.length;i<iLen;i++)
{out.push(_fnGetCellData(oSettings,iRow,aiColumns[i],sSpecific));}
return out;}
function _fnGetCellData(oSettings,iRow,iCol,sSpecific)
{var sData;var oCol=oSettings.aoColumns[iCol];var oData=oSettings.aoData[iRow]._aData;if((sData=oCol.fnGetData(oData,sSpecific))===undefined)
{if(oSettings.iDrawError!=oSettings.iDraw&&oCol.sDefaultContent===null)
{_fnLog(oSettings,0,"Requested unknown parameter "+
(typeof oCol.mData=='function'?'{mData function}':"'"+oCol.mData+"'")+" from the data source for row "+iRow);oSettings.iDrawError=oSettings.iDraw;}
return oCol.sDefaultContent;}
if(sData===null&&oCol.sDefaultContent!==null)
{sData=oCol.sDefaultContent;}
else if(typeof sData==='function')
{return sData();}
if(sSpecific=='display'&&sData===null)
{return'';}
return sData;}
function _fnSetCellData(oSettings,iRow,iCol,val)
{var oCol=oSettings.aoColumns[iCol];var oData=oSettings.aoData[iRow]._aData;oCol.fnSetData(oData,val);}
var __reArray=/\[.*?\]$/;function _fnGetObjectDataFn(mSource)
{if(mSource===null)
{return function(data,type){return null;};}
else if(typeof mSource==='function')
{return function(data,type,extra){return mSource(data,type,extra);};}
else if(typeof mSource==='string'&&(mSource.indexOf('.')!==-1||mSource.indexOf('[')!==-1))
{var fetchData=function(data,type,src){var a=src.split('.');var arrayNotation,out,innerSrc;if(src!=="")
{for(var i=0,iLen=a.length;i<iLen;i++)
{arrayNotation=a[i].match(__reArray);if(arrayNotation){a[i]=a[i].replace(__reArray,'');if(a[i]!==""){data=data[a[i]];}
out=[];a.splice(0,i+1);innerSrc=a.join('.');for(var j=0,jLen=data.length;j<jLen;j++){out.push(fetchData(data[j],type,innerSrc));}
var join=arrayNotation[0].substring(1,arrayNotation[0].length-1);data=(join==="")?out:out.join(join);break;}
if(data===null||data[a[i]]===undefined)
{return undefined;}
data=data[a[i]];}}
return data;};return function(data,type){return fetchData(data,type,mSource);};}
else
{return function(data,type){return data[mSource];};}}
function _fnSetObjectDataFn(mSource)
{if(mSource===null)
{return function(data,val){};}
else if(typeof mSource==='function')
{return function(data,val){mSource(data,'set',val);};}
else if(typeof mSource==='string'&&(mSource.indexOf('.')!==-1||mSource.indexOf('[')!==-1))
{var setData=function(data,val,src){var a=src.split('.'),b;var arrayNotation,o,innerSrc;for(var i=0,iLen=a.length-1;i<iLen;i++)
{arrayNotation=a[i].match(__reArray);if(arrayNotation)
{a[i]=a[i].replace(__reArray,'');data[a[i]]=[];b=a.slice();b.splice(0,i+1);innerSrc=b.join('.');for(var j=0,jLen=val.length;j<jLen;j++)
{o={};setData(o,val[j],innerSrc);data[a[i]].push(o);}
return;}
if(data[a[i]]===null||data[a[i]]===undefined)
{data[a[i]]={};}
data=data[a[i]];}
data[a[a.length-1].replace(__reArray,'')]=val;};return function(data,val){return setData(data,val,mSource);};}
else
{return function(data,val){data[mSource]=val;};}}
function _fnGetDataMaster(oSettings)
{var aData=[];var iLen=oSettings.aoData.length;for(var i=0;i<iLen;i++)
{aData.push(oSettings.aoData[i]._aData);}
return aData;}
function _fnClearTable(oSettings)
{oSettings.aoData.splice(0,oSettings.aoData.length);oSettings.aiDisplayMaster.splice(0,oSettings.aiDisplayMaster.length);oSettings.aiDisplay.splice(0,oSettings.aiDisplay.length);_fnCalculateEnd(oSettings);}
function _fnDeleteIndex(a,iTarget)
{var iTargetIndex=-1;for(var i=0,iLen=a.length;i<iLen;i++)
{if(a[i]==iTarget)
{iTargetIndex=i;}
else if(a[i]>iTarget)
{a[i]--;}}
if(iTargetIndex!=-1)
{a.splice(iTargetIndex,1);}}
function _fnRender(oSettings,iRow,iCol)
{var oCol=oSettings.aoColumns[iCol];return oCol.fnRender({"iDataRow":iRow,"iDataColumn":iCol,"oSettings":oSettings,"aData":oSettings.aoData[iRow]._aData,"mDataProp":oCol.mData},_fnGetCellData(oSettings,iRow,iCol,'display'));}
function _fnCreateTr(oSettings,iRow)
{var oData=oSettings.aoData[iRow];var nTd;if(oData.nTr===null)
{oData.nTr=document.createElement('tr');oData.nTr._DT_RowIndex=iRow;if(oData._aData.DT_RowId)
{oData.nTr.id=oData._aData.DT_RowId;}
if(oData._aData.DT_RowClass)
{oData.nTr.className=oData._aData.DT_RowClass;}
for(var i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{var oCol=oSettings.aoColumns[i];nTd=document.createElement(oCol.sCellType);nTd.innerHTML=(typeof oCol.fnRender==='function'&&(!oCol.bUseRendered||oCol.mData===null))?_fnRender(oSettings,iRow,i):_fnGetCellData(oSettings,iRow,i,'display');if(oCol.sClass!==null)
{nTd.className=oCol.sClass;}
if(oCol.bVisible)
{oData.nTr.appendChild(nTd);oData._anHidden[i]=null;}
else
{oData._anHidden[i]=nTd;}
if(oCol.fnCreatedCell)
{oCol.fnCreatedCell.call(oSettings.oInstance,nTd,_fnGetCellData(oSettings,iRow,i,'display'),oData._aData,iRow,i);}}
_fnCallbackFire(oSettings,'aoRowCreatedCallback',null,[oData.nTr,oData._aData,iRow]);}}
function _fnBuildHead(oSettings)
{var i,nTh,iLen,j,jLen;var iThs=$('th, td',oSettings.nTHead).length;var iCorrector=0;var jqChildren;if(iThs!==0)
{for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{nTh=oSettings.aoColumns[i].nTh;nTh.setAttribute('role','columnheader');if(oSettings.aoColumns[i].bSortable)
{nTh.setAttribute('tabindex',oSettings.iTabIndex);nTh.setAttribute('aria-controls',oSettings.sTableId);}
if(oSettings.aoColumns[i].sClass!==null)
{$(nTh).addClass(oSettings.aoColumns[i].sClass);}
if(oSettings.aoColumns[i].sTitle!=nTh.innerHTML)
{nTh.innerHTML=oSettings.aoColumns[i].sTitle;}}}
else
{var nTr=document.createElement("tr");for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{nTh=oSettings.aoColumns[i].nTh;nTh.innerHTML=oSettings.aoColumns[i].sTitle;nTh.setAttribute('tabindex','0');if(oSettings.aoColumns[i].sClass!==null)
{$(nTh).addClass(oSettings.aoColumns[i].sClass);}
nTr.appendChild(nTh);}
$(oSettings.nTHead).html('')[0].appendChild(nTr);_fnDetectHeader(oSettings.aoHeader,oSettings.nTHead);}
$(oSettings.nTHead).children('tr').attr('role','row');if(oSettings.bJUI)
{for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{nTh=oSettings.aoColumns[i].nTh;var nDiv=document.createElement('div');nDiv.className=oSettings.oClasses.sSortJUIWrapper;$(nTh).contents().appendTo(nDiv);var nSpan=document.createElement('span');nSpan.className=oSettings.oClasses.sSortIcon;nDiv.appendChild(nSpan);nTh.appendChild(nDiv);}}
if(oSettings.oFeatures.bSort)
{for(i=0;i<oSettings.aoColumns.length;i++)
{if(oSettings.aoColumns[i].bSortable!==false)
{_fnSortAttachListener(oSettings,oSettings.aoColumns[i].nTh,i);}
else
{$(oSettings.aoColumns[i].nTh).addClass(oSettings.oClasses.sSortableNone);}}}
if(oSettings.oClasses.sFooterTH!=="")
{$(oSettings.nTFoot).children('tr').children('th').addClass(oSettings.oClasses.sFooterTH);}
if(oSettings.nTFoot!==null)
{var anCells=_fnGetUniqueThs(oSettings,null,oSettings.aoFooter);for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{if(anCells[i])
{oSettings.aoColumns[i].nTf=anCells[i];if(oSettings.aoColumns[i].sClass)
{$(anCells[i]).addClass(oSettings.aoColumns[i].sClass);}}}}}
function _fnDrawHead(oSettings,aoSource,bIncludeHidden)
{var i,iLen,j,jLen,k,kLen,n,nLocalTr;var aoLocal=[];var aApplied=[];var iColumns=oSettings.aoColumns.length;var iRowspan,iColspan;if(bIncludeHidden===undefined)
{bIncludeHidden=false;}
for(i=0,iLen=aoSource.length;i<iLen;i++)
{aoLocal[i]=aoSource[i].slice();aoLocal[i].nTr=aoSource[i].nTr;for(j=iColumns-1;j>=0;j--)
{if(!oSettings.aoColumns[j].bVisible&&!bIncludeHidden)
{aoLocal[i].splice(j,1);}}
aApplied.push([]);}
for(i=0,iLen=aoLocal.length;i<iLen;i++)
{nLocalTr=aoLocal[i].nTr;if(nLocalTr)
{while((n=nLocalTr.firstChild))
{nLocalTr.removeChild(n);}}
for(j=0,jLen=aoLocal[i].length;j<jLen;j++)
{iRowspan=1;iColspan=1;if(aApplied[i][j]===undefined)
{nLocalTr.appendChild(aoLocal[i][j].cell);aApplied[i][j]=1;while(aoLocal[i+iRowspan]!==undefined&&aoLocal[i][j].cell==aoLocal[i+iRowspan][j].cell)
{aApplied[i+iRowspan][j]=1;iRowspan++;}
while(aoLocal[i][j+iColspan]!==undefined&&aoLocal[i][j].cell==aoLocal[i][j+iColspan].cell)
{for(k=0;k<iRowspan;k++)
{aApplied[i+k][j+iColspan]=1;}
iColspan++;}
aoLocal[i][j].cell.rowSpan=iRowspan;aoLocal[i][j].cell.colSpan=iColspan;}}}}
function _fnDraw(oSettings)
{var aPreDraw=_fnCallbackFire(oSettings,'aoPreDrawCallback','preDraw',[oSettings]);if($.inArray(false,aPreDraw)!==-1)
{_fnProcessingDisplay(oSettings,false);return;}
var i,iLen,n;var anRows=[];var iRowCount=0;var iStripes=oSettings.asStripeClasses.length;var iOpenRows=oSettings.aoOpenRows.length;oSettings.bDrawing=true;if(oSettings.iInitDisplayStart!==undefined&&oSettings.iInitDisplayStart!=-1)
{if(oSettings.oFeatures.bServerSide)
{oSettings._iDisplayStart=oSettings.iInitDisplayStart;}
else
{oSettings._iDisplayStart=(oSettings.iInitDisplayStart>=oSettings.fnRecordsDisplay())?0:oSettings.iInitDisplayStart;}
oSettings.iInitDisplayStart=-1;_fnCalculateEnd(oSettings);}
if(oSettings.bDeferLoading)
{oSettings.bDeferLoading=false;oSettings.iDraw++;}
else if(!oSettings.oFeatures.bServerSide)
{oSettings.iDraw++;}
else if(!oSettings.bDestroying&&!_fnAjaxUpdate(oSettings))
{return;}
if(oSettings.aiDisplay.length!==0)
{var iStart=oSettings._iDisplayStart;var iEnd=oSettings._iDisplayEnd;if(oSettings.oFeatures.bServerSide)
{iStart=0;iEnd=oSettings.aoData.length;}
for(var j=iStart;j<iEnd;j++)
{var aoData=oSettings.aoData[oSettings.aiDisplay[j]];if(aoData.nTr===null)
{_fnCreateTr(oSettings,oSettings.aiDisplay[j]);}
var nRow=aoData.nTr;if(iStripes!==0)
{var sStripe=oSettings.asStripeClasses[iRowCount%iStripes];if(aoData._sRowStripe!=sStripe)
{$(nRow).removeClass(aoData._sRowStripe).addClass(sStripe);aoData._sRowStripe=sStripe;}}
_fnCallbackFire(oSettings,'aoRowCallback',null,[nRow,oSettings.aoData[oSettings.aiDisplay[j]]._aData,iRowCount,j]);anRows.push(nRow);iRowCount++;if(iOpenRows!==0)
{for(var k=0;k<iOpenRows;k++)
{if(nRow==oSettings.aoOpenRows[k].nParent)
{anRows.push(oSettings.aoOpenRows[k].nTr);break;}}}}}
else
{anRows[0]=document.createElement('tr');if(oSettings.asStripeClasses[0])
{anRows[0].className=oSettings.asStripeClasses[0];}
var oLang=oSettings.oLanguage;var sZero=oLang.sZeroRecords;if(oSettings.iDraw==1&&oSettings.sAjaxSource!==null&&!oSettings.oFeatures.bServerSide)
{sZero=oLang.sLoadingRecords;}
else if(oLang.sEmptyTable&&oSettings.fnRecordsTotal()===0)
{sZero=oLang.sEmptyTable;}
var nTd=document.createElement('td');nTd.setAttribute('valign',"top");nTd.colSpan=_fnVisbleColumns(oSettings);nTd.className=oSettings.oClasses.sRowEmpty;nTd.innerHTML=_fnInfoMacros(oSettings,sZero);anRows[iRowCount].appendChild(nTd);}
_fnCallbackFire(oSettings,'aoHeaderCallback','header',[$(oSettings.nTHead).children('tr')[0],_fnGetDataMaster(oSettings),oSettings._iDisplayStart,oSettings.fnDisplayEnd(),oSettings.aiDisplay]);_fnCallbackFire(oSettings,'aoFooterCallback','footer',[$(oSettings.nTFoot).children('tr')[0],_fnGetDataMaster(oSettings),oSettings._iDisplayStart,oSettings.fnDisplayEnd(),oSettings.aiDisplay]);var
nAddFrag=document.createDocumentFragment(),nRemoveFrag=document.createDocumentFragment(),nBodyPar,nTrs;if(oSettings.nTBody)
{nBodyPar=oSettings.nTBody.parentNode;nRemoveFrag.appendChild(oSettings.nTBody);if(!oSettings.oScroll.bInfinite||!oSettings._bInitComplete||oSettings.bSorted||oSettings.bFiltered)
{while((n=oSettings.nTBody.firstChild))
{oSettings.nTBody.removeChild(n);}}
for(i=0,iLen=anRows.length;i<iLen;i++)
{nAddFrag.appendChild(anRows[i]);}
oSettings.nTBody.appendChild(nAddFrag);if(nBodyPar!==null)
{nBodyPar.appendChild(oSettings.nTBody);}}
_fnCallbackFire(oSettings,'aoDrawCallback','draw',[oSettings]);oSettings.bSorted=false;oSettings.bFiltered=false;oSettings.bDrawing=false;if(oSettings.oFeatures.bServerSide)
{_fnProcessingDisplay(oSettings,false);if(!oSettings._bInitComplete)
{_fnInitComplete(oSettings);}}}
function _fnReDraw(oSettings)
{if(oSettings.oFeatures.bSort)
{_fnSort(oSettings,oSettings.oPreviousSearch);}
else if(oSettings.oFeatures.bFilter)
{_fnFilterComplete(oSettings,oSettings.oPreviousSearch);}
else
{_fnCalculateEnd(oSettings);_fnDraw(oSettings);}}
function _fnAddOptionsHtml(oSettings)
{var nHolding=$('<div></div>')[0];oSettings.nTable.parentNode.insertBefore(nHolding,oSettings.nTable);oSettings.nTableWrapper=$('<div id="'+oSettings.sTableId+'_wrapper" class="'+oSettings.oClasses.sWrapper+'" role="grid"></div>')[0];oSettings.nTableReinsertBefore=oSettings.nTable.nextSibling;var nInsertNode=oSettings.nTableWrapper;var aDom=oSettings.sDom.split('');var nTmp,iPushFeature,cOption,nNewNode,cNext,sAttr,j;for(var i=0;i<aDom.length;i++)
{iPushFeature=0;cOption=aDom[i];if(cOption=='<')
{nNewNode=$('<div></div>')[0];cNext=aDom[i+1];if(cNext=="'"||cNext=='"')
{sAttr="";j=2;while(aDom[i+j]!=cNext)
{sAttr+=aDom[i+j];j++;}
if(sAttr=="H")
{sAttr=oSettings.oClasses.sJUIHeader;}
else if(sAttr=="F")
{sAttr=oSettings.oClasses.sJUIFooter;}
if(sAttr.indexOf('.')!=-1)
{var aSplit=sAttr.split('.');nNewNode.id=aSplit[0].substr(1,aSplit[0].length-1);nNewNode.className=aSplit[1];}
else if(sAttr.charAt(0)=="#")
{nNewNode.id=sAttr.substr(1,sAttr.length-1);}
else
{nNewNode.className=sAttr;}
i+=j;}
nInsertNode.appendChild(nNewNode);nInsertNode=nNewNode;}
else if(cOption=='>')
{nInsertNode=nInsertNode.parentNode;}
else if(cOption=='l'&&oSettings.oFeatures.bPaginate&&oSettings.oFeatures.bLengthChange)
{nTmp=_fnFeatureHtmlLength(oSettings);iPushFeature=1;}
else if(cOption=='f'&&oSettings.oFeatures.bFilter)
{nTmp=_fnFeatureHtmlFilter(oSettings);iPushFeature=1;}
else if(cOption=='r'&&oSettings.oFeatures.bProcessing)
{nTmp=_fnFeatureHtmlProcessing(oSettings);iPushFeature=1;}
else if(cOption=='t')
{nTmp=_fnFeatureHtmlTable(oSettings);iPushFeature=1;}
else if(cOption=='i'&&oSettings.oFeatures.bInfo)
{nTmp=_fnFeatureHtmlInfo(oSettings);iPushFeature=1;}
else if(cOption=='p'&&oSettings.oFeatures.bPaginate)
{nTmp=_fnFeatureHtmlPaginate(oSettings);iPushFeature=1;}
else if(DataTable.ext.aoFeatures.length!==0)
{var aoFeatures=DataTable.ext.aoFeatures;for(var k=0,kLen=aoFeatures.length;k<kLen;k++)
{if(cOption==aoFeatures[k].cFeature)
{nTmp=aoFeatures[k].fnInit(oSettings);if(nTmp)
{iPushFeature=1;}
break;}}}
if(iPushFeature==1&&nTmp!==null)
{if(typeof oSettings.aanFeatures[cOption]!=='object')
{oSettings.aanFeatures[cOption]=[];}
oSettings.aanFeatures[cOption].push(nTmp);nInsertNode.appendChild(nTmp);}}
nHolding.parentNode.replaceChild(oSettings.nTableWrapper,nHolding);}
function _fnDetectHeader(aLayout,nThead)
{var nTrs=$(nThead).children('tr');var nTr,nCell;var i,k,l,iLen,jLen,iColShifted,iColumn,iColspan,iRowspan;var bUnique;var fnShiftCol=function(a,i,j){var k=a[i];while(k[j]){j++;}
return j;};aLayout.splice(0,aLayout.length);for(i=0,iLen=nTrs.length;i<iLen;i++)
{aLayout.push([]);}
for(i=0,iLen=nTrs.length;i<iLen;i++)
{nTr=nTrs[i];iColumn=0;nCell=nTr.firstChild;while(nCell){if(nCell.nodeName.toUpperCase()=="TD"||nCell.nodeName.toUpperCase()=="TH")
{iColspan=nCell.getAttribute('colspan')*1;iRowspan=nCell.getAttribute('rowspan')*1;iColspan=(!iColspan||iColspan===0||iColspan===1)?1:iColspan;iRowspan=(!iRowspan||iRowspan===0||iRowspan===1)?1:iRowspan;iColShifted=fnShiftCol(aLayout,i,iColumn);bUnique=iColspan===1?true:false;for(l=0;l<iColspan;l++)
{for(k=0;k<iRowspan;k++)
{aLayout[i+k][iColShifted+l]={"cell":nCell,"unique":bUnique};aLayout[i+k].nTr=nTr;}}}
nCell=nCell.nextSibling;}}}
function _fnGetUniqueThs(oSettings,nHeader,aLayout)
{var aReturn=[];if(!aLayout)
{aLayout=oSettings.aoHeader;if(nHeader)
{aLayout=[];_fnDetectHeader(aLayout,nHeader);}}
for(var i=0,iLen=aLayout.length;i<iLen;i++)
{for(var j=0,jLen=aLayout[i].length;j<jLen;j++)
{if(aLayout[i][j].unique&&(!aReturn[j]||!oSettings.bSortCellsTop))
{aReturn[j]=aLayout[i][j].cell;}}}
return aReturn;}
function _fnAjaxUpdate(oSettings)
{if(oSettings.bAjaxDataGet)
{oSettings.iDraw++;_fnProcessingDisplay(oSettings,true);var iColumns=oSettings.aoColumns.length;var aoData=_fnAjaxParameters(oSettings);_fnServerParams(oSettings,aoData);oSettings.fnServerData.call(oSettings.oInstance,oSettings.sAjaxSource,aoData,function(json){_fnAjaxUpdateDraw(oSettings,json);},oSettings);return false;}
else
{return true;}}
function _fnAjaxParameters(oSettings)
{var iColumns=oSettings.aoColumns.length;var aoData=[],mDataProp,aaSort,aDataSort;var i,j;aoData.push({"name":"sEcho","value":oSettings.iDraw});aoData.push({"name":"iColumns","value":iColumns});aoData.push({"name":"sColumns","value":_fnColumnOrdering(oSettings)});aoData.push({"name":"iDisplayStart","value":oSettings._iDisplayStart});aoData.push({"name":"iDisplayLength","value":oSettings.oFeatures.bPaginate!==false?oSettings._iDisplayLength:-1});for(i=0;i<iColumns;i++)
{mDataProp=oSettings.aoColumns[i].mData;aoData.push({"name":"mDataProp_"+i,"value":typeof(mDataProp)==="function"?'function':mDataProp});}
if(oSettings.oFeatures.bFilter!==false)
{aoData.push({"name":"sSearch","value":oSettings.oPreviousSearch.sSearch});aoData.push({"name":"bRegex","value":oSettings.oPreviousSearch.bRegex});for(i=0;i<iColumns;i++)
{aoData.push({"name":"sSearch_"+i,"value":oSettings.aoPreSearchCols[i].sSearch});aoData.push({"name":"bRegex_"+i,"value":oSettings.aoPreSearchCols[i].bRegex});aoData.push({"name":"bSearchable_"+i,"value":oSettings.aoColumns[i].bSearchable});}}
if(oSettings.oFeatures.bSort!==false)
{var iCounter=0;aaSort=(oSettings.aaSortingFixed!==null)?oSettings.aaSortingFixed.concat(oSettings.aaSorting):oSettings.aaSorting.slice();for(i=0;i<aaSort.length;i++)
{aDataSort=oSettings.aoColumns[aaSort[i][0]].aDataSort;for(j=0;j<aDataSort.length;j++)
{aoData.push({"name":"iSortCol_"+iCounter,"value":aDataSort[j]});aoData.push({"name":"sSortDir_"+iCounter,"value":aaSort[i][1]});iCounter++;}}
aoData.push({"name":"iSortingCols","value":iCounter});for(i=0;i<iColumns;i++)
{aoData.push({"name":"bSortable_"+i,"value":oSettings.aoColumns[i].bSortable});}}
return aoData;}
function _fnServerParams(oSettings,aoData)
{_fnCallbackFire(oSettings,'aoServerParams','serverParams',[aoData]);}
function _fnAjaxUpdateDraw(oSettings,json)
{if(json.sEcho!==undefined)
{if(json.sEcho*1<oSettings.iDraw)
{return;}
else
{oSettings.iDraw=json.sEcho*1;}}
if(!oSettings.oScroll.bInfinite||(oSettings.oScroll.bInfinite&&(oSettings.bSorted||oSettings.bFiltered)))
{_fnClearTable(oSettings);}
oSettings._iRecordsTotal=parseInt(json.iTotalRecords,10);oSettings._iRecordsDisplay=parseInt(json.iTotalDisplayRecords,10);var sOrdering=_fnColumnOrdering(oSettings);var bReOrder=(json.sColumns!==undefined&&sOrdering!==""&&json.sColumns!=sOrdering);var aiIndex;if(bReOrder)
{aiIndex=_fnReOrderIndex(oSettings,json.sColumns);}
var aData=_fnGetObjectDataFn(oSettings.sAjaxDataProp)(json);for(var i=0,iLen=aData.length;i<iLen;i++)
{if(bReOrder)
{var aDataSorted=[];for(var j=0,jLen=oSettings.aoColumns.length;j<jLen;j++)
{aDataSorted.push(aData[i][aiIndex[j]]);}
_fnAddData(oSettings,aDataSorted);}
else
{_fnAddData(oSettings,aData[i]);}}
oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();oSettings.bAjaxDataGet=false;_fnDraw(oSettings);oSettings.bAjaxDataGet=true;_fnProcessingDisplay(oSettings,false);}
function _fnFeatureHtmlFilter(oSettings)
{var oPreviousSearch=oSettings.oPreviousSearch;var sSearchStr=oSettings.oLanguage.sSearch;sSearchStr=(sSearchStr.indexOf('_INPUT_')!==-1)?sSearchStr.replace('_INPUT_','<input type="text" class="form-control" placeholder="Search" />'):sSearchStr===""?'<input type="text" class="form-control" placeholder="Search" />':sSearchStr+' <input type="text" class="form-control" placeholder="Search" />';var nFilter=document.createElement('div');nFilter.className=oSettings.oClasses.sFilter;nFilter.innerHTML='<label>'+sSearchStr+'</label>';if(!oSettings.aanFeatures.f)
{nFilter.id=oSettings.sTableId+'_filter';}
var jqFilter=$('input[type="text"]',nFilter);nFilter._DT_Input=jqFilter[0];jqFilter.val(oPreviousSearch.sSearch.replace('"','&quot;'));jqFilter.bind('keyup.DT',function(e){var n=oSettings.aanFeatures.f;var val=this.value===""?"":this.value;for(var i=0,iLen=n.length;i<iLen;i++)
{if(n[i]!=$(this).parents('div.dataTables_filter')[0])
{$(n[i]._DT_Input).val(val);}}
if(val!=oPreviousSearch.sSearch)
{_fnFilterComplete(oSettings,{"sSearch":val,"bRegex":oPreviousSearch.bRegex,"bSmart":oPreviousSearch.bSmart,"bCaseInsensitive":oPreviousSearch.bCaseInsensitive});}});jqFilter.attr('aria-controls',oSettings.sTableId).bind('keypress.DT',function(e){if(e.keyCode==13)
{return false;}});return nFilter;}
function _fnFilterComplete(oSettings,oInput,iForce)
{var oPrevSearch=oSettings.oPreviousSearch;var aoPrevSearch=oSettings.aoPreSearchCols;var fnSaveFilter=function(oFilter){oPrevSearch.sSearch=oFilter.sSearch;oPrevSearch.bRegex=oFilter.bRegex;oPrevSearch.bSmart=oFilter.bSmart;oPrevSearch.bCaseInsensitive=oFilter.bCaseInsensitive;};if(!oSettings.oFeatures.bServerSide)
{_fnFilter(oSettings,oInput.sSearch,iForce,oInput.bRegex,oInput.bSmart,oInput.bCaseInsensitive);fnSaveFilter(oInput);for(var i=0;i<oSettings.aoPreSearchCols.length;i++)
{_fnFilterColumn(oSettings,aoPrevSearch[i].sSearch,i,aoPrevSearch[i].bRegex,aoPrevSearch[i].bSmart,aoPrevSearch[i].bCaseInsensitive);}
_fnFilterCustom(oSettings);}
else
{fnSaveFilter(oInput);}
oSettings.bFiltered=true;$(oSettings.oInstance).trigger('filter',oSettings);oSettings._iDisplayStart=0;_fnCalculateEnd(oSettings);_fnDraw(oSettings);_fnBuildSearchArray(oSettings,0);}
function _fnFilterCustom(oSettings)
{var afnFilters=DataTable.ext.afnFiltering;var aiFilterColumns=_fnGetColumns(oSettings,'bSearchable');for(var i=0,iLen=afnFilters.length;i<iLen;i++)
{var iCorrector=0;for(var j=0,jLen=oSettings.aiDisplay.length;j<jLen;j++)
{var iDisIndex=oSettings.aiDisplay[j-iCorrector];var bTest=afnFilters[i](oSettings,_fnGetRowData(oSettings,iDisIndex,'filter',aiFilterColumns),iDisIndex);if(!bTest)
{oSettings.aiDisplay.splice(j-iCorrector,1);iCorrector++;}}}}
function _fnFilterColumn(oSettings,sInput,iColumn,bRegex,bSmart,bCaseInsensitive)
{if(sInput==="")
{return;}
var iIndexCorrector=0;var rpSearch=_fnFilterCreateSearch(sInput,bRegex,bSmart,bCaseInsensitive);for(var i=oSettings.aiDisplay.length-1;i>=0;i--)
{var sData=_fnDataToSearch(_fnGetCellData(oSettings,oSettings.aiDisplay[i],iColumn,'filter'),oSettings.aoColumns[iColumn].sType);if(!rpSearch.test(sData))
{oSettings.aiDisplay.splice(i,1);iIndexCorrector++;}}}
function _fnFilter(oSettings,sInput,iForce,bRegex,bSmart,bCaseInsensitive)
{var i;var rpSearch=_fnFilterCreateSearch(sInput,bRegex,bSmart,bCaseInsensitive);var oPrevSearch=oSettings.oPreviousSearch;if(!iForce)
{iForce=0;}
if(DataTable.ext.afnFiltering.length!==0)
{iForce=1;}
if(sInput.length<=0)
{oSettings.aiDisplay.splice(0,oSettings.aiDisplay.length);oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();}
else
{if(oSettings.aiDisplay.length==oSettings.aiDisplayMaster.length||oPrevSearch.sSearch.length>sInput.length||iForce==1||sInput.indexOf(oPrevSearch.sSearch)!==0)
{oSettings.aiDisplay.splice(0,oSettings.aiDisplay.length);_fnBuildSearchArray(oSettings,1);for(i=0;i<oSettings.aiDisplayMaster.length;i++)
{if(rpSearch.test(oSettings.asDataSearch[i]))
{oSettings.aiDisplay.push(oSettings.aiDisplayMaster[i]);}}}
else
{var iIndexCorrector=0;for(i=0;i<oSettings.asDataSearch.length;i++)
{if(!rpSearch.test(oSettings.asDataSearch[i]))
{oSettings.aiDisplay.splice(i-iIndexCorrector,1);iIndexCorrector++;}}}}}
function _fnBuildSearchArray(oSettings,iMaster)
{if(!oSettings.oFeatures.bServerSide)
{oSettings.asDataSearch=[];var aiFilterColumns=_fnGetColumns(oSettings,'bSearchable');var aiIndex=(iMaster===1)?oSettings.aiDisplayMaster:oSettings.aiDisplay;for(var i=0,iLen=aiIndex.length;i<iLen;i++)
{oSettings.asDataSearch[i]=_fnBuildSearchRow(oSettings,_fnGetRowData(oSettings,aiIndex[i],'filter',aiFilterColumns));}}}
function _fnBuildSearchRow(oSettings,aData)
{var sSearch=aData.join('  ');if(sSearch.indexOf('&')!==-1)
{sSearch=$('<div>').html(sSearch).text();}
return sSearch.replace(/[\n\r]/g," ");}
function _fnFilterCreateSearch(sSearch,bRegex,bSmart,bCaseInsensitive)
{var asSearch,sRegExpString;if(bSmart)
{asSearch=bRegex?sSearch.split(' '):_fnEscapeRegex(sSearch).split(' ');sRegExpString='^(?=.*?'+asSearch.join(')(?=.*?')+').*$';return new RegExp(sRegExpString,bCaseInsensitive?"i":"");}
else
{sSearch=bRegex?sSearch:_fnEscapeRegex(sSearch);return new RegExp(sSearch,bCaseInsensitive?"i":"");}}
function _fnDataToSearch(sData,sType)
{if(typeof DataTable.ext.ofnSearch[sType]==="function")
{return DataTable.ext.ofnSearch[sType](sData);}
else if(sData===null)
{return'';}
else if(sType=="html")
{return sData.replace(/[\r\n]/g," ").replace(/<.*?>/g,"");}
else if(typeof sData==="string")
{return sData.replace(/[\r\n]/g," ");}
return sData;}
function _fnEscapeRegex(sVal)
{var acEscape=['/','.','*','+','?','|','(',')','[',']','{','}','\\','$','^','-'];var reReplace=new RegExp('(\\'+acEscape.join('|\\')+')','g');return sVal.replace(reReplace,'\\$1');}
function _fnFeatureHtmlInfo(oSettings)
{var nInfo=document.createElement('div');nInfo.className=oSettings.oClasses.sInfo;if(!oSettings.aanFeatures.i)
{oSettings.aoDrawCallback.push({"fn":_fnUpdateInfo,"sName":"information"});nInfo.id=oSettings.sTableId+'_info';}
oSettings.nTable.setAttribute('aria-describedby',oSettings.sTableId+'_info');return nInfo;}
function _fnUpdateInfo(oSettings)
{if(!oSettings.oFeatures.bInfo||oSettings.aanFeatures.i.length===0)
{return;}
var
oLang=oSettings.oLanguage,iStart=oSettings._iDisplayStart+1,iEnd=oSettings.fnDisplayEnd(),iMax=oSettings.fnRecordsTotal(),iTotal=oSettings.fnRecordsDisplay(),sOut;if(iTotal===0)
{sOut=oLang.sInfoEmpty;}
else{sOut=oLang.sInfo;}
if(iTotal!=iMax)
{sOut+=' '+oLang.sInfoFiltered;}
sOut+=oLang.sInfoPostFix;sOut=_fnInfoMacros(oSettings,sOut);if(oLang.fnInfoCallback!==null)
{sOut=oLang.fnInfoCallback.call(oSettings.oInstance,oSettings,iStart,iEnd,iMax,iTotal,sOut);}
var n=oSettings.aanFeatures.i;for(var i=0,iLen=n.length;i<iLen;i++)
{$(n[i]).html(sOut);}}
function _fnInfoMacros(oSettings,str)
{var
iStart=oSettings._iDisplayStart+1,sStart=oSettings.fnFormatNumber(iStart),iEnd=oSettings.fnDisplayEnd(),sEnd=oSettings.fnFormatNumber(iEnd),iTotal=oSettings.fnRecordsDisplay(),sTotal=oSettings.fnFormatNumber(iTotal),iMax=oSettings.fnRecordsTotal(),sMax=oSettings.fnFormatNumber(iMax);if(oSettings.oScroll.bInfinite)
{sStart=oSettings.fnFormatNumber(1);}
return str.replace(/_START_/g,sStart).replace(/_END_/g,sEnd).replace(/_TOTAL_/g,sTotal).replace(/_MAX_/g,sMax);}
function _fnInitialise(oSettings)
{var i,iLen,iAjaxStart=oSettings.iInitDisplayStart;if(oSettings.bInitialised===false)
{setTimeout(function(){_fnInitialise(oSettings);},200);return;}
_fnAddOptionsHtml(oSettings);_fnBuildHead(oSettings);_fnDrawHead(oSettings,oSettings.aoHeader);if(oSettings.nTFoot)
{_fnDrawHead(oSettings,oSettings.aoFooter);}
_fnProcessingDisplay(oSettings,true);if(oSettings.oFeatures.bAutoWidth)
{_fnCalculateColumnWidths(oSettings);}
for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{if(oSettings.aoColumns[i].sWidth!==null)
{oSettings.aoColumns[i].nTh.style.width=_fnStringToCss(oSettings.aoColumns[i].sWidth);}}
if(oSettings.oFeatures.bSort)
{_fnSort(oSettings);}
else if(oSettings.oFeatures.bFilter)
{_fnFilterComplete(oSettings,oSettings.oPreviousSearch);}
else
{oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();_fnCalculateEnd(oSettings);_fnDraw(oSettings);}
if(oSettings.sAjaxSource!==null&&!oSettings.oFeatures.bServerSide)
{var aoData=[];_fnServerParams(oSettings,aoData);oSettings.fnServerData.call(oSettings.oInstance,oSettings.sAjaxSource,aoData,function(json){var aData=(oSettings.sAjaxDataProp!=="")?_fnGetObjectDataFn(oSettings.sAjaxDataProp)(json):json;for(i=0;i<aData.length;i++)
{_fnAddData(oSettings,aData[i]);}
oSettings.iInitDisplayStart=iAjaxStart;if(oSettings.oFeatures.bSort)
{_fnSort(oSettings);}
else
{oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();_fnCalculateEnd(oSettings);_fnDraw(oSettings);}
_fnProcessingDisplay(oSettings,false);_fnInitComplete(oSettings,json);},oSettings);return;}
if(!oSettings.oFeatures.bServerSide)
{_fnProcessingDisplay(oSettings,false);_fnInitComplete(oSettings);}}
function _fnInitComplete(oSettings,json)
{oSettings._bInitComplete=true;_fnCallbackFire(oSettings,'aoInitComplete','init',[oSettings,json]);}
function _fnLanguageCompat(oLanguage)
{var oDefaults=DataTable.defaults.oLanguage;if(!oLanguage.sEmptyTable&&oLanguage.sZeroRecords&&oDefaults.sEmptyTable==="No data available in table")
{_fnMap(oLanguage,oLanguage,'sZeroRecords','sEmptyTable');}
if(!oLanguage.sLoadingRecords&&oLanguage.sZeroRecords&&oDefaults.sLoadingRecords==="Loading...")
{_fnMap(oLanguage,oLanguage,'sZeroRecords','sLoadingRecords');}}
function _fnFeatureHtmlLength(oSettings)
{if(oSettings.oScroll.bInfinite)
{return null;}
var sName='name="'+oSettings.sTableId+'_length"';var sStdMenu='<select size="1" '+sName+' class="form-control">';var i,iLen;var aLengthMenu=oSettings.aLengthMenu;if(aLengthMenu.length==2&&typeof aLengthMenu[0]==='object'&&typeof aLengthMenu[1]==='object')
{for(i=0,iLen=aLengthMenu[0].length;i<iLen;i++)
{sStdMenu+='<option value="'+aLengthMenu[0][i]+'">'+aLengthMenu[1][i]+'</option>';}}
else
{for(i=0,iLen=aLengthMenu.length;i<iLen;i++)
{sStdMenu+='<option value="'+aLengthMenu[i]+'">'+aLengthMenu[i]+'</option>';}}
sStdMenu+='</select>';var nLength=document.createElement('div');if(!oSettings.aanFeatures.l)
{nLength.id=oSettings.sTableId+'_length';}
nLength.className=oSettings.oClasses.sLength;nLength.innerHTML='<label>'+oSettings.oLanguage.sLengthMenu.replace('_MENU_',sStdMenu)+'</label>';$('select option[value="'+oSettings._iDisplayLength+'"]',nLength).attr("selected",true);$('select',nLength).bind('change.DT',function(e){var iVal=$(this).val();var n=oSettings.aanFeatures.l;for(i=0,iLen=n.length;i<iLen;i++)
{if(n[i]!=this.parentNode)
{$('select',n[i]).val(iVal);}}
oSettings._iDisplayLength=parseInt(iVal,10);_fnCalculateEnd(oSettings);if(oSettings.fnDisplayEnd()==oSettings.fnRecordsDisplay())
{oSettings._iDisplayStart=oSettings.fnDisplayEnd()-oSettings._iDisplayLength;if(oSettings._iDisplayStart<0)
{oSettings._iDisplayStart=0;}}
if(oSettings._iDisplayLength==-1)
{oSettings._iDisplayStart=0;}
_fnDraw(oSettings);});$('select',nLength).attr('aria-controls',oSettings.sTableId);return nLength;}
function _fnCalculateEnd(oSettings)
{if(oSettings.oFeatures.bPaginate===false)
{oSettings._iDisplayEnd=oSettings.aiDisplay.length;}
else
{if(oSettings._iDisplayStart+oSettings._iDisplayLength>oSettings.aiDisplay.length||oSettings._iDisplayLength==-1)
{oSettings._iDisplayEnd=oSettings.aiDisplay.length;}
else
{oSettings._iDisplayEnd=oSettings._iDisplayStart+oSettings._iDisplayLength;}}}
function _fnFeatureHtmlPaginate(oSettings)
{if(oSettings.oScroll.bInfinite)
{return null;}
var nPaginate=document.createElement('div');nPaginate.className=oSettings.oClasses.sPaging+oSettings.sPaginationType;DataTable.ext.oPagination[oSettings.sPaginationType].fnInit(oSettings,nPaginate,function(oSettings){_fnCalculateEnd(oSettings);_fnDraw(oSettings);});if(!oSettings.aanFeatures.p)
{oSettings.aoDrawCallback.push({"fn":function(oSettings){DataTable.ext.oPagination[oSettings.sPaginationType].fnUpdate(oSettings,function(oSettings){_fnCalculateEnd(oSettings);_fnDraw(oSettings);});},"sName":"pagination"});}
return nPaginate;}
function _fnPageChange(oSettings,mAction)
{var iOldStart=oSettings._iDisplayStart;if(typeof mAction==="number")
{oSettings._iDisplayStart=mAction*oSettings._iDisplayLength;if(oSettings._iDisplayStart>oSettings.fnRecordsDisplay())
{oSettings._iDisplayStart=0;}}
else if(mAction=="first")
{oSettings._iDisplayStart=0;}
else if(mAction=="previous")
{oSettings._iDisplayStart=oSettings._iDisplayLength>=0?oSettings._iDisplayStart-oSettings._iDisplayLength:0;if(oSettings._iDisplayStart<0)
{oSettings._iDisplayStart=0;}}
else if(mAction=="next")
{if(oSettings._iDisplayLength>=0)
{if(oSettings._iDisplayStart+oSettings._iDisplayLength<oSettings.fnRecordsDisplay())
{oSettings._iDisplayStart+=oSettings._iDisplayLength;}}
else
{oSettings._iDisplayStart=0;}}
else if(mAction=="last")
{if(oSettings._iDisplayLength>=0)
{var iPages=parseInt((oSettings.fnRecordsDisplay()-1)/oSettings._iDisplayLength,10)+1;oSettings._iDisplayStart=(iPages-1)*oSettings._iDisplayLength;}
else
{oSettings._iDisplayStart=0;}}
else
{_fnLog(oSettings,0,"Unknown paging action: "+mAction);}
$(oSettings.oInstance).trigger('page',oSettings);return iOldStart!=oSettings._iDisplayStart;}
function _fnFeatureHtmlProcessing(oSettings)
{var nProcessing=document.createElement('div');if(!oSettings.aanFeatures.r)
{nProcessing.id=oSettings.sTableId+'_processing';}
nProcessing.innerHTML=oSettings.oLanguage.sProcessing;nProcessing.className=oSettings.oClasses.sProcessing;oSettings.nTable.parentNode.insertBefore(nProcessing,oSettings.nTable);return nProcessing;}
function _fnProcessingDisplay(oSettings,bShow)
{if(oSettings.oFeatures.bProcessing)
{var an=oSettings.aanFeatures.r;for(var i=0,iLen=an.length;i<iLen;i++)
{an[i].style.visibility=bShow?"visible":"hidden";}}
$(oSettings.oInstance).trigger('processing',[oSettings,bShow]);}
function _fnFeatureHtmlTable(oSettings)
{if(oSettings.oScroll.sX===""&&oSettings.oScroll.sY==="")
{return oSettings.nTable;}
var
nScroller=document.createElement('div'),nScrollHead=document.createElement('div'),nScrollHeadInner=document.createElement('div'),nScrollBody=document.createElement('div'),nScrollFoot=document.createElement('div'),nScrollFootInner=document.createElement('div'),nScrollHeadTable=oSettings.nTable.cloneNode(false),nScrollFootTable=oSettings.nTable.cloneNode(false),nThead=oSettings.nTable.getElementsByTagName('thead')[0],nTfoot=oSettings.nTable.getElementsByTagName('tfoot').length===0?null:oSettings.nTable.getElementsByTagName('tfoot')[0],oClasses=oSettings.oClasses;nScrollHead.appendChild(nScrollHeadInner);nScrollFoot.appendChild(nScrollFootInner);nScrollBody.appendChild(oSettings.nTable);nScroller.appendChild(nScrollHead);nScroller.appendChild(nScrollBody);nScrollHeadInner.appendChild(nScrollHeadTable);nScrollHeadTable.appendChild(nThead);if(nTfoot!==null)
{nScroller.appendChild(nScrollFoot);nScrollFootInner.appendChild(nScrollFootTable);nScrollFootTable.appendChild(nTfoot);}
nScroller.className=oClasses.sScrollWrapper;nScrollHead.className=oClasses.sScrollHead;nScrollHeadInner.className=oClasses.sScrollHeadInner;nScrollBody.className=oClasses.sScrollBody;nScrollFoot.className=oClasses.sScrollFoot;nScrollFootInner.className=oClasses.sScrollFootInner;if(oSettings.oScroll.bAutoCss)
{nScrollHead.style.overflow="hidden";nScrollHead.style.position="relative";nScrollFoot.style.overflow="hidden";nScrollBody.style.overflow="auto";}
nScrollHead.style.border="0";nScrollHead.style.width="100%";nScrollFoot.style.border="0";nScrollHeadInner.style.width=oSettings.oScroll.sXInner!==""?oSettings.oScroll.sXInner:"100%";nScrollHeadTable.removeAttribute('id');nScrollHeadTable.style.marginLeft="0";oSettings.nTable.style.marginLeft="0";if(nTfoot!==null)
{nScrollFootTable.removeAttribute('id');nScrollFootTable.style.marginLeft="0";}
var nCaption=$(oSettings.nTable).children('caption');if(nCaption.length>0)
{nCaption=nCaption[0];if(nCaption._captionSide==="top")
{nScrollHeadTable.appendChild(nCaption);}
else if(nCaption._captionSide==="bottom"&&nTfoot)
{nScrollFootTable.appendChild(nCaption);}}
if(oSettings.oScroll.sX!=="")
{nScrollHead.style.width=_fnStringToCss(oSettings.oScroll.sX);nScrollBody.style.width=_fnStringToCss(oSettings.oScroll.sX);if(nTfoot!==null)
{nScrollFoot.style.width=_fnStringToCss(oSettings.oScroll.sX);}
$(nScrollBody).scroll(function(e){nScrollHead.scrollLeft=this.scrollLeft;if(nTfoot!==null)
{nScrollFoot.scrollLeft=this.scrollLeft;}});}
if(oSettings.oScroll.sY!=="")
{nScrollBody.style.height=_fnStringToCss(oSettings.oScroll.sY);}
oSettings.aoDrawCallback.push({"fn":_fnScrollDraw,"sName":"scrolling"});if(oSettings.oScroll.bInfinite)
{$(nScrollBody).scroll(function(){if(!oSettings.bDrawing&&$(this).scrollTop()!==0)
{if($(this).scrollTop()+$(this).height()>$(oSettings.nTable).height()-oSettings.oScroll.iLoadGap)
{if(oSettings.fnDisplayEnd()<oSettings.fnRecordsDisplay())
{_fnPageChange(oSettings,'next');_fnCalculateEnd(oSettings);_fnDraw(oSettings);}}}});}
oSettings.nScrollHead=nScrollHead;oSettings.nScrollFoot=nScrollFoot;return nScroller;}
function _fnScrollDraw(o)
{var
nScrollHeadInner=o.nScrollHead.getElementsByTagName('div')[0],nScrollHeadTable=nScrollHeadInner.getElementsByTagName('table')[0],nScrollBody=o.nTable.parentNode,i,iLen,j,jLen,anHeadToSize,anHeadSizers,anFootSizers,anFootToSize,oStyle,iVis,nTheadSize,nTfootSize,iWidth,aApplied=[],aAppliedFooter=[],iSanityWidth,nScrollFootInner=(o.nTFoot!==null)?o.nScrollFoot.getElementsByTagName('div')[0]:null,nScrollFootTable=(o.nTFoot!==null)?nScrollFootInner.getElementsByTagName('table')[0]:null,ie67=o.oBrowser.bScrollOversize,zeroOut=function(nSizer){oStyle=nSizer.style;oStyle.paddingTop="0";oStyle.paddingBottom="0";oStyle.borderTopWidth="0";oStyle.borderBottomWidth="0";oStyle.height=0;};$(o.nTable).children('thead, tfoot').remove();nTheadSize=$(o.nTHead).clone()[0];o.nTable.insertBefore(nTheadSize,o.nTable.childNodes[0]);anHeadToSize=o.nTHead.getElementsByTagName('tr');anHeadSizers=nTheadSize.getElementsByTagName('tr');if(o.nTFoot!==null)
{nTfootSize=$(o.nTFoot).clone()[0];o.nTable.insertBefore(nTfootSize,o.nTable.childNodes[1]);anFootToSize=o.nTFoot.getElementsByTagName('tr');anFootSizers=nTfootSize.getElementsByTagName('tr');}
if(o.oScroll.sX==="")
{nScrollBody.style.width='100%';nScrollHeadInner.parentNode.style.width='100%';}
var nThs=_fnGetUniqueThs(o,nTheadSize);for(i=0,iLen=nThs.length;i<iLen;i++)
{iVis=_fnVisibleToColumnIndex(o,i);nThs[i].style.width=o.aoColumns[iVis].sWidth;}
if(o.nTFoot!==null)
{_fnApplyToChildren(function(n){n.style.width="";},anFootSizers);}
if(o.oScroll.bCollapse&&o.oScroll.sY!=="")
{nScrollBody.style.height=(nScrollBody.offsetHeight+o.nTHead.offsetHeight)+"px";}
iSanityWidth=$(o.nTable).outerWidth();if(o.oScroll.sX==="")
{o.nTable.style.width="100%";if(ie67&&($('tbody',nScrollBody).height()>nScrollBody.offsetHeight||$(nScrollBody).css('overflow-y')=="scroll"))
{o.nTable.style.width=_fnStringToCss($(o.nTable).outerWidth()-o.oScroll.iBarWidth);}}
else
{if(o.oScroll.sXInner!=="")
{o.nTable.style.width=_fnStringToCss(o.oScroll.sXInner);}
else if(iSanityWidth==$(nScrollBody).width()&&$(nScrollBody).height()<$(o.nTable).height())
{o.nTable.style.width=_fnStringToCss(iSanityWidth-o.oScroll.iBarWidth);if($(o.nTable).outerWidth()>iSanityWidth-o.oScroll.iBarWidth)
{o.nTable.style.width=_fnStringToCss(iSanityWidth);}}
else
{o.nTable.style.width=_fnStringToCss(iSanityWidth);}}
iSanityWidth=$(o.nTable).outerWidth();_fnApplyToChildren(zeroOut,anHeadSizers);_fnApplyToChildren(function(nSizer){aApplied.push(_fnStringToCss($(nSizer).width()));},anHeadSizers);_fnApplyToChildren(function(nToSize,i){nToSize.style.width=aApplied[i];},anHeadToSize);$(anHeadSizers).height(0);if(o.nTFoot!==null)
{_fnApplyToChildren(zeroOut,anFootSizers);_fnApplyToChildren(function(nSizer){aAppliedFooter.push(_fnStringToCss($(nSizer).width()));},anFootSizers);_fnApplyToChildren(function(nToSize,i){nToSize.style.width=aAppliedFooter[i];},anFootToSize);$(anFootSizers).height(0);}
_fnApplyToChildren(function(nSizer,i){nSizer.innerHTML="";nSizer.style.width=aApplied[i];},anHeadSizers);if(o.nTFoot!==null)
{_fnApplyToChildren(function(nSizer,i){nSizer.innerHTML="";nSizer.style.width=aAppliedFooter[i];},anFootSizers);}
if($(o.nTable).outerWidth()<iSanityWidth)
{var iCorrection=((nScrollBody.scrollHeight>nScrollBody.offsetHeight||$(nScrollBody).css('overflow-y')=="scroll"))?iSanityWidth+o.oScroll.iBarWidth:iSanityWidth;if(ie67&&(nScrollBody.scrollHeight>nScrollBody.offsetHeight||$(nScrollBody).css('overflow-y')=="scroll"))
{o.nTable.style.width=_fnStringToCss(iCorrection-o.oScroll.iBarWidth);}
nScrollBody.style.width=_fnStringToCss(iCorrection);o.nScrollHead.style.width=_fnStringToCss(iCorrection);if(o.nTFoot!==null)
{o.nScrollFoot.style.width=_fnStringToCss(iCorrection);}
if(o.oScroll.sX==="")
{_fnLog(o,1,"The table cannot fit into the current element which will cause column"+" misalignment. The table has been drawn at its minimum possible width.");}
else if(o.oScroll.sXInner!=="")
{_fnLog(o,1,"The table cannot fit into the current element which will cause column"+" misalignment. Increase the sScrollXInner value or remove it to allow automatic"+" calculation");}}
else
{nScrollBody.style.width=_fnStringToCss('100%');o.nScrollHead.style.width=_fnStringToCss('100%');if(o.nTFoot!==null)
{o.nScrollFoot.style.width=_fnStringToCss('100%');}}
if(o.oScroll.sY==="")
{if(ie67)
{nScrollBody.style.height=_fnStringToCss(o.nTable.offsetHeight+o.oScroll.iBarWidth);}}
if(o.oScroll.sY!==""&&o.oScroll.bCollapse)
{nScrollBody.style.height=_fnStringToCss(o.oScroll.sY);var iExtra=(o.oScroll.sX!==""&&o.nTable.offsetWidth>nScrollBody.offsetWidth)?o.oScroll.iBarWidth:0;if(o.nTable.offsetHeight<nScrollBody.offsetHeight)
{nScrollBody.style.height=_fnStringToCss(o.nTable.offsetHeight+iExtra);}}
var iOuterWidth=$(o.nTable).outerWidth();nScrollHeadTable.style.width=_fnStringToCss(iOuterWidth);nScrollHeadInner.style.width=_fnStringToCss(iOuterWidth);var bScrolling=$(o.nTable).height()>nScrollBody.clientHeight||$(nScrollBody).css('overflow-y')=="scroll";nScrollHeadInner.style.paddingRight=bScrolling?o.oScroll.iBarWidth+"px":"0px";if(o.nTFoot!==null)
{nScrollFootTable.style.width=_fnStringToCss(iOuterWidth);nScrollFootInner.style.width=_fnStringToCss(iOuterWidth);nScrollFootInner.style.paddingRight=bScrolling?o.oScroll.iBarWidth+"px":"0px";}
$(nScrollBody).scroll();if(o.bSorted||o.bFiltered)
{nScrollBody.scrollTop=0;}}
function _fnApplyToChildren(fn,an1,an2)
{var index=0,i=0,iLen=an1.length;var nNode1,nNode2;while(i<iLen)
{nNode1=an1[i].firstChild;nNode2=an2?an2[i].firstChild:null;while(nNode1)
{if(nNode1.nodeType===1)
{if(an2)
{fn(nNode1,nNode2,index);}
else
{fn(nNode1,index);}
index++;}
nNode1=nNode1.nextSibling;nNode2=an2?nNode2.nextSibling:null;}
i++;}}
function _fnConvertToWidth(sWidth,nParent)
{if(!sWidth||sWidth===null||sWidth==='')
{return 0;}
if(!nParent)
{nParent=document.body;}
var iWidth;var nTmp=document.createElement("div");nTmp.style.width=_fnStringToCss(sWidth);nParent.appendChild(nTmp);iWidth=nTmp.offsetWidth;nParent.removeChild(nTmp);return(iWidth);}
function _fnCalculateColumnWidths(oSettings)
{var iTableWidth=oSettings.nTable.offsetWidth;var iUserInputs=0;var iTmpWidth;var iVisibleColumns=0;var iColums=oSettings.aoColumns.length;var i,iIndex,iCorrector,iWidth;var oHeaders=$('th',oSettings.nTHead);var widthAttr=oSettings.nTable.getAttribute('width');var nWrapper=oSettings.nTable.parentNode;for(i=0;i<iColums;i++)
{if(oSettings.aoColumns[i].bVisible)
{iVisibleColumns++;if(oSettings.aoColumns[i].sWidth!==null)
{iTmpWidth=_fnConvertToWidth(oSettings.aoColumns[i].sWidthOrig,nWrapper);if(iTmpWidth!==null)
{oSettings.aoColumns[i].sWidth=_fnStringToCss(iTmpWidth);}
iUserInputs++;}}}
if(iColums==oHeaders.length&&iUserInputs===0&&iVisibleColumns==iColums&&oSettings.oScroll.sX===""&&oSettings.oScroll.sY==="")
{for(i=0;i<oSettings.aoColumns.length;i++)
{iTmpWidth=$(oHeaders[i]).width();if(iTmpWidth!==null)
{oSettings.aoColumns[i].sWidth=_fnStringToCss(iTmpWidth);}}}
else
{var
nCalcTmp=oSettings.nTable.cloneNode(false),nTheadClone=oSettings.nTHead.cloneNode(true),nBody=document.createElement('tbody'),nTr=document.createElement('tr'),nDivSizing;nCalcTmp.removeAttribute("id");nCalcTmp.appendChild(nTheadClone);if(oSettings.nTFoot!==null)
{nCalcTmp.appendChild(oSettings.nTFoot.cloneNode(true));_fnApplyToChildren(function(n){n.style.width="";},nCalcTmp.getElementsByTagName('tr'));}
nCalcTmp.appendChild(nBody);nBody.appendChild(nTr);var jqColSizing=$('thead th',nCalcTmp);if(jqColSizing.length===0)
{jqColSizing=$('tbody tr:eq(0)>td',nCalcTmp);}
var nThs=_fnGetUniqueThs(oSettings,nTheadClone);iCorrector=0;for(i=0;i<iColums;i++)
{var oColumn=oSettings.aoColumns[i];if(oColumn.bVisible&&oColumn.sWidthOrig!==null&&oColumn.sWidthOrig!=="")
{nThs[i-iCorrector].style.width=_fnStringToCss(oColumn.sWidthOrig);}
else if(oColumn.bVisible)
{nThs[i-iCorrector].style.width="";}
else
{iCorrector++;}}
for(i=0;i<iColums;i++)
{if(oSettings.aoColumns[i].bVisible)
{var nTd=_fnGetWidestNode(oSettings,i);if(nTd!==null)
{nTd=nTd.cloneNode(true);if(oSettings.aoColumns[i].sContentPadding!=="")
{nTd.innerHTML+=oSettings.aoColumns[i].sContentPadding;}
nTr.appendChild(nTd);}}}
nWrapper.appendChild(nCalcTmp);if(oSettings.oScroll.sX!==""&&oSettings.oScroll.sXInner!=="")
{nCalcTmp.style.width=_fnStringToCss(oSettings.oScroll.sXInner);}
else if(oSettings.oScroll.sX!=="")
{nCalcTmp.style.width="";if($(nCalcTmp).width()<nWrapper.offsetWidth)
{nCalcTmp.style.width=_fnStringToCss(nWrapper.offsetWidth);}}
else if(oSettings.oScroll.sY!=="")
{nCalcTmp.style.width=_fnStringToCss(nWrapper.offsetWidth);}
else if(widthAttr)
{nCalcTmp.style.width=_fnStringToCss(widthAttr);}
nCalcTmp.style.visibility="hidden";_fnScrollingWidthAdjust(oSettings,nCalcTmp);var oNodes=$("tbody tr:eq(0)",nCalcTmp).children();if(oNodes.length===0)
{oNodes=_fnGetUniqueThs(oSettings,$('thead',nCalcTmp)[0]);}
if(oSettings.oScroll.sX!=="")
{var iTotal=0;iCorrector=0;for(i=0;i<oSettings.aoColumns.length;i++)
{if(oSettings.aoColumns[i].bVisible)
{if(oSettings.aoColumns[i].sWidthOrig===null)
{iTotal+=$(oNodes[iCorrector]).outerWidth();}
else
{iTotal+=parseInt(oSettings.aoColumns[i].sWidth.replace('px',''),10)+
($(oNodes[iCorrector]).outerWidth()-$(oNodes[iCorrector]).width());}
iCorrector++;}}
nCalcTmp.style.width=_fnStringToCss(iTotal);oSettings.nTable.style.width=_fnStringToCss(iTotal);}
iCorrector=0;for(i=0;i<oSettings.aoColumns.length;i++)
{if(oSettings.aoColumns[i].bVisible)
{iWidth=$(oNodes[iCorrector]).width();if(iWidth!==null&&iWidth>0)
{oSettings.aoColumns[i].sWidth=_fnStringToCss(iWidth);}
iCorrector++;}}
var cssWidth=$(nCalcTmp).css('width');oSettings.nTable.style.width=(cssWidth.indexOf('%')!==-1)?cssWidth:_fnStringToCss($(nCalcTmp).outerWidth());nCalcTmp.parentNode.removeChild(nCalcTmp);}
if(widthAttr)
{oSettings.nTable.style.width=_fnStringToCss(widthAttr);}}
function _fnScrollingWidthAdjust(oSettings,n)
{if(oSettings.oScroll.sX===""&&oSettings.oScroll.sY!=="")
{var iOrigWidth=$(n).width();n.style.width=_fnStringToCss($(n).outerWidth()-oSettings.oScroll.iBarWidth);}
else if(oSettings.oScroll.sX!=="")
{n.style.width=_fnStringToCss($(n).outerWidth());}}
function _fnGetWidestNode(oSettings,iCol)
{var iMaxIndex=_fnGetMaxLenString(oSettings,iCol);if(iMaxIndex<0)
{return null;}
if(oSettings.aoData[iMaxIndex].nTr===null)
{var n=document.createElement('td');n.innerHTML=_fnGetCellData(oSettings,iMaxIndex,iCol,'');return n;}
return _fnGetTdNodes(oSettings,iMaxIndex)[iCol];}
function _fnGetMaxLenString(oSettings,iCol)
{var iMax=-1;var iMaxIndex=-1;for(var i=0;i<oSettings.aoData.length;i++)
{var s=_fnGetCellData(oSettings,i,iCol,'display')+"";s=s.replace(/<.*?>/g,"");if(s.length>iMax)
{iMax=s.length;iMaxIndex=i;}}
return iMaxIndex;}
function _fnStringToCss(s)
{if(s===null)
{return"0px";}
if(typeof s=='number')
{if(s<0)
{return"0px";}
return s+"px";}
var c=s.charCodeAt(s.length-1);if(c<0x30||c>0x39)
{return s;}
return s+"px";}
function _fnScrollBarWidth()
{var inner=document.createElement('p');var style=inner.style;style.width="100%";style.height="200px";style.padding="0px";var outer=document.createElement('div');style=outer.style;style.position="absolute";style.top="0px";style.left="0px";style.visibility="hidden";style.width="200px";style.height="150px";style.padding="0px";style.overflow="hidden";outer.appendChild(inner);document.body.appendChild(outer);var w1=inner.offsetWidth;outer.style.overflow='scroll';var w2=inner.offsetWidth;if(w1==w2)
{w2=outer.clientWidth;}
document.body.removeChild(outer);return(w1-w2);}
function _fnSort(oSettings,bApplyClasses)
{var
i,iLen,j,jLen,k,kLen,sDataType,nTh,aaSort=[],aiOrig=[],oSort=DataTable.ext.oSort,aoData=oSettings.aoData,aoColumns=oSettings.aoColumns,oAria=oSettings.oLanguage.oAria;if(!oSettings.oFeatures.bServerSide&&(oSettings.aaSorting.length!==0||oSettings.aaSortingFixed!==null))
{aaSort=(oSettings.aaSortingFixed!==null)?oSettings.aaSortingFixed.concat(oSettings.aaSorting):oSettings.aaSorting.slice();for(i=0;i<aaSort.length;i++)
{var iColumn=aaSort[i][0];var iVisColumn=_fnColumnIndexToVisible(oSettings,iColumn);sDataType=oSettings.aoColumns[iColumn].sSortDataType;if(DataTable.ext.afnSortData[sDataType])
{var aData=DataTable.ext.afnSortData[sDataType].call(oSettings.oInstance,oSettings,iColumn,iVisColumn);if(aData.length===aoData.length)
{for(j=0,jLen=aoData.length;j<jLen;j++)
{_fnSetCellData(oSettings,j,iColumn,aData[j]);}}
else
{_fnLog(oSettings,0,"Returned data sort array (col "+iColumn+") is the wrong length");}}}
for(i=0,iLen=oSettings.aiDisplayMaster.length;i<iLen;i++)
{aiOrig[oSettings.aiDisplayMaster[i]]=i;}
var iSortLen=aaSort.length;var fnSortFormat,aDataSort;for(i=0,iLen=aoData.length;i<iLen;i++)
{for(j=0;j<iSortLen;j++)
{aDataSort=aoColumns[aaSort[j][0]].aDataSort;for(k=0,kLen=aDataSort.length;k<kLen;k++)
{sDataType=aoColumns[aDataSort[k]].sType;fnSortFormat=oSort[(sDataType?sDataType:'string')+"-pre"];aoData[i]._aSortData[aDataSort[k]]=fnSortFormat?fnSortFormat(_fnGetCellData(oSettings,i,aDataSort[k],'sort')):_fnGetCellData(oSettings,i,aDataSort[k],'sort');}}}
oSettings.aiDisplayMaster.sort(function(a,b){var k,l,lLen,iTest,aDataSort,sDataType;for(k=0;k<iSortLen;k++)
{aDataSort=aoColumns[aaSort[k][0]].aDataSort;for(l=0,lLen=aDataSort.length;l<lLen;l++)
{sDataType=aoColumns[aDataSort[l]].sType;iTest=oSort[(sDataType?sDataType:'string')+"-"+aaSort[k][1]](aoData[a]._aSortData[aDataSort[l]],aoData[b]._aSortData[aDataSort[l]]);if(iTest!==0)
{return iTest;}}}
return oSort['numeric-asc'](aiOrig[a],aiOrig[b]);});}
if((bApplyClasses===undefined||bApplyClasses)&&!oSettings.oFeatures.bDeferRender)
{_fnSortingClasses(oSettings);}
for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{var sTitle=aoColumns[i].sTitle.replace(/<.*?>/g,"");nTh=aoColumns[i].nTh;nTh.removeAttribute('aria-sort');nTh.removeAttribute('aria-label');if(aoColumns[i].bSortable)
{if(aaSort.length>0&&aaSort[0][0]==i)
{nTh.setAttribute('aria-sort',aaSort[0][1]=="asc"?"ascending":"descending");var nextSort=(aoColumns[i].asSorting[aaSort[0][2]+1])?aoColumns[i].asSorting[aaSort[0][2]+1]:aoColumns[i].asSorting[0];nTh.setAttribute('aria-label',sTitle+
(nextSort=="asc"?oAria.sSortAscending:oAria.sSortDescending));}
else
{nTh.setAttribute('aria-label',sTitle+
(aoColumns[i].asSorting[0]=="asc"?oAria.sSortAscending:oAria.sSortDescending));}}
else
{nTh.setAttribute('aria-label',sTitle);}}
oSettings.bSorted=true;$(oSettings.oInstance).trigger('sort',oSettings);if(oSettings.oFeatures.bFilter)
{_fnFilterComplete(oSettings,oSettings.oPreviousSearch,1);}
else
{oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();oSettings._iDisplayStart=0;_fnCalculateEnd(oSettings);_fnDraw(oSettings);}}
function _fnSortAttachListener(oSettings,nNode,iDataIndex,fnCallback)
{_fnBindAction(nNode,{},function(e){if(oSettings.aoColumns[iDataIndex].bSortable===false)
{return;}
var fnInnerSorting=function(){var iColumn,iNextSort;if(e.shiftKey)
{var bFound=false;for(var i=0;i<oSettings.aaSorting.length;i++)
{if(oSettings.aaSorting[i][0]==iDataIndex)
{bFound=true;iColumn=oSettings.aaSorting[i][0];iNextSort=oSettings.aaSorting[i][2]+1;if(!oSettings.aoColumns[iColumn].asSorting[iNextSort])
{oSettings.aaSorting.splice(i,1);}
else
{oSettings.aaSorting[i][1]=oSettings.aoColumns[iColumn].asSorting[iNextSort];oSettings.aaSorting[i][2]=iNextSort;}
break;}}
if(bFound===false)
{oSettings.aaSorting.push([iDataIndex,oSettings.aoColumns[iDataIndex].asSorting[0],0]);}}
else
{if(oSettings.aaSorting.length==1&&oSettings.aaSorting[0][0]==iDataIndex)
{iColumn=oSettings.aaSorting[0][0];iNextSort=oSettings.aaSorting[0][2]+1;if(!oSettings.aoColumns[iColumn].asSorting[iNextSort])
{iNextSort=0;}
oSettings.aaSorting[0][1]=oSettings.aoColumns[iColumn].asSorting[iNextSort];oSettings.aaSorting[0][2]=iNextSort;}
else
{oSettings.aaSorting.splice(0,oSettings.aaSorting.length);oSettings.aaSorting.push([iDataIndex,oSettings.aoColumns[iDataIndex].asSorting[0],0]);}}
_fnSort(oSettings);};if(!oSettings.oFeatures.bProcessing)
{fnInnerSorting();}
else
{_fnProcessingDisplay(oSettings,true);setTimeout(function(){fnInnerSorting();if(!oSettings.oFeatures.bServerSide)
{_fnProcessingDisplay(oSettings,false);}},0);}
if(typeof fnCallback=='function')
{fnCallback(oSettings);}});}
function _fnSortingClasses(oSettings)
{var i,iLen,j,jLen,iFound;var aaSort,sClass;var iColumns=oSettings.aoColumns.length;var oClasses=oSettings.oClasses;for(i=0;i<iColumns;i++)
{if(oSettings.aoColumns[i].bSortable)
{$(oSettings.aoColumns[i].nTh).removeClass(oClasses.sSortAsc+" "+oClasses.sSortDesc+" "+oSettings.aoColumns[i].sSortingClass);}}
if(oSettings.aaSortingFixed!==null)
{aaSort=oSettings.aaSortingFixed.concat(oSettings.aaSorting);}
else
{aaSort=oSettings.aaSorting.slice();}
for(i=0;i<oSettings.aoColumns.length;i++)
{if(oSettings.aoColumns[i].bSortable)
{sClass=oSettings.aoColumns[i].sSortingClass;iFound=-1;for(j=0;j<aaSort.length;j++)
{if(aaSort[j][0]==i)
{sClass=(aaSort[j][1]=="asc")?oClasses.sSortAsc:oClasses.sSortDesc;iFound=j;break;}}
$(oSettings.aoColumns[i].nTh).addClass(sClass);if(oSettings.bJUI)
{var jqSpan=$("span."+oClasses.sSortIcon,oSettings.aoColumns[i].nTh);jqSpan.removeClass(oClasses.sSortJUIAsc+" "+oClasses.sSortJUIDesc+" "+
oClasses.sSortJUI+" "+oClasses.sSortJUIAscAllowed+" "+oClasses.sSortJUIDescAllowed);var sSpanClass;if(iFound==-1)
{sSpanClass=oSettings.aoColumns[i].sSortingClassJUI;}
else if(aaSort[iFound][1]=="asc")
{sSpanClass=oClasses.sSortJUIAsc;}
else
{sSpanClass=oClasses.sSortJUIDesc;}
jqSpan.addClass(sSpanClass);}}
else
{$(oSettings.aoColumns[i].nTh).addClass(oSettings.aoColumns[i].sSortingClass);}}
sClass=oClasses.sSortColumn;if(oSettings.oFeatures.bSort&&oSettings.oFeatures.bSortClasses)
{var nTds=_fnGetTdNodes(oSettings);var iClass,iTargetCol;var asClasses=[];for(i=0;i<iColumns;i++)
{asClasses.push("");}
for(i=0,iClass=1;i<aaSort.length;i++)
{iTargetCol=parseInt(aaSort[i][0],10);asClasses[iTargetCol]=sClass+iClass;if(iClass<3)
{iClass++;}}
var reClass=new RegExp(sClass+"[123]");var sTmpClass,sCurrentClass,sNewClass;for(i=0,iLen=nTds.length;i<iLen;i++)
{iTargetCol=i%iColumns;sCurrentClass=nTds[i].className;sNewClass=asClasses[iTargetCol];sTmpClass=sCurrentClass.replace(reClass,sNewClass);if(sTmpClass!=sCurrentClass)
{nTds[i].className=$.trim(sTmpClass);}
else if(sNewClass.length>0&&sCurrentClass.indexOf(sNewClass)==-1)
{nTds[i].className=sCurrentClass+" "+sNewClass;}}}}
function _fnSaveState(oSettings)
{if(!oSettings.oFeatures.bStateSave||oSettings.bDestroying)
{return;}
var i,iLen,bInfinite=oSettings.oScroll.bInfinite;var oState={"iCreate":new Date().getTime(),"iStart":(bInfinite?0:oSettings._iDisplayStart),"iEnd":(bInfinite?oSettings._iDisplayLength:oSettings._iDisplayEnd),"iLength":oSettings._iDisplayLength,"aaSorting":$.extend(true,[],oSettings.aaSorting),"oSearch":$.extend(true,{},oSettings.oPreviousSearch),"aoSearchCols":$.extend(true,[],oSettings.aoPreSearchCols),"abVisCols":[]};for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{oState.abVisCols.push(oSettings.aoColumns[i].bVisible);}
_fnCallbackFire(oSettings,"aoStateSaveParams",'stateSaveParams',[oSettings,oState]);oSettings.fnStateSave.call(oSettings.oInstance,oSettings,oState);}
function _fnLoadState(oSettings,oInit)
{if(!oSettings.oFeatures.bStateSave)
{return;}
var oData=oSettings.fnStateLoad.call(oSettings.oInstance,oSettings);if(!oData)
{return;}
var abStateLoad=_fnCallbackFire(oSettings,'aoStateLoadParams','stateLoadParams',[oSettings,oData]);if($.inArray(false,abStateLoad)!==-1)
{return;}
oSettings.oLoadedState=$.extend(true,{},oData);oSettings._iDisplayStart=oData.iStart;oSettings.iInitDisplayStart=oData.iStart;oSettings._iDisplayEnd=oData.iEnd;oSettings._iDisplayLength=oData.iLength;oSettings.aaSorting=oData.aaSorting.slice();oSettings.saved_aaSorting=oData.aaSorting.slice();$.extend(oSettings.oPreviousSearch,oData.oSearch);$.extend(true,oSettings.aoPreSearchCols,oData.aoSearchCols);oInit.saved_aoColumns=[];for(var i=0;i<oData.abVisCols.length;i++)
{oInit.saved_aoColumns[i]={};oInit.saved_aoColumns[i].bVisible=oData.abVisCols[i];}
_fnCallbackFire(oSettings,'aoStateLoaded','stateLoaded',[oSettings,oData]);}
function _fnCreateCookie(sName,sValue,iSecs,sBaseName,fnCallback)
{var date=new Date();date.setTime(date.getTime()+(iSecs*1000));var aParts=window.location.pathname.split('/');var sNameFile=sName+'_'+aParts.pop().replace(/[\/:]/g,"").toLowerCase();var sFullCookie,oData;if(fnCallback!==null)
{oData=(typeof $.parseJSON==='function')?$.parseJSON(sValue):eval('('+sValue+')');sFullCookie=fnCallback(sNameFile,oData,date.toGMTString(),aParts.join('/')+"/");}
else
{sFullCookie=sNameFile+"="+encodeURIComponent(sValue)+"; expires="+date.toGMTString()+"; path="+aParts.join('/')+"/";}
var
aCookies=document.cookie.split(';'),iNewCookieLen=sFullCookie.split(';')[0].length,aOldCookies=[];if(iNewCookieLen+document.cookie.length+10>4096)
{for(var i=0,iLen=aCookies.length;i<iLen;i++)
{if(aCookies[i].indexOf(sBaseName)!=-1)
{var aSplitCookie=aCookies[i].split('=');try{oData=eval('('+decodeURIComponent(aSplitCookie[1])+')');if(oData&&oData.iCreate)
{aOldCookies.push({"name":aSplitCookie[0],"time":oData.iCreate});}}
catch(e){}}}
aOldCookies.sort(function(a,b){return b.time-a.time;});while(iNewCookieLen+document.cookie.length+10>4096){if(aOldCookies.length===0){return;}
var old=aOldCookies.pop();document.cookie=old.name+"=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path="+
aParts.join('/')+"/";}}
document.cookie=sFullCookie;}
function _fnReadCookie(sName)
{var
aParts=window.location.pathname.split('/'),sNameEQ=sName+'_'+aParts[aParts.length-1].replace(/[\/:]/g,"").toLowerCase()+'=',sCookieContents=document.cookie.split(';');for(var i=0;i<sCookieContents.length;i++)
{var c=sCookieContents[i];while(c.charAt(0)==' ')
{c=c.substring(1,c.length);}
if(c.indexOf(sNameEQ)===0)
{return decodeURIComponent(c.substring(sNameEQ.length,c.length));}}
return null;}
function _fnSettingsFromNode(nTable)
{for(var i=0;i<DataTable.settings.length;i++)
{if(DataTable.settings[i].nTable===nTable)
{return DataTable.settings[i];}}
return null;}
function _fnGetTrNodes(oSettings)
{var aNodes=[];var aoData=oSettings.aoData;for(var i=0,iLen=aoData.length;i<iLen;i++)
{if(aoData[i].nTr!==null)
{aNodes.push(aoData[i].nTr);}}
return aNodes;}
function _fnGetTdNodes(oSettings,iIndividualRow)
{var anReturn=[];var iCorrector;var anTds,nTd;var iRow,iRows=oSettings.aoData.length,iColumn,iColumns,oData,sNodeName,iStart=0,iEnd=iRows;if(iIndividualRow!==undefined)
{iStart=iIndividualRow;iEnd=iIndividualRow+1;}
for(iRow=iStart;iRow<iEnd;iRow++)
{oData=oSettings.aoData[iRow];if(oData.nTr!==null)
{anTds=[];nTd=oData.nTr.firstChild;while(nTd)
{sNodeName=nTd.nodeName.toLowerCase();if(sNodeName=='td'||sNodeName=='th')
{anTds.push(nTd);}
nTd=nTd.nextSibling;}
iCorrector=0;for(iColumn=0,iColumns=oSettings.aoColumns.length;iColumn<iColumns;iColumn++)
{if(oSettings.aoColumns[iColumn].bVisible)
{anReturn.push(anTds[iColumn-iCorrector]);}
else
{anReturn.push(oData._anHidden[iColumn]);iCorrector++;}}}}
return anReturn;}
function _fnLog(oSettings,iLevel,sMesg)
{var sAlert=(oSettings===null)?"DataTables warning: "+sMesg:"DataTables warning (table id = '"+oSettings.sTableId+"'): "+sMesg;if(iLevel===0)
{if(DataTable.ext.sErrMode=='alert')
{alert(sAlert);}
else
{throw new Error(sAlert);}
return;}
else if(window.console&&console.log)
{console.log(sAlert);}}
function _fnMap(oRet,oSrc,sName,sMappedName)
{if(sMappedName===undefined)
{sMappedName=sName;}
if(oSrc[sName]!==undefined)
{oRet[sMappedName]=oSrc[sName];}}
function _fnExtend(oOut,oExtender)
{var val;for(var prop in oExtender)
{if(oExtender.hasOwnProperty(prop))
{val=oExtender[prop];if(typeof oInit[prop]==='object'&&val!==null&&$.isArray(val)===false)
{$.extend(true,oOut[prop],val);}
else
{oOut[prop]=val;}}}
return oOut;}
function _fnBindAction(n,oData,fn)
{$(n).bind('click.DT',oData,function(e){n.blur();fn(e);}).bind('keypress.DT',oData,function(e){if(e.which===13){fn(e);}}).bind('selectstart.DT',function(){return false;});}
function _fnCallbackReg(oSettings,sStore,fn,sName)
{if(fn)
{oSettings[sStore].push({"fn":fn,"sName":sName});}}
function _fnCallbackFire(oSettings,sStore,sTrigger,aArgs)
{var aoStore=oSettings[sStore];var aRet=[];for(var i=aoStore.length-1;i>=0;i--)
{aRet.push(aoStore[i].fn.apply(oSettings.oInstance,aArgs));}
if(sTrigger!==null)
{$(oSettings.oInstance).trigger(sTrigger,aArgs);}
return aRet;}
var _fnJsonString=(window.JSON)?JSON.stringify:function(o)
{var sType=typeof o;if(sType!=="object"||o===null)
{if(sType==="string")
{o='"'+o+'"';}
return o+"";}
var
sProp,mValue,json=[],bArr=$.isArray(o);for(sProp in o)
{mValue=o[sProp];sType=typeof mValue;if(sType==="string")
{mValue='"'+mValue+'"';}
else if(sType==="object"&&mValue!==null)
{mValue=_fnJsonString(mValue);}
json.push((bArr?"":'"'+sProp+'":')+mValue);}
return(bArr?"[":"{")+json+(bArr?"]":"}");};function _fnBrowserDetect(oSettings)
{var n=$('<div style="position:absolute; top:0; left:0; height:1px; width:1px; overflow:hidden">'+'<div style="position:absolute; top:1px; left:1px; width:100px; overflow:scroll;">'+'<div id="DT_BrowserTest" style="width:100%; height:10px;"></div>'+'</div>'+'</div>')[0];document.body.appendChild(n);oSettings.oBrowser.bScrollOversize=$('#DT_BrowserTest',n)[0].offsetWidth===100?true:false;document.body.removeChild(n);}
this.$=function(sSelector,oOpts)
{var i,iLen,a=[],tr;var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var aoData=oSettings.aoData;var aiDisplay=oSettings.aiDisplay;var aiDisplayMaster=oSettings.aiDisplayMaster;if(!oOpts)
{oOpts={};}
oOpts=$.extend({},{"filter":"none","order":"current","page":"all"},oOpts);if(oOpts.page=='current')
{for(i=oSettings._iDisplayStart,iLen=oSettings.fnDisplayEnd();i<iLen;i++)
{tr=aoData[aiDisplay[i]].nTr;if(tr)
{a.push(tr);}}}
else if(oOpts.order=="current"&&oOpts.filter=="none")
{for(i=0,iLen=aiDisplayMaster.length;i<iLen;i++)
{tr=aoData[aiDisplayMaster[i]].nTr;if(tr)
{a.push(tr);}}}
else if(oOpts.order=="current"&&oOpts.filter=="applied")
{for(i=0,iLen=aiDisplay.length;i<iLen;i++)
{tr=aoData[aiDisplay[i]].nTr;if(tr)
{a.push(tr);}}}
else if(oOpts.order=="original"&&oOpts.filter=="none")
{for(i=0,iLen=aoData.length;i<iLen;i++)
{tr=aoData[i].nTr;if(tr)
{a.push(tr);}}}
else if(oOpts.order=="original"&&oOpts.filter=="applied")
{for(i=0,iLen=aoData.length;i<iLen;i++)
{tr=aoData[i].nTr;if($.inArray(i,aiDisplay)!==-1&&tr)
{a.push(tr);}}}
else
{_fnLog(oSettings,1,"Unknown selection options");}
var jqA=$(a);var jqTRs=jqA.filter(sSelector);var jqDescendants=jqA.find(sSelector);return $([].concat($.makeArray(jqTRs),$.makeArray(jqDescendants)));};this._=function(sSelector,oOpts)
{var aOut=[];var i,iLen,iIndex;var aTrs=this.$(sSelector,oOpts);for(i=0,iLen=aTrs.length;i<iLen;i++)
{aOut.push(this.fnGetData(aTrs[i]));}
return aOut;};this.fnAddData=function(mData,bRedraw)
{if(mData.length===0)
{return[];}
var aiReturn=[];var iTest;var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);if(typeof mData[0]==="object"&&mData[0]!==null)
{for(var i=0;i<mData.length;i++)
{iTest=_fnAddData(oSettings,mData[i]);if(iTest==-1)
{return aiReturn;}
aiReturn.push(iTest);}}
else
{iTest=_fnAddData(oSettings,mData);if(iTest==-1)
{return aiReturn;}
aiReturn.push(iTest);}
oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();if(bRedraw===undefined||bRedraw)
{_fnReDraw(oSettings);}
return aiReturn;};this.fnAdjustColumnSizing=function(bRedraw)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);_fnAdjustColumnSizing(oSettings);if(bRedraw===undefined||bRedraw)
{this.fnDraw(false);}
else if(oSettings.oScroll.sX!==""||oSettings.oScroll.sY!=="")
{this.oApi._fnScrollDraw(oSettings);}};this.fnClearTable=function(bRedraw)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);_fnClearTable(oSettings);if(bRedraw===undefined||bRedraw)
{_fnDraw(oSettings);}};this.fnClose=function(nTr)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);for(var i=0;i<oSettings.aoOpenRows.length;i++)
{if(oSettings.aoOpenRows[i].nParent==nTr)
{var nTrParent=oSettings.aoOpenRows[i].nTr.parentNode;if(nTrParent)
{nTrParent.removeChild(oSettings.aoOpenRows[i].nTr);}
oSettings.aoOpenRows.splice(i,1);return 0;}}
return 1;};this.fnDeleteRow=function(mTarget,fnCallBack,bRedraw)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var i,iLen,iAODataIndex;iAODataIndex=(typeof mTarget==='object')?_fnNodeToDataIndex(oSettings,mTarget):mTarget;var oData=oSettings.aoData.splice(iAODataIndex,1);for(i=0,iLen=oSettings.aoData.length;i<iLen;i++)
{if(oSettings.aoData[i].nTr!==null)
{oSettings.aoData[i].nTr._DT_RowIndex=i;}}
var iDisplayIndex=$.inArray(iAODataIndex,oSettings.aiDisplay);oSettings.asDataSearch.splice(iDisplayIndex,1);_fnDeleteIndex(oSettings.aiDisplayMaster,iAODataIndex);_fnDeleteIndex(oSettings.aiDisplay,iAODataIndex);if(typeof fnCallBack==="function")
{fnCallBack.call(this,oSettings,oData);}
if(oSettings._iDisplayStart>=oSettings.fnRecordsDisplay())
{oSettings._iDisplayStart-=oSettings._iDisplayLength;if(oSettings._iDisplayStart<0)
{oSettings._iDisplayStart=0;}}
if(bRedraw===undefined||bRedraw)
{_fnCalculateEnd(oSettings);_fnDraw(oSettings);}
return oData;};this.fnDestroy=function(bRemove)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var nOrig=oSettings.nTableWrapper.parentNode;var nBody=oSettings.nTBody;var i,iLen;bRemove=(bRemove===undefined)?false:bRemove;oSettings.bDestroying=true;_fnCallbackFire(oSettings,"aoDestroyCallback","destroy",[oSettings]);if(!bRemove)
{for(i=0,iLen=oSettings.aoColumns.length;i<iLen;i++)
{if(oSettings.aoColumns[i].bVisible===false)
{this.fnSetColumnVis(i,true);}}}
$(oSettings.nTableWrapper).find('*').andSelf().unbind('.DT');$('tbody>tr>td.'+oSettings.oClasses.sRowEmpty,oSettings.nTable).parent().remove();if(oSettings.nTable!=oSettings.nTHead.parentNode)
{$(oSettings.nTable).children('thead').remove();oSettings.nTable.appendChild(oSettings.nTHead);}
if(oSettings.nTFoot&&oSettings.nTable!=oSettings.nTFoot.parentNode)
{$(oSettings.nTable).children('tfoot').remove();oSettings.nTable.appendChild(oSettings.nTFoot);}
oSettings.nTable.parentNode.removeChild(oSettings.nTable);$(oSettings.nTableWrapper).remove();oSettings.aaSorting=[];oSettings.aaSortingFixed=[];_fnSortingClasses(oSettings);$(_fnGetTrNodes(oSettings)).removeClass(oSettings.asStripeClasses.join(' '));$('th, td',oSettings.nTHead).removeClass([oSettings.oClasses.sSortable,oSettings.oClasses.sSortableAsc,oSettings.oClasses.sSortableDesc,oSettings.oClasses.sSortableNone].join(' '));if(oSettings.bJUI)
{$('th span.'+oSettings.oClasses.sSortIcon
+', td span.'+oSettings.oClasses.sSortIcon,oSettings.nTHead).remove();$('th, td',oSettings.nTHead).each(function(){var jqWrapper=$('div.'+oSettings.oClasses.sSortJUIWrapper,this);var kids=jqWrapper.contents();$(this).append(kids);jqWrapper.remove();});}
if(!bRemove&&oSettings.nTableReinsertBefore)
{nOrig.insertBefore(oSettings.nTable,oSettings.nTableReinsertBefore);}
else if(!bRemove)
{nOrig.appendChild(oSettings.nTable);}
for(i=0,iLen=oSettings.aoData.length;i<iLen;i++)
{if(oSettings.aoData[i].nTr!==null)
{nBody.appendChild(oSettings.aoData[i].nTr);}}
if(oSettings.oFeatures.bAutoWidth===true)
{oSettings.nTable.style.width=_fnStringToCss(oSettings.sDestroyWidth);}
iLen=oSettings.asDestroyStripes.length;if(iLen)
{var anRows=$(nBody).children('tr');for(i=0;i<iLen;i++)
{anRows.filter(':nth-child('+iLen+'n + '+i+')').addClass(oSettings.asDestroyStripes[i]);}}
for(i=0,iLen=DataTable.settings.length;i<iLen;i++)
{if(DataTable.settings[i]==oSettings)
{DataTable.settings.splice(i,1);}}
oSettings=null;oInit=null;};this.fnDraw=function(bComplete)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);if(bComplete===false)
{_fnCalculateEnd(oSettings);_fnDraw(oSettings);}
else
{_fnReDraw(oSettings);}};this.fnFilter=function(sInput,iColumn,bRegex,bSmart,bShowGlobal,bCaseInsensitive)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);if(!oSettings.oFeatures.bFilter)
{return;}
if(bRegex===undefined||bRegex===null)
{bRegex=false;}
if(bSmart===undefined||bSmart===null)
{bSmart=true;}
if(bShowGlobal===undefined||bShowGlobal===null)
{bShowGlobal=true;}
if(bCaseInsensitive===undefined||bCaseInsensitive===null)
{bCaseInsensitive=true;}
if(iColumn===undefined||iColumn===null)
{_fnFilterComplete(oSettings,{"sSearch":sInput+"","bRegex":bRegex,"bSmart":bSmart,"bCaseInsensitive":bCaseInsensitive},1);if(bShowGlobal&&oSettings.aanFeatures.f)
{var n=oSettings.aanFeatures.f;for(var i=0,iLen=n.length;i<iLen;i++)
{try{if(n[i]._DT_Input!=document.activeElement)
{$(n[i]._DT_Input).val(sInput);}}
catch(e){$(n[i]._DT_Input).val(sInput);}}}}
else
{$.extend(oSettings.aoPreSearchCols[iColumn],{"sSearch":sInput+"","bRegex":bRegex,"bSmart":bSmart,"bCaseInsensitive":bCaseInsensitive});_fnFilterComplete(oSettings,oSettings.oPreviousSearch,1);}};this.fnGetData=function(mRow,iCol)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);if(mRow!==undefined)
{var iRow=mRow;if(typeof mRow==='object')
{var sNode=mRow.nodeName.toLowerCase();if(sNode==="tr")
{iRow=_fnNodeToDataIndex(oSettings,mRow);}
else if(sNode==="td")
{iRow=_fnNodeToDataIndex(oSettings,mRow.parentNode);iCol=_fnNodeToColumnIndex(oSettings,iRow,mRow);}}
if(iCol!==undefined)
{return _fnGetCellData(oSettings,iRow,iCol,'');}
return(oSettings.aoData[iRow]!==undefined)?oSettings.aoData[iRow]._aData:null;}
return _fnGetDataMaster(oSettings);};this.fnGetNodes=function(iRow)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);if(iRow!==undefined){return(oSettings.aoData[iRow]!==undefined)?oSettings.aoData[iRow].nTr:null;}
return _fnGetTrNodes(oSettings);};this.fnGetPosition=function(nNode)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var sNodeName=nNode.nodeName.toUpperCase();if(sNodeName=="TR")
{return _fnNodeToDataIndex(oSettings,nNode);}
else if(sNodeName=="TD"||sNodeName=="TH")
{var iDataIndex=_fnNodeToDataIndex(oSettings,nNode.parentNode);var iColumnIndex=_fnNodeToColumnIndex(oSettings,iDataIndex,nNode);return[iDataIndex,_fnColumnIndexToVisible(oSettings,iColumnIndex),iColumnIndex];}
return null;};this.fnIsOpen=function(nTr)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var aoOpenRows=oSettings.aoOpenRows;for(var i=0;i<oSettings.aoOpenRows.length;i++)
{if(oSettings.aoOpenRows[i].nParent==nTr)
{return true;}}
return false;};this.fnOpen=function(nTr,mHtml,sClass)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var nTableRows=_fnGetTrNodes(oSettings);if($.inArray(nTr,nTableRows)===-1)
{return;}
this.fnClose(nTr);var nNewRow=document.createElement("tr");var nNewCell=document.createElement("td");nNewRow.appendChild(nNewCell);nNewCell.className=sClass;nNewCell.colSpan=_fnVisbleColumns(oSettings);if(typeof mHtml==="string")
{nNewCell.innerHTML=mHtml;}
else
{$(nNewCell).html(mHtml);}
var nTrs=$('tr',oSettings.nTBody);if($.inArray(nTr,nTrs)!=-1)
{$(nNewRow).insertAfter(nTr);}
oSettings.aoOpenRows.push({"nTr":nNewRow,"nParent":nTr});return nNewRow;};this.fnPageChange=function(mAction,bRedraw)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);_fnPageChange(oSettings,mAction);_fnCalculateEnd(oSettings);if(bRedraw===undefined||bRedraw)
{_fnDraw(oSettings);}};this.fnSetColumnVis=function(iCol,bShow,bRedraw)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var i,iLen;var aoColumns=oSettings.aoColumns;var aoData=oSettings.aoData;var nTd,bAppend,iBefore;if(aoColumns[iCol].bVisible==bShow)
{return;}
if(bShow)
{var iInsert=0;for(i=0;i<iCol;i++)
{if(aoColumns[i].bVisible)
{iInsert++;}}
bAppend=(iInsert>=_fnVisbleColumns(oSettings));if(!bAppend)
{for(i=iCol;i<aoColumns.length;i++)
{if(aoColumns[i].bVisible)
{iBefore=i;break;}}}
for(i=0,iLen=aoData.length;i<iLen;i++)
{if(aoData[i].nTr!==null)
{if(bAppend)
{aoData[i].nTr.appendChild(aoData[i]._anHidden[iCol]);}
else
{aoData[i].nTr.insertBefore(aoData[i]._anHidden[iCol],_fnGetTdNodes(oSettings,i)[iBefore]);}}}}
else
{for(i=0,iLen=aoData.length;i<iLen;i++)
{if(aoData[i].nTr!==null)
{nTd=_fnGetTdNodes(oSettings,i)[iCol];aoData[i]._anHidden[iCol]=nTd;nTd.parentNode.removeChild(nTd);}}}
aoColumns[iCol].bVisible=bShow;_fnDrawHead(oSettings,oSettings.aoHeader);if(oSettings.nTFoot)
{_fnDrawHead(oSettings,oSettings.aoFooter);}
for(i=0,iLen=oSettings.aoOpenRows.length;i<iLen;i++)
{oSettings.aoOpenRows[i].nTr.colSpan=_fnVisbleColumns(oSettings);}
if(bRedraw===undefined||bRedraw)
{_fnAdjustColumnSizing(oSettings);_fnDraw(oSettings);}
_fnSaveState(oSettings);};this.fnSettings=function()
{return _fnSettingsFromNode(this[DataTable.ext.iApiIndex]);};this.fnSort=function(aaSort)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);oSettings.aaSorting=aaSort;_fnSort(oSettings);};this.fnSortListener=function(nNode,iColumn,fnCallback)
{_fnSortAttachListener(_fnSettingsFromNode(this[DataTable.ext.iApiIndex]),nNode,iColumn,fnCallback);};this.fnUpdate=function(mData,mRow,iColumn,bRedraw,bAction)
{var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);var i,iLen,sDisplay;var iRow=(typeof mRow==='object')?_fnNodeToDataIndex(oSettings,mRow):mRow;if($.isArray(mData)&&iColumn===undefined)
{oSettings.aoData[iRow]._aData=mData.slice();for(i=0;i<oSettings.aoColumns.length;i++)
{this.fnUpdate(_fnGetCellData(oSettings,iRow,i),iRow,i,false,false);}}
else if($.isPlainObject(mData)&&iColumn===undefined)
{oSettings.aoData[iRow]._aData=$.extend(true,{},mData);for(i=0;i<oSettings.aoColumns.length;i++)
{this.fnUpdate(_fnGetCellData(oSettings,iRow,i),iRow,i,false,false);}}
else
{_fnSetCellData(oSettings,iRow,iColumn,mData);sDisplay=_fnGetCellData(oSettings,iRow,iColumn,'display');var oCol=oSettings.aoColumns[iColumn];if(oCol.fnRender!==null)
{sDisplay=_fnRender(oSettings,iRow,iColumn);if(oCol.bUseRendered)
{_fnSetCellData(oSettings,iRow,iColumn,sDisplay);}}
if(oSettings.aoData[iRow].nTr!==null)
{_fnGetTdNodes(oSettings,iRow)[iColumn].innerHTML=sDisplay;}}
var iDisplayIndex=$.inArray(iRow,oSettings.aiDisplay);oSettings.asDataSearch[iDisplayIndex]=_fnBuildSearchRow(oSettings,_fnGetRowData(oSettings,iRow,'filter',_fnGetColumns(oSettings,'bSearchable')));if(bAction===undefined||bAction)
{_fnAdjustColumnSizing(oSettings);}
if(bRedraw===undefined||bRedraw)
{_fnReDraw(oSettings);}
return 0;};this.fnVersionCheck=DataTable.ext.fnVersionCheck;function _fnExternApiFunc(sFunc)
{return function(){var aArgs=[_fnSettingsFromNode(this[DataTable.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));return DataTable.ext.oApi[sFunc].apply(this,aArgs);};}
this.oApi={"_fnExternApiFunc":_fnExternApiFunc,"_fnInitialise":_fnInitialise,"_fnInitComplete":_fnInitComplete,"_fnLanguageCompat":_fnLanguageCompat,"_fnAddColumn":_fnAddColumn,"_fnColumnOptions":_fnColumnOptions,"_fnAddData":_fnAddData,"_fnCreateTr":_fnCreateTr,"_fnGatherData":_fnGatherData,"_fnBuildHead":_fnBuildHead,"_fnDrawHead":_fnDrawHead,"_fnDraw":_fnDraw,"_fnReDraw":_fnReDraw,"_fnAjaxUpdate":_fnAjaxUpdate,"_fnAjaxParameters":_fnAjaxParameters,"_fnAjaxUpdateDraw":_fnAjaxUpdateDraw,"_fnServerParams":_fnServerParams,"_fnAddOptionsHtml":_fnAddOptionsHtml,"_fnFeatureHtmlTable":_fnFeatureHtmlTable,"_fnScrollDraw":_fnScrollDraw,"_fnAdjustColumnSizing":_fnAdjustColumnSizing,"_fnFeatureHtmlFilter":_fnFeatureHtmlFilter,"_fnFilterComplete":_fnFilterComplete,"_fnFilterCustom":_fnFilterCustom,"_fnFilterColumn":_fnFilterColumn,"_fnFilter":_fnFilter,"_fnBuildSearchArray":_fnBuildSearchArray,"_fnBuildSearchRow":_fnBuildSearchRow,"_fnFilterCreateSearch":_fnFilterCreateSearch,"_fnDataToSearch":_fnDataToSearch,"_fnSort":_fnSort,"_fnSortAttachListener":_fnSortAttachListener,"_fnSortingClasses":_fnSortingClasses,"_fnFeatureHtmlPaginate":_fnFeatureHtmlPaginate,"_fnPageChange":_fnPageChange,"_fnFeatureHtmlInfo":_fnFeatureHtmlInfo,"_fnUpdateInfo":_fnUpdateInfo,"_fnFeatureHtmlLength":_fnFeatureHtmlLength,"_fnFeatureHtmlProcessing":_fnFeatureHtmlProcessing,"_fnProcessingDisplay":_fnProcessingDisplay,"_fnVisibleToColumnIndex":_fnVisibleToColumnIndex,"_fnColumnIndexToVisible":_fnColumnIndexToVisible,"_fnNodeToDataIndex":_fnNodeToDataIndex,"_fnVisbleColumns":_fnVisbleColumns,"_fnCalculateEnd":_fnCalculateEnd,"_fnConvertToWidth":_fnConvertToWidth,"_fnCalculateColumnWidths":_fnCalculateColumnWidths,"_fnScrollingWidthAdjust":_fnScrollingWidthAdjust,"_fnGetWidestNode":_fnGetWidestNode,"_fnGetMaxLenString":_fnGetMaxLenString,"_fnStringToCss":_fnStringToCss,"_fnDetectType":_fnDetectType,"_fnSettingsFromNode":_fnSettingsFromNode,"_fnGetDataMaster":_fnGetDataMaster,"_fnGetTrNodes":_fnGetTrNodes,"_fnGetTdNodes":_fnGetTdNodes,"_fnEscapeRegex":_fnEscapeRegex,"_fnDeleteIndex":_fnDeleteIndex,"_fnReOrderIndex":_fnReOrderIndex,"_fnColumnOrdering":_fnColumnOrdering,"_fnLog":_fnLog,"_fnClearTable":_fnClearTable,"_fnSaveState":_fnSaveState,"_fnLoadState":_fnLoadState,"_fnCreateCookie":_fnCreateCookie,"_fnReadCookie":_fnReadCookie,"_fnDetectHeader":_fnDetectHeader,"_fnGetUniqueThs":_fnGetUniqueThs,"_fnScrollBarWidth":_fnScrollBarWidth,"_fnApplyToChildren":_fnApplyToChildren,"_fnMap":_fnMap,"_fnGetRowData":_fnGetRowData,"_fnGetCellData":_fnGetCellData,"_fnSetCellData":_fnSetCellData,"_fnGetObjectDataFn":_fnGetObjectDataFn,"_fnSetObjectDataFn":_fnSetObjectDataFn,"_fnApplyColumnDefs":_fnApplyColumnDefs,"_fnBindAction":_fnBindAction,"_fnExtend":_fnExtend,"_fnCallbackReg":_fnCallbackReg,"_fnCallbackFire":_fnCallbackFire,"_fnJsonString":_fnJsonString,"_fnRender":_fnRender,"_fnNodeToColumnIndex":_fnNodeToColumnIndex,"_fnInfoMacros":_fnInfoMacros,"_fnBrowserDetect":_fnBrowserDetect,"_fnGetColumns":_fnGetColumns};$.extend(DataTable.ext.oApi,this.oApi);for(var sFunc in DataTable.ext.oApi)
{if(sFunc)
{this[sFunc]=_fnExternApiFunc(sFunc);}}
var _that=this;this.each(function(){var i=0,iLen,j,jLen,k,kLen;var sId=this.getAttribute('id');var bInitHandedOff=false;var bUsePassedData=false;if(this.nodeName.toLowerCase()!='table')
{_fnLog(null,0,"Attempted to initialise DataTables on a node which is not a "+"table: "+this.nodeName);return;}
for(i=0,iLen=DataTable.settings.length;i<iLen;i++)
{if(DataTable.settings[i].nTable==this)
{if(oInit===undefined||oInit.bRetrieve)
{return DataTable.settings[i].oInstance;}
else if(oInit.bDestroy)
{DataTable.settings[i].oInstance.fnDestroy();break;}
else
{_fnLog(DataTable.settings[i],0,"Cannot reinitialise DataTable.\n\n"+"To retrieve the DataTables object for this table, pass no arguments or see "+"the docs for bRetrieve and bDestroy");return;}}
if(DataTable.settings[i].sTableId==this.id)
{DataTable.settings.splice(i,1);break;}}
if(sId===null||sId==="")
{sId="DataTables_Table_"+(DataTable.ext._oExternConfig.iNextUnique++);this.id=sId;}
var oSettings=$.extend(true,{},DataTable.models.oSettings,{"nTable":this,"oApi":_that.oApi,"oInit":oInit,"sDestroyWidth":$(this).width(),"sInstance":sId,"sTableId":sId});DataTable.settings.push(oSettings);oSettings.oInstance=(_that.length===1)?_that:$(this).dataTable();if(!oInit)
{oInit={};}
if(oInit.oLanguage)
{_fnLanguageCompat(oInit.oLanguage);}
oInit=_fnExtend($.extend(true,{},DataTable.defaults),oInit);_fnMap(oSettings.oFeatures,oInit,"bPaginate");_fnMap(oSettings.oFeatures,oInit,"bLengthChange");_fnMap(oSettings.oFeatures,oInit,"bFilter");_fnMap(oSettings.oFeatures,oInit,"bSort");_fnMap(oSettings.oFeatures,oInit,"bInfo");_fnMap(oSettings.oFeatures,oInit,"bProcessing");_fnMap(oSettings.oFeatures,oInit,"bAutoWidth");_fnMap(oSettings.oFeatures,oInit,"bSortClasses");_fnMap(oSettings.oFeatures,oInit,"bServerSide");_fnMap(oSettings.oFeatures,oInit,"bDeferRender");_fnMap(oSettings.oScroll,oInit,"sScrollX","sX");_fnMap(oSettings.oScroll,oInit,"sScrollXInner","sXInner");_fnMap(oSettings.oScroll,oInit,"sScrollY","sY");_fnMap(oSettings.oScroll,oInit,"bScrollCollapse","bCollapse");_fnMap(oSettings.oScroll,oInit,"bScrollInfinite","bInfinite");_fnMap(oSettings.oScroll,oInit,"iScrollLoadGap","iLoadGap");_fnMap(oSettings.oScroll,oInit,"bScrollAutoCss","bAutoCss");_fnMap(oSettings,oInit,"asStripeClasses");_fnMap(oSettings,oInit,"asStripClasses","asStripeClasses");_fnMap(oSettings,oInit,"fnServerData");_fnMap(oSettings,oInit,"fnFormatNumber");_fnMap(oSettings,oInit,"sServerMethod");_fnMap(oSettings,oInit,"aaSorting");_fnMap(oSettings,oInit,"aaSortingFixed");_fnMap(oSettings,oInit,"aLengthMenu");_fnMap(oSettings,oInit,"sPaginationType");_fnMap(oSettings,oInit,"sAjaxSource");_fnMap(oSettings,oInit,"sAjaxDataProp");_fnMap(oSettings,oInit,"iCookieDuration");_fnMap(oSettings,oInit,"sCookiePrefix");_fnMap(oSettings,oInit,"sDom");_fnMap(oSettings,oInit,"bSortCellsTop");_fnMap(oSettings,oInit,"iTabIndex");_fnMap(oSettings,oInit,"oSearch","oPreviousSearch");_fnMap(oSettings,oInit,"aoSearchCols","aoPreSearchCols");_fnMap(oSettings,oInit,"iDisplayLength","_iDisplayLength");_fnMap(oSettings,oInit,"bJQueryUI","bJUI");_fnMap(oSettings,oInit,"fnCookieCallback");_fnMap(oSettings,oInit,"fnStateLoad");_fnMap(oSettings,oInit,"fnStateSave");_fnMap(oSettings.oLanguage,oInit,"fnInfoCallback");_fnCallbackReg(oSettings,'aoDrawCallback',oInit.fnDrawCallback,'user');_fnCallbackReg(oSettings,'aoServerParams',oInit.fnServerParams,'user');_fnCallbackReg(oSettings,'aoStateSaveParams',oInit.fnStateSaveParams,'user');_fnCallbackReg(oSettings,'aoStateLoadParams',oInit.fnStateLoadParams,'user');_fnCallbackReg(oSettings,'aoStateLoaded',oInit.fnStateLoaded,'user');_fnCallbackReg(oSettings,'aoRowCallback',oInit.fnRowCallback,'user');_fnCallbackReg(oSettings,'aoRowCreatedCallback',oInit.fnCreatedRow,'user');_fnCallbackReg(oSettings,'aoHeaderCallback',oInit.fnHeaderCallback,'user');_fnCallbackReg(oSettings,'aoFooterCallback',oInit.fnFooterCallback,'user');_fnCallbackReg(oSettings,'aoInitComplete',oInit.fnInitComplete,'user');_fnCallbackReg(oSettings,'aoPreDrawCallback',oInit.fnPreDrawCallback,'user');if(oSettings.oFeatures.bServerSide&&oSettings.oFeatures.bSort&&oSettings.oFeatures.bSortClasses)
{_fnCallbackReg(oSettings,'aoDrawCallback',_fnSortingClasses,'server_side_sort_classes');}
else if(oSettings.oFeatures.bDeferRender)
{_fnCallbackReg(oSettings,'aoDrawCallback',_fnSortingClasses,'defer_sort_classes');}
if(oInit.bJQueryUI)
{$.extend(oSettings.oClasses,DataTable.ext.oJUIClasses);if(oInit.sDom===DataTable.defaults.sDom&&DataTable.defaults.sDom==="lfrtip")
{oSettings.sDom='<"H"lfr>t<"F"ip>';}}
else
{$.extend(oSettings.oClasses,DataTable.ext.oStdClasses);}
$(this).addClass(oSettings.oClasses.sTable);if(oSettings.oScroll.sX!==""||oSettings.oScroll.sY!=="")
{oSettings.oScroll.iBarWidth=_fnScrollBarWidth();}
if(oSettings.iInitDisplayStart===undefined)
{oSettings.iInitDisplayStart=oInit.iDisplayStart;oSettings._iDisplayStart=oInit.iDisplayStart;}
if(oInit.bStateSave)
{oSettings.oFeatures.bStateSave=true;_fnLoadState(oSettings,oInit);_fnCallbackReg(oSettings,'aoDrawCallback',_fnSaveState,'state_save');}
if(oInit.iDeferLoading!==null)
{oSettings.bDeferLoading=true;var tmp=$.isArray(oInit.iDeferLoading);oSettings._iRecordsDisplay=tmp?oInit.iDeferLoading[0]:oInit.iDeferLoading;oSettings._iRecordsTotal=tmp?oInit.iDeferLoading[1]:oInit.iDeferLoading;}
if(oInit.aaData!==null)
{bUsePassedData=true;}
if(oInit.oLanguage.sUrl!=="")
{oSettings.oLanguage.sUrl=oInit.oLanguage.sUrl;$.getJSON(oSettings.oLanguage.sUrl,null,function(json){_fnLanguageCompat(json);$.extend(true,oSettings.oLanguage,oInit.oLanguage,json);_fnInitialise(oSettings);});bInitHandedOff=true;}
else
{$.extend(true,oSettings.oLanguage,oInit.oLanguage);}
if(oInit.asStripeClasses===null)
{oSettings.asStripeClasses=[oSettings.oClasses.sStripeOdd,oSettings.oClasses.sStripeEven];}
iLen=oSettings.asStripeClasses.length;oSettings.asDestroyStripes=[];if(iLen)
{var bStripeRemove=false;var anRows=$(this).children('tbody').children('tr:lt('+iLen+')');for(i=0;i<iLen;i++)
{if(anRows.hasClass(oSettings.asStripeClasses[i]))
{bStripeRemove=true;oSettings.asDestroyStripes.push(oSettings.asStripeClasses[i]);}}
if(bStripeRemove)
{anRows.removeClass(oSettings.asStripeClasses.join(' '));}}
var anThs=[];var aoColumnsInit;var nThead=this.getElementsByTagName('thead');if(nThead.length!==0)
{_fnDetectHeader(oSettings.aoHeader,nThead[0]);anThs=_fnGetUniqueThs(oSettings);}
if(oInit.aoColumns===null)
{aoColumnsInit=[];for(i=0,iLen=anThs.length;i<iLen;i++)
{aoColumnsInit.push(null);}}
else
{aoColumnsInit=oInit.aoColumns;}
for(i=0,iLen=aoColumnsInit.length;i<iLen;i++)
{if(oInit.saved_aoColumns!==undefined&&oInit.saved_aoColumns.length==iLen)
{if(aoColumnsInit[i]===null)
{aoColumnsInit[i]={};}
aoColumnsInit[i].bVisible=oInit.saved_aoColumns[i].bVisible;}
_fnAddColumn(oSettings,anThs?anThs[i]:null);}
_fnApplyColumnDefs(oSettings,oInit.aoColumnDefs,aoColumnsInit,function(iCol,oDef){_fnColumnOptions(oSettings,iCol,oDef);});for(i=0,iLen=oSettings.aaSorting.length;i<iLen;i++)
{if(oSettings.aaSorting[i][0]>=oSettings.aoColumns.length)
{oSettings.aaSorting[i][0]=0;}
var oColumn=oSettings.aoColumns[oSettings.aaSorting[i][0]];if(oSettings.aaSorting[i][2]===undefined)
{oSettings.aaSorting[i][2]=0;}
if(oInit.aaSorting===undefined&&oSettings.saved_aaSorting===undefined)
{oSettings.aaSorting[i][1]=oColumn.asSorting[0];}
for(j=0,jLen=oColumn.asSorting.length;j<jLen;j++)
{if(oSettings.aaSorting[i][1]==oColumn.asSorting[j])
{oSettings.aaSorting[i][2]=j;break;}}}
_fnSortingClasses(oSettings);_fnBrowserDetect(oSettings);var captions=$(this).children('caption').each(function(){this._captionSide=$(this).css('caption-side');});var thead=$(this).children('thead');if(thead.length===0)
{thead=[document.createElement('thead')];this.appendChild(thead[0]);}
oSettings.nTHead=thead[0];var tbody=$(this).children('tbody');if(tbody.length===0)
{tbody=[document.createElement('tbody')];this.appendChild(tbody[0]);}
oSettings.nTBody=tbody[0];oSettings.nTBody.setAttribute("role","alert");oSettings.nTBody.setAttribute("aria-live","polite");oSettings.nTBody.setAttribute("aria-relevant","all");var tfoot=$(this).children('tfoot');if(tfoot.length===0&&captions.length>0&&(oSettings.oScroll.sX!==""||oSettings.oScroll.sY!==""))
{tfoot=[document.createElement('tfoot')];this.appendChild(tfoot[0]);}
if(tfoot.length>0)
{oSettings.nTFoot=tfoot[0];_fnDetectHeader(oSettings.aoFooter,oSettings.nTFoot);}
if(bUsePassedData)
{for(i=0;i<oInit.aaData.length;i++)
{_fnAddData(oSettings,oInit.aaData[i]);}}
else
{_fnGatherData(oSettings);}
oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();oSettings.bInitialised=true;if(bInitHandedOff===false)
{_fnInitialise(oSettings);}});_that=null;return this;};DataTable.fnVersionCheck=function(sVersion)
{var fnZPad=function(Zpad,count)
{while(Zpad.length<count){Zpad+='0';}
return Zpad;};var aThis=DataTable.ext.sVersion.split('.');var aThat=sVersion.split('.');var sThis='',sThat='';for(var i=0,iLen=aThat.length;i<iLen;i++)
{sThis+=fnZPad(aThis[i],3);sThat+=fnZPad(aThat[i],3);}
return parseInt(sThis,10)>=parseInt(sThat,10);};DataTable.fnIsDataTable=function(nTable)
{var o=DataTable.settings;for(var i=0;i<o.length;i++)
{if(o[i].nTable===nTable||o[i].nScrollHead===nTable||o[i].nScrollFoot===nTable)
{return true;}}
return false;};DataTable.fnTables=function(bVisible)
{var out=[];jQuery.each(DataTable.settings,function(i,o){if(!bVisible||(bVisible===true&&$(o.nTable).is(':visible')))
{out.push(o.nTable);}});return out;};DataTable.version="1.9.4";DataTable.settings=[];DataTable.models={};DataTable.models.ext={"afnFiltering":[],"afnSortData":[],"aoFeatures":[],"aTypes":[],"fnVersionCheck":DataTable.fnVersionCheck,"iApiIndex":0,"ofnSearch":{},"oApi":{},"oStdClasses":{},"oJUIClasses":{},"oPagination":{},"oSort":{},"sVersion":DataTable.version,"sErrMode":"alert","_oExternConfig":{"iNextUnique":0}};DataTable.models.oSearch={"bCaseInsensitive":true,"sSearch":"","bRegex":false,"bSmart":true};DataTable.models.oRow={"nTr":null,"_aData":[],"_aSortData":[],"_anHidden":[],"_sRowStripe":""};DataTable.models.oColumn={"aDataSort":null,"asSorting":null,"bSearchable":null,"bSortable":null,"bUseRendered":null,"bVisible":null,"_bAutoType":true,"fnCreatedCell":null,"fnGetData":null,"fnRender":null,"fnSetData":null,"mData":null,"mRender":null,"nTh":null,"nTf":null,"sClass":null,"sContentPadding":null,"sDefaultContent":null,"sName":null,"sSortDataType":'std',"sSortingClass":null,"sSortingClassJUI":null,"sTitle":null,"sType":null,"sWidth":null,"sWidthOrig":null};DataTable.defaults={"aaData":null,"aaSorting":[[0,'asc']],"aaSortingFixed":null,"aLengthMenu":[10,25,50,100],"aoColumns":null,"aoColumnDefs":null,"aoSearchCols":[],"asStripeClasses":null,"bAutoWidth":true,"bDeferRender":false,"bDestroy":false,"bFilter":true,"bInfo":true,"bJQueryUI":false,"bLengthChange":true,"bPaginate":true,"bProcessing":false,"bRetrieve":false,"bScrollAutoCss":true,"bScrollCollapse":false,"bScrollInfinite":false,"bServerSide":false,"bSort":true,"bSortCellsTop":false,"bSortClasses":true,"bStateSave":false,"fnCookieCallback":null,"fnCreatedRow":null,"fnDrawCallback":null,"fnFooterCallback":null,"fnFormatNumber":function(iIn){if(iIn<1000)
{return iIn;}
var s=(iIn+""),a=s.split(""),out="",iLen=s.length;for(var i=0;i<iLen;i++)
{if(i%3===0&&i!==0)
{out=this.oLanguage.sInfoThousands+out;}
out=a[iLen-i-1]+out;}
return out;},"fnHeaderCallback":null,"fnInfoCallback":null,"fnInitComplete":null,"fnPreDrawCallback":null,"fnRowCallback":null,"fnServerData":function(sUrl,aoData,fnCallback,oSettings){oSettings.jqXHR=$.ajax({"url":sUrl,"data":aoData,"success":function(json){if(json.sError){oSettings.oApi._fnLog(oSettings,0,json.sError);}
$(oSettings.oInstance).trigger('xhr',[oSettings,json]);fnCallback(json);},"dataType":"json","cache":false,"type":oSettings.sServerMethod,"error":function(xhr,error,thrown){if(error=="parsererror"){oSettings.oApi._fnLog(oSettings,0,"DataTables warning: JSON data from "+"server could not be parsed. This is caused by a JSON formatting error.");}}});},"fnServerParams":null,"fnStateLoad":function(oSettings){var sData=this.oApi._fnReadCookie(oSettings.sCookiePrefix+oSettings.sInstance);var oData;try{oData=(typeof $.parseJSON==='function')?$.parseJSON(sData):eval('('+sData+')');}catch(e){oData=null;}
return oData;},"fnStateLoadParams":null,"fnStateLoaded":null,"fnStateSave":function(oSettings,oData){this.oApi._fnCreateCookie(oSettings.sCookiePrefix+oSettings.sInstance,this.oApi._fnJsonString(oData),oSettings.iCookieDuration,oSettings.sCookiePrefix,oSettings.fnCookieCallback);},"fnStateSaveParams":null,"iCookieDuration":7200,"iDeferLoading":null,"iDisplayLength":10,"iDisplayStart":0,"iScrollLoadGap":100,"iTabIndex":0,"oLanguage":{"oAria":{"sSortAscending":": activate to sort column ascending","sSortDescending":": activate to sort column descending"},"oPaginate":{"sFirst":"First","sLast":"Last","sNext":"","sPrevious":""},"sEmptyTable":"No data available in table","sInfo":"Showing _START_ to _END_ of _TOTAL_ entries","sInfoEmpty":"Showing 0 to 0 of 0 entries","sInfoFiltered":"(filtered from _MAX_ total entries)","sInfoPostFix":"","sInfoThousands":",","sLengthMenu":"Show _MENU_ entries","sLoadingRecords":"Loading...","sProcessing":"Processing...","sSearch":"","sUrl":"","sZeroRecords":"No matching records found"},"oSearch":$.extend({},DataTable.models.oSearch),"sAjaxDataProp":"aaData","sAjaxSource":null,"sCookiePrefix":"SpryMedia_DataTables_","sDom":"lfrtip","sPaginationType":"two_button","sScrollX":"","sScrollXInner":"","sScrollY":"","sServerMethod":"GET"};DataTable.defaults.columns={"aDataSort":null,"asSorting":['asc','desc'],"bSearchable":true,"bSortable":true,"bUseRendered":true,"bVisible":true,"fnCreatedCell":null,"fnRender":null,"iDataSort":-1,"mData":null,"mRender":null,"sCellType":"td","sClass":"","sContentPadding":"","sDefaultContent":null,"sName":"","sSortDataType":"std","sTitle":null,"sType":null,"sWidth":null};DataTable.models.oSettings={"oFeatures":{"bAutoWidth":null,"bDeferRender":null,"bFilter":null,"bInfo":null,"bLengthChange":null,"bPaginate":null,"bProcessing":null,"bServerSide":null,"bSort":null,"bSortClasses":null,"bStateSave":null},"oScroll":{"bAutoCss":null,"bCollapse":null,"bInfinite":null,"iBarWidth":0,"iLoadGap":null,"sX":null,"sXInner":null,"sY":null},"oLanguage":{"fnInfoCallback":null},"oBrowser":{"bScrollOversize":false},"aanFeatures":[],"aoData":[],"aiDisplay":[],"aiDisplayMaster":[],"aoColumns":[],"aoHeader":[],"aoFooter":[],"asDataSearch":[],"oPreviousSearch":{},"aoPreSearchCols":[],"aaSorting":null,"aaSortingFixed":null,"asStripeClasses":null,"asDestroyStripes":[],"sDestroyWidth":0,"aoRowCallback":[],"aoHeaderCallback":[],"aoFooterCallback":[],"aoDrawCallback":[],"aoRowCreatedCallback":[],"aoPreDrawCallback":[],"aoInitComplete":[],"aoStateSaveParams":[],"aoStateLoadParams":[],"aoStateLoaded":[],"sTableId":"","nTable":null,"nTHead":null,"nTFoot":null,"nTBody":null,"nTableWrapper":null,"bDeferLoading":false,"bInitialised":false,"aoOpenRows":[],"sDom":null,"sPaginationType":"two_button","iCookieDuration":0,"sCookiePrefix":"","fnCookieCallback":null,"aoStateSave":[],"aoStateLoad":[],"oLoadedState":null,"sAjaxSource":null,"sAjaxDataProp":null,"bAjaxDataGet":true,"jqXHR":null,"fnServerData":null,"aoServerParams":[],"sServerMethod":null,"fnFormatNumber":null,"aLengthMenu":null,"iDraw":0,"bDrawing":false,"iDrawError":-1,"_iDisplayLength":10,"_iDisplayStart":0,"_iDisplayEnd":10,"_iRecordsTotal":0,"_iRecordsDisplay":0,"bJUI":null,"oClasses":{},"bFiltered":false,"bSorted":false,"bSortCellsTop":null,"oInit":null,"aoDestroyCallback":[],"fnRecordsTotal":function()
{if(this.oFeatures.bServerSide){return parseInt(this._iRecordsTotal,10);}else{return this.aiDisplayMaster.length;}},"fnRecordsDisplay":function()
{if(this.oFeatures.bServerSide){return parseInt(this._iRecordsDisplay,10);}else{return this.aiDisplay.length;}},"fnDisplayEnd":function()
{if(this.oFeatures.bServerSide){if(this.oFeatures.bPaginate===false||this._iDisplayLength==-1){return this._iDisplayStart+this.aiDisplay.length;}else{return Math.min(this._iDisplayStart+this._iDisplayLength,this._iRecordsDisplay);}}else{return this._iDisplayEnd;}},"oInstance":null,"sInstance":null,"iTabIndex":0,"nScrollHead":null,"nScrollFoot":null};DataTable.ext=$.extend(true,{},DataTable.models.ext);$.extend(DataTable.ext.oStdClasses,{"sTable":"dataTable","sPagePrevEnabled":"paginate_enabled_previous","sPagePrevDisabled":"paginate_disabled_previous","sPageNextEnabled":"paginate_enabled_next","sPageNextDisabled":"paginate_disabled_next","sPageJUINext":"","sPageJUIPrev":"","sPageButton":"paginate_button","sPageButtonActive":"paginate_active","sPageButtonStaticDisabled":"paginate_button paginate_button_disabled","sPageFirst":"first","sPagePrevious":"previous","sPageNext":"next","sPageLast":"last","sStripeOdd":"odd","sStripeEven":"even","sRowEmpty":"dataTables_empty","sWrapper":"dataTables_wrapper","sFilter":"dataTables_filter","sInfo":"dataTables_info","sPaging":"dataTables_paginate paging_","sLength":"dataTables_length","sProcessing":"dataTables_processing","sSortAsc":"sorting_asc","sSortDesc":"sorting_desc","sSortable":"sorting","sSortableAsc":"sorting_asc_disabled","sSortableDesc":"sorting_desc_disabled","sSortableNone":"sorting_disabled","sSortColumn":"sorting_","sSortJUIAsc":"","sSortJUIDesc":"","sSortJUI":"","sSortJUIAscAllowed":"","sSortJUIDescAllowed":"","sSortJUIWrapper":"","sSortIcon":"","sScrollWrapper":"dataTables_scroll","sScrollHead":"dataTables_scrollHead","sScrollHeadInner":"dataTables_scrollHeadInner","sScrollBody":"dataTables_scrollBody","sScrollFoot":"dataTables_scrollFoot","sScrollFootInner":"dataTables_scrollFootInner","sFooterTH":"","sJUIHeader":"","sJUIFooter":""});$.extend(DataTable.ext.oJUIClasses,DataTable.ext.oStdClasses,{"sPagePrevEnabled":"fg-button ui-button ui-state-default ui-corner-left","sPagePrevDisabled":"fg-button ui-button ui-state-default ui-corner-left ui-state-disabled","sPageNextEnabled":"fg-button ui-button ui-state-default ui-corner-right","sPageNextDisabled":"fg-button ui-button ui-state-default ui-corner-right ui-state-disabled","sPageJUINext":"ui-icon ui-icon-circle-arrow-e","sPageJUIPrev":"ui-icon ui-icon-circle-arrow-w","sPageButton":"fg-button ui-button ui-state-default","sPageButtonActive":"fg-button ui-button ui-state-default ui-state-disabled","sPageButtonStaticDisabled":"fg-button ui-button ui-state-default ui-state-disabled","sPageFirst":"first ui-corner-tl ui-corner-bl","sPageLast":"last ui-corner-tr ui-corner-br","sPaging":"dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi "+"ui-buttonset-multi paging_","sSortAsc":"ui-state-default","sSortDesc":"ui-state-default","sSortable":"ui-state-default","sSortableAsc":"ui-state-default","sSortableDesc":"ui-state-default","sSortableNone":"ui-state-default","sSortJUIAsc":"css_right ui-icon ui-icon-triangle-1-n","sSortJUIDesc":"css_right ui-icon ui-icon-triangle-1-s","sSortJUI":"css_right ui-icon ui-icon-carat-2-n-s","sSortJUIAscAllowed":"css_right ui-icon ui-icon-carat-1-n","sSortJUIDescAllowed":"css_right ui-icon ui-icon-carat-1-s","sSortJUIWrapper":"DataTables_sort_wrapper","sSortIcon":"DataTables_sort_icon","sScrollHead":"dataTables_scrollHead ui-state-default","sScrollFoot":"dataTables_scrollFoot ui-state-default","sFooterTH":"ui-state-default","sJUIHeader":"fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix","sJUIFooter":"fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"});$.extend(DataTable.ext.oPagination,{"two_button":{"fnInit":function(oSettings,nPaging,fnCallbackDraw)
{var oLang=oSettings.oLanguage.oPaginate;var oClasses=oSettings.oClasses;var fnClickHandler=function(e){if(oSettings.oApi._fnPageChange(oSettings,e.data.action))
{fnCallbackDraw(oSettings);}};var sAppend=(!oSettings.bJUI)?'<a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sPrevious+'</a>'+'<a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sNext+'</a>':'<a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="'+oSettings.oClasses.sPageJUIPrev+'"></span></a>'+'<a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="'+oSettings.oClasses.sPageJUINext+'"></span></a>';$(nPaging).append(sAppend);var els=$('a',nPaging);var nPrevious=els[0],nNext=els[1];oSettings.oApi._fnBindAction(nPrevious,{action:"previous"},fnClickHandler);oSettings.oApi._fnBindAction(nNext,{action:"next"},fnClickHandler);if(!oSettings.aanFeatures.p)
{nPaging.id=oSettings.sTableId+'_paginate';nPrevious.id=oSettings.sTableId+'_previous';nNext.id=oSettings.sTableId+'_next';nPrevious.setAttribute('aria-controls',oSettings.sTableId);nNext.setAttribute('aria-controls',oSettings.sTableId);}},"fnUpdate":function(oSettings,fnCallbackDraw)
{if(!oSettings.aanFeatures.p)
{return;}
var oClasses=oSettings.oClasses;var an=oSettings.aanFeatures.p;var nNode;for(var i=0,iLen=an.length;i<iLen;i++)
{nNode=an[i].firstChild;if(nNode)
{nNode.className=(oSettings._iDisplayStart===0)?oClasses.sPagePrevDisabled:oClasses.sPagePrevEnabled;nNode=nNode.nextSibling;nNode.className=(oSettings.fnDisplayEnd()==oSettings.fnRecordsDisplay())?oClasses.sPageNextDisabled:oClasses.sPageNextEnabled;}}}},"iFullNumbersShowPages":5,"full_numbers":{"fnInit":function(oSettings,nPaging,fnCallbackDraw)
{var oLang=oSettings.oLanguage.oPaginate;var oClasses=oSettings.oClasses;var fnClickHandler=function(e){if(oSettings.oApi._fnPageChange(oSettings,e.data.action))
{fnCallbackDraw(oSettings);}};$(nPaging).append('<a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageFirst+'">'+oLang.sFirst+'</a>'+'<a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPagePrevious+'">'+oLang.sPrevious+'</a>'+'<span></span>'+'<a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageNext+'">'+oLang.sNext+'</a>'+'<a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageLast+'">'+oLang.sLast+'</a>');var els=$('a',nPaging);var nFirst=els[0],nPrev=els[1],nNext=els[2],nLast=els[3];oSettings.oApi._fnBindAction(nFirst,{action:"first"},fnClickHandler);oSettings.oApi._fnBindAction(nPrev,{action:"previous"},fnClickHandler);oSettings.oApi._fnBindAction(nNext,{action:"next"},fnClickHandler);oSettings.oApi._fnBindAction(nLast,{action:"last"},fnClickHandler);if(!oSettings.aanFeatures.p)
{nPaging.id=oSettings.sTableId+'_paginate';nFirst.id=oSettings.sTableId+'_first';nPrev.id=oSettings.sTableId+'_previous';nNext.id=oSettings.sTableId+'_next';nLast.id=oSettings.sTableId+'_last';}},"fnUpdate":function(oSettings,fnCallbackDraw)
{if(!oSettings.aanFeatures.p)
{return;}
var iPageCount=DataTable.ext.oPagination.iFullNumbersShowPages;var iPageCountHalf=Math.floor(iPageCount/2);var iPages=Math.ceil((oSettings.fnRecordsDisplay())/oSettings._iDisplayLength);var iCurrentPage=Math.ceil(oSettings._iDisplayStart/oSettings._iDisplayLength)+1;var sList="";var iStartButton,iEndButton,i,iLen;var oClasses=oSettings.oClasses;var anButtons,anStatic,nPaginateList,nNode;var an=oSettings.aanFeatures.p;var fnBind=function(j){oSettings.oApi._fnBindAction(this,{"page":j+iStartButton-1},function(e){oSettings.oApi._fnPageChange(oSettings,e.data.page);fnCallbackDraw(oSettings);e.preventDefault();});};if(oSettings._iDisplayLength===-1)
{iStartButton=1;iEndButton=1;iCurrentPage=1;}
else if(iPages<iPageCount)
{iStartButton=1;iEndButton=iPages;}
else if(iCurrentPage<=iPageCountHalf)
{iStartButton=1;iEndButton=iPageCount;}
else if(iCurrentPage>=(iPages-iPageCountHalf))
{iStartButton=iPages-iPageCount+1;iEndButton=iPages;}
else
{iStartButton=iCurrentPage-Math.ceil(iPageCount/2)+1;iEndButton=iStartButton+iPageCount-1;}
for(i=iStartButton;i<=iEndButton;i++)
{sList+=(iCurrentPage!==i)?'<a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+'">'+oSettings.fnFormatNumber(i)+'</a>':'<a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButtonActive+'">'+oSettings.fnFormatNumber(i)+'</a>';}
for(i=0,iLen=an.length;i<iLen;i++)
{nNode=an[i];if(!nNode.hasChildNodes())
{continue;}
$('span:eq(0)',nNode).html(sList).children('a').each(fnBind);anButtons=nNode.getElementsByTagName('a');anStatic=[anButtons[0],anButtons[1],anButtons[anButtons.length-2],anButtons[anButtons.length-1]];$(anStatic).removeClass(oClasses.sPageButton+" "+oClasses.sPageButtonActive+" "+oClasses.sPageButtonStaticDisabled);$([anStatic[0],anStatic[1]]).addClass((iCurrentPage==1)?oClasses.sPageButtonStaticDisabled:oClasses.sPageButton);$([anStatic[2],anStatic[3]]).addClass((iPages===0||iCurrentPage===iPages||oSettings._iDisplayLength===-1)?oClasses.sPageButtonStaticDisabled:oClasses.sPageButton);}}}});$.extend(DataTable.ext.oSort,{"string-pre":function(a)
{if(typeof a!='string'){a=(a!==null&&a.toString)?a.toString():'';}
return a.toLowerCase();},"string-asc":function(x,y)
{return((x<y)?-1:((x>y)?1:0));},"string-desc":function(x,y)
{return((x<y)?1:((x>y)?-1:0));},"html-pre":function(a)
{return a.replace(/<.*?>/g,"").toLowerCase();},"html-asc":function(x,y)
{return((x<y)?-1:((x>y)?1:0));},"html-desc":function(x,y)
{return((x<y)?1:((x>y)?-1:0));},"date-pre":function(a)
{var x=Date.parse(a);if(isNaN(x)||x==="")
{x=Date.parse("01/01/1970 00:00:00");}
return x;},"date-asc":function(x,y)
{return x-y;},"date-desc":function(x,y)
{return y-x;},"numeric-pre":function(a)
{return(a=="-"||a==="")?0:a*1;},"numeric-asc":function(x,y)
{return x-y;},"numeric-desc":function(x,y)
{return y-x;}});$.extend(DataTable.ext.aTypes,[function(sData)
{if(typeof sData==='number')
{return'numeric';}
else if(typeof sData!=='string')
{return null;}
var sValidFirstChars="0123456789-";var sValidChars="0123456789.";var Char;var bDecimal=false;Char=sData.charAt(0);if(sValidFirstChars.indexOf(Char)==-1)
{return null;}
for(var i=1;i<sData.length;i++)
{Char=sData.charAt(i);if(sValidChars.indexOf(Char)==-1)
{return null;}
if(Char==".")
{if(bDecimal)
{return null;}
bDecimal=true;}}
return'numeric';},function(sData)
{var iParse=Date.parse(sData);if((iParse!==null&&!isNaN(iParse))||(typeof sData==='string'&&sData.length===0))
{return'date';}
return null;},function(sData)
{if(typeof sData==='string'&&sData.indexOf('<')!=-1&&sData.indexOf('>')!=-1)
{return'html';}
return null;}]);$.fn.DataTable=DataTable;$.fn.dataTable=DataTable;$.fn.dataTableSettings=DataTable.settings;$.fn.dataTableExt=DataTable.ext;}));}(window,document));

$.extend(true,$.fn.dataTable.defaults,{"sDom":"<'row'<'col-xs-6'l><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>","sPaginationType":"bootstrap","oLanguage":{"sLengthMenu":"_MENU_ records per page"}});$.extend($.fn.dataTableExt.oStdClasses,{"sWrapper":"dataTables_wrapper form-inline"});$.fn.dataTableExt.oApi.fnPagingInfo=function(oSettings)
{return{"iStart":oSettings._iDisplayStart,"iEnd":oSettings.fnDisplayEnd(),"iLength":oSettings._iDisplayLength,"iTotal":oSettings.fnRecordsTotal(),"iFilteredTotal":oSettings.fnRecordsDisplay(),"iPage":oSettings._iDisplayLength===-1?0:Math.ceil(oSettings._iDisplayStart/oSettings._iDisplayLength),"iTotalPages":oSettings._iDisplayLength===-1?0:Math.ceil(oSettings.fnRecordsDisplay()/oSettings._iDisplayLength)};};$.extend($.fn.dataTableExt.oPagination,{"bootstrap":{"fnInit":function(oSettings,nPaging,fnDraw){var oLang=oSettings.oLanguage.oPaginate;var fnClickHandler=function(e){e.preventDefault();if(oSettings.oApi._fnPageChange(oSettings,e.data.action)){fnDraw(oSettings);}};$(nPaging).append('<ul class="pagination">'+'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+'</ul>');var els=$('a',nPaging);$(els[0]).bind('click.DT',{action:"previous"},fnClickHandler);$(els[1]).bind('click.DT',{action:"next"},fnClickHandler);},"fnUpdate":function(oSettings,fnDraw){var iListLength=5;var oPaging=oSettings.oInstance.fnPagingInfo();var an=oSettings.aanFeatures.p;var i,ien,j,sClass,iStart,iEnd,iHalf=Math.floor(iListLength/2);if(oPaging.iTotalPages<iListLength){iStart=1;iEnd=oPaging.iTotalPages;}
else if(oPaging.iPage<=iHalf){iStart=1;iEnd=iListLength;}else if(oPaging.iPage>=(oPaging.iTotalPages-iHalf)){iStart=oPaging.iTotalPages-iListLength+1;iEnd=oPaging.iTotalPages;}else{iStart=oPaging.iPage-iHalf+1;iEnd=iStart+iListLength-1;}
for(i=0,ien=an.length;i<ien;i++){$('li:gt(0)',an[i]).filter(':not(:last)').remove();for(j=iStart;j<=iEnd;j++){sClass=(j==oPaging.iPage+1)?'class="active"':'';$('<li '+sClass+'><a href="#">'+j+'</a></li>').insertBefore($('li:last',an[i])[0]).bind('click',function(e){e.preventDefault();oSettings._iDisplayStart=(parseInt($('a',this).text(),10)-1)*oPaging.iLength;fnDraw(oSettings);});}
if(oPaging.iPage===0){$('li:first',an[i]).addClass('disabled');}else{$('li:first',an[i]).removeClass('disabled');}
if(oPaging.iPage===oPaging.iTotalPages-1||oPaging.iTotalPages===0){$('li:last',an[i]).addClass('disabled');}else{$('li:last',an[i]).removeClass('disabled');}}}}});if($.fn.DataTable.TableTools){$.extend(true,$.fn.DataTable.TableTools.classes,{"container":"DTTT btn-group","buttons":{"normal":"btn btn-default","disabled":"disabled"},"collection":{"container":"DTTT_dropdown dropdown-menu","buttons":{"normal":"","disabled":"disabled"}},"print":{"info":"DTTT_print_info modal"},"select":{"row":"active"}});$.extend(true,$.fn.DataTable.TableTools.DEFAULTS.oTags,{"collection":{"container":"ul","button":"li","liner":"a"}});}