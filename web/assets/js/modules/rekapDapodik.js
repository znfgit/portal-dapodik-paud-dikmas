$(document).ready(function(){
    var konektor = $('input.konektor').val();

    $.ajax({
        type: "GET",
        url: konektor + "report/getRekapDapodikCore2",
        success: function(isi){

	        var obj = jQuery.parseJSON(isi);

	        $('.sekolah_total').html(obj.sekolah_nasional);
	        $('.sekolah_sma').html(obj.sma_nasional);
	        $('.sekolah_smk').html(obj.smk_nasional);
	        $('.sekolah_smlb').html(obj.smlb_nasional);

	        $('.pd_total').html(obj.pd_nasional);
	        $('.pd_sma').html(obj.pd_sma_nasional);
	        $('.pd_smk').html(obj.pd_smk_nasional);
	        $('.pd_smlb').html(obj.pd_smlb_nasional);

	        $('.ptk_total').html(obj.ptk_nasional);
	        $('.ptk_sma').html(obj.ptk_sma_nasional);
	        $('.ptk_smk').html(obj.ptk_smk_nasional);
	        $('.ptk_smlb').html(obj.ptk_smlb_nasional);

	        d3.json(konektor + 'report/chart/baselineSekolahNasional', function(json) {

				var chart;
			  	nv.addGraph(function() {
			      	chart = nv.models.multiBarChart()
			      	.showControls(false);
			        // .barColor(d3.scale.category20().range());

			      	chart.multibar
			        .hideable(true);

			      	chart.reduceXTicks(true).staggerLabels(false);

			      	chart.xAxis
			        .showMaxMin(true)
			          //.rotateLabels(-45);
			        ;
			         // .tickFormat(d3.format(',f'));

			      	chart.yAxis
			        .tickFormat(d3.format('.'));

			     
			      	d3.select('#chartBaselineSekolahNasional svg')
			        .datum(json)
			        .transition().duration(500).call(chart);

			      	nv.utils.windowResize(chart.update);

				    // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

			      	return chart;
			  	});

			});

			d3.json(konektor + 'report/chart/baselinePdNasional', function(json) {

				var chart;
			  	nv.addGraph(function() {
			      	chart = nv.models.multiBarChart()
			      	.showControls(false);
			        // .barColor(d3.scale.category20().range());

			      	chart.multibar
			        .hideable(true);

			      	chart.reduceXTicks(true).staggerLabels(false);

			      	chart.xAxis
			        .showMaxMin(true)
			          //.rotateLabels(-45);
			        ;
			         // .tickFormat(d3.format(',f'));

			      	chart.yAxis
			        .tickFormat(d3.format('.'));

			     
			      	d3.select('#chartBaselinePdNasional svg')
			        .datum(json)
			        .transition().duration(500).call(chart);

			      	nv.utils.windowResize(chart.update);

				    // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

			      	return chart;
			  	});

			});

			d3.json(konektor + 'report/chart/baselinePtkNasional', function(json) {

				var chart;
			  	nv.addGraph(function() {
			      	chart = nv.models.multiBarChart()
			      	.showControls(false);
			        // .barColor(d3.scale.category20().range());

			      	chart.multibar
			        .hideable(true);

			      	chart.reduceXTicks(true).staggerLabels(false);

			      	chart.xAxis
			        .showMaxMin(true)
			          //.rotateLabels(-45);
			        ;
			         // .tickFormat(d3.format(',f'));

			      	chart.yAxis
			        .tickFormat(d3.format('.'));

			     
			      	d3.select('#chartBaselinePtkNasional svg')
			        .datum(json)
			        .transition().duration(500).call(chart);

			      	nv.utils.windowResize(chart.update);

				    // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

			      	return chart;
			  	});

			});

        }
    });

    function getTotalCount(datas) {
	          
	  	var sum = 0;
	 	var length = datas.length;
	  	for (var i = 0; i < length; i++) {
	    	sum += datas[i].y;
	    
	 	}
	  
	  	return sum;
	};

    d3.json(konektor + 'report/chart/persentaseSekolahNasional', function(json) {
		          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = ((x/countData)*100).toFixed(2);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPersentaseSekolahNasional svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});

	d3.json(konektor + 'report/chart/persentasePtkNasional', function(json) {
		          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = ((x/countData)*100).toFixed(2);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPersentasePtkNasional svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});

	d3.json(konektor + 'report/chart/persentasePdNasional', function(json) {
		          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = ((x/countData)*100).toFixed(2);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPersentasePdNasional svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});
});