$(document).ready(function(){
	var ptk_id = $('input.ptk_id').val();
	var konektor = $('input.konektor').val();

	// console.log(ptk_id);
	// console.log(konektor);

	$.ajax({
          type: "GET",
          url: konektor + "breadcrumb/sekolahAsal/" + ptk_id,
          success: function(isi){

                // var obj = jQuery.parseJSON(isi);
          
                $('.sekolahAsal').fadeIn('fast','',function(){
                      $('.sekolahAsal').html(isi);
                      //$('#loading').fadeOut('normal');
                });
          }
    });

    $.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getDetailSekolahAsalPtk/" + ptk_id,
          success: function(isi){

          		var obj = jQuery.parseJSON(isi);

          		$('.sekolah').html(obj.nama);
                $('.judul_sekolah').html(obj.nama);
                $('.npsn').html(obj.npsn);
                $('.status').html(obj.status);
				$('.bentuk_pendidikan').html(obj.bentuk_pendidikan_id_str);
				$('.alamat_jalan').html(obj.alamat_jalan);
				$('.rt').html(obj.rt);
				$('.rw').html(obj.rw);
				$('.nama_dusun').html(obj.nama_dusun);
				$('.desa_kelurahan').html(obj.desa_kelurahan);
				$('.kecamatan').html(obj.kecamatan);
				$('.kabupaten').html(obj.kabupaten);
				$('.propinsi').html(obj.propinsi);
				$('.kode_pos').html(obj.kode_pos);
          }
    });

    $.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getTahunAjaran",
          success: function(isi){

          	var obj = jQuery.parseJSON(isi);

          	var optionsSemester = "<option disabled=disabled selected>Pilih Tahun Ajaran</option>";

          	for (var i = obj.length - 1; i >= 0; i--) {
          		optionsSemester += "<option tahun_ajaran='" + obj[i].nama.substring(0,9) + "' value='"+ obj[i].tahun_ajaran_id +"'>" + obj[i].nama + "</option>";
          	};

          	$('#selectTa').html(optionsSemester);

          }
      });

   	$( "#selectTa" ).change(function () {

		$( ".penugasan" ).show();
		var str = $( "#selectTa option:selected" ).val();

		$('.sekolahterdaftar').html('');
  		$('.ta').html('');
  		$('.nomorsurattugas').html('');
  		$('.tanggalsurattugas').html('');
		$('.tmttugas').html('');
		$('.sekolahinduk').html('');

		$('.date_pill').css('background-color', '#f0f0f0');
		$('.checklist').html('');
		$('.date_pill h4').css('color', '#434343');

		$.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getPenugasanPtk/"+ ptk_id +"/" + str,
          success: function(isi){

          		if(isi != 'null'){

          			var obj = jQuery.parseJSON(isi);

	          		$('.sekolahterdaftar').html(obj.nama_sekolah);
	          		$('.ta').html(obj.tahun_ajaran_id);
	          		$('.nomorsurattugas').html(obj.nomor_surat_tugas);


	          		if(obj.tanggal_surat_tugas != null){
						var tanggalst = obj.tanggal_surat_tugas.split("-");

						$('.tanggalsurattugas').html(tanggalst[2].substring(0,2) + '-' + tanggalst[1] + '-' + tanggalst[0]);
					}

					if(obj.tmt_tugas != null){
						var tmt = obj.tmt_tugas.split("-");

						$('.tmttugas').html(tmt[2].substring(0,2) + '-' + tmt[1] + '-' + tmt[0]);
					}

					if(obj.ptk_induk == "1"){
						var induk = "Ya";
					}else if(obj.ptk_induk == "0") {
						var induk = "Tidak";
					}

					$('.sekolahinduk').html(induk);

					if(obj.aktif_bulan_01 == 1){
						$('.jan').css('background-color', '#0F9D58');
						$('.jan h4').css('color', '#ffffff');
						$('.jan span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_02 == 1){
						$('.feb').css('background-color', '#0F9D58');
						$('.feb h4').css('color', '#ffffff');
						$('.feb span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}
					
					if(obj.aktif_bulan_03 == 1){
						$('.mar').css('background-color', '#0F9D58');
						$('.mar h4').css('color', '#ffffff');
						$('.mar span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_04 == 1){
						$('.apr').css('background-color', '#0F9D58');
						$('.apr h4').css('color', '#ffffff');
						$('.apr span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_05 == 1){
						$('.mei').css('background-color', '#0F9D58');
						$('.mei h4').css('color', '#ffffff');
						$('.mei span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_06 == 1){
						$('.jun').css('background-color', '#0F9D58');
						$('.jun h4').css('color', '#ffffff');
						$('.jun span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_07 == 1){
						$('.jul').css('background-color', '#0F9D58');
						$('.jul h4').css('color', '#ffffff');
						$('.jul span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_08 == 1){
						$('.ags').css('background-color', '#0F9D58');
						$('.ags h4').css('color', '#ffffff');
						$('.ags span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_09 == 1){
						$('.sep').css('background-color', '#0F9D58');
						$('.sep h4').css('color', '#ffffff');
						$('.sep span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_10 == 1){
						$('.okt').css('background-color', '#0F9D58');
						$('.okt h4').css('color', '#ffffff');
						$('.okt span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_11 == 1){
						$('.nov').css('background-color', '#0F9D58');
						$('.nov h4').css('color', '#ffffff');
						$('.nov span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_12 == 1){
						$('.des').css('background-color', '#0F9D58');
						$('.des h4').css('color', '#ffffff');
						$('.des span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');

					}


          		}

          		$( ".penugasan" ).hide();
          		
          	}
    	});
	});

    $.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getPenugasanPtk/"+ ptk_id +"/2014",
          success: function(isi){

          		if(isi != 'null'){

	          		var obj = jQuery.parseJSON(isi);

	          		$('.sekolahterdaftar').html(obj.nama_sekolah);
	          		$('.ta').html(obj.tahun_ajaran_id);
	          		$('.nomorsurattugas').html(obj.nomor_surat_tugas);

	          		if(obj.tanggal_surat_tugas != null){
	          			
          				var tanggalst = obj.tanggal_surat_tugas.split("-");

          				$('.tanggalsurattugas').html(tanggalst[2].substring(0,2) + '-' + tanggalst[1] + '-' + tanggalst[0]);
	          			
				
					}

					if(obj.tmt_tugas != null){
						
						var tmt = obj.tmt_tugas.split("-");

						$('.tmttugas').html(tmt[2].substring(0,2) + '-' + tmt[1] + '-' + tmt[0]);
						
						
					}

					if(obj.ptk_induk == "1"){
						var induk = "Ya";
					}else if(obj.ptk_induk == "0") {
						var induk = "Tidak";
					}

					$('.sekolahinduk').html(induk);

					if(obj.aktif_bulan_01 == 1){
						$('.jan').css('background-color', '#0F9D58');
						$('.jan h4').css('color', '#ffffff');
						$('.jan span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_02 == 1){
						$('.feb').css('background-color', '#0F9D58');
						$('.feb h4').css('color', '#ffffff');
						$('.feb span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}
					
					if(obj.aktif_bulan_03 == 1){
						$('.mar').css('background-color', '#0F9D58');
						$('.mar h4').css('color', '#ffffff');
						$('.mar span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_04 == 1){
						$('.apr').css('background-color', '#0F9D58');
						$('.apr h4').css('color', '#ffffff');
						$('.apr span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_05 == 1){
						$('.mei').css('background-color', '#0F9D58');
						$('.mei h4').css('color', '#ffffff');
						$('.mei span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_06 == 1){
						$('.jun').css('background-color', '#0F9D58');
						$('.jun h4').css('color', '#ffffff');
						$('.jun span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_07 == 1){
						$('.jul').css('background-color', '#0F9D58');
						$('.jul h4').css('color', '#ffffff');
						$('.jul span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_08 == 1){
						$('.ags').css('background-color', '#0F9D58');
						$('.ags h4').css('color', '#ffffff');
						$('.ags span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_09 == 1){
						$('.sep').css('background-color', '#0F9D58');
						$('.sep h4').css('color', '#ffffff');
						$('.sep span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_10 == 1){
						$('.okt').css('background-color', '#0F9D58');
						$('.okt h4').css('color', '#ffffff');
						$('.okt span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_11 == 1){
						$('.nov').css('background-color', '#0F9D58');
						$('.nov h4').css('color', '#ffffff');
						$('.nov span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');
					}

					if(obj.aktif_bulan_12 == 1){
						$('.des').css('background-color', '#0F9D58');
						$('.des h4').css('color', '#ffffff');
						$('.des span').html('<span class="icon" style="font-size:30px;color:white">&#61452</span>');

					}
				}
          	}
    });

    $.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getDetailPtk/" + ptk_id,
          success: function(isi){

          		var obj = jQuery.parseJSON(isi);

				if(obj.tanggal_lahir != null){
					
					var tanggal = obj.tanggal_lahir.split("-");

					$('.ttl').html(obj.tempat_lahir + ', ' + tanggal[2].substring(0,2) + '-' + tanggal[1] + '-' + tanggal[0]);
					
				}
				
				if(obj.tgl_cpns != null){
					
					var tgl_cpns = obj.tgl_cpns.split("-");

					$('.tanggalcpns').html(tgl_cpns[2].substring(0,2) + '-' + tgl_cpns[1] + '-' + tgl_cpns[0]);	
					
					
				}

				if(obj.tmt_pengangkatan != null){
					var tmt_pengangkatan = obj.tmt_pengangkatan.split("-");

					$('.tmtpengangkatan').html(tmt_pengangkatan[2].substring(0,2) + '-' + tmt_pengangkatan[1] + '-' + tmt_pengangkatan[0]);
				}

				if(obj.tmt_pns != null){
					var tmt_pns = obj.tmt_pns.split("-");

					$('.tmtpns').html(tmt_pns[2].substring(0,2) + '-' + tmt_pns[1] + '-' + tmt_pns[0]);

				}
				
				// var bulan = tanggal[1];

				if(obj.jenis_kelamin == 'L'){
					var jk = 'Laki-laki';
				}else if(obj.jenis_kelamin == 'P'){
					var jk = 'Perempuan';
				}

				if(obj.status_perkawinan == 1){
					var kawin = 'Kawin';
				}else if(obj.status_perkawinan == 2){
					var kawin = 'Belum Kawin';
				}else if(obj.status_perkawinan == 3){
					var kawin = 'Janda/Duda';
				}

          		$('.nama_ptk').html(obj.nama_ptk);
          		$('.nik').html(obj.nik);
          		$('.nip').html(obj.nip);
          		
          		$('.jk').html(jk);
          		$('.alamat').html(obj.alamat_jalan);
          		$('.rtrw').html(obj.rt + '/' + obj.rw);
          		$('.dusun').html(obj.nama_dusun);
          		$('.kecamatan').html(obj.kecamatan);
          		$('.kabkota').html(obj.kabkota);
          		$('.propinsi').html(obj.propinsi);
          		$('.kodepos').html(obj.kode_pos);
          		$('.telepon').html(obj.no_telepon_rumah);
          		$('.hp').html(obj.no_hp);
          		$('.agama').html(obj.agama);
          		$('.kawin').html(kawin);
          		$('.statuskepegawaian').html(obj.status_kepegawaian);
          		$('.jenisptk').html(obj.jenis_ptk);
          		$('.skcpns').html(obj.sk_cpns);
          		
          		$('.skpengangkatan').html(obj.sk_pengangkatan);
          		
          		$('.lembagapengangkat').html(obj.lembaga_pengangkat);
          		$('.pangkatgolongan').html(obj.pangkat_golongan);
          		$('.sumbergaji').html(obj.sumber_gaji);
          		
          		$('.nuptk').html(obj.nuptk);
          		$('.keahlianlaboratorium').html(obj.keahlian_laboratorium);
          		$('.email').html(obj.email);

          		if(obj.sudah_lisensi_kepala_sekolah == "1"){
          			var lisensikepsek = 'Ya';
          		}else if(obj.sudah_lisensi_kepala_sekolah == "0"){
          			var lisensikepsek = 'Tidak';

          		}
          		$('.sudahlisensikepalasekolah').html(lisensikepsek);

          		if(obj.mampu_handle_kk == 0){
          			var kk = 'Tidak';
          		}else{
          			var kk = 'Ya';
          		}
          		$('.kk').html(kk);

          		if(obj.keahlian_braille == "1"){
          			var kb = 'Ya';
          		}else{
          			var kb = 'Tidak';
          		}
          		$('.kb').html(kb);

          		if(obj.keahlian_bhs_isyarat == "1"){
          			var kbi = 'Ya';
          		}else{
          			var kbi = 'Tidak';
          		}
          		$('.kbi').html(kbi);

          		$('.loading_profil').fadeOut('fast');

          }
    });
});