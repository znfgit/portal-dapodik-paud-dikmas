$(document).ready(function(){
    var konektor = $('input.konektor').val();

    $.ajax({
        type: "GET",
        url: konektor + "report/getRekapDapodikPdRombel",
        success: function(isi){

          var obj = jQuery.parseJSON(isi);

          $('.pd_terdaftar').html(obj.pd_terdaftar);
          $('.pd_terdaftar_persen').html(obj.pd_terdaftar_persen);
          $('.pd_rombel').html(obj.pd_rombel);
          $('.pd_rombel_persen').html(obj.pd_rombel_persen);
          $('.pd_belum_rombel').html(obj.pd_belum_rombel);
          $('.pd_belum_rombel_persen').html(obj.pd_belum_rombel_persen);

        }
    });

    $.ajax({
        type: "GET",
        url: konektor + "report/getRekapDapodikPdNegeriSwasta",
        success: function(isi){

          var obj = jQuery.parseJSON(isi);

          $('.pd_rombel_negeri').html(obj.pd_rombel_negeri);
          $('.pd_rombel_negeri_persen').html(obj.pd_rombel_negeri_persen);

          $('.pd_rombel_swasta').html(obj.pd_rombel_swasta);
          $('.pd_rombel_swasta_persen').html(obj.pd_rombel_swasta_persen);

          $('.pd_rombel_sma_negeri').html(obj.pd_rombel_sma_negeri);
          $('.pd_rombel_sma_negeri_persen').html(obj.pd_rombel_sma_negeri_persen);

          $('.pd_rombel_sma_swasta').html(obj.pd_rombel_sma_swasta);
          $('.pd_rombel_sma_swasta_persen').html(obj.pd_rombel_sma_swasta_persen);

          $('.pd_rombel_smk_negeri').html(obj.pd_rombel_smk_negeri);
          $('.pd_rombel_smk_negeri_persen').html(obj.pd_rombel_smk_negeri_persen);

          $('.pd_rombel_smk_swasta').html(obj.pd_rombel_smk_swasta);
          $('.pd_rombel_smk_swasta_persen').html(obj.pd_rombel_smk_swasta_persen);

          $('.pd_rombel_smlb_negeri').html(obj.pd_rombel_smlb_negeri);
          $('.pd_rombel_smlb_negeri_persen').html(obj.pd_rombel_smlb_negeri_persen);

          $('.pd_rombel_smlb_swasta').html(obj.pd_rombel_smlb_swasta);
          $('.pd_rombel_smlb_swasta_persen').html(obj.pd_rombel_smlb_swasta_persen);

        }
    });

	function getTotalCount(datas) {
	          
	  	var sum = 0;
	 	var length = datas.length;
	  	for (var i = 0; i < length; i++) {
	    	sum += datas[i].y;
	    
	 	}
	  
	  	return sum;
	};

	function chartPemetaanRombel(){
		d3.json(konektor + 'report/chartBarPdRombel', function(json) {

			var chart;
		  	nv.addGraph(function() {
		      	chart = nv.models.multiBarChart()
		      	.showControls(false);
		        // .barColor(d3.scale.category20().range());

		      	chart.multibar
		        .hideable(true);

		      	chart.reduceXTicks(true).staggerLabels(false);

		      	chart.xAxis
		        .showMaxMin(true)
		          //.rotateLabels(-45);
		        ;
		         // .tickFormat(d3.format(',f'));

		      	chart.yAxis
		        .tickFormat(d3.format('.'));

		     
		      	d3.select('#chartbarpemetaanrombel svg')
		        .datum(json)
		        .transition().duration(500).call(chart);

		      	nv.utils.windowResize(chart.update);

			    // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

		      	return chart;
		  	});

		});

		d3.json(konektor + 'report/chartPiePdRombel', function(json) {
	          
	    	var countData = getTotalCount(json);
		    nv.addGraph(function() {
		        var width = 600,
		            height = 500
		        ;

		        var chart = nv.models.pieChart()
		            .x(function(d) { return d.key })
		            .y(function(d) { return d.y })
		            .showLabels(true)
		            .labelType("percent")
		            .donut(false)
					.showLegend(true)
					.tooltipContent(function(key, y, e, graph) {
			            var re = /,/gi;
			            var re2 = /\.00/gi;
			            // replace .00
			            var x = y.replace(re2, "");
			            // replace , to .
			            var x = x.replace(re, "");
			            // console.log(Math.round((x/countData)*100));
			            var percent = Math.round((x/countData)*100);
			            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
			                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
			          })
		            .values(function(d) {
		              
		              return d;
		            })
		            //.color(d3.scale.category10().range())
		            .color(['#66C408','#FFB300', '#FF3636'])
		        ;

		        d3.select("#chartPiePdRombel svg")
		          .datum(json)
		          .transition().duration(500)
		          
		          .call(chart)
		        ;
		        nv.utils.windowResize(chart.update);

		        return chart;
		    });
		});
	}

	chartPemetaanRombel();

	$('a.pemetaanrombel').click(function (e){

		chartPemetaanRombel();
		
	});

	$('a.statussekolah').click(function (e){

		d3.json(konektor + 'report/chartBarPdStatusTotal', function(json) {

			var chart;
		  	nv.addGraph(function() {
		      	chart = nv.models.multiBarChart()
		      	.showControls(true);
		        // .barColor(d3.scale.category20().range());

		      	chart.multibar
		        .hideable(true);

		      	chart.reduceXTicks(true).staggerLabels(false);

		      	chart.xAxis
		        .showMaxMin(true)
		          //.rotateLabels(-45);
		        ;
		         // .tickFormat(d3.format(',f'));

		      	chart.yAxis
		        .tickFormat(d3.format('.'));

		     
		      	d3.select('#chartBarPdStatusTotal svg')
		        .datum(json)
		        .transition().duration(500).call(chart);

		      	nv.utils.windowResize(chart.update);

			    // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

		      	return chart;
		  	});

		});

		d3.json(konektor + 'report/chartPiePdStatusTotal', function(json) {
		          
		    var countData = getTotalCount(json);
		    nv.addGraph(function() {
		        var width = 600,
		            height = 500
		        ;

		        var chart = nv.models.pieChart()
		            .x(function(d) { return d.key })
		            .y(function(d) { return d.y })
		            .showLabels(true)
		            .labelType("percent")
		            .donut(false)
					.showLegend(true)
					.tooltipContent(function(key, y, e, graph) {
			            var re = /,/gi;
			            var re2 = /\.00/gi;
			            // replace .00
			            var x = y.replace(re2, "");
			            // replace , to .
			            var x = x.replace(re, "");
			            // console.log(Math.round((x/countData)*100));
			            var percent = Math.round((x/countData)*100);
			            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
			                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
			          })
		            .values(function(d) {
		              
		              return d;
		            })
		            //.color(d3.scale.category10().range())
		            .color(['#66C408','#FFB300', '#FF3636'])
		        ;

		        d3.select("#chartPiePdStatusTotal svg")
		          .datum(json)
		          .transition().duration(500)
		          
		          .call(chart)
		        ;
		        nv.utils.windowResize(chart.update);

		        return chart;
		    });
		});

		d3.json(konektor + 'report/chartPiePdStatusSma', function(json) {
		          
		    var countData = getTotalCount(json);
		    nv.addGraph(function() {
		        var width = 600,
		            height = 500
		        ;

		        var chart = nv.models.pieChart()
		            .x(function(d) { return d.key })
		            .y(function(d) { return d.y })
		            .showLabels(true)
		            .labelType("percent")
		            .donut(false)
					.showLegend(true)
					.tooltipContent(function(key, y, e, graph) {
			            var re = /,/gi;
			            var re2 = /\.00/gi;
			            // replace .00
			            var x = y.replace(re2, "");
			            // replace , to .
			            var x = x.replace(re, "");
			            // console.log(Math.round((x/countData)*100));
			            var percent = Math.round((x/countData)*100);
			            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
			                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
			          })
		            .values(function(d) {
		              
		              return d;
		            })
		            //.color(d3.scale.category10().range())
		            .color(['#66C408','#FFB300', '#FF3636'])
		        ;

		        d3.select("#chartPiePdStatusSma svg")
		          .datum(json)
		          .transition().duration(500)
		          
		          .call(chart)
		        ;
		        nv.utils.windowResize(chart.update);

		        return chart;
		    });
		});

		d3.json(konektor + 'report/chartPiePdStatusSmk', function(json) {
		          
		    var countData = getTotalCount(json);
		    nv.addGraph(function() {
		        var width = 600,
		            height = 500
		        ;

		        var chart = nv.models.pieChart()
		            .x(function(d) { return d.key })
		            .y(function(d) { return d.y })
		            .showLabels(true)
		            .labelType("percent")
		            .donut(false)
					.showLegend(true)
					.tooltipContent(function(key, y, e, graph) {
			            var re = /,/gi;
			            var re2 = /\.00/gi;
			            // replace .00
			            var x = y.replace(re2, "");
			            // replace , to .
			            var x = x.replace(re, "");
			            // console.log(Math.round((x/countData)*100));
			            var percent = Math.round((x/countData)*100);
			            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
			                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
			          })
		            .values(function(d) {
		              
		              return d;
		            })
		            //.color(d3.scale.category10().range())
		            .color(['#66C408','#FFB300', '#FF3636'])
		        ;

		        d3.select("#chartPiePdStatusSmk svg")
		          .datum(json)
		          .transition().duration(500)
		          
		          .call(chart)
		        ;
		        nv.utils.windowResize(chart.update);

		        return chart;
		    });
		});

		d3.json(konektor + 'report/chartPiePdStatusSmlb', function(json) {
		          
		    var countData = getTotalCount(json);
		    nv.addGraph(function() {
		        var width = 600,
		            height = 500
		        ;

		        var chart = nv.models.pieChart()
		            .x(function(d) { return d.key })
		            .y(function(d) { return d.y })
		            .showLabels(true)
		            .labelType("percent")
		            .donut(false)
					.showLegend(true)
					.tooltipContent(function(key, y, e, graph) {
			            var re = /,/gi;
			            var re2 = /\.00/gi;
			            // replace .00
			            var x = y.replace(re2, "");
			            // replace , to .
			            var x = x.replace(re, "");
			            // console.log(Math.round((x/countData)*100));
			            var percent = Math.round((x/countData)*100);
			            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
			                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
			          })
		            .values(function(d) {
		              
		              return d;
		            })
		            //.color(d3.scale.category10().range())
		            .color(['#66C408','#FFB300', '#FF3636'])
		        ;

		        d3.select("#chartPiePdStatusSmlb svg")
		          .datum(json)
		          .transition().duration(500)
		          
		          .call(chart)
		        ;
		        nv.utils.windowResize(chart.update);

		        return chart;
		    });
		});

	});

	
});