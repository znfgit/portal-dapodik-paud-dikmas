var _konektor = $('input.konektor').val();

$.ajax({
    type: "GET",
  	url: _konektor + "latestDownload",
  	success: function(isi){
          		
        var obj = jQuery.parseJSON(isi);

        $('.dl_nama').html(obj.NAMA);
        $('.dl_tanggal').html(obj.TANGGAL.substring(0,10));
        $('.dl_deskripsi').html(obj.DESKRIPSI);
        $('.dl_link').html('<a style="color:white" target="_blank" href="http://sourceforge.net/projects/dapodikmen/files/'+obj.FILE+'/download">'+
					      		'<button type="button" class="btn btn-success btn-lg" style="width:100%">'+
					      			'<img src="'+_konektor+'assets/img/dl.png" width="50px"/> '+
					      			'Download Installer'+
					      		'</button>'+
					  		'</a>');
    }

});

$.ajax({
    type: "GET",
  	url: _konektor + "getDokumentasi",
  	success: function(isi){
          		
        var obj = jQuery.parseJSON(isi);
        var str = '';

        for (var i = obj.length - 1; i >= 0; i--) {
        	console.log(obj[i]);

        	str += '<div class="panel panel-default">'+
		              '<div class="panel-body">'+
			              	'<div class="icon_dl_dok">'+
			        			'<img src="'+_konektor+'assets/img/pdf.png" />'+
			        		'</div>'+
			        		'<div class="text_dl">'+
				                '<div class="text_dl_left">'+
				              		'<b><h3>'+obj[i].NAMA+'</h3></b>'+
					              	'<h4>Tanggal Rilis: '+obj[i].TANGGAL.substring(0,10)+'</h4>'+
					            '</div>'+
				              	'<div class="text_dl_right">'+
				              		'<a style="color:white" href="'+_konektor+'docs/'+obj[i].FILE+'">'+
					              		'<button type="button" class="btn btn-primary" style="width:100%">'+
					              			'<img src="'+_konektor+'assets/img/dl.png" width="30px"/> '+
					              			'Download'+
					              		'</button>'+
				              		'</a>'+
				              	'</div>'+
				              	'<div class="clear"></div>'+
				            '</div>'+
		              '</div>'+
		            '</div>';
        };

        $('.dok_loop').html(str);
    }

});

$.ajax({
    type: "GET",
  	url: _konektor + "getFormulir",
  	success: function(isi){
          		
        var obj = jQuery.parseJSON(isi);
        var str = '';

        for (var i = obj.length - 1; i >= 0; i--) {
        	console.log(obj[i]);

        	str += '<div class="panel panel-default">'+
		              '<div class="panel-body">'+
			              	'<div class="icon_dl_dok">'+
			        			'<img src="'+_konektor+'assets/img/pdf.png" />'+
			        		'</div>'+
			        		'<div class="text_dl">'+
				                '<div class="text_dl_left">'+
				              		'<b><h3>'+obj[i].NAMA+'</h3></b>'+
					              	'<h4>Tanggal Rilis: '+obj[i].TANGGAL.substring(0,10)+'</h4>'+
					            '</div>'+
				              	'<div class="text_dl_right">'+
				              		'<a style="color:white" href="'+_konektor+'docs/'+obj[i].FILE+'">'+
					              		'<button type="button" class="btn btn-primary" style="width:100%">'+
					              			'<img src="'+_konektor+'assets/img/dl.png" width="30px"/> '+
					              			'Download'+
					              		'</button>'+
				              		'</a>'+
				              	'</div>'+
				              	'<div class="clear"></div>'+
				            '</div>'+
		              '</div>'+
		            '</div>';
        };

        $('.form_loop').html(str);
    }

});

