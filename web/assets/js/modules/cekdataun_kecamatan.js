$(document).ready(function(){
    var konektor = $('input.konektor').val();
    var id_level_wilayah = $('input.id_level_wilayah').val();
    var kode_wilayah = $('input.kode_wilayah').val();

      $.ajax({
        type: "GET",
        url: konektor + "breadcrumb/getsemesterterakhir",
        success: function(isi){
              
              var obj = jQuery.parseJSON(isi);
              // console.log(isi);

              var semester = obj.semester_id.substring(4);

              var semArr = { 1 : "Ganjil", 2 : "Genap" };

              console.log(semester);

              $('.judul_ta_semester').html("Semester " + semArr[semester] + " Tahun Ajaran " + obj.tahun_ajaran);

              console.log(id_level_wilayah);

              $.ajax({
                  type: "GET",
                  url: konektor + "report/cekdataun_kecamatan/"+id_level_wilayah+"/"+kode_wilayah,
                  success: function(isi){
                  
                      $('#ls').fadeIn('fast','',function(){
                          $('#ls').html(isi);
                          //$('#loading').fadeOut('normal');
                      });
                  },
                  error: function(){
                      $('#ls').fadeIn('fast','',function(){
                          $('#ls').load('report/reportProp');
                      });
                  }
              });
          }
      });

});