$(document).ready(function(){
    var konektor = $('input.konektor').val();
    var kode_wilayah = $('input.kode_wilayah').val();

    $.ajax({
        type: "GET",
        url: konektor + "breadcrumb/getsemester",
        success: function(isi){

          var obj = jQuery.parseJSON(isi);

          var optionsSemester = "";

          for (var i = obj.length - 1; i >= 0; i--) {
              optionsSemester += "<option tahun_ajaran='" + obj[i].nama.substring(0,9) + "' value='"+ obj[i].semester_id +"'>" + obj[i].nama + "</option>";
          };

          $('#selectSemester').html(optionsSemester);

        }
    });

    $.ajax({
        type: "GET",
        url: konektor + "breadcrumb/rekappdkabkota/" + kode_wilayah,
        success: function(isi){

          $('.nama_propinsi').html(isi);

        }
    });

    $.ajax({
        type: "GET",
        url: konektor + "breadcrumb/getsemesterterakhir",
        success: function(isi){
              
            var obj = jQuery.parseJSON(isi);
              // console.log(isi);

            var semester = obj.semester_id.substring(4);

            var semArr = { 1 : "Ganjil", 2 : "Genap" };

            // console.log(semester);

            $('.semester_terakhir').html("Semester " + semArr[semester] + " Tahun Ajaran " + obj.tahun_ajaran);

            $.ajax({
                  type: "GET",
                  url: konektor + "report/rekapPdKabkota/" + kode_wilayah + "/" + obj.semester_id,
                  success: function(isi){

                  	$('.loading_profil').hide();
                  
                        $('.tabel_rekap').fadeIn('fast','',function(){
                          $('.tabel_rekap').html(isi);
                          //$('#loading').fadeOut('normal');
                    });
                  },
                  error: function(){
                  	$('.loading_profil').hide();

                      $('.tabel_rekap').fadeIn('fast','',function(){
                          $('.tabel_rekap').html('Ada kesalahan dalam memuat data');
                      });
                  }
            });
        }
    });

	$( "#selectSemester" ).change(function () {
		$('.tabel_rekap').html('');

		$('.loading_profil').show();

		var str = $( "#selectSemester option:selected" ).val();

		var semester = str.substring(4);

    	var semArr = { 1 : "Ganjil", 2 : "Genap" };

    	var judul = $( "#selectSemester option:selected" ).attr('tahun_ajaran');

    	$('.semester_terakhir').html("Semester " + semArr[semester] + " Tahun Ajaran " + judul);

		$.ajax({
	          type: "GET",
	          url: konektor + "report/rekapPdKabkota/" + kode_wilayah + "/" + str,
	          success: function(isi){
	          	$('.loading_profil').hide();
	          
	              $('.tabel_rekap').fadeIn('fast','',function(){
	                  $('.tabel_rekap').html(isi);
	                  //$('#loading').fadeOut('normal');
	              });
	          },
	          error: function(){
	          	$('.loading_profil').hide();

	              $('.tabel_rekap').fadeIn('fast','',function(){
	                  $('.tabel_rekap').html('Ada Kesalahan dalam memuat data');
	              });
	          }
	      });
	
	});
});