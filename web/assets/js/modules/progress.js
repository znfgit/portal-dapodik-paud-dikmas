$(document).ready(function(){
    var konektor = $('input.konektor').val();
    
    $.ajax({
        type: "GET",
        url: konektor + "breadcrumb/getsemester",
        success: function(isi){

          var obj = jQuery.parseJSON(isi);

          var optionsSemester = "";

          for (var i = obj.length - 1; i >= 0; i--) {
              optionsSemester += "<option tahun_ajaran='" + obj[i].nama.substring(0,9) + "' value='"+ obj[i].semester_id +"'>" + obj[i].nama + "</option>";
          };

          $('#selectSemester').html(optionsSemester);

        }
    });

    $( "#selectSemester" ).change(function () {

      var str = $( "#selectSemester option:selected" ).val();

      $.ajax({
          type: "GET",
          url: konektor + "report/reportPropPeriodik/" + str,
          success: function(isi){
          
              $('#ls').fadeIn('fast','',function(){
                  $('#ls').html(isi);
                  //$('#loading').fadeOut('normal');
              });
          },
          error: function(){
              $('#ls').fadeIn('fast','',function(){
                  $('#ls').html('Ada kesalahan dalam memuat data');
              });
          }
      });


    });

      $.ajax({
        type: "GET",
        url: konektor + "breadcrumb/getsemesterterakhir",
        success: function(isi){
              
              var obj = jQuery.parseJSON(isi);
              // console.log(isi);

              var semester = obj.semester_id.substring(4);

              var semArr = { 1 : "Ganjil", 2 : "Genap" };

              console.log(semester);

              $('.judul_ta_semester').html("Semester " + semArr[semester] + " Tahun Ajaran " + obj.tahun_ajaran);

              $.ajax({
                  type: "GET",
                  url: konektor + "report/reportPropPeriodik/" + obj.semester_id,
                  success: function(isi){
                  
                      $('#ls').fadeIn('fast','',function(){
                          $('#ls').html(isi);
                          //$('#loading').fadeOut('normal');
                      });
                  },
                  error: function(){
                      $('#ls').fadeIn('fast','',function(){
                          $('#ls').load('report/reportProp');
                      });
                  }
              });
          }
      });

});