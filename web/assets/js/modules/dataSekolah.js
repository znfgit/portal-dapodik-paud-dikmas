$(document).ready(function(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = dd+'/'+mm+'/'+yyyy;
	
	$('.hari_ini').html(today + ' 00:00:00');

	var sekolah_id = $('input.sekolah_id').val();
	var konektor = $('input.konektor').val();
	// $('#mask').fadeIn('fast');
	// $('#mask').html('<div style="font-size:18px;color:white;width:50%;margin:auto;margin-top:15%;text-align:center">'+
					// 	'<img src="../../assets/img/loading8.gif" /><br>' +
					// 	'Memuat data sekolah ...'+
					// '</div>');
	// console.log(sekolah_id);
	// console.log(konektor);

	$.ajax({
          type: "GET",
          url: konektor + "breadcrumb/datasekolah/" + sekolah_id,
          success: function(isi){
          		
                var obj = jQuery.parseJSON(isi);

                $('.sekolah').html(obj.nama);
                $('.judul_sekolah').html(obj.nama);
                $('.npsn').html(obj.npsn);
                $('.status').html(obj.status);
				$('.bentuk_pendidikan').html(obj.bentuk_pendidikan_id_str);
				$('.alamat_jalan').html(obj.alamat_jalan);
				$('.rt').html(obj.rt);
				$('.rw').html(obj.rw);
				$('.nama_dusun').html(obj.nama_dusun);
				$('.desa_kelurahan').html(obj.desa_kelurahan);
				$('.kecamatan').html(obj.kecamatan);
				$('.kabupaten').html(obj.kabupaten);
				$('.propinsi').html(obj.propinsi);
				$('.kode_pos').html(obj.kode_pos);
				$('.lintang').html(obj.lintang);
				$('.bujur').html(obj.bujur);
				$('.nomor_telepon').html(obj.nomor_telepon);
				$('.nomor_fax').html(obj.nomor_fax);
				$('.website').html(obj.website);
				$('.email').html(obj.email);
				$('.sk_pendirian_sekolah').html(obj.sk_pendirian_sekolah);
				$('.tanggal_sk_pendirian').html(obj.tanggal_sk_pendirian);
				$('.status_kepemilikan_id_str').html(obj.status_kepemilikan_id_str);
				$('.sk_izin_operasional').html(obj.sk_izin_operasional);
				$('.tanggal_sk_izin_operasional').html(obj.tanggal_sk_izin_operasional);
				// $('.jumlah_ptk').html(obj.jumlah_ptk);
				// $('.jumlah_guru').html(obj.jumlah_guru);
				// $('.jumlah_administrasi').html(obj.jumlah_administrasi);
				// $('.jumlah_pengawas').html(obj.jumlah_pengawas);
				// $('.jumlah_pd').html(obj.jumlah_pd);
				// $('.jumlah_pd_lk').html(obj.jumlah_pd_lk);
				// $('.jumlah_pd_pr').html(obj.jumlah_pd_pr);

				// console.log(obj.jumlah_ptk);
				$('.loading_profil').fadeOut('fast');
                
          }
    });

	$.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getsemester",
          success: function(isi){

          	var obj = jQuery.parseJSON(isi);

          	var optionsSemester = "";

          	for (var i = obj.length - 1; i >= 0; i--) {
          		optionsSemester += "<option tahun_ajaran='" + obj[i].nama.substring(0,9) + "' value='"+ obj[i].semester_id +"'>" + obj[i].nama + "</option>";
          	};

          	$('#selectSemester').html(optionsSemester);

          }
      });

	$.ajax({
          type: "GET",
          url: konektor + "breadcrumb/getsemesterterakhir",
          success: function(isi){
          		
                var obj = jQuery.parseJSON(isi);

                var semester = obj.semester_id.substring(4);

                var semArr = { 1 : "Ganjil", 2 : "Genap" };

                console.log(semester);
                // console.log(tahun_ajaran);


				$('.semester_terakhir').html("Semester " + semArr[semester] + " Tahun Ajaran " + obj.tahun_ajaran);

				$.ajax({
			          type: "GET",
			          url: konektor + "breadcrumb/breadcrumbDataSekolahJumlahPtk/"+sekolah_id+"/" + obj.semester_id,
			          success: function(isi){
			          		
			                var obj = jQuery.parseJSON(isi);

							$('.jumlah_ptk').html(obj.jumlah_ptk);
							$('.jumlah_guru').html(obj.jumlah_guru);
							$('.jumlah_administrasi').html(obj.jumlah_administrasi);
							$('.jumlah_pengawas').html(obj.jumlah_pengawas);
							
							// $('#mask').fadeOut('fast');
							$('.loading_ptk').fadeOut('fast');
			                
			          }
			    });

			    $.ajax({
			          type: "GET",
			          url: konektor + "breadcrumb/breadcrumbDataSekolahJumlahPd/"+sekolah_id+"/" + obj.semester_id,
			          success: function(isi){
			          		
			                var obj = jQuery.parseJSON(isi);

							$('.jumlah_pd').html(obj.jumlah_pd);
							$('.jumlah_pd_lk').html(obj.jumlah_pd_lk);
							$('.jumlah_pd_pr').html(obj.jumlah_pd_pr);
							
							$('.loading_pd').fadeOut('fast');
			                
			          }
			    });

			    // $.ajax({
			    //       type: "GET",
			    //       url: konektor + "breadcrumb/breadcrumbDataSekolahJumlahPdTotal/" + sekolah_id,
			    //       success: function(isi){
			          		
			    //             var obj = jQuery.parseJSON(isi);

							// $('.jumlah_pd_total').html(obj.jumlah_pd);
							// $('.jumlah_pd_lk_total').html(obj.jumlah_pd_lk);
							// $('.jumlah_pd_pr_total').html(obj.jumlah_pd_pr);
							
							// $('.loading_pd_total').fadeOut('fast');
			                
			    //       }
			    // });

			    $.ajax({
			          type: "GET",
			          url: konektor + "breadcrumb/breadcrumbpdperrombel/" + sekolah_id + "/" + obj.semester_id,
			          success: function(isi){
			          		
			                var obj = jQuery.parseJSON(isi);

							$('.pd_kelas_x').html(obj.pd_kelas_x);
							$('.pd_kelas_xi').html(obj.pd_kelas_xi);
							$('.pd_kelas_xii').html(obj.pd_kelas_xii);
							$('.pd_kelas_xiii').html(obj.pd_kelas_xiii);
							
							
			          }
			    });

			    $.ajax({
			          type: "GET",
			          url: konektor + "getSekolahLongitudinal?sekolah_id="+sekolah_id+"&semester_id="+obj.semester_id,
			          success: function(isi){
			          		
			                var obj = jQuery.parseJSON(isi);

							if(obj.partisipasi_bos == 1){
								var part = 'Ya';
							}else{
								var part = 'Tidak';
							}

							$('.partisipasi_bos').html(part);
							$('.waktu_penyelenggaraan_id_str').html(obj.waktu_penyelenggaraan_id_str);

							
							$('.loading_sl').fadeOut('fast');
			          }
			    });

			    // $.ajax({
			    //       type: "GET",
			    //       url: konektor + "breadcrumb/breadcrumbDataSekolahJumlahPdTotal/" + sekolah_id,
			    //       success: function(isi){
			          		
			    //             var obj = jQuery.parseJSON(isi);

							// $('.jumlah_pd_total').html(obj.jumlah_pd);
							// $('.jumlah_pd_lk_total').html(obj.jumlah_pd_lk);
							// $('.jumlah_pd_pr_total').html(obj.jumlah_pd_pr);
							
							// $('.loading_pd_total').fadeOut('fast');
			                
			    //       }
			    // });

			    $.ajax({
				      type: "GET",
				      url: konektor + "data/logSinkronisasi/" + sekolah_id,
				      success: function(isi){
				      		
				            var obj = jQuery.parseJSON(isi);

				            $('.sinkron_terakhir').html(obj.tgl);

							$('.loading_sinkron').hide();
							
				      }
				});
								                
          }
    });

	$( "#selectSemester" ).change(function () {

    	var str = $( "#selectSemester option:selected" ).val();

    	var semester = str.substring(4);
    	var semArr = { 1 : "Ganjil", 2 : "Genap" };

    	var judul = $( "#selectSemester option:selected" ).attr('tahun_ajaran');

    	$('.semester_terakhir').html("Semester " + semArr[semester] + " Tahun Ajaran " + judul);

    	// console.log(str);
    	$('.loading_ptk').fadeIn('fast');
		$('.loading_pd').fadeIn('fast');
		$('.loading_sl').fadeIn('fast');

		$('.jumlah_ptk').html('');
		$('.jumlah_guru').html('');
		$('.jumlah_administrasi').html('');
		$('.jumlah_pengawas').html('');
		$('.jumlah_pd').html('');
		$('.jumlah_pd_lk').html('');
		$('.jumlah_pd_pr').html('');
		$('.pd_kelas_x').html('');
		$('.pd_kelas_xi').html('');
		$('.pd_kelas_xii').html('');
		$('.partisipasi_bos').html('');
		$('.waktu_penyelenggaraan_id_str').html('');

    	$.ajax({
	          type: "GET",
	          url: konektor + "breadcrumb/breadcrumbDataSekolahJumlahPtk/"+sekolah_id+"/" + str,
	          success: function(isi){
	          		
	                var obj = jQuery.parseJSON(isi);

					$('.jumlah_ptk').html(obj.jumlah_ptk);
					$('.jumlah_guru').html(obj.jumlah_guru);
					$('.jumlah_administrasi').html(obj.jumlah_administrasi);
					$('.jumlah_pengawas').html(obj.jumlah_pengawas);
					
					// $('#mask').fadeOut('fast');
					$('.loading_ptk').fadeOut('fast');
	                
	          }
	    });

	    $.ajax({
	          type: "GET",
	          url: konektor + "breadcrumb/breadcrumbDataSekolahJumlahPd/"+sekolah_id+"/" + str,
	          success: function(isi){
	          		
	                var obj = jQuery.parseJSON(isi);

					$('.jumlah_pd').html(obj.jumlah_pd);
					$('.jumlah_pd_lk').html(obj.jumlah_pd_lk);
					$('.jumlah_pd_pr').html(obj.jumlah_pd_pr);
					
					$('.loading_pd').fadeOut('fast');
	                
	          }
	    });

	    $.ajax({
		      type: "GET",
		      url: konektor + "breadcrumb/breadcrumbpdperrombel/" + sekolah_id + "/" + str,
		      success: function(isi){
		      		
		            var obj = jQuery.parseJSON(isi);

					$('.pd_kelas_x').html(obj.pd_kelas_x);
					$('.pd_kelas_xi').html(obj.pd_kelas_xi);
					$('.pd_kelas_xii').html(obj.pd_kelas_xii);
					
					
		      }
		});

		$.ajax({
			          type: "GET",
			          url: konektor + "getSekolahLongitudinal?sekolah_id="+sekolah_id+"&semester_id="+str,
			          success: function(isi){
			          		if(isi != 'null'){

				                var obj = jQuery.parseJSON(isi);

								if(obj.partisipasi_bos == 1){
									var part = 'Ya';
								}else{
									var part = 'Tidak';
								}

								$('.partisipasi_bos').html(part);
								$('.waktu_penyelenggaraan_id_str').html(obj.waktu_penyelenggaraan_id_str);

			          		}else{
			          			$('.partisipasi_bos').html('Belum diisi');
								$('.waktu_penyelenggaraan_id_str').html('Belum diisi');

			          		}
							
							$('.loading_sl').fadeOut('fast');
			          }
			    });

	});

	

	// $.ajax({
 //          type: "GET",
 //          url: konektor + "breadcrumb/getjumlahsiswaperrombel/{{sekolah_id}}",
 //          success: function(isi){
          		
 //                var obj = jQuery.parseJSON(isi);

 //                var arrayLength = obj.length;
 //                var html = '';

	// 			for (var i = 0; i < arrayLength; i++) {
	// 			    console.log(obj[i]);
				    
	// 			    html += '<div class="form-group">' +
	// 			  		'<div style="float:left;width:40%;border:0px solid #ccc"><b>Jumlah PD Kelas '+ obj[i].tingkat_pendidikan_id +'</b></div>' +
	// 			  		'<div style="float:left;width:5%;border:0px solid #ccc">:</div>' +
	// 			  		'<div style="float:left;width:55%;border:0px solid #ccc"><span class="jumlah_pd_pr">'+ obj[i].jumlah +'</span></div>' +
	// 				  	'<div class="clear"></div>' +
	// 				'</div>';
	// 			}

	// 			$('.jumlah_rombel').html(html);
 //          }
 //    });
});