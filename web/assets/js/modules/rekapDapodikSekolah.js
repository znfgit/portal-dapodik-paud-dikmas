$(document).ready(function(){
    var konektor = $('input.konektor').val();

    $.ajax({
        type: "GET",
        url: konektor + "report/getRekapDapodikSekolahStatus",
        success: function(isi){

          var obj = jQuery.parseJSON(isi);

          $('.status_negeri').html(obj.status_negeri);
          $('.status_negeri_persen').html(obj.status_negeri_persen);

          $('.status_swasta').html(obj.status_swasta);
          $('.status_swasta_persen').html(obj.status_swasta_persen);

          $('.status_sma_negeri').html(obj.status_sma_negeri);
          $('.status_sma_negeri_persen').html(obj.status_sma_negeri_persen);

          $('.status_sma_swasta').html(obj.status_sma_swasta);
          $('.status_sma_swasta_persen').html(obj.status_sma_swasta_persen);

          $('.status_smk_negeri').html(obj.status_smk_negeri);
          $('.status_smk_negeri_persen').html(obj.status_smk_negeri_persen);

          $('.status_smk_swasta').html(obj.status_smk_swasta);
          $('.status_smk_swasta_persen').html(obj.status_smk_swasta_persen);

          $('.status_smlb_swasta').html(obj.status_smlb_swasta);
          $('.status_smlb_swasta_persen').html(obj.status_smlb_swasta_persen);

          $('.status_smlb_negeri').html(obj.status_smlb_negeri);
          $('.status_smlb_negeri_persen').html(obj.status_smlb_negeri_persen);

        }
    });

	function getTotalCount(datas) {
	          
	  	var sum = 0;
	 	var length = datas.length;
	  	for (var i = 0; i < length; i++) {
	    	sum += datas[i].y;
	    
	 	}
	  
	  	return sum;
	};

	d3.json(konektor + 'report/chartPieSekolahStatus/total', function(json) {
	          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = Math.round((x/countData)*100);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPieSekolahStatusTotal svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});

	d3.json(konektor + 'report/chartPieSekolahStatus/sma', function(json) {
	          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = Math.round((x/countData)*100);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPieSekolahStatusSma svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});

	d3.json(konektor + 'report/chartPieSekolahStatus/smk', function(json) {
	          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = Math.round((x/countData)*100);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPieSekolahStatusSmk svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});

	d3.json(konektor + 'report/chartPieSekolahStatus/smlb', function(json) {
	          
	    var countData = getTotalCount(json);
	    nv.addGraph(function() {
	        var width = 600,
	            height = 500
	        ;

	        var chart = nv.models.pieChart()
	            .x(function(d) { return d.key })
	            .y(function(d) { return d.y })
	            .showLabels(true)
	            .labelType("percent")
	            .donut(false)
				.showLegend(true)
				.tooltipContent(function(key, y, e, graph) {
		            var re = /,/gi;
		            var re2 = /\.00/gi;
		            // replace .00
		            var x = y.replace(re2, "");
		            // replace , to .
		            var x = x.replace(re, "");
		            // console.log(Math.round((x/countData)*100));
		            var percent = Math.round((x/countData)*100);
		            return '<h2 class="btn-sm"><strong>' + key + '</strong></h2>' +
		                   '<p>' +  x + ' <strong> (' + percent + '%)</strong>' + '</p>'
		          })
	            .values(function(d) {
	              
	              return d;
	            })
	            //.color(d3.scale.category10().range())
	            .color(['#66C408','#FFB300', '#FF3636'])
	        ;

	        d3.select("#chartPieSekolahStatusSmlb svg")
	          .datum(json)
	          .transition().duration(500)
	          
	          .call(chart)
	        ;
	        nv.utils.windowResize(chart.update);

	        return chart;
	    });
	});
});