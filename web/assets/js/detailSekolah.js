$(document).ready(function(){
	$('.nav-tabs').fadeOut('fast');
	$('#myTabContent').fadeOut('fast');

    $('#btn_koreg').click(function (e){
    	var koreg = $( "input#koreg_validasi").val();
    	var sekolah_id = '{{sekolah_id}}';

    	$.ajax({
            type: "POST",
            url: "{{konektor}}report/validasiKoreg",
            data: {"koreg" : koreg, "sekolah_id" : sekolah_id},
            success: function(isi){
				var obj = jQuery.parseJSON(isi);

				if(obj.success == true){
					$('.nav-tabs').fadeIn('fast');
					$('#myTabContent').fadeIn('fast');
					$('.block_koreg').fadeOut('fast');
					
					$('#ptk_tabel').dataTable({
					    "bProcessing": true,
					    "bAutoWidth": true,
					    "sAjaxSource": "{{konektor}}report/daftarPtkJson/{{sekolah_id}}",
					    "aoColumnDefs": [
					        { "sClass": "text-left", "aTargets": [ 0 ] }
					    ],
					    "aoColumns": [
					        { "mDataProp": "no" },
					        { "mDataProp": "nama" },
					        { "mDataProp": "jenis_kelamin" },
					        { "mDataProp": "nuptk" },
					        { "mDataProp": "status_kepegawaian_id_str" }
					    ]
					});

					$('#pd_tabel').dataTable({
					    "bProcessing": true,
					    "bAutoWidth": true,
					    "sAjaxSource": "{{konektor}}report/daftarPdJson/{{sekolah_id}}",
					    "aoColumnDefs": [
					        { "sClass": "text-left", "aTargets": [ 0 ] }
					    ],
					    "aoColumns": [
					        { "mDataProp": "no" },
					        { "mDataProp": "nama" },
					        { "mDataProp": "jenis_kelamin" },
					        { "mDataProp": "nisn" },
					        { "mDataProp": "nik" }
					    ]
					});

					$.ajax({
			            type: "GET",
			            url: "{{konektor}}report/detailSekolah/{{sekolah_id}}",
			            success: function(isi){
			            
			            	$('#ls').fadeIn('fast','',function(){
			            		$('#ls').html(isi);
			            		//$('#loading').fadeOut('normal');
			            	});
			            },
			            error: function(){
			            	$('#ls').fadeIn('fast','',function(){
			            		$('#ls').load('report/reportProp');
			            	});
			            }
			        });

				}else if(obj.success == false){
					
					alert('Kode Registrasi Salah');

				}	            
            	
            },
            error: function(){
            	
            }
        });
    });
	/*
    $.ajax({
        type: "GET",
        data: { start: 0, limit: 20 },
        url: "{{konektor}}report/daftarPtk/{{sekolah_id}}",
        success: function(isi){
        
        	$('#ptk').fadeIn('fast','',function(){
        		$('#ptk').html(isi);
        		//$('#loading').fadeOut('normal');
        	});
        },
        error: function(){
        	$('#ptk').fadeIn('fast','',function(){
        		$('#ptk').load('report/reportProp');
        	});
        }
    });
	*/
});