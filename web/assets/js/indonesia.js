		//var map = L.map('map').setView([-2.986927,123.699463], 5);
		var map = L.map('map').setView([-2.986927,117.591065], 5);

		/*
		var cloudmade = L.tileLayer('http://{s}.tile.cloudmade.com/{key}/{styleId}/256/{z}/{x}/{y}.png', {
			attribution: 'Map data &copy; 2014 OpenStreetMap contributors, Imagery &copy; 2014 CloudMade',
			key: 'BC9A493B41014CAABB98F0471D759707',
			//styleId: 31891,
			styleId: 22677
		}).addTo(map);
		*/
		var tile = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png?{foo}', {foo: 'bar'}).addTo(map);
	

		// control that shows state info on hover
		var info = L.control();

		info.onAdd = function (map) {
			this._div = L.DomUtil.create('div', 'info');
			this.update();
			return this._div;
		};

		info.update = function (props, candidate) {

		    if (props){
		    	this._div.innerHTML = '<center><h4 style=color:white>' + props.name + '</h4>'
		    							+'</center>'
		    							+'<hr><span style=font-size:13px>Persentase Total: ' + parseFloat(Math.round(props.persen * 100) / 100).toFixed(2) + ' % </span><br>'
		    							+'<hr><span style=font-size:12px>SMA: ' + props.sma + '</span><br>'
		    							+'<span style=font-size:12px>SMK: ' + props.smk + '</span><br>'
		    							+'<span style=font-size:12px>SMLB: ' + props.smlb + '</span><br>'
		    							+'<hr><span style=font-size:12px>Pengiriman SMA: ' + props.kirim_sma + '</span><br>'
		    							+'<span style=font-size:12px>Pengiriman SMK: ' + props.kirim_smk + '</span><br>'
		    							+'<span style=font-size:12px>Pengiriman SMLB: ' + props.kirim_smlb + '</span><br>'
		    							;
		    
		    }
		    else{
		        this._div.innerHTML = '<center><h4 style=color:white>Progress Pengiriman Data</h4><hr>Arahkan Kursor ke Propinsi</center>';
		    }
		};

		info.addTo(map);


		// get color depending on population amount
		function getColor(d) {
			return d > 1000 ? '#800026' :
			       d > 500  ? '#BD0026' :
			       d > 200  ? '#E31A1C' :
			       d > 100  ? '#FC4E2A' :
			       d > 50   ? '#FD8D3C' :
			       d > 20   ? '#FEB24C' :
			       d > 10   ? '#FED976' :
			                  '#FFEDA0';
		}

		function style(feature) {
			return {
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.7,
				fillColor: getColor(feature.properties.persen)
			};
		}

		function highlightFeature(e) {
			var layer = e.target;

			layer.setStyle({
				weight: 2,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});

			if (!L.Browser.ie && !L.Browser.opera) {
				layer.bringToFront();
			}
//            console.dir(layer);
			info.update(layer.feature.properties, statesData.candidate);
		}

		var geojson;

		function resetHighlight(e) {
			geojson.resetStyle(e.target);
			info.update();
		}

		function zoomToFeature(e) {
			map.fitBounds(e.target.getBounds());

			console.log(feature.properties.kode_wilayah);
		}

		function onEachFeature(feature, layer) {
//		    console.dir(layer);
			layer.on({
				mouseover: highlightFeature,
				mouseout: resetHighlight
			});

			layer.on('click', function (e) {
		       	var kode_wilayah = feature.properties.kode_wilayah;

		       	//window.location.replace("laman/detailprop/" + kode_wilayah);

		       	map.fitBounds(e.target.getBounds());

		       	this._div.innerHTML = '<center><h4 style=color:white>' + feature.properties.name + '</h4><hr></center>';
		    });
		}

		var popup = L.popup();

		function onMapClick(e) {
			var layer = e.target;

			console.log(layer.properties.kode_wilayah);

			//return "You clicked the map at propinsi ";
			
		}

		map.on('click', onMapClick);
		/*
		$('#load-peta').click(function (e){
			
			var script = document.createElement('script');
			script.src = 'http://dapo.dikmen.kemdikbud.go.id/portal/web/peta/geojson?callback=loadpeta';

			document.getElementsByTagName('head')[0].appendChild(script);

			$('#load-peta').fadeIn('fast','',function(){
        		$('#load-peta').html('memproses..');
        	});

			
			$.ajax({
	            type: "GET",
	            url: "../assets/js/3.min.js",
	            success: function(isi){
	            	var statesData = isi;
	            */
	            	geojson = L.geoJson(statesData, {
						style: style,
						onEachFeature: onEachFeature
					}).addTo(map);
	            	/*
	            }
	        });
		

		});
		*/
		
		function loadpeta(data)
		{
			$('#load-peta').fadeIn('fast','',function(){
        		$('#load-peta').html('memproses..');
        	});

			geojson = L.geoJson(statesData, {
				style: style,
				onEachFeature: onEachFeature
			}).addTo(map);

		}

		map.attributionControl.addAttribution('Statistic Data provided by &copy; 2014 Kementerian Pendidikan dan Kebudayaan');


		var legend = L.control({position: 'bottomleft'});
        var datas = '';
		legend.onAdd = function (map) {

			var div = L.DomUtil.create('div', 'legend'),
				grades = [0, 10, 20, 50, 100, 200, 500, 1000],
				labels = [],
				from, to, datas2;

    		var datas2 = '';
    		for (i=0;i<=100;i++){
        	    datas += ('<tr><td class="left">Data '+i+'</td><td class="right">'+(i*3)+'</td></tr>');
    		}
    		console.log(datas);


			div.innerHTML = '<table class="tbl"  border="1">' +
				'<thead><tr><th class="left"><b>Province</b></th><th class="right"><b>Percentage</b></th></tr></thead>' +
				'<tbody id="item">' +
                datas
				'</tbody>' +
                '</table></center>';
			return div;
		};