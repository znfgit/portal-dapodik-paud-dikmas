datas2 = [
  {
    key: "Rasio PTK",
    values: [
    {
      "label"  : "Kepala Tenaga Administrasi",
      "value"  : 27
    },
    {
      "label"  : "Tenaga Administrasi",
      "value"  : 232
    },
    {
      "label"  : "Pesuruh/Penjaga Sekolah",
      "value"  : 0
    },
    {
      "label"  : "Kepala Perpustakaan",
      "value"  : 8
    },
    {
      "label"  : "Tenaga Perpustakaan",
      "value"  : 146
    },
    {
      "label"  : "Pustakawan",
      "value"  : 0
    },
    {
      "label"  : "Pengelola Kursus",
      "value"  : 0
    },
    {
      "label"  : "Pengelola Keaksaraan",
      "value"  : 0
    },
    {
      "label"  : "Pengelola PAUD",
      "value"  : 0
    },
    {
      "label"  : "Pengelola Kesetaraan",
      "value"  : 0
    },
    {
      "label"  : "Pengelola TBM",
      "value"  : 0
    },
    {
      "label"  : "Tutor Kesetaraan",
      "value"  : 0
    },
    {
      "label"  : "Pamong Belajar",
      "value"  : 0
    },
    {
      "label"  : "TLD",
      "value"  : 0
    },
    {
      "label"  : "Pengelola PKBM",
      "value"  : 0
    },
    {
      "label"  : "Pendidik PAUD",
      "value"  : 0
    },
    {
      "label"  : "Penilik",
      "value"  : 0
    },
    {
      "label"  : "Instruktur Kursus",
      "value"  : 0
    },
    {
      "label"  : "Tutor Paket A",
      "value"  : 0
    },
    {
      "label"  : "Tutor Paket B",
      "value"  : 0
    },
    {
      "label"  : "Tutor Paket C",
      "value"  : 0
    },
    {
      "label"  : "Kepala Lab (Non Guru)",
      "value"  : 0
    },
    {
      "label"  : "Kepala Perpustakaan (Non Guru)",
      "value"  : 0
    },
    {
      "label"  : "Tutor Keaksaraan",
      "value"  : 0
    },
    {
      "label"  : "Pengawas",
      "value"  : 120
    },
    {
      "label"  : "Wakil Kepala Sekolah",
      "value"  : 0
    },
    {
      "label"  : "Bendahara",
      "value"  : 0
    },
    {
      "label"  : "Kepala Laboratorium",
      "value"  : 15
    },
    {
      "label"  : "Teknisi Laboratorium",
      "value"  : 1
    },
    {
      "label"  : "laboran",
      "value"  : 14
    }
    ]
  }
];

datas = [
  {
    key: "Rasio PTK",
    values: [
    {
      "label"  : "Kepala Tenaga Administrasi",
      "value"  : 27
    },
    {
      "label"  : "Tenaga Administrasi",
      "value"  : 232
    },
    {
      "label"  : "Kepala Perpustakaan",
      "value"  : 8
    },
    {
      "label"  : "Tenaga Perpustakaan",
      "value"  : 146
    },
    {
      "label"  : "Pustakawan",
      "value"  : 0
    },
    {
      "label"  : "Pengawas",
      "value"  : 120
    },
    {
      "label"  : "Kepala Sekolah",
      "value"  : 246
    },
    {
      "label"  : "Wakil Kepala Sekolah",
      "value"  : 0
    },
    {
      "label"  : "Bendahara",
      "value"  : 0
    },
    {
      "label"  : "Kepala Laboratorium",
      "value"  : 15
    },
    {
      "label"  : "Teknisi Laboratorium",
      "value"  : 1
    },
    {
      "label"  : "laboran",
      "value"  : 14
    }
    ]
  }
];
// d3.json('http://223.27.144.201:8888/chart/data/profilsekolah_chart11_rasio_ptk.json', function(json) {
  // console.log(json);
  nv.addGraph(function() {
    var chart = nv.models.discreteBarChart()
        .x(function(d) { return d.label })
        .y(function(d) { return d.value })
        // .staggerLabels(true)
        .staggerLabels(datas[0].values.length > 8)
        .color(d3.scale.category20().range())
        .tooltips(true)
        // .showValues(true)
        .tooltipContent(function(key, x, y, e, graph) {
            return '<h3 class="btn-sm"><strong>' + x + '</strong></h3>' +
                   '<p>' +  y +'</p>'
          })
        // .hideable(true)
        // .showLabels(true)
        // .showLegend(false)
    ;

    d3.select('#chart11')
        .datum(datas)
        .transition().duration(500)
        .call(chart)
    ;

    nv.utils.windowResize(chart.update);

    return chart;
  });
// });