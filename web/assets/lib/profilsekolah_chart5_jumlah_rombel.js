
// d3.json('http://223.27.144.201:8888/silex/web/json/5', function(json) {
 d3.json('data/profilsekolah_chart5_jumlah_rombel.json', function(json) {
  // console.log(json);
  var countData = getTotalCountPie(json);
  var jWarna = ucolor(json);
  nv.addGraph(function() {
      var width = 600,
          height = 500
      ;

      var chart = nv.models.pieChart()
          .x(function(d) { return d.key })
          .y(function(d) { return d.y })
          .tooltipContent(function(key, y, e, graph) {
            var re = /,/gi;
            var re2 = /\.00/gi;
            
			// replace .00
            var x = y.replace(re2, "");
            // replace , to .
            var x = x.replace(re, "");
            // console.log(Math.round((x/countData)*100));
            var percent = Math.round((x/countData)*100);
            return '<h3 class="btn-sm"><strong>' + key + '</strong></h3>' +
                   '<p>' +  x + ' <strong>(' + percent + '%)</strong>' + '</p>'
          })
          .values(function(d) {
            // console.log(d);
            return d;
          })
          //.color(d3.scale.category10().range())
		  .color(jWarna);
      ;

      d3.select("#chart5")
        .datum([json])
        .transition().duration(500)
        .call(chart)
      ;
      nv.utils.windowResize(chart.update);

      return chart;
  });
});