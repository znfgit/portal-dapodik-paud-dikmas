// d3.json('http://223.27.144.201:8888/silex/web/json/6', function(json) {
 d3.json('data/profilsekolah_chart6_jumlah_rombelSD.json', function(json) {
  /**
   * Get total sum of given data
   * @param  {Object Json} datas [description]
   * @return {Object}       [description]
   */


  var countData = getTotalCount(json);
  // console.log(json);
  // console.log(countData);

  var chart;
  nv.addGraph(function() {
      chart = nv.models.multiBarChart();
        // .barColor(d3.scale.category20().range());

      chart.multibar
        .hideable(true);

      chart.reduceXTicks(false).staggerLabels(true);

      chart.xAxis
          .showMaxMin(false);
          // .showLabel(false)
         // .tickFormat(d3.format(',f'));

      chart.yAxis
          .tickFormat(d3.format('.'));

      chart.tooltipContent(function(key, x, y, e, graph) {
            // console.log(Math.round((x/countData)*100));
            var xTemp = key.replace(/ /gi, "");
            // console.log(xTemp);
            // console.log(countData[xTemp]);
            var percent = Math.round((y/countData[xTemp])*100);
            return '<h3 class="btn-sm"><strong>' + key + '</strong></h3>' +
                   '<p>' +  y + ' on ' + x + ' (<strong>'+ percent +'%/'+ countData[xTemp] +'</strong>)</p>'
          });

      d3.select('#chart6')
          .datum(json)
        .transition().duration(500).call(chart);

      nv.utils.windowResize(chart.update);

      // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

      return chart;
  });
});
