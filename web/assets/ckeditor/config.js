/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config ){
    config.filebrowserBrowseUrl = '../kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '../kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '../kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = '../../kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = '../kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '../kcfinder/upload.php?type=flash';
	config.toolbar = 'MyToolbar';
	config.resize_enabled = false;
 
	config.toolbar_MyToolbar =
	[
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'insert', items : [ 'Image','Table','Smiley','SpecialChar' ] },
	'/',
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	];
};